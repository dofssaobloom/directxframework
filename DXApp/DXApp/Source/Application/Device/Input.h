#pragma once
#include<Framework.h>

namespace application {

	class Input
	{
	public:

		static bool isAttack();


		/// <summary>
		/// ポーズボタン
		/// </summary>
		/// <returns></returns>
		static bool isPause();


		/// <summary>
		/// 攻撃物理攻撃チャージ
		/// </summary>
		/// <returns></returns>
		static bool isAttackChage();

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		static bool isAttackUpTrriger();

		static bool isBack();

		static bool isSetUp();

		static bool isLB();

		/// <summary>
		/// セットアップボタン押した瞬間
		/// </summary>
		/// <returns></returns>
		static bool isSetUpTrriger();

		/// <summary>
		/// セットアップボタンを離した瞬間
		/// </summary>
		/// <returns></returns>
		static bool isSetUpButtonUpTrriger();

		/// <summary>
		/// 武器チャージボタン
		/// </summary>
		/// <returns></returns>
		static bool isCharge();

		/// <summary>
		/// チャージボタン押した瞬間
		/// </summary>
		/// <returns></returns>
		static bool isChargeTrriger();

		/// <summary>
		/// 武器発射ボタン
		/// </summary>
		/// <returns></returns>
		static bool isShot();

		/// <summary>
		/// ジャンプボタン
		/// </summary>
		/// <returns></returns>
		static bool isJump();

		static bool isKeyAny();
		static bool isPadAny();

		static util::Vec2 leftVelocity();
		static bool leftDownTrigger();
		static bool leftUpTrigger();
		static bool leftRightTrigger();
		static bool leftLeftTrigger();
		static util::Vec2 rightVelocity();

		static bool m_ReverseX;
		static bool m_ReverseY;

	private:
		Input() {}
	};
}
