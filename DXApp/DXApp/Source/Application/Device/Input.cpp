#include"Input.h"

namespace application {
	bool Input::m_ReverseX = false;
	bool Input::m_ReverseY = false;

	bool Input::isAttack()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		return pad.getButtonTrriger(framework::ButtonCode::B) | key.isKeyDownTrriger(framework::KeyBoard::KeyCords::ENTER);
	}

	bool Input::isPause()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		return pad.getButtonTrriger(framework::ButtonCode::Start) | key.isKeyDownTrriger(framework::KeyBoard::KeyCords::P);
	}

	bool Input::isAttackChage()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		return pad.getButton(framework::ButtonCode::B) | key.isKeyDown(framework::KeyBoard::KeyCords::ENTER);
	}

	bool Input::isAttackUpTrriger()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		return pad.getUpButtonTrriger(framework::ButtonCode::B) | key.isKeyUpTrriger(framework::KeyBoard::KeyCords::ENTER);
	}

	bool Input::isBack()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		return pad.getButtonTrriger(framework::ButtonCode::X) | key.isKeyDownTrriger(framework::KeyBoard::KeyCords::BACK);
	}

	bool Input::isSetUp()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		return pad.isPushLT() | key.isKeyDown(framework::KeyBoard::KeyCords::LSHIFT);
	}

	bool Input::isLB()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		return pad.getButtonTrriger(framework::ButtonCode::LeftShouler) | key.isKeyDownTrriger(framework::KeyBoard::KeyCords::Q);
	}

	bool Input::isSetUpTrriger()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		return pad.isPushLTTrriger() | key.isKeyDownTrriger(framework::KeyBoard::KeyCords::LSHIFT);
	}

	bool Input::isSetUpButtonUpTrriger()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		return pad.isUpLTTrriger() | key.isKeyUpTrriger(framework::KeyBoard::KeyCords::LSHIFT);
	}

	bool Input::isCharge()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		return pad.isPushRT() | key.isKeyDown(framework::KeyBoard::KeyCords::ENTER);
	}

	bool Input::isChargeTrriger()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		return pad.isPushRTTrriger() | key.isKeyDownTrriger(framework::KeyBoard::KeyCords::ENTER);
	}

	bool Input::isShot()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		return pad.isUpRTTrriger() | key.isKeyUpTrriger(framework::KeyBoard::KeyCords::ENTER);
	}

	bool Input::isJump()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		return pad.getButtonTrriger(framework::ButtonCode::A) | key.isKeyDownTrriger(framework::KeyBoard::KeyCords::SPACE);
	}

	util::Vec2 Input::rightVelocity()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		int front = key.isKeyDown(framework::KeyBoard::KeyCords::UP);
		int down = -key.isKeyDown(framework::KeyBoard::KeyCords::DOWN);
		int left = -key.isKeyDown(framework::KeyBoard::KeyCords::LEFT);
		int right = key.isKeyDown(framework::KeyBoard::KeyCords::RIGHT);

		//���]
		float signX = m_ReverseX ? -1 : 1;
		float signY = m_ReverseY ? -1 : 1;

		auto result = pad.getRightStick() + util::Vec2(left + right, front + down).normalize();
		
		result.x *= signX;
		result.y *= signY;

		return result;

	}

	util::Vec2 Input::leftVelocity()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		int front = key.isKeyDown(framework::KeyBoard::KeyCords::W);
		int down = -key.isKeyDown(framework::KeyBoard::KeyCords::S);
		int left = -key.isKeyDown(framework::KeyBoard::KeyCords::A);
		int right = key.isKeyDown(framework::KeyBoard::KeyCords::D);

		return pad.getLeftStick().normalize() + util::Vec2(left + right, front + down).normalize();
	}

	bool Input::leftDownTrigger()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		int down = -key.isKeyDownTrriger(framework::KeyBoard::KeyCords::S);
		
		return pad.isLeftStick(framework::StickDir::Down) || down;
	}

	bool Input::leftUpTrigger()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		int front = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::W);

		return pad.isLeftStick(framework::StickDir::Up) || front;
	}

	bool Input::leftRightTrigger()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		int right = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::D);

		return pad.isLeftStick(framework::StickDir::Right) || right;
	}

	bool Input::leftLeftTrigger()
	{
		auto&& pad = framework::Input::getGamePad();
		auto&& key = framework::Input::getKey();

		int left = -key.isKeyDownTrriger(framework::KeyBoard::KeyCords::A);
		return pad.isLeftStick(framework::StickDir::Left) || left;
	}

	bool Input::isKeyAny()
	{
		auto&& key = framework::Input::getKey();

		auto kayA = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::A);
		auto kayW = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::W);
		auto kayS = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::S);
		auto kayD = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::D);
		auto keyQ = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::Q);
		auto keyE = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::E);

		auto kayFront = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::UP);
		auto kayDown = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::DOWN);
		auto kayLeft = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::LEFT);
		auto kayRight = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::RIGHT);

		auto keyEnter = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::ENTER);
		auto keyBack = key.isKeyDownTrriger(framework::KeyBoard::KeyCords::BACK);

		return kayA || kayW || kayS || kayD || keyQ || keyE || kayFront || kayDown || kayLeft || kayRight || keyEnter || keyBack;
	}

	bool Input::isPadAny()
	{
		auto&& pad = framework::Input::getGamePad();

		auto padA = pad.getButtonTrriger(framework::ButtonCode::A);
		auto padB = pad.getButtonTrriger(framework::ButtonCode::B);
		auto padX = pad.getButtonTrriger(framework::ButtonCode::X);
		auto padY = pad.getButtonTrriger(framework::ButtonCode::Y);

		auto padBack = pad.getButtonTrriger(framework::ButtonCode::Back);
		auto padStart = pad.getButtonTrriger(framework::ButtonCode::Start);

		auto padLB = pad.getButtonTrriger(framework::ButtonCode::LeftShouler);
		auto padRB = pad.getButtonTrriger(framework::ButtonCode::RightShouler);

		return padA || padB || padX || padY || padBack || padStart || padLB || padRB;
	}
}