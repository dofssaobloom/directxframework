#include"TestApp.h"
#include<Framework\Source\Application\WindowFactory\WindowFactory.h>
#include<Framework\Source\Util\Type.h>
#include<Framework\Source\Device\Render\DirectXInstance.h>
#include<Source\Application\Screen\Screen.h>
#include<Framework\Source\Util\Render\DXFunc.h>
#include<stdio.h>
#include<resource.h>
#include<Framework\Source\Util\Event\WindowEvent.h>
#include<Framework\Source\Util\Win\WinFunc.h>
#include<Framework\Source\Device\Render\Shader\StandardMaterial.h>
#include<Framework\Source\Task\TaskManager.h>
#include<Framework\Source\Entity\Entity.h>
#include<Framework\Source\Resource\ResourceManager.h>
#include<Framework\Source\Application\Scene\SceneThread.h>
#include<Framework\Source\Application\Screen\Screen.h>
#include<Framework\Source\Util\WrapFunc.h>
#include<Framework\Source\Device\Input\Input.h>
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>
#include<Source\Application\Device\Input.h>

#include<Framework.h>

#include<Source\Component\DebugTest\Test.h>
#include<Source\Component\DebugTest\CollisionCallBackTest.h>
#include<Source\Component\DebugTest\DebugContoroller.h>
#include<Source\Component\DebugTest\DebugParticl.h>
#include<Source\Component\DebugTest\DebugPointLightController.h>
#include<Source\Component\DebugTest\DebuigPhysics.h>
#include<Source\Component\Device\CameraController.h>
#include<Source\Component\Player\AttackComponent.h>
#include<Source\Component\Player\PlayerComponent.h>
#include<Source\Component\Enemy\EnemyAttackComponent.h>
#include<Source\Component\Scene\GameMain\HPUI.h>
#include<Source\Component\Scene\GameMain\KillCounter.h>
#include<Source\Component\Scene\GameMain\ReadyComponent.h>
#include<Source\Component\Scene\GameMain\RulesComponent.h>
#include<Source\Component\Scene\Result\NextButton.h>
#include<Source\Component\Scene\Result\RankSortComponent.h>
#include<Source\Component\Scene\Title\Buttom.h>
#include<Source\Component\Scene\Title\Menu.h>
#include<Source\Component\Scene\Title\TitleCharacter.h>
#include<Source\Util\ImGUI\ImGUI.h>
#include<Source\Component\Bullet\Bullet.h>
#include<Source\Component\Bullet\LaunchingDevice.h>

#include<Source\Component\Enemy\EnemySpowner.h>
#include<Source\Component\Enemy\EnemyComponent.h>
#include<Source\Component\Enemy\BossEnemyComponent.h>
#include<Source\Component\Enemy\PoisonExplosion.h>
#include<Source\Component\Player\BoneJointComponent.h>
#include<Source\Component\Scene\GameMain\GameTimerCompornent.h>
#include<Source\Component\Scene\GameMain\ScoreComponent.h>
#include<Source\Component\Scene\GameMain\ComboComponent.h>
#include<Source\Component\Scene\GameMain\EnemyRulesComponent.h>

#include<Source\Component\Scene\Result\ResultTest.h>
#include<Source\Component\Scene\GameMain\RulesComponent.h>
#include<Source\Component\Player\SearchComponent.h>
#include<Source\Component\Scene\Title\Title.h>
#include<Source\Component\Scene\Config\ConfigScene.h>
#include<Source\Component\Scene\GameMain\GmaePause.h>
#include<Source\Component\Scene\Credit\Credit.h>

#include<Source\Component\Scene\GameMain\AddCount.h>


using namespace component;

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp) {

	util::ImGUI::updateMouseHandle(hwnd, msg, wp, lp);

	switch (msg)
	{
	case WM_COMMAND:
#ifdef _MDEBUG
		util::WinFunc::switchCheckItem(hwnd, wp);
		util::WindowEvent::getInstance()->onEvent(LOWORD(wp), hwnd);
#endif // _MDEBUG

		break;

	case WM_KEYDOWN:
		if (wp == VK_ESCAPE) {
			PostQuitMessage(0);
		}

		break;
	case WM_DESTROY:        // 閉じるボタンをクリックした時
		PostQuitMessage(0); // WM_QUITメッセージを発行
		break;
	case WM_CLOSE:

		//int isOK = util::WinFunc::drawQuestion(hwnd, TEXT(""), TEXT("終了しますか？"));
		//if (isOK != IDOK) {
		//	return 0;
		//}

		break;
	}

	return DefWindowProc(hwnd, msg, wp, lp);
}


TestApp::TestApp(HINSTANCE hInst)
	:Application(hInst)
{
	util::CSVLoader loader("Resource/Confing.csv");

	auto param = loader.load();


	int widht = atoi(param[0][1].c_str());
	int height = atoi(param[0][2].c_str());
	bool isWindowMode = (bool)atoi(param[1][1].c_str());
	int pixX = atoi(param[2][1].c_str());
	int pixY = atoi(param[2][2].c_str());

	framework::Screen::WINDOW_WIDTH = Screen::WINDOW_WIDTH = widht;
	framework::Screen::WINDOW_HEIGHT = Screen::WINDOW_HEIGHT = height;
	framework::Screen::WINDOW_WIDTH_HALF = Screen::WINDOW_WIDTH_HALF = framework::Screen::WINDOW_WIDTH / 2;
	framework::Screen::WINDOW_HEIGHT_HALF = Screen::WINDOW_HEIGHT_HALF = framework::Screen::WINDOW_HEIGHT / 2;
	framework::Screen::PERSPECTIVE = Screen::PERSPECTIVE;
	framework::Screen::FAR_ = Screen::FAR_;

	framework::Screen::IS_FULLSCREEN = Screen::IS_FULLSCREEN = !isWindowMode;

	framework::Screen::UI_WIDTH = Screen::UI_WIDTH;
	framework::Screen::UI_HEIGHT = Screen::UI_HEIGHT;

	framework::Screen::PIXEL_WIDTH = Screen::PIXEL_WIDTH = pixX;
	framework::Screen::PIXEL_HEIGHT = Screen::PIXEL_HEIGHT = pixY;
	framework::Screen::PIXEL_WIDTH_HALF = Screen::PIXEL_WIDTH_HALF = framework::Screen::PIXEL_WIDTH / 2;
	framework::Screen::PIXEL_HEIGHT_HALF = Screen::PIXEL_HEIGHT_HALF = framework::Screen::PIXEL_HEIGHT / 2;



	//アプリケーションのウィンドウを生成する

	framework::WindowFactory wf(m_hInstance);
	//リリース用ウィンドウクラス作成
	wf.setClassName(TEXT("Release"));
	wf.setProc(WndProc);
	wf.setStyle(CS_HREDRAW | CS_VREDRAW);
	wf.setIcon(hInst, MAKEINTRESOURCE(IDI_ICON2));
	wf.regist();

	//デバッグ用ウィンドウクラス作成
	wf.setClassName(TEXT("Debug"));
	wf.setMenuName(IDR_MENU1);
	wf.regist();

#ifdef _MDEBUG
	m_hWnd = wf.createWnd(TEXT("Debug"), TEXT("PoisonBursunie"), util::Vec2(0, 0), util::Vec2(Screen::WINDOW_WIDTH, Screen::WINDOW_HEIGHT));
	m_WindowName = "Debug";
#else
	m_hWnd = wf.createWnd(TEXT("Release"), TEXT("PoisonBursunie"), util::Vec2(100, 100), util::Vec2(Screen::WINDOW_WIDTH, Screen::WINDOW_HEIGHT));
	m_WindowName = "Release";
#endif
	SetClassLong(m_hWnd, GCL_HBRBACKGROUND, (LONG)CreateSolidBrush(RGB(0, 0, 0)));

	ShowWindow(m_hWnd, SW_SHOW);


	// デバイスとスワップ・チェインの作成
	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));    // 構造体の値を初期化(必要な場合)
	sd.BufferCount = 1;       // バック・バッファ数
	sd.BufferDesc.Width = Screen::PIXEL_WIDTH;     // バック・バッファの幅
	sd.BufferDesc.Height = Screen::PIXEL_HEIGHT;     // バック・バッファの高さ
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;  // フォーマット
	sd.BufferDesc.RefreshRate.Numerator = 30;  // リフレッシュ・レート(分子)
	sd.BufferDesc.RefreshRate.Denominator = 1; // リフレッシュ・レート(分母)
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; // バック・バッファの使用法
	sd.OutputWindow = m_hWnd;    // 関連付けるウインドウ
	sd.SampleDesc.Count = 1;        // マルチ・サンプルの数
	sd.SampleDesc.Quality = 0;      // マルチ・サンプルのクオリティ
	sd.Windowed = isWindowMode;             // ウインドウ・モード
	sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH; // モード自動切り替え



	HRESULT hr = framework::DirectXInstance::getInstance()->create(sd);

	util::ImGUI::initAPI(m_hWnd);

	framework::Input::createInstance(m_hInstance);

	util::WindowEvent::getInstance()->addEvent(ID_RESET, [&](HWND hwnd) {
		scene.clear();
		importMaterial();
		init();
	});

	scene.registComponent<Test>("Test");
	scene.registComponent<CameraController>("CameraContoroller");
	scene.registComponent<DebugContoroller>("DebugContoroller");
	scene.registComponent<DebugPointLightController>("DebugPointLightController");
	scene.registComponent<DebugParticl>("DebugParticl");
	scene.registComponent<TitleCharacter>("TitleCharacter");
	scene.registComponent<Buttom>("Buttom");
	scene.registComponent<RankSortComponent>("RankSortComponent");
	scene.registComponent<NextButton>("NextButton");
	scene.registComponent<ReadyComponent>("ReadyComponent");
	scene.registComponent<CollisionCallBackTest>("CollisionCallBackTest");
	scene.registComponent<CameraController>("CameraController");
	scene.registComponent<HPUI>("HPUI");
	scene.registComponent<Menu>("Menu");
	scene.registComponent<KillCounter>("KillCounter");
	scene.registComponent<PlayerComponent>("PlayerComponent");
	scene.registComponent<AttackComponent>("AttackComponent");
	scene.registComponent<EnemyAttackComponent>("EnemyAttackComponent");
	scene.registComponent<Bullet>("Bullet");
	scene.registComponent<LaunchingDevice>("LaunchingDevice");

	scene.registComponent<EnemySpowner>("EnemySpowner");
	scene.registComponent<EnemyComponent>("EnemyComponent");
	scene.registComponent<BossEnemyComponent>("BossEnemyComponent");
	scene.registComponent<PoisonExplosion>("PoisonExplosion");
	scene.registComponent<BoneJointComponent>("BoneJointComponent");

	scene.registComponent<GameTimerCompornent>("GameTimerCompornent");
	scene.registComponent<ScoreComponent>("ScoreComponent");
	scene.registComponent<ComboComponent>("ComboComponent");
	scene.registComponent<EnemyRulesComponent>("EnemyRulesComponent");

	scene.registComponent<RulesComponent>("RulesComponent");
	scene.registComponent<ResultTest>("ResultTest");
	scene.registComponent<SearchComponent>("SearchComponent");
	scene.registComponent<Title>("Title");
	scene.registComponent<ConfigScene>("ConfigScene");
	scene.registComponent<GamePause>("GamePause");
	scene.registComponent<Credit>("Credit");

	scene.registComponent<AddCount>("AddCount");

	framework::EffectManager::getInstance()->create(8000);
	framework::EffectManager::getInstance()->registPath("Resource/Effect/Path.csv");
	framework::EffectManager::getInstance()->clear();


}

TestApp::~TestApp()
{
	util::ImGUI::endAPI();
}

void TestApp::init()
{

	//util::Quaternion rotateQuaternion = XMQuaternionRotationRollPitchYaw(XMConvertToRadians(11.54), XMConvertToRadians(90.54), XMConvertToRadians(0));
	//auto euler = util::quaternionToEuler(rotateQuaternion);

	util::CSVLoader loader("Resource/FirstScene.csv");
	auto firstName = loader.load();

	//コメントの行削除
	firstName.erase(firstName.begin());

	scene.setScenePath(firstName[0][0]);

	scene.resourceLoad();

	scene.construction();

	scene.init();

	framework::TaskManager::getInstance()->init();
}

void TestApp::update()
{
	framework::Input::update();
	framework::TaskManager::getInstance()->update();
	framework::TaskManager::getInstance()->physicsUpdate();
}

void TestApp::draw()
{
	framework::TaskManager::getInstance()->draw();
}
