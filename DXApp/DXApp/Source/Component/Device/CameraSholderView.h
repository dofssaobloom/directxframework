#pragma once

#include<Source\State\State.h>
#include<Source\Component\Device\CameraController.h>

class CameraSholderView : public State<component::CameraController>
{
public:
	CameraSholderView();
	~CameraSholderView();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::CameraController* outerValue);

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::CameraController* outerValue, std::shared_ptr<State<component::CameraController>>* nextState);

	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::CameraController* outerValue);

private:
	util::Vec3 rotate(component::CameraController* outerValue);

	util::Vec3 m_sholderViewOffset;
	util::Vec3 m_playerSholderOffset;

	util::Vec2 m_DefClampX;
	util::Vec2 m_sholderAngle;
	float m_lerpAngleX;
	float m_SholderRadius;
	float m_defaAngleY;
};