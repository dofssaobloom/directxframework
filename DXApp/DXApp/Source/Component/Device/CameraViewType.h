#pragma once


enum class CameraViewType
{
	ThirdPersonView,
	SholderView,
};