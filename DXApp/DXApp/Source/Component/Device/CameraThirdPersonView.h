#pragma once

#include<Source\State\State.h>
#include<Source\Component\Device\CameraController.h>

class CameraThirdPersonView : public State<component::CameraController>
{
public:
	CameraThirdPersonView();
	~CameraThirdPersonView();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::CameraController* outerValue);

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::CameraController* outerValue, std::shared_ptr<State<component::CameraController>>* nextState);

	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::CameraController* outerValue);

private:
	util::Vec3 rotate(component::CameraController* outerValue);

private:

	util::Vec3 m_TPViewOffset;
};