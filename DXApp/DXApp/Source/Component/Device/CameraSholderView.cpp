#include "CameraSholderView.h"
#include<Source\Component\Device\CameraThirdPersonView.h>
#include<Source\Application\Device\Input.h>

UsingNamespace;

CameraSholderView::CameraSholderView()
	:m_SholderRadius(200),
	m_defaAngleY(XMConvertToRadians(170)),
	m_sholderViewOffset(util::Vec3(-80, 130, -10))
{
}

CameraSholderView::~CameraSholderView()
{
}

void CameraSholderView::entryAction(component::CameraController * outerValue)
{
	auto lookTrans = outerValue->getLookEntity().lock()->getTransform();
	//カメラ方向とプレイヤーの向きが逆なため＋180
	lookTrans->m_Rotation.y = outerValue->getRotation().y + 180;
	outerValue->m_startOffSet = outerValue->m_OffSet;

	m_sholderAngle = util::Vec2(0, m_defaAngleY);

	float lerpTime = 0.1f;	//Lerp時間（s）
	outerValue->changeCameraLerp(m_SholderRadius, lerpTime);
	//元々の角度上限を保存
	m_DefClampX = outerValue->m_ClampAngleX;
	//角度上限更新
	outerValue->m_ClampAngleX = util::Vec2(-20,40);
}

Trans CameraSholderView::inputAction(component::CameraController * outerValue, std::shared_ptr<State<component::CameraController>>* nextState)
{
	if (outerValue->m_viewType == CameraViewType::ThirdPersonView) {
		outerValue->m_viewType = CameraViewType::ThirdPersonView;
		*nextState = std::make_shared<CameraThirdPersonView>();
		return Trans::Trans_Occured;
	}

	//ｘ軸回転は自由　ｙ軸回転は固定
	outerValue->m_Angle = util::Vec2(outerValue->m_Angle.x, m_defaAngleY);

	auto lookTrans = outerValue->getLookEntity().lock()->getTransform();

	util::Vec3 lerpLength = m_sholderViewOffset - outerValue->m_startOffSet;
	outerValue->m_OffSet.x = util::bezierCurve(outerValue->getLerpRate(), outerValue->m_startOffSet.x, outerValue->m_startOffSet.x + lerpLength.x * 2 / 3, m_sholderViewOffset.x);
	outerValue->m_OffSet.y = util::bezierCurve(outerValue->getLerpRate(), outerValue->m_startOffSet.y, outerValue->m_startOffSet.y + lerpLength.y * 2 / 3, m_sholderViewOffset.y);
	outerValue->m_OffSet.z = util::bezierCurve(outerValue->getLerpRate(), outerValue->m_startOffSet.z, outerValue->m_startOffSet.z + lerpLength.z * 2 / 3, m_sholderViewOffset.z);

	util::Vec3 playerUp = lookTrans->up();
	util::Vec3 playerRight = -lookTrans->left();
	util::Vec3 playerFront = lookTrans->front();

	//オフセットの位置を現在の方向に対応させる
	util::Vec3 playerSholderOffsetX = playerRight * outerValue->m_OffSet.x;
	util::Vec3 playerSholderOffsetY = playerUp    * outerValue->m_OffSet.y;
	util::Vec3 playerSholderOffsetZ = playerFront * outerValue->m_OffSet.z;
	m_playerSholderOffset = playerSholderOffsetX + playerSholderOffsetY;
	m_playerSholderOffset += playerSholderOffsetZ;

	outerValue->getCamera().lock()->setLookOffset(m_playerSholderOffset);
	outerValue->initDefaPos();

	outerValue->createNormalize();
	rotate(outerValue);

	*nextState = std::make_shared<CameraSholderView>(*this);

	return Trans::Trans_Again;
}

void CameraSholderView::exitAction(component::CameraController * outerValue)
{
	outerValue->m_ClampAngleX = m_DefClampX;
}

util::Vec3 CameraSholderView::rotate(component::CameraController * outerValue)
{

	util::Vec3 result;
	auto lookTrans = outerValue->getLookEntity().lock()->getTransform();

	//Quaternionに変換
	util::Quaternion q = XMQuaternionRotationAxis(outerValue->getUpVector().toXMVector(), outerValue->m_Angle.y);

	//プレイヤーの回転と合わせる
	float angle = -XMConvertToRadians(lookTrans->m_Rotation.y);
	q = XMQuaternionMultiply(q.toXMVector(), XMQuaternionRotationAxis(outerValue->getUpVector().toXMVector(), angle));

	//Quaternion合成（左右と上下の回転を合成）
	q = XMQuaternionMultiply(q.toXMVector(), XMQuaternionRotationAxis(outerValue->getRightVector().toXMVector(), outerValue->m_Angle.x));

	result = util::rotation(q, outerValue->getDeafaPos());

	//３人称視点
	result += (lookTrans->m_Position);

	//肩越し視点
	result += m_playerSholderOffset;

	outerValue->setPosition(result);

	return  result;
}
