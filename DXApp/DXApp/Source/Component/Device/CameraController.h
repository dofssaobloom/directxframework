#pragma once
#include<Framework.h>
#include <Source\Component\Device\CameraViewType.h>
#include<Source\Util\Math\Randam.h>

class ShakeMove
{
public:
	ShakeMove();
	~ShakeMove();

	void setBeginOffset(const util::Vec3& begin);

	void begin(float width,float width2);

	void updaet();
	
	util::Vec3 get(util::Transform* trans);

	bool isBegin();

private:
	void entiry();

	void stay();

	void exit();

private:
	bool m_IsBegin;
	int shakeNum;
	util::Randam m_Random;

	util::Vec3 m_BeginPos;

	float m_Width;
	float m_Width2;

	float m_Power;

	int m_Count;

	float m_Rand;
	std::unique_ptr<util::Timer> m_pMoveTimer;

	bool m_IsFirst;
	bool m_IsEnd;
};



namespace component {

	class PhysicsWorld;

	class CameraController : public framework::UpdateComponent
	{
	public:
		CameraController();
		~CameraController();

		void init()override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		/// <summary>
		/// 注視対象からの位置を保存
		/// </summary>
		void initDefaPos();

		/// <summary>
		///　回転の軸となるベクトルを生成
		/// </summary>
		void createNormalize();


		/// <summary>
		/// 半径変更
		/// </summary>
		void changeRadius(float radius);

		/// <summary>
		/// 半径変更Lerp処理
		/// </summary>
		void changeCameraLerp(float targetRadius, float lerpSecond);

		/// <summary>
		/// 角度変更Lerp処理
		/// </summary>
		void changeAngleLerp(util::Vec2 targetAngle, float lerpSecond);

		/// <summary>
		/// 角度を加える（上限下限あり）
		/// </summary>
		void addRadius(float addRadius);

		/// <summary>
		/// カメラタイプ変更（引数）
		/// </summary>
		void changeView(CameraViewType viewType);

		/// <summary>
		/// もう一つのカメラタイプへ
		/// </summary>
		void changeView();

		/// <summary>
		/// 位置設定
		/// </summary>
		void setPosition(util::Vec3 position);

		util::Vec3 getRotation();
		framework::WeakEntity getLookEntity();
		std::weak_ptr<CameraComponent> getCamera();
		util::Vec3 getDeafaPos();
		float getDefaRadius();
		float getOffsetY();
		util::Vec3 getUpVector();
		util::Vec3 getRightVector();
		float getLerpRate();

		/// <summary>
		/// アングル取得
		/// </summary>
		/// <returns></returns>
		const util::Vec2& getAgnle();

		/// <summary>
		/// 一番したを無輝時０　上むいてるとき１
		/// </summary>
		/// <returns></returns>
		const float angleRate();

		/// <summary>
		/// 画面揺らしOn
		/// </summary>
		void onShake(framework::WeakEntity& entity);


		void setShakeOffset(const util::Vec3& offset);

	private:

		/// <summary>
		/// X軸回転
		/// </summary>
		void moveAxisX();

		/// <summary>
		/// Y軸回転
		/// </summary>
		void moveAxisY();

		/// <summary>
		/// カメラ当たり判定Lerp処理更新
		/// </summary>
		void lerpUpdate();

		/// <summary>
		/// 角度Lerp処理更新
		/// </summary>
		void lerpAngleUpdate();

		/// <summary>
		/// カメラ回転
		/// </summary>
		virtual util::Vec3 rotate();

		/// <summary>
		/// カメラ当たり判定
		/// </summary>
		void hitWall();

	public:
		util::Vec2 m_Angle;	//回転角度（ｘ軸回転,ｙ軸回転）
		util::Vec2 m_startAngle;	//Lerp用開始角度

		CameraViewType m_viewType;	//カメラタイプ

		//!カメラX軸回転クランプ（x:Min y:Max）
		util::Vec2 m_ClampAngleX;

		util::Vec3 m_OffSet;
		util::Vec3 m_startOffSet;	//Lerp用開始距離

	private:

		std::weak_ptr<CameraComponent> m_pCamera;
		//!注視オブジェクト
		framework::WeakEntity m_pLookEntity;

		//!カメラと注視オブジェクトの距離
		float m_Radius;

		float m_startRadius;	//Lerp用開始距離
		float m_targetRadius;	//Lerp用目標距離

		float m_lerpCount;		//Lerp用カウント
		float m_lerpTime;		//Lerpにかかる時間

		util::Vec2 m_lerpStartAngle;
		util::Vec2 m_lerpTargetAngle;
		std::unique_ptr<util::Timer> m_lerpAngleTimer;

		//三人称視点の時の距離（後に三人称視点用スクリプトに移動）
		float m_DefaRadius;
		//!アングル回転速度
		util::Vec2 m_AngleSpeed;

		util::Vec3 m_upVec;		//上方向ベクトル
		util::Vec3 m_rightVec;	//下方向ベクトル

		//!注視点高さオフセット
		float m_OffsetY;

		//現在の状態
		std::shared_ptr<State<CameraController>> m_States;

		util::Vec3 m_defaPos;

		float m_hitMoveSpeed;	//カメラ衝突時のズーム速度
		std::weak_ptr<component::PhysicsWorld > m_world;

		ShakeMove m_ShakeMove;
		const int MAX_SHAKE = 1800;
		bool isright;
	};



}