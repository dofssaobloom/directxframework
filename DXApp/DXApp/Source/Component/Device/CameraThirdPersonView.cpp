#include "CameraThirdPersonView.h"
#include <Source\Component\Device\CameraController.h>
#include<Source\Component\Device\CameraSholderView.h>
#include<Source\Application\Device\Input.h>

UsingNamespace;


CameraThirdPersonView::CameraThirdPersonView()
{
}

CameraThirdPersonView::~CameraThirdPersonView()
{
}

void CameraThirdPersonView::entryAction(component::CameraController * outerValue)
{
	auto lookTrans = outerValue->getLookEntity().lock()->getTransform();
	outerValue->m_Angle = util::Vec2(outerValue->m_Angle.x, XMConvertToRadians(-lookTrans->m_Rotation.y+180));
	outerValue->m_startOffSet = outerValue->m_OffSet;
	m_TPViewOffset = util::Vec3(0, outerValue->getOffsetY(), 0);

	//角度上限更新
	outerValue->m_ClampAngleX = util::Vec2(-20, 40);

	outerValue->getCamera().lock()->setLookOffset(outerValue->m_OffSet);
	//画面揺らし中に視点が切り替わったときオフセットを変えるとバグるのでオフセット再設定
	outerValue->setShakeOffset(outerValue->getCamera().lock()->getOffet());
	outerValue->changeCameraLerp(outerValue->getDefaRadius(), 0.1f);
	outerValue->initDefaPos();
	outerValue->createNormalize();
}

Trans CameraThirdPersonView::inputAction(component::CameraController * outerValue, std::shared_ptr<State<component::CameraController>>* nextState)
{
	if (outerValue->m_viewType == CameraViewType::SholderView) {
		outerValue->m_viewType = CameraViewType::SholderView;
		*nextState = std::make_shared<CameraSholderView>();
		return Trans::Trans_Occured;
	}
	util::Vec3 lerpLength = m_TPViewOffset - outerValue->m_startOffSet;
	outerValue->m_OffSet.x = util::bezierCurve(outerValue->getLerpRate(), outerValue->m_startOffSet.x, outerValue->m_startOffSet.x + lerpLength.x * 2 / 3, m_TPViewOffset.x);
	outerValue->m_OffSet.y = util::bezierCurve(outerValue->getLerpRate(), outerValue->m_startOffSet.y, outerValue->m_startOffSet.y + lerpLength.y * 2 / 3, m_TPViewOffset.y);
	outerValue->m_OffSet.z = util::bezierCurve(outerValue->getLerpRate(), outerValue->m_startOffSet.z, outerValue->m_startOffSet.z + lerpLength.z * 2 / 3, m_TPViewOffset.z);

	outerValue->getCamera().lock()->setLookOffset(outerValue->m_OffSet);
	//画面揺らし中に視点が切り替わったときオフセットを変えるとバグるのでオフセット再設定
	outerValue->setShakeOffset(outerValue->getCamera().lock()->getOffet());
	outerValue->initDefaPos();
	outerValue->createNormalize();

	rotate(outerValue);

	*nextState = std::make_shared<CameraThirdPersonView>(*this);
	return Trans::Trans_Again;
}

void CameraThirdPersonView::exitAction(component::CameraController * outerValue){}

util::Vec3 CameraThirdPersonView::rotate(component::CameraController * outerValue)
{
	util::Vec3 result;
	auto lookTrans = outerValue->getLookEntity().lock()->getTransform();

	//Quaternionに変換
	util::Quaternion q = XMQuaternionRotationAxis(outerValue->getUpVector().toXMVector(), outerValue->m_Angle.y);

	//Quaternion合成（左右と上下の回転を合成）
	q = XMQuaternionMultiply(q.toXMVector(), XMQuaternionRotationAxis(outerValue->getRightVector().toXMVector(), outerValue->m_Angle.x));

	result = util::rotation(q, outerValue->getDeafaPos());

	//３人称視点
	result += lookTrans->m_Position;
	//オフセット分
	result += util::Vec3(0, outerValue->getOffsetY(), 0);

	outerValue->setPosition(result);
	return  result;
}

