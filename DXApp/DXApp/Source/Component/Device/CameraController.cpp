#include"CameraController.h"
#include<Source\Util\Lua\LuaBase.h>
#include<Source\Application\Device\Input.h>
#include <Source\Component\Device\CameraThirdPersonView.h>
#include<Source\Component\Physics\PhysicsWorld.h>
#include <algorithm>
#include <math.h>
using namespace framework;

ShakeMove::ShakeMove()
{
	m_IsBegin = false;
	m_Count = 0;
}

ShakeMove::~ShakeMove()
{
}

void ShakeMove::setBeginOffset(const util::Vec3 & begin)
{
	m_BeginPos = begin;
}


void ShakeMove::begin(float width, float width2)
{
	m_Count += 1;
	m_Width = width + (m_Count * width * 0.1);
	m_Width2 = width2;
	m_IsBegin = true;
	m_IsFirst = true;
	m_IsEnd = false;
	shakeNum = 3;
}


void ShakeMove::updaet()
{
	if (!m_IsBegin)return;

	if (m_IsFirst) {
		entiry();
	}

	if (!m_IsEnd) {
		m_IsFirst = false;
		stay();
	}
	else {
		exit();
	}

}

util::Vec3 ShakeMove::get(util::Transform* trans)
{
	return trans->up() * m_Power + m_BeginPos;
}

bool ShakeMove::isBegin()
{
	return m_IsBegin;
}

void ShakeMove::entiry()
{
	m_pMoveTimer = std::make_unique<util::Timer>(5);

	auto rate = ((float)shakeNum / 3);

	m_Rand = m_Random.next<float>((m_Width + m_Width2 ) * -rate, (m_Width + m_Width2) * rate);
}

void ShakeMove::stay()
{
	m_Power = util::bezierCurve(m_pMoveTimer->rate(), 0, m_Rand, 0);

	if (m_pMoveTimer->isEnd()) {
		--shakeNum;
		entiry();
	}

	//κθρVFCN΅½ηIΉ
	if (shakeNum == 0) {
		m_IsEnd = true;
	}

	m_pMoveTimer->update();

}

void ShakeMove::exit()
{
	m_IsBegin = false;
	m_Count = 0;
}


namespace component {

	CameraController::CameraController()
		:m_viewType(CameraViewType::ThirdPersonView),
		m_Angle(util::Vec2(0, 0)),
		m_lerpAngleTimer(std::make_unique<util::Timer>(0))
	{
	}

	CameraController::~CameraController()
	{
	}

	void CameraController::init()
	{
		m_States = std::make_shared<CameraThirdPersonView>();
		m_pCamera = m_Entity.lock()->getComponent<CameraComponent>();
		m_pLookEntity = m_pCamera.lock()->getLookEntity();
		float initAngle = 0;
		m_Angle.x = XMConvertToRadians(initAngle);
		m_Angle.y = XMConvertToRadians(m_pLookEntity.lock()->getTransform()->m_Rotation.y);
		m_OffSet = util::Vec3(0, m_OffsetY, 0);
		m_pCamera.lock()->setLookOffset(m_OffSet);

		m_upVec = util::Vec3(0, 1, 0);
		m_upVec = m_upVec.normalize();

		initDefaPos();
		createNormalize();

		m_world = Entity::findGameObj("PhysicsWorld").lock()->getComponent<PhysicsWorld>();
	}

	void CameraController::update()
	{
		hitWall();

		moveAxisX();
		moveAxisY();

		m_States = m_States->update(this);
		lerpUpdate();
		lerpAngleUpdate();

		if (application::Input::isLB())
		{
			float targetAngleY = (int)-m_pLookEntity.lock()->getTransform()->m_Rotation.y + 180 % 360;
			float angleY = (int)XMConvertToDegrees(m_Angle.y) % 360;
			if (180 < targetAngleY -angleY)
				targetAngleY -= 360;
			else if (targetAngleY - angleY < -180)
				targetAngleY += 360;

			util::Vec2 targetAngle = util::Vec2(0, XMConvertToRadians(targetAngleY));

			/*
			util::Vec3 targetAngleVector = util::Vec3(targetAngle.x, targetAngle.y,0);
			isright = m_rightVec.dot(targetAngleVector) < 0;

			if (XMConvertToRadians(360) < abs(targetAngle.y - m_Angle.y))
			{
				targetAngle.y = 0 < targetAngle.y - m_Angle.y ? targetAngle.y + XMConvertToRadians(360) : targetAngle.y - XMConvertToRadians(360);
			}
			if(!isright)
				targetAngle.y -= XMConvertToRadians(360);*/
			float second = 0.1f;
			changeAngleLerp(targetAngle, second);
		}

		if (m_ShakeMove.isBegin()) {
			m_ShakeMove.updaet();
			m_pCamera.lock()->setLookOffset(m_ShakeMove.get(m_Entity.lock()->getTransform()));
		}

	}

	//J£
	//ρ]¬x
	//
	void CameraController::setParam(const std::vector<std::string>& param)
	{
		util::LuaBase lua(param[0]);

		m_Radius = m_DefaRadius = lua.getParam<float>("Radius");
		m_AngleSpeed = util::Vec2(
			lua.getParam<float>("UpSpeed"),
			lua.getParam<float>("AbgleSpeed")
		);

		//cρ]ΕεΖΕ¬
		m_ClampAngleX.x = lua.getParam<float>("MinYPos");
		m_ClampAngleX.y = lua.getParam<float>("MaxYPos");

		m_OffsetY = lua.getParam<float>("OffsetY");
	}

	void CameraController::moveAxisX()
	{
		auto vel = application::Input::rightVelocity();

		m_Angle.x -= vel.y *  XMConvertToRadians(m_AngleSpeed.x);
		m_Angle.x = util::clamp(m_Angle.x, XMConvertToRadians(m_ClampAngleX.x), XMConvertToRadians(m_ClampAngleX.y));
	}

	void CameraController::moveAxisY()
	{
		auto vel = application::Input::rightVelocity();

		m_Angle.y += -vel.x *  XMConvertToRadians(m_AngleSpeed.y);

		auto angleY = (int)XMConvertToDegrees(m_Angle.y) % 360;
		m_Angle.y = XMConvertToRadians(angleY);
	}

	void CameraController::lerpUpdate()
	{
		float lerpTime = m_lerpCount / m_lerpTime;
		if (1 < lerpTime)
			return;

		m_lerpCount++;

		lerpTime = util::clamp(lerpTime, 0.0f, 1.0f);
		float lerpLength = m_targetRadius - m_startRadius;
		m_Radius = util::bezierCurve(lerpTime, m_startRadius, m_startRadius + lerpLength * 2 / 3, m_targetRadius);
	}

	void CameraController::lerpAngleUpdate()
	{
		if (m_lerpAngleTimer->isEnd())
			return;

		m_lerpAngleTimer->update();
		float lerpRate = 1 - m_lerpAngleTimer->rate();

		util::Vec2 lerpLength = m_lerpTargetAngle - m_lerpStartAngle;
		m_Angle.x = util::bezierCurve(lerpRate, m_lerpStartAngle.x, m_lerpStartAngle.x + lerpLength.x * 2 / 3, m_lerpTargetAngle.x);
		m_Angle.y = util::bezierCurve(lerpRate, m_lerpStartAngle.y, m_lerpStartAngle.y + lerpLength.y * 2 / 3, m_lerpTargetAngle.y);

	}

	void CameraController::changeRadius(float radius)
	{
		m_Radius = m_targetRadius = radius;
	}

	void CameraController::changeCameraLerp(float targetRadius, float lerpSecond)
	{
		m_startRadius = m_Radius;
		m_targetRadius = targetRadius;
		m_lerpTime = lerpSecond * 30;
		m_lerpCount = 0;
	}

	void CameraController::changeAngleLerp(util::Vec2 targetAngle, float lerpSecond)
	{
		m_lerpStartAngle = m_Angle;
		m_lerpTargetAngle = targetAngle;
		m_lerpAngleTimer->init(lerpSecond * 30);
	}

	void CameraController::addRadius(float addRadius)
	{
		float minRadius = 100.0f;
		m_Radius = util::clamp(m_Radius + addRadius, minRadius, m_targetRadius);
	}

	void CameraController::changeView(CameraViewType viewType)
	{
		m_viewType = viewType;
	}

	void CameraController::changeView()
	{
		if (m_viewType == CameraViewType::SholderView)
			m_viewType = CameraViewType::ThirdPersonView;

		else if (m_viewType == CameraViewType::ThirdPersonView)
			m_viewType = CameraViewType::SholderView;
	}

	void CameraController::setPosition(util::Vec3 position)
	{
		auto myTrans = m_Entity.lock()->getTransform();
		myTrans->m_Position = position;
	}

	util::Vec3 CameraController::getRotation()
	{
		auto myTrans = m_Entity.lock()->getTransform();

		return myTrans->m_Rotation;
	}

	void CameraController::initDefaPos()
	{
		util::Vec3 result;

		auto lookTrans = m_pLookEntity.lock()->getTransform();
		util::Vec3 lookPos = result = lookTrans->m_Position;

		result.z -= m_Radius;

		auto myTrans = m_Entity.lock()->getTransform();
		myTrans->m_Position = result;

		m_defaPos = myTrans->m_Position - lookPos;
	}

	void CameraController::createNormalize()
	{
		m_rightVec = m_upVec.cross(m_defaPos.normalize()).normalize();
	}

	util::Vec3 CameraController::rotate()
	{
		util::Vec3 result;
		auto lookTrans = m_pLookEntity.lock()->getTransform();

		//QuaternionΙΟ·(ΆEρ])
		util::Quaternion q = XMQuaternionRotationAxis(m_upVec.toXMVector(), m_Angle.y);

		//Quaternion¬iΆEΖγΊΜρ]π¬j
		q = XMQuaternionMultiply(q.toXMVector(), XMQuaternionRotationAxis(m_rightVec.toXMVector(), m_Angle.x));

		result = util::rotation(q, m_defaPos);

		//RlΜ_
		result += lookTrans->m_Position;

		return  result;
	}

	void CameraController::hitWall()
	{
		auto lookTrans = m_pLookEntity.lock()->getTransform();
		auto myTrans = m_Entity.lock()->getTransform();

		m_hitMoveSpeed = (myTrans->m_Position - lookTrans->m_Position).length();

		auto test = m_world.lock()->rayCast(lookTrans->m_Position, myTrans->m_Position);

		if (test.hasHit() && test.getHitEntity()->getTag() == "Stage")
		{
			//myPos--->hitPos
			util::Vec3 toHitPosVector = util::convertUtVector3(test.m_hitPointWorld) - myTrans->m_Position;
			float length = toHitPosVector.length();
			//ilookPos-->myPosϋόͺ{j
			float sign = ((myTrans->m_Position - lookTrans->m_Position).dot(toHitPosVector) < 0) ? -1 : 1;
			m_hitMoveSpeed = length * sign;
		}

		addRadius(m_hitMoveSpeed / 30);

		m_hitMoveSpeed = (myTrans->m_Position - lookTrans->m_Position).length();
	}

	framework::WeakEntity CameraController::getLookEntity()
	{
		return m_pLookEntity;
	}

	std::weak_ptr<CameraComponent> CameraController::getCamera()
	{
		return m_pCamera;
	}
	util::Vec3 CameraController::getDeafaPos()
	{
		return m_defaPos;
	}
	float CameraController::getDefaRadius()
	{
		return m_DefaRadius;
	}
	float CameraController::getOffsetY()
	{
		return m_OffsetY;
	}
	util::Vec3 CameraController::getUpVector()
	{
		return m_upVec;
	}
	util::Vec3 CameraController::getRightVector()
	{
		return m_rightVec;
	}
	float CameraController::getLerpRate()
	{
		float lerpTime = m_lerpCount / m_lerpTime;
		lerpTime = util::clamp(lerpTime, 0.0f, 1.0f);
		return lerpTime;
	}
	const util::Vec2 & CameraController::getAgnle()
	{
		return m_Angle;
	}
	const float CameraController::angleRate()
	{
		return 1 - ((m_Angle.x / XMConvertToRadians(m_ClampAngleX.y)) * 0.5 + 0.5);
	}

	void CameraController::onShake(framework::WeakEntity& entity)
	{
		if (!m_ShakeMove.isBegin()) {
			m_ShakeMove.setBeginOffset(m_pCamera.lock()->getOffet());
		}

		//­ΖΜ£vZ
		auto&& vec = entity.lock()->getTransform()->m_Position - m_Entity.lock()->getTransform()->m_Position;
		auto length = vec.length();

		const short shakeMaxPower = 50;

		length = min(length, MAX_SHAKE);
		auto&& rate = 1.0f - (length / MAX_SHAKE);
		auto power = shakeMaxPower * rate;

		m_ShakeMove.begin(power, power * 0.3);
	}

	void CameraController::setShakeOffset(const util::Vec3 & offset)
	{
		m_ShakeMove.setBeginOffset(offset);
	}


}

