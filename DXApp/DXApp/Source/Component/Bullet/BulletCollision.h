#pragma once
#include<Framework.h>

namespace component {

	class BulletCollision : public framework::Component
	{
	public:
		BulletCollision();
		~BulletCollision();

		/**
		* @brief		衝突したときの処理
		* @param other	衝突した相手オブジェクト
		*/
		virtual void onCollisionEnter(const framework::HitData& other);

	private:


	};
}