#pragma once
#include<Framework.h>

namespace  framework {
	class Effect;
}

namespace component {

	/// <summary>
	/// 接続ゲームオブジェクトに斜方投射の挙動を付与するクラス
	/// </summary>
	class Bullet : public framework::UpdateComponent
	{
	public:
		Bullet();
		~Bullet();


		/**
		* @brief		初期化
		*/
		virtual void init()override;

		/**
		* @brief		更新
		*/
		virtual void update()override;

		virtual void setParam(const std::vector<std::string>& param);


		/// <summary>
		/// 斜方投射座標計算
		/// </summary>
		static util::Vec3 bulletTrajectory(float time,float vec,float angle, util::Mat4& rotate);

		virtual void onCollisionEnter(const framework::HitData& other)override;

		const short getPower()const;

	private:
		//!エフェクトハンドル
		std::weak_ptr<framework::Effect> m_pPoisonEffectHandle;
		util::Timer m_Timer;

		//!壁に当たってすぐ弾が消滅しないようにするためのタイマ
		util::Timer m_WallTimer;

		util::Vec3 m_InitPos;
		util::Vec3 m_LocalPos;

		short m_Power;

		float m_Time;
		float m_Vec;
		float m_Angle;
		float m_Speed;
	};
}