#include"LaunchingDevice.h"
#include<Source\Component\UI\GUIComponent.h>
#include<Framework.h>
#include<Source\Component\Device\Renderer\LineRenderComponent.h>
#include<Source\Application\Device\Input.h>
#include<Source\Component\Player\PlayerComponent.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Component\Device\CameraController.h>
#include<Source\Component\Physics\PhysicsWorld.h>

UsingNamespace;

namespace component {

	LaunchingDevice::LaunchingDevice()
	{
		m_CallOrder = 1;
	}

	LaunchingDevice::~LaunchingDevice()
	{
	}

	void LaunchingDevice::init()
	{
		m_Entity.lock()->addEvent("Shooting", [&]() {
			framework::ResourceManager::getInstance()->setVolume("Shooting", -2000);
			framework::ResourceManager::getInstance()->setDoppler("Shooting", false);
			framework::ResourceManager::getInstance()->playSound("Shooting");
		});
		m_Entity.lock()->addEvent("GunClick", [&]() {
			framework::ResourceManager::getInstance()->playSound("GunClick2");
			framework::ResourceManager::getInstance()->setVolume("GunClick2", -1000);
			framework::ResourceManager::getInstance()->setDoppler("GunClick2", 0);
		});
		m_BulletDesc.vec = m_pUI.lock()->getSliderPointer("Vec");
		m_BulletDesc.speed = m_pUI.lock()->getSlider("BulletSpeed");
		m_BulletDesc.angle = m_pUI.lock()->getSliderPointer("Angle");
		m_BulletDesc.chargeTime = m_pUI.lock()->getSlider("MaxChargeTime");
		m_MaxChageCutPoisonTank = m_pUI.lock()->getSlider("MaxChargeCuntPoisonTank");
		m_MinChageCutPoisonTank = m_pUI.lock()->getSlider("MinChargeCuntPoisonTank");
		m_TankRecovery = m_pUI.lock()->getSlider("TankRecovery");


		m_pBulletTimer = std::make_unique<util::Timer>(m_BulletDesc.chargeTime);
		std::vector<LineVertexLayout> vertex;

		for (size_t i = 0; i < 20; i++)
		{
			vertex.emplace_back(LineVertexLayout(util::Vec3(0, 0, i), util::Vec3(0, 1, 0), util::Vec3(1, 0, 0)));
		}

		m_pLineRenderer.lock()->addPoint(vertex);
		m_pLineRenderer.lock()->setLineWidth(10);
		m_pLineRenderer.lock()->setScale(util::Vec3(1, 1, 1));
		m_pLineRenderer.lock()->setOffset(util::Vec3(10, 0, 0));
		m_pLineRenderer.lock()->setColor(util::Vec3(0, 0, 1));
		//for (size_t i = 0; i < 10; i++)
		//{
		//	m_pLineRenderer.lock()->setControllPoint(i, util::Vec3(0, i, 0));
		//}

		m_pLineRenderer.lock()->deActive();

		m_pPlayerComponent = m_Entity.lock()->getComponent<PlayerComponent>();

		m_pCameraController = CameraComponent::getMainCamera().lock()->getGameObj().lock()->getComponent<CameraController>();

		m_pWorld = Entity::findGameObj("PhysicsWorld").lock()->getComponent<PhysicsWorld>();

		m_IsChargeTrriger = false;
		m_IsChargeMax = false;
		m_LastChargeNum = 0.0f;
	}

	void LaunchingDevice::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath });
		(*componentInitalizer).emplace_back(m_pUI);

		m_pLineRenderer = m_Entity.lock()->addComponent<LineRenderComponent>(componentInitalizer, { "50" });
		(*componentInitalizer).emplace_back(m_pLineRenderer);
	}

	void LaunchingDevice::update()
	{

#ifdef _MDEBUG
		m_BulletDesc.speed = m_pUI.lock()->getSlider("BulletSpeed");
		m_MaxChageCutPoisonTank = m_pUI.lock()->getSlider("MaxChargeCuntPoisonTank");
		m_MinChageCutPoisonTank = m_pUI.lock()->getSlider("MinChargeCuntPoisonTank");
		m_TankRecovery = m_pUI.lock()->getSlider("TankRecovery");

		//発射ボタンが押されていない間のみ
		if (!application::Input::isCharge()) {
			m_BulletDesc.chargeTime = m_pUI.lock()->getSlider("MaxChargeTime");
		}

		//if (application::Input::isShot()) {
		//	m_pBulletTimer = std::make_unique<util::Timer>(m_BulletDesc.chargeTime);
		//}
#endif
		m_pPlayerComponent.lock()->getPlayerData().lock()->pPoisonTank->add(m_TankRecovery);

		if (m_pCameraController.lock()->m_viewType == CameraViewType::ThirdPersonView)return;

		auto trans = m_Entity.lock()->getTransform();
		util::Mat4 indentity = XMMatrixIdentity();

		util::Vec3 controllPoint[20];

		for (size_t i = 0; i < 20; i++)
		{
			//原点から線を引くとプレイヤーモデルを貫通するので少し遅れて線を出す
			m_pLineRenderer.lock()->setControllPoint(i, Bullet::bulletTrajectory(i + 0.5, *(m_BulletDesc.vec), *(m_BulletDesc.angle), indentity));
	
		}

		auto&& pos = m_Entity.lock()->getTransform()->m_Position;

		for (size_t i = 0; i < 20; i++)
		{	
			controllPoint[i] = Bullet::bulletTrajectory(i, *(m_BulletDesc.vec), *(m_BulletDesc.angle), trans->toRotateMatrix()) + pos;
		}

		std::vector<int> rayPoint;

		for (size_t i = 0; i < 38; i++)
		{
			rayPoint.emplace_back(i);
			rayPoint.emplace_back(i);
		}

		//先端だけ削除
		rayPoint.erase(rayPoint.begin());
		rayPoint.erase(std::prev(rayPoint.end(), 1));

		auto centerOffset = m_Entity.lock()->getTransform()->left() * 10;

		m_pLineRenderer.lock()->setColor(util::Vec3(0, 0, 1));
		for (short i = 0, end = rayPoint.size() / 2; i < end; i+= 2)
		{
			auto&& beginPoint = controllPoint[rayPoint[i]] - centerOffset;
			auto&& endPoint = controllPoint[rayPoint[i + 1]] - centerOffset;

			auto hit = m_pWorld.lock()->rayCast(beginPoint, endPoint);
			if (hit.hasHit()) {
				if (hit.getHitEntity()->getTag() == "Enemy") {
					m_pLineRenderer.lock()->setColor(util::Vec3(1, 0, 0));
					break;
				}
			}
		}

	}

	void LaunchingDevice::setParam(const std::vector<std::string>& param)
	{
		m_LuaPath = param[0];
	}

	float LaunchingDevice::getChageRate()
	{
		return 1.0f - m_LastChargeNum;
	}

	void LaunchingDevice::setAngle(float angle)
	{
		*(m_BulletDesc.angle) = angle;
	}

	void LaunchingDevice::setVec(float vec)
	{
		*(m_BulletDesc.vec) = vec;
	}

	void LaunchingDevice::spown()
	{

		auto&& cutNum = util::lerp<float>(1.0f - m_pBulletTimer->rate(), m_MinChageCutPoisonTank, m_MaxChageCutPoisonTank);
		auto playerData = m_pPlayerComponent.lock()->getPlayerData();

		if (!playerData.lock()->pPoisonTank->isLow(cutNum)) {

			playerData.lock()->pPoisonTank->cutBack(cutNum);
			m_Entity.lock()->onEvent("Shooting");
		}
		else {

			//TODO 弾切れSEを鳴らす
			m_Entity.lock()->onEvent("GunClick");
			return;
		}
		auto trans = m_Entity.lock()->getTransform();

		//TODO モデルのサイズがあってないので新しく生成する　また調整する必要がある
		util::Transform bulletTrans = *trans;
		bulletTrans.m_Scale = util::Vec3(50, 50, 50);
		auto bullet = Entity::createEntity("Bullet", "Bullet", bulletTrans);
		std::vector<std::weak_ptr<Component>> damy;
		auto bulletComponent = bullet.lock()->addComponent<Bullet>(&damy, { std::to_string(*(m_BulletDesc.vec)),std::to_string(m_BulletDesc.speed),std::to_string(*(m_BulletDesc.angle)),std::to_string(getChageRate())});
		auto rigidBody = bullet.lock()->addComponent < BulletRigidBody >(&damy, { "Resource/Script/Lua/UI/RigidBody.lua" ,"Resource/ByteData/UIPrefab/BulletRigidBody.pfb" });
		auto collider = bullet.lock()->addComponent<BulletSphereCollider>(&damy, { "Resource/Script/Lua/UI/SphereCollider.lua","Resource/ByteData/UIPrefab/BulletCollider.pfb" });
		bulletComponent.lock()->init();

		//順不同
		collider.lock()->init();
		rigidBody.lock()->init();
	}

	void LaunchingDevice::initTimer()
	{
		m_pBulletTimer->init();
	}

	bool LaunchingDevice::isEndTimer()
	{
		return m_pBulletTimer->isEnd();
	}

	void LaunchingDevice::updateTimer()
	{
		m_pBulletTimer->update();
		m_LastChargeNum = m_pBulletTimer->rate();
	}

	void LaunchingDevice::activeLine()
	{
		m_pLineRenderer.lock()->active();
	}

	void LaunchingDevice::deActiveLine()
	{
		m_pLineRenderer.lock()->deActive();
	}

}