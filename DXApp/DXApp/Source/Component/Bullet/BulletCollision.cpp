#include "BulletCollision.h"

component::BulletCollision::BulletCollision()
{
}

component::BulletCollision::~BulletCollision()
{
}

void component::BulletCollision::onCollisionEnter(const framework::HitData & other)
{
	if (other.other.lock()->getTag() != "Stage")return;
	if (other.other.lock()->getTag() == "Player")return;

	destory(m_Entity.lock()->getParent());

	destory(m_Entity);
}
