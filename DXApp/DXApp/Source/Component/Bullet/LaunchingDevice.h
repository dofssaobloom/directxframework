#pragma once
#include<Framework.h>
#include"Bullet.h"

namespace framework {
	class Effect;
}


namespace component {
	class SpriteRenderComponent;
	class LineRenderComponent;
	class PlayerComponent;
	class GUIComponent;
	class CameraController;
	class PhysicsWorld;

	struct BulletDesc
	{
		float *vec, *angle, speed,chargeTime;
	};

	class LaunchingDevice : public framework::UpdateComponent
	{
	public:
		LaunchingDevice();
		~LaunchingDevice();

		virtual void init()override;

		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		virtual void setParam(const std::vector<std::string>& param);

		// UpdateComponent を介して継承されました
		virtual void update() override;


		/// <summary>
		///　溜め撃ち強度 0 ~1の間
		/// </summary>
		/// <returns></returns>
		float getChageRate();

		/// <summary>
		/// 投射アングルセット
		/// </summary>
		/// <param name="angle"></param>
		void setAngle(float angle);

		/// <summary>
		/// 射法初速セット
		/// </summary>
		/// <param name="vec"></param>
		void setVec(float vec);

		/// <summary>
		/// Bullet生成
		/// </summary>
		void spown();

		void initTimer();

		/// <summary>
		/// /タイマーが終了しているか
		/// </summary>
		bool isEndTimer();

		void updateTimer();

		/// <summary>
		/// ラインのアクティブ
		/// </summary>
		void activeLine();

		/// <summary>
		/// ラインの非アクティブ
		/// </summary>
		void deActiveLine();

	private:


	private:
		std::weak_ptr<GUIComponent> m_pUI;
		std::string m_LuaPath;

		//!推している時間を計測するタイマー
		std::unique_ptr<util::Timer> m_pBulletTimer;
		std::weak_ptr<LineRenderComponent> m_pLineRenderer;
		std::weak_ptr<PlayerComponent> m_pPlayerComponent;
		std::weak_ptr<framework::Effect> m_pChageEffect;
		std::weak_ptr<CameraController> m_pCameraController;
		std::weak_ptr<PhysicsWorld> m_pWorld;

		bool m_IsChargeTrriger;

		bool m_IsChargeMax;

		//毒タンクが減る量
		float m_MaxChageCutPoisonTank;
		float m_MinChageCutPoisonTank;
		//タンク回復量
		float m_TankRecovery;

		//!リセット
		float m_LastChargeNum;

		BulletDesc m_BulletDesc;

		bool one;
	};

	

}
