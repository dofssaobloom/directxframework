#pragma once
#include<Framework.h>

namespace component {
	class GUIComponent;

	/// <summary>
	/// 爆発よう当たり判定を出すクラス
	/// </summary>
	class PoisonExplosion : public framework::UpdateComponent
	{
	public:
		PoisonExplosion();
		~PoisonExplosion();

		/**
		* @brief		初期化
		*/
		virtual void init()override;

		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		virtual void setParam(const std::vector<std::string>& param);
		/**
		* @brief		更新
		*/
		virtual void update()override;
		
		/// <summary>
		/// あたり安定有効
		/// </summary>
		void activeCollision();

		/// <summary>
		/// 当たり判定向こう
		/// </summary>
		void deActiveCollision();

		void destroyCollision();

	private:
		//!毒範囲用判定を持ったオブジェクト
		framework::WeakEntity m_pPoisonCollisioinObj;
		std::weak_ptr<BulletRigidBody> m_pPoisonRigid;
		util::Transform* m_pTrans;
		std::weak_ptr<GUIComponent> m_pUI;
		std::string m_LuaPath;

	};
}