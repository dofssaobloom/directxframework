#include "EnemySpowner.h"
#include "EnemyComponent.h"
#include<Source\Component\ComponentInclude.h>
#include<Source\Component\Scene\GameMain\RulesComponent.h>
#include"PoisonExplosion.h"
#include<Source\Component\Player\SearchComponent.h>
#include<Source\Component\Player\AttackComponent.h>
#include<Source\Component\Scene\GameMain\GameTimerCompornent.h>
#include<Source\Actor\Enemy\EEnemyType.h>
#include<Source\Component\Effect\PointLightComponent.h>
#include<Source\Component\UI\GUIComponent.h>

UsingNamespace;
namespace component {
	EnemySpowner::EnemySpowner() :m_BornUpTimer(30 * 20)
	{
		m_CallOrder = 2;
	}

	EnemySpowner::~EnemySpowner()
	{
	}

	void EnemySpowner::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		//GUIの追加
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer,{ "Resource/Script/Lua/UI/EnemySpownerUI.lua" });
		(*componentInitalizer).emplace_back(m_pUI);
	}

	void EnemySpowner::init()
	{
		//UIの設定
		m_BornMax = m_pUI.lock()->getSlider("BornMax");
		m_MaxInterval = m_pUI.lock()->getSlider("MaxInterval") * 30;
		m_MinInterval = m_pUI.lock()->getSlider("MinInterval") * 30;

		auto trans = m_Entity.lock()->getTransform();
		trans->m_Position.x = m_pUI.lock()->getSlider("PositionX");
		trans->m_Position.y = m_pUI.lock()->getSlider("PositionY");
		trans->m_Position.z = m_pUI.lock()->getSlider("PositionZ");

		m_EnemyTypeA = m_pUI.lock()->getSlider("EnemyTypeA");
		m_EnemyTypeB = m_pUI.lock()->getSlider("EnemyTypeB");
		m_EnemyTypeC = m_pUI.lock()->getSlider("EnemyTypeC");


		m_pGameTimer = Entity::findGameObj("GameTimer").lock()->getComponent<GameTimerCompornent>();

		m_pTimer = std::make_unique<util::Timer>(util::lerp<float>(m_pGameTimer.lock()->getTimeRate(), m_MaxInterval, m_MinInterval));

		//listの最大値をUIのm_BornMaxに設定
		m_EnemyList = std::list<std::weak_ptr<framework::Entity>>();


		m_BornUpTimer.init();

		auto defaultEnemyNum = m_Randam.next(2,3);

		for (size_t i = 0; i < defaultEnemyNum; i++)
		{
			spown();
		}

	}

	void EnemySpowner::setParam(const std::vector<std::string>& param)
	{
	}

	void EnemySpowner::update()
	{
		//GUIの値取得
#ifdef _MDEBUG
		m_BornMax = m_pUI.lock()->getSlider("BornMax");
		m_MaxInterval = m_pUI.lock()->getSlider("MaxInterval") * 30;
		m_MinInterval = m_pUI.lock()->getSlider("MinInterval") * 30;

		auto trans = m_Entity.lock()->getTransform();
		trans->m_Position.x= m_pUI.lock()->getSlider("PositionX");
		trans->m_Position.y = m_pUI.lock()->getSlider("PositionY");
		trans->m_Position.z = m_pUI.lock()->getSlider("PositionZ");

		m_EnemyTypeA = m_pUI.lock()->getSlider("EnemyTypeA");
		m_EnemyTypeB = m_pUI.lock()->getSlider("EnemyTypeB");
		m_EnemyTypeC = m_pUI.lock()->getSlider("EnemyTypeC");

#endif
		EnemyListElementDelete();


		//生成数マックスならそれ以上生成しない。
		if (isEnemyMax())return;

		m_pTimer->update();
		//もしタイマーが0なら生成
		if (m_pTimer->isEnd())
		{
			spown();
			m_pTimer = std::make_unique < util::Timer >(util::lerp<float>(m_pGameTimer.lock()->getTimeRate(), m_MaxInterval, m_MinInterval));
		}
	}

	void EnemySpowner::spown()
	{
		//スケールの変更
		auto newTrans = *(m_Entity.lock()->getTransform());
		newTrans.scaling(50);

		//Entityの動的作成 newTrans
		auto enemy = Entity::createEntity("Enemy", "Enemy", newTrans);

		//listに追加
		m_EnemyList.push_back(enemy);
		std::vector<std::weak_ptr<Component>> damy;
		auto enemyComponent = m_EnemyList.back().lock()->addComponent<EnemyComponent>(&damy, { "Resource/Script/Lua/UI/EnemyUI.lua" });
		auto enemyTypeRandam = m_Randam.next(0, 100);

		std::string enemyType;
		std::string rigidBodyType;
		util::Vec4 color;

		EnemyType eEnemyType = EnemyType::Black;
		rigidBodyType = "Resource/ByteData/UIPrefab/MashroomRigidBodyA.pfb";
		//デフォルト値(25%,25%,50%)
		if (enemyTypeRandam % 100 <= m_EnemyTypeA)
		{
			eEnemyType = EnemyType::Black;
			enemyType = "MushroomTypeA";
			color = util::Vec4(0.9,0.21,0,1);
			//rigidBodyType = "Resource/ByteData/UIPrefab/MashroomRigidBodyA.pfb";
		}
		else if (enemyTypeRandam % 100 >= m_EnemyTypeA + 1 && enemyTypeRandam % 100 <= m_EnemyTypeA + m_EnemyTypeB)
		{
			eEnemyType = EnemyType::Green;
			enemyType = "MushroomTypeB";
			color = util::Vec4(0.3, 0.9, 0.3, 1);
			//rigidBodyType = "Resource/ByteData/UIPrefab/MashroomRigidBodyB.pfb";
		}
		else if (enemyTypeRandam % 100 >= m_EnemyTypeB + 1 && enemyTypeRandam % 100 <= m_EnemyTypeA + m_EnemyTypeB + m_EnemyTypeC)
		{
			eEnemyType = EnemyType::Red;
			enemyType = "MushroomTypeC";
			color = util::Vec4(0.7, 0.3, 0, 1);
			//rigidBodyType = "Resource/ByteData/UIPrefab/MashroomRigidBodyC.pfb";
		}

		auto render = enemy.lock()->addComponent<RenderClientComponent>(&damy, { enemyType });
		auto rigidBody = enemy.lock()->addComponent<BulletRigidBody>(&damy, { "Resource/Script/Lua/UI/RigidBody.lua" ,rigidBodyType });
		auto collider = enemy.lock()->addComponent<BulletSphereCollider>(&damy, { "Resource/Script/Lua/UI/SphereCollider.lua","Resource/ByteData/UIPrefab/MashroomColider.pfb" });
		auto animator = enemy.lock()->addComponent<AnimatorComponent>(&damy,{ "Resource/Motion/MushroomMotion.csv","Resource/Event/AnimationEvent/MushroomEvent.csv" });
		std::vector<std::weak_ptr<Component>> initContainer;
		auto attack = enemy.lock()->addComponent<AttackComponent>(&initContainer, { "Resource/Script/Lua/UI/AttackUI.lua" ,"Resource/ByteData/UIPrefab/EnemyAttackCollision.pfb" ,"Resource/ByteData/UIPrefab/EnemyAttack.pfb" });
		auto pointLight = enemy.lock()->addComponent<PointLightComponent>(&initContainer);
		auto lightGUI = enemy.lock()->addComponent<GUIComponent>(&initContainer, { "Resource/Script/Lua/UI/PointLightUI.lua" ,"Resource/ByteData/UIPrefab/EnemyPointLight.pfb" });

		//auto serchComponet = enemy.lock()->addComponent<SearchComponent>(&damy, { "Resource/Script/Lua/UI/SeachUI.lua","Stage","Player","Camera"});


		//初期化
		collider.lock()->init();
		rigidBody.lock()->init();
		render.lock()->init();
		enemyComponent.lock()->init();
		enemyComponent.lock()->getEnemyData()->enemyType = eEnemyType;
		animator.lock()->init();
		lightGUI.lock()->init();
		pointLight.lock()->init();
		pointLight.lock()->setColor(color);

		std::sort(initContainer.begin(), initContainer.end(), [](std::weak_ptr<Component>&left, std::weak_ptr<Component>&right) {
			return left.lock()->m_CallOrder < right.lock()->m_CallOrder;
		});

		for (auto& component : initContainer)
		{
			component.lock()->init();
		}
		attack.lock()->init();
		attack.lock()->setTime(2);
		attack.lock()->setFrontFlag(false);
		//serchComponet.lock()->init();
		//serchComponet.lock()->setRadius(100);
	}

	void EnemySpowner::EnemyListElementDelete()
	{
		//参照切れを起こしたオブジェクトを毎フレームチェックしてデリート
		auto deleteElement = std::remove_if(m_EnemyList.begin(), m_EnemyList.end(), [](std::weak_ptr<framework::Entity>& enemy) {
			return enemy.expired();
		});

		m_EnemyList.erase(deleteElement, m_EnemyList.end());
	}

	bool EnemySpowner::isEnemyMax()
	{
		//リストのサイズが0以上なら最大値以上になるまでtrue
		if (m_EnemyList.size() >= 0) {
			return m_EnemyList.size() >= m_BornMax;
		}
		return false;
	}

	bool EnemySpowner::EnemyTypeRandam(int randam, int designation)
	{
		return randam % 100 <= designation;
	}
}