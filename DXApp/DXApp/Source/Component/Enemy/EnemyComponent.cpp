#include "EnemyComponent.h"
#include<Source\Actor\Enemy\EnemyIdleState.h>
#include<Source\Actor\Enemy\EnemyMoveState.h>
#include<Source\Component\Player\PlayerComponent.h>
#include<Framework.h>
#include<Source\Actor\Enemy\EnemySwellState.h>
#include"PoisonExplosion.h"
#include<Source\Component\Bullet\LaunchingDevice.h>

#include<Source\Component\Scene\GameMain\EnemyRulesComponent.h>
#include<Source\Component\Player\AttackComponent.h>
#include<Source\Actor\Enemy\EnemyStanState.h>
#include<Source\Component\Player\SearchComponent.h>
#include<Source\Component\Physics\PhysicsWorld.h>
#include<Source\Component\Player\AttackComponent.h>
#include<Source\Actor\Enemy\EnemyDamageState.h>
#include<Source\Component\Device\CameraController.h>
#include<Source\Component\Effect\ZoomBlur.h>
#include<Source\Actor\Enemy\EnemyAttackState.h>
#include<Source\Component\Effect\PointLightComponent.h>

UsingNamespace;

namespace component {

	EnemyComponent::EnemyComponent()
		:m_MinSwelTime(30 * 3), m_MaxSwelTime(30 * 15), m_BossHitTimer(90)
	{
		m_CallOrder = 2;
	}

	EnemyComponent::~EnemyComponent()
	{
	}

	void EnemyComponent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { "Resource/Script/Lua/UI/EnemyUI.lua" });
		auto poison = m_Entity.lock()->addComponent<PoisonExplosion>(componentInitalizer, { "Resource/Script/Lua/UI/PosionExplosion.lua" });

		(*componentInitalizer).emplace_back(m_pUI);
		(*componentInitalizer).emplace_back(poison);
	}

	void EnemyComponent::init()
	{
		////自分のコリジョン有効化
		//m_Entity.lock()->addEvent("OnCollision", [&]() {
		//	m_EnemyData->pAttack.lock()->onCollision();
		//});

		//自分のコリジョン無効化
		m_Entity.lock()->addEvent("OffCollision", [&]() {
			m_EnemyData->pAttack.lock()->offCollision();
		});

		m_Entity.lock()->addEvent("OnAttackEffect", [&]() {
			if (getEnemyData()->enemyType == EnemyType::Green)
				return;
			auto effectTrans = *m_Entity.lock()->getTransform();
			effectTrans.scaling(7);
			effectTrans.m_Position -= effectTrans.front() * 130;
			effectTrans.m_Position.y += 50;
			effectTrans.m_Rotation.y += 180;
			auto effect = EffectManager::getInstance()->generate("Sonic", effectTrans);
			effect.lock()->setSpeed(2.0f);
		});

		m_Entity.lock()->addEvent("OnAttackHint", [&]() {
			//TODO 攻撃ヒントエフェクトを入れる
		});

		m_Entity.lock()->addEvent("EnemyPoisn", [&]() {
			ResourceManager::getInstance()->playSound("EnemyPoisn");
			ResourceManager::getInstance()->setVolume("EnemyPoisn", 0);
			ResourceManager::getInstance()->setDoppler("EnemyPoisn", 1);
			ResourceManager::getInstance()->setDopplerVelocity("EnemyPoisn", util::Vec3(0, 0, 1));
			ResourceManager::getInstance()->setPosition("EnemyPoisn", *m_Entity.lock()->getTransform());
			ResourceManager::getInstance()->setDistance("EnemyPoisn", 4.5f, 40.0f);
		});

		m_Entity.lock()->addEvent("EnemyDead", [&]() {
			ResourceManager::getInstance()->playSound("EnemyDead");
			ResourceManager::getInstance()->setVolume("EnemyDead", 0);
			ResourceManager::getInstance()->setDoppler("EnemyDead", 1);
			ResourceManager::getInstance()->setDopplerVelocity("EnemyDead", util::Vec3(0, 0, 1));
			ResourceManager::getInstance()->setPosition("EnemyDead", *m_Entity.lock()->getTransform());
			ResourceManager::getInstance()->setDistance("EnemyDead", 4.5f, 40.0f);
		});

		m_Entity.lock()->addEvent("EnemyDamage", [&]() {
			ResourceManager::getInstance()->playSound("EnemyDamage");
			ResourceManager::getInstance()->setVolume("EnemyDamage", 0);
			ResourceManager::getInstance()->setDoppler("EnemyDamage", 1);
			ResourceManager::getInstance()->setDopplerVelocity("EnemyDamage", util::Vec3(0, 0, 1));
			ResourceManager::getInstance()->setPosition("EnemyDamage", *m_Entity.lock()->getTransform());
			ResourceManager::getInstance()->setDistance("EnemyDamage", 4.5f, 40.0f);
		});

		m_Entity.lock()->addEvent("EnemyBomb", [&]() {
			ResourceManager::getInstance()->playSound("Bomb2");
			ResourceManager::getInstance()->setVolume("Bomb2", 0);
			ResourceManager::getInstance()->setDoppler("Bomb2", 1);
			ResourceManager::getInstance()->setDopplerVelocity("Bomb2", util::Vec3(0, 0, 1));
			ResourceManager::getInstance()->setPosition("Bomb2", *m_Entity.lock()->getTransform());
			ResourceManager::getInstance()->setDistance("Bomb2", 4.0f, 80.0f);
		});

		//最初はアイドル状態
		m_States = std::make_shared<EnemyIdleState>();
		m_EnemyData = std::make_unique<EnemyData>();

		m_IsBulletDead = false;
		m_EnemyRules = Entity::findGameObj("EnemyRules").lock()->getComponent<EnemyRulesComponent>();

		m_EnemyData->pPoison = m_Entity.lock()->getComponent<PoisonExplosion>();
		//テスト
		m_EnemyData->maxScale = 80.0f;
		m_EnemyData->swellTime = 30 * 20;
		m_EnemyData->isDead = false;
		m_EnemyData->searchRange = m_pUI.lock()->getSlider("SearchRange");
		m_EnemyData->poisonExplosionSize = 60;
		m_EnemyData->pSphereCollider = m_Entity.lock()->getComponent<BulletSphereCollider>();
		m_EnemyData->pRender = m_Entity.lock()->getComponent<RenderClientComponent>();
		m_EnemyData->pHP = std::make_unique<Tank<short>>(100);

		float scale;
		m_EnemyData->pSphereCollider.lock()->getScale(&scale);
		m_EnemyData->colliderSize = std::make_pair(scale, 120.0f);

		auto offset = m_EnemyData->pSphereCollider.lock()->getOffset();
		m_EnemyData->colliderOffset = std::make_pair(offset.y, -120.0f);

		m_pRigidBody = m_Entity.lock()->getComponent<BulletRigidBody>();
		m_EnemyData->pPlayer = Entity::findGameObj("Player1");

		m_pPlayerAnimator = m_EnemyData->pPlayer.lock()->getComponent<AnimatorComponent>();
		m_pPlayerAttack = m_EnemyData->pPlayer.lock()->getComponent<AttackComponent>();
		m_EnemyData->pWorld = Entity::findGameObj("PhysicsWorld").lock()->getComponent<PhysicsWorld>();
		m_EnemyData->pAttack = m_Entity.lock()->getComponent<AttackComponent>();

		m_EnemyData->pRecastTimer = std::make_unique<util::Timer>(50);

		m_EnemyData->pCameraController = CameraComponent::getMainCamera().lock()->getGameObj().lock()->getComponent<CameraController>();

		m_EnemyData->pBluer = CameraComponent::getMainCamera().lock()->getGameObj().lock()->getComponent<ZoomBlur>();

		m_IsBossHit = false;

		m_pPointLight = m_Entity.lock()->getComponent<PointLightComponent>();
		//	zoomBlur.lock()->onBlurStart(m_Entity.lock()->getTransform());
	}

	void EnemyComponent::poisnSE()
	{
		m_Entity.lock()->onEvent("EnemyPoisn");
	}

	void EnemyComponent::damageSE()
	{
		m_Entity.lock()->onEvent("EnemyDamage");
	}

	void EnemyComponent::deadSE()
	{
		m_Entity.lock()->onEvent("EnemyDead");
	}

	void EnemyComponent::bombSE()
	{
		m_Entity.lock()->onEvent("EnemyBomb");
	}

	void EnemyComponent::setParam(const std::vector<std::string>& param)
	{
	}

	EnemyData * EnemyComponent::getEnemyData()
	{
		return m_EnemyData.get();
	}

	std::weak_ptr<BulletRigidBody> EnemyComponent::getRigidBody()
	{
		return m_pRigidBody;
	}

	void EnemyComponent::addScore()
	{
		m_EnemyRules.lock()->addScore();
	}

	void EnemyComponent::addTimer()
	{
		m_EnemyRules.lock()->addTimer();
	}

	std::weak_ptr<AttackComponent> EnemyComponent::getPlayerAttack()
	{
		return m_pPlayerAttack;
	}

	void EnemyComponent::bossHit()
	{
		m_IsBossHit = true;
	}

	void EnemyComponent::pauseDeActive()
	{
		m_Entity.lock()->getComponent<AnimatorComponent>().lock()->onStop();
	}

	void EnemyComponent::pauseActive()
	{
		m_Entity.lock()->getComponent<AnimatorComponent>().lock()->onStart();
	}

	void EnemyComponent::setColor(const util::Vec4 & color)
	{
		m_pPointLight.lock()->setColor(color);
	}

	void EnemyComponent::setAtten(const util::Vec3 & atten)
	{
		m_pPointLight.lock()->setAtten(atten);
	}

	util::Vec3 EnemyComponent::hitControl(const util::Vec3& front)
	{
		util::Mat4 mat;
		util::createMatrixFromFront(front.normalize(), &mat);

		auto leftInput = application::Input::leftVelocity();
		util::Vec3 vec(leftInput.x, 0, leftInput.y);
		vec = XMVector3Transform(vec.toXMVector(), mat.toXMMatrix());
		return (vec + front).normalize();
	}

	void EnemyComponent::hitEnemy(const framework::HitData & other)
	{
		if (other.other.lock()->getTag() != "Enemy") return;
		if (!m_EnemyData->isDead)return;

		util::Vec3 velocity;
		m_pRigidBody.lock()->getLinearVelocity(velocity);

		auto velLength = velocity.length();

		//移動量があまりにも低い場合は起爆しない
		if (velLength <= 110) {
			return;
		}

		if (other.other.lock()->getName() != "Golem") {

			auto otherEnemy = other.other.lock()->getComponent<EnemyComponent>();
			//!自分と相手両方毒状態だったら
			if (!otherEnemy.lock()->getEnemyData()->isDead)return;

			otherEnemy.lock()->getEnemyData()->isContactExplosion = true;
		}

		auto copyTrans = *m_Entity.lock()->getTransform();
		copyTrans.scaling(50);
		copyTrans.m_Position = other.hitPosA;
		auto effect = EffectManager::getInstance()->generate("Impact", copyTrans);
		effect.lock()->setSpeed(2);
		m_EnemyData->isContactExplosion = true;
	}

	void EnemyComponent::hitPlayer(const framework::HitData & other)
	{

		if (other.other.lock()->getTag() != "Player") return;
		auto dir = m_Entity.lock()->getTransform()->m_Position - other.other.lock()->getTransform()->m_Position;
		dir = dir.normalize();
		m_pRigidBody.lock()->addForce(dir * 10);//TODO あとで強さを変えられるようにする

	}

	void EnemyComponent::hitPoisonCollision(const framework::HitData & other)
	{
		if (other.other.lock()->getTag() != "PoisonCollision") return;
		if (m_EnemyData->isDead) {
			m_EnemyData->isContactExplosion = true;
			return;
		}
		//回転のフリーズを解除

		auto dir = m_Entity.lock()->getTransform()->m_Position - other.other.lock()->getTransform()->m_Position;
		dir = dir.normalize();
		//m_pRigidBody.lock()->addForce(dir * 4000);//TODO あとで強さを変えられるようにする
		std::shared_ptr<EnemyStanState> stanState = std::dynamic_pointer_cast<EnemyStanState>(m_States);
		if (nullptr != stanState) {
			stanState->deleteStanEffect();
		}
		std::shared_ptr<EnemyAttackState> attackState = std::dynamic_pointer_cast<EnemyAttackState>(m_States);
		if (nullptr != attackState) {
			attackState->deleteEffect();
		}

		//拡散のときのみタイマー増加
		addTimer();
		m_States = std::make_shared<EnemySwellState>(m_EnemyData->swellTime, true, dir);

	}

	void EnemyComponent::hitBullet(const framework::HitData & other)
	{
		if (other.other.lock()->getTag() != "Bullet") return;
		if (m_EnemyData->isDead) {
			m_EnemyData->isContactExplosion = true;
			return;
		}
		m_IsBulletDead = true;

		auto copyTrans = *m_Entity.lock()->getTransform();
		copyTrans.m_Position = other.hitPosA;
		copyTrans.scaling(30);
		EffectManager::getInstance()->generate("PoisonHit", copyTrans);

		std::shared_ptr<EnemyStanState> stanState = std::dynamic_pointer_cast<EnemyStanState>(m_States);
		if (nullptr != stanState) {
			m_EnemyData->pHP->cutBack(100);
		}
		else {
			auto bullet = other.other.lock()->getComponent<Bullet>();

			auto rate = bullet.lock()->getPower();

			m_EnemyData->pHP->cutBack(util::lerp<float>(rate, 35, 100));

			m_EnemyData->pRender.lock()->setTexLerp(1.0f - m_EnemyData->pHP->rate());
		}

	}

	void EnemyComponent::hitAttackCollision(const framework::HitData & other)
	{
		if (other.other.lock()->getName() != m_pPlayerAttack.lock()->getCollisionName()) return;
		auto otherTrans = m_Entity.lock()->getTransform();

		auto copy = *otherTrans;

		copy.scaling(50);
		copy.m_Position += util::Vec3(0, 100, 0);

		framework::EffectManager::getInstance()->generate("Hit", copy);
		damageSE();
		m_pPlayerAnimator.lock()->onStopTimer(5);
		if (m_EnemyData->isDead) {
			auto dir = m_Entity.lock()->getTransform()->m_Position - other.other.lock()->getTransform()->m_Position;
			//auto dir = other.other.lock()->getTransform()->front();
			dir = dir.normalize();
			dir = hitControl(dir);

			std::shared_ptr<EnemySwellState> swellState = std::dynamic_pointer_cast<EnemySwellState>(m_States);
			if (nullptr != swellState) {
				swellState->addForce(this, dir, m_pPlayerAttack.lock()->attackPower());
			}

			//auto power = m_pPlayerAttack.lock()->attackPower();
			//m_pRigidBody.lock()->addForce(dir * power);
		}
		else {
			auto dir = m_Entity.lock()->getTransform()->m_Position - other.other.lock()->getTransform()->m_Position;
			//auto dir = other.other.lock()->getTransform()->front();
			dir = dir.normalize();
			dir = hitControl(dir);
			//スタン状態でなかった場合
			std::shared_ptr<EnemyStanState> stanState = std::dynamic_pointer_cast<EnemyStanState>(m_States);
			if (nullptr == stanState) {
				m_States = std::make_shared<EnemyStanState>(30 * 6, dir);
			}
			else {
				//すでにスタン状態だった場合スタンタイマーだけリセットする
				stanState->timerReset();
				stanState->addForce(dir, this);
			}
		}
	}


	void EnemyComponent::addCombo()
	{
		m_EnemyRules.lock()->addCombo();
	}

	void EnemyComponent::update()
	{
		//GUIの値取得
#ifdef _MDEBUG
		m_EnemyData->searchRange = m_pUI.lock()->getSlider("SearchRange");
#endif
		m_States = m_States->update(this);

		if (m_IsBossHit) {
			m_BossHitTimer.update();
			if (m_BossHitTimer.isEnd()) {
				m_IsBossHit = false;
				m_BossHitTimer.init();
				m_pRigidBody.lock()->setRotateFreezeFlag({ true,true,true });
				m_pRigidBody.lock()->setPositionFreezeFlag({ true,false,true });
			}
		}

		if (m_EnemyData->pHP->isEmpty()) {
			if (!m_EnemyData->isDead) {
				//m_pRigidBody.lock()->setRotateFreezeFlag({ false,false,false });
				//m_pRigidBody.lock()->setPositionFreezeFlag({ false,false,false });
				std::shared_ptr<EnemyStanState> stanState = std::dynamic_pointer_cast<EnemyStanState>(m_States);
				if (nullptr != stanState) {
					stanState->deleteStanEffect();
				}
				std::shared_ptr<EnemyAttackState> attackState = std::dynamic_pointer_cast<EnemyAttackState>(m_States);
				if (nullptr != attackState) {
					attackState->deleteEffect();
				}

				m_States = std::make_shared<EnemyDamageState>(m_EnemyData->swellTime);
			}
		}
	}

	void EnemyComponent::onCollisionEnter(const framework::HitData& other)
	{
		hitBullet(other);

		hitPoisonCollision(other);

		hitPlayer(other);

		hitAttackCollision(other);

		hitEnemy(other);

		if (other.other.lock()->getTag() == "EnemyAttack") {

			//if (m_EnemyData->isDead)return;

			m_pRigidBody.lock()->setPositionFreezeFlag({ false,false,false });
			auto dir = m_Entity.lock()->getTransform()->m_Position - other.other.lock()->getTransform()->m_Position;
			dir = dir.normalize();
			m_pRigidBody.lock()->addForce(dir * 4000);//TODO あとで強さを変えられるようにする

			std::shared_ptr<EnemyAttackState> attackState = std::dynamic_pointer_cast<EnemyAttackState>(m_States);
			if (nullptr != attackState) {
				attackState->deleteEffect();
			}

			bossHit();

			std::shared_ptr<EnemyStanState> stanState = std::dynamic_pointer_cast<EnemyStanState>(m_States);
		}
	}
}
