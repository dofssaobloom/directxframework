#include "BossEnemyComponent.h"
#include<Framework.h>
#include<Source\Component\Enemy\EnemyAttackComponent.h>
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>
#include<Source\Component\Bullet\LaunchingDevice.h>
#include<Source\Actor\BossEnemy\BossEnemyStateIdle.h>
#include<Source\Actor\BossEnemy\BossEnemyStateDead.h>
#include<Source\Actor\BossEnemy\BossEnemyStateAttackAround.h>
#include<Source\Actor\BossEnemy\BossEnemyStateAttackPlayer.h>
#include<Source\Actor\BossEnemy\BossEnemyStateDamage.h>
#include<Source\Component\Scene\GameMain\EnemyRulesComponent.h>


UsingNamespace;

namespace component {

	BossEnemyComponent::BossEnemyComponent()
	{
		m_CallOrder = 2;
	}

	BossEnemyComponent::~BossEnemyComponent()
	{
	}

	void BossEnemyComponent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { "Resource/Script/Lua/UI/BossEnemyUI.lua" });

		(*componentInitalizer).emplace_back(m_pUI);
	}

	void BossEnemyComponent::init()
	{
		m_Entity.lock()->addEvent("GolemBorn", [&]() {
			ResourceManager::getInstance()->playSound("Golem");
			ResourceManager::getInstance()->setVolume("Golem", 0);
			ResourceManager::getInstance()->setDoppler("Golem", 1);
			ResourceManager::getInstance()->setDopplerVelocity("Golem", util::Vec3(0, 0, 1));
			ResourceManager::getInstance()->setPosition("Golem", *m_Entity.lock()->getTransform());
			ResourceManager::getInstance()->setDistance("Golem", 4.5f, 40.0f);
		});

		m_Entity.lock()->addEvent("GolemCrash", [&]() {
			ResourceManager::getInstance()->playSound("GolemCrash");
			ResourceManager::getInstance()->setVolume("GolemCrash", 0);
			ResourceManager::getInstance()->setDoppler("GolemCrash", 1);
			ResourceManager::getInstance()->setDopplerVelocity("GolemCrash", util::Vec3(0, 0, 1));
			ResourceManager::getInstance()->setPosition("GolemCrash", *m_Entity.lock()->getTransform());
			ResourceManager::getInstance()->setDistance("GolemCrash", 4.5f, 40.0f);
		});

		m_Entity.lock()->addEvent("GolemAttack", [&]() {
			ResourceManager::getInstance()->playSound("GolemAttack");
			ResourceManager::getInstance()->setVolume("GolemAttack", 0);
			ResourceManager::getInstance()->setDoppler("GolemAttack", 1);
			ResourceManager::getInstance()->setDopplerVelocity("GolemAttack", util::Vec3(0, 0, 1));
			ResourceManager::getInstance()->setPosition("GolemAttack", *m_Entity.lock()->getTransform());
			ResourceManager::getInstance()->setDistance("GolemAttack", 4.0f, 40.0f);
		});

		m_pAttackComponent = m_Entity.lock()->getComponent<EnemyAttackComponent>();


		deActiveCollision();

		//攻撃用コリジョン有効化
		m_Entity.lock()->addEvent("OnCollision", [&]() {
			activeCollision();
		});

		//攻撃用コリジョン無効化
		m_Entity.lock()->addEvent("OffCollision", [&]() {
			deActiveCollision();
		});

		m_Entity.lock()->addEvent("OnAttackEffect", [&]() {
			auto trans = *m_Entity.lock()->getTransform();
			trans.scaling(40);
			trans.m_Position -= trans.front() * 250;
			m_AttackEffect = EffectManager::getInstance()->generate("BossAttack", trans);
			m_AttackEffect.lock()->setSpeed(2);
		});

		m_Entity.lock()->addEvent("OnAttackEffect2", [&]() {
			auto trans = *m_Entity.lock()->getTransform();
			trans.scaling(40);
			trans.m_Position -= trans.front() * 200;
			trans.m_Position += trans.left() * 50;
			auto effect = EffectManager::getInstance()->generate("BossAttack2", trans);
			effect.lock()->setSpeed(2);
		});

		//最初はアイドル状態
		//m_States = std::make_shared<BossEnemyStateIdle>();
		m_States = std::make_shared<BossEnemyStateDead>(30 * 50, true);
		m_BossEnemyData = std::make_unique<BossEnemyData>();

		m_IsBulletDead = false;

		//テスト
		m_BossEnemyData->isDead = false;
		m_BossEnemyData->canAttackPlayer = true;
		m_BossEnemyData->canAttackAround = true;
		m_BossEnemyData->searchRange = m_pUI.lock()->getSlider("SearchRange");
		m_BossEnemyData->attackRange = m_pUI.lock()->getSlider("AttackRange");
		m_BossEnemyData->poisonExplosionSize = 60;
		m_BossEnemyData->pSphereCollider = m_Entity.lock()->getComponent<BulletSphereCollider>();
		m_BossEnemyData->pRender = m_Entity.lock()->getComponent<RenderClientComponent>();
	      m_BossEnemyData->HP = m_pUI.lock()->getSlider("HP");
	      m_BossEnemyData->deadCount = 0;
              m_BossEnemyData->pPatrolPosNumber = 0;
		m_isAroundEnemy = false;

		m_pRigidBody = m_Entity.lock()->getComponent<BulletRigidBody>();
		m_BossEnemyData->pPlayer = Entity::findGameObj("Player1");
		m_pLaunchiDevice = m_BossEnemyData->pPlayer.lock()->getComponent<LaunchingDevice>();

		m_pPlayerAnimator = m_BossEnemyData->pPlayer.lock()->getComponent<AnimatorComponent>();
		m_pPlayerAttack = m_BossEnemyData->pPlayer.lock()->getComponent<EnemyAttackComponent>();

		m_EnemyRules = Entity::findGameObj("EnemyRules").lock()->getComponent<EnemyRulesComponent>();
		m_BulletDamageCount = 0;

		//auto myTrans = m_Entity.lock()->getTransform();
		//myTrans->m_Position = util::Vec3(-1000, -300, 0);

	}

	void BossEnemyComponent::update()
	{
		//GUIの値取得
#ifdef _MDEBUG
		m_BossEnemyData->searchRange = m_pUI.lock()->getSlider("SearchRange");
		m_BossEnemyData->attackRange = m_pUI.lock()->getSlider("AttackRange");
#endif

		//HPが０以下の場合死亡
		m_BossEnemyData->isDead = m_BossEnemyData->HP <= 0;

		if (m_BossEnemyData->isDead) {
			std::shared_ptr<BossEnemyStateDead> deadState = std::dynamic_pointer_cast<BossEnemyStateDead>(m_States);
			if (deadState == nullptr) {
				m_States = std::make_shared<BossEnemyStateDead>(30 * 50);
			}
		}
		else if (m_isAroundEnemy && m_BossEnemyData->canAttackAround) {

			std::shared_ptr<BossEnemyStateAttackAround> attackState = std::dynamic_pointer_cast<BossEnemyStateAttackAround>(m_States);
			std::shared_ptr<BossEnemyStateDead> deadState = std::dynamic_pointer_cast<BossEnemyStateDead>(m_States);
			bool isDead = deadState != nullptr;
			if (nullptr == attackState && !isDead) {
				//連続でアラウンド攻撃しないようにすでにアラウンドステートだったら処理しない
				m_States = std::make_shared<BossEnemyStateAttackAround>();
			}
		}
		m_isAroundEnemy = false;

		m_States = m_States->update(this);

		checkAttackAroundCoolTime();
		checkAttackPlayerCoolTime();
	}

	void BossEnemyComponent::onCollisionEnter(const framework::HitData & other)
	{

		hitEnemy(other);

		hitPlayer(other);

		hitPoisonCollision(other);

		hitBullet(other);

	}

	void BossEnemyComponent::activeCollision()
	{
		m_pAttackComponent.lock()->onCollision();
	}

	void BossEnemyComponent::deActiveCollision()
	{
		m_pAttackComponent.lock()->offCollision();
	}

	void BossEnemyComponent::setParam(const std::vector<std::string>& param)
	{
	}

	BossEnemyData * BossEnemyComponent::getBossEnemyData()
	{
		return m_BossEnemyData.get();
	}

	std::weak_ptr<BulletRigidBody> BossEnemyComponent::getRigidBody()
	{
		return m_pRigidBody;
	}

	void BossEnemyComponent::AttackPlayer()
	{
		m_BossEnemyData->canAttackPlayer = false;
		m_attackPlayerCoolCount = m_attackPlayerCoolTime;
	}

	void BossEnemyComponent::AttackAround()
	{
		m_BossEnemyData->canAttackAround = false;
		m_attackAroundCoolCount = m_attackAroundCoolTime;
	}

	void BossEnemyComponent::addBoddScore(short killCount)
	{
		m_EnemyRules.lock()->addBossScore(killCount);
	}

	void BossEnemyComponent::initHP()
	{
		//死んだらヒットカウントをリセット
		m_BulletDamageCount = 0;
		m_BossEnemyData->HP = m_pUI.lock()->getSlider("HP");
	}

	void BossEnemyComponent::pauseDeActive()
	{
		m_Entity.lock()->getComponent<AnimatorComponent>().lock()->onStop();
	}

	void BossEnemyComponent::pauseActive()
	{
		m_Entity.lock()->getComponent<AnimatorComponent>().lock()->onStart();
	}

	void BossEnemyComponent::checkAttackPlayerCoolTime()
	{
		if (m_BossEnemyData->canAttackPlayer)
			return;

		m_attackPlayerCoolCount -= 1;

		if (m_attackPlayerCoolCount <= 0)
			m_BossEnemyData->canAttackPlayer = true;
	}

	void BossEnemyComponent::checkAttackAroundCoolTime()
	{
		if (m_BossEnemyData->canAttackAround)
			return;

		m_attackAroundCoolCount -= 1;

		if (m_attackAroundCoolCount <= 0)
			m_BossEnemyData->canAttackAround = true;
	}

	void BossEnemyComponent::addHP(int add)
	{
		if (m_BossEnemyData->isDead)
			return;

		m_BossEnemyData->HP += add;
	}

	void BossEnemyComponent::hitEnemy(const framework::HitData & other)
	{
		if (other.other.lock()->getTag() != "Enemy")return;
		if (m_BossEnemyData->isDead)return;

		m_isAroundEnemy = true;
	}

	void BossEnemyComponent::hitPlayer(const framework::HitData & other)
	{
		if (other.other.lock()->getTag() != "Player") return;
		if (m_BossEnemyData->isDead)return;
		m_isAroundEnemy = true;

		auto dir = m_Entity.lock()->getTransform()->m_Position - other.other.lock()->getTransform()->m_Position;
		dir = dir.normalize();
		m_pRigidBody.lock()->addForce(dir * 10);//TODO あとで強さを変えられるようにする
	}

	void BossEnemyComponent::hitPoisonCollision(const framework::HitData & other)
	{
		if (other.other.lock()->getTag() != "PoisonCollision") return;
		if (m_BossEnemyData->isDead)return;

		int poisonDamage = -10;
		addHP(poisonDamage);

		//TODO 現在のステートでエフェクト生成されてる場合は消してからダメージに遷移するようにする
		//攻撃エフェクトが追加される予定
		std::shared_ptr<BossEnemyStateAttackAround> attackState = std::dynamic_pointer_cast<BossEnemyStateAttackAround>(m_States);
		if (attackState != nullptr) {
			//もし攻撃エフェクトが再生されていたらキャンセル
			if (!m_AttackEffect.expired()) {
				auto handle = m_AttackEffect.lock()->getHandle();
				EffectManager::getInstance()->effectStop(handle);
			}
		}

		m_States = std::make_shared<BossEnemyStateDamage>();

	}

	void BossEnemyComponent::hitBullet(const framework::HitData & other)
	{
		if (other.other.lock()->getTag() != "Bullet")return;
		if (m_BossEnemyData->isDead)return;

		auto bullet = other.other.lock()->getComponent<Bullet>();

		auto rate = bullet.lock()->getPower();

		auto damage = util::lerp<float>(rate, 1, 5);
		int bulletDamage = -damage;
		addHP(bulletDamage);
		m_BulletDamageCount += damage;

		//10ダメージ受けるたびにひるませる
		if (m_BulletDamageCount >= 5) {
			std::shared_ptr<BossEnemyStateAttackAround> attackState = std::dynamic_pointer_cast<BossEnemyStateAttackAround>(m_States);
			if (attackState != nullptr) {
				//もし攻撃エフェクトが再生されていたらキャンセル
				if (!m_AttackEffect.expired()) {
					auto handle = m_AttackEffect.lock()->getHandle();
					EffectManager::getInstance()->effectStop(handle);
				}
			}
			m_BulletDamageCount = 0;
			m_States = std::make_shared<BossEnemyStateDamage>();
		}

		auto copyTrans = *m_Entity.lock()->getTransform();
		copyTrans.m_Position = other.hitPosA;
		copyTrans.scaling(30);
		EffectManager::getInstance()->generate("PoisonHit", copyTrans);

	}
}
