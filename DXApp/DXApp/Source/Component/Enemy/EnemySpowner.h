#pragma once
#include<Framework.h>
#include<memory>
#include<list>
#include<Source\Util\Math\Randam.h>


namespace component {
	class RulesComponent;
	class GameTimerCompornent;

	class EnemySpowner : public framework::UpdateComponent
	{
	public:
		EnemySpowner();
		~EnemySpowner();

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		/**
		* @brief		初期化
		* @Detail		すべてのコンポーネントが一斉に呼ばれる
		*/
		virtual void init();

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		// UpdateComponent を介して継承されました
		virtual void update()override;


		/// <summary>
		/// エネミー生成
		/// </summary>
		void spown();

		/// <summary>
		/// 最大の数取得(生成数限定用)
		/// </summary>
		/// <returns></returns>
		bool isEnemyMax();

	private:
		/// <summary>
		/// エネミーリストの要素削除
		/// </summary>
		void EnemyListElementDelete();

		/// <summary>
		/// エネミーのタイプ用ランダム関数
		/// </summary>
		/// <param name="randam">乱数用</param>
		/// <param name="designation">指定範囲</param>
		/// <returns></returns>
		bool EnemyTypeRandam(int randam,int designation);

	private:
		float m_BornMax;
		float m_MaxInterval;
		float m_MinInterval;

		short m_EnemyTypeA;
		short m_EnemyTypeB;
		short m_EnemyTypeC;


		short m_BornMin;

		util::Timer m_BornUpTimer;
		util::Randam m_Randam;
		std::unique_ptr<util::Timer> m_pTimer;
		std::weak_ptr<GUIComponent> m_pUI;
		std::weak_ptr<GameTimerCompornent> m_pGameTimer;
		std::list<std::weak_ptr<framework::Entity>> m_EnemyList;
	};
}
