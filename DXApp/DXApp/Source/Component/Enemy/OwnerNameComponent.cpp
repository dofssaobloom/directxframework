#include "OwnerNameComponent.h"


namespace component {
	OwnerNameComponent::OwnerNameComponent()
	{
	}
	OwnerNameComponent::~OwnerNameComponent()
	{
	}
	void OwnerNameComponent::setParam(const std::vector<std::string>& param)
	{
		m_OwnerName = param[0];
	}

	const std::string & OwnerNameComponent::getOwnerName()
	{
		return m_OwnerName;
	}


}