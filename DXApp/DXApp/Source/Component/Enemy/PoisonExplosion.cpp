#include "PoisonExplosion.h"
#include<Source\Component\UI\GUIComponent.h>

namespace component {
	PoisonExplosion::PoisonExplosion()
	{
	}

	PoisonExplosion::~PoisonExplosion()
	{
	}

	void PoisonExplosion::init()
	{
		m_pUI.lock()->getSlider("Explosion");

	}

	void PoisonExplosion::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pTrans = m_Entity.lock()->getTransform();
		m_pPoisonCollisioinObj = framework::Entity::createEntity("Poison", "PoisonCollision", *m_pTrans);
		m_pPoisonRigid = m_pPoisonCollisioinObj.lock()->addComponent < BulletRigidBody >(componentInitalizer, { "Resource/Script/Lua/UI/RigidBody.lua" ,"Resource/ByteData/UIPrefab/PoisonRigidBody.pfb" });
		auto collider = m_pPoisonCollisioinObj.lock()->addComponent < BulletSphereCollider >(componentInitalizer, { "Resource/Script/Lua/UI/SphereCollider.lua" ,"Resource/ByteData/UIPrefab/PosionCollider.pfb" });
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath });

		collider.lock()->init();
		m_pPoisonRigid.lock()->init();
		m_pPoisonRigid.lock()->deActive();

		(*componentInitalizer).emplace_back(m_pUI);
		//(*componentInitalizer).emplace_back(collider);
		//(*componentInitalizer).emplace_back(m_pPoisonRigid);
	}

	void PoisonExplosion::setParam(const std::vector<std::string>& param)
	{
		m_LuaPath = param[0];
	}

	void PoisonExplosion::update()
	{
		//ポジションを同期
		if (!m_pPoisonCollisioinObj.expired())
			m_pPoisonCollisioinObj.lock()->getTransform()->m_Position = m_pTrans->m_Position;
	}

	void PoisonExplosion::activeCollision()
	{
		m_pPoisonRigid.lock()->active();
	}

	void PoisonExplosion::deActiveCollision()
	{
		m_pPoisonRigid.lock()->deActive();
	}

	void PoisonExplosion::destroyCollision()
	{
		destory(m_pPoisonCollisioinObj);
	}
}