#pragma once
#include<Framework.h>
#include<memory>
#include<list>
#include<Source\Util\Math\Randam.h>


namespace component {
	class RulesComponent;
	class GameTimerCompornent;

	class OwnerNameComponent : public framework::Component
	{
	public:
		OwnerNameComponent();
		~OwnerNameComponent();


		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		/// <summary>
		/// 持ち主名取得
		/// </summary>
		/// <returns></returns>
		const std::string& getOwnerName();


	private:
		std::string m_OwnerName;
	};
}
