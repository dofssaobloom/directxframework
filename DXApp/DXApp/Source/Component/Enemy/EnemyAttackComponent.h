#pragma once

#include<Framework.h>
namespace framework {
	class Entity;
}

namespace component {
	class ReadyComponent;
	class GUIComponent;

	class EnemyAttackComponent : public framework::UpdateComponent
	{
	public:
		EnemyAttackComponent();
		~EnemyAttackComponent();

		void init()override;

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		/// <summary>
		/// 攻撃用コリジョンをオンにする
		/// </summary>
		void onCollision();

		/// <summary>
		/// 攻撃用コリジョンをオフにする
		/// </summary>
		void offCollision();

		void changeColliderRadius(float newRadius);
		void changeColliderOffset(util::Vec3 offSet);


	private:
		std::weak_ptr<framework::Entity> m_pAttackEntity;
		std::weak_ptr<BulletRigidBody> m_pRigidBody;

		std::weak_ptr<BulletSphereCollider> m_pCollider;

		std::weak_ptr<GUIComponent> m_pUI;

		std::unique_ptr<util::Timer> m_pChangeTime;

		float m_FrontOffset;

		std::string m_LuaPath;

		short m_MaxPower;

		short m_MinPower;

		short m_PreChageTime;

	};
}
