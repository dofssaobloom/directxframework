#pragma once
#include<Framework.h>
#include<memory>
#include<Source\State\State.h>
#include<Source\/Application\Device\Input.h>
#include<Source\Uitl\Tank.h>
#include<Source\Actor\Enemy\EEnemyType.h>

namespace component {
	class PlayerComponent;
	class PoisonExplosion;
	class BulletRigidBody;
	class SearchComponent;
	class PhysicsWorld;
	class AttackComponent;
	class CameraController;
	class ZoomBlur;

	struct EnemyData
	{
		//!膨らむときの最大の大きさ
		float maxScale;

		//爆発エフェクトの大きさ
		float poisonExplosionSize;
		
		//見た目が大きくなるのでコリジョンも大きくする
		std::pair<float, float> colliderSize;

		//コリジョンが大きくなるので大きくなった分だけオフセットをずらす
		std::pair<float, float> colliderOffset;

		//膨らむ時間
		int swellTime;

		//!死亡フラグ
		bool isDead;

		//!毒状態エネミー同士が衝突したかどうか
		bool isContactExplosion;

		float searchRange;

		framework::WeakEntity pPlayer;

		//!HPオブジェクト
		std::unique_ptr<Tank<short>> pHP;

		std::weak_ptr<PoisonExplosion> pPoison;
		std::weak_ptr<BulletSphereCollider> pSphereCollider;
		std::weak_ptr<RenderClientComponent> pRender;
		std::weak_ptr<PhysicsWorld> pWorld;
		std::weak_ptr<AttackComponent> pAttack;
		std::unique_ptr<util::Timer> pRecastTimer;
		std::weak_ptr<ZoomBlur> pBluer;

		EnemyType enemyType;
		std::weak_ptr<CameraController> pCameraController;
	};
}

namespace component {
	class LaunchingDevice;
	class EnemyRulesComponent;
	class AnimatorComponent;
	class AttackComponent;
	class PointLightComponent;

	class EnemyComponent : public framework::UpdateComponent
	{
	public:
		EnemyComponent();
		~EnemyComponent();

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		void init()override;

		void poisnSE();
		void damageSE();
		void deadSE();
		void bombSE();

		// UpdateComponent を介して継承されました
		virtual void update() override;

		virtual void onCollisionEnter(const framework::HitData& other)override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		/// <summary>
		/// エネミーの設定等取得
		/// </summary>
		/// <returns></returns>
		EnemyData* getEnemyData();

		/// <summary>
		/// リジッドボディ取得
		/// </summary>
		/// <returns></returns>
		std::weak_ptr<BulletRigidBody> getRigidBody();

		//コンボとスコアとタイマーの追加
		void addCombo();
		void addScore();
		void addTimer();


		std::weak_ptr<AttackComponent> getPlayerAttack();

		void bossHit();

		/// <summary>
		/// ポーズ用ディアクティブ
		/// </summary>
		void pauseDeActive();


		/// <summary>
		/// ポーズ用アクティブ
		/// </summary>
		void pauseActive();

		/// <summary>
		/// ライトカラーセット
		/// </summary>
		/// <param name="color"></param>
		void setColor(const util::Vec4& color);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="atten"></param>
		void setAtten(const util::Vec3& atten);
	private:
		/// <summary>
		/// プレイヤーに殴られたとき多少方向を決めれる
		/// </summary>
		util::Vec3 hitControl(const util::Vec3& front);


		//コリジョン処理

		/// <summary>
		/// 他の敵にあたったとき
		/// </summary>
		void hitEnemy(const framework::HitData& other);

		void hitPlayer(const framework::HitData& other);

		void hitPoisonCollision(const framework::HitData& other);

		void hitBullet(const framework::HitData& other);

		void hitAttackCollision(const framework::HitData& other);

	private:
		bool m_IsBulletDead;
		std::weak_ptr<EnemyRulesComponent> m_EnemyRules;
		std::weak_ptr<AnimatorComponent> m_pPlayerAnimator;
		std::weak_ptr<AttackComponent> m_pPlayerAttack;
		std::unique_ptr<EnemyData> m_EnemyData;
		std::shared_ptr<State<EnemyComponent>> m_States;
		std::weak_ptr<GUIComponent> m_pUI;
		std::weak_ptr<BulletRigidBody> m_pRigidBody;
		std::weak_ptr<LaunchingDevice> m_pLaunchiDevice;
		std::weak_ptr<PointLightComponent> m_pPointLight;

		//ボスに攻撃されると吹っ飛ぶので一致時間したらリジッドボディのフリーズをもとに戻す
		util::Timer m_BossHitTimer;
		bool m_IsBossHit;

		const float m_MinSwelTime;
		const float m_MaxSwelTime;
	};
}
