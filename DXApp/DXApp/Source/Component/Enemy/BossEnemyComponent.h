#pragma once
#include<Framework.h>
#include<memory>
#include<Source\State\State.h>
#include<Source\/Application\Device\Input.h>
#include<Source\Uitl\Tank.h>

namespace framework {
	class Effect;
}

namespace component {

	class PlayerComponent;
	class PoisonExplosion;
	class BulletRigidBody;
	class EnemyRulesComponent;

	struct BossEnemyData
	{
		//爆発エフェクトの大きさ
		float poisonExplosionSize;
		
		//!死亡フラグ
		bool isDead;
		bool canAttackPlayer;
		bool canAttackAround;

		float searchRange;
		float attackRange;

		framework::WeakEntity pPlayer;

		int HP;
		//!死んだ数　スコアに影響する
              short deadCount;
	      int pPatrolPosNumber;

		std::weak_ptr<PoisonExplosion> pExplosion;
		std::weak_ptr<BulletSphereCollider> pSphereCollider;
		std::weak_ptr<RenderClientComponent> pRender;
	};
}

namespace component {
	class LaunchingDevice;
	class AnimatorComponent;
	class EnemyAttackComponent;

	class BossEnemyComponent : public framework::UpdateComponent
	{
	public:
		BossEnemyComponent();
		~BossEnemyComponent();

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		void init()override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

		virtual void onCollisionEnter(const framework::HitData& other)override;

		/**
		* @brief		あたり判定を有効
		*/
		void activeCollision();

		/**
		* @brief		あたり判定を無効
		*/
		void deActiveCollision();

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		/// <summary>
		/// エネミーの設定等取得
		/// </summary>
		/// <returns></returns>
		BossEnemyData* getBossEnemyData();

		/// <summary>
		/// リジッドボディ取得
		/// </summary>
		/// <returns></returns>
		std::weak_ptr<BulletRigidBody> getRigidBody();

		void AttackPlayer();
		void AttackAround();

		/// <summary>
		/// ボスのスコア加算
		/// </summary>
		void addBoddScore(short killCount);

		//HP初期化
		void initHP();


		/// <summary>
		/// ポーズ用ディアクティブ
		/// </summary>
		void pauseDeActive();


		/// <summary>
		/// ポーズ用アクティブ
		/// </summary>
		void pauseActive();

	private:
		void checkAttackPlayerCoolTime();
		void checkAttackAroundCoolTime();

		void addHP(int add);


		void hitEnemy(const framework::HitData & other);

		void hitPlayer(const framework::HitData & other);

		void hitPoisonCollision(const framework::HitData & other);

		void hitBullet(const framework::HitData & other);

	private:
		bool m_IsBulletDead;
		bool m_isAroundEnemy;

		//!バレットで１０ダメージ受けるたびにひるませるためのカウンタ
		short m_BulletDamageCount;
		const float m_attackPlayerCoolTime = 30 * 5;
		float m_attackPlayerCoolCount;

		const float m_attackAroundCoolTime = 30 * 10;
		float m_attackAroundCoolCount;
		//std::weak_ptr<EnemyRulesComponent> m_EnemyRules;

		std::weak_ptr<framework::Effect> m_AttackEffect;

		std::weak_ptr<EnemyAttackComponent> m_pAttackComponent;
		std::weak_ptr<AnimatorComponent> m_pPlayerAnimator;
		std::weak_ptr<EnemyAttackComponent> m_pPlayerAttack;
		std::unique_ptr<BossEnemyData> m_BossEnemyData;
		std::shared_ptr<State<BossEnemyComponent>> m_States;
		std::weak_ptr<GUIComponent> m_pUI;
		std::weak_ptr<BulletRigidBody> m_pRigidBody;
		std::weak_ptr<LaunchingDevice> m_pLaunchiDevice;
		std::weak_ptr<EnemyRulesComponent> m_EnemyRules;

	};
}
