#include "EnemyAttackComponent.h"
#include<math.h>
#include<Source\Application\Device\Input.h>

namespace component {
	component::EnemyAttackComponent::EnemyAttackComponent()
	{
		m_CallOrder = 2;
	}

	component::EnemyAttackComponent::~EnemyAttackComponent()
	{
	}

	void component::EnemyAttackComponent::init()
	{
		m_FrontOffset = m_pUI.lock()->getSlider("FrontOffset");
		short time = m_PreChageTime = m_pUI.lock()->getSlider("ChageTime");
		m_MinPower = m_pUI.lock()->getSlider("MinAttackPower");
		m_MaxPower = m_pUI.lock()->getSlider("MaxAttackPower");

		m_pChangeTime = std::make_unique<util::Timer>(time);


		m_pRigidBody.lock()->deActive();
	}

	void component::EnemyAttackComponent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath });

		(*componentInitalizer).emplace_back(m_pUI);

		m_pAttackEntity = framework::Entity::createEntity("EnemyAttack", "EnemyAttack", util::Transform(m_Entity.lock()->getTransform()->m_Position, util::Vec3(), util::Vec3(1, 1, 1)));
		m_pAttackEntity.lock()->setParent(m_Entity);
		m_pRigidBody = m_pAttackEntity.lock()->addComponent<BulletRigidBody>(componentInitalizer, { "Resource/Script/Lua/UI/RigidBody.lua" ,"" });
		m_pCollider = m_pAttackEntity.lock()->addComponent<BulletSphereCollider>(componentInitalizer, { "Resource/Script/Lua/UI/SphereCollider.lua","" });
		(*componentInitalizer).emplace_back(m_pRigidBody);
		(*componentInitalizer).emplace_back(m_pCollider);
	}

	void component::EnemyAttackComponent::update()
	{
#ifdef _MDEBUG
		m_FrontOffset = m_pUI.lock()->getSlider("FrontOffset");
		short time = m_pUI.lock()->getSlider("ChageTime");
		m_MinPower = m_PreChageTime = m_pUI.lock()->getSlider("MinAttackPower");
		m_MaxPower = m_PreChageTime = m_pUI.lock()->getSlider("MaxAttackPower");

		if (m_PreChageTime != time) {
			m_pChangeTime = std::make_unique<util::Timer>(time);
			m_PreChageTime = time;
		}
#endif
		/*if (application::Input::isAttack()) {
			m_pChangeTime->init();
		}

		if (application::Input::isAttackChage()) {
			m_pChangeTime->update();
		}*/

		auto myTrans = m_Entity.lock()->getTransform();
		auto front = myTrans->front();
		front = front * m_FrontOffset;
		m_pAttackEntity.lock()->getTransform()->m_Position = myTrans->m_Position + front;
	}

	void component::EnemyAttackComponent::setParam(const std::vector<std::string>& param)
	{
		m_LuaPath = param[0];
	}

	void component::EnemyAttackComponent::onCollision()
	{
		m_pRigidBody.lock()->active();
	}

	void component::EnemyAttackComponent::offCollision()
	{
		m_pRigidBody.lock()->deActive();
	}
	void EnemyAttackComponent::changeColliderRadius(float newRadius)
	{
		m_pCollider.lock()->setScale(newRadius);
	}
	void EnemyAttackComponent::changeColliderOffset(util::Vec3 offSet)
	{
		m_pCollider.lock()->setOffset(offSet);
	}
}