#pragma once
#include<Framework.h>
#include<Source\Resource\Motion\Motion.h>
#include<unordered_map>

namespace component {

	class AnimatorComponent;
	class GUIComponent;

	/// <summary>
	/// 接続されているオブジェクトを親のボーンにジョイントさせるクラス
	/// </summary>
	class BoneJointComponent : public framework::UpdateComponent
	{
	public:
		BoneJointComponent();
		~BoneJointComponent();

		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		/**
		* @brief		初期化
		*/
		virtual void init()override;

		/**
		* @brief		更新
		*/
		virtual void update()override;


		virtual void setParam(const std::vector<std::string>& param);

	private:

		/// <summary>
		/// 現在のフレームのボーン行列取得
		/// </summary>
		/// <param name="result"></param>
		void getCurrentBoneMat(util::Mat4 & result);

		/// <summary>
		/// モーションブレンド
		/// </summary>
		void motionBlend(util::Mat4 & currentBone);

	private:
		//モーションコンテナ
		std::unordered_map<std::string, std::vector<util::Mat4>> m_MotionContainer;

		//親オブジェクトのアニメーター
		std::weak_ptr<AnimatorComponent> m_ParentAnimator;

		std::string m_JointBoneName;

		std::weak_ptr<GUIComponent> m_pUI;

		std::string m_LuaPath;

		util::Transform m_OffsetTrans;

		//!オリジナルボーン回転
		util::Vec3 m_BoneRotate;
		//!ブレンド回転
		util::Vec3 m_BlendBoneRotate;
		//!オリジナルとブレンドのミックス
		util::Vec3 m_MixRotate;


		float m_Weight;
	};
}