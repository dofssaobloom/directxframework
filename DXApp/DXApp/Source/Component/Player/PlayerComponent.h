#pragma once

#include<Source\Component\UpdateComponent.h>
#include<Source\Actor\PlayerStates.h>
#include<memory>
#include<Source\State\State.h>
#include<unordered_map>
#include<assert.h>
#include<Source\Util\Type.h>
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>
#include<Source\Util\Timer\Timer.h>
#include<Source\Uitl\Tank.h>

namespace framework{
	class Entity;
}

namespace component {
	class RulesComponent;
	class LaunchingDevice;
	class BulletRigidBody;
	class PhysicsWorld;
	class CameraController;
	class GameTimerCompornent;
}

struct PlayerData
{
	PlayerState currentState;
	std::unordered_map<PlayerState, std::string> motionNames;
	std::weak_ptr<component::LaunchingDevice> pLauncher;
	std::weak_ptr<component::BulletRigidBody> pRigidBody;
	std::weak_ptr<component::CameraController> pCameraController;
	std::weak_ptr<component::GameTimerCompornent> pGameTimer;

	//!コントローラの番号
	int controlID;

	//UIの変数
	float bendFriction;
	float speed;
	float setupRotationSpeed;
	float setupMoveSpeed;
	float jumpFrontForce;
	float jumpUpForce;

	//!毒の発射できる量
	std::unique_ptr<Tank<float>> pPoisonTank;
	////!タンクの最大蓄積量
	//float maxPoisonTank;

	//!地面に接しているか
	bool isFloorContact;
	//!前のフレーム地面に接触しているか
	bool isPreFloorContact;

	std::weak_ptr<framework::Effect> smokeEffect;
	bool hitDamage;
	int hp;

	std::unordered_map<std::string, PlayerState> convertData
		= {std::make_pair("Idle",PlayerState::idle),
		std::make_pair("Move",PlayerState::move),
		std::make_pair("Attack",PlayerState::attack),
		std::make_pair("Damage",PlayerState::damage),
		std::make_pair("Jump",PlayerState::jump),
		std::make_pair("Die",PlayerState::die),
		std::make_pair("Exchanging",PlayerState::setup),

		
	};

	PlayerState convert(const std::string& stateName){
		assert(convertData.find(stateName) != convertData.end() && "キーが登録されていません");
		return convertData[stateName];
	}
};

namespace component {

	class ReadyComponent;
	class GUIComponent;
	class SpriteRenderComponent;
	class AttackComponent;

	class PlayerComponent : public framework::UpdateComponent
	{
	public:
		PlayerComponent();
		~PlayerComponent();

		void init()override;

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		const PlayerState& getSate();

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);


		/**
		* @brief		更新
		*/
		virtual void update();

		/**
		* @brief		プレイヤーデータ取得
		*/
		std::weak_ptr<PlayerData> getPlayerData();

		/**
		* @brief		衝突したときの処理
		* @param other	衝突した相手オブジェクト
		*/
		virtual void onCollisionEnter(const framework::HitData& other)override;

		virtual void onCollisionStay(const framework::HitData& other)override;

		/**
		* @brief		あたり判定を有効
		*/
		void activeCollision();

		/**
		* @brief		あたり判定を無効
		*/
		void deActiveCollision();
		

		void damage();
		void damageSE();
		void attack();

		bool isDead();

		/// <summary>
		/// 地面から離れたばかりかどうか
		/// </summary>
		/// <returns></returns>
		bool isJumpTrriger();

		/// <summary>
		/// 地面に着地した瞬間
		/// </summary>
		/// <returns></returns>
		bool isLandingTrriger();


		/// <summary>
		/// 無敵時間を発生させるフラグを立てる
		/// </summary>
		void hit();

		/// <summary>
		/// 強制的にアイドルにする
		/// </summary>
		void changeIdle();


		/// <summary>
		/// ポーズ用ディアクティブ
		/// </summary>
		void pauseDeActive();


		/// <summary>
		/// ポーズ用アクティブ
		/// </summary>
		void pauseActive();

	private:
		bool isBorer();

		/// <summary>
		/// 壁かどうか
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		bool isWall(const framework::HitData& other);

		/// <summary>
		/// 床かどうか
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		bool isFloor(const framework::HitData& other);

	public:
		static const float BORDER;


	private:
		std::shared_ptr<PlayerData> m_PlayerData;
		std::shared_ptr<State<PlayerComponent>> m_States;

		std::weak_ptr<AttackComponent> m_pAttackComponent;
		std::weak_ptr<SpriteRenderComponent> m_pHPGauge;
		std::weak_ptr<SpriteRenderComponent> m_pPoisonGauge1;
		std::weak_ptr<SpriteRenderComponent> m_pPoisonGauge2;
		std::weak_ptr<GUIComponent> m_pUI;
		std::weak_ptr<PhysicsWorld> m_pWorld;

		//!無敵時間
		util::Timer m_InvincibleTimer;
		bool m_IsHit;

		std::string m_LuaPath;

		bool isEnd;

		//!無敵時間
		util::Timer m_DamageTimer;

		const int maxHP;

		float d;
	};

}
