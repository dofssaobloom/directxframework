#pragma once
#include<Framework.h>
namespace framework {
	class Entity;
}

namespace component {
	class CircleCollider;
	class ReadyComponent;
	class GUIComponent;

	class AttackComponent : public framework::UpdateComponent
	{
	public:
		AttackComponent();
		~AttackComponent();

		void onConect()override;

		void init()override;

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);


		/**
		* @brief		エフェクトが終了したか
		*/
		bool isEffectEnd();

		/// <summary>
		/// 攻撃用コリジョンをオンにする
		/// </summary>
		void onCollision();

		/// <summary>
		/// 攻撃用コリジョンをオフにする
		/// </summary>
		void offCollision();

		/// <summary>
		/// チャージ率取得
		/// </summary>
		/// <returns></returns>
		float attackPower();


		/// <summary>
		/// 攻撃コリジョンオブジェクトの名前
		/// </summary>
		/// <returns></returns>
		const std::string& getCollisionName();

		void setTime(short time);

		void setFrontFlag(bool isFront);

		/// <summary>
		/// 移動時間 0 ~ 1
		/// </summary>
		/// <returns></returns>
		float moveRate();
	private:

	private:
		std::weak_ptr<framework::Entity> m_pAttackEntity;
		std::weak_ptr<BulletRigidBody> m_pRigidBody;

		std::weak_ptr<GUIComponent> m_pUI;

		std::unique_ptr<util::Timer> m_pChangeTime;

		std::unique_ptr<util::Timer> m_pMoveTimer;

		float m_FrontOffset;

		std::string m_LuaPath;

		std::string m_CollisionPath;

		std::string m_AttackLuaPath;

		short m_MaxPower;

		short m_MinPower;

		short m_PreChageTime;

		//!前方向が反転してるか
		bool m_IsFornt;

	};



}
