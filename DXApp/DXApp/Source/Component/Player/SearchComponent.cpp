#include "SearchComponent.h"

component::SearchComponent::SearchComponent()
{
	//リジッドボディよりあとに初期化
	m_CallOrder = 2;
}

component::SearchComponent::~SearchComponent()
{
}

void component::SearchComponent::init()
{
	m_Radius = m_pUI.lock()->getSliderPointer("SearchRadius");
	m_pCollider.lock()->setScale(*m_Radius);

	m_Offset.x = m_pUI.lock()->getSlider("SearchOffsetX");
	m_Offset.y = m_pUI.lock()->getSlider("SearchOffsetY");
	m_Offset.z = m_pUI.lock()->getSlider("SearchOffsetZ");


	//アップデートを遅くしないとヒットリストがクリアされてしまう
	m_CallOrder = 100;

}

const short component::SearchComponent::getSearchNum()
{
	return m_HitList.size();
}

void component::SearchComponent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
{
	m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath });

	(*componentInitalizer).emplace_back(m_pUI);

	m_pSearchEntity = framework::Entity::createEntity(m_Entity.lock()->getName() + "Serch", "Serch", util::Transform(m_Entity.lock()->getTransform()->m_Position, util::Vec3(), util::Vec3(1, 1, 1)));
	m_pSearchEntity.lock()->setParent(m_Entity);
	m_pRigidBody = m_pSearchEntity.lock()->addComponent<BulletRigidBody>(componentInitalizer, { "Resource/Script/Lua/UI/RigidBody.lua" ,"" });
	m_pCollider = m_pSearchEntity.lock()->addComponent<BulletSphereCollider>(componentInitalizer, { "Resource/Script/Lua/UI/SphereCollider.lua","" });
	
	m_pCollider.lock()->init();
	m_pRigidBody.lock()->init();
	m_pRigidBody.lock()->setTrriger(true);
}

void component::SearchComponent::update()
{
#ifdef _MDEBUG
	m_pCollider.lock()->setScale(*m_Radius);

	m_Offset.x = m_pUI.lock()->getSlider("SearchOffsetX");
	m_Offset.y = m_pUI.lock()->getSlider("SearchOffsetY");
	m_Offset.z = m_pUI.lock()->getSlider("SearchOffsetZ");
#endif

	m_HitList.clear();
	m_pSearchEntity.lock()->getTransform()->m_Position = m_Entity.lock()->getTransform()->m_Position + m_Offset;


}

void component::SearchComponent::setParam(const std::vector<std::string>& param)
{
	m_LuaPath = param[0];
	auto copy = param;
	
	copy.erase(copy.begin());
	m_MaskList = copy;
}

void component::SearchComponent::onCollisionStay(const framework::HitData & other)
{
	//このコンポーネントをつけてるゲームオブジェクトは対象外とする
	if (other.other._Get() == m_Entity._Get())return;
	if (other.other.lock()->getTag() == "Serch")return;
	//マスクで設定したタグのものは除外する
	for (auto& name : m_MaskList)
	{
		if (name == other.other.lock()->getTag())
			return;
	}
	m_HitList.emplace_back(other.other);
}

void component::SearchComponent::setRadius(float radius)
{
	*m_Radius = radius;
}
