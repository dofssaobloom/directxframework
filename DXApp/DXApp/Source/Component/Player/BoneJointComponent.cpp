#include "BoneJointComponent.h"
#include<Source\Component\UI\GUIComponent.h>
#include<Source\Util\Math\Math.h>

namespace component {

	BoneJointComponent::BoneJointComponent()
	{
		m_CallOrder = 0;
	}

	BoneJointComponent::~BoneJointComponent()
	{
	}

	void BoneJointComponent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath });

		(*componentInitalizer).emplace_back(m_pUI);
	}

	void BoneJointComponent::init()
	{
		auto parent = m_Entity.lock()->getParent();

		assert(!parent.expired() && "親オブジェクトがいません");

		m_ParentAnimator = parent.lock()->getComponent<AnimatorComponent>();
		assert(!m_ParentAnimator.expired() && "アニメーターがついていません");

		std::unordered_map<std::string, std::shared_ptr<framework::Motion>> motionContainer;

		//リソースマネージャからモーションデータ取得
		for (auto name : m_ParentAnimator.lock()->getMotionNames()) {
			motionContainer[name] = framework::ResourceManager::getInstance()->getMotion(name);
		}

		//指定したボーンの行列酒取得
		for (auto& motion : motionContainer)
		{
			m_MotionContainer[motion.first] = motion.second->getFrameMotionWithName(m_JointBoneName);
		}

		m_OffsetTrans.m_Position.x = m_pUI.lock()->getSlider("OffsetX");
		m_OffsetTrans.m_Position.y = m_pUI.lock()->getSlider("OffsetY");
		m_OffsetTrans.m_Position.z = m_pUI.lock()->getSlider("OffsetZ");

		m_Weight = m_pUI.lock()->getSlider("Weight");
	}

	void BoneJointComponent::update()
	{

#ifdef _MDEBUG
		m_OffsetTrans.m_Position.x = m_pUI.lock()->getSlider("OffsetX");
		m_OffsetTrans.m_Position.y = m_pUI.lock()->getSlider("OffsetY");
		m_OffsetTrans.m_Position.z = m_pUI.lock()->getSlider("OffsetZ");
		m_Weight = m_pUI.lock()->getSlider("Weight");
#endif

		auto trasn = m_Entity.lock()->getTransform();
		auto parentTrans = m_Entity.lock()->getParent().lock()->getTransform();


		util::Mat4 boenMat;
		getCurrentBoneMat(boenMat);

		motionBlend(boenMat);


		if (m_ParentAnimator.lock()->isBlend()) {
			auto blendRate = m_ParentAnimator.lock()->getBlendRate();
			m_MixRotate.x = util::lerp<float>(blendRate, m_BoneRotate.x, m_BlendBoneRotate.x);
			m_MixRotate.y = util::lerp<float>(blendRate, m_BoneRotate.y, m_BlendBoneRotate.y);
			m_MixRotate.z = util::lerp<float>(blendRate, m_BoneRotate.z, m_BlendBoneRotate.z);
		}
		else {
			m_MixRotate = m_BoneRotate;
		}

		XMVECTOR scale;
		XMVECTOR rotate;
		XMVECTOR p;

		auto q = util::Vec3::toQuaternion(util::Vec3(XMConvertToRadians(m_MixRotate.x), XMConvertToRadians(m_MixRotate.y), XMConvertToRadians(m_MixRotate.z)));
		XMMatrixDecompose(&scale, &rotate, &p, parentTrans->toMatrix().transpose().toXMMatrix());
		q = XMQuaternionMultiply(q.toXMVector(), rotate);

		m_MixRotate = util::quaternionToEuler(q);

		auto pos = m_OffsetTrans.m_Position;
		pos = XMVector3Transform(pos.toXMVector(), boenMat.transpose().toXMMatrix());
		pos = XMVector3Transform(pos.toXMVector(), parentTrans->toMatrix().transpose().toXMMatrix());


		trasn->m_Position = pos;
		trasn->m_Rotation = m_MixRotate;
	}

	void BoneJointComponent::setParam(const std::vector<std::string>& param)
	{
		m_JointBoneName = param[0];
		m_LuaPath = param[1];
	}

	void BoneJointComponent::getCurrentBoneMat(util::Mat4 & result)
	{
		auto currentMotionName = m_ParentAnimator.lock()->getCurrentName();
		int frame = 0;
		frame = m_ParentAnimator.lock()->getFrame();

		result = m_MotionContainer[currentMotionName][frame];

		XMVECTOR scale;
		XMVECTOR rotate;
		XMVECTOR p;

		XMMatrixDecompose(&scale, &rotate, &p, result.transpose().toXMMatrix());
		util::Quaternion q = rotate;
		m_BoneRotate = util::quaternionToEuler(q);
	}
	void BoneJointComponent::motionBlend(util::Mat4 & result)
	{
		//ブレンドでなければそのまま返す
		if (!m_ParentAnimator.lock()->isBlend()) {
			return;
		}


		auto blendRate = m_ParentAnimator.lock()->getBlendRate();

		auto blendFrame = m_ParentAnimator.lock()->getBlendFrame();

		const auto blendName = m_ParentAnimator.lock()->getBlendMotionName();

		auto& blendMat = m_MotionContainer[blendName][blendFrame];

		XMVECTOR scale;
		XMVECTOR rotate;
		XMVECTOR p;

		XMMatrixDecompose(&scale, &rotate, &p, blendMat.transpose().toXMMatrix());
		util::Quaternion q = rotate;
		m_BlendBoneRotate = util::quaternionToEuler(q);

		result = util::lerpMatrix(blendRate, result, blendMat);

	}

}
