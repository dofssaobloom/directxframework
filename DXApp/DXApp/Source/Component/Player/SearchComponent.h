#pragma once
#include<Framework.h>

namespace component {
	class CircleCollider;
	class ReadyComponent;
	class GUIComponent;

	/// <summary>
	/// 周囲のアクターを検索するコンポーネント
	/// </summary>
	class SearchComponent : public framework::UpdateComponent
	{
	public:
		SearchComponent();
		~SearchComponent();

		/**
		* @brief		初期化
		*/
		virtual void init()override;


		/// <summary>
		/// 周囲にいるゲームオブジェクトの数
		/// </summary>
		/// <returns></returns>
		const short getSearchNum();

		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;


		// UpdateComponent を介して継承されました
		virtual void update() override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		virtual void onCollisionStay(const framework::HitData& other);

		/// <summary>
		/// 半径セット
		/// </summary>
		/// <param name="radius"></param>
		void setRadius(float radius);

	private:
		//!検索用ゲームオブジェクト
		std::weak_ptr<framework::Entity> m_pSearchEntity;
		std::weak_ptr<BulletRigidBody> m_pRigidBody;
		std::weak_ptr<BulletSphereCollider> m_pCollider;

		std::list<framework::WeakEntity> m_HitList;
		//!検索対象外リスト
		std::vector<std::string> m_MaskList;

		std::weak_ptr<GUIComponent> m_pUI;

		//!コリジョンの座標オフセット
		util::Vec3 m_Offset;

		float* m_Radius;

		std::string m_LuaPath;
	};
}