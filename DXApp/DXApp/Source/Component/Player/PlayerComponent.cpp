#include"PlayerComponent.h"
#include<Source\Entity\Entity.h>
#include<Source\Actor\PlayerMoveState.h>
#include<Source\Actor\PlayerIdleState.h>
#include<Source\Actor\PlayerSetUpState.h>
#include<Source\Util\IO\CSVLoader.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Resource\Motion\Motion.h>
#include<Source\Component\Physics\CircleCollider.h>
#include<Source\Component\BoxRenderClientComponent.h>
#include<Source\Component\Physics\RigidBody.h>
#include"AttackComponent.h"
#include<Source\Actor\PlayerDamageState.h>
#include<Source\Application\Scene\SceneDelivery.h>
#include<Source\Component\Player\PlayerComponent.h>
#include<Source\Component\Scene\GameMain\ReadyComponent.h>
#include<Source\Util\Lua\LuaBase.h>
#include<Source\Component\UI\GUIComponent.h>
#include<Source\Component\Bullet\LaunchingDevice.h>
#include<Source\Component\Player\AttackComponent.h>
#include<Source\Component\Physics\PhysicsWorld.h>
#include<Source\Component\Device\CameraController.h>
#include<Source\Component\Effect\ZoomBlur.h>
#include<Source\Component\Scene\GameMain\GameTimerCompornent.h>

using namespace framework;

namespace component {

	const float PlayerComponent::BORDER = 1450;

	PlayerComponent::PlayerComponent()
		:m_DamageTimer(50),
		maxHP(100),
		m_InvincibleTimer(30 * 5)
	{
		m_PlayerData = std::make_shared<PlayerData>();

		m_CallOrder = 2;
	}

	PlayerComponent::~PlayerComponent()
	{
	}

	void PlayerComponent::init()
	{
		m_Entity.lock()->addEvent("PlayerAttack", [&]() {
			ResourceManager::getInstance()->playSound("PlayerAttack2");
			ResourceManager::getInstance()->setVolume("PlayerAttack2", -2000);
			ResourceManager::getInstance()->setDoppler("PlayerAttack2", 0);
		});

		m_Entity.lock()->addEvent("PlayerDamage", [&]() {
			ResourceManager::getInstance()->playSound("PlayerDamage2");
			ResourceManager::getInstance()->setVolume("PlayerDamage2", -2000);
			ResourceManager::getInstance()->setDoppler("PlayerDamage2", 0);
		});

		m_States = std::make_shared<PlayerIdleState>();

		//最初はアイドル状態
		m_PlayerData->currentState = PlayerState::idle;

		m_PlayerData->hp = maxHP;

		m_pAttackComponent = m_Entity.lock()->getComponent<AttackComponent>();


		//自分のコリジョン有効化
		m_Entity.lock()->addEvent("OnCollision", [&]() {
			activeCollision();
		});

		//自分のコリジョン無効化
		m_Entity.lock()->addEvent("OffCollision", [&]() {
			deActiveCollision();
		});

		m_PlayerData->hitDamage = false;

		isEnd = false;

		m_PlayerData->isPreFloorContact = m_PlayerData->isFloorContact = true;

		//後で初期化
		m_PlayerData->bendFriction = m_pUI.lock()->getSlider("Friction");
		m_PlayerData->speed = m_pUI.lock()->getSlider("Speed");
		m_PlayerData->setupRotationSpeed = m_pUI.lock()->getSlider("SetUpRotationSpeed");
		m_PlayerData->setupMoveSpeed = m_pUI.lock()->getSlider("SetUpMoveSpeed");
		m_PlayerData->jumpFrontForce = m_pUI.lock()->getSlider("JumpFrontForce");
		m_PlayerData->jumpUpForce = m_pUI.lock()->getSlider("JumpUpForce");

		m_PlayerData->pPoisonTank = std::make_unique<Tank<float>>(100);


		m_PlayerData->pLauncher = m_Entity.lock()->getComponent<LaunchingDevice>();
		m_PlayerData->pRigidBody = m_Entity.lock()->getComponent<BulletRigidBody>();
		m_PlayerData->pRigidBody.lock()->setPositionFreezeFlag({ true,false,true });
		m_PlayerData->pRigidBody.lock()->setRotateFreezeFlag({ true,true,true });

		m_pHPGauge = Entity::findGameObj("HPGauge").lock()->getComponent<SpriteRenderComponent>();
		m_pPoisonGauge1 = Entity::findGameObj("PoisonGauge1").lock()->getComponent<SpriteRenderComponent>();
		m_pPoisonGauge2 = Entity::findGameObj("PoisonGauge2").lock()->getComponent<SpriteRenderComponent>();
		m_PlayerData->pGameTimer = framework::Entity::findGameObj("GameTimer").lock()->getComponent<GameTimerCompornent>();


		m_pWorld = Entity::findGameObj("PhysicsWorld").lock()->getComponent<PhysicsWorld>();

		//auto golem = Entity::findGameObj("Golem").lock()->getComponent<AnimatorComponent>();
		//golem.lock()->changeAnime("Golem_Walk");


		//霊キャストテスト
		//enemy = Entity::findGameObj("E1");

		//world = Entity::findGameObj("PhysicsWorld").lock()->getComponent<PhysicsWorld>();

		//auto test = world.lock()->rayCast(m_Entity.lock()->getTransform()->m_Position, enemy.lock()->getTransform()->m_Position);
		//if (test.hasHit()) {
		//	//あたったゲームオブジェクト
		//	auto e = test.getHitEntity();
		//	//あたった座標
		//	test.m_hitPointWorld;
		//}

		auto cameraObj = CameraComponent::getMainCamera().lock()->getGameObj();
		m_PlayerData->pCameraController = cameraObj.lock()->getComponent<CameraController>();
		m_InvincibleTimer.init();
		m_IsHit = false;

	}

	void PlayerComponent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath });

		(*componentInitalizer).emplace_back(m_pUI);
	}

	const PlayerState & PlayerComponent::getSate()
	{
		return m_PlayerData->currentState;
	}

	void PlayerComponent::setParam(const std::vector<std::string>& param)
	{
		util::CSVLoader charaMotion(param[0]);
		auto motionDesc = charaMotion.load();

		//!csvコメント部分削除
		motionDesc.erase(motionDesc.begin());

		for (auto desc : motionDesc) {
			m_PlayerData->motionNames[m_PlayerData->convert(desc[0])] = desc[1];
		}

		m_LuaPath = param[1];
	}

	void PlayerComponent::update()
	{
#ifdef _MDEBUG
		m_PlayerData->bendFriction = m_pUI.lock()->getSlider("Friction");
		m_PlayerData->speed = m_pUI.lock()->getSlider("Speed");
		m_PlayerData->setupRotationSpeed = m_pUI.lock()->getSlider("SetUpRotationSpeed");
		m_PlayerData->setupMoveSpeed = m_pUI.lock()->getSlider("SetUpMoveSpeed");
		m_PlayerData->jumpFrontForce = m_pUI.lock()->getSlider("JumpFrontForce");
		m_PlayerData->jumpUpForce = m_pUI.lock()->getSlider("JumpUpForce");
#endif

		m_States = m_States->update(this);

		auto trans = m_Entity.lock()->getTransform();
		trans->m_Rotation.x = 0;
		trans->m_Rotation.z = 0;

		auto rate = m_PlayerData->hp / (float)maxHP;

		m_pHPGauge.lock()->rightScaling(rate);

		if (m_PlayerData->hitDamage) {
			m_DamageTimer.update();

			if (m_DamageTimer.isEnd()) {
				m_DamageTimer.init();
				m_PlayerData->hitDamage = false;
			}
		}

		rate = m_PlayerData->pPoisonTank->rate();
		m_pPoisonGauge1.lock()->rightScaling(rate);
		m_pPoisonGauge2.lock()->rightScaling(rate);

		if (m_IsHit) {
			m_InvincibleTimer.update();
			if (m_InvincibleTimer.isEnd()) {
				m_InvincibleTimer.init();
				m_IsHit = false;
			}
		}

		m_PlayerData->isPreFloorContact = m_PlayerData->isFloorContact;
		//毎フレームフィジックスアップデートで地面に触れているか確認するためにアップデートの最後でfalseにする
		m_PlayerData->isFloorContact = false;

	}

	std::weak_ptr<PlayerData> PlayerComponent::getPlayerData()
	{
		return m_PlayerData;
	}

	void PlayerComponent::onCollisionEnter(const framework::HitData& other)
	{
		if (m_PlayerData->currentState == PlayerState::die)return;

		if (other.other.expired())return;

		//なにかにあたったら連続で当たらないように無敵時間を作る
		if (m_IsHit)return;

		if (other.other.lock()->getTag() == "EnemyAttack") {
			if (!m_PlayerData->hitDamage) {
				m_PlayerData->hp -= 15;
				auto dir = m_Entity.lock()->getTransform()->m_Position - other.other.lock()->getTransform()->m_Position;
				dir = dir.normalize();
				m_PlayerData->pRigidBody.lock()->addForce(dir * 1000);

				std::shared_ptr<PlayerSetUpState> stanState = std::dynamic_pointer_cast<PlayerSetUpState>(m_States);
				if (nullptr != stanState) {
					stanState->deleteEffect();
				}

				m_States = std::make_shared<PlayerDamageState>(HitEntity::Enemy);
				m_pAttackComponent.lock()->offCollision();
			}
		}

		if (other.other.lock()->getTag() == "PoisonCollision") {
			if (!m_PlayerData->hitDamage) {
				m_PlayerData->hp -= 15;
				auto trans = m_Entity.lock()->getTransform();
				auto dir = trans->m_Position - other.other.lock()->getTransform()->m_Position;
				dir = -dir.normalize();
				//m_PlayerData->pRigidBody.lock()->addForce(dir * 1000);

				std::shared_ptr<PlayerSetUpState> stanState = std::dynamic_pointer_cast<PlayerSetUpState>(m_States);
				if (nullptr != stanState) {
					stanState->deleteEffect();
				}

				//爆発のあった方向を向く
				trans->lookAt(dir);
				m_States = std::make_shared<PlayerDamageState>(HitEntity::PoisonCollision);
				m_pAttackComponent.lock()->offCollision();
				m_IsHit = true;
			}
		}
		//自分以外の攻撃コリジョンに触れたとき
		else if (other.other.lock()->getTag() == "Attack" && other.other.lock()->getName() != "Player1Attack") {
			if (!m_PlayerData->hitDamage) {
				m_PlayerData->hp -= 5;

				std::shared_ptr<PlayerSetUpState> stanState = std::dynamic_pointer_cast<PlayerSetUpState>(m_States);
				if (nullptr != stanState) {
					stanState->deleteEffect();
				}
				m_States = std::make_shared<PlayerDamageState>(HitEntity::Enemy);
				m_pAttackComponent.lock()->offCollision();
				m_IsHit = true;
			}
		}
	}

	void PlayerComponent::onCollisionStay(const framework::HitData & other)
	{

		if (isFloor(other)) {
			m_PlayerData->isFloorContact = true;
		}

	}

	void PlayerComponent::activeCollision()
	{
		m_pAttackComponent.lock()->onCollision();
	}

	void PlayerComponent::deActiveCollision()
	{
		m_pAttackComponent.lock()->offCollision();
	}

	void PlayerComponent::damage()
	{
		m_PlayerData->hp -= 1;
		m_Entity.lock()->onEvent("Damage");
	}

	void PlayerComponent::damageSE()
	{
		m_Entity.lock()->onEvent("PlayerDamage");
	}

	void PlayerComponent::attack()
	{
		m_Entity.lock()->onEvent("PlayerAttack");
	}

	bool PlayerComponent::isDead()
	{
		return m_PlayerData->hp <= 0;
	}

	bool PlayerComponent::isJumpTrriger()
	{
		return  !m_PlayerData->isFloorContact && m_PlayerData->isPreFloorContact;
	}

	bool PlayerComponent::isLandingTrriger()
	{
		return  m_PlayerData->isFloorContact && !m_PlayerData->isPreFloorContact;
	}

	void PlayerComponent::hit()
	{
		m_IsHit = true;
	}

	void PlayerComponent::changeIdle()
	{
		m_States = std::make_shared<PlayerIdleState>();
	}

	void PlayerComponent::pauseDeActive()
	{
		m_Entity.lock()->getComponent<AnimatorComponent>().lock()->onStop();
	}

	void PlayerComponent::pauseActive()
	{
		m_Entity.lock()->getComponent<AnimatorComponent>().lock()->onStart();
	}

	bool PlayerComponent::isBorer()
	{
		auto trans = m_Entity.lock()->getTransform();
		//return abs(trans->m_Position.x) >= component::PlayerComponent::BORDER || abs(trans->m_Position.y) >= component::PlayerComponent::BORDER;
		return abs(trans->m_Position.y) >= component::PlayerComponent::BORDER;
	}

	bool PlayerComponent::isWall(const framework::HitData & other)
	{
		return false;
	}

	bool PlayerComponent::isFloor(const framework::HitData & other)
	{
	//	auto trans = m_Entity.lock()->getTransform();
	//	auto&& pos = trans->m_Position;
		//auto hit = m_pWorld.lock()->rayCast(other.hitPosA, other.hitPosB);

		//if (!hit.hasHit()) {
		//	return false;
		//}

		//auto normal = util::convertUtVector3(hit.m_hitNormalWorld);
		//auto d = normal.dot(util::Vec3(0, -1, 0));

		//return d >= 0.3f;

		/*auto vec = other.hitPosA - other.hitPosB;
		vec = vec.normalize();
*/

		

//上方向と比べる
		auto hitVec = other.hitPosB;
		hitVec = hitVec.normalize();
		d = hitVec.dot(util::Vec3(0, 1, 0).normalize());

		//ある程度斜めまでは地面と判断する
		bool isUp = d >= 0.4f;

		//ある程度斜めまでは地面と判断する
		bool isDown = (-d) >= 0.4f;

		//ノーマルが反転してる場合があるので上か下にほぼ一致していれば地面とみなす
		if (isUp || isDown) {
			return true;
		}
		else {
			auto trans = m_Entity.lock()->getTransform();
			auto&& pos = trans->m_Position;

			auto ray = m_pWorld.lock()->rayCast(pos, pos - (trans->up() * 50));
			if (ray.hasHit()) {
				return true;
			}
			else {
				return false;
			}
		}
	}
}


