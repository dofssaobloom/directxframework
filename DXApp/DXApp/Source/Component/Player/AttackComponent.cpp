#include"AttackComponent.h"
#include<Source\Component\Physics\CircleCollider.h>
#include<math.h>
#include<Source\Application\Device\Input.h>

namespace component {

	AttackComponent::AttackComponent()
	{
		m_CallOrder = 3;
	}

	AttackComponent::~AttackComponent()
	{
	}

	void AttackComponent::onConect()
	{
		m_pMoveTimer = std::make_unique<util::Timer>(5);
	}

	void AttackComponent::init()
	{
		m_FrontOffset = m_pUI.lock()->getSlider("FrontOffset");
		short time = m_PreChageTime = m_pUI.lock()->getSlider("ChageTime");
		m_MinPower = m_pUI.lock()->getSlider("MinAttackPower");
		m_MaxPower = m_pUI.lock()->getSlider("MaxAttackPower");

		m_pChangeTime = std::make_unique<util::Timer>(time);

		m_pRigidBody.lock()->setTrriger(true);
		m_pRigidBody.lock()->deActive();

		m_IsFornt = true;
	}


	void AttackComponent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath ,  m_AttackLuaPath });

		(*componentInitalizer).emplace_back(m_pUI);

		m_pAttackEntity = framework::Entity::createEntity(m_Entity.lock()->getName() + "Attack", "Attack", util::Transform(m_Entity.lock()->getTransform()->m_Position, util::Vec3(), util::Vec3(1, 1, 1)));
		m_pAttackEntity.lock()->setParent(m_Entity);
		m_pRigidBody = m_pAttackEntity.lock()->addComponent<BulletRigidBody>(componentInitalizer, { "Resource/Script/Lua/UI/RigidBody.lua" ,""});
		auto sphere = m_pAttackEntity.lock()->addComponent<BulletSphereCollider>(componentInitalizer, { "Resource/Script/Lua/UI/SphereCollider.lua",m_CollisionPath });
		(*componentInitalizer).emplace_back(m_pRigidBody);
		(*componentInitalizer).emplace_back(sphere);
	}


	void AttackComponent::update()
	{
#ifdef _MDEBUG
		m_FrontOffset = m_pUI.lock()->getSlider("FrontOffset");
		short time = m_pUI.lock()->getSlider("ChageTime");
		m_MinPower = m_PreChageTime = m_pUI.lock()->getSlider("MinAttackPower");
		m_MaxPower = m_PreChageTime = m_pUI.lock()->getSlider("MaxAttackPower");

		if (m_PreChageTime != time) {
			m_pChangeTime = std::make_unique<util::Timer>(time);
			m_PreChageTime = time;
		}
#endif
		if (m_pRigidBody.lock()->isActive()) {
			m_pMoveTimer->update();
		}

		if (application::Input::isAttack()) {
			m_pChangeTime->init();
		}

		if (application::Input::isAttackChage()) {
			m_pChangeTime->update();
		}

		auto myTrans = m_Entity.lock()->getTransform();

	
		short sign = m_IsFornt ? 1 : -1;

		//前か後ろでベクトルを反転させる
		auto front = myTrans->front() * sign;
		auto left = myTrans->left();
		front = front * m_FrontOffset * (1.0f - m_pMoveTimer->rate());
		m_pAttackEntity.lock()->getTransform()->m_Position = myTrans->m_Position + front;
	}

	void AttackComponent::setParam(const std::vector<std::string>& param)
	{
		m_LuaPath = param[0];

		if (param.size() >= 2) {
			m_CollisionPath = param[1];
		}
		if (param.size() >= 3) {
			m_AttackLuaPath = param[2];
		}
	}

	bool AttackComponent::isEffectEnd()
	{
		return false;
	}

	void AttackComponent::onCollision()
	{
		m_pMoveTimer->init();
		m_pRigidBody.lock()->active();
	}

	void AttackComponent::offCollision()
	{
		m_pRigidBody.lock()->deActive();
	}

	float AttackComponent::attackPower()
	{
		return util::lerp<float>(1.0f - m_pChangeTime->rate(), m_MinPower, m_MaxPower) * 2;
	}

	const std::string& AttackComponent::getCollisionName()
	{
		return m_pAttackEntity.lock()->getName();
	}

	void AttackComponent::setTime(short time)
	{
		m_pMoveTimer = std::make_unique<util::Timer> (time);
	}

	void AttackComponent::setFrontFlag(bool isFront)
	{
		m_IsFornt = isFront;
	}

	float AttackComponent::moveRate()
	{
		return 1.0f - m_pMoveTimer->rate();
	}



}