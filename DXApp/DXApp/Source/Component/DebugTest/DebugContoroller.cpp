#include "DebugContoroller.h"
#include<Source\Device\Input\Input.h>
#include<Source\Entity\Entity.h>
#include<math.h>
#include<Source\Util\Win\WinFunc.h>

namespace component {

	DebugContoroller::DebugContoroller()
	{
	}

	DebugContoroller::~DebugContoroller()
	{
		
	}

	void DebugContoroller::init()
	{
		m_Animator = m_Entity.lock()->getComponent<AnimatorComponent>();
	}

	void DebugContoroller::update()
	{
		using namespace framework;
		const float speed = 20.0f;

		auto leftStick = Input::getGamePad().getLeftStick();

		if (leftStick.x >= Input::getGamePad().m_MaxHalfStickNum || Input::getKey().isKeyDown(KeyBoard::KeyCords::RIGHT)) {
			auto trans = m_Entity.lock()->getTransform();
			trans->m_Position.x += speed;
		}
		if (leftStick.x <= -Input::getGamePad().m_MaxHalfStickNum || Input::getKey().isKeyDown(KeyBoard::KeyCords::LEFT)) {
			auto trans = m_Entity.lock()->getTransform();
			trans->m_Position.x -= speed;
		}
		if (leftStick.y >= Input::getGamePad().m_MaxHalfStickNum&&
			!Input::getGamePad().getButton(ButtonCode::RightShouler) || Input::getKey().isKeyDown(KeyBoard::KeyCords::UP) && Input::getKey().isKeyDown(KeyBoard::KeyCords::LSHIFT)) {
			auto trans = m_Entity.lock()->getTransform();
			trans->m_Position.y += speed;
		}
		if (leftStick.y <= -Input::getGamePad().m_MaxHalfStickNum&&
			!Input::getGamePad().getButton(ButtonCode::RightShouler)|| Input::getKey().isKeyDown(KeyBoard::KeyCords::DOWN) && Input::getKey().isKeyDown(KeyBoard::KeyCords::LSHIFT)) {
			auto trans = m_Entity.lock()->getTransform();
			trans->m_Position.y -= speed;
		}

		if (leftStick.y >= Input::getGamePad().m_MaxHalfStickNum&&
			Input::getGamePad().getButton(ButtonCode::RightShouler) || Input::getKey().isKeyDown(KeyBoard::KeyCords::DOWN)) {
			auto trans = m_Entity.lock()->getTransform();
			trans->m_Position.z -= speed;
		}

		if (leftStick.y <= -Input::getGamePad().m_MaxHalfStickNum&&
			Input::getGamePad().getButton(ButtonCode::RightShouler) || Input::getKey().isKeyDown(KeyBoard::KeyCords::UP)) {
			auto trans = m_Entity.lock()->getTransform();
			trans->m_Position.z += speed;
		}

		//auto rightStick = Input::getGamePad().getRightStick();

		//static float rotate = 0;

		//if (rightStick.x >= Input::getGamePad().m_MaxHalfStickNum || Input::getKey().isKeyDown(KeyBoard::KeyCords::RIGHT)) {
		//	rotate += 1;
		//}
		//if (rightStick.x <= -Input::getGamePad().m_MaxHalfStickNum || Input::getKey().isKeyDown(KeyBoard::KeyCords::LEFT)) {
		//	rotate -= 1;
		//}

		//auto trans = m_Entity.lock()->getTransform();
		//auto a = 
		//trans->m_Position.x = cos(XMConvertToRadians(rotate)) * 550;
		//trans->m_Position.z = sin(XMConvertToRadians(rotate)) * 550;

	/*	if (Input::getGamePad().getButton(ButtonCode::B)) {
			if (!m_Animator.expired()) {
				if (m_Animator.lock()->isEnd()) {
					m_Animator.lock()->changeMotion("Nail_Attack");
					m_Animator.lock()->onNotLoop();
				}
			}
		}
*/

	}
}
