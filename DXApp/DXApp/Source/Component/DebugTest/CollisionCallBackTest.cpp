#include "CollisionCallBackTest.h"

using namespace framework;

namespace component {


	CollisionCallBackTest::CollisionCallBackTest()
	{
	}

	CollisionCallBackTest::~CollisionCallBackTest()
	{
	}

	void CollisionCallBackTest::onCollisionEnter(const framework::HitData& other)
	{
		util::WinFunc::outLog("OnEnter");
	}

	void CollisionCallBackTest::onCollisionStay(const std::weak_ptr<Entity>& other)
	{
		util::WinFunc::outLog("OnStay");
	}

	void CollisionCallBackTest::onCollisionExit(const std::weak_ptr<Entity>& other)
	{
		util::WinFunc::outLog("OnExit");
	}

}