#pragma once
#include<Framework.h>

namespace component {

	class CollisionCallBackTest : public framework::Component
	{
	public:
		CollisionCallBackTest();
		~CollisionCallBackTest();

		/**
		* @brief		衝突したときの処理
		* @param other	衝突した相手オブジェクト
		*/
		virtual void onCollisionEnter(const framework::HitData& other);

		/**
		* @brief		衝突している間の処理
		* @param other	衝突している相手オブジェクト
		*/
		virtual void onCollisionStay(const std::weak_ptr<framework::Entity>& other);

		/**
		* @brief		衝突から抜けた時の処理
		* @param other	衝突していた相手オブジェクト
		*/
		virtual void onCollisionExit(const std::weak_ptr<framework::Entity>& other);

	private:

	};


}