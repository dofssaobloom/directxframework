#include "ConfigScene.h"
#include<Source\Util\Math\Math.h>
#include<Source\Component\Util\Counter.h>
#include<Source\Component\Scene\GameMain\ScoreComponent.h>
#include<Source\Component\Scene\GameMain\GameTimerCompornent.h>
#include<Source\Component\Scene\GameMain\GameTimerCompornent.h>
#include<Source\Application\Scene\SceneDelivery.h>
#include<Source\Component\SceneChangeComponent.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Application\Device\Input.h>
#include<Source\Component\FadeComponent.h>
#include<Source\Util\IO\BinaryWriter.h>
#include<Source\Util\IO\BinaryLoader.h>
#include<Source\Application\Screen\Screen.h>
#include<Source\Task\TaskManager.h>
#include<Source\Resource\ResourceManager.h>

#define CHANGE_WINDOWSIZE 0

namespace component {
	ConfigScene::ConfigScene()
		:m_longInputCategoryTimer(5), m_longInputItemTimer(5), m_SpeedUpCategoryTimer(2), 
		m_SpeedUpItemTimer(2),m_ApllySelect(2),m_Category(6), m_GraphicsSelect(3),m_WindowSelect(3), m_CameraSelect(2), m_ItemSelect(2),
		m_frashTimer(10)
	{
	}

	ConfigScene::~ConfigScene()
	{
	}

	void ConfigScene::init()
	{
		m_pSceneChange = framework::Entity::findGameObj("GlobalEvent").lock()->getComponent<SceneChangeComponent>();
		m_Fade = framework::Entity::findGameObj("Fade").lock()->getComponent<FadeComponent>();
		m_FullScreenPaint = framework::Entity::findGameObj("PaintFullScreen").lock()->getComponent<SpriteRenderComponent>();
		m_FullScreenCheck = framework::Entity::findGameObj("CheckFullScreen").lock()->getComponent<SpriteRenderComponent>();

		m_PaintEntity = framework::Entity::findGameObj("CheckPaint");
		m_Paint = m_PaintEntity.lock()->getComponent<SpriteRenderComponent>();
		m_GraphicsPaintEntity = framework::Entity::findGameObj("GraphicsPaint");
		m_GraphicsPaint = m_GraphicsPaintEntity.lock()->getComponent<SpriteRenderComponent>();
		m_CameraPaintEntity = framework::Entity::findGameObj("CameraPaint");
		m_CameraPaint = m_CameraPaintEntity.lock()->getComponent<SpriteRenderComponent>();
		m_WindowPaintEntity = framework::Entity::findGameObj("WindowPaint");
		m_WindowPaint = m_WindowPaintEntity.lock()->getComponent<SpriteRenderComponent>();

		m_ApplyEntity = framework::Entity::findGameObj("Apply");
		m_CancelEntity = framework::Entity::findGameObj("Cancel");
		m_Apply = m_ApplyEntity.lock()->getComponent<SpriteRenderComponent>();
		m_Cancel = m_CancelEntity.lock()->getComponent<SpriteRenderComponent>();

		m_WindowCheckEntity = framework::Entity::findGameObj("MiddleCheck");
		m_MiddleCheck = framework::Entity::findGameObj("MiddleCheck").lock()->getComponent<SpriteRenderComponent>();
		m_XReverseCheck = framework::Entity::findGameObj("ReverseXCheck").lock()->getComponent<SpriteRenderComponent>();
		m_YReverseCheck = framework::Entity::findGameObj("ReverseYCheck").lock()->getComponent<SpriteRenderComponent>();
		m_BlurCheck = framework::Entity::findGameObj("BlurCheck").lock()->getComponent<SpriteRenderComponent>();
		m_DOFCheck = framework::Entity::findGameObj("DOFCheck").lock()->getComponent<SpriteRenderComponent>();
		m_BloomCheck = framework::Entity::findGameObj("BloomCheck").lock()->getComponent<SpriteRenderComponent>();

		m_Entity.lock()->addEvent("Entry", [&]() {
			framework::ResourceManager::getInstance()->playSound("Entry");
			framework::ResourceManager::getInstance()->setVolume("Entry", -1200);
			framework::ResourceManager::getInstance()->setDoppler("Entry", 0);
		});
		m_Entity.lock()->addEvent("Carsol", [&]() {
			framework::ResourceManager::getInstance()->playSound("CarsolSelect");
			framework::ResourceManager::getInstance()->setVolume("CarsolSelect", -1200);
			framework::ResourceManager::getInstance()->setDoppler("CarsolSelect", 0);
		});
		m_Entity.lock()->addEvent("Check", [&]() {
			framework::ResourceManager::getInstance()->playSound("Check");
			framework::ResourceManager::getInstance()->setVolume("Check", -500);
			framework::ResourceManager::getInstance()->setDoppler("Check", 0);
		});

		byteLoad();
		m_Fade.lock()->onStart();
		m_longInputCategoryTimer.init();
		m_SpeedUpCategoryTimer.init();
		m_longInputItemTimer.init();
		m_SpeedUpItemTimer.init();
		m_ItemSelect.init();
		m_frashTimer.init();

		m_PaintEntity.lock()->getTransform()->m_Position.y = -5;
		m_WindowPaintEntity.lock()->getTransform()->m_Position.y = 83;
		m_GraphicsPaintEntity.lock()->getTransform()->m_Position.y = 188;
		m_CameraPaintEntity.lock()->getTransform()->m_Position.y = 301;
		m_isCanselFrash = false;
		m_isApplyFrash = false;
		m_isUpZero = true;
		m_isDownZero = true;
		m_isRightZero = true;
		m_isLeftZero = true;
	}

	void ConfigScene::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
	}

	void ConfigScene::update()
	{
		if (m_Fade.lock()->isEnd()) {
			selectCategory();
			auto select = (CATEGORY)m_Category.getSelectNum();

			//item選択なら
			if (m_IsCategorySelect) {
				selectUI(select);
			}
		}
		else {
			m_GraphicsPaint.lock()->setAlpha(0.5f);
			m_CameraPaint.lock()->setAlpha(0.5f);
			m_WindowPaint.lock()->setAlpha(0.5f);
		}

		//各項目のペイント位置
		m_WindowPaintEntity.lock()->getTransform()->m_Position.x = m_WindowGraphicsPos[m_WindowSelect.getSelectNum()];
		m_GraphicsPaintEntity.lock()->getTransform()->m_Position.x = m_WindowGraphicsPos[m_GraphicsSelect.getSelectNum()];
		m_CameraPaintEntity.lock()->getTransform()->m_Position.x = m_cameraPos[m_CameraSelect.getSelectNum()];
		m_Flags.graphicPaint = m_GraphicsSelect.getSelectNum();
		m_Flags.cameraPaint = m_CameraSelect.getSelectNum();

		//解像度のチェック位置
		if (m_Flags.isSmall) {
			m_WindowCheckEntity.lock()->getTransform()->m_Position.x = m_checkPos[0];
		}
		else if (m_Flags.isMiddle) {
			m_WindowCheckEntity.lock()->getTransform()->m_Position.x = m_checkPos[1];
		}
		else if (m_Flags.isBig) {
			m_WindowCheckEntity.lock()->getTransform()->m_Position.x = m_checkPos[2];
		}

		setAlpha();
		framework::DirectXInstance::getInstance()->changeWindowMode(m_Flags.isFullScreen);
	}

	void ConfigScene::setParam(const std::vector<std::string>& param)
	{
	}

	void ConfigScene::byteWrite()
	{
		util::BinaryWriter writer("Resource/GameConfig.byte", util::FileType::newFile);
		std::vector<float> writeData;
		writeData.emplace_back(m_Flags.isBloom);
		writeData.emplace_back(m_Flags.isDOF);
		writeData.emplace_back(m_Flags.isBlur);
		writeData.emplace_back(m_Flags.isCameraXConfig);
		writeData.emplace_back(m_Flags.isCameraYConfig);
		writeData.emplace_back(m_Flags.isFullScreen);
		writeData.emplace_back(m_Flags.isSmall);
		writeData.emplace_back(m_Flags.isMiddle);
		writeData.emplace_back(m_Flags.isBig);
		writeData.emplace_back(m_Flags.cameraPaint);
		writeData.emplace_back(m_Flags.graphicPaint);
		writer.write(writeData);
	}

	void ConfigScene::byteLoad()
	{
		util::BinaryLoader loader("Resource/GameConfig.byte");
		std::vector<float> result;
		loader.load<float>(&result);
		if (!result.empty()) {
			m_Flags.isBloom = result[0];
			m_Flags.isDOF = result[1];
			m_Flags.isBlur = result[2];
			m_Flags.isCameraXConfig = result[3];
			m_Flags.isCameraYConfig = result[4];
			m_Flags.isFullScreen = result[5];
			m_Flags.isSmall = result[6];
			m_Flags.isMiddle = result[7];
			m_Flags.isBig = result[8];
			m_Flags.cameraPaint = result[9];
			m_Flags.graphicPaint = result[10];
		}

		//解像度のチェック位置にペイントを合わせる
		if (m_Flags.isSmall) {
			screenSize(640, 360);
			m_WindowSelect.init(0);
		}
		else if (m_Flags.isMiddle) {
			screenSize(1280, 720);
			m_WindowSelect.init(1);
		}
		else if (m_Flags.isBig) {
			screenSize(1920, 1080);
			m_WindowSelect.init(2);
		}

		//ペイントの初期位置
		m_GraphicsSelect.init(m_Flags.graphicPaint);
		m_CameraSelect.init(m_Flags.cameraPaint);
	}

	void ConfigScene::selectUI(CATEGORY select)
	{
		switch (select)
		{
		case CATEGORY::FULL_SCREEN:
			selectFullScreen();
			break;
		case CATEGORY::SCREEN_SELECT:
			selectWindow();
			break;
		case CATEGORY::GRAPHUICS_SELECT:
			selectGraphics();
			break;
		case CATEGORY::CAMERA_SELECT:
			selectCamera();
			break;
		case CATEGORY::CANCEL_SELECT:
			break;
		case CATEGORY::APPLY_SELECT:
			break;

		default:
			assert("!不正なアクセス");
			break;
		}
	}

	void ConfigScene::selectCategory()
	{
		auto left = application::Input::leftVelocity();
		auto isLeftUp = application::Input::leftUpTrigger();
		auto isLeftDown = application::Input::leftDownTrigger();
		m_GraphicsPaint.lock()->setAlpha(0.5f);
		m_CameraPaint.lock()->setAlpha(0.5f);
		m_WindowPaint.lock()->setAlpha(0.5f);

		//Category選択なら上下移動が可能
		if (!m_IsCategorySelect) {
			if (m_longInputCategoryTimer.isEnd()) {
				m_SpeedUpCategoryTimer.update();
				//速いカーソル移動
				if (m_SpeedUpCategoryTimer.isEnd() && left.y < 0)
				{
					m_Entity.lock()->onEvent("Carsol");
					m_Category.enter();
					m_SpeedUpCategoryTimer.init();
				}
				if (m_SpeedUpCategoryTimer.isEnd() && left.y > 0)
				{
					m_Entity.lock()->onEvent("Carsol");
					m_Category.back();
					m_SpeedUpCategoryTimer.init();
				}
				else if (left.y == 0) {
					m_longInputCategoryTimer.init();
				}
			}
			else {
				//長押し
				if (left.y < 0 || left.y > 0) {
					m_longInputCategoryTimer.update();
				}
			}
			
			//普通のカーゾル速度
			if (m_isDownZero && isLeftDown)
			{
				m_longInputCategoryTimer.init();
				m_isDownZero = false;
				m_Entity.lock()->onEvent("Carsol");
				m_Category.enter();
			}

			if (m_isUpZero && isLeftUp)
			{
				m_longInputCategoryTimer.init();
				m_isUpZero = false;
				m_Entity.lock()->onEvent("Carsol");
				m_Category.back();
			}
			if (!isLeftUp) {
				m_isUpZero = true;
			}
			if (!isLeftDown) {
				m_isDownZero = true;
			}
		}

		bool isNotItemSelect;
		if (m_Category.getSelectNum() == 4) {
			//Cancel
			m_Paint.lock()->setAlpha(0);
			m_ApplyEntity.lock()->getTransform()->scaling(0.5f);
			m_CancelEntity.lock()->getTransform()->scaling(0.6f);
			isNotItemSelect = false;
			if (application::Input::isJump()) {
				m_frashTimer.init();
				m_isCanselFrash = true;
				m_Entity.lock()->onEvent("Entry");
				byteLoad();
			}
		}
		else if (m_Category.getSelectNum() == 5) {
			//Applay
			m_Paint.lock()->setAlpha(0);
			m_ApplyEntity.lock()->getTransform()->scaling(0.6f);
			m_CancelEntity.lock()->getTransform()->scaling(0.5f);
			isNotItemSelect = false;
			nextScene("TitleScene");
		}
		else if (m_Category.getSelectNum() != 4 && m_Category.getSelectNum() != 5) {
			//ApplayもCancelも選択されていない
			m_Paint.lock()->setAlpha(1);
			m_ApplyEntity.lock()->getTransform()->scaling(0.5f);
			m_CancelEntity.lock()->getTransform()->scaling(0.5f);
			isNotItemSelect = true;
		}

		//ペイントの移動
		int pos[4] = {-5, 83, 188, 301};
		m_PaintEntity.lock()->getTransform()->m_Position.y = pos[m_Category.getSelectNum()];

		//項目の選択に行けるかどうか
		if (isNotItemSelect)
			changeCategorySelect(true);
	}

	void ConfigScene::selectFullScreen()
	{
		m_IsCategorySelect = false;
		m_PaintEntity.lock()->getTransform()->m_Position.y = -5;
		//選択
		if (application::Input::isJump())
		{
			m_Entity.lock()->onEvent("Check");
			m_Flags.isFullScreen = !m_Flags.isFullScreen;
		}
	}

	void ConfigScene::selectWindow()
	{
		m_Paint.lock()->setAlpha(0.5f);
		m_WindowPaint.lock()->setAlpha(1);
		m_ItemSelect.update();

		//選択
		if (m_ItemSelect.isEnd() && application::Input::isJump()) {
			if (m_WindowSelect.getSelectNum() == 0) {
				if (!m_Flags.isSmall) {
					m_Entity.lock()->onEvent("Check");
				}
				screenSize(640, 360);
				m_Flags.isSmall = true;
				m_Flags.isMiddle = m_Flags.isBig = false;
			}
			else if (m_WindowSelect.getSelectNum() == 1) {
				if (!m_Flags.isMiddle) {
					m_Entity.lock()->onEvent("Check");
				}
				screenSize(1280, 720);
				m_Flags.isSmall = m_Flags.isBig = false;
				m_Flags.isMiddle = true;
			}
			else if (m_WindowSelect.getSelectNum() == 2) {
				if (!m_Flags.isBig) {
					m_Entity.lock()->onEvent("Check");
				}
				screenSize(1920, 1080);
				m_Flags.isSmall = m_Flags.isMiddle = false;
				m_Flags.isBig = true;
			}
		}

		sideMoveSelecter(m_WindowSelect);
		changeCategorySelect(false);
	}

	void ConfigScene::selectGraphics()
	{
		m_Paint.lock()->setAlpha(0.5f);
		m_GraphicsPaint.lock()->setAlpha(1);
		m_ItemSelect.update();

		//選択
		if (m_ItemSelect.isEnd() && application::Input::isJump()) {
			m_Entity.lock()->onEvent("Check");

			if (m_GraphicsSelect.getSelectNum() == 0) {
				m_Flags.isDOF = !m_Flags.isDOF;
			}
			else if (m_GraphicsSelect.getSelectNum() == 1) {
				m_Flags.isBloom = !m_Flags.isBloom;
			}
			else if (m_GraphicsSelect.getSelectNum() == 2) {
				m_Flags.isBlur = !m_Flags.isBlur;
			}
		}
		sideMoveSelecter(m_GraphicsSelect);
		changeCategorySelect(false);
	}

	void ConfigScene::selectCamera()
	{
		m_Paint.lock()->setAlpha(0.5f);
		m_CameraPaint.lock()->setAlpha(1);
		m_ItemSelect.update();

		//選択
		if (m_ItemSelect.isEnd() && application::Input::isJump()) {
			m_Entity.lock()->onEvent("Check");
			if (m_CameraSelect.getSelectNum() == 0) {
				m_Flags.isCameraXConfig = !m_Flags.isCameraXConfig;
			}
			else if (m_CameraSelect.getSelectNum() == 1) {
				m_Flags.isCameraYConfig = !m_Flags.isCameraYConfig;
			}
		}
		sideMoveSelecter(m_CameraSelect);
		changeCategorySelect(false);
	}
	
	void ConfigScene::screenSize(int width, int height)
	{
		framework::TaskManager::getInstance()->resizeBuffer(util::Vec2(width,height));
	}

	void ConfigScene::nextScene(std::string sceneName)
	{
		if (application::Input::isJump()) {
			m_frashTimer.init();
			m_isApplyFrash = true;
			m_Entity.lock()->onEvent("Entry");
			m_Fade.lock()->onStart();
		}

		if (m_Fade.lock()->isEnd() && m_Fade.lock()->isOutFade()) {
			byteWrite();
			framework::Scene::m_NextSceneName = sceneName;
			m_pSceneChange.lock()->changeSceneNotification();
		}
	}

	void ConfigScene::setAlpha()
	{
		m_FullScreenCheck.lock()->setAlpha(m_Flags.isFullScreen);
		m_DOFCheck.lock()->setAlpha(m_Flags.isDOF);
		m_BloomCheck.lock()->setAlpha(m_Flags.isBloom);
		m_BlurCheck.lock()->setAlpha(m_Flags.isBlur);
		m_XReverseCheck.lock()->setAlpha(m_Flags.isCameraXConfig);
		m_YReverseCheck.lock()->setAlpha(m_Flags.isCameraYConfig);

		//Cancelの点滅演出
		if (m_isCanselFrash) {
			m_frashTimer.update();
			auto alphaRate = m_frashTimer.rate();
			auto alpha = util::bezierCurve(alphaRate, 1.0f, 0.0f, 1.0f);
			m_Cancel.lock()->setAlpha(alpha);
		}

		//Applyの点滅演出
		if (m_isApplyFrash) {
			m_frashTimer.update();
			auto alphaRate = m_frashTimer.rate();
			auto alpha = util::bezierCurve(alphaRate, 1.0f, 0.0f, 1.0f);
			m_Apply.lock()->setAlpha(alpha);
		}

		if (m_frashTimer.isEnd()) {
			m_isCanselFrash = false;
			m_isApplyFrash = false;
		}
	}

	void ConfigScene::changeCategorySelect(bool change)
	{
		if (change && application::Input::isJump())
		{
			//trueならItem選択を行う
			m_IsCategorySelect = change;
		}
		else if (!change && application::Input::isAttack())
		{
			//falseならCategory選択に戻る
			m_IsCategorySelect = change;
			m_ItemSelect.init();
		}
	}

	void ConfigScene::sideMoveSelecter(util::Selector& select)
	{
		auto left = application::Input::leftVelocity();
		auto isLeftRight = application::Input::leftRightTrigger();
		auto isLeftLeft = application::Input::leftLeftTrigger();

		if (m_longInputItemTimer.isEnd()) {
			m_SpeedUpItemTimer.update();
			//速い速度
			if (m_SpeedUpItemTimer.isEnd() && left.x < 0)
			{
				m_Entity.lock()->onEvent("Carsol");
				select.back();
				m_SpeedUpItemTimer.init();
			}
			else if (m_SpeedUpItemTimer.isEnd() && left.x > 0)
			{
				m_Entity.lock()->onEvent("Carsol");
				select.enter();
				m_SpeedUpItemTimer.init();
			}
			else if (left.x == 0) {
				m_longInputItemTimer.init();
			}
		}
		else
		{
			//長押し
			if (left.x < 0 || left.x > 0) {
				m_longInputItemTimer.update();
			}
		}

		//普通の速度
		if (m_isLeftZero && isLeftLeft)
		{
			m_longInputItemTimer.init();
			m_isLeftZero = false;
			m_Entity.lock()->onEvent("Carsol");
			select.back();
			
		}
		else if (m_isRightZero &&isLeftRight)
		{
			m_longInputItemTimer.init();
			m_isRightZero = false;
			m_Entity.lock()->onEvent("Carsol");
			select.enter();
		}

		if (!isLeftRight) {
			m_isRightZero = true;
		}
		if (!isLeftLeft) {
			m_isLeftZero = true;
		}
	}
}
