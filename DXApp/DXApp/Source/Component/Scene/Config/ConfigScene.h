#pragma once
#include<Framework.h>
#include<vector>


enum class CATEGORY {
	FULL_SCREEN,
	SCREEN_SELECT,
	GRAPHUICS_SELECT,
	CAMERA_SELECT,
	APPLY_SELECT,
	CANCEL_SELECT,
};


namespace component {
	class SceneChangeComponent;
	class FadeComponent;

	class ConfigScene : public framework::UpdateComponent
	{
	public:
		ConfigScene();
		~ConfigScene();

		void init()override;

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

		virtual void setParam(const std::vector<std::string>& param);

	private:
		void byteWrite();
		void byteLoad();

		void selectUI(CATEGORY select);

		/// <summary>
		/// カテゴリ選択
		/// </summary>
		void selectCategory();

		/// <summary>
		/// フルスクリーン
		/// </summary>
		void selectFullScreen();

		/// <summary>
		/// 解像度選択
		/// </summary>
		void selectWindow();

		/// <summary>
		/// ポストエフェクト選択
		/// </summary>
		void selectGraphics();


		/// <summary>
		/// カメラ選択
		/// </summary>
		void selectCamera();

		/// <summary>
		/// スクリーンサイズの設定用
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		void screenSize(int width, int height);

		//次のシーン
		void nextScene(std::string sceneName);

		/// <summary>
		/// 透明度の変更
		/// </summary>
		void setAlpha();

		/// <summary>
		/// カテゴリの選択状態にするかどうか
		/// </summary>
		/// <param name="change"></param>
		void changeCategorySelect(bool change);

		/// <summary>
		/// item部分のペイント、x移動に関して
		/// </summary>
		/// <param name="select"></param>
		void sideMoveSelecter(util::Selector& select);

	private:
		struct Flags {
			Flags() {
				isBloom = false;
				isDOF = false;
				isBlur = false;
				isCameraXConfig = false;
				isCameraYConfig = false;
				isFullScreen = false;
				isSmall = false;
				isMiddle = false;
				isBig = false;
				cameraPaint = 0;
				graphicPaint = 0;
			}

			bool isBloom;
			bool isDOF;
			bool isBlur;
			bool isCameraXConfig;
			bool isCameraYConfig;
			bool isFullScreen;
			bool isSmall;
			bool isMiddle;
			bool isBig;
			int cameraPaint;
			int graphicPaint;
		};

	private:
		std::weak_ptr<SceneChangeComponent> m_pSceneChange;
		std::weak_ptr<FadeComponent> m_Fade;

		framework::WeakEntity m_PaintEntity;
		framework::WeakEntity m_WindowPaintEntity;
		framework::WeakEntity m_WindowCheckEntity;
		framework::WeakEntity m_GraphicsPaintEntity;
		framework::WeakEntity m_CameraPaintEntity;
		framework::WeakEntity m_ApplyEntity;
		framework::WeakEntity m_CancelEntity;

		std::weak_ptr<SpriteRenderComponent> m_Paint;
		std::weak_ptr<SpriteRenderComponent> m_FullScreenPaint;
		std::weak_ptr<SpriteRenderComponent> m_WindowPaint;
		std::weak_ptr<SpriteRenderComponent> m_GraphicsPaint;
		std::weak_ptr<SpriteRenderComponent> m_CameraPaint;
		std::weak_ptr<SpriteRenderComponent> m_FullScreenCheck;
		std::weak_ptr<SpriteRenderComponent> m_MiddleCheck;
		std::weak_ptr<SpriteRenderComponent> m_XReverseCheck;
		std::weak_ptr<SpriteRenderComponent> m_YReverseCheck;
		std::weak_ptr<SpriteRenderComponent> m_BlurCheck;
		std::weak_ptr<SpriteRenderComponent> m_DOFCheck;
		std::weak_ptr<SpriteRenderComponent> m_BloomCheck;
		std::weak_ptr<SpriteRenderComponent> m_Cancel;
		std::weak_ptr<SpriteRenderComponent> m_Apply;

		util::Timer m_longInputCategoryTimer;
		util::Timer m_longInputItemTimer;
		util::Timer m_SpeedUpCategoryTimer;
		util::Timer m_SpeedUpItemTimer;
		util::Timer m_frashTimer;

		//isEndでアイテムにチェックを付けることが出来るTimer
		util::Timer m_ItemSelect;

		util::Selector m_Category;
		util::Selector m_GraphicsSelect;
		util::Selector m_WindowSelect;
		util::Selector m_CameraSelect;
		util::Selector m_ApllySelect;

		Flags m_Flags;

		//!Aボタン押して項目選びに移ったか
		bool m_IsCategorySelect;
		bool m_isCanselFrash;
		bool m_isApplyFrash;
		const int m_Left = 350 - 40;
		const int m_Center = 625 - 40;
		const int m_Right = 900 - 40;
		const int m_WindowGraphicsPos[3] = { m_Left,m_Center,m_Right };

		const int m_checkPos[3] = { 335,605,890 };
		const int m_cameraPos[2] = { 350,730 };

		bool m_isUpZero;
		bool m_isDownZero;
		bool m_isRightZero;
		bool m_isLeftZero;
	};
}
