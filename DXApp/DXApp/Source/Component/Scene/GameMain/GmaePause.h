#pragma once
#include<Framework.h>
#include<Source\Util\MenuVector.h>

namespace component {

	class GradationEffect;
	class PlayerComponent;
	class EnemyComponent;
	class BossEnemyComponent;
	class EnemySpowner;
	class GameTimerCompornent;
	class PhysicsWorld;
	class Bullet;
	class EnemyBullet;
	class RulesComponent;
	class SpriteRenderComponent;
	class EnemyRulesComponent;
	class ComboComponent;

	/// <summary>
	/// ゲーム画面をポーズさせるコンポーネント
	/// </summary>
	class GamePause : public framework::UpdateComponent
	{
	public:
		GamePause();
		~GamePause();

		void init()override;

		/// <summary>
		/// ポーズOn
		/// </summary>
		void onPause();

		/// <summary>
		/// ポーズOff
		/// </summary>
		void offPause();

		// UpdateComponent を介して継承されました
		virtual void update() override;

	private:

		/// <summary>
		/// ポーズ中に止めるべきオブジェクトを止める
		/// </summary>
		void deActiveObject();

		/// <summary>
		/// ポーズ中止ってるゲームオブジェクトを有効にする
		/// </summary>
		void activeGameObject();

		void instanceGet();

		/// <summary>
		/// ポーズ中の処理
		/// </summary>
		void pauseAction();

		/// <summary>
		/// メニューのアクティブ化
		/// </summary>
		void menuActive();

		/// <summary>
		/// メニューの非アクティブ化
		/// </summary>
		void menuDeActive();

		void nextScene(std::string sceneName);

	private:
		std::weak_ptr<GradationEffect> m_pGradationEffect;

		std::weak_ptr<PlayerComponent> m_pPlayerComponent;

		std::weak_ptr<BossEnemyComponent> m_pBossEnemyComponent;
		
		std::weak_ptr<GameTimerCompornent> m_pGameTimer;

		std::weak_ptr<PhysicsWorld> m_pPhysicsWorld;

		std::weak_ptr<RulesComponent> m_pRules;

		std::weak_ptr<EnemyRulesComponent> m_pEnemyRules;

		std::weak_ptr<ComboComponent> m_Comb1;

		std::weak_ptr<ComboComponent> m_Comb2;

		std::list<std::weak_ptr<EnemyComponent>> m_pEnemyComponents;

		std::list<std::weak_ptr<EnemySpowner>> m_pSpownerComponents;	

		std::list<std::weak_ptr<Bullet>> m_pBulletComponents;

		std::list<std::weak_ptr<EnemyBullet>> m_pEnemyBulletComponents;

		std::weak_ptr<SceneChangeComponent> m_pSceneChange;

		framework::WeakEntity m_pPauseBG;
		framework::WeakEntity m_pRetryButton;
		framework::WeakEntity m_pTitleButton;
		framework::WeakEntity m_pCloseButton;
		
		//各EntityのCSVで保存されたYポジション保持する
		float m_larpPauseBG;
		float m_larpRetryYPos;
		float m_larpTitleYPos;
		float m_larpCloseYPos;

		bool m_selectFrashRetry;
		bool m_selectFrashTitle;
		bool m_selectFrashClose;

		//選択時セレクトとボタンの二度押し禁止用
		bool m_isSelect;

		std::weak_ptr<FadeComponent> m_Fade;

		std::vector<std::weak_ptr<SpriteRenderComponent>> m_pRenderers;

		//!ぼかしラープ用タイマー
		util::Timer m_GradationTimer;
		util::Timer m_frashIntarvalTimer;
		util::Timer m_frashTotalTimer;
		
		//トリガー判定用
		bool m_isUpZeroDown;
		bool m_isDownZeroDown;
		bool m_isWood;
		bool m_IsPause;
		bool m_isPauseEnd;
		util::MenuVector m_Manu;
	};
}