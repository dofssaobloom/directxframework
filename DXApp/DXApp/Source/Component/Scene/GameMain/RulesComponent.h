#pragma once
#include<Framework.h>
#include<list>

namespace component {
	class SceneChangeComponent;
	class SpriteRenderComponent;
	class PlayerComponent;
	class EnemySpowner;
	class KillCounter;
	class GameTimerCompornent;
	class FadeComponent;
	class CameraComponent;
	class EnemyRulesComponent;

	class RulesComponent : public framework::UpdateComponent
	{
	public:
		RulesComponent();
		~RulesComponent();

		void init()override;

		virtual void update() override;
	
		/// <summary>
		/// カウントダウンとその演出
		/// </summary>
		void countDown();

		/// <summary>
		/// GoとFinishのアルファ値変更
		/// </summary>
		void changeUIAlpha();

		/// <summary>
		/// 次のシーン(Result)に行く
		/// </summary>
		void changeScene();

		/// <summary>
		/// バイナリファイルの読み込み
		/// </summary>
		void byteLoad();

		/// <summary>
		///　ゲームが開始されているか
		/// </summary>
		/// <returns></returns>
		bool isStart();

	private:
		//カウントの演出用タイマー
		util::Timer m_CountAlphaTimer;
		util::Timer m_CountRotationScaleTimer;
		util::Timer m_CountSideScaleTimer;

		//GoとFinishの演出用タイマー
		util::Timer m_GoAlphaTimer;
		util::Timer m_GoScaleTimer;
		util::Timer m_FinishTimer;

		//次のシーン遷移用
		util::Timer m_NextSceneTimer;

		
		bool m_IsStart;
		bool m_IsEnd;
		bool m_Isfade;
		bool m_IsActive;
		bool m_Count1SE;
		bool m_Count2SE;
		bool m_Count3SE;
		bool m_CountStartSE;
		bool m_FinishSE;
		bool m_IsBloom;
		bool m_IsBlur;
		bool m_IsDOF;

		std::weak_ptr<BloomEffect> m_Bloom;
		std::weak_ptr<DOFEffect> m_DOF;
		std::weak_ptr<BlurEffect> m_Blur;

		std::weak_ptr<EnemySpowner> m_EnemySpowner;
		std::weak_ptr<EnemySpowner> m_EnemySpowner2;
		std::weak_ptr<EnemySpowner> m_EnemySpowner3;

		std::weak_ptr<PlayerComponent> m_pPlayer;
		std::weak_ptr<FadeComponent> m_Fade;

		//タイマーが常時減る用のGameTimerComponent
		std::weak_ptr<GameTimerCompornent> m_GameTimer;

		//StartとFinish
		framework::WeakEntity m_Finish;
		framework::WeakEntity m_GoEntity;
		std::weak_ptr<SpriteRenderComponent> m_FinishUI;
		std::weak_ptr<SpriteRenderComponent> m_GoUI;

		//カウント
		short m_count;
		framework::WeakEntity m_Count1Entity;
		framework::WeakEntity m_Count2Entity;
		framework::WeakEntity m_Count3Entity;	
		std::weak_ptr<SpriteRenderComponent> m_Count1;
		std::weak_ptr<SpriteRenderComponent> m_Count2;
		std::weak_ptr<SpriteRenderComponent> m_Count3;


		std::weak_ptr<EnemyRulesComponent> m_EnemyRules;
	};
}
