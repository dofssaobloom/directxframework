#include "GmaePause.h"
#include<Source\Application\Device\Input.h>
#include<Source\Component\Player\PlayerComponent.h>
#include<Source\Component\Enemy\BossEnemyComponent.h>
#include<Source\Component\Enemy\EnemyComponent.h>
#include<Source\Component\Enemy\EnemySpowner.h>
#include<Source\Component\Scene\GameMain\GameTimerCompornent.h>
#include<Source\Component\Physics\PhysicsWorld.h>
#include<Source\Component\Bullet\Bullet.h>
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>
#include<Source\Actor\Enemy\EnemyBullet.h>
#include<Source\Component\Scene\GameMain\RulesComponent.h>
#include<Source\Component\SpriteRenderComponent.h>
#include<Source\Component\Scene\GameMain\EnemyRulesComponent.h>
#include<Source\Component\Scene\GameMain\ComboComponent.h>
#include<Source\Component\FadeComponent.h>
#include<Source\Resource\ResourceManager.h>

namespace component {

	GamePause::GamePause()
		:m_GradationTimer(6), m_frashIntarvalTimer(3), m_frashTotalTimer(10)
	{
	}

	GamePause::~GamePause()
	{
	}

	void GamePause::init()
	{
		m_pGradationEffect = CameraComponent::getMainCamera().lock()->getGameObj().lock()->getComponent<GradationEffect>();

		auto player = framework::Entity::findGameObj("Player1");

		m_pPlayerComponent = player.lock()->getComponent<PlayerComponent>();

		auto golem = framework::Entity::findGameObj("Golem");

		m_pBossEnemyComponent = golem.lock()->getComponent<BossEnemyComponent>();

		m_pGameTimer = framework::Entity::findGameObj("GameTimer").lock()->getComponent<GameTimerCompornent>();

		m_pPhysicsWorld = framework::Entity::findGameObj("PhysicsWorld").lock()->getComponent<PhysicsWorld>();

		m_Fade = framework::Entity::findGameObj("Fade").lock()->getComponent<FadeComponent>();

		auto rules = framework::Entity::findGameObj("EnemyRules");

		m_pRules = rules.lock()->getComponent<RulesComponent>();

		m_pEnemyRules = rules.lock()->getComponent<EnemyRulesComponent>();
		m_Comb1 = framework::Entity::findGameObj("Combo").lock()->getComponent<ComboComponent>();
		m_Comb2 = framework::Entity::findGameObj("Fever").lock()->getComponent<ComboComponent>();

		m_pGradationEffect.lock()->setShift(util::Vec2(0, 0));

		m_IsPause = false;
		m_isWood = false;
		m_isUpZeroDown = false;
		m_isDownZeroDown = false;
		m_isPauseEnd = false;
		m_isSelect = false;

		m_pPauseBG = framework::Entity::findGameObj("PauseBG");
		m_pRetryButton = framework::Entity::findGameObj("RetryButton");
		m_pTitleButton = framework::Entity::findGameObj("TitleButton");
		m_pCloseButton = framework::Entity::findGameObj("CloseButton");

		int originX = 517 / 4;
		int originY = -314 / 4;
		m_pPauseBG.lock()->getTransform()->m_Origin.x = originX;
		m_pPauseBG.lock()->getTransform()->m_Origin.y = originY;

		originX = 512 / 4;
		originY = -256 / 4;
		m_pRetryButton.lock()->getTransform()->m_Origin.x = originX;
		m_pRetryButton.lock()->getTransform()->m_Origin.y = originY;
		m_pTitleButton.lock()->getTransform()->m_Origin.x = originX;
		m_pTitleButton.lock()->getTransform()->m_Origin.y = originY;
		m_pCloseButton.lock()->getTransform()->m_Origin.x = originX;
		m_pCloseButton.lock()->getTransform()->m_Origin.y = originY;

		//CSVのY座標の値を保持
		m_larpPauseBG = m_pPauseBG.lock()->getTransform()->m_Position.y;
		m_larpRetryYPos = m_pRetryButton.lock()->getTransform()->m_Position.y;
		m_larpTitleYPos = m_pTitleButton.lock()->getTransform()->m_Position.y;
		m_larpCloseYPos = m_pCloseButton.lock()->getTransform()->m_Position.y;

		m_pRenderers.emplace_back(m_pRetryButton.lock()->getComponent<SpriteRenderComponent>());
		m_pRenderers.emplace_back(m_pTitleButton.lock()->getComponent<SpriteRenderComponent>());
		m_pRenderers.emplace_back(m_pCloseButton.lock()->getComponent<SpriteRenderComponent>());

		m_pSceneChange = framework::Entity::findGameObj("GlobalEvent").lock()->getComponent<SceneChangeComponent>();

		m_Entity.lock()->addEvent("Carsol", [&]() {
			framework::ResourceManager::getInstance()->playSound("CarsolSelect");
			framework::ResourceManager::getInstance()->setVolume("CarsolSelect", -1500);
			framework::ResourceManager::getInstance()->setDoppler("CarsolSelect", 0);
		});

		m_Entity.lock()->addEvent("Enter", [&]() {
			framework::ResourceManager::getInstance()->playSound("Entry");
			framework::ResourceManager::getInstance()->setVolume("Entry", -1500);
			framework::ResourceManager::getInstance()->setDoppler("Entry", 0);
		});

		m_Entity.lock()->addEvent("Wood", [&]() {
			framework::ResourceManager::getInstance()->playSound("Wood");
			framework::ResourceManager::getInstance()->setVolume("Wood", -2000);
			framework::ResourceManager::getInstance()->setDoppler("Wood", 0);
		});

		m_Manu.addMenu([&]() {
			framework::EffectManager::getInstance()->updateStart();
			framework::EffectManager::getInstance()->clear();
			m_selectFrashRetry = !m_selectFrashRetry;
			m_isPauseEnd = true;

			m_Fade.lock()->onStart();
			
		});

		m_Manu.addMenu([&]() {
			framework::EffectManager::getInstance()->updateStart();
			framework::EffectManager::getInstance()->clear();
			m_selectFrashTitle = !m_selectFrashTitle;
			m_isPauseEnd = true;

			m_Fade.lock()->onStart();
		});

		m_Manu.addMenu([&]() {
			m_selectFrashClose = !m_selectFrashClose;
		});
	}

	void GamePause::onPause()
	{
		m_pGradationEffect.lock()->active();
		m_selectFrashRetry = false;
		m_selectFrashTitle = false;
		m_selectFrashClose = false;
		m_isWood = false;
		m_isSelect = false;
		m_isPauseEnd = false;
		m_isUpZeroDown = false;
		m_isDownZeroDown = false;

		//エネミーはスポーンされるので毎回更新する必要がある
		instanceGet();
		deActiveObject();
		m_GradationTimer.init();
		m_frashTotalTimer.init();
		m_frashIntarvalTimer.init();
		m_Manu.init(0);

		menuActive();
	}

	void GamePause::offPause()
	{
		m_selectFrashRetry = false;
		m_selectFrashTitle = false;
		m_selectFrashClose = false;
		m_isWood = false;
		m_isSelect = false;
		m_isUpZeroDown = false;
		m_isDownZeroDown = false;

		m_pRenderers[2].lock()->setAlpha(1);

		activeGameObject();
		m_GradationTimer.init();
		m_frashTotalTimer.init();
		m_frashIntarvalTimer.init();
		m_Manu.init(0);

		menuDeActive();
	}

	void GamePause::update()
	{
		if (!m_pRules.lock()->isStart())return;

		if (!m_GradationTimer.isEnd()) {
			float min = m_IsPause ? 5 : 0.1;
			float max = m_IsPause ? 0.1 : 5;

			auto shift = util::lerp<float>(m_GradationTimer.rate(), min, max);
			m_pGradationEffect.lock()->setShift(util::Vec2(shift, shift));
		}
		else {
			if (!m_IsPause) {
				m_pGradationEffect.lock()->deActive();
			}
		}

		m_GradationTimer.update();
		
		//Scene変更
		if (m_isPauseEnd && m_selectFrashRetry && m_frashTotalTimer.isEnd()) {
			nextScene("StageBeta");
		}
		if (m_isPauseEnd && m_selectFrashTitle && m_frashTotalTimer.isEnd()) {
			nextScene("TitleScene");
		}

		//Closeボタンが押されてフラッシュが終わったら閉じる
		if (m_selectFrashClose && m_frashTotalTimer.isEnd()) {
			m_IsPause = false;
			offPause();
		}

		if (application::Input::isPause())
		{
			m_IsPause = !m_IsPause;
			if (m_IsPause) {
				onPause();
				
			}
			else {
				offPause();
			}
		}

		pauseAction();
	}

	void GamePause::deActiveObject()
	{
		m_pPhysicsWorld.lock()->deActivePhysics();


		m_pPlayerComponent.lock()->pauseDeActive();
		m_pPlayerComponent.lock()->deActive();

		m_pBossEnemyComponent.lock()->pauseDeActive();
		m_pBossEnemyComponent.lock()->deActive();

		m_pGameTimer.lock()->deActive();

		m_pEnemyRules.lock()->deActive();

		m_Comb1.lock()->deActive();

		m_Comb2.lock()->deActive();

		for (auto& enemyComponent : m_pEnemyComponents)
		{
			if (enemyComponent.expired())continue;
			enemyComponent.lock()->pauseDeActive();
			enemyComponent.lock()->deActive();
		}

		for (auto& spowner : m_pSpownerComponents)
		{
			spowner.lock()->deActive();
		}

		//プレイヤーバレット
		for (auto& bullet : m_pBulletComponents)
		{
			if (!bullet.expired())
				bullet.lock()->deActive();
		}

		//エネミーバレット
		for (auto& bullet : m_pEnemyBulletComponents)
		{
			if (!bullet.expired())
				bullet.lock()->deActive();
		}

		framework::EffectManager::getInstance()->updateStop();
	}

	void GamePause::activeGameObject()
	{
		m_pPhysicsWorld.lock()->activePhysics();

		m_pPlayerComponent.lock()->pauseActive();
		m_pPlayerComponent.lock()->active();

		m_pBossEnemyComponent.lock()->pauseActive();
		m_pBossEnemyComponent.lock()->active();

		m_pGameTimer.lock()->active();

		m_pEnemyRules.lock()->active();

		m_Comb1.lock()->active();

		m_Comb2.lock()->active();

		for (auto& enemyComponent : m_pEnemyComponents)
		{
			if (enemyComponent.expired())continue;
			enemyComponent.lock()->pauseActive();
			enemyComponent.lock()->active();
		}

		for (auto& spowner : m_pSpownerComponents)
		{
			spowner.lock()->active();
		}


		//プレイヤーのバレット
		for (auto& bullet : m_pBulletComponents)
		{
			if (!bullet.expired())
				bullet.lock()->active();
		}


		//エネミーバレット
		for (auto& bullet : m_pEnemyBulletComponents)
		{
			if (!bullet.expired())
				bullet.lock()->active();
		}

		framework::EffectManager::getInstance()->updateStart();
	}

	void GamePause::instanceGet()
	{
		auto enemys = framework::Entity::findGameObjWithTags("Enemy");

		for (auto& enemy : enemys)
		{
			auto enemyComponent = enemy.lock()->getComponent<EnemyComponent>();
			if (!enemyComponent.expired()) {
				m_pEnemyComponents.emplace_back(enemyComponent);
			}
		}

		auto spowners = framework::Entity::findGameObjWithTags("Spowner");

		for (auto& spowner : spowners)
		{
			auto s = spowner.lock()->getComponent<EnemySpowner>();
			m_pSpownerComponents.emplace_back(s);
		}

		auto bullets = framework::Entity::findGameObjWithTags("Bullet");

		for (auto& bullet : bullets)
		{
			auto bulletComponent = bullet.lock()->getComponent<Bullet>();
			m_pBulletComponents.emplace_back(bulletComponent);
		}

		bullets = framework::Entity::findGameObjs("EnemyBullet");

		for (auto& bullet : bullets)
		{
			auto bulletComponent = bullet.lock()->getComponent<EnemyBullet>();
			m_pEnemyBulletComponents.emplace_back(bulletComponent);
		}
	}

	void GamePause::pauseAction()
	{
		if (!m_IsPause)return;
		auto isLeftUp = application::Input::leftUpTrigger();
		auto isLeftDown = application::Input::leftDownTrigger();

		if (m_GradationTimer.isEnd() && isLeftUp) {
			if (!m_isSelect && !m_isUpZeroDown) {
				m_Entity.lock()->onEvent("Carsol");
				m_isUpZeroDown = true;
				m_Manu.back();
			}
		}
		else {
			m_isUpZeroDown = false;
		}

		if (m_GradationTimer.isEnd() && isLeftDown) {
			if (!m_isSelect && !m_isDownZeroDown) {
				m_Entity.lock()->onEvent("Carsol");
				m_isDownZeroDown = true;
				m_Manu.enter();
			}
		}
		else {
			m_isDownZeroDown = false;
		}

		//Apply
		if (m_GradationTimer.isEnd() && application::Input::isJump()) {
			if (!m_isSelect) {
				m_Entity.lock()->onEvent("Enter");
				m_Manu.applay();
			}
			m_isSelect = true;
		}

		//点滅開始
		if (m_selectFrashRetry || m_selectFrashTitle || m_selectFrashClose) {
			m_frashIntarvalTimer.update();
			m_frashTotalTimer.update();
		}
		if (m_frashIntarvalTimer.isEnd()) {
			m_frashIntarvalTimer.init();
		}

		//移動
		int startPos = -300;
		auto yMove = util::bezierCurve(m_GradationTimer.rate(), m_larpPauseBG, startPos, startPos);
		m_pPauseBG.lock()->getTransform()->m_Position.y = yMove;
		yMove = util::bezierCurve(m_GradationTimer.rate(), m_larpRetryYPos, startPos - 8, startPos - 8);
		m_pRetryButton.lock()->getTransform()->m_Position.y = yMove;
		yMove = util::bezierCurve(m_GradationTimer.rate(), m_larpTitleYPos, startPos + 70, startPos + 70);
		m_pTitleButton.lock()->getTransform()->m_Position.y = yMove;
		yMove = util::bezierCurve(m_GradationTimer.rate(), m_larpCloseYPos, startPos + 70 * 2, startPos + 70 * 2);
		m_pCloseButton.lock()->getTransform()->m_Position.y = yMove;

		//傾き
		int slope = -12;
		m_pPauseBG.lock()->getTransform()->m_Rotation.z = slope;
		m_pRetryButton.lock()->getTransform()->m_Rotation.z = slope;
		m_pTitleButton.lock()->getTransform()->m_Rotation.z = slope;
		m_pCloseButton.lock()->getTransform()->m_Rotation.z = slope;

		if (m_GradationTimer.isEnd()) {
			if (!m_isWood) {
				m_Entity.lock()->onEvent("Wood");
				m_isWood = true;
			}

			slope = 0;
			m_pPauseBG.lock()->getTransform()->m_Rotation.z = slope;
			m_pRetryButton.lock()->getTransform()->m_Rotation.z = slope;
			m_pTitleButton.lock()->getTransform()->m_Rotation.z = slope;
			m_pCloseButton.lock()->getTransform()->m_Rotation.z = slope;
		}
		else {
			m_isUpZeroDown = true;
			m_isDownZeroDown = true;
		}

		//ボタンの大きさ変換
		for (size_t i = 0; i < m_pRenderers.size(); i++)
		{
			if (m_Manu.getCount() == i) {
				m_pRenderers[i].lock()->getGameObj().lock()->getTransform()->scaling(0.6);
			}
			else {
				m_pRenderers[i].lock()->getGameObj().lock()->getTransform()->scaling(0.5);
			}
		}

		//点滅
		auto frashRate = m_frashIntarvalTimer.rate();
		auto frash = util::bezierCurve(frashRate, 1.0, 0.0, 1.0);
		if (m_selectFrashRetry) {
			m_pRenderers[0].lock()->setAlpha(frash);
		}
		if (m_selectFrashTitle) {
			m_pRenderers[1].lock()->setAlpha(frash);
		}
		if (m_selectFrashClose) {
			m_pRenderers[2].lock()->setAlpha(frash);
		}
	}

	void GamePause::menuActive()
	{
		m_pPauseBG.lock()->active();
		m_pRetryButton.lock()->active();
		m_pTitleButton.lock()->active();
		m_pCloseButton.lock()->active();
	}

	void GamePause::menuDeActive()
	{
		m_pPauseBG.lock()->deActive();
		m_pRetryButton.lock()->deActive();
		m_pTitleButton.lock()->deActive();
		m_pCloseButton.lock()->deActive();
	}

	void GamePause::nextScene(std::string sceneName) {
		if (m_Fade.lock()->isEnd() && m_Fade.lock()->isOutFade()) {
			framework::Scene::m_NextSceneName = sceneName;
			m_pSceneChange.lock()->changeSceneNotification();
		}
	}
}