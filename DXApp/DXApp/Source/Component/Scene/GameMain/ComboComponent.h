#pragma once
#include<Framework.h>
#include<vector>
#include<Source\Util\Timer\Timer.h>

namespace component {
	class Counter;

	struct ComboDesc {
		//カットインの入る経過時間
		float cutInTime;

		//フォントサイズ
		float maxFontSize;

		//コンボ数表示する値
		short drawCombo;

		//到達地点
		short reachedPosition;

		//移動速度
		short moveSpeed;

		//位置
		util::Transform trans;
	};
	
	class ComboComponent : public framework::UpdateComponent
	{
	public:
		ComboComponent();
		~ComboComponent();

		void init()override;

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		//コンボを増やす
		void addCombo(short addCombo);

		//一定以上タイマーが進行したらコンボを消す
		void deleatCombo();

		//フォントサイズの変更
		void changeFontSize(float fontSize);

		//位置のセット
		void setPosition();

		//タイムが半分かどうか
		bool rateTime();

		//フォントの移動
		void movePosition(short stopPosition, short moveSpeed);

		//コンボ表示
		bool isComboDraw();
		short returnCombo();

		void frashComboDraw();

		void isFever(bool fever);
		void alphaZero(bool alpha);

		//位置の変更
		void changePos(const util::Vec2& pos);

		void setCallOrder(short call);

	private:
		/// <summary>
		/// ナンバー画像追加
		/// </summary>
		/// <param name="name"></param>
		/// <param name="pos"></param>
		std::weak_ptr<Counter> addNumber(const std::string& name, const util::Vec2& pos, const util::Vec2& scale, std::vector<std::weak_ptr<Component>>* componentInitalizer);
	private:
		ComboDesc m_comboDesc;
		bool m_Fever;
		bool m_alpha0;
		short m_TextSize;
		//ミリ秒(毎フレ)
		short m_Combo;
		float m_Scale;

		short m_frashCount;

		util::Timer m_ComboDrawTimer;
		util::Timer m_ComboFrashTimer;

		//パラメータで設定した値保管用
		util::Transform m_LarpTrans;

		framework::WeakEntity m_ComboUIEntity;
		std::weak_ptr<SpriteRenderComponent> m_ComboUI;
		
		std::vector < std::weak_ptr<Counter>> m_Renderers;
		std::vector<framework::WeakEntity> m_Counters;
		std::vector<std::string> m_TextureNames;
	};
}
