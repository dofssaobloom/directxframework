#include"RulesComponent.h"
#include<Source\Component\SceneChangeComponent.h>
#include<Source\Application\Scene\SceneDelivery.h>
#include<Source\Component\Player\PlayerComponent.h>
#include<Source\Component\SpriteRenderComponent.h>
#include<Source\Util\Math\Math.h>
#include<Source\Component\Scene\GameMain\KillCounter.h>
#include<Source\Application\Device\Input.h>
#include<Source\Component\Scene\GameMain\GameTimerCompornent.h>
#include<Source\Component\FadeComponent.h>
#include"ScoreComponent.h"
#include"Source\Component\Enemy\EnemySpowner.h"
#include<Source\Util\IO\BinaryLoader.h>
#include<Source\Component\CameraComponent.h>
#include"EnemyRulesComponent.h"

UsingNamespace;

namespace component {
	RulesComponent::RulesComponent()
		:m_FinishTimer(30), m_GoAlphaTimer(20), m_GoScaleTimer(10), m_NextSceneTimer(30 * 2), m_CountAlphaTimer(30),
		m_CountRotationScaleTimer(10), m_CountSideScaleTimer(2)
	{
		m_CallOrder = 1;
	}

	RulesComponent::~RulesComponent()
	{
	}

	void RulesComponent::init()
	{
		m_Isfade = false;
		m_Count1SE = false;
		m_Count2SE = false;
		m_Count3SE = false;
		m_CountStartSE = false;
		m_FinishSE = false;
		m_IsEnd = false;

		m_Entity.lock()->addEvent("CountDown1", [&]() {
			framework::ResourceManager::getInstance()->playSound("CountDown1");
			framework::ResourceManager::getInstance()->setVolume("CountDown1", -2500);
			framework::ResourceManager::getInstance()->setDoppler("CountDown1", 0);
		});
		m_Entity.lock()->addEvent("CountDownStart", [&]() {
			framework::ResourceManager::getInstance()->playSound("CountDownStart");
			framework::ResourceManager::getInstance()->setVolume("CountDownStart", -2500);
			framework::ResourceManager::getInstance()->setDoppler("CountDownStart", 0);
		});
		m_Entity.lock()->addEvent("Whistle", [&]() {
			framework::ResourceManager::getInstance()->playSound("Whistle");
			framework::ResourceManager::getInstance()->setVolume("Whistle", -2500);
			framework::ResourceManager::getInstance()->setDoppler("Whistle", 0);
		});

		//ゲームオブジェクトの取得
		auto camera = CameraComponent::getMainCamera().lock()->getGameObj();

		m_Bloom = camera.lock()->getComponent<BloomEffect>();
		m_DOF = camera.lock()->getComponent<DOFEffect>();
		m_Blur = camera.lock()->getComponent<BlurEffect>();

		m_EnemySpowner = framework::Entity::findGameObj("EnemySpowner").lock()->getComponent<EnemySpowner>();
		m_EnemySpowner2 = framework::Entity::findGameObj("EnemySpowner2").lock()->getComponent<EnemySpowner>();
		m_EnemySpowner3 = framework::Entity::findGameObj("EnemySpowner3").lock()->getComponent<EnemySpowner>();

		m_Fade = framework::Entity::findGameObj("Fade").lock()->getComponent<FadeComponent>();
		m_pPlayer = Entity::findGameObj("Player1").lock()->getComponent<PlayerComponent>();
		m_GameTimer = framework::Entity::findGameObj("GameTimer").lock()->getComponent<GameTimerCompornent>();

		m_Finish = framework::Entity::findGameObj("FinishUI");
		m_FinishUI = m_Finish.lock()->getComponent<SpriteRenderComponent>();

		m_GoEntity = framework::Entity::findGameObj("GoUI");
		m_GoUI = m_GoEntity.lock()->getComponent<SpriteRenderComponent>();

		m_Count1Entity = framework::Entity::findGameObj("Count_1");
		m_Count2Entity = framework::Entity::findGameObj("Count_2");
		m_Count3Entity = framework::Entity::findGameObj("Count_3");
		m_Count1 = m_Count1Entity.lock()->getComponent<SpriteRenderComponent>();
		m_Count2 = m_Count2Entity.lock()->getComponent<SpriteRenderComponent>();
		m_Count3 = m_Count3Entity.lock()->getComponent<SpriteRenderComponent>();

		//m_FinishUI.lock()->setAlpha(0.0f);
		//m_GoUI.lock()->setAlpha(0.0f);

		ScoreComponent::m_Score = 0;
	    m_EnemyRules = m_Entity.lock()->getComponent<EnemyRulesComponent>();

		
		m_FinishTimer.init();
		m_GoAlphaTimer.init();
		m_GoScaleTimer.init();
		m_NextSceneTimer.init();
		m_CountSideScaleTimer.init();
		m_CountRotationScaleTimer.init();
		m_CountAlphaTimer.init();

		m_Finish.lock()->deActive();
		m_GoEntity.lock()->deActive();

		byteLoad();
		m_count = 4;

		m_EnemySpowner.lock()->deActive();
		m_EnemySpowner2.lock()->deActive();
		m_EnemySpowner3.lock()->deActive();
		m_pPlayer.lock()->deActive();

		m_IsStart = false;
	}

	void RulesComponent::update()
	{
		if (m_Fade.lock()->isEnd()) {
			m_CountRotationScaleTimer.update();

			//回転し終わったら透過開始
			if (m_CountRotationScaleTimer.isEnd()) {
				m_CountSideScaleTimer.update();
				m_CountAlphaTimer.update();
			}
		}

		//ゲームが終了した||プレイヤーが死んだ
		if (m_GameTimer.lock()->isEnd() || m_pPlayer.lock()->isDead())
		{
			if (!m_Isfade) {
				m_EnemyRules.lock()->combToScore();
			}
			if (!m_pPlayer.lock()->isDead()) {
				//強制的にアイドルステートにする
				m_pPlayer.lock()->changeIdle();
			}
			m_Isfade = true;
			m_IsEnd = true;
			//m_pPlayer.lock()->deActive();
			m_Finish.lock()->active();

			m_FinishTimer.update();

			//FinishSE
			if (!m_FinishSE) {
				m_Entity.lock()->onEvent("Whistle");
				m_FinishSE = true;
			}
		}

		//Finishの表示後次のシーンに行くまでの時間
		if (m_FinishTimer.isEnd()) {
			m_NextSceneTimer.update();
		}

		changeUIAlpha();

		if (m_NextSceneTimer.isEnd() && m_Isfade)
		{
			m_Fade.lock()->onStart();
		}

		if (m_Isfade) {
			changeScene();
		}
	}

	void RulesComponent::countDown()
	{
		auto countRate = m_CountAlphaTimer.rate();
		auto countAlpha = util::bezierCurve(countRate, 0.0f, 1.0f, 1.0f, 1.0f);

		auto countRotationRate = m_CountRotationScaleTimer.rate();
		auto countRotation = util::bezierCurve(countRotationRate, 0.0f, 0.0f, 360.0f);
		auto countScele = util::bezierCurve(countRotationRate, 0.5f, 0.5f, 0.0f);

		auto countSideScaleRate = m_CountSideScaleTimer.rate();
		auto countSideScele = util::bezierCurve(countSideScaleRate, 0.5f, 1.0f, 0.5f);

		//カウントダウン
		if (m_count == 3) {
			m_Count3.lock()->active();
			m_Count3Entity.lock()->getTransform()->m_Rotation.z = countRotation;
			m_Count3Entity.lock()->getTransform()->scaling(countScele);
			m_Count3Entity.lock()->getTransform()->m_Scale.x = countSideScele;
			m_Count3.lock()->setAlpha(countAlpha);
			if (!m_Count1SE) {
				m_Entity.lock()->onEvent("CountDown1");
				m_Count1SE = true;
			}
		}
		else if (m_count == 2) {
			m_Count2.lock()->active();
			m_Count2Entity.lock()->getTransform()->m_Rotation.z = countRotation;
			m_Count2Entity.lock()->getTransform()->scaling(countScele);
			m_Count2Entity.lock()->getTransform()->m_Scale.x = countSideScele;
			m_Count2.lock()->setAlpha(countAlpha);
			if (!m_Count2SE) {
				m_Entity.lock()->onEvent("CountDown1");
				m_Count2SE = true;
			}
		}
		else if (m_count == 1) {
			m_Count1.lock()->active();
			m_Count1Entity.lock()->getTransform()->m_Rotation.z = countRotation;
			m_Count1Entity.lock()->getTransform()->scaling(countScele);
			m_Count1Entity.lock()->getTransform()->m_Scale.x = countSideScele;
			m_Count1.lock()->setAlpha(countAlpha);
			if (!m_Count3SE) {
				m_Entity.lock()->onEvent("CountDown1");
				m_Count3SE = true;
			}
		}

		if (m_CountAlphaTimer.isEnd() && m_count > 0) {
			m_count -= 1;
			m_CountSideScaleTimer.init();
			m_CountRotationScaleTimer.init();
			m_CountAlphaTimer.init();
		}
	}

	void RulesComponent::changeUIAlpha()
	{
		countDown();

		//GoUIの透明度
		if (m_count == 0){
			m_GoEntity.lock()->active();
			m_Count1.lock()->deActive();
			m_Count2.lock()->deActive();
			m_Count3.lock()->deActive();

			m_GoScaleTimer.update();

			//スケールの変更が終わったら透明度変更
			if (m_GoScaleTimer.isEnd()) {
				m_GoAlphaTimer.update();
			}

			if (!m_CountStartSE) {
				m_Entity.lock()->onEvent("CountDownStart");
				m_CountStartSE = true;
			}

			auto goRate = m_GoAlphaTimer.rate();
			auto goAlpha = util::bezierCurve(goRate, 0.0f, 1.0f, 1.0f, 1.0f);
			auto scaleRate = m_GoScaleTimer.rate();
			auto scale = util::bezierCurve(scaleRate, 0.5f, 0.5f, 0.5f, 0.8f);
			m_GoEntity.lock()->getTransform()->scaling(scale);
			m_GoUI.lock()->setAlpha(goAlpha);

			m_GameTimer.lock()->IsStartDecrement(true);

			if (m_GoAlphaTimer.isEnd()) {
				m_GoEntity.lock()->deActive();
			}
		}
		else {
			m_GoUI.lock()->setAlpha(0);
		}

		//Goの表示後に動ける
		if (m_count == 0  && !m_IsStart) {
			m_EnemySpowner.lock()->active();
			m_EnemySpowner2.lock()->active();
			m_EnemySpowner3.lock()->active();
			m_pPlayer.lock()->active();
			m_IsStart = true;
		}

		//FinishUIの透明度とスケールの変更
		auto finishRate = m_FinishTimer.rate();
		auto finishAlpha = util::bezierCurve(finishRate, 1.0f, 1.0f, 1.0f, 0.0f);
		auto finishScale = util::bezierCurve(finishRate, 0.5f, 0.5f, 2.5f);
		m_Finish.lock()->getTransform()->scaling(finishScale);
		m_FinishUI.lock()->setAlpha(finishAlpha);
	}

	void RulesComponent::changeScene()
	{
		if (m_IsEnd && m_Fade.lock()->isEnd() && m_Fade.lock()->isOutFade())
		{
			m_Finish.lock()->deActive();
			Scene::m_NextSceneName = "ResultScene";
			framework::EffectManager::getInstance()->clear();
			framework::Entity::findGameObj("GlobalEvent").lock()->getComponent<SceneChangeComponent>().lock()->changeSceneNotification();
		}
	}
	void RulesComponent::byteLoad()
	{
		util::BinaryLoader loader("Resource/GameConfig.byte");
		std::vector<float> result;
		loader.load<float>(&result);
		if (result.size() > 0) {
			m_IsBloom = result[0];
			m_IsDOF = result[1];
			m_IsBlur = result[2];
		}

		if (m_IsBloom) {
			m_Bloom.lock()->active();
		}
		else {
			m_Bloom.lock()->deActive();
		}

		if (m_IsDOF) {
			m_DOF.lock()->active();
		}
		else {
			m_DOF.lock()->deActive();
		}

		if (m_IsBlur) {
			m_Blur.lock()->active();
		}
		else {
			m_Blur.lock()->deActive();
		}

		application::Input::m_ReverseX = result[3];
		application::Input::m_ReverseY = result[4];
	}

	bool RulesComponent::isStart()
	{
		return m_IsStart;
	}

}
