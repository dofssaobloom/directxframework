#include "GameTimerCompornent.h"
#include<Source\Util\Lua\LuaBase.h>

#include<Source\Component\Util\Counter.h>
#include<Source\Util\Math\Math.h>
#include<Source\Component\SpriteRenderComponent.h>

namespace component {
	GameTimerCompornent::GameTimerCompornent()
	{
		m_CallOrder = 1;
	}

	GameTimerCompornent::~GameTimerCompornent()
	{
	}

	void GameTimerCompornent::init()
	{
		m_MinitchColon = framework::Entity::findGameObj("ColonMinitch").lock()->getComponent<SpriteRenderComponent>();
		m_SecondColon = framework::Entity::findGameObj("ColonSecond").lock()->getComponent<SpriteRenderComponent>();

		m_Count = 0;
		m_LarpSecondCount = 0;
		m_LarpMinitchCount = 0;
		m_Decrement = false;
		changeFontSize(0.3f, 0.4f);

		//全部０にする
		for (auto& render : m_Renderers)
		{
			render.lock()->changeNum(0);
			render.lock()->m_CallOrder = 5;
		}
		m_MinitchColon.lock()->setAlpha(0.93);
		m_SecondColon.lock()->setAlpha(0.93);
		/*
		for (auto& counter : m_Renderers)
		{
			counter.lock()->m_CallOrder = 5;
		}
		*/

		m_MaxTime = (m_MinitchCount * 60) + m_SecondCount;
	}

	void GameTimerCompornent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		//幅
		const int texSize = 128 / 5;

		//文字の大きさ
		m_Scale = 0.5f;

		//UIの位置と大きさ設定
		auto&& scale = util::Vec2(m_Scale, m_Scale);

		//ミリ秒
		auto counter = addNumber("Count1", util::Vec2(m_Trans.m_Position.x, m_Trans.m_Position.y + 5), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);

		//秒
		counter = addNumber("Count2", util::Vec2(m_Trans.m_Position.x - texSize * 2, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count3", util::Vec2(m_Trans.m_Position.x - texSize * 3, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);

		//分
		counter = addNumber("Count4", util::Vec2(m_Trans.m_Position.x - texSize * 5, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count5", util::Vec2(m_Trans.m_Position.x - texSize * 6, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
	}

	void GameTimerCompornent::update()
	{
		if (isEnd())return;

		for (auto& counter : m_Renderers)
		{
			counter.lock()->setAlpha(0.95f);
		}

		if (m_Decrement) {
			timeDecrement();
		}

		//秒 : 位の設定
		short digid = util::getDigid(m_Count, 1);
		m_Renderers[0].lock()->changeNum(digid);
		digid = util::getDigid(m_SecondCount, 1);
		m_Renderers[1].lock()->changeNum(digid);
		digid = util::getDigid(m_SecondCount, 10);
		m_Renderers[2].lock()->changeNum(digid);

		//分 : 位の設定
		digid = util::getDigid(m_MinitchCount, 1);
		//分 : 描画用の追加
		m_Renderers[3].lock()->changeNum(digid);
		digid = util::getDigid(m_MinitchCount, 10);
		m_Renderers[4].lock()->changeNum(digid);


	}

	void GameTimerCompornent::setParam(const std::vector<std::string>& param)
	{
		//位置
		m_Trans.m_Position.x = std::atoi(param[0].c_str());
		m_Trans.m_Position.y = std::atoi(param[1].c_str());

		util::LuaBase lua(param[2]);

		//分
		m_MinitchCount = lua.getParam<float>("MinitchCount");

		//秒
		m_SecondCount = lua.getParam<float>("SecondCount");

		auto p = param;
		p.erase(p.begin(), std::next(p.begin(), 3));
		m_TextureNames.resize(10);

		for (short i = 0; i < p.size(); i++)
		{
			m_TextureNames[i] = p[i];
		}
	}

	void GameTimerCompornent::changeFontSize(float secondFontSize, float fontSize)
	{
		m_Counters[0].lock()->getTransform()->scaling(secondFontSize);
		m_Counters[1].lock()->getTransform()->scaling(fontSize);
		m_Counters[2].lock()->getTransform()->scaling(fontSize);
		m_Counters[3].lock()->getTransform()->scaling(fontSize);
		m_Counters[4].lock()->getTransform()->scaling(fontSize);
	}

	void GameTimerCompornent::addTimer(short incrementSecondCount)
	{
		//終了していたら加算しない
		if (isEnd())return;

		short minitch = incrementSecondCount / 60;
		short second = incrementSecondCount % 60;	

		//増やす分の秒
		m_LarpSecondCount = m_SecondCount + second;

		minitch += m_LarpSecondCount / 60;
		m_SecondCount = m_LarpSecondCount % 60;

		if (m_MinitchCount <= 59) {
			//増やす分の分
			m_MinitchCount += minitch;
		}
		else {
			m_MinitchCount = 59;
		}

		//初期値の最大時間を変える
		m_MaxTime += m_LarpSecondCount;
	}

	void GameTimerCompornent::countDecrement(short decrementSecondCount,short decrementMinitchCount)
	{
		//減らす分の秒
		m_LarpSecondCount = m_SecondCount - decrementSecondCount;

		//減らす分まで秒を増やす
		if (m_LarpSecondCount < m_SecondCount)
			m_SecondCount--;

		//減らす分の分
		m_LarpMinitchCount = m_MinitchCount - decrementMinitchCount;

		//減らす分まで分を増やす
		if (m_LarpMinitchCount < m_MinitchCount)
			m_MinitchCount--;
	}

	void GameTimerCompornent::timeIncrement()
	{
		//ミリ秒(毎フレ)
		if (m_Count < 599)
		{
			m_Count++;
		}
		else
		{
			m_Count = 0;
		}

		//秒
		if (!(m_Count % 30)) {
			if (m_SecondCount < 59) 
			{
				m_SecondCount++;
			}
			else 
			{
				m_SecondCount = 0;
			}
		}
		
		//分
		if (!(m_Count % 600) && m_MinitchCount < 59)
		{
			m_MinitchCount++;
		}
	}

	void GameTimerCompornent::timeDecrement()
	{
		//秒(毎フレ)
		if (m_SecondCount >= 1) 
		{
			if (m_Count > 0)
			{
				m_Count--;
			}
			else
			{
				m_Count = 299;
			}
		}
		else if(m_SecondCount < 1)
		{
			m_Count = 0;
		}

		//秒
		if (!(m_Count % 30)) {
			if (m_SecondCount > 0) 
			{
				m_SecondCount--;
			}
			else
			{
				//分
				if (m_MinitchCount >= 1) {
					m_MinitchCount--;
					m_SecondCount = 59;
				}
				else if (m_MinitchCount < 1) {
					m_SecondCount = 0;
				}
			}
		}
	}

	std::weak_ptr<Counter> GameTimerCompornent::addNumber(const std::string & name, const util::Vec2 & pos, const util::Vec2& scale, std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		auto entity = framework::Entity::createEntity(name, "UI", util::Transform(util::Vec3(pos.x, pos.y, 0), util::Vec3(), util::Vec3(scale.x, scale.y, 1)));
		auto counter = entity.lock()->addComponent<Counter>(componentInitalizer, m_TextureNames);
		counter.lock()->m_CallOrder = 0;

		(*componentInitalizer).emplace_back(counter);
		m_Counters.emplace_back(entity);
		return counter;
	}

	bool GameTimerCompornent::IsStartDecrement(bool start)
	{
		return m_Decrement = start;
	}

	float GameTimerCompornent::getTimeRate()
	{
		short time = (m_MinitchCount * 60) + m_SecondCount;

		return 1.0f - (time / (float)m_MaxTime);
	}

	void GameTimerCompornent::changePos(const util::Vec2 & pos)
	{
		//UIの位置と大きさ設定
		const int texSize = 128 / 2;

		//位置
		m_Counters[0].lock()->getTransform()->m_Position = util::Vec3(pos.x, pos.y, 0);
		m_Counters[1].lock()->getTransform()->m_Position = util::Vec3(pos.x - texSize, pos.y, 0);
		m_Counters[2].lock()->getTransform()->m_Position = util::Vec3(pos.x - texSize * 2, pos.y, 0);
		m_Counters[3].lock()->getTransform()->m_Position = util::Vec3(pos.x - texSize * 3, pos.y, 0);
		m_Counters[4].lock()->getTransform()->m_Position = util::Vec3(pos.x - texSize * 4, pos.y, 0);
	}

	bool GameTimerCompornent::isEnd()
	{
		return m_MinitchCount < 1 && m_SecondCount < 1 && m_Count < 1;
	}
}
