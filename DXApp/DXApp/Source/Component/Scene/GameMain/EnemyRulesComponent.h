#pragma once
#include<Framework.h>
#include<list>

namespace component {
	class ComboComponent;
	class ScoreComponent;
	class GameTimerCompornent;
	class GUIComponent;
	class AddCount;

	struct EnemyRulesDesc
	{
		short addCombo;
		short addScore;
		short addTimer;
		short multiplyCount;
		short fiverEnemyDeadCount;
		short addFeverTimer;
	};

	class EnemyRulesComponent : public framework::UpdateComponent
	{
	public:
		EnemyRulesComponent();
		~EnemyRulesComponent();

		void init()override;

		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		virtual void setParam(const std::vector<std::string>& param);

		virtual void update() override;

		//コンボの追加
		void addCombo();

		//スコアの追加
		void addScore();

		/// <summary>
		/// ボス用スコア加算
		/// 倒した数
		/// </summary>
		void addBossScore(short killCount);

		//タイマーの追加
		void addTimer();

		//0,0の位置に
		void autScreenUI();

		bool isGameTimerEnd();


		/// <summary>
		/// コンボをスコアに強制変換
		/// </summary>
		void combToScore();

		void TimeCountspown(int addCount, bool isDigit);

		void ScoreCountspown(int addCount, int positionY, bool isScale, bool isDigit);

	private:
		util::Timer m_comboFiverTimer;
		int m_ComboCount;
		float m_ScoreCount;

		//!コンボ中に獲得したスコアの合計
		int m_TotalCombScore;

		//!コンボちゅうか
		bool m_IsComb;
		
		bool m_IsPreComb;

		std::string m_LuaPath;
		std::weak_ptr<GUIComponent> m_pUI;
		EnemyRulesDesc m_EnemyRulesDesc;

		//スコアの追加合計値用
		int m_ScoreTotalCount;
		int m_ScoreTotalLarp;

		//タイマーの追加合計値用
		int m_TimeTotalCount;

		//Timerが追加されたかどうか
		bool m_isAddTimer;
		bool m_isAddScore;

		//タイマーの追加合計値表示タイマー
		util::Timer m_TotalTimer;
		util::Timer m_TotalScoreTimer;

		std::weak_ptr<ComboComponent> m_Combo;
		std::weak_ptr<ComboComponent> m_Fever;
		std::weak_ptr<ScoreComponent> m_Score;
		std::weak_ptr<GameTimerCompornent> m_pGameTimer;
	};
}