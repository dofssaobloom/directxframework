#pragma once
#include<Framework.h>
#include<vector>
#include<Source\Util\Timer\Timer.h>

namespace component {

	class PlayerComponent;

	class HPUI : public framework::UpdateComponent
	{
	public:
		HPUI();
		~HPUI();

		void init()override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

	private:
		std::weak_ptr<SpriteRenderComponent> addSprite(framework::WeakEntity entity);


	private:
		std::vector < std::weak_ptr<SpriteRenderComponent>> m_Renderers;

		std::vector<framework::WeakEntity> m_Entitys;

		float m_Scale;

		std::weak_ptr<PlayerComponent> m_pPlayer;

		util::Timer m_Timer;

		bool isPlayerDamage;
	};
}
