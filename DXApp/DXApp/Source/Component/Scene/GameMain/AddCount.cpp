#include "AddCount.h"
#include<Source\Util\Math\Math.h>
#include<Source\Component\Util\Counter.h>
#include<Source\Component\SpriteRenderComponent.h>

namespace component {
	AddCount::AddCount()
		:m_AlphaTimer(45), m_MoveSpeedTimer(10), m_ScoreTotalAlphaTimer(30 * 2)
	{
		m_CallOrder = 1;
	}

	AddCount::~AddCount()
	{
	}

	void AddCount::init()
	{
		for (auto& render : m_Renderers)
		{
			render.lock()->init();
		}
		//全部０にする
		for (auto& render : m_Renderers)
		{
			render.lock()->changeNum(0);
		}

		for (auto& counter : m_Renderers)
		{
			counter.lock()->m_CallOrder = 5;
		}

		m_EntityPositionY = m_Entity.lock()->getTransform()->m_Position.y;
		m_MoveSpeedTimer.init();
		m_AlphaTimer.init();
		m_ScoreTotalAlphaTimer.init();
		m_isDeleat = false;
		m_isMovePlus = false;
		m_isScale = false;
		m_isdDigit = false;
	}

	void AddCount::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		//幅
		const int texSize = 128 / 4;

		//文字の大きさ
		m_Scale = 0.5f;

		//UIの位置と大きさ設定
		auto&& scale = util::Vec2(m_Scale, m_Scale);

		//秒
		auto counter = addNumber("Count1", util::Vec2(m_Entity.lock()->getTransform()->m_Position.x, m_Entity.lock()->getTransform()->m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count2", util::Vec2(m_Entity.lock()->getTransform()->m_Position.x - texSize, m_Entity.lock()->getTransform()->m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count3", util::Vec2(m_Entity.lock()->getTransform()->m_Position.x - texSize * 2, m_Entity.lock()->getTransform()->m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count4", util::Vec2(m_Entity.lock()->getTransform()->m_Position.x - texSize * 3, m_Entity.lock()->getTransform()->m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count5", util::Vec2(m_Entity.lock()->getTransform()->m_Position.x - texSize * 4, m_Entity.lock()->getTransform()->m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
	}

	void AddCount::update()
	{
		//タイマーかどうか
		if (m_isTimer) {
			moveCount(590, 130, 160);
		}
		else {
			if (!m_isScale) {
				moveCount(990, 50, 82);
			}
			else {
				changeScale();
				//115
				moveCount(990, 72, 102);
			}
		}

		changeAlpha();

		//消してもいい場合Destroy
		if (isDereat() && m_isMovePlus) {
			//Plusが表示されていたら
			m_Plus.lock()->destory(m_PlusEntity);
			destory(m_Entity);
		}

		//位の設定
		short digid = util::getDigid(m_Count, 1);
		m_Renderers[0].lock()->changeNum(digid);
		digid = util::getDigid(m_Count, 10);
		m_Renderers[1].lock()->changeNum(digid);
		digid = util::getDigid(m_Count, 100);
		m_Renderers[2].lock()->changeNum(digid);
		digid = util::getDigid(m_Count, 1000);
		m_Renderers[3].lock()->changeNum(digid);
		digid = util::getDigid(m_Count, 10000);
		m_Renderers[4].lock()->changeNum(digid);
	}

	void AddCount::setParam(const std::vector<std::string>& param)
	{
		auto p = param;
		p.erase(p.begin(), std::next(p.begin(), 0));
		m_TextureNames.resize(12);
		for (short i = 0; i < p.size(); i++)
		{
			m_TextureNames[i] = p[i];
		}
	}

	void AddCount::addCount(bool plus, int count)
	{
		m_PlusEntity.lock()->active();
		if (!plus) {
			//カウントは引数の値
			m_Count = count;
		}
		else {
			//カウントは引数の値を加算する
			m_Count += count;
		}
	}

	void AddCount::moveCount(int setPlusXPosition, int setDigitPosition, int setPlusPosition)
	{
		m_MoveSpeedTimer.update();

		//移動幅
		auto moveRate = m_MoveSpeedTimer.rate();
		auto movePos = util::bezierCurve(moveRate, setDigitPosition, setDigitPosition, setDigitPosition, m_EntityPositionY);
		auto plusMovePos = util::bezierCurve(moveRate, setPlusPosition, setPlusPosition, setPlusPosition, m_EntityPositionY);

		//幅
		if (m_isScale) {
			m_texSize = 128 / 3;
		}
		else {
			m_texSize = 128 / 4;
		}

		//Plus表示中かどうか
		if (m_isMovePlus&& !m_isScale) {
			if (m_isdDigit) {
				//Plusの移動
				m_PlusEntity.lock()->getTransform()->m_Position = util::Vec3((m_Entity.lock()->getTransform()->m_Position.x - m_texSize * 5) - 10, plusMovePos, 0);
			}
			else {
				//Plusの移動
				m_PlusEntity.lock()->getTransform()->m_Position = util::Vec3((m_Entity.lock()->getTransform()->m_Position.x - m_texSize * 3) - 10, plusMovePos, 0);
			}
		}
		else if (m_isMovePlus && m_isScale) {
			//Plusの移動
			m_PlusEntity.lock()->getTransform()->m_Position = util::Vec3((m_Entity.lock()->getTransform()->m_Position.x - m_texSize * 5) - 10, plusMovePos, 0);
		}
		

		//桁の位置移動
		m_Counters[0].lock()->getTransform()->m_Position = util::Vec3(m_Entity.lock()->getTransform()->m_Position.x, movePos, 0);
		m_Counters[1].lock()->getTransform()->m_Position = util::Vec3(m_Entity.lock()->getTransform()->m_Position.x - m_texSize, movePos, 0);
		m_Counters[2].lock()->getTransform()->m_Position = util::Vec3(m_Entity.lock()->getTransform()->m_Position.x - m_texSize * 2, movePos, 0);
		
		if (m_isdDigit) {
			m_Counters[3].lock()->getTransform()->m_Position = util::Vec3(m_Entity.lock()->getTransform()->m_Position.x - m_texSize * 3, movePos, 0);
			m_Counters[4].lock()->getTransform()->m_Position = util::Vec3(m_Entity.lock()->getTransform()->m_Position.x - m_texSize * 4, movePos, 0);
		}
		else {
			m_Counters[3].lock()->getTransform()->m_Position = util::Vec3(m_Entity.lock()->getTransform()->m_Position.x + m_texSize * 800, movePos, 0);
			m_Counters[4].lock()->getTransform()->m_Position = util::Vec3(m_Entity.lock()->getTransform()->m_Position.x + m_texSize * 800, movePos, 0);
		}
	}

	void AddCount::changeAlpha()
	{
		//移動終わったら
		if (m_MoveSpeedTimer.isEnd())
		{
			//削除可能
			m_isDeleat = true;

			if (m_isScale) {
				m_ScoreTotalAlphaTimer.update();
				auto alphaRate = m_ScoreTotalAlphaTimer.rate();
				auto alpha = util::bezierCurve(alphaRate, 0.0f, 1.0f, 1.0f, 1.0f);
				setAlpha(alpha);
			}
			else {
				//透明にする
				m_AlphaTimer.update();
				auto alphaRate = m_AlphaTimer.rate();
				auto alpha = util::bezierCurve(alphaRate, 0.0f, 0.0f, 1.0f, 1.0f);
				setAlpha(alpha);
			}
		}
	}
	void AddCount::setAlpha(float alpha)
	{
		//表示されていたらPlusの透明度変更
		if (m_isMovePlus) {
			m_Plus.lock()->setAlpha(alpha);
		}

		for (auto r : m_Renderers)
		{
			r.lock()->setAlpha(alpha);
		}
	}

	void AddCount::changeScale() 
	{
		//Plus表示中かどうか
		if (m_isMovePlus) {
			m_PlusEntity.lock()->getTransform()->scaling(1.0f);
		}

		for (auto c : m_Counters) {
			c.lock()->getTransform()->scaling(0.7f);
		}
	}

	void AddCount::pluseTexture(std::string entityName, std::string textureName, bool isTimer)
	{
		m_isTimer = isTimer;

		//スケールの変更
		auto newTrans = *(m_Entity.lock()->getTransform());
		newTrans.scaling(0.7);
		//Entityの動的作成 newTrans
		m_PlusEntity = framework::Entity::createEntity(entityName, entityName, newTrans);
		std::vector<std::weak_ptr<Component>> damy;
		m_Plus = m_PlusEntity.lock()->
			addComponent<SpriteRenderComponent>(&damy, { textureName, "Resource/Script/Lua/UI/SpriteRenderer.lua" });
		m_Plus.lock()->init();
		m_isMovePlus = true;
	}

	bool AddCount::isDereat()
	{
		//移動が終わっていて完全に透明になったら
		return m_isDeleat && m_AlphaTimer.isEnd();
	}

	std::weak_ptr<Counter> AddCount::addNumber(const std::string & name, const util::Vec2 & pos, const util::Vec2 & scale, std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		auto entity = framework::Entity::createEntity(name, "UI", util::Transform(util::Vec3(pos.x, pos.y, 0), util::Vec3(), util::Vec3(scale.x, scale.y, 1)));
		auto counter = entity.lock()->addComponent<Counter>(componentInitalizer, m_TextureNames);
		counter.lock()->m_CallOrder = 0;

		(*componentInitalizer).emplace_back(counter);
		m_Counters.emplace_back(entity);
		return counter;
	}

	void AddCount::isSet(bool isScale, bool isAddDigit)
	{
		m_isScale = isScale;
		m_isdDigit = isAddDigit;
	}
}
