#include "HPUI.h"
#include<Framework.h>
#include<vector>
#include<Source\Util\Timer\Timer.h>
#include<Source\Component\Player\PlayerComponent.h>
#include<Source\Util\Math\Math.h>


namespace component {


	HPUI::HPUI()
		:m_Timer(30)
	{
	}

	HPUI::~HPUI()
	{
	}

	void HPUI::init()
	{
		m_Scale = 0.23f;
		auto&& scale = util::Vec3(m_Scale, m_Scale, m_Scale);

		const int texSize = 250 / 3;
		m_Entitys.emplace_back(framework::Entity::createEntity("HP1", "UI", util::Transform(util::Vec3(120, -50, 0), util::Vec3(), scale)));
		m_Entitys.emplace_back(framework::Entity::createEntity("HP2", "UI", util::Transform(util::Vec3(120 - texSize, -50, 0), util::Vec3(), scale)));
		m_Entitys.emplace_back(framework::Entity::createEntity("HP3", "UI", util::Transform(util::Vec3(120 - texSize * 2, -50, 0), util::Vec3(), scale)));

		for (size_t i = 0; i < 3; i++)
		{
			auto count = addSprite(m_Entitys[i]);
			m_Renderers.emplace_back(count);
		}
		auto player = framework::Entity::findGameObj("Player1");
		m_pPlayer = player.lock()->getComponent<PlayerComponent>();

		isPlayerDamage = false;

		player.lock()->addEvent("Damage", [&]() {
			isPlayerDamage = true;
		});
	}

	void HPUI::update()
	{
		if (m_Entitys.empty())return;

		if (!isPlayerDamage)return;
		m_Timer.update();

		m_Entitys[0].lock()->getTransform()->scaling(m_Scale * util::bezierCurve(1.0f - m_Timer.rate(), 1.0f, 2.0f, 0.0f));

		if (m_Timer.isEnd()) {
			m_Timer.init();

			destory(m_Entitys[0]);

			m_Entitys.erase(m_Entitys.begin());

			isPlayerDamage = false;
		}

	}

	std::weak_ptr<SpriteRenderComponent> HPUI::addSprite(framework::WeakEntity entity)
	{
		std::vector<std::weak_ptr<Component>> damy;
		auto sprite = entity.lock()->addComponent<SpriteRenderComponent>(&damy,{ "HP","Resource/Script/Lua/UI/SpriteRenderer.lua" });
		sprite.lock()->init();
		return sprite;
	}

}