#include "KillCounter.h"
#include<Source\Component\Util\Counter.h>
#include<Source\Util\Math\Math.h>

namespace component {


	KillCounter::KillCounter()
		:m_Timer(10)
	{
	}

	KillCounter::~KillCounter()
	{
	}

	void KillCounter::init()
	{
		m_Count = 0;

		const int texSize = 128 / 2;

		isIncrement = false;

		m_Scale = 0.5f;
		auto&& scale = util::Vec3(m_Scale, m_Scale, m_Scale);
		m_Counters.emplace_back(framework::Entity::createEntity("Count1", "UI", util::Transform(util::Vec3(1100, 0, 0), util::Vec3(), scale)));
		m_Counters.emplace_back(framework::Entity::createEntity("Count2", "UI", util::Transform(util::Vec3(1100 - texSize, 0, 0), util::Vec3(), scale)));
		m_Counters.emplace_back(framework::Entity::createEntity("Count3", "UI", util::Transform(util::Vec3(1100 - texSize * 2, 0, 0), util::Vec3(), scale)));

		for (size_t i = 0; i < 3; i++)
		{
			auto count = addCounter(m_Counters[i]);
			m_Renderers.emplace_back(count);
		}


		//全部０にする
		for (auto& render : m_Renderers)
		{
			render.lock()->changeNum(0);
		}

	}

	void KillCounter::update()
	{
		scaleUpdate();

		//位の設定
		int digid1 = util::getDigid(m_Count, 1);
		int digid2 = util::getDigid(m_Count, 10);
		int digid3 = util::getDigid(m_Count, 100);

		m_Renderers[0].lock()->changeNum(digid1);
		m_Renderers[1].lock()->changeNum(digid2);
		m_Renderers[2].lock()->changeNum(digid3);

		for (auto& counter : m_Counters)
		{
			counter.lock()->getTransform()->scaling(m_Scale * util::bezierCurve(m_Timer.rate(), 1.0f, 3.0f, 1.0f));
		}

	}

	void KillCounter::increment()
	{
		isIncrement = true;
		m_Count++;
	}

	void KillCounter::decrement()
	{
		m_Count--;
	}

	void KillCounter::scaleUpdate()
	{
		if (!isIncrement)return;
		m_Timer.update();

		if (m_Timer.isEnd()) {
			m_Timer.init();
			isIncrement = false;
		}
	}

	std::weak_ptr<Counter> KillCounter::addCounter(framework::WeakEntity entity)
	{
		std::vector<std::weak_ptr<Component>> damy;
		auto counter = entity.lock()->addComponent<Counter>(&damy);

		counter.lock()->onConect();
		counter.lock()->active();
		counter.lock()->init();
		return counter;
	}

	void KillCounter::changePos(const util::Vec2 & pos)
	{
		const int texSize = 128 / 2;
		m_Counters[0].lock()->getTransform()->m_Position = util::Vec3(pos.x, pos.y, 0);
		m_Counters[1].lock()->getTransform()->m_Position = util::Vec3(pos.x - texSize, pos.y, 0);
		m_Counters[2].lock()->getTransform()->m_Position = util::Vec3(pos.x - texSize * 2, pos.y, 0);
	}
}