#include "ComboComponent.h"
#include<Source\Util\Lua\LuaBase.h>

#include<Source\Component\Util\Counter.h>
#include<Source\Util\Math\Math.h>
#include<Source\Component\SpriteRenderComponent.h>

namespace component {
	ComboComponent::ComboComponent() 
		: m_ComboDrawTimer(30 * 8), m_ComboFrashTimer(7)
	{
		m_CallOrder = 1;
	}

	ComboComponent::~ComboComponent()
	{
	}

	void ComboComponent::init()
	{
		m_Fever = false;
		m_LarpTrans.m_Position.x = m_comboDesc.trans.m_Position.x;
		m_LarpTrans.m_Position.y = m_comboDesc.trans.m_Position.y;

		m_ComboUIEntity = framework::Entity::findGameObj("ComboUI");
		m_ComboUI = m_ComboUIEntity.lock()->getComponent<SpriteRenderComponent>();
		m_TextSize = 128 / 3;

		//全部０にする
		for (auto& render : m_Renderers)
		{
			render.lock()->changeNum(0);
		}
		m_ComboFrashTimer.init();
		m_ComboDrawTimer.init();
		m_frashCount = 0;
		m_Combo = 0;
		setCallOrder(5);

		m_ComboUI.lock()->setAlpha(0);
	}

	void ComboComponent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		//幅
		const int texSize = 128 / 2;

		//文字の大きさ
		m_Scale = 0.5f;

		//UIの位置と大きさ設定
		auto&& scale = util::Vec2(m_Scale, m_Scale);

		//秒
		auto counter = addNumber("Count1" + m_Entity.lock()->getName(), util::Vec2(m_comboDesc.trans.m_Position.x, m_comboDesc.trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count2" + m_Entity.lock()->getName(), util::Vec2(m_comboDesc.trans.m_Position.x - texSize, m_comboDesc.trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count3" + m_Entity.lock()->getName(), util::Vec2(m_comboDesc.trans.m_Position.x - texSize * 2, m_comboDesc.trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
	}

	void ComboComponent::setParam(const std::vector<std::string>& param)
	{
		//位置をパラメータで変更できる
		m_comboDesc.trans.m_Position.x = std::atoi(param[0].c_str());
		m_comboDesc.trans.m_Position.y = std::atoi(param[1].c_str());

		util::LuaBase lua(param[2]);

		//到達地点
		m_comboDesc.reachedPosition = lua.getParam<float>("ReachedPosition");
		m_comboDesc.moveSpeed = lua.getParam<float>("MoveSpeed");

		//カットインの入る経過時間
		m_comboDesc.cutInTime = lua.getParam<float>("CutInTime");

		//コンボ表示開始数値
		m_comboDesc.drawCombo = lua.getParam<float>("DrawCombo");
		m_comboDesc.maxFontSize = lua.getParam<float>("MaxFontSize");

		auto p = param;
		p.erase(p.begin(), std::next(p.begin(), 3));
		m_TextureNames.resize(11);
		for (short i = 0; i < p.size(); i++)
		{
			m_TextureNames[i] = p[i];
		}
	}


	void ComboComponent::update()
	{
		if (m_Combo > 0) {
			//フォントの大きさの変更
			if (m_Fever) {
				m_TextSize = 128 / 2;
				changeFontSize(m_comboDesc.maxFontSize);
				m_ComboUIEntity.lock()->getTransform()->m_Position.y = +50;
			}
			else {
				m_TextSize = 128 / 4;
				changeFontSize(0.8f);
				m_ComboUIEntity.lock()->getTransform()->m_Position.y = +60;
			}
		}

		//位の設定
		short digid = util::getDigid(m_Combo, 1);
		m_Renderers[0].lock()->changeNum(digid);
		digid = util::getDigid(m_Combo, 10);
		m_Renderers[1].lock()->changeNum(digid);

		if (m_Combo >= 100) {
			digid = util::getDigid(m_Combo, 100);
			m_Renderers[2].lock()->changeNum(digid);
		}

		//コンボが1より大きい
		if (isComboDraw()){
			//経過時間半分以上
			if (!rateTime())
				movePosition(m_comboDesc.reachedPosition, m_comboDesc.moveSpeed);
			else
				frashComboDraw();

			//コンボを初期化するタイマーの更新
			deleatCombo();
		}
		else
		{
			m_frashCount = 0;
			setPosition();
			m_ComboUI.lock()->setAlpha(0);
		}
	}

	void ComboComponent::addCombo(short addCombo)
	{
		if (m_Combo >= 999) 
			m_Combo = 999;
		else
			m_Combo += addCombo;

		//初期値に戻す
		m_frashCount = 0;
		m_ComboDrawTimer.init();
	}

	void ComboComponent::deleatCombo()
	{
		//タイマーの更新
		m_ComboDrawTimer.update();

		//1秒終わったかどうか
		if (m_ComboDrawTimer.isEnd()) {
			m_Combo = 0;
			setPosition();
			m_ComboDrawTimer.init();
		}
	}

	void ComboComponent::changeFontSize(float fontSize)
	{
		for (auto counters : m_Counters)
			counters.lock()->getTransform()->scaling(fontSize);
	}

	void ComboComponent::setPosition()
	{
		m_comboDesc.trans.m_Position.x = m_LarpTrans.m_Position.x;
		changePos(util::Vec2(m_comboDesc.trans.m_Position.x, m_comboDesc.trans.m_Position.y));
	}

	bool ComboComponent::rateTime()
	{
		float rate = m_ComboDrawTimer.rate();
		return rate < m_comboDesc.cutInTime;
	}

	void ComboComponent::movePosition(short stopPosition, short moveSpeed)
	{
		//不透明
		if (!m_alpha0) {
			for (auto r : m_Renderers)
			{
				r.lock()->setAlpha(1);
			}
			m_ComboUI.lock()->setAlpha(1);
		}
		else {
			for (auto r : m_Renderers)
			{
				r.lock()->setAlpha(0);
			}
		}

		if (stopPosition < m_comboDesc.trans.m_Position.x)
		{
			m_comboDesc.trans.m_Position.x -= moveSpeed;
			changePos(util::Vec2(m_comboDesc.trans.m_Position.x, m_comboDesc.trans.m_Position.y));
		}
		else
		{
			changePos(util::Vec2(stopPosition, m_comboDesc.trans.m_Position.y));
		}
	}

	bool ComboComponent::isComboDraw()
	{
		return m_Combo > m_comboDesc.drawCombo - 1;
	}

	short ComboComponent::returnCombo()
	{
		return m_Combo;
	}

	std::weak_ptr<Counter> ComboComponent::addNumber(const std::string & name, const util::Vec2 & pos, const util::Vec2& scale, std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		auto entity = framework::Entity::createEntity(name, "UI", util::Transform(util::Vec3(pos.x, pos.y, 0), util::Vec3(), util::Vec3(scale.x, scale.y, 1)));
		auto counter = entity.lock()->addComponent<Counter>(componentInitalizer, m_TextureNames);
		counter.lock()->m_CallOrder = 0;

		(*componentInitalizer).emplace_back(counter);
		m_Counters.emplace_back(entity);
		return counter;
	}

	void ComboComponent::frashComboDraw()
	{
		//タイマーの更新
		m_ComboFrashTimer.update();
		float alpharate = m_ComboFrashTimer.rate();
		float alpha = util::bezierCurve(alpharate, 1.0f, 1.0f, 0.3f, 1.0f);

		if (!m_alpha0) {
			//透明
			for (auto r : m_Renderers)
			{
				r.lock()->setAlpha(alpha);
			}
			m_ComboUI.lock()->setAlpha(alpha);
		}
		else {
			for (auto r : m_Renderers)
			{
				r.lock()->setAlpha(0);
			}
		}

		//1秒終わったかどうか
		if (m_ComboFrashTimer.isEnd()) {
			//初期値に戻す
			m_ComboFrashTimer.init();
		}
	}

	void ComboComponent::isFever(bool fever)
	{
		m_Fever = fever;
		//return !(m_Combo % 3);
	}

	void ComboComponent::alphaZero(bool alpha)
	{
		m_alpha0 = alpha;
	}

	void ComboComponent::changePos(const util::Vec2 & pos)
	{
		//位置
		m_Counters[0].lock()->getTransform()->m_Position = util::Vec3(pos.x, pos.y, 0);

		if (m_Combo >= 10) {
			m_Counters[1].lock()->getTransform()->m_Position = util::Vec3(pos.x - m_TextSize, pos.y, 0);
		}
		else {
			m_Counters[1].lock()->getTransform()->m_Position = util::Vec3(pos.x + m_TextSize * 10, pos.y, 0);
		}

		if (m_Combo >= 100) {
			m_Counters[2].lock()->getTransform()->m_Position = util::Vec3(pos.x - m_TextSize * 2, pos.y, 0);
		}
		else {
			m_Counters[2].lock()->getTransform()->m_Position = util::Vec3(pos.x + m_TextSize * 10, pos.y, 0);
		}
	}
	void ComboComponent::setCallOrder(short call)
	{
		for (auto& counter : m_Renderers)
		{
			counter.lock()->setSort(call);
		}
	}
}
