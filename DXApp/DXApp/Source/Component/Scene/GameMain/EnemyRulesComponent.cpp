#include "EnemyRulesComponent.h"
#include<Source\Component\UI\GUIComponent.h>

#include<Source\Component\Scene\GameMain\ComboComponent.h>
#include<Source\Component\Scene\GameMain\ScoreComponent.h>
#include<Source\Component\Scene\GameMain\GameTimerCompornent.h>
#include<Source\Component\Scene\GameMain\AddCount.h>


UsingNamespace;

namespace component {
	EnemyRulesComponent::EnemyRulesComponent()
		:m_comboFiverTimer(10), m_TotalTimer(10), m_TotalScoreTimer(10)
	{
	}

	EnemyRulesComponent::~EnemyRulesComponent()
	{
	}

	void EnemyRulesComponent::init()
	{
		m_EnemyRulesDesc.addCombo = m_pUI.lock()->getSlider("AddCombo");
		m_EnemyRulesDesc.addScore = m_pUI.lock()->getSlider("AddScore");
		m_EnemyRulesDesc.addTimer = 1;

		m_EnemyRulesDesc.addFeverTimer = m_pUI.lock()->getSlider("FeverTimer");
		m_EnemyRulesDesc.multiplyCount = m_pUI.lock()->getSlider("MultiplyCount");
		m_EnemyRulesDesc.fiverEnemyDeadCount = m_pUI.lock()->getSlider("FiverEnemyDeadCount");

		m_Combo = framework::Entity::findGameObj("Combo").lock()->getComponent<ComboComponent>();
		m_Fever = framework::Entity::findGameObj("Fever").lock()->getComponent<ComboComponent>();
		m_Score = framework::Entity::findGameObj("Score").lock()->getComponent<ScoreComponent>();
		m_pGameTimer = framework::Entity::findGameObj("GameTimer").lock()->getComponent<GameTimerCompornent>();

		m_ScoreCount = 1;
		m_TotalCombScore = 0;
		m_ScoreTotalCount = 0;
		m_ScoreTotalLarp = 0;
		m_TimeTotalCount = 0;
		m_IsPreComb = m_IsComb = false;
		m_isAddTimer = false;
		m_isAddScore = false;

		m_TotalTimer.init();
		m_TotalScoreTimer.init();
	}

	void EnemyRulesComponent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath });
		(*componentInitalizer).emplace_back(m_pUI);
	}

	void EnemyRulesComponent::setParam(const std::vector<std::string>& param)
	{
		m_LuaPath = param[0];
	}

	void EnemyRulesComponent::update()
	{
#ifdef _MDEBUG
		m_EnemyRulesDesc.addCombo = m_pUI.lock()->getSlider("AddCombo");
		m_EnemyRulesDesc.addScore = m_pUI.lock()->getSlider("AddScore");
		m_EnemyRulesDesc.addTimer = 1;

		m_EnemyRulesDesc.addFeverTimer = m_pUI.lock()->getSlider("FeverTimer");
		m_EnemyRulesDesc.multiplyCount = m_pUI.lock()->getSlider("MultiplyCount");
		m_EnemyRulesDesc.fiverEnemyDeadCount = m_pUI.lock()->getSlider("FiverEnemyDeadCount");
#endif
		if (m_comboFiverTimer.isEnd()) {
			m_ComboCount = 0;
			m_comboFiverTimer.init();
		}

		//タイマー追加されたら
		if (m_isAddTimer) {
			m_TotalTimer.update();
			//タイマーに合計値を追加
			if (m_TotalTimer.isEnd() && m_TimeTotalCount > 1) {
				TimeCountspown(m_TimeTotalCount, false);
				m_TimeTotalCount = 0;
				m_isAddTimer = false;
			}
		}

		//スコアが追加されたら
		if (m_isAddScore) {
			m_TotalScoreTimer.update();
			//スコアに合計値を追加
			if (m_TotalScoreTimer.isEnd() && m_ScoreTotalLarp > 1) {
				ScoreCountspown(m_ScoreTotalLarp, 200, false, false);
				m_ScoreTotalLarp = 0;
				m_isAddScore = false;
			}
		}

		m_IsPreComb = m_IsComb;
		m_IsComb = m_Combo.lock()->returnCombo() != 0;

		//コンボが切れた時にスコアに乗算値を加算
		if (!m_IsComb && m_IsPreComb) {
			combToScore();
		}
	}

	void EnemyRulesComponent::addCombo()
	{
		m_comboFiverTimer.update();
		m_ComboCount++;

		//コンボが途切れそうなときに(コンボの加算)
		if (m_Combo.lock()->rateTime())
		{
			m_Combo.lock()->setPosition();
			m_Fever.lock()->setPosition();

			m_Combo.lock()->addCombo(m_EnemyRulesDesc.addCombo);
			m_Fever.lock()->addCombo(m_EnemyRulesDesc.addCombo);
		}
		else
		{
			m_Combo.lock()->addCombo(m_EnemyRulesDesc.addCombo);
			m_Fever.lock()->addCombo(m_EnemyRulesDesc.addCombo);
		}

		//m_EnemyRulesDesc.fiverEnemyDeadCount体以上同時に死んだ場合
		if (m_ComboCount >= m_EnemyRulesDesc.fiverEnemyDeadCount) {
			m_Fever.lock()->alphaZero(false);
			m_Fever.lock()->setCallOrder(0);
			m_Combo.lock()->alphaZero(true);
			m_Combo.lock()->setCallOrder(5);

			m_Fever.lock()->isFever(true);
			m_Combo.lock()->isFever(true);
			m_ComboCount = 0;
		}
		else {
			m_Fever.lock()->alphaZero(true);
			m_Fever.lock()->setCallOrder(5);
			m_Combo.lock()->alphaZero(false);
			m_Combo.lock()->setCallOrder(0);

			m_Combo.lock()->isFever(false);
			m_Fever.lock()->isFever(false);
		}
	}

	void EnemyRulesComponent::addScore()
	{
		if (m_Combo.lock()->isComboDraw()) {
			m_ScoreCount += 0.1f;
		}
		m_Score.lock()->addScore(m_EnemyRulesDesc.addScore);
		m_TotalCombScore += m_EnemyRulesDesc.addScore;

		m_ScoreTotalCount += m_EnemyRulesDesc.addScore;
		m_ScoreTotalLarp += m_EnemyRulesDesc.addScore;
		m_TotalScoreTimer.init();
		m_isAddScore = true;
		//ScoreCountspown(m_EnemyRulesDesc.addScore, 200, false,false);
	}

	void EnemyRulesComponent::addBossScore(short killCount)
	{
		if (m_Combo.lock()->isComboDraw()) {
			m_ScoreCount += 0.1f;
		}
		const short bossScore = 1000;

		m_Score.lock()->addScore(bossScore * killCount);
		ScoreCountspown(bossScore, 200, false, true);

		m_TotalCombScore += bossScore;
	}

	void EnemyRulesComponent::addTimer()
	{
		//一定数同時に倒さないとタイムは加算されない
		//if (m_ComboCount < m_EnemyRulesDesc.fiverEnemyDeadCount) return;
		m_pGameTimer.lock()->addTimer(1);
		m_TotalTimer.init();
		m_isAddTimer = true;

		m_TimeTotalCount += 1;
		//TimeCountspown(1,false);
	}

	void EnemyRulesComponent::autScreenUI()
	{
		m_Combo.lock()->changePos(util::Vec2(0, 0));
		m_Fever.lock()->changePos(util::Vec2(0, 0));
		m_Score.lock()->changePos(util::Vec2(0, 0));
		m_pGameTimer.lock()->changePos(util::Vec2(0, 0));
	}
	bool EnemyRulesComponent::isGameTimerEnd()
	{
		return m_pGameTimer.lock()->isEnd();
	}

	void EnemyRulesComponent::combToScore()
	{
		//util::WinFunc::outLog("倍率 : " + std::to_string(m_ScoreCount));

		//なんとなくトータルの半分×倒した数
		int score = (m_TotalCombScore * 0.5) * m_ScoreCount;
		m_ScoreTotalCount += score;

		ScoreCountspown(score, 200, true, true);
		//util::WinFunc::outLog("スコア : " + std::to_string(score));
		m_Score.lock()->addScore(score);
		m_ScoreCount = 1;
		m_TotalCombScore = 0;
	}

	void EnemyRulesComponent::TimeCountspown(int addCount, bool isDigit)
	{
		//スケールの変更
		auto newTrans = *(m_Entity.lock()->getTransform());
		newTrans.m_Position.x = 700;
		newTrans.m_Position.y = 200;
		newTrans.scaling(1);

		//Entityの動的作成 newTrans
		auto addCountEntity = Entity::createEntity("AddTimeCount", "AddTimeCount", newTrans);

		std::vector<std::weak_ptr<Component>> damy;

		auto addCountComponent = addCountEntity.lock()->
			addComponent<AddCount>(&damy, { "Timer_0","Timer_1","Timer_2","Timer_3","Timer_4","Timer_5","Timer_6","Timer_7","Timer_8","Timer_9" });

		addCountComponent.lock()->init();
		addCountComponent.lock()->pluseTexture("AddTimeCount", "TimerPlus", true);
		addCountComponent.lock()->addCount(false, addCount);
		addCountComponent.lock()->isSet(false, isDigit);
	}

	void EnemyRulesComponent::ScoreCountspown(int addCount, int positionY, bool isScale, bool isDigit)
	{
		//スケールの変更
		auto newTrans = *(m_Entity.lock()->getTransform());
		newTrans.m_Position.x = 1100;
		newTrans.m_Position.y = positionY;
		newTrans.scaling(1);

		//Entityの動的作成 newTrans
		auto addCountEntity = Entity::createEntity("AddScoreCount", "AddScoreCount", newTrans);

		std::vector<std::weak_ptr<Component>> damy;

		auto addCountComponent = addCountEntity.lock()->
			addComponent<AddCount>(&damy, { "Score_0","Score_1","Score_2","Score_3","Score_4","Score_5","Score_6","Score_7","Score_8","Score_9" });

		addCountComponent.lock()->init();
		addCountComponent.lock()->pluseTexture("AddScoreCount", "ScorePlus", false);
		addCountComponent.lock()->addCount(false, addCount);
		addCountComponent.lock()->isSet(isScale,isDigit);
	}
}
