#pragma once
#include<Framework.h>
#include<vector>
#include<Source\Util\Timer\Timer.h>

namespace component {
	class Counter;

	class ScoreComponent : public framework::UpdateComponent
	{
	public:
		ScoreComponent();
		~ScoreComponent();

		void init()override;

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		//スコアを増やす
		void addScore(int addScore);

		//フォントサイズの変更
		void changeFontSize(float fontSize);

		//位置変え
		void changePos(const util::Vec2& pos);

		//スコアの取得
		static int getScore();

	private:
		/// <summary>
		/// ナンバー画像追加
		/// </summary>
		/// <param name="name"></param>
		/// <param name="pos"></param>
		std::weak_ptr<Counter> addNumber(const std::string& name, const util::Vec2& pos, const util::Vec2& scale, std::vector<std::weak_ptr<Component>>* componentInitalizer);

	public:
		//スコア
		static int m_Score;

	private:
		std::vector <std::weak_ptr<Counter>> m_Renderers;
		std::vector<framework::WeakEntity> m_Counters;
		util::Transform m_Trans;
		std::vector<std::string> m_TextureNames;

		float m_Scale;
	};
}