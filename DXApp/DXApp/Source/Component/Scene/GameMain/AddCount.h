#pragma once
#include<Framework.h>
#include<vector>
#include<Source\Util\Timer\Timer.h>

namespace component {
	class Counter;

	class AddCount : public framework::UpdateComponent
	{
	public:
		AddCount();
		~AddCount();

		void init()override;

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		/// <summary>
		/// カウントの追加
		/// </summary>
		/// /// <param name="plus">m_Countを加算するかどうか</param>
		/// <param name="count">追加するカウント</param>
		void addCount(bool plus,int count);

		/// <summary>
		/// カウントの移動
		/// </summary>
		void moveCount(int setPlusXPosition, int setDigitPosition,int setPlusPosition);

		/// <summary>
		/// 透明度の変更
		/// </summary>
		void changeAlpha();

		/// <summary>
		/// 手プラスのテクスチャ
		/// </summary>
		/// <param name="textureName">TimePlus or ScorePlus</param>
		void pluseTexture(std::string entityName, std::string textureName,bool isTimer);

		/// <summary>
		/// 削除していいかどうかのフラグ
		/// </summary>
		/// <returns></returns>
		bool isDereat();

		/// <summary>
		/// スケール値の変更(Combo合計値に必要)
		/// </summary>
		void changeScale();

		void isSet(bool isScale, bool isAddDigit);

	private:
		/// <summary>
		/// ナンバー画像追加
		/// </summary>
		/// <param name="name"></param>
		/// <param name="pos"></param>
		std::weak_ptr<Counter> addNumber(const std::string& name, const util::Vec2& pos, const util::Vec2& scale, std::vector<std::weak_ptr<Component>>* componentInitalizer);

		void setAlpha(float alpha);
	private:
		int m_Count;

		framework::WeakEntity m_PlusEntity;
		std::weak_ptr<SpriteRenderComponent> m_Plus;

		std::vector <std::weak_ptr<Counter>> m_Renderers;
		std::vector<framework::WeakEntity> m_Counters;
		std::vector<std::string> m_TextureNames;
		util::Transform m_Trans;

		util::Timer m_AlphaTimer;
		util::Timer m_MoveSpeedTimer;
		util::Timer m_ScoreTotalAlphaTimer;

		//Plusが表示されているかどうか
		bool m_isMovePlus;

		//Destory可能かどうか
		bool m_isDeleat;

		//Timerならtrue
		bool m_isTimer;

		//スケール変更するかどうか
		bool m_isScale;

		bool m_isdDigit;

		//文字間隔(MoveCountで指定)
		int m_texSize;

		//Y位置
		short m_EntityPositionY;
		float m_Scale;
	};
}
