#include "ScoreComponent.h"
#include<Source\Util\Math\Math.h>
#include<Source\Component\Util\Counter.h>

namespace component {
	int ScoreComponent::m_Score = 0;

	ScoreComponent::ScoreComponent()
	{
		m_CallOrder = 1;
	}

	ScoreComponent::~ScoreComponent()
	{
	}

	void ScoreComponent::init()
	{
		//全部０にする
		for (auto& render : m_Renderers)
		{
			render.lock()->changeNum(0);
		}
	}

	void ScoreComponent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		//幅
		const int texSize = 128 / 4;
		
		//文字の大きさ
		m_Scale = 0.5f;

		//UIの位置と大きさ設定
		auto&& scale = util::Vec2(m_Scale, m_Scale);

		//スコア
		auto counter = addNumber("Count1", util::Vec2(m_Trans.m_Position.x, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count2", util::Vec2(m_Trans.m_Position.x - texSize, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count3", util::Vec2(m_Trans.m_Position.x - texSize * 2, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count4", util::Vec2(m_Trans.m_Position.x - texSize * 3, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count5", util::Vec2(m_Trans.m_Position.x - texSize * 4, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
	}

	void ScoreComponent::setParam(const std::vector<std::string>& param)
	{
		m_Trans.m_Position.x = std::atoi(param[0].c_str());
		m_Trans.m_Position.y = std::atoi(param[1].c_str());

		auto p = param;
		p.erase(p.begin(), std::next(p.begin(), 2));
		m_TextureNames.resize(10);
		for (short i = 0; i < p.size(); i++)
		{
			m_TextureNames[i] = p[i];
		}
	}

	void ScoreComponent::update()
	{
		changeFontSize(0.5f);
		//for (auto& counter : m_Renderers)
		//{
		//	counter.lock()->m_CallOrder = 1;
		//}

		//スコア位の設定
		short digid = util::getDigid(m_Score, 1);
		m_Renderers[0].lock()->changeNum(digid);
		digid = util::getDigid(m_Score, 10);
		m_Renderers[1].lock()->changeNum(digid);
		digid = util::getDigid(m_Score, 100);
		m_Renderers[2].lock()->changeNum(digid);
		digid = util::getDigid(m_Score, 1000);
		m_Renderers[3].lock()->changeNum(digid);
		digid = util::getDigid(m_Score, 10000);
		m_Renderers[4].lock()->changeNum(digid);
	}

	void ScoreComponent::addScore(int addScore)
	{
		if (addScore < 0) {
			//もしマイナスの値が入っていたら処理しない
			return;
		}
		m_Score += addScore;
	}

	void ScoreComponent::changeFontSize(float fontSize)
	{
		for (auto counters : m_Counters)
		{
			counters.lock()->getTransform()->scaling(fontSize);
		}
	}

	std::weak_ptr<Counter> ScoreComponent::addNumber(const std::string & name, const util::Vec2 & pos, const util::Vec2& scale, std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		auto entity = framework::Entity::createEntity(name, "UI", util::Transform(util::Vec3(pos.x, pos.y, 0), util::Vec3(), util::Vec3(scale.x, scale.y, 1)));
		auto counter = entity.lock()->addComponent<Counter>(componentInitalizer, m_TextureNames);
		counter.lock()->m_CallOrder = 0;

		(*componentInitalizer).emplace_back(counter);
		m_Counters.emplace_back(entity);
		return counter;
	}

	void ScoreComponent::changePos(const util::Vec2 & pos)
	{
		//UIの位置と大きさ設定
		const int texSize = 128 / 2;

		//位置
		m_Counters[0].lock()->getTransform()->m_Position = util::Vec3(pos.x, pos.y, 0);
		m_Counters[1].lock()->getTransform()->m_Position = util::Vec3(pos.x - texSize, pos.y, 0);
		m_Counters[2].lock()->getTransform()->m_Position = util::Vec3(pos.x - texSize * 2, pos.y, 0);
		m_Counters[3].lock()->getTransform()->m_Position = util::Vec3(pos.x - texSize * 3, pos.y, 0);
		m_Counters[4].lock()->getTransform()->m_Position = util::Vec3(pos.x - texSize * 4, pos.y, 0);
	}

	int ScoreComponent::getScore()
	{
		return m_Score;
	}
}
