#pragma once
#include<Framework.h>
#include<vector>
#include<Source\Util\Timer\Timer.h>

namespace component {
	class Counter;

	class KillCounter : public framework::UpdateComponent
	{
	public:
		KillCounter();
		~KillCounter();

		void init()override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

		void increment();

		void decrement();


		void changePos(const util::Vec2& pos);
	private:

		void scaleUpdate();

		std::weak_ptr<Counter> addCounter(framework::WeakEntity entity);


	private:

		//!キル数
		int m_Count;

		bool isIncrement;

		util::Timer m_Timer;

		std::vector < std::weak_ptr<Counter>> m_Renderers;

		std::vector<framework::WeakEntity> m_Counters;

		float m_Scale;
	};


}