#pragma once
#include<Framework.h>
#include<vector>
#include<Source\Util\Timer\Timer.h>

namespace component {
	class Counter;

	class GameTimerCompornent : public framework::UpdateComponent
	{
	public:
		GameTimerCompornent();
		~GameTimerCompornent();

		void init()override;

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		//カウントを増やす
		void addTimer(short incrementSecondCount);

		//カウントを減らす
		void countDecrement(short decrementSecondCount,short decrementMinitchCount);

		//タイマーが増える
		void timeIncrement();

		//タイマーが減る
		void timeDecrement();

		void changeFontSize(float secondFontSize, float fontSize);

		void changePos(const util::Vec2& pos);

		bool isEnd();

		bool IsStartDecrement(bool start);

		/// 時間経過率取得
		/// </summary>
		/// <returns></returns>
		float getTimeRate();
	private:
		/// <summary>
		/// ナンバー画像追加
		/// </summary>
		/// <param name="name"></param>
		/// <param name="pos"></param>
		std::weak_ptr<Counter> addNumber(const std::string& name,const util::Vec2& pos, const util::Vec2& scale, std::vector<std::weak_ptr<Component>>* componentInitalizer);
	private:
		short m_LarpSecondCount;
		short m_LarpMinitchCount;
		bool m_Decrement;

		//ミリ秒(毎フレ)
		short m_Count;

		//秒
		short m_SecondCount;

		//分
		short m_MinitchCount;

		std::weak_ptr<SpriteRenderComponent> m_MinitchColon;
		std::weak_ptr<SpriteRenderComponent> m_SecondColon;
		std::vector < std::weak_ptr<Counter>> m_Renderers;
		std::vector<framework::WeakEntity> m_Counters;
		util::Transform m_Trans;
		std::vector<std::string> m_TextureNames;

		short m_MaxTime;

		float m_Scale;
	};
}