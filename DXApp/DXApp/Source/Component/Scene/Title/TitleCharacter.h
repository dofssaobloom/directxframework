#pragma once
#include<Framework.h>
#include<Source\Component\Animation\AnimatorComponent.h>
#include<memory>
#include<Source\Util\Math\Randam.h>
#include<Source\Util\Timer\Timer.h>

namespace component {

	class TitleCharacter : public framework::UpdateComponent
	{
	public:
		TitleCharacter() {
			m_pTimer = std::make_unique<util::Timer>(m_Rand.next(0,50));
		
		}
		~TitleCharacter() {}

		void init()override {
			m_Animator = m_Entity.lock()->getComponent<AnimatorComponent>();
			m_Animator.lock()->changeAnime(m_FirstName);
			m_Animator.lock()->deActive();
		}

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param)override {
			m_FirstName = param[0];
		}

		// UpdateComponent を介して継承されました
		virtual void update() override {
			m_pTimer->update();

			if (m_pTimer->isEnd()) {
				m_Animator.lock()->active();
			}
		}

	private:
		std::string m_FirstName;
		util::Randam m_Rand;
		std::unique_ptr<util::Timer> m_pTimer;
		 
		std::weak_ptr<AnimatorComponent> m_Animator;


	};
}