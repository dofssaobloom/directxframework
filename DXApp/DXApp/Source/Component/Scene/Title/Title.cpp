#include "Title.h"
#include<Source\Util\Math\Math.h>
#include<Source\Component\SceneChangeComponent.h>
#include<Source\Application\Scene\SceneDelivery.h>
#include<Source\Application\Device\Input.h>
#include<Source\Component\FadeComponent.h>
#include<Source\Task\TaskManager.h>
#include<Source\Util\IO\BinaryLoader.h>
#include<Source\Resource\ResourceManager.h>

namespace component {
	Title::Title() : m_selectorSpeedUpTimer(2), m_selectorLongMoveTimer(5),
		m_settingButtonMoveStartTimer(5), m_howtoplayButtonMoveStartTimer(10), m_creditButtonMoveStartTimer(15),
		m_gameStartMoveTimer(15), m_settingMoveTimer(15), m_howtoplayMoveTimer(15), m_creditMoveTimer(15),
		m_frashUITimer(10), m_nextSceneTimer(30 * 2), m_scalingTitleLogoTimer(15), m_ExplosionAlphaTimer(7), m_BubbleMoveTimer(50), m_BubbleMoveSlowTimer(60)
	{
		m_CallOrder = 10;
	}

	Title::~Title()
	{

	}

	void Title::init()
	{
		m_TitleEntity = framework::Entity::findGameObj("Title");
		m_TitleExplosionEntity = framework::Entity::findGameObj("TitleExplosion");
		m_TitleLogoEntity = framework::Entity::findGameObj("TitleLogo");

		m_BubbleLeftUpEntity = framework::Entity::findGameObj("Bubble_leftup");
		m_MiniBubbleLeftUpEntity = framework::Entity::findGameObj("MiniBubble_leftup");
		m_BubbleCenterUpEntity = framework::Entity::findGameObj("Bubble_centerup");
		m_BubbleRightUpEntity = framework::Entity::findGameObj("Bubble_rightup");
		m_MiniBubbleLeftDownEntity = framework::Entity::findGameObj("MiniBubble_leftdown");
		m_BubbleCenterDownEntity = framework::Entity::findGameObj("Bubble_centerdown");
		m_MiniBubbleRightDownEntity = framework::Entity::findGameObj("MiniBubble_rightdown");


		m_GameStartEntity = framework::Entity::findGameObj("GameStart");
		m_SettingEntity = framework::Entity::findGameObj("Setting");
		m_HowToPlayEntity = framework::Entity::findGameObj("HowToPlay");
		m_CreditEntity = framework::Entity::findGameObj("Credit");

		m_pRenderers.emplace_back(m_GameStart = m_GameStartEntity.lock()->getComponent<SpriteRenderComponent>());
		m_pRenderers.emplace_back(m_Setting = m_SettingEntity.lock()->getComponent<SpriteRenderComponent>());
		m_pRenderers.emplace_back(m_HowToPlay = m_HowToPlayEntity.lock()->getComponent<SpriteRenderComponent>());
		m_pRenderers.emplace_back(m_Credit = m_CreditEntity.lock()->getComponent<SpriteRenderComponent>());

		m_isGameStartFrash = m_SettingFrash = m_HowToPlayFrash = m_CreditFrash = false;
		
		m_TitleExplosion = m_TitleExplosionEntity.lock()->getComponent<SpriteRenderComponent>();

		m_Fade = framework::Entity::findGameObj("Fade").lock()->getComponent<FadeComponent>();
		m_pSceneChange = framework::Entity::findGameObj("GlobalEvent").lock()->getComponent<SceneChangeComponent>();

		m_Entity.lock()->addEvent("Carsol", [&]() {
			framework::ResourceManager::getInstance()->playSound("CarsolSelect");
			framework::ResourceManager::getInstance()->setVolume("CarsolSelect", -1500);
			framework::ResourceManager::getInstance()->setDoppler("CarsolSelect", 0);
		});

		m_Entity.lock()->addEvent("Enter", [&]() {
			framework::ResourceManager::getInstance()->playSound("Entry");
			framework::ResourceManager::getInstance()->setVolume("Entry", -1500);
			framework::ResourceManager::getInstance()->setDoppler("Entry", 0);
		});

		m_Entity.lock()->onEvent("TitleBGM");
		m_isBubbleMove = m_isBubbleSlowMove = false;

		m_isSelecterMove = true;

		m_isUpZero = m_isDownZero = true;

		m_selectorSpeedUpTimer.init();
		m_selectorLongMoveTimer.init();

		m_frashUITimer.init();
		m_nextSceneTimer.init();

		m_gameStartMoveTimer.init();
		m_settingMoveTimer.init();
		m_howtoplayMoveTimer.init();
		m_creditMoveTimer.init();

		m_settingButtonMoveStartTimer.init();
		m_howtoplayButtonMoveStartTimer.init();
		m_creditButtonMoveStartTimer.init();

		m_scalingTitleLogoTimer.init();
		m_ExplosionAlphaTimer.init();
		m_BubbleMoveTimer.init();
		m_BubbleMoveSlowTimer.init();
		m_Fade.lock()->onStart();
		byteLoad();

		m_Manu.addMenu([&]() {
			gameStart();
		});
		m_Manu.addMenu([&]() {
			setting();
		});
		m_Manu.addMenu([&]() {
			howtoplay();
		});
		m_Manu.addMenu([&]() {
			credit();
		});

		m_Manu.init();
	}

	void Title::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
	}

	void Title::setParam(const std::vector<std::string>& param)
	{
	}

	void Title::update()
	{
		moveBubble();
		//フェードが終わったら
		if (m_Fade.lock()->isEnd()) {
			moveButtons();
			scaleTitleLogoSelecterStart();
		}

		//ボタンが押されたら
		if (m_isGameStartFrash || m_SettingFrash) {
			m_frashUITimer.update();
			m_nextSceneTimer.update();
		}
		frashUI();

		nextScene(m_nextScene);
	}

	void Title::moveButtons() {
		//GameStartUIの移動
		m_gameStartMoveTimer.update();
		auto gameStartRate = m_gameStartMoveTimer.rate();
		auto gameStartMovespd = util::bezierCurve(gameStartRate, 210.0f, 190.0f, 0.0f, m_Entity.lock()->getTransform()->m_Position.x);
		m_GameStartEntity.lock()->getTransform()->m_Position.x = gameStartMovespd;

		//SettingUIの移動
		m_settingButtonMoveStartTimer.update();
		if (m_settingButtonMoveStartTimer.isEnd()) {
			m_settingMoveTimer.update();
			auto settingRate = m_settingMoveTimer.rate();
			auto settingMovespd = util::bezierCurve(settingRate, 230.0f, 200.0f, 0.0f, m_Entity.lock()->getTransform()->m_Position.x);
			m_SettingEntity.lock()->getTransform()->m_Position.x = settingMovespd;
		}

		//HowToPlayUIの移動
		m_howtoplayButtonMoveStartTimer.update();
		if (m_howtoplayButtonMoveStartTimer.isEnd()) {
			m_howtoplayMoveTimer.update();
			auto howtoplayRate = m_howtoplayMoveTimer.rate();
			auto howtoplayMovespd = util::bezierCurve(howtoplayRate, 250.0f, 200.0f, 0.0f, m_Entity.lock()->getTransform()->m_Position.x);
			m_HowToPlayEntity.lock()->getTransform()->m_Position.x = howtoplayMovespd;
		}

		//CreditUIの移動
		m_creditButtonMoveStartTimer.update();
		if (m_creditButtonMoveStartTimer.isEnd()) {
			m_creditMoveTimer.update();
			auto creditRate = m_creditMoveTimer.rate();
			auto creditMovespd = util::bezierCurve(creditRate, 270.0f, 200.0f, 0.0f, m_Entity.lock()->getTransform()->m_Position.x);
			m_CreditEntity.lock()->getTransform()->m_Position.x = creditMovespd;
		}
	}

	void Title::frashUI() {
		auto frashRate = m_frashUITimer.rate();
		auto frash = util::bezierCurve(frashRate, 1.0, 0.0, 1.0);

		if (m_isGameStartFrash) {
			m_pRenderers[0].lock()->setAlpha(frash);
		}
		if (m_SettingFrash) {
			m_pRenderers[1].lock()->setAlpha(frash);
		}
		if (m_HowToPlayFrash) {
			m_pRenderers[2].lock()->setAlpha(frash);
		}
		if (m_CreditFrash) {
			m_pRenderers[2].lock()->setAlpha(frash);
		}

		if (m_frashUITimer.isEnd()) {
			m_frashUITimer.init();
		}
	}

	void Title::scaleTitleLogoSelecterStart() {
		//UIが到着後、TitleLogoの大きさ変更とUIのセレクトが可能.
		if (m_settingMoveTimer.isEnd()) {
			m_scalingTitleLogoTimer.update();
			auto scaleRate = m_scalingTitleLogoTimer.rate();
			auto scaleTitleLogo = util::bezierCurve(scaleRate, 0.5f, 0.7f, 0.3f, 0.0f);
			m_TitleLogoEntity.lock()->getTransform()->scaling(scaleTitleLogo);

			if (m_isSelecterMove) {
				selectUI();
			}
		}

		//TitleExplosionの透明度変更
		if (m_scalingTitleLogoTimer.isEnd()) {
			m_ExplosionAlphaTimer.update();
			auto ExplosionRate = m_ExplosionAlphaTimer.rate();
			auto alphaExplosion = util::bezierCurve(ExplosionRate, 1.0f, 2.0f, 0.0f);
			auto scalingExplosion = util::bezierCurve(ExplosionRate, 0.5f, 0.7f,0.5f,0.0f);
			m_TitleExplosion.lock()->setAlpha(alphaExplosion);
			m_TitleExplosionEntity.lock()->getTransform()->scaling(scalingExplosion);

			auto scaleTitle = util::bezierCurve(ExplosionRate, 0.5f, 0.3f, 0.7f, 0.5f);
			m_TitleEntity.lock()->getTransform()->m_Scale.x = 0.5f;
			m_TitleEntity.lock()->getTransform()->m_Scale.y = scaleTitle;
		}
	}

	void Title::moveBubble()
	{
		m_BubbleMoveTimer.update();
		if (m_BubbleMoveTimer.isEnd()) {
			m_isBubbleMove = !m_isBubbleMove;
			m_BubbleMoveTimer.init();
		}

		m_BubbleMoveSlowTimer.update();
		if (m_BubbleMoveSlowTimer.isEnd()) {
			m_isBubbleSlowMove = !m_isBubbleSlowMove;
			m_BubbleMoveSlowTimer.init();
		}

		float move = 1.0f;
		if (m_isBubbleMove) {
			m_BubbleLeftUpEntity.lock()->getTransform()->m_Position.y += move;
			m_BubbleCenterDownEntity.lock()->getTransform()->m_Position.y -= move;
			m_MiniBubbleRightDownEntity.lock()->getTransform()->m_Position.y += move;
		}
		else {
			m_BubbleLeftUpEntity.lock()->getTransform()->m_Position.y -= move;
			m_BubbleCenterDownEntity.lock()->getTransform()->m_Position.y += move;
			m_MiniBubbleRightDownEntity.lock()->getTransform()->m_Position.y -= move;
		}

		float move_slow = 0.5f;
		if (m_isBubbleSlowMove) {
			m_MiniBubbleLeftUpEntity.lock()->getTransform()->m_Position.y -= move_slow;
			m_BubbleCenterUpEntity.lock()->getTransform()->m_Position.y += move_slow;
			m_BubbleRightUpEntity.lock()->getTransform()->m_Position.y -= move_slow;
			m_MiniBubbleLeftDownEntity.lock()->getTransform()->m_Position.y += move_slow;
		}
		else {
			m_MiniBubbleLeftUpEntity.lock()->getTransform()->m_Position.y += move_slow;
			m_BubbleCenterUpEntity.lock()->getTransform()->m_Position.y -= move_slow;
			m_BubbleRightUpEntity.lock()->getTransform()->m_Position.y += move_slow;
			m_MiniBubbleLeftDownEntity.lock()->getTransform()->m_Position.y -= move_slow;
		}
	}

	void Title::byteLoad()
	{
		util::BinaryLoader loader("Resource/GameConfig.byte");
		std::vector<float> result;
		loader.load<float>(&result);
		if (!result.empty()) {
			m_IsFullScreen = result[5];
			m_IsSmall = result[6];
			m_IsMiddle = result[7];
			m_IsBig = result[8];
		}

		framework::DirectXInstance::getInstance()->changeWindowMode(m_IsFullScreen);

		if(m_IsSmall)
			configScreenSize(640, 360);
		else if(m_IsMiddle)
			configScreenSize(1280, 720);
		else if(m_IsBig)
			configScreenSize(1920, 1080);
	}

	void Title::configScreenSize(int width, int height)
	{
		framework::TaskManager::getInstance()->resizeBuffer(util::Vec2(width, height));
	}

	void Title::selectUI() {
		auto left = application::Input::leftVelocity();
		auto isLeftUp = application::Input::leftUpTrigger();
		auto isLeftDown = application::Input::leftDownTrigger();

		if (m_selectorLongMoveTimer.isEnd()) {
			m_selectorSpeedUpTimer.update();
			//長押しの速度
			if (m_selectorSpeedUpTimer.isEnd() && left.y < 0)
			{
				m_Entity.lock()->onEvent("Carsol");
				m_Manu.enter();
				m_selectorSpeedUpTimer.init();
			}
			else if (m_selectorSpeedUpTimer.isEnd() && left.y > 0)
			{
				m_Entity.lock()->onEvent("Carsol");
				m_Manu.back();
				m_selectorSpeedUpTimer.init();
			}
			else if (left.y == 0) {
				m_selectorLongMoveTimer.init();
			}
		}
		else
		{	
			//長押し
			if (left.y < 0 || left.y > 0) {
				m_selectorLongMoveTimer.update();
			}
		}

		//普通の速度
		if (isLeftDown)
		{
			if (m_isDownZero) {
				m_selectorLongMoveTimer.init();
				m_Entity.lock()->onEvent("Carsol");
				m_Manu.enter();
				m_isDownZero = false;
			}
		}
		else {
			m_isDownZero = true;
		}
		if (isLeftUp)
		{
			if (m_isUpZero) {
				m_selectorLongMoveTimer.init();
				m_Entity.lock()->onEvent("Carsol");
				m_Manu.back();
				m_isUpZero = false;
			}
		}
		else {
			m_isUpZero = true;
		}

		if (application::Input::isJump()) {
			m_Manu.applay();
		}

		//ボタンの大きさ変換
		for (size_t i = 0; i < m_pRenderers.size(); i++)
		{
			if (m_Manu.getCount() == i) {
				m_pRenderers[i].lock()->getGameObj().lock()->getTransform()->scaling(0.6);
			}
			else {
				m_pRenderers[i].lock()->getGameObj().lock()->getTransform()->scaling(0.5);
			}
		}
	}

	void Title::gameStart() {
		if (!m_isGameStartFrash) {
			m_Entity.lock()->onEvent("Enter");
			m_Fade.lock()->onStart();
			m_nextScene = "StageBeta";

			m_isGameStartFrash = true;
			m_SettingFrash = false;
			m_HowToPlayFrash = false;
			m_CreditFrash = false;

			m_isSelecterMove = false;
		}
	}

	void Title::setting() {
		if (!m_SettingFrash) {
			m_Entity.lock()->onEvent("Enter");
			m_Fade.lock()->onStart();
			m_nextScene = "ConfigScene";

			m_isGameStartFrash = false;
			m_SettingFrash = true;
			m_HowToPlayFrash = false;
			m_CreditFrash = false;

			m_isSelecterMove = false;
		}
	}

	void Title::howtoplay()
	{
		if (!m_HowToPlayFrash) {
			m_Entity.lock()->onEvent("Enter");
			m_Fade.lock()->onStart();
			m_nextScene = "ConfigScene";

			m_isGameStartFrash = false;
			m_SettingFrash = false;
			m_HowToPlayFrash = true;
			m_CreditFrash = false;

			m_isSelecterMove = false;
		}
	}

	void Title::credit()
	{
		if (!m_HowToPlayFrash) {
			m_Entity.lock()->onEvent("Enter");
			m_Fade.lock()->onStart();
			m_nextScene = "CreditScene";

			m_isGameStartFrash = false;
			m_SettingFrash = false;
			m_HowToPlayFrash = false;
			m_CreditFrash = true;

			m_isSelecterMove = false;
		}
	}

	void Title::nextScene(std::string sceneName) {
		if (m_Fade.lock()->isEnd() && m_Fade.lock()->isOutFade()) {
			framework::Scene::m_NextSceneName = sceneName;
			m_pSceneChange.lock()->changeSceneNotification();
		}
	}
}
