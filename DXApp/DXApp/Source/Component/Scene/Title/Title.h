#pragma once
#include<Framework.h>
#include<vector>
#include<Source\Util\MenuVector.h>

namespace component {
	class FadeComponent;
	class SceneChangeComponent;

	class Title : public framework::UpdateComponent
	{
	public:
		Title();
		~Title();

		void init()override;

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

		virtual void setParam(const std::vector<std::string>& param);

		//settingUIとgamestartUIの大きさ変更
		void gameStart();
		void setting();
		void howtoplay();
		void credit();

	private:
		//どのUIを選択しているか
		void selectUI();

		//次のシーン
		void nextScene(std::string sceneName);

		//UIの動き
		void moveButtons();

		//UIの点滅
		void frashUI();

		//TitleLogoのスケール変更
		void scaleTitleLogoSelecterStart();

		//Bubblesの移動
		void moveBubble();

		//byteのロード
		void byteLoad();

		//解像度の設定
		void configScreenSize(int width, int height);
	private:
		std::string m_nextScene;
		std::weak_ptr<SceneChangeComponent> m_pSceneChange;
		std::weak_ptr<FadeComponent> m_Fade;

		//セレクトの長押し、セレクトのスピードアップ
		util::Timer m_selectorLongMoveTimer;
		util::Timer m_selectorSpeedUpTimer;

		//点滅と次のシーンへ遷移する時間
		util::Timer m_frashUITimer;
		util::Timer m_nextSceneTimer;

		//TitleTextures
		framework::WeakEntity m_TitleEntity;
		framework::WeakEntity m_TitleExplosionEntity;
		framework::WeakEntity m_TitleLogoEntity;
		std::weak_ptr<SpriteRenderComponent> m_TitleExplosion;

		//Titleロゴのスケール変更用
		util::Timer m_scalingTitleLogoTimer;

		//TitleExplosionの透明度変更用
		util::Timer m_ExplosionAlphaTimer;

		//ボタン
		framework::WeakEntity m_GameStartEntity;
		framework::WeakEntity m_SettingEntity;
		framework::WeakEntity m_HowToPlayEntity;
		framework::WeakEntity m_CreditEntity;
		std::weak_ptr<SpriteRenderComponent> m_GameStart;
		std::weak_ptr<SpriteRenderComponent> m_Setting;
		std::weak_ptr<SpriteRenderComponent> m_HowToPlay;
		std::weak_ptr<SpriteRenderComponent> m_Credit;

		//TitleTexture,GameStartUI,SettingUIの移動用
		util::Timer m_gameStartMoveTimer;
		util::Timer m_settingMoveTimer;
		util::Timer m_howtoplayMoveTimer;
		util::Timer m_creditMoveTimer;

		//SettingUIが移動を始める時間
		util::Timer m_settingButtonMoveStartTimer;
		util::Timer m_howtoplayButtonMoveStartTimer;
		util::Timer m_creditButtonMoveStartTimer;

		//点滅
		bool m_isGameStartFrash, m_SettingFrash, m_HowToPlayFrash, m_CreditFrash;

		//Bubbles
		framework::WeakEntity m_BubbleLeftUpEntity;
		framework::WeakEntity m_MiniBubbleLeftUpEntity;
		framework::WeakEntity m_BubbleCenterUpEntity;
		framework::WeakEntity m_BubbleRightUpEntity;
		framework::WeakEntity m_MiniBubbleLeftDownEntity;
		framework::WeakEntity m_BubbleCenterDownEntity;
		framework::WeakEntity m_MiniBubbleRightDownEntity;

		//バブルの動き二種類
		util::Timer m_BubbleMoveTimer;
		util::Timer m_BubbleMoveSlowTimer;
		bool m_isBubbleMove, m_isBubbleSlowMove;

		util::MenuVector m_Manu;
		std::vector<std::weak_ptr<SpriteRenderComponent>> m_pRenderers;

		//セレクトを動かせるかどうか
		bool m_isSelecterMove;

		//トリガー判定
		bool m_isUpZero, m_isDownZero;

		//グラフィックエフェクト
		bool m_IsFullScreen;
		bool m_IsSmall;
		bool m_IsMiddle;
		bool m_IsBig;
	};
}
