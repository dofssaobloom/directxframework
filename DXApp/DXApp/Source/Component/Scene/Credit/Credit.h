#pragma once
#include<Framework.h>
#include<vector>
#include<Source\Util\MenuVector.h>

namespace component {
	class FadeComponent;
	class SceneChangeComponent;

	class Credit : public framework::UpdateComponent
	{
	public:
		Credit();
		~Credit();

		void init()override;

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

		virtual void setParam(const std::vector<std::string>& param);
	private:

		//次のシーン
		void nextScene(std::string sceneName);

		//byteのロード
		void byteLoad();

		//解像度の設定
		void configScreenSize(int width, int height);
	private:
		std::string m_nextScene;
		std::weak_ptr<SceneChangeComponent> m_pSceneChange;
		std::weak_ptr<FadeComponent> m_Fade;

		bool m_isEnd;

		//グラフィックエフェクト
		bool m_IsFullScreen;
		bool m_IsSmall;
		bool m_IsMiddle;
		bool m_IsBig;
	};
}
