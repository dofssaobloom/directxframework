#include "Credit.h"
#include<Source\Util\Math\Math.h>
#include<Source\Component\SceneChangeComponent.h>
#include<Source\Application\Scene\SceneDelivery.h>
#include<Source\Application\Device\Input.h>
#include<Source\Component\FadeComponent.h>
#include<Source\Task\TaskManager.h>
#include<Source\Util\IO\BinaryLoader.h>
#include<Source\Resource\ResourceManager.h>

namespace component {
	Credit::Credit()
	{
	}

	Credit::~Credit()
	{
	}

	void Credit::init()
	{
		m_Fade = framework::Entity::findGameObj("Fade").lock()->getComponent<FadeComponent>();
		m_pSceneChange = framework::Entity::findGameObj("GlobalEvent").lock()->getComponent<SceneChangeComponent>();
		m_Fade.lock()->onStart();
		byteLoad();

		m_isEnd = false;
	}

	void Credit::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
	}

	void Credit::update()
	{
		auto isKeyAny = application::Input::isKeyAny();
		auto isPadAny = application::Input::isPadAny();
		
		nextScene("TitleScene");
		if (m_isEnd)return;

		if (isKeyAny || isPadAny) 
		{
			m_Fade.lock()->onStart();
			m_isEnd = true;
		}
	}

	void Credit::setParam(const std::vector<std::string>& param)
	{
	}

	void Credit::nextScene(std::string sceneName)
	{
		if (m_isEnd && m_Fade.lock()->isEnd() && m_Fade.lock()->isOutFade()) {
			framework::Scene::m_NextSceneName = sceneName;
			m_pSceneChange.lock()->changeSceneNotification();
		}
	}

	void Credit::byteLoad()
	{
		util::BinaryLoader loader("Resource/GameConfig.byte");
		std::vector<float> result;
		loader.load<float>(&result);
		if (!result.empty()) {
			m_IsFullScreen = result[5];
			m_IsSmall = result[6];
			m_IsMiddle = result[7];
			m_IsBig = result[8];
		}

		framework::DirectXInstance::getInstance()->changeWindowMode(m_IsFullScreen);

		if (m_IsSmall)
			configScreenSize(640, 360);
		else if (m_IsMiddle)
			configScreenSize(1280, 720);
		else if (m_IsBig)
			configScreenSize(1920, 1080);
	}

	void Credit::configScreenSize(int width, int height)
	{
		framework::TaskManager::getInstance()->resizeBuffer(util::Vec2(width, height));
	}
}
