#include "ResultTest.h"
#include<Source\Util\Math\Math.h>
#include<Source\Component\Util\Counter.h>
#include<Source\Component\Scene\GameMain\ScoreComponent.h>
#include<Source\Component\Scene\GameMain\GameTimerCompornent.h>

#include<Source\Component\SceneChangeComponent.h>
#include<Source\Application\Scene\SceneDelivery.h>
#include<Source\Application\Device\Input.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Component\Scene\GameMain\GameTimerCompornent.h>
#include<Source\Component\FadeComponent.h>
#include "..\Title\Title.h"
#include<Source\Resource\ResourceManager.h>
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>


namespace component {
	ResultTest::ResultTest()
		:m_ScaleEvalueTimer(15), m_BornEvalueTimer(10)
	{
		m_CallOrder = 10;
	}

	ResultTest::~ResultTest()
	{

	}

	void ResultTest::init()
	{
		m_Entity.lock()->addEvent("Entry", [&]() {
			framework::ResourceManager::getInstance()->playSound("Entry");
			framework::ResourceManager::getInstance()->setVolume("Entry", -1500);
			framework::ResourceManager::getInstance()->setDoppler("Entry", 0);
		});
		m_Entity.lock()->addEvent("Carsol", [&]() {
			framework::ResourceManager::getInstance()->playSound("CarsolSelect");
			framework::ResourceManager::getInstance()->setVolume("CarsolSelect", -1500);
			framework::ResourceManager::getInstance()->setDoppler("CarsolSelect", 0);
		});
		m_Entity.lock()->addEvent("CheerGroap", [&]() {
			framework::ResourceManager::getInstance()->playSound("CheerGroap");
			framework::ResourceManager::getInstance()->setVolume("CheerGroap", -1300);
			framework::ResourceManager::getInstance()->setDoppler("CheerGroap", 0);
		});
		m_Entity.lock()->addEvent("Sympathy", [&]() {
			framework::ResourceManager::getInstance()->playSound("Sympathy");
			framework::ResourceManager::getInstance()->setVolume("Sympathy", 0);
			framework::ResourceManager::getInstance()->setDoppler("Sympathy", 0);
		});


		m_pEvalueEntity = framework::Entity::findGameObj("ClearEvalue");
		m_Fade = framework::Entity::findGameObj("Fade").lock()->getComponent<FadeComponent>();

		//クリア評価
		m_CleawrEvalue = m_pEvalueEntity.lock()->getComponents<SpriteRenderComponent>();

		int SS = 35000;
		int S = 25000;
		int A = 15000;
		int B = 8000;
		int C = 4000;

		if (ScoreComponent::m_Score >= SS) {
			m_Evalue = "SS";
		}
		else if (ScoreComponent::m_Score >= S) {
			m_Evalue = "S";
		}
		else if (ScoreComponent::m_Score >= A) {
			m_Evalue = "A";
		}
		else if (ScoreComponent::m_Score >= B) {
			m_Evalue = "B";
		}
		else if (ScoreComponent::m_Score >= C) {
			m_Evalue = "C";
		}else {
			m_Evalue = "D";
		}

		auto find = std::find_if(m_CleawrEvalue.begin(), m_CleawrEvalue.end(), [&](std::weak_ptr<SpriteRenderComponent>& sprite) {
			return sprite.lock()->getSpriteName() == m_Evalue;
		});

		if (find != m_CleawrEvalue.end()) {
			if (!find->expired()) {
				find->lock()->active();
				m_pEvalueSprite = *find;
			}
		}

		auto menuWindow = framework::Entity::findGameObj("MenuWindow").lock()->getComponents<SpriteRenderComponent>();

		m_pCharaIcon = framework::Entity::findGameObj("MenuWindow");
		m_CharaIconY = m_pCharaIcon.lock()->getTransform()->m_Position.y;

		short iconCalloader = 12;
		std::string windowName;
		if (m_Evalue == "B" || m_Evalue == "A" || m_Evalue == "S" || m_Evalue == "SS") {
			windowName = "Menu_Window";
			m_FirstMotionName = "Player_Win1";
			m_CharaIconFirstY = m_pCharaIcon.lock()->getTransform()->m_Position.y += 500;
			iconCalloader = 0;
			m_EffectName = "Hanabi";
		}
		else {
			windowName = "Menu_Failed_Window";
			m_FirstMotionName = "Player_Failed1";
			m_CharaIconFirstY = m_pCharaIcon.lock()->getTransform()->m_Position.y -= 500;
			m_EffectName = "Rain";
		}


		//m_FirstMotionName = "Player_Win1";
		find = std::find_if(menuWindow.begin(), menuWindow.end(), [&](std::weak_ptr<SpriteRenderComponent>& sprite) {
			return sprite.lock()->getSpriteName() == windowName;
		});

		if (find != menuWindow.end()) {
			if (!find->expired()) {
				find->lock()->active();
				find->lock()->setSort(iconCalloader);
				find->lock()->setAlpha(0);
				m_pCharaIconSprite = *find;
			}
		}

		m_IsRetry = true;
		m_Retry = framework::Entity::findGameObj("GoRetry").lock()->getComponent<SpriteRenderComponent>();
		m_Title = framework::Entity::findGameObj("GoTitle").lock()->getComponent<SpriteRenderComponent>();

		m_pSceneChange = framework::Entity::findGameObj("GlobalEvent").lock()->getComponent<SceneChangeComponent>();

		CameraComponent::getMainCamera().lock()->setLookOffset(util::Vec3(-50, 80, 0));


		m_isMotionChange = false;
		m_pAnimator = framework::Entity::findGameObj("Player1").lock()->getComponent<AnimatorComponent>();

		m_pAnimator.lock()->changeAnime(m_FirstMotionName);

		m_pAnimator.lock()->onStop();

		m_FirstMotionName.erase(std::prev(m_FirstMotionName.end(), 1));

		//全部０にする
		for (auto& render : m_Renderers)
		{
			render.lock()->setSort(5);
			render.lock()->changeNum(0);
		}

		m_ScoreNamber = 0;
		m_EvalueScale = 0.0f;
		m_IsScale = false;
		m_IsMaxScore = false;
		m_isEvalueSound = false;
		m_pEvalueEntity.lock()->getTransform()->scaling(m_EvalueScale);

		m_ScaleEvalueTimer.init();
		m_BornEvalueTimer.init();
		
		m_Fade.lock()->onStart();
		m_pTimer = std::make_unique<util::Timer>(30);
		m_pIconTimer = std::make_unique<util::Timer>(20);

	}

	void ResultTest::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		//文字の大きさ
		m_Scale = 128.0f / 2;

		const int texSize = m_Scale * 0.8;

		//UIの位置と大きさ設定
		auto&& scale = util::Vec2(m_Scale, m_Scale);

		//スコア
		auto counter = addNumber("Count1", util::Vec2(m_Trans.m_Position.x, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count2", util::Vec2(m_Trans.m_Position.x - texSize, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count3", util::Vec2(m_Trans.m_Position.x - texSize * 2, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count4", util::Vec2(m_Trans.m_Position.x - texSize * 3, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);
		counter = addNumber("Count5", util::Vec2(m_Trans.m_Position.x - texSize * 4, m_Trans.m_Position.y), scale, componentInitalizer);
		m_Renderers.emplace_back(counter);

	}

	void ResultTest::update()
	{
		if (m_pAnimator.lock()->getCurrentName() == (m_FirstMotionName + "1") && m_pAnimator.lock()->isEnd() && !m_isMotionChange) {
			//m_pAnimator.lock()->beginBlend(m_FirstMotionName + "2",MotionFrame::begin,0);
			m_pAnimator.lock()->changeAnime(m_FirstMotionName + "2");
			m_isMotionChange = true;
		}

		changeFontSize(0.7f);

		if (m_Fade.lock()->isEnd())
		{
			addScore();
			drawEvalue();
		}
	}

	void ResultTest::setParam(const std::vector<std::string>& param)
	{
		m_Trans.m_Position.x = std::atoi(param[0].c_str());
		m_Trans.m_Position.y = std::atoi(param[1].c_str());

		auto p = param;
		p.erase(p.begin(), std::next(p.begin(), 2));
		m_TextureNames.resize(10);
		for (short i = 0; i < p.size(); i++)
		{
			m_TextureNames[i] = p[i];
		}
	}

	void ResultTest::changeFontSize(float fontSize)
	{
		for (auto counters : m_Counters)
		{
			counters.lock()->getTransform()->scaling(fontSize);
		}
	}

	void ResultTest::changePos(const util::Vec2 & pos)
	{
		//UIの位置と大きさ設定
		const int texSize = 128 / 2;

		//位置
		m_Counters[0].lock()->getTransform()->m_Position = util::Vec3(pos.x, pos.y, 0);
		m_Counters[1].lock()->getTransform()->m_Position = util::Vec3(pos.x - texSize, pos.y, 0);
		m_Counters[2].lock()->getTransform()->m_Position = util::Vec3(pos.x - texSize * 2, pos.y, 0);
	}

	void ResultTest::HahabiEffect()
	{
		for (auto& timer : m_HanabiTimerContainer)
		{
			timer->update();
		}

		int flashCount = 0;

		auto deleteObj = std::remove_if(m_HanabiTimerContainer.begin(), m_HanabiTimerContainer.end(), [&](const std::shared_ptr<util::Timer>& tiemr) {
			if (tiemr->isEnd()) {
				flashCount++;
				return true;
			}

			return false;
		});


		//タイマーが終了してる数分エフェクト再生
		for (int i = 0; i != flashCount; i++)
		{
			auto centerPosX = m_Randam.next<float>(-100, 200);

			auto centerPosY = m_Randam.next<float>(50, 130);

			auto scale = m_Randam.next<float>(10, 15);

			auto effect = framework::EffectManager::getInstance()->generate(m_EffectName, util::Transform(util::Vec3(centerPosX, centerPosY, 280),
				util::Vec3(0, 0, 0),
				util::Vec3(scale, scale, scale)));
			effect.lock()->setSpeed(1.5);
		}


		//終了したタイマーを削除
		m_HanabiTimerContainer.erase(deleteObj, m_HanabiTimerContainer.end());

		//一度キューを詰めたらなくなるまで待つ
		if (!m_HanabiTimerContainer.empty())return;
		//花火の数ランダム
		auto hanabiCount = m_Randam.next<int>(0, 5);

		for (size_t i = 0; i < hanabiCount; i++)
		{
			//再生までのインターバル
			auto flashTime = m_Randam.next<int>(10, 50);
			m_HanabiTimerContainer.emplace_back(std::make_shared<util::Timer>(flashTime));
		}
	}

	void ResultTest::RainEffect()
	{
		//すでに雨エフェクトが再生中だったら処理しない
		if (!m_pResultEffect.expired())return;

		m_pResultEffect = framework::EffectManager::getInstance()->generate(m_EffectName, util::Transform(util::Vec3(40, 130, 200),
			util::Vec3(0, 0, 0),
			util::Vec3(15, 10, 10)));
	}

	std::weak_ptr<Counter> ResultTest::addNumber(const std::string & name, const util::Vec2 & pos, const util::Vec2 & scale, std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		auto entity = framework::Entity::createEntity(name, "UI", util::Transform(util::Vec3(pos.x, pos.y, 0), util::Vec3(), util::Vec3(scale.x, scale.y, 1)));
		auto counter = entity.lock()->addComponent<Counter>(componentInitalizer, m_TextureNames);
		counter.lock()->m_CallOrder = 0;

		(*componentInitalizer).emplace_back(counter);
		m_Counters.emplace_back(entity);
		return counter;
	}

	void ResultTest::selectUI() {
		auto leftStick = application::Input::leftVelocity();

		auto isLeftUp = application::Input::leftUpTrigger();
		auto isLeftDown = application::Input::leftDownTrigger();

		if (!m_IsRetry&& isLeftUp)
		{
			m_Entity.lock()->onEvent("Carsol");
			m_IsRetry = true;
		}
		else if (m_IsRetry && isLeftDown)
		{
			m_Entity.lock()->onEvent("Carsol");
			m_IsRetry = false;
		}

		//透明度の変更
		if (m_IsRetry)
		{
			m_Title.lock()->setAlpha(0.5f);
			m_Retry.lock()->setAlpha(1.0f);

			nextScene("StageBeta");
		}
		else
		{
			m_Title.lock()->setAlpha(1.0f);
			m_Retry.lock()->setAlpha(0.5f);

			nextScene("TitleScene");
		}
	}

	void ResultTest::nextScene(std::string sceneName) {
		if (application::Input::isJump()) {
			m_Entity.lock()->onEvent("Entry");
			m_Fade.lock()->onStart();
		}

		if (m_Fade.lock()->isEnd() && m_Fade.lock()->isOutFade()) {
			framework::EffectManager::getInstance()->create();
			framework::Scene::m_NextSceneName = sceneName;
			m_pSceneChange.lock()->changeSceneNotification();
		}
	}

	void ResultTest::addScore()
	{
		m_pTimer->update();
		m_ScoreNamber = util::lerp<float>(1.0f - m_pTimer->rate(), 0, ScoreComponent::m_Score);

		m_IsMaxScore = m_pTimer->isEnd();

		//スコア位の設定
		short digid = util::getDigid(m_ScoreNamber, 1);
		m_Renderers[0].lock()->changeNum(digid);
		digid = util::getDigid(m_ScoreNamber, 10);
		m_Renderers[1].lock()->changeNum(digid);
		digid = util::getDigid(m_ScoreNamber, 100);
		m_Renderers[2].lock()->changeNum(digid);
		digid = util::getDigid(m_ScoreNamber, 1000);
		m_Renderers[3].lock()->changeNum(digid);
		digid = util::getDigid(m_ScoreNamber, 10000);
		m_Renderers[4].lock()->changeNum(digid);

	}
	void ResultTest::drawEvalue()
	{
		//タイマーの更新
		if (m_IsMaxScore) {
			m_BornEvalueTimer.update();
		}

		if (m_BornEvalueTimer.isEnd()) {
			m_ScaleEvalueTimer.update();

			//ランクの透明度とスケール値の変更
			auto rate = m_ScaleEvalueTimer.rate();
			auto evalueScale = util::bezierCurve(rate, 1.0f, 0.0f, 0.5f, 3.5f);
			auto evaluealpha = util::bezierCurve(rate, 1.0f, 0.5f, 0.5f, 0.0f);
			m_pEvalueSprite.lock()->setAlpha(evaluealpha);
			m_pEvalueEntity.lock()->getTransform()->scaling(evalueScale);
		}

		if (m_ScaleEvalueTimer.isEnd()) {
			m_pAnimator.lock()->onStart();
			m_pCharaIconSprite.lock()->setAlpha(1.0f - m_pIconTimer->rate());
			m_pCharaIcon.lock()->getTransform()->m_Position.y = util::bezierCurve(m_pIconTimer->rate(), m_CharaIconY, m_CharaIconY - m_CharaIconFirstY * 0.3f, m_CharaIconY + m_CharaIconFirstY * 0.3f, m_CharaIconFirstY);
			m_pIconTimer->update();

			selectUI();

			if (m_EffectName == "Hanabi") {
				HahabiEffect();
				if (!m_isEvalueSound) {
					m_Entity.lock()->onEvent("CheerGroap");
					m_isEvalueSound = true;
				}
			}
			else {
				RainEffect();
				if (!m_isEvalueSound) {
					m_Entity.lock()->onEvent("Sympathy");
					m_isEvalueSound = true;
				}
			}
		}
	}
}
