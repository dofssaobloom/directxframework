#pragma once
#include<Framework.h>
#include<vector>
#include<Source\Util\Math\Randam.h>


namespace framework {
	class Effect;
}


namespace component {
	class SceneChangeComponent;
	class ScoreComponent;
	class GameTimerCompornent;
	class FadeComponent;
	class SceneChangeComponent;

	class ResultTest : public framework::UpdateComponent
	{
	public:
		ResultTest();
		~ResultTest();

		void init()override;

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

		virtual void setParam(const std::vector<std::string>& param);

		void changeFontSize(float fontSize);

		void changePos(const util::Vec2& pos);

		void HahabiEffect();

		void RainEffect();

	private:
		std::weak_ptr<Counter> addNumber(const std::string& name, const util::Vec2& pos, const util::Vec2& scale, std::vector<std::weak_ptr<Component>>* componentInitalizer);

		//どのUIを選択しているか
		void selectUI();

		//次のシーン
		void nextScene(std::string sceneName);

		//スコアの加算
		void addScore();

		//ランクの表示
		void drawEvalue();
	private:
		std::string m_EvalueSound;
		std::vector <std::weak_ptr<Counter>> m_Renderers;
		std::vector<framework::WeakEntity> m_Counters;
		util::Transform m_Trans;
		std::vector<std::string> m_TextureNames;
		std::weak_ptr<ScoreComponent> m_Score;
		std::weak_ptr<GameTimerCompornent> m_GameTimer;
		std::weak_ptr<SceneChangeComponent> m_pSceneChange;
		std::weak_ptr<SpriteRenderComponent> m_Title;
		std::weak_ptr<SpriteRenderComponent> m_Retry;
		std::list<std::weak_ptr<SpriteRenderComponent>> m_CleawrEvalue;
		std::weak_ptr<SpriteRenderComponent> m_pEvalueSprite;
		std::weak_ptr<AnimatorComponent> m_pAnimator;

		std::weak_ptr<framework::Effect> m_pResultEffect;
		std::string m_EffectName;
		util::Randam m_Randam;
		std::vector<std::shared_ptr< util::Timer>> m_HanabiTimerContainer;

		framework::WeakEntity m_pEvalueEntity;
		std::string m_Evalue;
		std::string m_FirstMotionName;
		std::unique_ptr<util::Timer> m_pTimer;
		std::unique_ptr<util::Timer> m_pIconTimer;

		util::Timer m_ScaleEvalueTimer;
		util::Timer m_BornEvalueTimer;
		
		std::weak_ptr<FadeComponent> m_Fade;
		framework::WeakEntity m_pCharaIcon;
		std::weak_ptr<SpriteRenderComponent> m_pCharaIconSprite;
		float m_CharaIconY;
		float m_CharaIconFirstY;
		bool m_isMotionChange;
		bool m_IsRetry;
		bool m_IsScale;
		bool m_IsMaxScore;
		bool m_isEvalueSound;
		float m_Scale;
		float m_EvalueScale;
		int m_ScoreNamber;
	};
}
