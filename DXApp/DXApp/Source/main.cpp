#define WIN32_LEAN_AND_MEAN

#include<Windows.h>
#include<D3DX11.h>
#include<Source\Application\TestApp.h>
#include<Source\Util\Math\Math.h>

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR, int) {
	
	//メモリリークデバッグ
#ifdef _MDEBUG	
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	TestApp app(hInst);

	auto msg = app.run();

	return msg;
}