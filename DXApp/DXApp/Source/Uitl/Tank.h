#pragma once
#include<minmax.h>

/// <summary>
/// 増減するオブジェクトを表すクラス
/// </summary>
template<typename T = short>
class Tank
{
public:
	Tank(const T maxTank)
		:m_MaxTank(maxTank),
		m_TankNum(m_MaxTank)
	{

	}

	Tank(const Tank & other)
		: m_MaxTank(other.m_MaxTank)
	{
		m_TankNum = other.m_TankNum;
	}

	~Tank() {

	}

	/// <summary>
	/// タンクの中身を増やす
	/// </summary>
	/// <param name="addNum"></param>
	void add(T addNum) {
		m_TankNum += addNum;
		m_TankNum = min(m_TankNum, m_MaxTank);
	}


	/// <summary>
	/// タンクの中身をへらす
	/// </summary>
	/// <param name="cutNum"></param>
	void cutBack(T cutNum) {
		m_TankNum -= cutNum;
		m_TankNum = max(m_TankNum, 0);
	}

	/// <summary>
	/// 残量を0~1で表す
	/// </summary>
	/// <returns></returns>
	float rate()const {
		return  m_TankNum / (float)m_MaxTank;
	}

	/// <summary>
	/// 残量から減らそうとしてる値が減らしたら０を下回るかどうか
	/// </summary>
	/// <returns></returns>
	bool isLow(T num) {
		return (m_TankNum - num) <= 0;
	}

	bool isEmpty() {
		return m_TankNum == 0;
	}

	/// <summary>
	/// 代入オペレーター
	/// </summary>
	/// <param name="other"></param>
	/// <returns></returns>
	Tank operator=(const Tank& other) {
		*this = other;

		return *this;
	}

private:
	//最大蓄積値
	const T m_MaxTank;
	//!タンクの残量
	T m_TankNum;

};