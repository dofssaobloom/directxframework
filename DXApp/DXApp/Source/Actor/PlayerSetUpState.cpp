#include "PlayerSetUpState.h"
#include "PlayerIdleState.h"
#include<Source\Device\Input\Input.h>
#include<Source\Entity\Entity.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Component\Player\PlayerComponent.h>
#include"PlayerIdleState.h"
#include<assert.h>
#include<Source\Component\Animation\AnimatorComponent.h>
#include<Source\Actor\PlayerAttackState.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Component\Physics\RigidBody.h>
#include<Source\Actor\PlayerSliderState.h>
#include<Source\Util\Win\WinFunc.h>
#include<Framework.h>
#include<Source\Application\Device\Input.h>
#include<Source\Component\Device\CameraController.h>
#include<Source\Component\Bullet\LaunchingDevice.h>

UsingNamespace;

PlayerSetUpState::PlayerSetUpState()
{
}

PlayerSetUpState::~PlayerSetUpState()
{
}

void PlayerSetUpState::entryAction(component::PlayerComponent * outerValue)
{
	//移動ステータスに変更
	auto&& currentState = outerValue->getPlayerData().lock()->currentState = PlayerState::setup;
	m_pAnimator = outerValue->getGameObj().lock()->getComponent<component::AnimatorComponent>();

	//m_pAnimator.lock()->changeAnime(outerValue->getPlayerData().lock()->motionNames[currentState], m_pAnimator.lock()->getBlendFrame());
	//m_pAnimator.lock()->beginBlend("Player_ShootingIdle", MotionFrame::begin, 10);
	//m_pAnimator.lock()->changeAnime("Player_ShootingIdle", m_pAnimator.lock()->getBlendFrame());
	//m_pAnimator.lock()->endBlend();

	m_pLauncher = outerValue->getPlayerData().lock()->pLauncher;
	m_pLauncher.lock()->activeLine();

	m_pCameraController = outerValue->getPlayerData().lock()->pCameraController;
	m_pCameraController.lock()->changeView();
	//m_pCameraController.lock()->deActive();
	m_IsChargeMax = m_IsBulletCharge = false;
}

Trans PlayerSetUpState::inputAction(component::PlayerComponent * outerValue, std::shared_ptr<State<component::PlayerComponent>>* nextState)
{

	auto vel = application::Input::leftVelocity();


	if (application::Input::isShot()) {
		shoopting();
	}
	else if (vel.isZero()) {
		neutral();
	}
	else if (abs(vel.y) == max(abs(vel.x), abs(vel.y))) {
		verticalMove(vel);
	}
	else {
		bool isRight = util::sign(vel.x);

		if (isRight) {
			rightMove(vel);
		}
		else {
			leftMove(vel);
		}

	}

	if (m_pAnimator.lock()->isBlend()) {
		if (m_pAnimator.lock()->getBlendMotionName() == "Player_Exchanging") {
			if (m_pAnimator.lock()->isBlendAnimeEnd()) {
				m_pAnimator.lock()->changeAnime(m_pAnimator.lock()->getBlendMotionName(), m_pAnimator.lock()->getBlendFrame());
				m_pAnimator.lock()->endBlend();
			}
		}
		else if (m_pAnimator.lock()->isBlendTimeEnd()) {
			m_pAnimator.lock()->changeAnime(m_pAnimator.lock()->getBlendMotionName(), m_pAnimator.lock()->getBlendFrame());
			m_pAnimator.lock()->endBlend();
		}
	}


	//if (!vel.isZero()) {
	//	m_pAnimator.lock()->changeAnime("Player_ShootingRun");
	//}
	//else {
	//	m_pAnimator.lock()->changeAnime("Player_ShootingIdle");
	//}

	//if (m_pAnimator.lock()->getBlendMotionName() != "Player_Exchanging" && m_pAnimator.lock()->isBendAnimeEnd()) {
	//	//移動モーションに切り替える
	//	//アニメーションだけアイドル
	//	m_pAnimator.lock()->changeAnime("Player_ShootingIdle", m_pAnimator.lock()->getBlendFrame());
	//	m_pAnimator.lock()->endBlend();
	//}

	//SEの更新
	seUpdate();

	myUpdate(outerValue);

	angleUpdate(outerValue);

	spawnBullet(outerValue);

	//セットアップを押していないとき
	if (!application::Input::isSetUp() && !application::Input::isCharge())
	{
		*nextState = std::make_shared<PlayerIdleState>();
		return Trans::Trans_Occured;
	}

	*nextState = std::make_shared<PlayerSetUpState>(*this);
	return Trans::Trans_Again;
}

void PlayerSetUpState::exitAction(component::PlayerComponent * outerValue)
{
	deleteEffect();

	m_pCameraController.lock()->changeView();
	m_pLauncher.lock()->deActiveLine();
	//m_pCameraController.lock()->active();
	//.lock()->onEvent("OnBlur");
}

void PlayerSetUpState::deleteEffect()
{
	if (!m_pChargeEffect.expired()) {
		auto handle = m_pChargeEffect.lock()->getHandle();
		EffectManager::getInstance()->effectStop(handle);
	}

	if (!m_pMaxEffect.expired()) {
		auto handle = m_pMaxEffect.lock()->getHandle();
		EffectManager::getInstance()->effectStop(handle);
	}
}

void PlayerSetUpState::seUpdate()
{
	//SEの再生
	//if (framework::ResourceManager::getInstance()->isPlaying("Footsteps"))return;
	//framework::ResourceManager::getInstance()->playSound("Footsteps");
	//framework::ResourceManager::getInstance()->setVolume("Footsteps", 0);
	//framework::ResourceManager::getInstance()->setDoppler("Footsteps", 0);
}

void PlayerSetUpState::myUpdate(component::PlayerComponent * outerValue)
{
	auto trans = outerValue->getGameObj().lock()->getTransform();
	auto vec = velocity(trans, outerValue->getPlayerData().lock()->controlID);

	//回転
	trans->m_Rotation += util::Vec3(0, m_right.x * outerValue->getPlayerData().lock()->setupRotationSpeed, 0);


	//カメラの方向に合わせて移動
	trans->m_Position.x += vec.x*outerValue->getPlayerData().lock()->setupMoveSpeed;
	trans->m_Position.z += vec.z*outerValue->getPlayerData().lock()->setupMoveSpeed;
}

util::Vec3 PlayerSetUpState::velocity(util::Transform* trans, int controlID)
{
	using namespace framework;

	//スティックの入力
	m_left = application::Input::leftVelocity();
	m_right = application::Input::rightVelocity();

	//カメラの追従
	//auto vec = component::CameraComponent::getMainCamera().lock()->
	//	convertViewDir(util::Vec3(-m_left.x, 0, -m_swswleft.y));

	auto playerTransCopy(*trans);

	auto front = playerTransCopy.front();
	front.y = 0;

	util::Mat4 rotete;
	util::createMatrixFromFront(front.normalize(), &rotete);

	util::Vec3 vec(m_left.x, 0, m_left.y);
	vec = XMVector3Transform(vec.toXMVector(), rotete.toXMMatrix());

	vec.y = 0;

	//左スティックが0かどうかのフラグ
	m_IsChange = m_left.isZero();

	return vec.normalize();
}

void PlayerSetUpState::angleUpdate(component::PlayerComponent * outerValue)
{
	auto rate = m_pCameraController.lock()->angleRate();
	outerValue->getPlayerData().lock()->pLauncher.lock()->setAngle(util::lerp<float>(rate, 0, 50));
	outerValue->getPlayerData().lock()->pLauncher.lock()->setVec(util::lerp<float>(rate, 50, 180));
}

void PlayerSetUpState::verticalMove(const util::Vec2& input)
{
	if (m_pAnimator.lock()->getCurrentName() == "Player_ShootingRun")return;
	if (m_pAnimator.lock()->isBlend()) {
		if (m_pAnimator.lock()->getBlendMotionName() == "Player_ShootingRun")return;

		m_pAnimator.lock()->changeAnime(m_pAnimator.lock()->getBlendMotionName(), m_pAnimator.lock()->getBlendFrame());
		m_pAnimator.lock()->endBlend();
		m_pAnimator.lock()->beginBlend("Player_ShootingRun", component::MotionFrame::begin, 5);

	}
	else {
		m_pAnimator.lock()->beginBlend("Player_ShootingRun", component::MotionFrame::begin, 5);
	}


}

void PlayerSetUpState::rightMove(const util::Vec2& input)
{
	//TODO あとで横歩きモーションに差し替える

	if (m_pAnimator.lock()->getCurrentName() == "Player_ShootingRightRun")return;
	if (m_pAnimator.lock()->isBlend()) {
		if (m_pAnimator.lock()->getBlendMotionName() == "Player_ShootingRightRun")return;

		m_pAnimator.lock()->changeAnime(m_pAnimator.lock()->getBlendMotionName(), m_pAnimator.lock()->getBlendFrame());
		m_pAnimator.lock()->endBlend();
		m_pAnimator.lock()->beginBlend("Player_ShootingRightRun", component::MotionFrame::begin, 5);
	}
	else {
		m_pAnimator.lock()->beginBlend("Player_ShootingRightRun", component::MotionFrame::begin, 5);
	}

}

void PlayerSetUpState::leftMove(const util::Vec2 & input)
{
	if (m_pAnimator.lock()->getCurrentName() == "Player_ShootingLeftRun")return;
	if (m_pAnimator.lock()->isBlend()) {
		if (m_pAnimator.lock()->getBlendMotionName() == "Player_ShootingLeftRun")return;

		m_pAnimator.lock()->changeAnime(m_pAnimator.lock()->getBlendMotionName(), m_pAnimator.lock()->getBlendFrame());
		m_pAnimator.lock()->endBlend();
		m_pAnimator.lock()->beginBlend("Player_ShootingLeftRun", component::MotionFrame::begin, 5);
	}
	else {
		m_pAnimator.lock()->beginBlend("Player_ShootingLeftRun", component::MotionFrame::begin, 5);
	}
}

void PlayerSetUpState::neutral()
{
	if (m_pAnimator.lock()->getCurrentName() == "Player_ShootingIdle")return;
	//m_pAnimator.lock()->changeAnime("Player_ShootingIdle");

	if (m_pAnimator.lock()->isBlend()) {
		if (m_pAnimator.lock()->getBlendMotionName() == "Player_ShootingIdle")return;

		m_pAnimator.lock()->changeAnime(m_pAnimator.lock()->getBlendMotionName(), m_pAnimator.lock()->getBlendFrame());
		m_pAnimator.lock()->endBlend();
		m_pAnimator.lock()->beginBlend("Player_ShootingIdle", component::MotionFrame::begin, 15);

	}
	else {
		m_pAnimator.lock()->beginBlend("Player_ShootingIdle", component::MotionFrame::begin, 5);
	}

}

void PlayerSetUpState::shoopting()
{
	if (m_pAnimator.lock()->getCurrentName() == "Player_Shooting")return;

	if (m_pAnimator.lock()->isBlend()) {
		if (m_pAnimator.lock()->getBlendMotionName() == "Player_Shooting")return;

		m_pAnimator.lock()->changeAnime(m_pAnimator.lock()->getBlendMotionName(), m_pAnimator.lock()->getBlendFrame());
		m_pAnimator.lock()->endBlend();
		m_pAnimator.lock()->beginBlend("Player_Shooting", component::MotionFrame::begin, 15);
	}
	else {
		m_pAnimator.lock()->beginBlend("Player_Shooting", component::MotionFrame::begin, 5);
	}
}

void PlayerSetUpState::spawnBullet(component::PlayerComponent * outerValue)
{
	auto trans = outerValue->getGameObj().lock()->getTransform();
	if (application::Input::isCharge() && !m_IsBulletCharge) {
		auto copy = *trans;
		copy.scaling(30);
		m_pChargeEffect = EffectManager::getInstance()->generate("ChargeEffect", copy);
		m_pChargeEffect.lock()->setSpeed(1.5);
		m_pLauncher.lock()->initTimer();
		m_IsBulletCharge = true;
	}

	if (m_IsBulletCharge) {
		m_pLauncher.lock()->updateTimer();
		//エフェクトの座標を更新
		if (!m_pChargeEffect.expired()) {
			m_pChargeEffect.lock()->m_Trans.m_Position = trans->m_Position;
			m_pChargeEffect.lock()->m_Trans.m_Position.y += 45;
			m_pChargeEffect.lock()->m_Trans.m_Position += trans->front() * 45;
			m_pChargeEffect.lock()->m_Trans.m_Position += trans->left() * 15;

		}

		if (m_pLauncher.lock()->isEndTimer() && !m_IsChargeMax) {
			auto copy = *trans;
			copy.scaling(30);
			m_pMaxEffect = EffectManager::getInstance()->generate("ChargeMax", copy);
			m_pMaxEffect.lock()->setSpeed(3.5);
			m_IsChargeMax = true;
		}

		if (m_IsChargeMax) {
			if (!m_pMaxEffect.expired()) {
				m_pMaxEffect.lock()->m_Trans.m_Position = trans->m_Position;
				m_pMaxEffect.lock()->m_Trans.m_Position.y += 45;
				m_pMaxEffect.lock()->m_Trans.m_Position += trans->front() * 45;
				m_pMaxEffect.lock()->m_Trans.m_Position += trans->left() * 15;
			}
		}
	}


	if (application::Input::isShot()) {
		m_pLauncher.lock()->spown();
		m_IsChargeMax = m_IsBulletCharge = false;

		deleteEffect();
	}


}
