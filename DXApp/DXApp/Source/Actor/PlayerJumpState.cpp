#include "PlayerJumpState.h"
#include<Source\Device\Input\Input.h>
#include<Source\Entity\Entity.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Component\Player\PlayerComponent.h>
#include"PlayerIdleState.h"
#include "PlayerMoveState.h"
#include<assert.h>
#include<Source\Component\Animation\AnimatorComponent.h>
#include<Source\Actor\PlayerAttackState.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Actor\PlayerSliderState.h>
#include<Source\Util\Win\WinFunc.h>
#include<Framework.h>
#include<Source\Application\Device\Input.h>
#include<Source\Component\Device\CameraController.h>

UsingNamespace;


PlayerJumpState::PlayerJumpState(float jumpRate)
	:m_JumpRate(1.0f - jumpRate)
{
}

PlayerJumpState::~PlayerJumpState()
{
}

void PlayerJumpState::entryAction(component::PlayerComponent * outerValue)
{
	//移動ステータスに変更
	auto&& currentState = outerValue->getPlayerData().lock()->currentState = PlayerState::jump;
	m_pAnimator = outerValue->getGameObj().lock()->getComponent<component::AnimatorComponent>();
	assert(!m_pAnimator.expired() && "プレイヤーにアニメーターがついていません");

	auto motionName = outerValue->getPlayerData().lock()->motionNames[currentState];

	//移動モーションに切り替える
	m_pAnimator.lock()->changeAnime(motionName, m_pAnimator.lock()->getBlendFrame());
	m_pAnimator.lock()->endBlend();

	m_pRigidBody = outerValue->getGameObj().lock()->getComponent<component::BulletRigidBody>();
	m_FreezeFlag = m_pRigidBody.lock()->getPosFreezeFlag();

	//util::Vec3 vel;
	//m_pRigidBody.lock()->getLinearVelocity(vel);

	auto vel = application::Input::leftVelocity();

	m_pRigidBody.lock()->setPositionFreezeFlag({ false,false,false });

	auto&& front = outerValue->getGameObj().lock()->getTransform()->front()  * outerValue->getPlayerData().lock()->jumpFrontForce * util::Vec3(vel.x, 0, vel.y).length();
	auto upVec = util::Vec3(0, 1, 0) * outerValue->getPlayerData().lock()->jumpUpForce;
	auto jumpVec = upVec + front;

	jumpVec = jumpVec * max(m_JumpRate, 0.3);

	m_pRigidBody.lock()->addForce(jumpVec);

	m_IsEnd = false;

	m_pLaidingTimer = std::make_shared<util::Timer>(2);
	m_IsPreJump = m_IsJamp = false;
	m_IsJumpTrriger = false;

	//無敵時間発生
	outerValue->hit();
}

Trans PlayerJumpState::inputAction(component::PlayerComponent * outerValue, std::shared_ptr<State<component::PlayerComponent>>* nextState)
{
	m_IsPreJump = m_IsJamp;
	m_IsJamp = !outerValue->getPlayerData().lock()->isFloorContact;

	if (m_IsJamp && !m_IsPreJump) {
		m_IsJumpTrriger = true;
	}

	//ジャンプステートになって2フレたっても地面から離れていなかったらジャンプしたとみなす
	if (m_pLaidingTimer->isEnd()) {
		m_IsJumpTrriger = true;
	}

	m_pLaidingTimer->update();

	//auto trans = outerValue->getGameObj().lock()->getTransform();
	if (m_IsEnd) {
		if (m_pAnimator.lock()->isBlendTimeEnd()) {
			*nextState = m_NextState;
			return Trans::Trans_Occured;
		}
	}




	//地面に着地または、地面から離れなかった場合はステートを抜ける
	if (m_IsJumpTrriger && outerValue->getPlayerData().lock()->isFloorContact)
	{
		if (!m_IsEnd) {
			auto vec = application::Input::leftVelocity();
			if (vec.isZero()) {
				//次のアニメーションとブレンド
				m_pAnimator.lock()->beginBlend(outerValue->getPlayerData().lock()->motionNames[PlayerState::idle], component::MotionFrame::begin, 3);
				m_NextState = std::make_shared<PlayerIdleState>();
			}
			else {
				//次のアニメーションとブレンド
				m_pAnimator.lock()->beginBlend(outerValue->getPlayerData().lock()->motionNames[PlayerState::move], component::MotionFrame::begin, 3);
				m_NextState = std::make_shared<PlayerMoveState>();
			}
			m_IsEnd = true;
			m_pAnimator.lock()->onStart();
		}
	}
	else {
		if (!m_IsEnd) {
			if (m_pAnimator.lock()->getFrame() >= 34 && m_pAnimator.lock()->getFrame() < 35) {
				m_pAnimator.lock()->onStop();
			}
		}
	}

	if (!outerValue->getPlayerData().lock()->isFloorContact) {
		myUpdate(outerValue);
	}

	//SEの更新
	//seUpdate();

	/*if (outerValue->isJumpTrriger()) {
		m_IsJamp = true;
	}
	m_LaidingTimer.update();
	*/

	*nextState = std::make_shared<PlayerJumpState>(*this);
	return Trans::Trans_Again;
}

void PlayerJumpState::exitAction(component::PlayerComponent * outerValue)
{
	m_pRigidBody.lock()->setPositionFreezeFlag({ true,false,true});
}

void PlayerJumpState::seUpdate()
{
	//SEの再生
	if (framework::ResourceManager::getInstance()->isPlaying("Footsteps"))return;
	framework::ResourceManager::getInstance()->playSound("Footsteps");
	framework::ResourceManager::getInstance()->setVolume("Footsteps", 0);
	framework::ResourceManager::getInstance()->setDoppler("Footsteps", 0);
}

void PlayerJumpState::myUpdate(component::PlayerComponent * outerValue)
{
	using namespace framework;
	auto trans = outerValue->getGameObj().lock()->getTransform();
	auto vel = velocity(trans, outerValue->getPlayerData().lock()->controlID);
	float length = vel.length();//どれくらい入力されているか

	//正面方向
	auto dir = trans->front();

	if (dir.dot(vel) <= -0.9 && !vel.isZero()) {
		vel = util::Vec3(0, 1, 0).cross(dir);
	}
	//回転に摩擦係数をかける
	vel = vel * outerValue->getPlayerData().lock()->bendFriction;

	dir += vel;
	dir = dir.normalize();
	trans->lookAt(dir);

	dir = dir * length * 5;

	//移動
	trans->m_Position += dir;
}

util::Vec3 PlayerJumpState::velocity(util::Transform* trans, int controlID)
{
	using namespace framework;

	//左スティックの入力
	auto leftStick = application::Input::leftVelocity();//.normalize();

														//カメラの追従
	auto vec = component::CameraComponent::getMainCamera().lock()->
		convertViewDir(util::Vec3(-leftStick.x, 0, -leftStick.y));

	vec = vec.normalize();
	//yへの移動なし
	vec.y = 0;

	return vec;
}
