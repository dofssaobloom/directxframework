#pragma once
#include<Source\State\State.h>
#include"PlayerStates.h"
#include<Source\Util\Type.h>
#include<Source\Component\Player\PlayerComponent.h>
#include<Source\Component\Physics\BulletRigidBody.h>

namespace util {
	class Transform;
}

namespace component {
	class AnimatorComponent;
	class BulletRigidBody;
	class CameraController;
}


class PlayerJumpState : public State<component::PlayerComponent>
{
public:
	PlayerJumpState(float jumpRate);
	~PlayerJumpState();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::PlayerComponent* outerValue);

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::PlayerComponent* outerValue, std::shared_ptr<State<component::PlayerComponent>>* nextState);
	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::PlayerComponent* outerValue);
private:
	/*
	SEの再生
	*/
	void seUpdate();

	/*
	回転速度
	*/
	void myUpdate(component::PlayerComponent * outerValue);

	/*
	Playerの回転とカメラの追従
	*/
	util::Vec3 velocity(util::Transform* trans, int controlID);

private:
	std::weak_ptr<component::AnimatorComponent> m_pAnimator;
	std::shared_ptr<State<component::PlayerComponent>> m_NextState;
	std::weak_ptr<component::BulletRigidBody> m_pRigidBody;
	component::BulletRigidBody::FreezeElement m_FreezeFlag;


	//切り替え条件かどうか
	bool m_IsEnd;

	float m_JumpRate;

	//!一度でも地面から離れたかどうか
	bool m_IsJumpTrriger;

	//!ジャンプステートに履いて一度も空中に行かない場合ステートを抜けるための大麻
	std::shared_ptr<util::Timer> m_pLaidingTimer;
	bool m_IsJamp;
	bool m_IsPreJump;

	util::Vec2 m_right;
	util::Vec2 m_left;
};