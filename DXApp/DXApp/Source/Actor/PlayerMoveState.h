#pragma once
#include<Source\State\State.h>
#include"PlayerStates.h"
#include<Source\Util\Type.h>
#include<Source\Component\Player\PlayerComponent.h>
#include<Source\Util\Timer\Timer.h>


namespace util {
	class Transform;
}

namespace component {
	class AnimatorComponent;
	class BulletRigidBody;
}


class PlayerMoveState : public State<component::PlayerComponent>
{
public:
	PlayerMoveState();
	~PlayerMoveState();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction( component::PlayerComponent* outerValue);

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction( component::PlayerComponent* outerValue, std::shared_ptr<State<component::PlayerComponent>>* nextState);
	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction( component::PlayerComponent* outerValue);
private:
	/*
	SEの再生
	*/
	void seUpdate();

	/*
	回転速度
	*/
	void myUpdate(component::PlayerComponent * outerValue);

	/*
	エフェクト
	*/
	void effectUpdate(component::PlayerComponent *);

	/*
	Playerの回転とカメラの追従
	*/
	util::Vec3 velocity(util::Transform* trans,int controlID);

	/// <summary>
	/// スモークエフェクト生成
	/// </summary>
	/// <returns></returns>
	std::weak_ptr<framework::Effect> generateSmoke(component::PlayerComponent * outerValue);


	//void tornadoUpdate(component::PlayerComponent *);

private:
	//ステート変更用フラグ
	bool m_IsChange;

	util::Vec3 m_EffectOffset;
	std::weak_ptr<component::AnimatorComponent> m_pAnimator;
	std::weak_ptr<framework::Effect> m_ptTornado;

	//切り替え条件かどうか
	bool m_IsEnd;
	std::weak_ptr<component::BulletRigidBody> m_pRigidBody;
	std::shared_ptr<State<component::PlayerComponent>> m_NextState;

	//!止まるときの速度摩擦係数
	float m_SpeedFriction;

	//!走りよりもセットアップ優先にするためのフラグ
	bool m_IsSetUP;

	util::Timer m_JumpTimer;
	bool m_IsJump;
	bool m_IsAttack;
};
