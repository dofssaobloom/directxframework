#include "PlayerMoveState.h"
#include<Source\Device\Input\Input.h>
#include<Source\Entity\Entity.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Component\Player\PlayerComponent.h>
#include"PlayerIdleState.h"
#include"PlayerSetUpState.h"
#include"PlayerJumpState.h"
#include<assert.h>
#include<Source\Component\Animation\AnimatorComponent.h>
#include<Source\Actor\PlayerAttackState.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Component\Physics\RigidBody.h>
#include<Source\Actor\PlayerSliderState.h>
#include<Source\Util\Win\WinFunc.h>
#include<Framework.h>
#include<Source\Application\Device\Input.h>

UsingNamespace;


PlayerMoveState::PlayerMoveState()
	:m_EffectOffset(0, -200, 0),
	m_JumpTimer(3)
{
}

PlayerMoveState::~PlayerMoveState()
{
}

void PlayerMoveState::entryAction(component::PlayerComponent * outerValue)
{
	//移動ステータスに変更
	auto&& currentState = outerValue->getPlayerData().lock()->currentState = PlayerState::move;
	m_pAnimator = outerValue->getGameObj().lock()->getComponent<component::AnimatorComponent>();
	assert(!m_pAnimator.expired() && "プレイヤーにアニメーターがついていません");

	//前のアニメーションとブレンド
	//m_pAnimator.lock()->beginBlend(outerValue->getPlayerData().lock()->motionNames[preState], m_pAnimator.lock()->getFrame(),20);

	//移動モーションに切り替える
	m_pAnimator.lock()->changeAnime(outerValue->getPlayerData().lock()->motionNames[currentState], m_pAnimator.lock()->getBlendFrame());
	m_pAnimator.lock()->endBlend();//一度ブレンドを切る
	m_IsEnd = false;
	m_IsSetUP = false;

	m_pRigidBody = outerValue->getGameObj().lock()->getComponent<component::BulletRigidBody>();
	m_pRigidBody.lock()->setPositionFreezeFlag({ true,false,true });
	m_pRigidBody.lock()->setRotateFreezeFlag({ true,true,true });

	m_SpeedFriction = 1.0f;

	//走っている際の煙
	//generateSmoke(outerValue);

	//後ろから出る亜音速のエフェクト
	//EffectManager::getInstance()->generate("Tornado", util::Transform(outerValue->getGameObj().lock()->getTransform()->m_Position,
	//	util::Vec3(0, outerValue->getGameObj().lock()->getTransform()->m_Rotation.y + 180, 0),
	//	util::Vec3(1, 1, 1)));

	m_IsJump = false;
	m_JumpTimer.init();
	m_IsAttack = false;
}

Trans PlayerMoveState::inputAction(component::PlayerComponent * outerValue, std::shared_ptr<State<component::PlayerComponent>>* nextState)
{
	//アタックの入力、アタックアニメーション再生

	if (application::Input::isAttack()) {
		if (!m_IsEnd) {
			//次のアニメーションとブレンド
			m_pAnimator.lock()->beginBlend(outerValue->getPlayerData().lock()->motionNames[PlayerState::attack], component::MotionFrame::begin, 6);
			m_NextState = std::make_shared<PlayerAttackState>();
			m_IsEnd = true;
			m_IsAttack = true;
		}
	}


	//SEの更新
	seUpdate();

	//更新
	myUpdate(outerValue);

	if (m_IsEnd) {
		m_SpeedFriction -= 0.15;
		m_SpeedFriction = max(m_SpeedFriction, 0);

		if (!m_IsSetUP && !m_IsJump && !m_IsAttack) {
			if (!m_IsChange) {
				entryAction(outerValue);
			}
		}

		if (m_pAnimator.lock()->isBlendTimeEnd()) {
			*nextState = m_NextState;
			return Trans::Trans_Occured;
		}
	}

	//エフェクトの更新
	//effectUpdate(outerValue);
	//tornadoUpdate(outerValue);

	//左スティックの入力が無ければIdleStateへ遷移する
	if (m_IsChange) {
		if (!m_IsEnd) {
			//次のアニメーションとブレンド
			m_pAnimator.lock()->beginBlend(outerValue->getPlayerData().lock()->motionNames[PlayerState::idle], MotionFrame::begin, 10);
			m_NextState = std::make_shared<PlayerIdleState>();
		}
		m_IsEnd = true;

		//TODO 多分ココで次の遷移先のアニメーションにブレンド設定し直す必要がある
	}
	if (application::Input::isSetUp()) {
		if (!m_IsEnd) {
			//次のアニメーションとブレンド
			m_pAnimator.lock()->beginBlend(outerValue->getPlayerData().lock()->motionNames[PlayerState::setup], component::MotionFrame::begin, 6);
			m_NextState = std::make_shared<PlayerSetUpState>();
			m_IsEnd = true;
			m_IsSetUP = true;
		}
		m_IsEnd = true;
	}

	auto trans = outerValue->getGameObj().lock()->getTransform();

	//if (application::Input::isJump()) {
	//	m_JumpTimer.update();
	//	m_IsJump = true;
	//}

	if (outerValue->getPlayerData().lock()->isFloorContact && application::Input::isJump()) {
		if (!m_IsEnd) {
			//次のアニメーションとブレンド
			m_pAnimator.lock()->beginBlend(outerValue->getPlayerData().lock()->motionNames[PlayerState::jump], component::MotionFrame::begin, 3);
			m_NextState = std::make_shared<PlayerJumpState>(0);
			m_IsJump = m_IsEnd = true;
		}
	}


	*nextState = std::make_shared<PlayerMoveState>(*this);
	return Trans::Trans_Again;
}

void PlayerMoveState::exitAction(component::PlayerComponent * outerValue)
{
}

void PlayerMoveState::seUpdate()
{
	//SEの再生
	//if (framework::ResourceManager::getInstance()->isPlaying("Footsteps"))return;
	//framework::ResourceManager::getInstance()->playSound("Footsteps");
	//framework::ResourceManager::getInstance()->setVolume("Footsteps", 0);
}

void PlayerMoveState::myUpdate(component::PlayerComponent * outerValue)
{
	using namespace framework;
	auto trans = outerValue->getGameObj().lock()->getTransform();
	auto vel = velocity(trans, outerValue->getPlayerData().lock()->controlID);

	//正面方向
	auto dir = trans->front();

	if (dir.dot(vel) <= -0.9 && !vel.isZero()) {
		vel = util::Vec3(0,1,0).cross(dir);
	}

	//回転に摩擦係数をかける
	vel = vel * outerValue->getPlayerData().lock()->bendFriction;
	dir += vel;
	dir = dir.normalize();
	trans->lookAt(dir);

	dir = dir * outerValue->getPlayerData().lock()->speed;
	dir = dir * m_SpeedFriction;

	//移動
	trans->m_Position += dir;
}

void PlayerMoveState::effectUpdate(component::PlayerComponent * outerValue)
{
	//エフェクトが再生されていなければ再生する
	if (EffectManager::getInstance()->isEffectExits(outerValue->getPlayerData().lock()->smokeEffect)) {
		generateSmoke(outerValue);
	}

	outerValue->getPlayerData().lock()->smokeEffect.lock()->m_Trans.m_Position = outerValue->getGameObj().lock()->getTransform()->m_Position;
}

//回転
util::Vec3 PlayerMoveState::velocity(util::Transform* trans, int controlID)
{
	using namespace framework;

	//左スティックの入力
	auto leftStick = application::Input::leftVelocity();//.normalize();

	//カメラの追従
	auto vec = component::CameraComponent::getMainCamera().lock()->
		convertViewDir(util::Vec3(-leftStick.x, 0, -leftStick.y));

	vec = vec.normalize();
	//yへの移動なし
	vec.y = 0;

	//左スティック
	m_IsChange = leftStick.isZero();

	return vec;
}

std::weak_ptr<framework::Effect> PlayerMoveState::generateSmoke(component::PlayerComponent * outerValue)
{
	return outerValue->getPlayerData().lock()->smokeEffect =
		EffectManager::getInstance()->generate("Smoke", util::Transform(outerValue->getGameObj().lock()->getTransform()->m_Position + m_EffectOffset,
			util::Vec3(0, outerValue->getGameObj().lock()->getTransform()->m_Rotation.y + 180, 0),
			util::Vec3(0.3f, 0.3f, 0.3f)));

	//EffectManager::getInstance()->generate("Smoke", util::Transform(outerValue->getGameObj().lock()->getTransform()->m_Position + m_EffectOffset,
	//	outerValue->getGameObj().lock()->getTransform()->m_Rotation,
	//	util::Vec3(0.3f, 0.3f, 0.3f)));
}

//void PlayerMoveState::tornadoUpdate(component::PlayerComponent *outerValue)
//{
//	if (!outerValue->isMaxSpeed())return;
//	//すでにエフェクトが生成されていれば生成しない
//	if (!m_ptTornado.expired())return;
//
//	m_ptTornado = EffectManager::getInstance()->generate("Tornado", util::Transform(outerValue->getGameObj().lock()->getTransform()->m_Position,
//		util::Vec3(0, outerValue->getGameObj().lock()->getTransform()->m_Rotation.y + 180, 0),
//		util::Vec3(1, 1, 1)));
//
//}
