#pragma once
#include<Source\State\State.h>
#include"PlayerStates.h"
#include<Source\Util\Type.h>
#include<Source\Component\Player\PlayerComponent.h>

namespace util {
	class Transform;
}

namespace component {
	class AnimatorComponent;
	class CameraController;
	class LaunchingDevice;
}


namespace framework {
	class Effect;
}


class PlayerSetUpState : public State<component::PlayerComponent>
{
public:
	PlayerSetUpState();
	~PlayerSetUpState();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::PlayerComponent* outerValue);

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::PlayerComponent* outerValue, std::shared_ptr<State<component::PlayerComponent>>* nextState);
	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::PlayerComponent* outerValue);

	/// <summary>
	/// エフェクト削除
	/// </summary>
	void deleteEffect();

private:
	/*
	SEの再生
	*/
	void seUpdate();

	/*
	回転速度
	*/
	void myUpdate(component::PlayerComponent * outerValue);

	/*
	Playerの回転とカメラの追従
	*/
	util::Vec3 velocity(util::Transform* trans, int controlID);

	/// <summary>
	/// ランチャーの投射角度更新
	/// </summary>
	void angleUpdate(component::PlayerComponent * outerValue);

	/// <summary>
	/// 縦移動中
	/// </summary>
	void verticalMove(const util::Vec2& input);

	/// <summary>
	/// 右移動中
	/// </summary>
	void rightMove(const util::Vec2& input);


	/// <summary>
	/// 左移動
	/// </summary>
	/// <param name="input"></param>
	void leftMove(const util::Vec2& input);

	/// <summary>
	/// 入力なし
	/// </summary>
	void neutral();

	/// <summary>
	/// 射撃中
	/// </summary>
	void shoopting();

	/// <summary>
	/// 弾の射出
	/// </summary>
	void spawnBullet(component::PlayerComponent * outerValue);

private:
	//ステート変更用フラグ
	bool m_IsChange;

	bool m_IsBulletCharge;
	bool m_IsChargeMax;
	std::weak_ptr<framework::Effect> m_pChargeEffect;
	std::weak_ptr<framework::Effect> m_pMaxEffect;

	util::Vec2 m_right;
	util::Vec2 m_left;
	std::weak_ptr<component::AnimatorComponent> m_pAnimator;
	std::weak_ptr<component::CameraController> m_pCameraController;
	std::weak_ptr<component::LaunchingDevice> m_pLauncher;
};
