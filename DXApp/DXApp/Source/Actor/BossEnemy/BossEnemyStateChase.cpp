#include "BossEnemyStateChase.h"
#include "BossEnemyStateIdle.h"

UsingNamespace;


BossEnemyStateChase::BossEnemyStateChase()
{
}

BossEnemyStateChase::~BossEnemyStateChase()
{
}

void BossEnemyStateChase::entryAction(component::BossEnemyComponent * outerValue)
{
	m_Animator = outerValue->getGameObj().lock()->getComponent<AnimatorComponent>();
	m_Animator.lock()->changeAnime("Golem_Walk", m_Animator.lock()->getBlendFrame());
	m_Animator.lock()->endBlend();

	m_Player = outerValue->getBossEnemyData()->pPlayer;

	m_IsEnd = false;
}

Trans BossEnemyStateChase::inputAction(component::BossEnemyComponent * outerValue, std::shared_ptr<State<component::BossEnemyComponent>>* nextState)
{
	if (!m_IsEnd) {
		//Moveステートへ切り替え
		if (isChangeIdle(outerValue))
		{
			m_Animator.lock()->beginBlend("Golem_Idle", MotionFrame::begin, 6);
			m_IsEnd = true;
		}
	}

	if (m_IsEnd) {
		if (m_Animator.lock()->isBlendTimeEnd()) {
			*nextState = std::make_shared<BossEnemyStateIdle>();
			return Trans::Trans_Occured;
		}
	}

	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	//Playerの位置
	auto otherVec = m_Player.lock()->getTransform()->m_Position - myTrans->m_Position;

	otherVec = -otherVec.normalize();

	otherVec.y = 0;

	auto front = myTrans->front();

	front += otherVec * 0.5;

	front = front.normalize();

	//向かせる方向
	myTrans->lookAt(front);

	//移動
	myTrans->m_Position += -front * m_Speed;

	*nextState = std::make_shared<BossEnemyStateChase>(*this);
	return Trans::Trans_Again;
}

void BossEnemyStateChase::exitAction(component::BossEnemyComponent * outerValue)
{
}

bool BossEnemyStateChase::isChangeIdle(component::BossEnemyComponent * outerValue)
{

	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	//プレイヤーの位置
	auto playerTrans = m_Player.lock()->getTransform();

	//距離計算
	auto vec = myTrans->m_Position - playerTrans->m_Position;

	vec.y = 0;


	//円の直径
	float serchRadius = outerValue->getBossEnemyData()->searchRange;

	//円の直径
	float attackRadius = outerValue->getBossEnemyData()->attackRange;

	//距離が円の直径以下なら
	return  vec.length() <= attackRadius || serchRadius <= vec.length();
}
