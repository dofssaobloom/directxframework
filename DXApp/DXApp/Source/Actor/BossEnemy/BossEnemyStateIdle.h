#pragma once
#include<Source\State\State.h>
#include<Source\Util\Type.h>
#include<Source\Component\Enemy\BossEnemyComponent.h>

class BossEnemyStateIdle : public State<component::BossEnemyComponent>
{
public:
	BossEnemyStateIdle();
	~BossEnemyStateIdle();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::BossEnemyComponent* outerValue);

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::BossEnemyComponent* outerValue, std::shared_ptr<State<component::BossEnemyComponent>>* nextState);

	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::BossEnemyComponent* outerValue);


private:

	//ステート変更
	bool isChangeChase(component::BossEnemyComponent * outerValue);
	bool isChangeAttack(component::BossEnemyComponent * outerValue);
	bool isChangeMove(component::BossEnemyComponent * outerValue);

private:
	std::weak_ptr<component::AnimatorComponent> m_Animator;
	framework::WeakEntity m_Player;

	std::shared_ptr<State<component::BossEnemyComponent>> m_pnNextState;
	
	bool m_IsEnd;
	/*
	//起動音
	util::Timer m_bornSETimer;
	bool m_isBornSE;
	*/
	//!半径
	const float m_Radius = 500;

};
