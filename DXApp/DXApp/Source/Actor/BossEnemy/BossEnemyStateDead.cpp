#include "BossEnemyStateDead.h"
#include "BossEnemyStateIdle.h"
#include <Source\Component\Enemy\EnemyAttackComponent.h>

UsingNamespace;


BossEnemyStateDead::BossEnemyStateDead(int bowneTimer, bool isDeadTrans)
	:m_ReBowneTimer(bowneTimer + 1),
	m_IsDeadTrans(isDeadTrans), m_bornSETimer(1)
{
}

BossEnemyStateDead::~BossEnemyStateDead()
{
}

void BossEnemyStateDead::entryAction(component::BossEnemyComponent * outerValue)
{
	m_Animator = outerValue->getGameObj().lock()->getComponent<AnimatorComponent>();
	m_Animator.lock()->changeAnime("Golem_Death", 0);
	m_Animator.lock()->endBlend();
	if (m_IsDeadTrans) {
		auto endFrame = m_Animator.lock()->getEndFrame();
		m_Animator.lock()->changeAnime("Golem_Idle");
		m_Animator.lock()->changeAnime("Golem_Death", endFrame);
	}
	else {
		//最初から死亡状態だった場合を除いてスコア追加
		outerValue->addBoddScore(outerValue->getBossEnemyData()->deadCount);
		outerValue->getBossEnemyData()->deadCount += 1;
	}
	m_BeginPos = outerValue->getGameObj().lock()->getTransform()->m_Position;
	//auto rigidBody =  outerValue->getGameObj().lock()->getComponent<BulletRigidBody>();
	m_IsBowne = false;

	outerValue->deActiveCollision();

	//起動音
	m_isBornSE = false;
	m_bornSETimer.init();
}

Trans BossEnemyStateDead::inputAction(component::BossEnemyComponent * outerValue, std::shared_ptr<State<component::BossEnemyComponent>>* nextState)
{
	outerValue->getGameObj().lock()->getTransform()->m_Position = m_BeginPos;

	/*
	m_bornSETimer.update();
	//起動音の再生
	if (m_bornSETimer.isEnd()) {
		if (!m_isBornSE) {
			outerValue->bornSE();
			m_isBornSE = true;
		}
	}
	*/

	//Idleステートへ切り替え
	if (isChange(outerValue) && !m_IsBowne)
	{
		m_Animator.lock()->changeAnime("Golem_Spawn");
		m_IsBowne = true;
	}

	if (m_Animator.lock()->getCurrentName() == "Golem_Spawn" && m_Animator.lock()->isEnd()) {
		outerValue->initHP();
		outerValue->getBossEnemyData()->isDead = false;
		*nextState = std::make_shared<BossEnemyStateIdle>();
		return Trans::Trans_Occured;
	}

	*nextState = std::make_shared<BossEnemyStateDead>(*this);
	return Trans::Trans_Again;
}

void BossEnemyStateDead::exitAction(component::BossEnemyComponent * outerValue)
{
}

bool BossEnemyStateDead::isChange(component::BossEnemyComponent * outerValue)
{
	m_ReBowneTimer.update();
	return m_ReBowneTimer.isEnd();
}
