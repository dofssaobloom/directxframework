#include "BossEnemyStateMove.h"
#include "BossEnemyStateIdle.h"

UsingNamespace;

BossEnemyStateMove::BossEnemyStateMove()
{
}

BossEnemyStateMove::~BossEnemyStateMove()
{
}

void BossEnemyStateMove::entryAction(component::BossEnemyComponent * outerValue)
{
	m_Animator = outerValue->getGameObj().lock()->getComponent<AnimatorComponent>();
	m_Animator.lock()->changeAnime("Golem_Walk", m_Animator.lock()->getBlendFrame());
	m_Animator.lock()->endBlend();

	m_Player = outerValue->getBossEnemyData()->pPlayer;

	registerPatrolPos();

	m_IsEnd = false;
}

Trans BossEnemyStateMove::inputAction(component::BossEnemyComponent * outerValue, std::shared_ptr<State<component::BossEnemyComponent>>* nextState)
{
	if (!m_IsEnd) {
		//Moveステートへ切り替え
		if (isChange(outerValue))
		{
			m_Animator.lock()->beginBlend("Golem_Idle",MotionFrame::begin,6);
			m_IsEnd = true;
		}
	}

	if (m_IsEnd) {
		if (m_Animator.lock()->isBlendTimeEnd()) {
			*nextState = std::make_shared<BossEnemyStateIdle>();
			return Trans::Trans_Occured;
		}
	}

	outerValue->getBossEnemyData()->pPatrolPosNumber %= m_patrolPositions.size();

	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	//Playerの位置
	auto otherVec = m_patrolPositions[outerValue->getBossEnemyData()->pPatrolPosNumber] - myTrans->m_Position;

	otherVec.y = 0;

	if (otherVec.length() <= 50)
		outerValue->getBossEnemyData()->pPatrolPosNumber += 1;

	otherVec = -otherVec.normalize();

	auto front = myTrans->front();

	front += otherVec * 0.5;

	front = front.normalize();

	//向かせる方向
	myTrans->lookAt(front);

	//移動
	myTrans->m_Position += -front * m_Speed;

	*nextState = std::make_shared<BossEnemyStateMove>(*this);
	return Trans::Trans_Again;
}

void BossEnemyStateMove::exitAction(component::BossEnemyComponent * outerValue)
{
}

bool BossEnemyStateMove::isChange(component::BossEnemyComponent * outerValue)
{
	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	//プレイヤーの位置
	auto playerTrans = m_Player.lock()->getTransform();

	//距離計算
	auto vec = myTrans->m_Position - playerTrans->m_Position;

	vec.y = 0;

	//円の直径
	float searchRadisu = outerValue->getBossEnemyData()->searchRange;

	//距離が円の直径以下なら
	return vec.length() < searchRadisu;
}

void BossEnemyStateMove::registerPatrolPos()
{
	util::Vec3 initPos = util::Vec3(-1000, -300, 0);
	util::Vec3 patrolFront = util::Vec3(-300, -300, 100);
	util::Vec3 patrolLeft = util::Vec3(-900, -300, -1300);
	util::Vec3 patrolRight = util::Vec3(-1200, -300, 1200);

	m_patrolPositions.clear();
	m_patrolPositions.reserve(6);
	m_patrolPositions.emplace_back(initPos);
	m_patrolPositions.emplace_back(patrolLeft);
	m_patrolPositions.emplace_back(initPos);
	m_patrolPositions.emplace_back(patrolFront);
	m_patrolPositions.emplace_back(initPos);
	m_patrolPositions.emplace_back(patrolRight);
}
