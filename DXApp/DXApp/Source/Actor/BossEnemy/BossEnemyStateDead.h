#pragma once

#include<Source\State\State.h>
#include<Source\Util\Type.h>
#include<Source\Component\Enemy\BossEnemyComponent.h>

class BossEnemyStateDead : public State<component::BossEnemyComponent>
{
public:
	BossEnemyStateDead(int bowneTimer,bool isDeadTrans = false);
	~BossEnemyStateDead();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::BossEnemyComponent* outerValue);

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::BossEnemyComponent* outerValue, std::shared_ptr<State<component::BossEnemyComponent>>* nextState);

	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::BossEnemyComponent* outerValue);


private:

	//ステート変更
	bool isChange(component::BossEnemyComponent * outerValue);

private:
	std::weak_ptr<component::AnimatorComponent> m_Animator;
	util::Timer m_ReBowneTimer;

	//!最初から岩形態の状態にするかどうか
	bool m_IsDeadTrans;

	//起動音
	util::Timer m_bornSETimer;
	bool m_isBornSE;

	util::Vec3 m_BeginPos;
	bool m_IsBowne;
	
};