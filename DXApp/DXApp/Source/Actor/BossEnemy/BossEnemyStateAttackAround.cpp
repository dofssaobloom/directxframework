#include "BossEnemyStateAttackAround.h"
#include <Source\Component\Enemy\EnemyAttackComponent.h>
#include "BossEnemyStateIdle.h"
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>

UsingNamespace;

BossEnemyStateAttackAround::BossEnemyStateAttackAround()
	//:m_creshSETimer(35)
{
}

BossEnemyStateAttackAround::~BossEnemyStateAttackAround()
{
}

void BossEnemyStateAttackAround::entryAction(component::BossEnemyComponent * outerValue)
{
	m_Animator = outerValue->getGameObj().lock()->getComponent<AnimatorComponent>();

	if (m_Animator.lock()->isBlend()) {
		m_Animator.lock()->changeAnime(m_Animator.lock()->getBlendMotionName(), m_Animator.lock()->getBlendFrame());

	}
	else {
		m_Animator.lock()->changeAnime(m_Animator.lock()->getCurrentName(), m_Animator.lock()->getBlendFrame());
	}

	m_Animator.lock()->endBlend();

	m_Animator.lock()->beginBlend("Golem_Attack2", MotionFrame::begin, 12);

	auto attack = outerValue->getGameObj().lock()->getComponent<EnemyAttackComponent>();
	float colliderRadius = 230.0f;
	attack.lock()->changeColliderRadius(colliderRadius);

	util::Vec3 colliderOffSet = util::Vec3(120, -10, 0);
	auto myTrans = outerValue->getGameObj().lock()->getTransform();
	util::Vec3 colliderRocalOffSet =
		myTrans->front()  * colliderOffSet.x
		+ myTrans->up()   * colliderOffSet.y
		+ myTrans->left() * colliderOffSet.z;
	attack.lock()->changeColliderOffset(colliderRocalOffSet);

	//m_creshSETimer.init();
	//m_isCrashSE = false;
}

Trans BossEnemyStateAttackAround::inputAction(component::BossEnemyComponent * outerValue, std::shared_ptr<State<component::BossEnemyComponent>>* nextState)
{
	/*
	m_creshSETimer.update();
	//Crash音鳴らす
	if (m_creshSETimer.isEnd()) {
		if (!m_isCrashSE) {
			outerValue->attackSE();
			m_isCrashSE = true;
		}
	}
	*/

	if (m_Animator.lock()->isBlend()) {
		if (m_Animator.lock()->getBlendMotionName() == "Golem_Attack2") {
			if (m_Animator.lock()->isBlendTimeEnd()) {
				m_Animator.lock()->changeAnime("Golem_Attack2", m_Animator.lock()->getBlendFrame());
				m_Animator.lock()->endBlend();
			}
		}
	}


	//Idleステートへ切り替え
	if (isChange(outerValue))
	{
		*nextState = std::make_shared<BossEnemyStateIdle>();
		return Trans::Trans_Occured;
	}
	*nextState = std::make_shared<BossEnemyStateAttackAround>(*this);
	return Trans::Trans_Again;
}

void BossEnemyStateAttackAround::exitAction(component::BossEnemyComponent * outerValue)
{
	outerValue->AttackAround();
}

bool BossEnemyStateAttackAround::isChange(component::BossEnemyComponent * outerValue)
{
	return m_Animator.lock()->isEnd() && !m_Animator.lock()->isBlend();
}
