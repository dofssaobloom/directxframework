#pragma once

#include<Source\State\State.h>
#include<Source\Util\Type.h>
#include<Source\Component\Enemy\BossEnemyComponent.h>

class BossEnemyStateAttackAround : public State<component::BossEnemyComponent>
{
public:
	BossEnemyStateAttackAround();
	~BossEnemyStateAttackAround();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::BossEnemyComponent* outerValue);

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::BossEnemyComponent* outerValue, std::shared_ptr<State<component::BossEnemyComponent>>* nextState);

	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::BossEnemyComponent* outerValue);


private:

	//ステート変更
	bool isChange(component::BossEnemyComponent * outerValue);

private:
	std::weak_ptr<component::AnimatorComponent> m_Animator;

	/*
	util::Timer m_creshSETimer;
	bool m_isCrashSE;
	*/

	//!半径
	const float m_AttackRadius = 500;

};