#include "BossEnemyStateAttackPlayer.h"
#include <Source\Component\Enemy\EnemyAttackComponent.h>
#include "BossEnemyStateIdle.h"

UsingNamespace;

BossEnemyStateAttackPlayer::BossEnemyStateAttackPlayer()
{
}

BossEnemyStateAttackPlayer::~BossEnemyStateAttackPlayer()
{
}

void BossEnemyStateAttackPlayer::entryAction(component::BossEnemyComponent * outerValue)
{
	m_Animator = outerValue->getGameObj().lock()->getComponent<AnimatorComponent>();

	m_Animator.lock()->changeAnime("Golem_Attack1", m_Animator.lock()->getBlendFrame());
	m_Animator.lock()->endBlend();
	auto attack = outerValue->getGameObj().lock()->getComponent<EnemyAttackComponent>();
	float colliderRadius = 82.0f;
	attack.lock()->changeColliderRadius(colliderRadius);

	util::Vec3 colliderOffSet = util::Vec3(160, -70, 40);
	auto myTrans = outerValue->getGameObj().lock()->getTransform();
	util::Vec3 colliderRocalOffSet =
		myTrans->front() * colliderOffSet.x
		+ myTrans->up()   * colliderOffSet.y
		+ myTrans->left() * colliderOffSet.z;
	attack.lock()->changeColliderOffset(colliderRocalOffSet);

	m_IsEnd = false;
}

Trans BossEnemyStateAttackPlayer::inputAction(component::BossEnemyComponent * outerValue, std::shared_ptr<State<component::BossEnemyComponent>>* nextState)
{
	if (!m_IsEnd) {
		//Idleステートへ切り替え
		if (isChange(outerValue))
		{
			m_Animator.lock()->beginBlend("Golem_Idle", MotionFrame::begin, 6);
			m_IsEnd = true;
		}
	}

	if (m_IsEnd) {
		if (m_Animator.lock()->isBlendTimeEnd()) {
			*nextState = std::make_shared<BossEnemyStateIdle>();
			return Trans::Trans_Occured;
		}
	}

	*nextState = std::make_shared<BossEnemyStateAttackPlayer>(*this);
	return Trans::Trans_Again;
}

void BossEnemyStateAttackPlayer::exitAction(component::BossEnemyComponent * outerValue)
{
	outerValue->AttackPlayer();
}

bool BossEnemyStateAttackPlayer::isChange(component::BossEnemyComponent * outerValue)
{
	return m_Animator.lock()->isEnd();
}
