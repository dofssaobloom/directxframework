#include "BossEnemyStateIdle.h"
#include "BossEnemyStateChase.h"
#include "BossEnemyStateAttackPlayer.h"
#include "BossEnemyStateMove.h"


UsingNamespace;

BossEnemyStateIdle::BossEnemyStateIdle()
	//:m_bornSETimer(1)
{
}

BossEnemyStateIdle::~BossEnemyStateIdle()
{
}

void BossEnemyStateIdle::entryAction(component::BossEnemyComponent * outerValue)
{
	m_Animator = outerValue->getGameObj().lock()->getComponent<AnimatorComponent>();

	m_Animator.lock()->endBlend();
	m_Animator.lock()->changeAnime("Golem_Idle");

	m_Player = outerValue->getBossEnemyData()->pPlayer;

	m_IsEnd = false;
	/*
	//起動音
	m_isBornSE = false;
	m_bornSETimer.init();
	*/
}

Trans BossEnemyStateIdle::inputAction(component::BossEnemyComponent * outerValue, std::shared_ptr<State<component::BossEnemyComponent>>* nextState)
{
	/*
	m_bornSETimer.update();
	//起動音の再生
	if (m_bornSETimer.isEnd()) {
		if (!m_isBornSE) {
			outerValue->bornSE();
			m_isBornSE = true;
		}
	}
	*/

	if (!m_IsEnd) {
		//ステート切り替え
		if (isChangeAttack(outerValue))
		{
			m_pnNextState = std::make_shared<BossEnemyStateAttackPlayer>();
			m_Animator.lock()->beginBlend("Golem_Attack1", MotionFrame::begin, 12);
			m_IsEnd = true;
		}
		else if (isChangeChase(outerValue))
		{
			m_pnNextState = std::make_shared<BossEnemyStateChase>();
			m_Animator.lock()->beginBlend("Golem_Walk", MotionFrame::begin, 12);
			m_IsEnd = true;
		}
		else if (isChangeMove(outerValue))
		{
			m_pnNextState = std::make_shared<BossEnemyStateMove>();
			m_Animator.lock()->beginBlend("Golem_Walk", MotionFrame::begin, 12);
			m_IsEnd = true;
		}
	}

	if (m_IsEnd) {
		if (m_Animator.lock()->isBlend()) {
			if (m_Animator.lock()->isBlendTimeEnd()) {
				*nextState = m_pnNextState;
				return Trans::Trans_Occured;
			}
		}
	}

	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	//Playerの位置
	auto otherVec = m_Player.lock()->getTransform()->m_Position - myTrans->m_Position;

	otherVec = -otherVec.normalize();

	otherVec.y = 0;

	auto front = myTrans->front();

	front += otherVec * 0.5;

	front = front.normalize();

	//向かせる方向
	myTrans->lookAt(front);

	*nextState = std::make_shared<BossEnemyStateIdle>(*this);
	return Trans::Trans_Again;
}

void BossEnemyStateIdle::exitAction(component::BossEnemyComponent * outerValue)
{
}

bool BossEnemyStateIdle::isChangeChase(component::BossEnemyComponent * outerValue)
{
	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	//プレイヤーの位置
	auto playerTrans = m_Player.lock()->getTransform();

	//距離計算
	auto vec = myTrans->m_Position - playerTrans->m_Position;

	vec.y = 0;

	//円の直径
	float searchRadisu = outerValue->getBossEnemyData()->searchRange;
	//円の直径
	float attackRadius = outerValue->getBossEnemyData()->attackRange;

	//距離が円の直径以下なら
	bool inChaseDistance = attackRadius < vec.length() && vec.length() < searchRadisu;

	return inChaseDistance;
}

bool BossEnemyStateIdle::isChangeAttack(component::BossEnemyComponent * outerValue)
{
	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	//プレイヤーの位置
	auto playerTrans = m_Player.lock()->getTransform();

	//距離計算
	auto vec = myTrans->m_Position - playerTrans->m_Position;

	vec.y = 0;

	//円の直径
	float attackRadius = outerValue->getBossEnemyData()->attackRange;

	//距離が円の直径以下なら
	bool inAttckDistance = vec.length() <= attackRadius;;

	return inAttckDistance && outerValue->getBossEnemyData()->canAttackPlayer;
}

bool BossEnemyStateIdle::isChangeMove(component::BossEnemyComponent * outerValue)
{
	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	//プレイヤーの位置
	auto playerTrans = m_Player.lock()->getTransform();

	//距離計算
	auto vec = myTrans->m_Position - playerTrans->m_Position;

	vec.y = 0;

	//円の直径
	float searchRadisu = outerValue->getBossEnemyData()->searchRange;

	//距離が円の直径以下なら
	return searchRadisu <= vec.length();
}
