#include "BossEnemyStateDamage.h"
#include <Source\Component\Enemy\EnemyAttackComponent.h>
#include"BossEnemyStateIdle.h"

UsingNamespace;

BossEnemyStateDamage::BossEnemyStateDamage()
{
}

BossEnemyStateDamage::~BossEnemyStateDamage()
{
}

void BossEnemyStateDamage::entryAction(component::BossEnemyComponent * outerValue)
{
	m_Animator = outerValue->getGameObj().lock()->getComponent<AnimatorComponent>();

	//if (m_Animator.lock()->isBlend()) {
	//	m_Animator.lock()->endBlend();
	//	m_Animator.lock()->changeAnime(m_Animator.lock()->getBlendMotionName(), m_Animator.lock()->getBlendFrame());
	//}
	//else {
	//	m_Animator.lock()->changeAnime(m_Animator.lock()->getCurrentName(), m_Animator.lock()->getFrame());
	//}
	m_Animator.lock()->endBlend();
	m_Animator.lock()->changeAnime("Golem_Damage", 0);
	//m_Animator.lock()->beginBlend("Golem_Damage", MotionFrame::begin, 6);

	m_Player = outerValue->getBossEnemyData()->pPlayer;

	m_IsEnd = false;
}

Trans BossEnemyStateDamage::inputAction(component::BossEnemyComponent * outerValue, std::shared_ptr<State<component::BossEnemyComponent>>* nextState)
{
	if (!m_IsEnd) {
	/*	if (m_Animator.lock()->isBlend()) {
			if (m_Animator.lock()->isBlendAnimeEnd()) {
				m_Animator.lock()->changeAnime(m_Animator.lock()->getBlendMotionName(), m_Animator.lock()->getBlendFrame());
				m_Animator.lock()->beginBlend("Golem_Idle", MotionFrame::begin, 6);
				m_IsEnd = true;
			}
		}*/

		if (m_Animator.lock()->isEnd()) {
			m_Animator.lock()->beginBlend("Golem_Idle", MotionFrame::begin, 6);
			m_IsEnd = true;
		}
	}


	if (m_IsEnd) {
		if (m_Animator.lock()->isBlendTimeEnd()) {
			*nextState = std::make_shared<BossEnemyStateIdle>();
			return Trans::Trans_Occured;
		}
	}


	*nextState = std::make_shared<BossEnemyStateDamage>(*this);
	return Trans::Trans_Again;
}

void BossEnemyStateDamage::exitAction(component::BossEnemyComponent * outerValue)
{
}
