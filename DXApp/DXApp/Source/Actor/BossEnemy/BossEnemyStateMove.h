#pragma once
#include<Source\State\State.h>
#include<Source\Util\Type.h>
#include<Source\Component\Enemy\BossEnemyComponent.h>

class BossEnemyStateMove : public State<component::BossEnemyComponent>
{
public:
	BossEnemyStateMove();
	~BossEnemyStateMove();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::BossEnemyComponent* outerValue);

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::BossEnemyComponent* outerValue, std::shared_ptr<State<component::BossEnemyComponent>>* nextState);

	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::BossEnemyComponent* outerValue);


private:

	//ステート変更
	bool isChange(component::BossEnemyComponent * outerValue);

	void registerPatrolPos();

private:
	std::weak_ptr<component::AnimatorComponent> m_Animator;
	framework::WeakEntity m_Player;

	bool m_IsEnd;

	std::vector<util::Vec3> m_patrolPositions;
	const float m_Speed = 4.5f;
};
