#include"PlayerDamageState.h"
#include<Framework.h>
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Actor\PlayerIdleState.h>
#include<Source\Actor\PlayerMoveState.h>
#include<Source\Actor\PlayerDeadState.h>
#include<Source\Component\Device\CameraController.h>
#include<Source\Component\Bullet\LaunchingDevice.h>

UsingNamespace;

PlayerDamageState::PlayerDamageState(HitEntity hitOther)
	:m_HitOther(hitOther)
{
}

PlayerDamageState::~PlayerDamageState()
{
}

void PlayerDamageState::entryAction(component::PlayerComponent * outerValue)
{
	//攻撃モーションに変更
	auto&& currentState = outerValue->getPlayerData().lock()->currentState = PlayerState::damage;

	m_IsDead = outerValue->getPlayerData().lock()->hp <= 0;

	m_Animator = outerValue->getGameObj().lock()->getComponent<component::AnimatorComponent>();
	assert(!m_Animator.expired() && "プレイヤーにアニメーターがついていません");
	//移動モーションに切り替える

	m_Animator.lock()->endBlend();
	if (m_HitOther == HitEntity::Enemy) {
		m_FirstMotionName = outerValue->getPlayerData().lock()->motionNames[currentState];
	}
	else {
		m_FirstMotionName = "Player_Blow";
	}

	m_Animator.lock()->beginBlend(m_FirstMotionName, component::MotionFrame::begin, 6);

	/*m_Animator.lock()->changeAnime(outerValue->getPlayerData().lock()->motionNames[PlayerState::damage], 0);
	m_Animator.lock()->beginBlend("Player_ShootingIdle", component::MotionFrame::begin, 30);
*/
	outerValue->getPlayerData().lock()->hitDamage = true;

	outerValue->getPlayerData().lock()->pCameraController.lock()->changeView(CameraViewType::ThirdPersonView);
	outerValue->getPlayerData().lock()->pLauncher.lock()->deActiveLine();

	//アニメーションが止まってる可能性があるのでスタートさせる
	m_Animator.lock()->onStart();

	outerValue->damageSE();

	m_IsShootingIdleBlendEnd = false;
	m_IsEnd = false;
}

Trans PlayerDamageState::inputAction(component::PlayerComponent * outerValue, std::shared_ptr<State<component::PlayerComponent>>* nextState)
{
	bool isEnd = false;

	if (m_HitOther == HitEntity::Enemy) {
		isEnd = hitEnemyUpdate(outerValue);
	}
	else {
		isEnd = hitPoisonCollisionUpdate(outerValue);
	}

	//終了フラグが立って居たら次のステートへ
	if (isEnd) {
		*nextState = m_pNextState;
		return Trans::Trans_Occured;
	}


	*nextState = std::make_shared<PlayerDamageState>(*this);
	return Trans::Trans_Again;
}

void PlayerDamageState::exitAction(component::PlayerComponent * outerValue)
{
}

bool PlayerDamageState::hitEnemyUpdate(component::PlayerComponent * outerValue)
{
	if (m_Animator.lock()->isBlend()) {
		if (m_Animator.lock()->getBlendMotionName() == m_FirstMotionName) {
			if (m_Animator.lock()->isBlendTimeEnd()) {
				m_Animator.lock()->changeAnime(m_FirstMotionName, m_Animator.lock()->getBlendFrame());
				m_Animator.lock()->endBlend();
				m_IsShootingIdleBlendEnd = true;
			}
		}
	}


	if (m_IsShootingIdleBlendEnd && m_Animator.lock()->rate() >= 0.3f && m_Animator.lock()->getCurrentName() == m_FirstMotionName) {
		if (!m_IsEnd) {
			if (m_IsDead) {
				//死んで入れば死亡アニメーション
				m_Animator.lock()->beginBlend("Player_Dead", component::MotionFrame::begin, 20);

			}
			else {
				m_Animator.lock()->beginBlend("Player_Idle", component::MotionFrame::begin, 10);
			}

			//爆発にあたった場合はそのまま死亡ステートへ
			m_IsEnd = true;
		}
	}


	if (m_IsEnd ) {
		auto isBlendEnd = false;
		if (m_Animator.lock()->isBlend()) {
			isBlendEnd = m_Animator.lock()->isBlendTimeEnd();
		}
		else {
			isBlendEnd = true;
		}

		if (isBlendEnd) {
			if (m_IsDead) {
				m_pNextState = std::make_shared<PlayerDeadState>();
				return true;
			}
			else {
				m_pNextState = std::make_shared<PlayerIdleState>();
				return true;
			}
		}
	}

	return false;
}

bool PlayerDamageState::hitPoisonCollisionUpdate(component::PlayerComponent * outerValue)
{

	if (m_Animator.lock()->isBlend()) {
		if (m_Animator.lock()->getBlendMotionName() == m_FirstMotionName) {
			if (m_Animator.lock()->isBlendTimeEnd()) {
				m_Animator.lock()->changeAnime(m_FirstMotionName, m_Animator.lock()->getBlendFrame());
				m_Animator.lock()->endBlend();
				m_IsShootingIdleBlendEnd = true;
			}
		}
	}


	if (m_IsShootingIdleBlendEnd && m_Animator.lock()->getCurrentName() == m_FirstMotionName) {
		if (!m_IsEnd) {
			if (m_IsDead) {
				if (m_Animator.lock()->getFrame() == 30) {
					m_Animator.lock()->onStop();
					m_IsEnd = true;
				}
			}
			else if(m_Animator.lock()->rate() >= 0.8f){
				m_Animator.lock()->beginBlend("Player_Idle", component::MotionFrame::begin, 20);
				m_IsEnd = true;
			}
		}
	}


	if (!m_IsDead) {
		//地面についていない間はアニメーションを止める
		if (m_Animator.lock()->isBlend()) {
			if (m_Animator.lock()->getBlendMotionName() == "Player_Blow" && !outerValue->getPlayerData().lock()->isFloorContact) {
				if (m_Animator.lock()->getBlendFrame() == 36) {
					m_Animator.lock()->onStop();
				}
			}
		}
		else {
			m_Animator.lock()->onStart();
		}
	}


	if (m_IsEnd && outerValue->getPlayerData().lock()->isFloorContact) {
		auto isBlendEnd = false;
		if (m_Animator.lock()->isBlend()) {
			isBlendEnd = m_Animator.lock()->isBlendTimeEnd();
		}
		else {
			isBlendEnd = true;
		}
		if (isBlendEnd) {
			if (m_IsDead) {
				m_pNextState = std::make_shared<PlayerDeadState>();
				return true;
			}
			else {
				m_pNextState = std::make_shared<PlayerIdleState>();
				return true;
			}
		}
	}
	return false;
}

