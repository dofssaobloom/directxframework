#pragma once
#include<Source\State\State.h>
#include<Source\Util\Type.h>
#include<Source\Component\Enemy\EnemyComponent.h>

class EnemyIdleState : public State<component::EnemyComponent>
{
public:
	EnemyIdleState();
	~EnemyIdleState();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::EnemyComponent* outerValue);

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::EnemyComponent* outerValue, std::shared_ptr<State<component::EnemyComponent>>* nextState);

	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::EnemyComponent* outerValue);


private:

	//ステート変更
	bool isChange(component::EnemyComponent * outerValue);

private:
	std::weak_ptr<component::AnimatorComponent> m_Animator;
	framework::WeakEntity m_Player;
	
	//!半径
	const float m_Radius = 500;

};