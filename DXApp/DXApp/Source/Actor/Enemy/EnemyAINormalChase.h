#pragma once
#include "EnemyAI.h"

//追跡するAI
class EnemyAINormalChase : public EnemyAI
{
public :
	EnemyAINormalChase();
	~EnemyAINormalChase();
	
	// EnemyAI を介して継承されました
	virtual void move(component::EnemyComponent * outerValue) override;

};
