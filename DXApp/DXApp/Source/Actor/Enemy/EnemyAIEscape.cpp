#include "EnemyAIEscape.h"

EnemyAIEscape::EnemyAIEscape()
{
	m_escapeCount = 0;
}

EnemyAIEscape::~EnemyAIEscape()
{
}

void EnemyAIEscape::move(component::EnemyComponent * outerValue)
{
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	auto playerTrans = outerValue->getEnemyData()->pPlayer.lock()->getTransform();

	//�����͈͂Ƀv���C���[����������
	float toPlayerDistance = (playerTrans->m_Position - myTrans->m_Position).length();
	if (toPlayerDistance < m_escapeDistance)
	{
		//�������Ԑݒ�
		m_escapeCount = m_escapeTime;
	}
	
	//�������Ԃ����݂���ꍇ
	if (0 < m_escapeCount)
	{
		escape(outerValue);
		m_escapeCount -= 1;
	}
	else if(toPlayerDistance < m_lookDistance)
		look(outerValue);
	else
		randomMove(outerValue);
}
