#include "EnemyAIRandomMove.h"

EnemyAIRandomMove::EnemyAIRandomMove()
{
}

EnemyAIRandomMove::~EnemyAIRandomMove()
{
}

void EnemyAIRandomMove::move(component::EnemyComponent * outerValue)
{
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	auto playerTrans = outerValue->getEnemyData()->pPlayer.lock()->getTransform();

	float toPlayerDistance = (playerTrans->m_Position - myTrans->m_Position).length();

	if (toPlayerDistance < m_chaseDistance)
		chase(outerValue);
	else
		randomMove(outerValue);
}
