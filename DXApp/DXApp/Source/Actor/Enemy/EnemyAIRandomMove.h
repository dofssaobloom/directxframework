#pragma once

#include "EnemyAI.h"

//基本ランダムで移動してプレイヤーが近づくと追跡するAI
class EnemyAIRandomMove: public EnemyAI
{
public:
	EnemyAIRandomMove();
	~EnemyAIRandomMove();

	// EnemyAI を介して継承されました
	virtual void move(component::EnemyComponent * outerValue) override;

private:
	const float m_chaseDistance = 800.0f;
};