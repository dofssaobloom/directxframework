#include "EnemyIdleState.h"
#include "EnemyMoveState.h"
#include<Source\Component\Player\SearchComponent.h>
#include<Source\Component\Physics\PhysicsWorld.h>

UsingNamespace;

EnemyIdleState::EnemyIdleState()
{
}

EnemyIdleState::~EnemyIdleState()
{
}

void EnemyIdleState::entryAction(component::EnemyComponent * outerValue)
{
	m_Animator = outerValue->getGameObj().lock()->getComponent<AnimatorComponent>();
	m_Animator.lock()->endBlend();
	m_Animator.lock()->changeAnime("Mushroom_Idle");

	m_Player = outerValue->getEnemyData()->pPlayer;
}

Trans EnemyIdleState::inputAction(component::EnemyComponent * outerValue, std::shared_ptr<State<component::EnemyComponent>>* nextState)
{
	*nextState = std::make_shared<EnemyMoveState>();
	return Trans::Trans_Occured;
	auto trans = outerValue->getGameObj().lock()->getTransform();

	auto vec = trans->m_Position - m_Player.lock()->getTransform()->m_Position;
	vec.y = 0;
	trans->lookAt(vec);

	//auto&& beginPos = trans->m_Position + util::Vec3(0, 50, 0);
	//auto&& end = beginPos + -trans->front() * 150;
	//auto rayTest = outerValue->getEnemyData()->pWorld.lock()->rayCast(beginPos, end);
	//if (rayTest.hasHit())
	//{
	//	auto&& tag = rayTest.getHitEntity()->getTag();
	//	if ( tag == "Stage") {
	//		*nextState = std::make_shared<EnemyMoveState>();
	//		return Trans::Trans_Occured;
	//	}
	//}
	//else {
	//	*nextState = std::make_shared<EnemyMoveState>();
	//	return Trans::Trans_Occured;
	//}

	//ฺฎ
	//trans->m_Position.x += 2;

	*nextState = std::make_shared<EnemyIdleState>(*this);
	return Trans::Trans_Again;
}

void EnemyIdleState::exitAction(component::EnemyComponent * outerValue)
{
	//m_Animator.reset();
}

bool EnemyIdleState::isChange(component::EnemyComponent * outerValue)
{
	//~ฬผa
	float totalRadisu = (outerValue->getEnemyData()->searchRange * 100) * 2;

	//Enemyฬสu
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	//vC[ฬสu
	auto playerTrans = m_Player.lock()->getTransform();

	//ฃvZ
	auto vec = myTrans->m_Position - playerTrans->m_Position;

	vec.y = 0;

	//ฃช~ฬผaศบศ็
	return vec.length() < totalRadisu;
}
