#pragma once
#include<Source\State\State.h>
#include<Source\Util\Type.h>
#include<Source\Component\Enemy\EnemyComponent.h>

namespace framework {
	class Effect;
}

namespace component {
	class EnemyComponent;
	class BulletRigidBody;
	class AnimatorComponent;
}

class EnemySwellState : public State<component::EnemyComponent>
{
public:
	EnemySwellState(int time,bool isHit = false, const util::Vec3& dir = util::Vec3());
	~EnemySwellState();


	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::EnemyComponent* outerValue)override;

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::EnemyComponent* outerValue, std::shared_ptr<State<component::EnemyComponent>>* nextState)override;

	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::EnemyComponent* outerValue)override;

	void addForce(component::EnemyComponent* outerValue,const util::Vec3& dir, float power);

private :
	void move(component::EnemyComponent * outerValue);

	void rotate(util::Transform* trans);

private:

	//!膨らみタイマー
	util::Timer m_SwellTimer;

	util::Transform* trans;

	//!初期スケール
	util::Vec3 m_InitScale;

	//今の所3方向にバラバラのスケール変更する予定はない
	//!最大スケール
	float m_MaxScale;

	float m_DefOriginY;

	util::Timer m_FadeInTimer;

	//!爆発寸前のタイマ
	util::Timer m_Warning;

	std::weak_ptr<framework::Effect> m_pPoisonEffect;

	component::WeakEntity m_Player;

	std::weak_ptr<framework::Effect> m_pExplosionCircleEffect;

	std::weak_ptr<component::BulletRigidBody> m_pRigidBody;

	std::weak_ptr<component::AnimatorComponent> m_pAnimator;

	
	bool m_IsHit;
	//!ヒットしてから歩行モードに切り替わるまでの時間
	util::Timer m_HitTimer;

	//!このステートになった瞬間にヒットがあった場合に吹っ飛ぶ方向
	util::Vec3 m_DefaultHitVec;

	float m_Speed = 5;

};

