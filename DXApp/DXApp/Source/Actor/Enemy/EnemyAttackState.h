#pragma once
#pragma once
#include<Source\State\State.h>
#include<Source\Util\Type.h>
#include<Source\Component\Enemy\EnemyComponent.h>

namespace component {
	class AttackComponent;
}

namespace framework {
	class Effect;
}


class EnemyAttackState : public State<component::EnemyComponent>
{
public:
	EnemyAttackState(float length);
	~EnemyAttackState();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::EnemyComponent* outerValue);

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::EnemyComponent* outerValue, std::shared_ptr<State<component::EnemyComponent>>* nextState);

	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::EnemyComponent* outerValue);


	/// <summary>
	/// エフェクト削除
	/// </summary>
	void deleteEffect();
private:

	/// <summary>
	/// 回転
	/// </summary>
	void rotate(util::Transform* trans);

	void shoot(util::Transform* trans);


private:
	std::weak_ptr<component::AnimatorComponent> m_Animator;
	std::weak_ptr<component::AttackComponent> m_pAttackComponent;
	std::weak_ptr<framework::Effect> m_pAttackEffect;

	//緑だけチャージエフェクト使う
	std::weak_ptr<framework::Effect> m_pChageEffect;
	framework::WeakEntity m_Player;

	bool m_IsResposEnd;

	//!攻撃前に動きを止める
	util::Timer m_ResposTime;

	util::Vec3 beginPos;
	//!突進先
	util::Vec3 m_FrontMove;

	bool m_IsEnd;
	float m_Lenth;
};