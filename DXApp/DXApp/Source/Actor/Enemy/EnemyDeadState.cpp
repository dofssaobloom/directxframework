#include "EnemyDeadState.h"
#include<Source\Component\Enemy\PoisonExplosion.h>
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>
#include<Source\Component\Device\CameraController.h>
#include<Source\Component\Effect\ZoomBlur.h>

UsingNamespace;

EnemyDeadState::EnemyDeadState(){
}

EnemyDeadState::~EnemyDeadState()
{
}

void EnemyDeadState::entryAction(component::EnemyComponent * outerValue)
{
	outerValue->getEnemyData()->pPoison.lock()->activeCollision();
	m_pDeadTimer = std::make_shared<util::Timer>(2);

	auto effectSize = outerValue->getEnemyData()->poisonExplosionSize;
	auto trans = outerValue->getGameObj().lock()->getTransform();
	m_pExplosionEffect = EffectManager::getInstance()->generate("PoisonExplosion", util::Transform(trans->m_Position + util::Vec3(0,200,0),
		util::Vec3(0, 0, 0),
		util::Vec3(effectSize, effectSize, effectSize)));

	outerValue->getEnemyData()->pCameraController.lock()->onShake(outerValue->getGameObj());

	outerValue->getEnemyData()->pBluer.lock()->onBlurStart(trans);

	/*
	framework::ResourceManager::getInstance()->playSound("Bomb2");
	framework::ResourceManager::getInstance()->setVolume("Bomb2", 0);
	framework::ResourceManager::getInstance()->setDoppler("Bomb2", 1);
	framework::ResourceManager::getInstance()->setDistance("Bomb2", 4, 30);

	auto speed = util::Vec3(1.0f, 0.0f, 0.0f);
	framework::ResourceManager::getInstance()->setDopplerVelocity("Bomb2", speed);

	auto myTrans = outerValue->getGameObj().lock()->getTransform();
	framework::ResourceManager::getInstance()->setPosition("Bomb2", *myTrans);
	*/


	//コンボの追加
	outerValue->addCombo();
	outerValue->addScore();
	//outerValue->addTimer();

	outerValue->bombSE();
}

Trans EnemyDeadState::inputAction(component::EnemyComponent * outerValue, std::shared_ptr<State<component::EnemyComponent>>* nextState)
{
	*nextState = std::make_shared<EnemyDeadState>(*this);
	m_pDeadTimer->update();

	//今は演出がないのですぐ死ぬようにする
	if (m_pDeadTimer->isEnd()) {
		return Trans::Trans_Occured;
	}

	//今は演出がないのですぐ死ぬようにする
	return Trans::Trans_Again;
}

void EnemyDeadState::exitAction(component::EnemyComponent * outerValue)
{
	outerValue->deadSE();

	outerValue->getEnemyData()->pPoison.lock()->destroyCollision();
	//ゲームオブジェクトを破棄する
	outerValue->destory(outerValue->getGameObj());
}
