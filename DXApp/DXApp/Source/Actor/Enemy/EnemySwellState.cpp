#include "EnemySwellState.h"
#include"EnemyDeadState.h"
#include<Framework.h>
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>
#include<Source\Component\Physics\BulletRigidBody.h>
#include<Source\Component\Player\AttackComponent.h>

UsingNamespace;

EnemySwellState::EnemySwellState(int time, bool isHit, const util::Vec3& dir)
	:m_SwellTimer(time),
	m_FadeInTimer(10),
	m_Warning(time * 0.5),
	m_HitTimer(time * 0.5)//とりま５秒
	, m_IsHit(isHit),
	m_DefaultHitVec(dir)
{
}

EnemySwellState::~EnemySwellState()
{
}

void EnemySwellState::entryAction(component::EnemyComponent * outerValue)
{
	outerValue->getEnemyData()->isDead = true;

	m_pAnimator = outerValue->getGameObj().lock()->getComponent<AnimatorComponent>();
	m_pAnimator.lock()->endBlend();
	m_pAnimator.lock()->changeAnime("Mushroom_Run");

	trans = outerValue->getGameObj().lock()->getTransform();
	m_MaxScale = outerValue->getEnemyData()->maxScale;
	m_InitScale = trans->m_Scale;

	m_DefOriginY = outerValue->getRigidBody().lock()->getDefOriginY();
	m_pPoisonEffect = EffectManager::getInstance()->generate("Poison", util::Transform(util::Vec3(),
		util::Vec3(0, 0, 0),
		util::Vec3(30, 30, 30)));

	m_pExplosionCircleEffect = EffectManager::getInstance()->generate("ExplosionCircle", util::Transform(util::Vec3(),
		util::Vec3(0, 0, 0),
		util::Vec3(130, 130, 130)));
	m_pExplosionCircleEffect.lock()->setSpeed(2);
	m_FadeInTimer.init();
	m_Warning.init();
	outerValue->damageSE();

	m_pRigidBody = outerValue->getGameObj().lock()->getComponent<BulletRigidBody>();

	outerValue->getEnemyData()->pRender.lock()->setTexLerp(1.0f);
	m_Player = outerValue->getEnemyData()->pPlayer;


	if (m_IsHit) {
		addForce(outerValue, m_DefaultHitVec, 4000);
	}

	m_HitTimer.init();

	outerValue->setColor(util::Vec4(1, 0, 1, 1));
	//outerValue->setAtten(util::Vec3(0.001,0.01,0.001));
}

Trans EnemySwellState::inputAction(component::EnemyComponent * outerValue, std::shared_ptr<State<component::EnemyComponent>>* nextState)
{
	m_FadeInTimer.update();

	if (m_IsHit) {
		m_HitTimer.update();
		if (m_HitTimer.isEnd()) {
			m_IsHit = false;

			m_pRigidBody.lock()->setRotateFreezeFlag({ true,true,true });
			m_pRigidBody.lock()->setPositionFreezeFlag({ true,false,true });
			m_pAnimator.lock()->changeAnime("Mushroom_Run");
		}
	}
	else {
		move(outerValue);
	}

	if (m_pExplosionCircleEffect.expired()) {
		//エフェクトが終了したらまた生成
		m_pExplosionCircleEffect = EffectManager::getInstance()->generate("ExplosionCircle", util::Transform(util::Vec3(),
			util::Vec3(0, 0, 0),
			util::Vec3(130, 130, 130)));
	}

	//点滅部分
	if (m_SwellTimer.rate() <= 0.5f) {
		m_Warning.update();
		if (!m_pExplosionCircleEffect.expired())
			m_pExplosionCircleEffect.lock()->setSpeed(util::lerp<float>(m_Warning.rate(), 7.0f, 1.0f));
	}


	//エフェクトのポジション更新
	if (!m_pPoisonEffect.expired())
		m_pPoisonEffect.lock()->m_Trans.m_Position = outerValue->getGameObj().lock()->getTransform()->m_Position + util::Vec3(0, util::lerp<float>(1 - m_SwellTimer.rate(), 100, 250), 0);
	if (!m_pExplosionCircleEffect.expired())
		m_pExplosionCircleEffect.lock()->m_Trans.m_Position = outerValue->getGameObj().lock()->getTransform()->m_Position + util::Vec3(0, 10, 0);

	m_SwellTimer.update();

	auto&& rate = 1.0f - m_SwellTimer.rate();
	outerValue->getEnemyData()->pRender.lock()->setFat(util::lerp<float>(rate, 0, 0.3));



	if (m_SwellTimer.isEnd() || outerValue->getEnemyData()->isContactExplosion) {
		//死亡ステートに遷移する
		*nextState = std::make_shared<EnemyDeadState>();
		return Trans::Trans_Occured;
	}

	//球体なので全部同じスケール
	trans->m_Scale.x = trans->m_Scale.y = trans->m_Scale.z = util::lerp<float>(1 - m_SwellTimer.rate(), m_InitScale.x, m_MaxScale);

	//コリジョンのサイズ変更
	auto&& size = outerValue->getEnemyData()->colliderSize;
	float collisionSize = util::lerp<float>(1 - m_SwellTimer.rate(), size.first, size.second);
	outerValue->getEnemyData()->pSphereCollider.lock()->setScale(collisionSize);
	trans->m_Origin = util::Vec3(0, util::lerp<float>(m_DefOriginY, -collisionSize, collisionSize), 0);
	//オフセット変更
	auto&& offset = outerValue->getEnemyData()->colliderOffset;
	float collisionOffset = util::lerp<float>(1 - m_SwellTimer.rate(), offset.first, offset.second);
	outerValue->getEnemyData()->pSphereCollider.lock()->setOffset(util::Vec3(0, collisionOffset, 0));


	//自分を返す
	*nextState = std::make_shared<EnemySwellState>(*this);
	return Trans::Trans_Again;
}

void EnemySwellState::exitAction(component::EnemyComponent * outerValue)
{
	//エフェクト削除
	if (!m_pPoisonEffect.expired()) {
		auto handle = m_pPoisonEffect.lock()->getHandle();
		EffectManager::getInstance()->effectStop(handle);
	}
	if (!m_pExplosionCircleEffect.expired()) {
		auto handle = m_pExplosionCircleEffect.lock()->getHandle();
		EffectManager::getInstance()->effectStop(handle);
	}
}

void EnemySwellState::addForce(component::EnemyComponent * outerValue, const util::Vec3 & dir, float power)
{
	m_pRigidBody.lock()->setRotateFreezeFlag({ false,false,false });
	m_pRigidBody.lock()->setPositionFreezeFlag({ false,false,false });
	m_pAnimator.lock()->changeAnime("Mushroom_Idle");
	m_pRigidBody.lock()->addForce(dir * power);
	m_IsHit = true;
	m_HitTimer.init();
}


void EnemySwellState::move(component::EnemyComponent * outerValue)
{
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	//Playerの位置
	auto otherVec = m_Player.lock()->getTransform()->m_Position - myTrans->m_Position;

	otherVec = otherVec.normalize();

	otherVec.y = 0;

	//向かせる方向
	rotate(myTrans);
	//移動
	myTrans->m_Position += otherVec * m_Speed;
}

void EnemySwellState::rotate(util::Transform * trans)
{
	auto vec = trans->m_Position - m_Player.lock()->getTransform()->m_Position;
	vec.y = 0;
	vec = vec.normalize();

	auto front = trans->front();

	front += vec * 0.14;
	front = front.normalize();

	trans->lookAt(front);
}
