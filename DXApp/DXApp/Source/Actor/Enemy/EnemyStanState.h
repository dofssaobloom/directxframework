#pragma once
#include<Source\State\State.h>
#include<Source\Util\Type.h>
#include<Source\Component\Enemy\EnemyComponent.h>

namespace framework {
	class Effect;
}

namespace component {
	class EnemyComponent;
	class BulletRigidBody;
	class AnimatorComponent;
}

class EnemyStanState : public State<component::EnemyComponent>
{
public:
	EnemyStanState(int stanTime,const util::Vec3& dir);
	~EnemyStanState();


	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::EnemyComponent* otherValue)override;

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::EnemyComponent* otherValue, std::shared_ptr<State<component::EnemyComponent>>* nextState)override;

	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::EnemyComponent* otherValue)override;

	/// <summary>
	/// スタンタイマをリセット
	/// </summary>
	void timerReset();

	/// <summary>
	/// スタンエフェクト削除
	/// </summary>
	void deleteStanEffect();

	void addForce(const util::Vec3&dir, component::EnemyComponent * outerValue);

private:

	//!膨らみタイマー
	util::Timer m_StanTimer;

	util::Vec3 m_HitDir;

	std::weak_ptr<framework::Effect> m_pStanEffect;

	std::weak_ptr<component::BulletRigidBody> m_pRigidBody;

	std::weak_ptr<component::AnimatorComponent> m_pAnimator;

};
