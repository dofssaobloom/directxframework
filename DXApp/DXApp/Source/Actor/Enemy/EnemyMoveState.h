#pragma once
#include<Source\State\State.h>
#include<Source\Util\Type.h>
#include<Source\Component\Enemy\EnemyComponent.h>
#include"EnemyAI.h"
#include<Source\Util\Math\Randam.h>

namespace component {
	class PhysicsWorld;
}

class EnemyMoveState : public State<component::EnemyComponent>
{
public:
	EnemyMoveState();
	~EnemyMoveState();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::EnemyComponent* outerValue);

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::EnemyComponent* outerValue, std::shared_ptr<State<component::EnemyComponent>>* nextState);

	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::EnemyComponent* outerValue);

private:

	/// <summary>
	/// ステートの変更
	/// </summary>
	/// <param name="outerValue"></param>
	/// <returns></returns>
	bool isChange(component::EnemyComponent * outerValue);

	bool isHitTag(std::weak_ptr<component::PhysicsWorld>& world,const std::string & tag, const util::Vec3& vec, const util::Vec3& front);

	void rotate(util::Transform* trans);
private:
	std::weak_ptr<component::AnimatorComponent> m_Animator;
	std::shared_ptr<EnemyAI> m_enemyAI;
	component::WeakEntity m_Player;

};