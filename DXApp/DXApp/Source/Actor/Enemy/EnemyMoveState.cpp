#include "EnemyMoveState.h"
#include "EnemyIdleState.h"
#include<Source\Component\Player\SearchComponent.h>
#include<Source\Component\Physics\PhysicsWorld.h>
#include"EnemyAttackState.h"
#include "EnemyAINormalChase.h"
#include "EnemyAIForestall.h"
#include "EnemyAIEscape.h"
#include "EnemyAIWait.h"
#include "EnemyAIRandomMove.h"

UsingNamespace;

EnemyMoveState::EnemyMoveState()
{
}

EnemyMoveState::~EnemyMoveState()
{
}

void EnemyMoveState::entryAction(component::EnemyComponent * outerValue)
{
	m_Animator = outerValue->getGameObj().lock()->getComponent<AnimatorComponent>();
	m_Animator.lock()->endBlend();
	m_Animator.lock()->changeAnime("Mushroom_Run", 0);
	outerValue->getEnemyData()->pRecastTimer->init();

	m_Player = outerValue->getEnemyData()->pPlayer;
	
	int enemyAITypeNum = 5;	//エネミーAIの種類数

	util::Randam randam;
	switch ((randam.next(99) % enemyAITypeNum))
	{
	case 0:
		m_enemyAI = std::make_shared<EnemyAIRandomMove>();
		break;
	case 1:
		m_enemyAI = std::make_shared<EnemyAIEscape>();
		break;
	case 2:
		m_enemyAI = std::make_shared<EnemyAIForestall>();
		break;
	case 3:
		m_enemyAI = std::make_shared<EnemyAINormalChase>();
		break;
	case 4:
		m_enemyAI = std::make_shared<EnemyAIWait>();
		break;
	default:
		m_enemyAI = std::make_shared<EnemyAIRandomMove>();
		break;
	}
}

Trans EnemyMoveState::inputAction(component::EnemyComponent * outerValue, std::shared_ptr<State<component::EnemyComponent>>* nextState)
{

	//if (isHitTag(outerValue->getEnemyData()->pWorld,"Player", trans->m_Position, -trans->front())) {
	//			*nextState = std::make_shared<EnemyIdleState>();
	//			return Trans::Trans_Occured;
	//}
	outerValue->getEnemyData()->pRecastTimer->update();


	auto trans = outerValue->getGameObj().lock()->getTransform();

	auto&& beginPos = trans->m_Position + util::Vec3(0, 50, 0);
	auto&& end = beginPos + -trans->front() * 700;
	auto rayTest = outerValue->getEnemyData()->pWorld.lock()->rayCast(beginPos, end);
	if (rayTest.hasHit())
	{
		auto&& tag = rayTest.getHitEntity()->getTag();

		if (tag == "Stage") {
			m_enemyAI->move(outerValue);
		}
		else if (tag == "Enemy") {
			auto&& p = util::convertUtVector3(rayTest.m_hitPointWorld) - beginPos;
			auto length = p.length();
			if (length <= 50) {
				auto vec = trans->m_Position - m_Player.lock()->getTransform()->m_Position;
				vec.y = 0;
				trans->lookAt(vec);
			}
			else {
				m_enemyAI->move(outerValue);
			}
		}
		else if (tag == "Player" && outerValue->getEnemyData()->pRecastTimer->isEnd()) {
			auto&& p = util::convertUtVector3(rayTest.m_hitPointWorld) - beginPos;
			auto length = p.length();

			//緑きのこの攻撃範囲か
			bool inGreenEnemyErea = 
				length <= 600 
				&& outerValue->getEnemyData()->enemyType == EnemyType::Green;
			//その他きのこの攻撃範囲か
			bool inAttackErea     = length <= 180;

			if (inGreenEnemyErea || inAttackErea) {
				*nextState = std::make_shared<EnemyAttackState>(length);
				return Trans::Trans_Occured;
			}
			else {
				m_enemyAI->move(outerValue);
			}
		}
	}
	else {
		m_enemyAI->move(outerValue);
	}


	*nextState = std::make_shared<EnemyMoveState>(*this);
	return Trans::Trans_Again;
}

void EnemyMoveState::exitAction(component::EnemyComponent * outerValue)
{
	//	m_Animator.reset();
}

bool EnemyMoveState::isChange(component::EnemyComponent * outerValue)
{
	//円の直径
	float totalRadisu = (outerValue->getEnemyData()->searchRange * 100) * 2;

	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	//プレイヤーの位置
	auto playerTrans = m_Player.lock()->getTransform();

	//距離計算
	auto vec = myTrans->m_Position - playerTrans->m_Position;

	vec.y = 0;

	//距離が円の直径以下なら
	return vec.length() < totalRadisu;
}

bool EnemyMoveState::isHitTag(std::weak_ptr<component::PhysicsWorld>& world, const std::string & tag, const util::Vec3& vec, const util::Vec3& front)
{
	auto rayTest = world.lock()->rayCast(vec, front * 100);

	if (rayTest.hasHit())
	{
		auto&& t = rayTest.getHitEntity()->getTag();

		if (t == tag) {
			return true;
		}
		else if (t == "Stage") {
			return isHitTag(world, tag, util::convertUtVector3(rayTest.m_hitPointWorld), front);
		}
	}

	return false;
}

void EnemyMoveState::rotate(util::Transform * trans)
{
	auto vec = trans->m_Position - m_Player.lock()->getTransform()->m_Position;
	vec.y = 0;
	vec = vec.normalize();

	auto front = trans->front();

	front += vec * 0.14;
	front = front.normalize();

	trans->lookAt(front);
}

