#pragma once

#include "EnemyAI.h"

//基本ランダムで移動して
//プレイヤーが近づくと様子見
//より近づくと逃げるAI
class EnemyAIEscape : public EnemyAI
{
public:
	EnemyAIEscape();
	~EnemyAIEscape();

	// EnemyAI を介して継承されました
	virtual void move(component::EnemyComponent * outerValue) override;

private :
	const float m_escapeTime = 30 * 5;
	float m_escapeCount;
	const float m_escapeDistance = 300.0f;
	const float m_lookDistance = 500.0f;

};