#pragma once

#include "EnemyAI.h"

//一定の距離を保とうとするAI
class EnemyAIWait : public EnemyAI
{
public:
	EnemyAIWait();
	~EnemyAIWait();

	// EnemyAI を介して継承されました
	virtual void move(component::EnemyComponent * outerValue) override;

private:
	const float m_waitDistance = 600.0f;
};