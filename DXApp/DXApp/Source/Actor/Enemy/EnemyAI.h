#pragma once
#include<Source\Component\Enemy\EnemyComponent.h>

class EnemyAI
{
public:

	virtual void move(component::EnemyComponent * outerValue) = 0;
	virtual ~EnemyAI() {};

protected:
	void chase(component::EnemyComponent * outerValue);
	void forestall(component::EnemyComponent * outerValue);
	void look(component::EnemyComponent * outerValue);
	void escape(component::EnemyComponent * outerValue);
	void randomMove(component::EnemyComponent * outerValue);

protected:

	//移動スピード
	float m_Speed = 2.5;

private:

	util::Vec3 m_oldPlayerPos;

	const float m_randomMoveTime = 30 * 5;
	float m_randomMoveCount = 0;
	util::Vec3 m_randomMoveVec;
};