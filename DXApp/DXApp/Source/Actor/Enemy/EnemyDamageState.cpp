#include "EnemyDamageState.h"
#include "EnemyIdleState.h"
#include<Source\Component\Player\SearchComponent.h>
#include<Source\Component\Physics\PhysicsWorld.h>
#include"EnemyAttackState.h"
#include"EnemySwellState.h"

UsingNamespace;

EnemyDamageState::EnemyDamageState(short swellTime,bool isHit,const util::Vec3& dir)
	:m_SwellTime(swellTime),
	m_IsHit(isHit),
	m_Dir(dir)
{
}

EnemyDamageState::~EnemyDamageState()
{
}

void EnemyDamageState::entryAction(component::EnemyComponent * outerValue)
{
	outerValue->getEnemyData()->isDead = true;

	m_Animator = outerValue->getGameObj().lock()->getComponent<AnimatorComponent>();

	if (m_Animator.lock()->isBlend()) {
		m_Animator.lock()->changeAnime(m_Animator.lock()->getBlendMotionName(), m_Animator.lock()->getBlendFrame());
		m_Animator.lock()->beginBlend("Mushroom_Damage", MotionFrame::begin, 6);
	}
	else {
		m_Animator.lock()->changeAnime("Mushroom_Damage", 0);
	}


	//m_Animator.lock()->endBlend();
	//m_Animator.lock()->beginBlend("Mushroom_Damage", MotionFrame::begin, 6);
	//m_Animator.lock()->endBlend();
	//m_Animator.lock()->changeAnime("Mushroom_Damage", 0);

	m_Player = outerValue->getEnemyData()->pPlayer;
}

Trans EnemyDamageState::inputAction(component::EnemyComponent * outerValue, std::shared_ptr<State<component::EnemyComponent>>* nextState)
{
	if (m_Animator.lock()->isBlend()) {
		if (m_Animator.lock()->isBlendAnimeEnd()) {
			*nextState = std::make_shared<EnemySwellState>(m_SwellTime, m_IsHit, m_Dir);
			return Trans::Trans_Occured;
		}
	}
	else {
		if (m_Animator.lock()->isEnd()) {
			*nextState = std::make_shared<EnemySwellState>(m_SwellTime, m_IsHit, m_Dir);
			return Trans::Trans_Occured;
		}
	}

	*nextState = std::make_shared<EnemyDamageState>(*this);
	return Trans::Trans_Again;
}

void EnemyDamageState::exitAction(component::EnemyComponent * outerValue)
{
}
