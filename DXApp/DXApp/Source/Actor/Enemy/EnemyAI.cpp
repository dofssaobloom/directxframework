#include "EnemyAI.h"
#include <math.h>
#include<Source\Util\Math\Randam.h>

void EnemyAI::chase(component::EnemyComponent * outerValue)
{
	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	look(outerValue);

	//移動
	myTrans->m_Position += -myTrans->front() * m_Speed;
}

void EnemyAI::forestall(component::EnemyComponent * outerValue)
{
	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	auto playerTrans = outerValue->getEnemyData()->pPlayer.lock()->getTransform();

	auto playerMoveVec = (playerTrans->m_Position - m_oldPlayerPos);
	//プレイヤーと敵の相対速度
	auto Vr = playerMoveVec - myTrans->front() * m_Speed;
	//相対座標
	auto Sr = playerTrans->m_Position - myTrans->m_Position;
	//接近までの時間
	float Tc = Sr.length() / Vr.length();

	//Playerの移動予測位置
	auto otherVec = (playerTrans->m_Position + playerMoveVec*Tc) - myTrans->m_Position;

	auto vecLength = otherVec.length();

	otherVec = -otherVec.normalize();

	otherVec.y = 0;

	//向かせる方向
	myTrans->lookAt(otherVec);

	//５フレーム分の移動範囲
	if (m_Speed*5 <= vecLength)
		//移動
		myTrans->m_Position += -otherVec * m_Speed;

	m_oldPlayerPos = playerTrans->m_Position;
}

void EnemyAI::look(component::EnemyComponent * outerValue)
{
	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	auto player = outerValue->getEnemyData()->pPlayer;;

	//Playerの位置
	auto otherVec = player.lock()->getTransform()->m_Position - myTrans->m_Position;

	otherVec = -otherVec.normalize();

	otherVec.y = 0;

	//向かせる方向
	myTrans->lookAt(otherVec);
}

void EnemyAI::escape(component::EnemyComponent * outerValue)
{
	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	auto player = outerValue->getEnemyData()->pPlayer;;

	//Playerの位置
	auto otherVec = myTrans->m_Position - player.lock()->getTransform()->m_Position;

	otherVec = -otherVec.normalize();

	otherVec.y = 0;

	//向かせる方向
	myTrans->lookAt(otherVec);

	//移動
	myTrans->m_Position += -otherVec * m_Speed * 3;
}

void EnemyAI::randomMove(component::EnemyComponent * outerValue)
{
	//Enemyの位置
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	if (m_randomMoveCount <= 0)
	{
		//-1500~1500の範囲でランダム座標指定
		const int N = 3000;
		std::random_device rnd;	//非決定的な乱数生成器を生成
		std::mt19937 mt(rnd());	//メルセンヌ・ツイスタの３２ビット版、引数は初期シード値
		std::uniform_int_distribution<> randN(0, N - 1);
		
		float randPosX = randN(mt);
		randPosX = randPosX - N / 2;

		float randPosY = randN(mt);
		randPosY = randPosY - N / 2;

		util::Vec3 randomPos = util::Vec3(randPosX, 0, randPosY);
		m_randomMoveVec = randomPos - myTrans->m_Position;
		m_randomMoveVec.y = 0;
		m_randomMoveVec = -m_randomMoveVec.normalize();

		//移動時間設定
		std::uniform_int_distribution<> randMoveTime(m_randomMoveTime, m_randomMoveTime * 2);
		m_randomMoveCount = randMoveTime(mt);
	}

	//向かせる方向
	myTrans->lookAt(m_randomMoveVec);

	//移動
	myTrans->m_Position += -m_randomMoveVec * m_Speed;

	m_randomMoveCount -= 1;
}
