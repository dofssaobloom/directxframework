#pragma once

enum class EnemyType
{
	Red,
	Black,
	Green
};