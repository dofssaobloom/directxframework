#pragma once

#include "EnemyAI.h"

//プレイヤーの移動方向より先回りするAI
//近づくと追跡
class EnemyAIForestall : public EnemyAI
{
public:
	EnemyAIForestall();
	~EnemyAIForestall();

	// EnemyAI を介して継承されました
	virtual void move(component::EnemyComponent * outerValue) override;

private:
	const float m_chaseDistance = 400.0f;
};