#include "EnemyAIWait.h"

EnemyAIWait::EnemyAIWait()
{
}

EnemyAIWait::~EnemyAIWait()
{
}

void EnemyAIWait::move(component::EnemyComponent * outerValue)
{
	auto myTrans = outerValue->getGameObj().lock()->getTransform();

	auto playerTrans = outerValue->getEnemyData()->pPlayer.lock()->getTransform();

	float toPlayerDistance = (playerTrans->m_Position - myTrans->m_Position).length();

	//�T�t���[�����̈ړ��ʂ�P�\��
	if (toPlayerDistance < m_waitDistance - m_Speed*5)
		escape(outerValue);
	else if (m_waitDistance + m_Speed*5 < toPlayerDistance)
		chase(outerValue);
	else
		look(outerValue);
}
