#include "EnemyStanState.h"
#include"EnemyIdleState.h"
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>
#include<Framework.h>
#include<Source\Component\Physics\BulletRigidBody.h>
#include<Source\Component\Player\AttackComponent.h>

UsingNamespace;

EnemyStanState::EnemyStanState(int stanTime, const util::Vec3& dir)
	:m_StanTimer(stanTime),
	m_HitDir(dir)
{
}

EnemyStanState::~EnemyStanState()
{
}

void EnemyStanState::entryAction(component::EnemyComponent * outerValue)
{
	m_pAnimator = outerValue->getGameObj().lock()->getComponent<AnimatorComponent>();

	if (m_pAnimator.lock()->isBlend()) {
		m_pAnimator.lock()->changeAnime(m_pAnimator.lock()->getBlendMotionName(), m_pAnimator.lock()->getBlendFrame());
		m_pAnimator.lock()->beginBlend("Mushroom_Damage", MotionFrame::begin, 6);
	}
	else {
		m_pAnimator.lock()->changeAnime("Mushroom_Damage", 0);
	}

	m_pStanEffect = EffectManager::getInstance()->generate("StanEffect", util::Transform(util::Vec3(),
		util::Vec3(0, 0, 0),
		util::Vec3(30, 30, 30)));


	m_pRigidBody = outerValue->getGameObj().lock()->getComponent<BulletRigidBody>();
	//m_pRigidBody.lock()->setPositionFreezeFlag({ false,false,false });
	//m_pRigidBody.lock()->setRotateFreezeFlag({ false,false,false });

	m_pRigidBody.lock()->addForce(m_HitDir * outerValue->getPlayerAttack().lock()->attackPower());

	m_StanTimer.init();
}

Trans EnemyStanState::inputAction(component::EnemyComponent * otherValue, std::shared_ptr<State<component::EnemyComponent>>* nextState)
{
	if (m_pAnimator.lock()->isBlend()) {
		if (m_pAnimator.lock()->getBlendMotionName() == "Mushroom_Damage") {
			if (m_pAnimator.lock()->isBlendTimeEnd()) {
				m_pAnimator.lock()->changeAnime(m_pAnimator.lock()->getBlendMotionName(), m_pAnimator.lock()->getBlendFrame());
				m_pAnimator.lock()->endBlend();
				m_pAnimator.lock()->beginBlend("Mushroom_Idle", MotionFrame::begin, 6);
			}
		}
		else {
			if (m_pAnimator.lock()->isBlendTimeEnd()) {
				m_pAnimator.lock()->changeAnime(m_pAnimator.lock()->getBlendMotionName(), m_pAnimator.lock()->getBlendFrame());
			}
		}
	}
	else {
		//TODO スタンモーションに差し替える
		m_pAnimator.lock()->beginBlend("Mushroom_Idle", MotionFrame::begin, 6);
	}

	if (!m_pStanEffect.expired()) {
		m_pStanEffect.lock()->m_Trans.m_Position = otherValue->getGameObj().lock()->getTransform()->m_Position;
		m_pStanEffect.lock()->m_Trans.m_Position.y += 100;
	}

	m_StanTimer.update();

	if (m_StanTimer.isEnd()) {
		*nextState = std::make_shared<EnemyIdleState>();
		return Trans::Trans_Occured;
	}


	*nextState = std::make_shared<EnemyStanState>(*this);
	return Trans::Trans_Again;
}

void EnemyStanState::exitAction(component::EnemyComponent * outerValue)
{
	//m_pRigidBody.lock()->setPositionFreezeFlag({ true,false,true });
	//m_pRigidBody.lock()->setRotateFreezeFlag({ true,true,true });
	deleteStanEffect();
}

void EnemyStanState::timerReset()
{
	m_StanTimer.init();
}

void EnemyStanState::deleteStanEffect()
{
	if (!m_pStanEffect.expired()) {
		auto handle = m_pStanEffect.lock()->getHandle();
		EffectManager::getInstance()->effectStop(handle);
	}
}

void EnemyStanState::addForce(const util::Vec3 & dir, component::EnemyComponent * outerValue)
{
	m_pRigidBody.lock()->addForce(dir * outerValue->getPlayerAttack().lock()->attackPower());
}
