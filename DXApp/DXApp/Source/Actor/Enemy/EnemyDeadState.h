#pragma once
#include<Source\State\State.h>
#include<Source\Util\Type.h>
#include<Source\Component\Enemy\EnemyComponent.h>
#include<Framework.h>

namespace framework {
	class Effect;
}

class EnemyDeadState : public State<component::EnemyComponent>
{
public:
	EnemyDeadState();

	~EnemyDeadState();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::EnemyComponent* outerValue)override;

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::EnemyComponent* outerValue, std::shared_ptr<State<component::EnemyComponent>>* nextState)override;

	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::EnemyComponent* outerValue)override;

private:
	std::shared_ptr<util::Timer> m_pDeadTimer;
	std::weak_ptr<framework::Effect> m_pExplosionEffect;
};
