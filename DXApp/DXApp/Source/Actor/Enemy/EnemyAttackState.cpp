#include "EnemyAttackState.h"
#include "EnemyMoveState.h"
#include "EnemyIdleState.h"
#include<Source\Component\Player\SearchComponent.h>
#include<Source\Component\Physics\PhysicsWorld.h>
#include<Source\Component\Player\AttackComponent.h>
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>
#include<Source\Actor\Enemy\EnemyBullet.h>
#include<Source\Actor\Enemy\EEnemyType.h>

UsingNamespace;

EnemyAttackState::EnemyAttackState(float length)
	:m_ResposTime(30),
	m_Lenth(length)
{
}

EnemyAttackState::~EnemyAttackState()
{
}

void EnemyAttackState::entryAction(component::EnemyComponent * outerValue)
{
	m_Animator = outerValue->getGameObj().lock()->getComponent<AnimatorComponent>();
	//m_Animator.lock()->changeAnime("Mushroom_Attack",0);
	m_pAttackComponent = outerValue->getGameObj().lock()->getComponent<AttackComponent>();
	m_pAttackComponent.lock()->setFrontFlag(false);
	m_pAttackComponent.lock()->setTime(3);
	m_Player = outerValue->getEnemyData()->pPlayer;

	if (outerValue->getEnemyData()->enemyType == EnemyType::Green) {
		auto  copy = *outerValue->getGameObj().lock()->getTransform();
		copy.scaling(20);
		copy.m_Position.y += 180;
		//エフェクトが出ていなければ
		if (m_pChageEffect.expired())
			m_pChageEffect = framework::EffectManager::getInstance()->generate("EnemyBallCharge", copy);
	}

	beginPos = outerValue->getGameObj().lock()->getTransform()->m_Position;
	m_IsResposEnd = false;
	m_IsEnd = false;
	m_ResposTime.init();
}

Trans EnemyAttackState::inputAction(component::EnemyComponent * outerValue, std::shared_ptr<State<component::EnemyComponent>>* nextState)
{
	auto trans = outerValue->getGameObj().lock()->getTransform();


	if (m_ResposTime.isEnd() && !m_IsResposEnd) {
		m_IsResposEnd = true;
		outerValue->getEnemyData()->pAttack.lock()->onCollision();
		if (outerValue->getEnemyData()->enemyType == EnemyType::Green) {
			if (!m_pChageEffect.expired())
			{
				auto handle = m_pChageEffect.lock()->getHandle();
				framework::EffectManager::getInstance()->effectStop(handle);
			}
			shoot(trans);
		}
		m_Animator.lock()->beginBlend("Mushroom_Attack", MotionFrame::begin, 5);
	}

	beginPos = outerValue->getGameObj().lock()->getTransform()->m_Position;
	if (m_IsResposEnd) {
		//射撃するタイプは前に出ない
		if (outerValue->getEnemyData()->enemyType != EnemyType::Green)
			outerValue->getGameObj().lock()->getTransform()->m_Position = util::Vec3::lerp(m_pAttackComponent.lock()->moveRate(), beginPos, m_FrontMove);
	}
	else {
		m_ResposTime.update();
	}

	//攻撃開始タイマーが半分までプレイヤーを方向で追跡する
	if (!m_IsResposEnd && m_ResposTime.rate() >= 0.4f) {
		auto trans = outerValue->getGameObj().lock()->getTransform();
		rotate(trans);
	}


	if (m_Animator.lock()->isBlend() && !m_IsEnd) {
		if (m_Animator.lock()->isBlendAnimeEnd()) {
			m_Animator.lock()->changeAnime(m_Animator.lock()->getBlendMotionName(), m_Animator.lock()->getBlendFrame());
			m_Animator.lock()->endBlend();
			m_Animator.lock()->beginBlend("Mushroom_Run", MotionFrame::begin, 16);
			m_IsEnd = true;
		}
	}

	if (m_Animator.lock()->isBlend()) {
		if (m_Animator.lock()->getBlendMotionName() == "Mushroom_Run" && m_Animator.lock()->isBlendTimeEnd()) {
			*nextState = std::make_shared<EnemyMoveState>();
			return Trans::Trans_Occured;
		}
	}


	*nextState = std::make_shared<EnemyAttackState>(*this);
	return Trans::Trans_Again;
}

void EnemyAttackState::exitAction(component::EnemyComponent * outerValue)
{
	deleteEffect();
}

void EnemyAttackState::deleteEffect()
{
	if (!m_pChageEffect.expired()) {
		auto handle = m_pChageEffect.lock()->getHandle();
		framework::EffectManager::getInstance()->effectStop(handle);
	}

}

void EnemyAttackState::rotate(util::Transform* trans)
{
	auto vec = trans->m_Position - m_Player.lock()->getTransform()->m_Position;
	vec.y = 0;
	vec = vec.normalize();

	auto front = trans->front();

	front += vec * 0.14;
	front = front.normalize();

	trans->lookAt(front);

	//m_FrontMove = beginPos;
	m_FrontMove = beginPos + -front * m_Lenth * 0.5;
}

void EnemyAttackState::shoot(util::Transform* trans)
{
	//TODO モデルのサイズがあってないので新しく生成する　また調整する必要がある
	util::Transform bulletTrans = *trans;
	bulletTrans.m_Scale = util::Vec3(50, 50, 50);
	auto bullet = Entity::createEntity("EnemyBullet", "Attack", bulletTrans);
	bullet.lock()->getTransform()->m_Position.y += 30;
	bullet.lock()->getTransform()->m_Rotation.y += 180;

	std::vector<std::weak_ptr<Component>> damy;
	float length = (trans->m_Position - m_Player.lock()->getTransform()->m_Position).length();
	float speed = 0.8f;
	float angle = 75.0f;
	float vec = abs(
		sqrt(9.8 * length 
			/ 
		(2 * sin(XMConvertToRadians(angle)) * cos(XMConvertToRadians(angle)))
		));
	auto bulletComponent = bullet.lock()->addComponent<EnemyBullet>(&damy, { std::to_string(vec), std::to_string(speed), std::to_string(angle), std::to_string(1) });

	auto rigidBody = bullet.lock()->addComponent <BulletRigidBody >(&damy, { "Resource/Script/Lua/UI/RigidBody.lua" ,"Resource/ByteData/UIPrefab/BulletRigidBody.pfb" });
	auto collider = bullet.lock()->addComponent<BulletSphereCollider>(&damy, { "Resource/Script/Lua/UI/SphereCollider.lua","Resource/ByteData/UIPrefab/BulletCollider.pfb" });
	bulletComponent.lock()->init();

	//順不同
	collider.lock()->init();
	rigidBody.lock()->init();
}
