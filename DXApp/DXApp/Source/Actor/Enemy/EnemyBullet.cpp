#include "EnemyBullet.h"
#include<Source\Component\UI\GUIComponent.h>
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>
#include<Source\Resource\Effect\Effect.h>
#include<Source\Component\Bullet\BulletCollision.h>

namespace component {

	EnemyBullet::EnemyBullet()
		:m_Timer(2)
	{
	}

	EnemyBullet::~EnemyBullet()
	{
		if (m_pPoisonEffectHandle.expired())return;
		auto handle = m_pPoisonEffectHandle.lock()->getHandle();
		framework::EffectManager::getInstance()->effectStop(handle);
	}

	void EnemyBullet::setParam(const std::vector<std::string>& param)
	{
		m_Vec = std::atof(param[0].c_str());
		m_Speed = std::atof(param[1].c_str());
		m_Angle = std::atof(param[2].c_str());
		m_Power = std::atoi(param[3].c_str());
	}


	util::Vec3 EnemyBullet::bulletTrajectory(float time, float vec, float angle, util::Mat4& rotate)
	{
		util::Vec3 pos;

		pos.z = -vec * cos(XMConvertToRadians(angle)) * time;
		pos.y = vec * sin(XMConvertToRadians(angle)) * time - 9.8 * pow(time, 2.0) / 2.0;

		pos.x -= 50;
		pos.y += 60;

		//回転後のポジション
		util::Vec3 rotaePos = XMVector3Transform(pos.toXMVector(), rotate.toXMMatrix());

		rotaePos.x = -rotaePos.x;

		return rotaePos;
	}

	void EnemyBullet::onCollisionEnter(const framework::HitData& other)
	{
		if (!m_Timer.isEnd())return;
		if (other.other.lock()->getTag() == "Enemy")return;
		if (other.other.lock()->getTag() == "Bullet")return;

		destory(m_Entity);
	}

	const short EnemyBullet::getPower() const
	{
		return m_Power;
	}


	void EnemyBullet::init()
	{
		m_Time = 0;
		auto trans = m_Entity.lock()->getTransform();
    	m_InitPos = trans->m_Position;
		auto copy = *trans;

		auto collision = framework::Entity::createEntity("EnemyBulletCollision", "Attack", copy);
		collision.lock()->setParent(m_Entity);

		copy.scaling(30);
		m_pPoisonEffectHandle = framework::EffectManager::getInstance()->generate("EnemyBall", copy);


		std::vector<std::weak_ptr<Component>> damy;
		auto rigidBody = collision.lock()->addComponent < BulletRigidBody >(&damy, { "Resource/Script/Lua/UI/RigidBody.lua" ,"Resource/ByteData/UIPrefab/BulletRigidBody.pfb" });
		auto collider = collision.lock()->addComponent<BulletSphereCollider>(&damy, { "Resource/Script/Lua/UI/SphereCollider.lua","Resource/ByteData/UIPrefab/BulletCollider2.pfb" });
		collision.lock()->addComponent<BulletCollision>(&damy);

		collider.lock()->init();
		rigidBody.lock()->init();
	}

	void EnemyBullet::update()
	{
		m_Time += m_Speed;
		auto trans = m_Entity.lock()->getTransform();
		auto newPos = bulletTrajectory(m_Time, m_Vec, m_Angle, trans->toRotateMatrix()) + m_InitPos;

		trans->m_Position = newPos;
		if (!m_pPoisonEffectHandle.expired())
			m_pPoisonEffectHandle.lock()->m_Trans.m_Position = trans->m_Position;

		//とりあえず適当にしたにいったら削除
		if (trans->m_Position.y <= -2000) {
			destory(m_Entity);
		}

		m_Timer.update();
	}

}
