#pragma once

#include<Framework.h>

namespace  framework {
	class Effect;
}

namespace component {

	/// <summary>
	/// 接続ゲームオブジェクトに斜方投射の挙動を付与するクラス
	/// </summary>
	class EnemyBullet : public framework::UpdateComponent
	{
	public:
		EnemyBullet();
		~EnemyBullet();


		/**
		* @brief		初期化
		*/
		virtual void init()override;

		/**
		* @brief		更新
		*/
		virtual void update()override;

		virtual void setParam(const std::vector<std::string>& param);


		/// <summary>
		/// 斜方投射座標計算
		/// </summary>
		static util::Vec3 bulletTrajectory(float time, float vec, float angle, util::Mat4& rotate);

		virtual void onCollisionEnter(const framework::HitData& other)override;

		const short getPower()const;

	private:
		//!エフェクトハンドル
		std::weak_ptr<framework::Effect> m_pPoisonEffectHandle;
		util::Timer m_Timer;

		util::Vec3 m_InitPos;

		short m_Power;

		float m_Time;
		float m_Vec;
		float m_Angle;
		float m_Speed;
	};
}