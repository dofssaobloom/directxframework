#include "PlayerDeadState.h"
#include<Framework.h>
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Actor\PlayerIdleState.h>
#include<Source\Actor\PlayerMoveState.h>
#include<Source\Component\Scene\GameMain\RulesComponent.h>

PlayerDeadState::PlayerDeadState()
{
}

PlayerDeadState::~PlayerDeadState()
{
}

void PlayerDeadState::entryAction(component::PlayerComponent * outerValue)
{
	//攻撃モーションに変更
	auto&& currentState = outerValue->getPlayerData().lock()->currentState = PlayerState::die;

	m_Animator = outerValue->getGameObj().lock()->getComponent<component::AnimatorComponent>();
	assert(!m_Animator.expired() && "プレイヤーにアニメーターがついていません");
	//移動モーションに切り替える

	//m_Animator.lock()->changeAnime(outerValue->getPlayerData().lock()->motionNames[currentState]);
}

Trans PlayerDeadState::inputAction(component::PlayerComponent * outerValue, std::shared_ptr<State<component::PlayerComponent>>* nextState)
{
	//ダメージを受けてたら即死なので変わることはない
	*nextState = std::make_shared<PlayerDeadState>(*this);
	return Trans::Trans_Again;
}

void PlayerDeadState::exitAction(component::PlayerComponent * outerValue)
{
}
