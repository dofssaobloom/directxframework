#pragma once
#include<Source\State\State.h>
#include<Source\Component\Player\PlayerComponent.h>
#include<Source\Component\Animation\AnimatorComponent.h>


//ダメージを誰にされたか
enum class HitEntity {
	Enemy,
	PoisonCollision,
};



class PlayerDamageState : public State<component::PlayerComponent>
{
public:
	PlayerDamageState(HitEntity hitOther);
	~PlayerDamageState();

	/**
	* @brief					切り替わって初めに呼ばれるコールバックメソッド
	*/
	virtual void entryAction(component::PlayerComponent* outerValue);

	/**
	* @brief					マイフレームアップデートで呼ばれるコールバックメソッド
	*/
	virtual Trans inputAction(component::PlayerComponent* outerValue, std::shared_ptr<State<component::PlayerComponent>>* nextState);

	/**
	* @brief					次のステートに切り替わる直前に呼ばれるコールバックメソッド
	*/
	virtual void exitAction(component::PlayerComponent* outerValue);

private:

	/// <summary>
	/// 敵に攻撃されたとき
	/// </summary>
	/// <param name="outerValue"></param>
	bool hitEnemyUpdate(component::PlayerComponent* outerValue);

	/// <summary>
	/// 爆発コリジョンにあたったとき
	/// </summary>
	/// <param name="outerValue"></param>
	bool hitPoisonCollisionUpdate(component::PlayerComponent* outerValue);

private:
	std::weak_ptr<component::AnimatorComponent> m_Animator;

	std::shared_ptr<State<component::PlayerComponent>> m_pNextState;

	//!誰にあたったか
	HitEntity m_HitOther;

	//!弱モーションの名前か強モーションの名前
	std::string m_FirstMotionName;
	bool m_IsShootingIdleBlendEnd;

	bool m_IsEnd;

	bool m_IsDead;
};

