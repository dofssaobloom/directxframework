#include"PlayerIdleState.h"
#include<Source\Device\Input\Input.h>
#include"PlayerMoveState.h"
#include"PlayerSetUpState.h"
#include"PlayerJumpState.h"
#include<assert.h>
#include<Source\Component\Animation\AnimatorComponent.h>
#include<Source\Component\Player\PlayerComponent.h>
#include<Source\Entity\Entity.h>
#include<Source\Actor\PlayerAttackState.h>
#include<Source\Actor\PlayerStepState.h>
#include<Source\Actor\PlayerSliderState.h>
#include<Source\Application\Device\Input.h>
#include<Source\Component\Physics\BulletRigidBody.h>
#include<Source\Component\Scene\GameMain\GameTimerCompornent.h>

UsingNamespace;

PlayerIdleState::PlayerIdleState()
	:m_JumpTimer(3)
{
}

PlayerIdleState::~PlayerIdleState()
{
}

void PlayerIdleState::entryAction(component::PlayerComponent * outerValue)
{
	//アイドルステータスに変更
	auto&& currentState = outerValue->getPlayerData().lock()->currentState = PlayerState::idle;

	m_pAnimator = outerValue->getGameObj().lock()->getComponent<component::AnimatorComponent>();
	assert(!m_pAnimator.expired() && "プレイヤーにアニメーターがついていません");
	auto motionName = outerValue->getPlayerData().lock()->motionNames[currentState];

	//移動モーションに切り替える
	m_pAnimator.lock()->changeAnime(motionName, m_pAnimator.lock()->getBlendFrame());
	m_pAnimator.lock()->endBlend();


	m_pRigidBody = outerValue->getGameObj().lock()->getComponent<component::BulletRigidBody>();
	m_pRigidBody.lock()->setPositionFreezeFlag({ true,false,true });
	m_pRigidBody.lock()->setRotateFreezeFlag({ true,true,true });
	m_IsEnd = false;

	m_IsJump = false;
	m_JumpTimer.init();
}

Trans PlayerIdleState::inputAction(component::PlayerComponent * outerValue, std::shared_ptr<State<component::PlayerComponent>>* nextState)
{
	auto vec = application::Input::leftVelocity();

	if (m_IsEnd) {
		if (m_pAnimator.lock()->isBlendTimeEnd()) {
			*nextState = m_NextState;
			return Trans::Trans_Occured;
		}
	}

	//ゲームが終了していたらもうほかには遷移しない
	if (outerValue->getPlayerData().lock()->pGameTimer.lock()->isEnd()) {
		*nextState = std::make_shared<PlayerIdleState>(*this);
		return Trans::Trans_Again;
	}


	//左スティックの入力があればmoveStateへ遷移する
	if (!vec.isZero())
	{
		if (!m_IsEnd) {
			//次のアニメーションとブレンド
			m_pAnimator.lock()->beginBlend(outerValue->getPlayerData().lock()->motionNames[PlayerState::move], component::MotionFrame::begin, 6);
			m_NextState = std::make_shared<PlayerMoveState>();
			m_IsEnd = true;
		}
		
	}

	if ( application::Input::isSetUp()) {
		if (!m_IsEnd) {
			//次のアニメーションとブレンド
			m_pAnimator.lock()->beginBlend(outerValue->getPlayerData().lock()->motionNames[PlayerState::setup], component::MotionFrame::begin, 6);
			m_NextState = std::make_shared<PlayerSetUpState>();
			m_IsEnd = true;
		}
	}

	if (application::Input::isAttack()) {
		if (!m_IsEnd) {
			//次のアニメーションとブレンド
			m_pAnimator.lock()->beginBlend(outerValue->getPlayerData().lock()->motionNames[PlayerState::attack], component::MotionFrame::begin, 6);
			m_NextState = std::make_shared<PlayerAttackState>();
			m_IsEnd = true;
		}
	}
	
	//ジャンプステートへ移行
	auto trans = outerValue->getGameObj().lock()->getTransform();


	//if (application::Input::isJump()) {
	//	m_JumpTimer.update();
	//	m_IsJump = true;
	//}

	if (outerValue->getPlayerData().lock()->isFloorContact && application::Input::isJump()) {
		if (!m_IsEnd) {
			//次のアニメーションとブレンド
			m_pAnimator.lock()->beginBlend(outerValue->getPlayerData().lock()->motionNames[PlayerState::jump], component::MotionFrame::begin, 3);
			m_NextState = std::make_shared<PlayerJumpState>(0);
			m_IsEnd = true;
		}
	}

	*nextState = std::make_shared<PlayerIdleState>(*this);
	return Trans::Trans_Again;

}

void PlayerIdleState::exitAction(component::PlayerComponent * outerValue)
{
}
