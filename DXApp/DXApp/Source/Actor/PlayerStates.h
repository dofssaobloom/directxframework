#pragma once
#include<Source\State\State.h>

enum class PlayerState
{
	idle,	//止まっている状態
	move,	//動いている状態
	setup,	//構え状態
	jump,	//ジャンプ状態
	attack,	//攻撃している状態
	damage,	//ダメージ状態
	die,

};

struct PlayerDesc
{
	int hp;

};