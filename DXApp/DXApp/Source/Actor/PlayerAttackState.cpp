#include"PlayerAttackState.h"
#include<Source\Entity\Entity.h>
#include<assert.h>
#include<Source\Actor\PlayerIdleState.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Actor\PlayerStepState.h>
#include<Source\Device\Input\Input.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Actor\PlayerMoveState.h>
#include<Framework.h>
#include<Source\Application\Device\Input.h>
UsingNamespace;

PlayerAttackState::PlayerAttackState()
	:m_EffectOffset(0, -200, 0), m_AttackSETime(1)
{
}

PlayerAttackState::~PlayerAttackState()
{
}

void PlayerAttackState::entryAction(component::PlayerComponent * outerValue)
{
	m_IsAttackSE = false;
	//攻撃モーションに変更
	auto&& currentState = outerValue->getPlayerData().lock()->currentState = PlayerState::attack;

	m_pAnimator = outerValue->getGameObj().lock()->getComponent<component::AnimatorComponent>();
	assert(!m_pAnimator.expired() && "プレイヤーにアニメーターがついていません");

	m_pAnimator.lock()->changeAnime(outerValue->getPlayerData().lock()->motionNames[currentState]);
	m_pAnimator.lock()->endBlend();//一度ブレンドを切る


	m_IsEnd = false;
	m_IsAttackEnd = false;
	m_IsChageEnd = false;
}

Trans PlayerAttackState::inputAction(component::PlayerComponent * outerValue, std::shared_ptr<State<component::PlayerComponent>>* nextState)
{
	if (m_pAnimator.lock()->getCurrentName() == "Player_AttackBegin") {
		if (m_pAnimator.lock()->isEnd()) {
			if (!m_pAnimator.lock()->isBlend()) {
				//if (m_pAnimator.lock()->getBlendMotionName() != "Player_AttackChage")
				m_pAnimator.lock()->beginBlend("Player_AttackChage", MotionFrame::begin, 3);
			}
		}
	}
	if (!application::Input::isAttackChage() && !m_IsChageEnd) {
		if (m_pAnimator.lock()->isBlend()) {
			if (m_pAnimator.lock()->getBlendMotionName() != "Player_AttackEnd") {
				if (m_pAnimator.lock()->isBlend()) {
					m_pAnimator.lock()->changeAnime(m_pAnimator.lock()->getBlendMotionName(), m_pAnimator.lock()->getBlendFrame());
					m_pAnimator.lock()->endBlend();
				}
				else {
					m_pAnimator.lock()->endBlend();
				}
				m_pAnimator.lock()->beginBlend("Player_AttackEnd", MotionFrame::begin, 3);
			}
		}
		else {
			m_pAnimator.lock()->endBlend();
			m_pAnimator.lock()->beginBlend("Player_AttackEnd", MotionFrame::begin, 3);
		}
		m_IsChageEnd = true;
	}
	else {
		if (application::Input::isAttackChage()) {
			//チャージ中は回転できる
			//myUpdate(outerValue);
		}
	}

	if (m_pAnimator.lock()->isBlend()) {
		if (m_pAnimator.lock()->isBlendTimeEnd()) {
			if (m_pAnimator.lock()->getBlendMotionName() == "Player_AttackEnd") {
				m_pAnimator.lock()->changeAnime(m_pAnimator.lock()->getBlendMotionName(), m_pAnimator.lock()->getBlendFrame());
				m_pAnimator.lock()->endBlend();
			}
			else if (m_pAnimator.lock()->getBlendMotionName() == "Player_AttackChage" && !m_IsChageEnd) {
				m_pAnimator.lock()->changeAnime(m_pAnimator.lock()->getBlendMotionName(), m_pAnimator.lock()->getBlendFrame());
				m_pAnimator.lock()->endBlend();
				m_IsChageEnd = true;
			}
		}
	}

	if (m_pAnimator.lock()->getCurrentName() == "Player_AttackChage") {
		auto&& vec = application::Input::leftVelocity();
		if (!application::Input::isAttackChage()) {
			if (!m_IsEnd && !m_IsAttackEnd) {
				//次のアニメーションとブレンド
				m_pAnimator.lock()->beginBlend("Player_AttackEnd", MotionFrame::begin, 3);
				m_IsAttackEnd = true;
			}
		}
	}

	//SEの再生
	if (m_pAnimator.lock()->getCurrentName() == "Player_AttackEnd") {
		if (!m_IsAttackSE)
			m_AttackSETime.update();
		else
			m_AttackSETime.init();

		if (m_AttackSETime.isEnd()) {
			outerValue->attack();
			m_IsAttackSE = true;
		}
	}

	if (m_pAnimator.lock()->getCurrentName() == "Player_AttackEnd" && m_pAnimator.lock()->isEnd() && !m_IsEnd) {
		auto vec = application::Input::leftVelocity();
		if (vec.isZero()) {
			m_pAnimator.lock()->beginBlend(outerValue->getPlayerData().lock()->motionNames[PlayerState::idle], MotionFrame::begin, 3);
			m_NextState = std::make_shared<PlayerIdleState>();
		}
		else {
			m_pAnimator.lock()->beginBlend(outerValue->getPlayerData().lock()->motionNames[PlayerState::move], MotionFrame::begin, 3);
			m_NextState = std::make_shared<PlayerMoveState>();
		}
		m_IsEnd = true;
	}


	if (m_IsEnd) {
		if (m_pAnimator.lock()->isBlendTimeEnd()) {
			*nextState = m_NextState;
			return Trans::Trans_Occured;
		}
	}

	*nextState = std::make_shared<PlayerAttackState>(*this);
	return Trans::Trans_Again;
}

void PlayerAttackState::exitAction(component::PlayerComponent * outerValue)
{
	m_pAnimator.reset();
}


void PlayerAttackState::myUpdate(component::PlayerComponent * outerValue)
{
	using namespace framework;
	auto trans = outerValue->getGameObj().lock()->getTransform();
	auto vel = velocity(trans, outerValue->getPlayerData().lock()->controlID);

	//回転に摩擦係数をかける
	vel = vel * outerValue->getPlayerData().lock()->bendFriction;

	//正面方向
	auto dir = trans->front();
	//dir = -dir;
	//dir.x = -dir.x;
	dir += vel;
	dir = dir.normalize();
	//dir = -dir;
	trans->lookAt(dir);

	dir = dir * outerValue->getPlayerData().lock()->speed;
	dir = dir * 0.5;

	//移動
	trans->m_Position += dir;
	trans->m_Position += dir * 10;
}

util::Vec3 PlayerAttackState::velocity(util::Transform* trans, int controlID)
{
	using namespace framework;

	//左スティックの入力
	auto leftStick = application::Input::leftVelocity();//.normalize();

														//カメラの追従
	auto vec = component::CameraComponent::getMainCamera().lock()->
		convertViewDir(util::Vec3(-leftStick.x, 0, -leftStick.y));

	//yへの移動なし
	vec.y = 0;

	return vec.normalize();
}


