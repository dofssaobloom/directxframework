// 定数バッファのデータ定義�@　変更しないデータ
cbuffer ConstBuffer : register(b0) { // 常にスロット「0」を使う
	matrix Projection;   // 透視変換行列
	matrix View;   // ビュー変換行列
	matrix World;   // 座標変換行列
};

Texture2D myTex2D;   // テクスチャ
					 // サンプラ
SamplerState smpWrap : register(s0);

struct VS_INPUT {
	float3 pos : SV_Position;
	float3 color : COLOR;
};

struct VS_OUT {
	float4 pos : SV_Position;
	float3 color : COLOR;
};


VS_OUT VS(VS_INPUT input){

	float4 pos;
	pos = mul(float4(input.pos,1), World);
	pos = mul(pos, View);
	pos = mul(pos, Projection);

	VS_OUT outPut;
	outPut.pos = pos;
	outPut.color = float4(input.color,1);
	return outPut;
}

float4 PS(VS_OUT input) : SV_TARGET
{
	return float4(input.color,1);
}