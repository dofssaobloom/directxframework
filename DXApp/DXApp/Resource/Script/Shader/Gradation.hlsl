// 定数バッファのデータ定義�@　変更しないデータ
cbuffer ConstBuffer : register(b0) { // 常にスロット「0」を使う
	matrix Projection;   // 透視変換行列
};

// 定数バッファのデータ定義�A　変更頻度の低いデータ
cbuffer UpdateBuffer : register(b1) { // 常にスロット「1」を使う
	matrix World;   // 座標変換行列
	float2 Damy;
	float Offset;
	float IsVertical;//縦ぼかしかどうか
	float4 Weight0; //ガウスウェイト
	float4 Weight1; //ガウスウェイト
};

Texture2D tex: register(t0);   // テクスチャ
					 // サンプラ
SamplerState smpWrap : register(s0);

SamplerState smpClap
{
	//Filter = MIN_MAG_MIP_LINEAR;
	//AddressU = Clamp;
	//AddressV = Clamp;
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = BORDER;
	AddressV = BORDER;
	AddressW = BORDER;
};

struct VS_INPUT {
	float3 pos : SV_Position;
	float2 uv : TEXTURE;
};

struct VS_OUT {
	float4 pos : SV_Position;
	float2 uv : TEXTURE;
	float2 tex0 : TEXCOORD0;
	float2 tex1 : TEXCOORD1;
	float2 tex2 : TEXCOORD2;
	float2 tex3 : TEXCOORD3;
	float2 tex4 : TEXCOORD4;
	float2 tex5 : TEXCOORD5;
	float2 tex6 : TEXCOORD6;
	float2 tex7 : TEXCOORD7;
};

/// <summary>
/// 縦ぼかしUV
/// </summary>
void VerticalUV(inout VS_OUT inData) {
	inData.uv = inData.uv;
	inData.pos = inData.pos;

	float width, height;
	tex.GetDimensions(width, height);

	inData.tex0 = inData.uv + float2(0.0f, -1.0f / height);
	inData.tex1 = inData.uv + float2(0.0f, -3.0f / height);
	inData.tex2 = inData.uv + float2(0.0f, -5.0f / height);
	inData.tex3 = inData.uv + float2(0.0f, -7.0f / height);
	inData.tex4 = inData.uv + float2(0.0f, -9.0f / height);
	inData.tex5 = inData.uv + float2(0.0f, -11.0f / height);
	inData.tex6 = inData.uv + float2(0.0f, -13.0f / height);
	inData.tex7 = inData.uv + float2(0.0f, -15.0f / height);
}

/// <summary>
/// 横ぼかしUV
/// </summary>
void HorizontalUV(inout VS_OUT inData) {
	inData.uv = inData.uv;
	inData.pos = inData.pos;

	float width, height;
	tex.GetDimensions(width, height);

	inData.tex0 = inData.uv + float2(-1.0f / width, 0.0f);
	inData.tex1 = inData.uv + float2(-3.0f / width, 0.0f);
	inData.tex2 = inData.uv + float2(-5.0f / width, 0.0f);
	inData.tex3 = inData.uv + float2(-7.0f / width, 0.0f);
	inData.tex4 = inData.uv + float2(-9.0f / width, 0.0f);
	inData.tex5 = inData.uv + float2(-11.0f / width, 0.0f);
	inData.tex6 = inData.uv + float2(-13.0f / width, 0.0f);
	inData.tex7 = inData.uv + float2(-15.0f / width, 0.0f);
}

VS_OUT VS(in VS_INPUT input) {

	float4 pos;
	pos = mul(float4(input.pos, 1), World);
	pos = mul(pos, Projection);

	VS_OUT outPut;
	outPut.pos = pos;
	outPut.uv = input.uv;
	if (IsVertical == 1) {
		VerticalUV(outPut);
	}
	else {
		HorizontalUV(outPut);
	}
	return outPut;
}



float4 Sample(in float weight, in float2 uv, in float2 reUV, in float2 shift) {
	return weight * (tex.Sample(smpClap, uv) + tex.Sample(smpClap, reUV + shift));
}

/**
* @brief	縦ぼかし処理
*/
void VerticalGradation(in VS_OUT input, out float4 result) {
	result = float4(0, 0, 0, 1);


	float width, height;
	tex.GetDimensions(width, height);

	float2 offset = float2(0, Offset / height);

	result += Sample(Weight0.x, input.tex0, input.tex7, offset);
	result += Sample(Weight0.y, input.tex1, input.tex6, offset);
	result += Sample(Weight0.z, input.tex2, input.tex5, offset);
	result += Sample(Weight0.w, input.tex3, input.tex4, offset);

	result += Sample(Weight1.x, input.tex4, input.tex3, offset);
	result += Sample(Weight1.y, input.tex5, input.tex2, offset);
	result += Sample(Weight1.z, input.tex6, input.tex1, offset);
	result += Sample(Weight1.w, input.tex7, input.tex0, offset);
}

/**
* @brief	横ぼかし処理
*/
void HorizontalGradation(in VS_OUT input, out float4 result) {
	result = float4(0, 0, 0, 1);

	float width, height;
	tex.GetDimensions(width, height);

	float2 offset = float2(Offset / width, 0);

	result += Sample(Weight0.x, input.tex0, input.tex7, offset);
	result += Sample(Weight0.y, input.tex1, input.tex6, offset);
	result += Sample(Weight0.z, input.tex2, input.tex5, offset);
	result += Sample(Weight0.w, input.tex3, input.tex4, offset);

	result += Sample(Weight1.x, input.tex4, input.tex3, offset);
	result += Sample(Weight1.y, input.tex5, input.tex2, offset);
	result += Sample(Weight1.z, input.tex6, input.tex1, offset);
	result += Sample(Weight1.w, input.tex7, input.tex0, offset);
}


float4 PS(in VS_OUT input) : SV_TARGET
{
	float4 color;
	if (IsVertical == 1) {
		VerticalGradation(input, color);
	}
	else {
		HorizontalGradation(input, color);
	}
	//VerticalGradation(input, color);
	return color;
	//return  gradation(input.uv);
}