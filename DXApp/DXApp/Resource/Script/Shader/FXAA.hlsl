#define FXAA_REDUCE_MIN   (1.0/ 128.0)
#define FXAA_REDUCE_MUL   (1.0 / 8.0)
#define FXAA_SPAN_MAX     8.0


// 定数バッファのデータ定義�@　変更しないデータ
cbuffer ConstBuffer : register(b0) { // 常にスロット「0」を使う
	matrix Projection;   // 透視変換行列
};

// 定数バッファのデータ定義�A　変更頻度の低いデータ
cbuffer UpdateBuffer : register(b1) { // 常にスロット「1」を使う
	matrix World;   // 座標変換行列
	float LumaThreshold;
	float3 Damu;
};

Texture2D diffuse: register(t0);   // テクスチャ
					 // サンプラ
SamplerState smpWrap : register(s0);

struct VS_INPUT {
	float3 pos : SV_Position;
	float2 uv : TEXTURE;
};

struct VS_OUT {
	float4 pos : SV_Position;
	float2 uv : TEXTURE;
};


struct PS_INPUT {

};

VS_OUT VS(VS_INPUT input) {

	float4 pos;
	pos = mul(float4(input.pos, 1), World);
	pos = mul(pos, Projection);

	VS_OUT outPut;
	outPut.pos = pos;
	outPut.uv = input.uv;
	return outPut;
}


float4 PS(VS_OUT input) : SV_TARGET
{
	float2 texSize,level;
	diffuse.GetDimensions(texSize.x, texSize.y);

	float pixX = 1.0 / texSize.x;
	float pixY = 1.0 / texSize.y;

	float3 baseColor = diffuse.Sample(smpWrap, input.uv).xyz;


	float3 n = diffuse.Sample(smpWrap, input.uv + float2(-pixX, 0)).xyz;
	float3 e = diffuse.Sample(smpWrap, input.uv + float2(0, -pixY)).xyz;
	float3 w = diffuse.Sample(smpWrap, input.uv + float2(0, -pixY)).xyz;
	float3 s = diffuse.Sample(smpWrap, input.uv + float2(pixX, 0)).xyz;

	float3 nw = diffuse.Sample(smpWrap, input.uv + float2(-pixX,-pixY)).xyz;
	float3 ne = diffuse.Sample(smpWrap, input.uv + float2(pixX, -pixY)).xyz;
	float3 sw = diffuse.Sample(smpWrap, input.uv + float2(-pixX, pixY)).xyz;
	float3 se = diffuse.Sample(smpWrap, input.uv + float2(pixX, pixY)).xyz;

	//!基準色(グレースケール)
	const float3 toLuma = float3(0.299,0.587,0.114);

	float lumaN = dot(n, toLuma);
	float lumaE = dot(e, toLuma);
	float lumaW = dot(w, toLuma);
	float lumaS = dot(s, toLuma);

	float lumaNW = dot(nw, toLuma);
	float lumaNE = dot(ne, toLuma);
	float lumaSW = dot(sw, toLuma);
	float lumaSE = dot(se, toLuma);
	float lumaBase = dot(baseColor, toLuma);




	//一番輝度の小さい色をみつける
	float lumaMin = min(lumaNW, lumaNE);
	lumaMin = min(lumaMin,min(lumaSW,lumaSE));
	lumaMin = min(lumaMin, min(lumaN, lumaE));
	lumaMin = min(lumaMin, min(lumaW, lumaS));
	lumaMin = min(lumaMin,lumaBase);

	float lumaMax = max(lumaNW, lumaNE);
	lumaMax = max(lumaMax, max(lumaSW, lumaSE));
	lumaMax = max(lumaMax, max(lumaN, lumaE));
	lumaMax = max(lumaMax, max(lumaW, lumaS));
	lumaMax = max(lumaMax, lumaBase);


	//エッジでない部分は普通に描画する
	if (lumaMax - lumaMin < lumaMax * LumaThreshold) {
		return  float4(baseColor, 1);
	}

	float3 color = nw + ne + sw + se + n + e + w + s;
	color /= 8;
	return float4(color.x, color.y, color.z,1);
}