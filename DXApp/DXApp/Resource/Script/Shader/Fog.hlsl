// 定数バッファのデータ定義�@　変更しないデータ
cbuffer ConstBuffer : register(b0) { // 常にスロット「0」を使う
	matrix Projection;   // 透視変換行列
};

// 定数バッファのデータ定義�A　変更頻度の低いデータ
cbuffer UpdateBuffer : register(b1) { // 常にスロット「1」を使う
	matrix World;   // 座標変換行列
	float3 FogColor;//深度センター
	float Start;//フォグはじめ
	float End;//フォグ終わり
	float Far;
	float Near;
	float Damy;
};

Texture2D diffuse: register(t0);   // テクスチャ
Texture2D depthTex: register(t1);   // 深度テクスチャ


					 // サンプラ
SamplerState smpWrap : register(s0);

struct VS_INPUT {
	float3 pos : SV_Position;
	float2 uv : TEXTURE;
};

struct VS_OUT {
	float4 pos : SV_Position;
	float2 uv : TEXTURE;
};


struct PS_INPUT {

};

VS_OUT VS(VS_INPUT input) {

	float4 pos;
	pos = mul(float4(input.pos, 1), World);
	pos = mul(pos, Projection);

	VS_OUT outPut;
	outPut.pos = pos;
	outPut.uv = input.uv;
	return outPut;
}


float Depth(float inZ) {
	float linerDepth = 1.0 / (Far - Near);

	return linerDepth * inZ;
}



float4 PS(VS_OUT input) : SV_TARGET
{

	float z = depthTex.Sample(smpWrap, input.uv).z;

	float rate = (End - z) / (End - Start);

	float fogFactor = clamp(rate,0.0,1.0);

	float4 diffuseColor = diffuse.Sample(smpWrap, input.uv);
	float3 color = lerp(diffuseColor.xyz, FogColor, fogFactor);

	////float4 result = float4(z,z,z,1);
	//float4 result = float4(color.x, color.y, color.z, 1);

	return float4(color.x, color.y, color.z,1);

	//return float4(1,1,1,1);

}