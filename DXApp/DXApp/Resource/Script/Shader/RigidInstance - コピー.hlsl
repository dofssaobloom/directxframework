#include"CommonFunction.hlsl"

// 定数バッファのデータ定義�@　変更しないデータ
cbuffer ConstBuffer : register(b0) { // 常にスロット「0」を使う
	matrix Projection;   // 透視変換行列
};

// 定数バッファのデータ定義�A　変更頻度の低いデータ
cbuffer UpdateBuffer : register(b1) { // 常にスロット「1」を使う
	matrix View;   // ビュー変換行列
	matrix World;   // 座標変換行列
	float Width; //線の幅
	float3 Damy;
};

//頂点シェーダの入力
struct VS_INPUT {
	float3 pos : SV_Position;
	float3 normal : NORMAL;
	float3 color : COLOR;
	uint vID : SV_VertexID;
};
//頂点シェーダの出力
struct VS_OUT {
	float4 pos : SV_Position;
	float3 normal : NORMAL;
	float3 color : COLOR;
};

//ピクセルシェーダの出力
struct PS_OUT {
	float4 color : SV_TARGET0;
	float4 normal : SV_TARGET1;
	float4 world : SV_TARGET2;
	float4 materialParam : SV_TARGET3;
	float4 shadow : SV_TARGET4;
	float4 environment : SV_TARGET5;
	float4 metalness : SV_TARGET6;
	float4 velocity : SV_TARGET7;
};

VS_OUT VS(VS_INPUT input) {

	float4 world = mul(float4(input.pos, 1), World);
	float4 pos = mul(world, View);
	pos = mul(pos, Projection);

	matrix rotateMat = GerRotateMatrix(World);

	VS_OUT outPut;
	outPut.pos = pos;
	outPut.normal = mul(float4(input.normal, 1), rotateMat);
	outPut.color = input.color;

	return outPut;
}


/// <summary>
/// ジオメトリシェーダーで4角形を追加
/// </summary>
void addQuad(in line VS_OUT input[2], inout TriangleStream<VS_OUT> outStream, float3 normal, float width, float3 offset) {
	VS_OUT outPut;

	//縦方向の幅
	float3 wight = normal * width + offset;

	for (int v = 0; v < 2; ++v) {
		outPut.pos = input[v].pos;
		outPut.normal = input[v].normal;
		outPut.color = input[v].color;

		outStream.Append(outPut);
	}

	outPut.pos = input[0].pos - float4(wight, 0);

	outStream.Append(outPut);

	outStream.RestartStrip();


	//２個め

	for (int v = 0; v < 2; ++v) {
		outPut.pos = input[v].pos - float4(wight, 0);
		outPut.normal = input[v].normal;
		outPut.color = input[v].color;

		outStream.Append(outPut);
	}

	outPut.pos = outPut.pos + float4(wight, 0);
	outPut.normal = input[1].normal;
	outPut.color = input[1].color;
	outStream.Append(outPut);

	outStream.RestartStrip();

}


[maxvertexcount(18)]
void GS(in line VS_OUT input[2], inout TriangleStream<VS_OUT> outStream) {

	float3 verticalNormal = normalize(input[0].normal);
	float3 front = float3(0, 0, 1);
	float3 horizontalNormal = cross(verticalNormal, front);

	float offset = Width * 0.5;
	float3 centerOffset = float3(offset,0, 0);
	//addQuad(input, outStream, verticalNormal, Width, centerOffset);
	addQuad(input, outStream, horizontalNormal, Width, centerOffset);

	//addQuad(input, outStream, verticalNormal, width, float3(-offset, -offset,0));
	//addQuad(input, outStream, horizontalNormal, width, float3(-offset, offset, 0));
}

float4 PS(in VS_OUT input) : SV_TARGET
{
	return float4(1,0,0,1);
}