// 定数バッファのデータ定義�@　変更しないデータ
cbuffer ConstBuffer : register(b0) { // 常にスロット「0」を使う
	matrix Projection;   // 透視変換行列
};

// 定数バッファのデータ定義�A　変更頻度の低いデータ
cbuffer UpdateBuffer : register(b1) { // 常にスロット「1」を使う
	matrix World;   // 座標変換行列
	float2 PixSize;//ピクセルサイズ
	float VelocitySoeed; //移動速度調整用
	float Damy;
};

Texture2D diffuse: register(t0);   // テクスチャ
Texture2D velocityTex: register(t1);   // テクスチャ

					 // サンプラ
SamplerState smpWrap : register(s0);

struct VS_INPUT {
	float3 pos : SV_Position;
	float2 uv : TEXTURE;
};

struct VS_OUT {
	float4 pos : SV_Position;
	float2 uv : TEXTURE;
};


struct PS_INPUT {

};

VS_OUT VS(VS_INPUT input) {

	float4 pos;
	pos = mul(float4(input.pos, 1), World);
	pos = mul(pos, Projection);

	VS_OUT outPut;
	outPut.pos = pos;
	outPut.uv = input.uv;
	return outPut;
}


float4 velocityGradation(float2 uv, float2 vel) {
	float4 color = float4(0,0,0,0);

	int sampleCount = 10;

	for (int i = 0; i < sampleCount; i++) {
		//uv += vel;
		float2 offset = vel * (float(i) / float(sampleCount - 1) - 0.5);

		color += diffuse.Sample(smpWrap, uv + offset);
	}

	return color/ sampleCount ;
}



float4 PS(VS_OUT input) : SV_TARGET
{
	float2 velocity = velocityTex.Sample(smpWrap,  input.uv).xy;
	float speed = VelocitySoeed;


	return velocityGradation(input.uv, velocity.xy * speed);
}