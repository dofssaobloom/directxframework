// 定数バッファのデータ定義�@　変更しないデータ
cbuffer ConstBuffer : register(b0) { // 常にスロット「0」を使う
	matrix Projection;   // 透視変換行列
};

// 定数バッファのデータ定義�A　変更頻度の低いデータ
cbuffer UpdateBuffer : register(b1) { // 常にスロット「1」を使う
	matrix World;   // 座標変換行列
	float2 Center;
	float Shift;
	float Damy;
};

Texture2D diffuse: register(t0);   // テクスチャ
					 // サンプラ
SamplerState smpWrap : register(s0);

struct VS_INPUT {
	float3 pos : SV_Position;
	float2 uv : TEXTURE;
};

struct VS_OUT {
	float4 pos : SV_Position;
	float2 uv : TEXTURE;
};

float rand(float3 co)
{
	return frac(sin(dot(co.xyz, float3(12.9898, 78.233, 56.787))) * 43758.5453);
}

float noise(float3 pos)
{
	float3 ip = floor(pos);
	float3 fp = smoothstep(0, 1, frac(pos));
	float4 a = float4(
		rand(ip + float3(0, 0, 0)),
		rand(ip + float3(1, 0, 0)),
		rand(ip + float3(0, 1, 0)),
		rand(ip + float3(1, 1, 0)));
	float4 b = float4(
		rand(ip + float3(0, 0, 1)),
		rand(ip + float3(1, 0, 1)),
		rand(ip + float3(0, 1, 1)),
		rand(ip + float3(1, 1, 1)));

	a = lerp(a, b, fp.z);
	a.xy = lerp(a.xy, a.zw, fp.y);
	return lerp(a.x, a.y, fp.x);
}

float perlin(float3 pos)
{
	return
		(noise(pos) * 32 +
			noise(pos * 2) * 16 +
			noise(pos * 4) * 8 +
			noise(pos * 8) * 4 +
			noise(pos * 16) * 2 +
			noise(pos * 32)) / 63;
}

VS_OUT VS(VS_INPUT input) {

	float4 pos;
	pos = mul(float4(input.pos, 1), World);
	pos = mul(pos, Projection);

	VS_OUT outPut;
	outPut.pos = pos;
	outPut.uv = input.uv;
	return outPut;
}


float4 PS(VS_OUT input) : SV_TARGET
{
	float3 color = float3(0,0,0);
	float random = rand(float3(input.uv.x,input.uv.y,0) + float3(12.9898, 78.233, 151.7182));

	float2 clampedCenter = smoothstep(float2(0,0),float2(1,1), Center);
	float2 center = clampedCenter - input.uv;
	float totalWeight = 0.0;
	float2 pixSize;
	diffuse.GetDimensions(pixSize.x, pixSize.y);
	pixSize = 1.0 / pixSize;

	float end = 30;
	float rate = 1.0 / end;
	for (int i = 0; i < end; i++)
	{
		float percent = (i + random) * rate * pixSize;
		float weight = percent - pow(percent,2);
		float2 shift = center * percent * Shift;
		color += diffuse.Sample(smpWrap, input.uv + shift).xyz * weight;
		totalWeight += weight;
	}

	color /= totalWeight;

	return float4(color.x, color.y, color.z,1.0);
}
