
/**
* @file	DeferredLighting.hlsl
* @brief	遅延レンダリングシェーダー
* @authro	高須優輝
* @date	2017/03/26
*/

#define MAX_POINT_LIGHT_NUM 160
#define ANBIENT 0.15

// 定数バッファのデータ定義�@　変更しないデータ
cbuffer ConstBuffer : register(b0) { // 常にスロット「0」を使う
	matrix Projection;   // 透視変換行列
};

// 定数バッファのデータ定義�A　変更頻度の低いデータ
cbuffer UpdateBuffer : register(b1) { // 常にスロット「1」を使う
	matrix World;   // 座標変換行列
	float4 DirectionalLightPos;
	float4 DirectionalLightColor;
	//float4 LightPos[MAX_POINT_LIGHT_NUM];
	//float4 Attrib[MAX_POINT_LIGHT_NUM];
	//float4 PointColor[MAX_POINT_LIGHT_NUM];
	int  ActiveLightNum;
	float3 CameraPos;
	float4 AmbientColor;
};

cbuffer LightBuffer : register(b2) { // 常にスロット「1」を使う

	float4 LightPos[MAX_POINT_LIGHT_NUM];
	float4 Attrib[MAX_POINT_LIGHT_NUM];
	float4 PointColor[MAX_POINT_LIGHT_NUM];
};

Texture2D colorBuffer : register(t0);   //! カラーテクスチャ
Texture2D normalBuffer : register(t1);   //! ノーマルテクスチャ
Texture2D worldBuffer : register(t2);   //! ワールド座標テクスチャ
Texture2D paramBuffer : register(t3);   //! マテリアlパラメータテクスチャ
Texture2D shadowBuffer : register(t4);   //! スペキュラーテクスチャ
Texture2D environmentBuffer : register(t5);   //! 環境マッピングテクスチャ
Texture2D metalRoughnessBuffer : register(t6);   //! メタルネス&ラフネステクスチャ

TextureCube environmentTex : register(t8);   // 環境テクスチャ

					 // サンプラ
SamplerState smpClamp : register(s0);

struct VS_INPUT {
	float3 pos : SV_Position;
	float2 uv : TEXTURE;
};

struct VS_OUT {
	float4 pos : SV_Position;
	float2 uv : TEXTURE;
	float3 lightVec : TEXTURE1;
};

/// <summary>
/// ライト情報
/// </summary>
struct LightInfo {
	float3 bright;
	float specular;
};

struct SurfaceInfo {
	float4 baseColor;
	float3 normal;
	float3 world;
	float4 environment;
	float3 viewVec;
	float specularMap;
	float specularPower;
	float metalness;
	float roughness;
	bool isNotLighting;
};


VS_OUT VS(in VS_INPUT input) {

	float4 pos;
	pos = mul(float4(input.pos, 1), World);
	pos = mul(pos, Projection);

	float3 lightVec = normalize(DirectionalLightPos.xyz - float3(0, 0, 0));

	VS_OUT outPut;
	outPut.pos = pos;
	outPut.uv = input.uv;
	outPut.lightVec = lightVec;
	return outPut;
}

float4 Decode(float4 color) {
	return color * 0.5 - 0.5;
}

float Specular(in SurfaceInfo surface, in float3 lightVec) {
	float3 H = normalize(lightVec + surface.viewVec);

	float s = saturate(dot(surface.normal, H));

	return max(pow(s, surface.specularPower), 0.0);
}

float D_GGX(in SurfaceInfo surface, in float3 lightVec) {
	float3 hVec = normalize(lightVec + surface.viewVec);
	float s = saturate(dot(hVec, surface.normal));
	//あとでラフネスパラメータに変更する
	float raughness = saturate(surface.roughness);
	//float raughness = saturate(0.5);
	//TODO powの関数呼び出しがコスト高いようなら変更する
	float alpha = raughness * raughness;
	float alpha2 = alpha * alpha;

	float t = (s * s) * (alpha2 - 1.0) + 1.0;
	float PI = 3.1415926535897;

	return alpha2 / (PI * t * t);
}

float G_CookTorrance(in SurfaceInfo surface, in float3 lightVec) {
	float3 hVec = normalize(lightVec + surface.viewVec);

	float nDotH = saturate(dot(surface.normal, hVec));
	float nDotL = saturate(dot(surface.normal, lightVec));
	float nDotV = saturate(dot(surface.normal, surface.viewVec));
	float vDotH = saturate(dot(surface.viewVec, hVec));

	float NH2 = 2 * nDotH;
	float g1 = (NH2 * nDotV) / vDotH;
	float g2 = (NH2 * nDotL) / vDotH;
	float G = min(1.0, min(g1, g2));

	return G;
}

/// <summary>
/// フルネル項の計算
/// </summary>
float Flesnel(in SurfaceInfo surface, in float3 lightVec) {
	float3 hVec = normalize(lightVec + surface.viewVec);
	float vewDotH = saturate(dot(lightVec, hVec));

	//TODO あとで変更できるようにする
	float reflectance = 0.5;
	float flesnel = pow(1 - vewDotH, 5.0);
	flesnel *= (1.0 - reflectance);
	flesnel += reflectance;
	return flesnel;
}

/**
* @brief	1つあたりのポイントライトの計算をする
*/
LightInfo PointLight(in SurfaceInfo surface, int location) {

	float3 lihgtVec = LightPos[location] - surface.world;
	//!ライトの影響度
	float lightLength = length(lihgtVec);
	//!ライトの方向
	lihgtVec = normalize(lihgtVec);

	// //最大の明るさ　0(Max) ~ 1(最低)
	// float maxBright = 0.5;
	// //近い距離をどれほど照らすか0(Max) ~ 1(最低)
	// float lightNear = 0.5;
	// //遠い距離まで届きやすさ0(Max) ~ 1(最低)
	// float lightFar = 0.0;
	float num = (Attrib[location].x +
		Attrib[location].y * lightLength +
		Attrib[location].z  * pow(lightLength, 2));
	float attenution = 0;
	if (num != 0)
		attenution = 1.0 / num;

	float pointPower = max(dot(lihgtVec, surface.normal), ANBIENT) * attenution;
	float3 bright = surface.baseColor * PointColor[location] * pointPower;
	float specular = Specular(surface, lihgtVec) * attenution;
	LightInfo result = { bright, specular };
	return result;
}


/**
* @brief	このピクセルのすべてのポイントライトの計算をする
*/
LightInfo  AllPointLighting(in SurfaceInfo surface) {
	//if (ActiveLightNum >= MAX_POINT_LIGHT_NUM) {
	//	LightInfo result = { float3(0,0,0) ,0 };
	//	return result;
	//}
	float3 totalBright = (float4)0;
	float3 bright = (float4)0;
	float totalSpecular = (float4)0;
	float specular = (float4)0;
	for (int i = 0; i != ActiveLightNum; i++) {
		LightInfo attrib = PointLight(surface, i);
		bright = max(attrib.bright * Attrib[i].w, bright);
		totalBright = bright;
		specular = max(attrib.specular , specular);
		totalSpecular = specular;
	}
	LightInfo result = { totalBright ,totalSpecular };
	return result;
}


//ピクセルシェーダの出力
struct PS_OUT {
	float4 color : SV_TARGET0;
	float4 specular : SV_TARGET1;
};

/// <summary>
/// フェースの方向がライトのベクトルと一致するか
/// </summary>
bool faceIsLightDirection(in float3 normal, in float3 lightVec) {
	return dot(normal, lightVec) >= 0.0;
}

/// <summary>
/// スペキュラを質量保存に従わせる
/// </summary>
float convertF(in SurfaceInfo face, in float specular) {

	float localSpecular = 0.5;
	//スペキュラを質量保存に従わせるための値
	float f = lerp(0.08 * localSpecular, face.baseColor, face.metalness);

	return f + (1 - f)* specular;
}

/// <summary>
/// スペキュラー追加
/// </summary>
void addSpecular(bool isShadow, in LightInfo directionalInfo,
	in LightInfo pointInfo, in SurfaceInfo face,
	in out float4 outColor, out float outSpecular) {

	if (isShadow) {
		float pointSpecular = pointInfo.specular * face.specularMap;
		pointSpecular = convertF(face, pointSpecular);
		outColor += pointSpecular;
		outSpecular = pointSpecular;
	}
	else {
		float specular = 0;
		specular = max(directionalInfo.specular, pointInfo.specular);//強い光で上書き
		specular = convertF(face, specular);
		outColor += specular;
		outSpecular = specular;
	}
}


/// <summary>
/// ディレクショナルライト計算
/// </summary>
LightInfo DirectionalCual(in SurfaceInfo face, in float3 lightVec) {
	float power = max(dot(normalize(face.normal), lightVec), AmbientColor);
	float3 dColor = max(DirectionalLightColor, AmbientColor);
	float3 bright = face.baseColor * dColor * power;
	//スペキュラの計算は他でしている
//	float specular = Specular(face, lightVec) * max(DirectionalLightColor.z,max(DirectionalLightColor.x, DirectionalLightColor.y));
	LightInfo result = { bright,0 };
	return result;
}

/// <summary>
/// フェース情報を格納
/// </summary>
void StoreFaceInfo(in VS_OUT input, out SurfaceInfo face) {
	face.baseColor = pow(colorBuffer.Sample(smpClamp, input.uv), 2.2);
	face.normal = normalBuffer.Sample(smpClamp, input.uv).xyz;
	face.world = worldBuffer.Sample(smpClamp, input.uv).xyz;
	//ビューのベクトル作成
	face.viewVec = normalize(CameraPos - face.world);
	float3 param = paramBuffer.Sample(smpClamp, input.uv);
	face.specularPower = param.x;
	float2 metalRoghness = metalRoughnessBuffer.Sample(smpClamp, input.uv).xy;
	face.metalness = metalRoghness.x;
	//face.metalness = 1.0;
	face.roughness = metalRoghness.y;
	//face.roughness = 1.0;
	face.isNotLighting = param.y == 1;//ライティングするかどうか
	face.specularMap = param.z;
	float3 n = float3(face.normal.x, -face.normal.y, face.normal.z);
	float3 ref = reflect(face.viewVec, n);
	face.environment = environmentTex.SampleLevel(smpClamp, ref, 12 * face.roughness); // 0 ~ 12でラープする
	face.environment = lerp(face.environment, face.environment * face.baseColor, face.metalness);
}


/**
* @brief	ピクセルシェーダのエントリポイント
*/
PS_OUT PS(in VS_OUT input)
{
	//テクスチャから取り出したフェースの情報
	SurfaceInfo surfaceInfo;

	StoreFaceInfo(input, surfaceInfo);

	//ライティングしないものはそのまま出力
	if (surfaceInfo.isNotLighting) {
		PS_OUT result;
		result.color = pow(surfaceInfo.baseColor, 1 / 2.2) * DirectionalLightColor;
		result.color.w = 1.0;
		result.specular = float4(0, 0, 0, 1);

		result.specular.a = result.color.a = 1.0;
		return result;
	}


	//!最終的に出力される情報
	PS_OUT result;
	result.specular = 0;

	//ディレクショナルライト計算
	LightInfo directionalInfo = DirectionalCual(surfaceInfo, input.lightVec);

	//計算済みの影フラグ取得
	bool isShadow = shadowBuffer.Sample(smpClamp, input.uv).x;

	if (faceIsLightDirection(surfaceInfo.normal, -input.lightVec)) {
		isShadow = 0;
	}

	LightInfo pointLight = AllPointLighting(surfaceInfo);

	//影であれば
	if (isShadow != 0) {
		directionalInfo.bright *= AmbientColor;
	}


	float D = D_GGX(surfaceInfo, input.lightVec);

	float F = Flesnel(surfaceInfo, input.lightVec);

	float G = G_CookTorrance(surfaceInfo, input.lightVec);

	float3 outColor = (float3)0;
	outColor = max(pointLight.bright, directionalInfo.bright);//強い光で上書き
	outColor = pow(outColor, 1 / 2.2);
	result.color = float4(outColor.x, outColor.y, outColor.z, 1.0);
	result.color = lerp(result.color, surfaceInfo.environment, surfaceInfo.metalness);

	//影でなければ
	if (isShadow == 0) {
		float nDotL = saturate(dot(surfaceInfo.normal, input.lightVec));
		float nDotV = saturate(dot(surfaceInfo.normal, surfaceInfo.viewVec));
		result.specular = (D * F * G) / (4.0 * nDotV * nDotL + 0.001);
		result.specular = result.specular * DirectionalLightColor;// *surfaceInfo.specularMap;
		result.specular *= max(surfaceInfo.metalness, 0.08);
		result.specular.a = 1;
	}

	result.specular += pointLight.specular * max(surfaceInfo.metalness, 0.08);// *surfaceInfo.specularMap;

	result.color = result.specular + result.color;
	result.color.a = 1;

	result.specular = saturate(result.specular);

	//result.color = surfaceInfo.environment;


	return result;
}