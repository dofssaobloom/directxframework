/// <summary>
/// 共通関数
/// </summary>

/// <summary>
/// データ構造
/// Animation1
/// boneID1.x, boneID3.x, boneID3.x…
/// boneID1.y, boneID3.y, boneID3.y…
/// boneID1.z, boneID3.z, boneID3.z…
/// boneID1.w, boneID3.w, boneID3.w…
/// 
/// /// Animation2
/// boneID1.x, boneID3.x, boneID3.x…
/// boneID1.y, boneID3.y, boneID3.y…
/// boneID1.z, boneID3.z, boneID3.z…
/// boneID1.w, boneID3.w, boneID3.w…
/// .
/// .
/// .
/// </summary>
matrix LoadBoneTex(int boneID, int frame, int animeNum,in Texture2D boneTexture) {

	int animeOffset = (120 * 4) *
		animeNum;
	int frameOffset = frame * 4 + animeOffset;

	float4 bone1 = boneTexture.Load(int3(boneID, frameOffset + 0, 0));
	float4 bone2 = boneTexture.Load(int3(boneID, frameOffset + 1, 0));
	float4 bone3 = boneTexture.Load(int3(boneID, frameOffset + 2, 0));
	float4 bone4 = boneTexture.Load(int3(boneID, frameOffset + 3, 0));
	return matrix(
		bone1,
		bone2,
		bone3,
		bone4);
}

/// <summary>
/// テクスチャを読み込んでマトリクス作成
/// </summary>
matrix LoadBoneMatrix(in uint4 boneIndex,in int frame,in int animeNum,
	in Texture2D boneTexture,in float4 weight,in float blendWeight = 1.0) {
	matrix boneMat;
	boneMat =  LoadBoneTex(boneIndex.x, frame, animeNum, boneTexture) * weight.x * blendWeight;
	boneMat += LoadBoneTex(boneIndex.y, frame, animeNum, boneTexture) * weight.y * blendWeight;
	boneMat += LoadBoneTex(boneIndex.z, frame, animeNum, boneTexture) * weight.z * blendWeight;
	boneMat += LoadBoneTex(boneIndex.w, frame, animeNum, boneTexture) * weight.w * blendWeight;

	return boneMat;
}

/// <summary>
/// ボーンブレンド
/// boneIndex ボーンID
/// currentFrame　ブレンドする現在のアニメーションのフレーム番号
/// animeNum	  現在のアニメーション番号
/// nextFrame     ブレンドする次のアニメーションのフレーム番号
/// nextAnimeNum  次のアニメーション番号
/// boneTexture   ボーンテクスチャ
/// boneWight     ボーンの重み
/// lerpX         ラープ値
/// </summary>
matrix BoneBlending(in uint4 boneIndex, in int currentFrame, in int animeNum, in int nextFrame,
	in int nextAnimeNum,in Texture2D boneTexture,float4 boneWight,in float lerpX) {

	matrix boneMat = LoadBoneMatrix(boneIndex, currentFrame, animeNum, boneTexture, boneWight, 1.0 - lerpX);
	boneMat += LoadBoneMatrix(boneIndex, nextFrame, nextAnimeNum, boneTexture, boneWight, lerpX);

	return boneMat;
}


matrix IdentityMat() {
	return matrix(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);
}

/**
* @brief			モーション行列から移動成分を削除する
* @param inMat		モーション行列
*/
matrix removeMoveVec(matrix inMat) {
	return matrix(
		1, inMat[0][1], inMat[0][2], 0,
		inMat[1][0], 1, inMat[1][2], 0,
		inMat[2][0], inMat[2][1], 1, 0,
		0.0, 0.0, 0.0, 1.0
		);
}

/// <summary>
/// 行列から回転成分だけ取り出す
/// </summary>
matrix GerRotateMatrix(in matrix mat) {

	mat[0][1] = 0;
	mat[0][3] = 0;
	mat[1][0] = 0;
	mat[1][1] = 1;
	mat[1][3] = 0;
	mat[2][1] = 0;
	mat[2][3] = 0;
	mat[3][0] = 0;
	mat[3][1] = 0;
	mat[3][2] = 0;
	mat[3][3] = 1;

	return mat;
}