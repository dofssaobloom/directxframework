#include"CommonFunction.hlsl"

//最大描画数は100
#define MAX_DRAW_NUM 100

// 定数バッファのデータ定義�@　変更しないデータ
cbuffer ConstBuffer : register(b0) { // 常にスロット「0」を使う
	matrix Projection;   // 透視変換行列
};

// 定数バッファのデータ定義�A　変更頻度の低いデータ
cbuffer UpdateBuffer : register(b1) { // 常にスロット「1」を使う
	matrix View;   // ビュー変換行列
	matrix PreView;
	matrix LightView; //差シャドウマッピング用行列
	matrix World[MAX_DRAW_NUM];   // 座標変換行列
	matrix PreWorld[MAX_DRAW_NUM];   // 座標変換行列
	matrix Roate[MAX_DRAW_NUM];   //法線を回転させるための回転行列
	float3 EyePos;
	float Near;//描画近さ
	float Far;//描画距離
	float FatParam;
	float2 Damy;
};

//マテリアルのパラメータ用バッファ
cbuffer MaterialBuffer : register(b2) {
	float Height;
	float Specular;
	float isNotLighting;
	float Bias;

	float Mettalic;//テスト用
	float3 Damy2;
}

Texture2D colorTex : register(t0);   // テクスチャ
Texture2D normalTex : register(t1);   // テクスチャ
Texture2D heightTex : register(t2);   // テクスチャ
Texture2D specularTex : register(t3);   // テクスチャ
Texture2D metalnessTex : register(t4);   // テクスチャ
Texture2D roughnessTex : register(t5);   // テクスチャ

Texture2D shadowTex : register(t9);   // シャドウマップ
//TextureCube environmentTex[6] : register(t10);   // サンプラ
SamplerState smpClamp : register(s0);

SamplerState shadowSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};


//頂点シェーダの入力
struct VS_INPUT {
	float3 pos : SV_Position;
	float3 normal : NORMAL;
	float3 binormal : BINORMAL;
	float3 tangent : TANGENT;
	float2 uv : TEXTURE;
	uint instID : SV_InstanceID;
};
//頂点シェーダの出力
struct VS_OUT {
	float4 pos : SV_Position;
	float4 normal : NORMAL;
	float3 binormal : BINORMAL;
	float3 tangent : TANGENT;
	float2 uv : TEXTURE0;
	uint instID : TEXTURE1;
	float4 worldPos : TEXTURE2;
	float4 shadowUV : TEXTURE3;
	float4 projPos : TEXTURE4;
	float4 prePos :  TEXTURE5;
};

//ピクセルシェーダの出力
struct PS_OUT {
	float4 color : SV_TARGET0;
	float4 normal : SV_TARGET1;
	float4 world : SV_TARGET2;
	float4 materialParam : SV_TARGET3;
	float4 shadow : SV_TARGET4;
	float4 environment : SV_TARGET5;
	float4 metalness : SV_TARGET6;
	float4 velocity : SV_TARGET7;
};


VS_OUT VS(VS_INPUT input) {

	//float height = heightTex.SampleLevel(smpClamp, input.uv,0).x;
	float3 fatVec = input.normal * FatParam;// *pow((1 - input.pos.y), 3);
	//float3 fatVec = input.normal * FatParam ;
	//if (height != 0) {
	//	fatVec.y += FatParam * -0.5;
	//}
	//float4 fatPos = float4(input.pos + fatVec, 1);
	float4 world = mul(float4(input.pos, 1), World[input.instID]);
	float4 pos = mul(world, View);
	pos = mul(pos, Projection);

	float4 prePos = mul(float4(input.pos, 1), PreWorld[input.instID]);
	prePos = mul(prePos, PreView);
	prePos = mul(prePos, Projection);

	VS_OUT outPut;
	outPut.pos = pos;
	outPut.worldPos = world;
	outPut.projPos = pos;
	outPut.prePos = prePos;
	outPut.normal = float4(input.normal, 1);
	outPut.binormal = input.binormal;
	outPut.tangent = input.tangent;
	outPut.uv = input.uv;
	outPut.instID = input.instID;

	//ライトから見た状態
	outPut.shadowUV = mul(mul(world, LightView), Projection);
	return outPut;
}

/**
* @brief				ノーマルマップを接空間にするための行列
* @param tangent		タンジェント
* @param binormal		バイノーマル
* @param vertexNormal	頂点の法線
*/
matrix InvTangentMatrix(float3 tangent, float3 binormal, float3 vertexNormal) {
	matrix mat = matrix(
		float4(tangent, 0),
		float4(binormal, 0),
		float4(vertexNormal, 0),
		float4(0, 0, 0, 1));
	return mat;
}

/**
* @brief				視点ベクトル算出
*/
float3 ViewVec(float3 worldPos) {
	return normalize(EyePos - worldPos);
}

float4 normalEyeMap(float3 eyeVec, float2 uv) {
	float hScale = heightTex.Sample(smpClamp, uv).r * Height;
	float2 texCoord = uv - hScale * eyeVec.xy;
	return normalTex.Sample(smpClamp, texCoord) * 2.0 - 1.0;
}

float thawing(float4 comissionDepth) {
	const float r = 1.0;
	const float g = 1.0 / 255.0;
	const float b = 1.0 / pow(255.0, 2);
	const float a = 1.0 / pow(255.0, 3);
	float depth = dot(comissionDepth, float4(r, g, b, a));
	return depth;
}

float liner() {
	return 1.0 / (Far - Near);
}

float isShadow(VS_OUT input) {

	input.shadowUV.xy /= input.shadowUV.w;
	float2 tex = input.shadowUV.xy * float2(0.5, -0.5) + 0.5;
	//float preDepth = thawing(shadowTex.Sample(smpClamp, tex));
	float preDepth = shadowTex.Sample(shadowSampler, tex).x;
	float currentDepth = input.shadowUV.z * liner();
	float maxDepthSlope = max(abs(ddx(input.shadowUV.z)), abs(ddx(input.shadowUV.z)));
	float slopeScaleBias = 0.0001;
	float shadowBias = Bias + slopeScaleBias * maxDepthSlope;

	//影は１　影でなければ０
	return preDepth < currentDepth - shadowBias ? 1.0 : 0.0;
}



float2 culcVelocity(VS_OUT input) {

	float2 a = (input.projPos.xy / input.projPos.w) * 0.5 + 0.5;
	float2 b = (input.prePos.xy / input.prePos.w) * 0.5 + 0.5;
	float2 vel = a - b;
	return  vel;
}


PS_OUT PS(in VS_OUT input)
{
	PS_OUT outColor;
	float4 color = colorTex.Sample(smpClamp, input.uv);
	//0~1を-1~1の範囲に変更する
	float4 normal = normalTex.Sample(smpClamp, input.uv) * 2.0 - 1.0;
	//float4 normal = normalEyeMap(ViewVec(input.worldPos), input.uv);
	//ノーマルマップを接空間に移動する
	normal = mul(normal, InvTangentMatrix(input.tangent, input.binormal, input.normal));
	normal = mul(normal, Roate[input.instID]);

	outColor.color = color;
	outColor.color.w = 1.0;
	//	float3 ref = reflect(input.worldPos - EyePos, normal);
	//outColor.environment = environmentTex.Sample(smpClamp, float4(reflect(viewVec, normal), 0));
	//outColor.environment = environmentTex.Sample(smpClamp, float4(normal.xyz, 1));
	outColor.normal = normal;
	outColor.world = input.worldPos;
	outColor.materialParam = float4(Specular, isNotLighting, specularTex.Sample(smpClamp, input.uv).x, 1);
	float shadow = isShadow(input);
	outColor.shadow = float4(shadow, input.projPos.z, input.projPos.z * liner(), 1);
	outColor.metalness.x = metalnessTex.Sample(smpClamp, input.uv).x;
	outColor.metalness.y = roughnessTex.Sample(smpClamp, input.uv).x;

	//outColor.metalness.x = Mettalic;

	float2 vel = culcVelocity(input);
	outColor.velocity = float4(vel.x, vel.y, 0, 1);
	return outColor;
	//return float4(Height,Specular, 0, 1);
}