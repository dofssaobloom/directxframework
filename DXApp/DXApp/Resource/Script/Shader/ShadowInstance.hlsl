#include"CommonFunction.hlsl"

//最大描画数は100
#define MAX_DRAW_NUM 100

//ボーンの最大数は128
#define BONE_NUM 128

#define PREFRAME 3

// 定数バッファのデータ定義�@　変更しないデータ
cbuffer ConstBuffer : register(b0) { // 常にスロット「0」を使う
	matrix Projection;   // 透視変換行列
};

// 定数バッファのデータ定義�A　変更頻度の低いデータ
cbuffer UpdateBuffer : register(b1) { // 常にスロット「1」を使う
	matrix View;   // ビュー変換行列
	matrix PreView;
	matrix LightView;   // ライトから見たビュー変換行列
	matrix World[MAX_DRAW_NUM];   // 座標変換行列
	matrix PreWorld[MAX_DRAW_NUM];   // 座標変換行列
	matrix Roate[MAX_DRAW_NUM];   //法線を回転させるための回転行列
	float3 EyePos;
	float Near;//描画近さ
	float Far;//描画距離
	float3 Damy1;
};

//マテリアルのパラメータ用バッファ
cbuffer MaterialBuffer : register(b2) {
	float Height;
	float Specular;
	float isNotLighting;
	float Bias;
}

//更新頻度の高いバッファ
cbuffer SkinningBuffer : register(b3)
{
	//!ボーン行列 メッシュの数　ボーンの数
	//matrix BoneMatrix[BONE_NUM];
	//int DrawData[MAX_DRAW_NUM];
	float4 DrawData[MAX_DRAW_NUM];//フレーム番号　アニメーション番号 アニメータがあるかどうか
	float4 BlendData[MAX_DRAW_NUM];//blendLerp,次のアニメーション番号,ブレンドするかどうか,ブレンドするフレーム番号

	float4 FatParam[MAX_DRAW_NUM];//膨らませ値
	float4 TexLerp[MAX_DRAW_NUM];//バッファ0 と6をラープする
	//float BlendLerp;//ブレンドの割合
	//int NextAnimeID;//次のアニメーションのID
	//float IsBlend;//ブレンドするかどうか
	//float Damy2;
};

Texture2D colorTex : register(t0);   // テクスチャ
Texture2D normalTex : register(t1);   // テクスチャ
Texture2D heightTex : register(t2);   // テクスチャ
Texture2D specularTex : register(t3);   // テクスチャ
Texture2D metalnessTex : register(t4);   // テクスチャ
Texture2D roughnessTex : register(t5);   // テクスチャ

Texture2D shadowTex : register(t9);   // シャドウマップ
TextureCube environmentTex : register(t10);   // 環境テクスチャ
Texture2D boneTexture : register(t11);
//Texture3D boneTexture : register(t11);
SamplerState smpWrap : register(s0);

SamplerComparisonState shadowSampler
{
	// sampler state
	Filter = MIN_MAG_LINEAR_MIP_POINT;
	AddressU = MIRROR;
	AddressV = MIRROR;

	// sampler comparison state
	ComparisonFunc = LESS;
	ComparisonFilter = COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
};


//頂点シェーダの入力
struct VS_INPUT {
	float3 pos : SV_Position;
	float3 normal : NORMAL;
	float3 binormal : BINORMAL;
	float3 tangent : TANGENT;
	float2 uv : TEXTURE;
	uint4 boneIndex : TEXTURE1;
	float4 weight : TEXTURE2;
	uint instID : SV_InstanceID;
};

//頂点シェーダの出力
struct VS_OUT {
	float4 pos : SV_Position;
	uint instID : TEXTURE1;
	float4 wvpPos : TEXTURE2;
};


VS_OUT VS(VS_INPUT input) {

	matrix boneMat = IdentityMat();

	bool isAnimatorFind = DrawData[input.instID].z;

	if (isAnimatorFind) {
		bool isBlend = BlendData[input.instID].z;
		if (isBlend) {
			boneMat = BoneBlending(input.boneIndex, DrawData[input.instID].x, DrawData[input.instID].y,
				BlendData[input.instID].w, BlendData[input.instID].y, boneTexture, input.weight, BlendData[input.instID].x);
		}
		else {
			boneMat = LoadBoneMatrix(input.boneIndex, DrawData[input.instID].x, DrawData[input.instID].y, boneTexture, input.weight);
		}
		//BoneBlending(input.boneIndex, DrawData[input.instID].x, DrawData[input.instID].y, boneTexture, input.weight, BlendLerp);
	}

	float4 fatPos = float4(input.pos + input.normal * FatParam[input.instID].x, 1);
	float4 localPos = mul(fatPos, boneMat);
	float4 world = mul(localPos, World[input.instID]);

	//シャドウマップ用なのでディレクショナルライト空間のビュープロジェクション
	float4 lightViewPos = mul(world, View);
	lightViewPos = mul(lightViewPos, Projection);

	VS_OUT outPut;
	outPut.pos = lightViewPos;
	outPut.instID = input.instID;
	outPut.wvpPos = lightViewPos;
	return outPut;
}

/// <summary>
/// データを縮める
/// </summary>
float4 Compression(float depth) {
	float r = depth;
	float g = frac(r * 255.0);
	float b = frac(g * 255.0);
	float a = frac(b * 255.0);
	float coef = 1.0 / 255.0;

	r -= g * coef;
	g -= b * coef;
	b -= a * coef;

	return float4(r, g, b, a);
}

/// <summary>
/// ライナーデプスに変換
/// </summary>
float Depth(float inZ) {
	float linerDepth = 1.0 / (Far - Near);

	return linerDepth * inZ;
}

float4 PS(VS_OUT input) : SV_TARGET0
{
	float z = Depth(input.wvpPos.z);
	return float4(z,z,z,1);
//return float4(1,0,0,1);
//float depth =  Depth(input.wvpPos.z);
//return  Compression(z);
}