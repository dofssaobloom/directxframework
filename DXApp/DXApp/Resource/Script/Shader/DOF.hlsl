// 定数バッファのデータ定義�@　変更しないデータ
cbuffer ConstBuffer : register(b0) { // 常にスロット「0」を使う
	matrix Projection;   // 透視変換行列
};

// 定数バッファのデータ定義�A　変更頻度の低いデータ
cbuffer UpdateBuffer : register(b1) { // 常にスロット「1」を使う
	matrix World;   // 座標変換行列
	float Center;//深度センター
	float Near;
	float Far;
	float Damy;
};

Texture2D diffuse: register(t0);   // テクスチャ
Texture2D gradationTex: register(t1);   //ぼかした状態
Texture2D depthTex : register(t2);   // 大きくぼかした状態

					 // サンプラ
SamplerState smpWrap : register(s0);

struct VS_INPUT {
	float3 pos : SV_Position;
	float2 uv : TEXTURE;
};

struct VS_OUT {
	float4 pos : SV_Position;
	float2 uv : TEXTURE;
};


struct PS_INPUT {

};

VS_OUT VS(VS_INPUT input) {

	float4 pos;
	pos = mul(float4(input.pos, 1), World);
	pos = mul(pos, Projection);

	VS_OUT outPut;
	outPut.pos = pos;
	outPut.uv = input.uv;
	return outPut;
}


float Depth(float inZ) {
	float linerDepth = 1.0 / (3000 - 1);

	return linerDepth * inZ;
}


/// <summary>
/// 1の範囲を広げる
/// </summary>
float convDepth(float depth, float focus) {

	float d = clamp(depth + focus, 0.0, 1.0);
	if (d > 0.6) {
		d = 2.5 * (1.0 - d);
	}
	else if (d >= 0.4) {
		d = 1.0;
	}
	else {
		d *= 2.5;
	}

	return d;

	//float d = clamp(depth + focus, 0.0, 1.0);
	//if (d > 0.5) {
	//	d = 2.5 * (1.0 - d);
	//}
	//else if (d >= 0.49) {
	//	d = 1.0;
	//}
	//else {
	//	d *= 0.5;
	//}

	//return d;
}


float3 colorLerp(float d,float3 color1,float3 color2,float3 color3) {

	float3 newColor1 = lerp(color1,color2,d);
	float3 newColor2 = lerp(color2, color3, d);

	return lerp(newColor1, newColor2,d);
}


float4 PS(VS_OUT input) : SV_TARGET
{
	//return gradationTex.Sample(smpWrap, input.uv);
	float3 defaultColor = diffuse.Sample(smpWrap, input.uv).xyz;
	float3 g1 = gradationTex.Sample(smpWrap, input.uv).xyz;

	float z = Depth(depthTex.Sample(smpWrap, input.uv).y);

	z = convDepth(z, Center);

	z = clamp(z, 0.0, 1.0);

	//float3 mixColor = colorLerp(1- z,defaultColor,g1,g2);
	float3 mixColor = lerp(defaultColor, g1, 1 - z);
	//float3 mixColor = lerp(defaultColor, g2, 1);



	//return float4(z,z,z,1);

	//return float4(g2.x, g2.y, g2.z,1);
	return float4(mixColor.x, mixColor.y, mixColor.z,1.0);
}