// 定数バッファのデータ定義�@　変更しないデータ
cbuffer ConstBuffer : register(b0) { // 常にスロット「0」を使う
	matrix Projection;   // 透視変換行列
};

// 定数バッファのデータ定義�A　変更頻度の低いデータ
cbuffer UpdateBuffer : register(b1) { // 常にスロット「1」を使う
	matrix World;   // 座標変換行列
	float Alpha;
	float3 damy;
	float4 PosisionOffset[2];
};

Texture2D myTex2D;   // テクスチャ
					 // サンプラ
SamplerState smpWrap : register(s0);

struct VS_INPUT {
	float3 pos : SV_Position;
	float2 uv : TEXTURE;
	uint vID : SV_VertexID;
};

struct VS_OUT {
	float4 pos : SV_Position;
	float2 uv : TEXTURE;
};


struct PS_INPUT {

};

VS_OUT VS(VS_INPUT input) {
	float4 pos;
	pos = float4(input.pos, 1);

	if (input.vID == 0) {
		pos.x += PosisionOffset[0].x;
		pos.y += PosisionOffset[1].x;
	}
	if (input.vID == 1) {
		pos.x += PosisionOffset[0].y;
		pos.y += PosisionOffset[1].y;
	}
	if (input.vID == 2) {
		pos.x += PosisionOffset[0].z;
		pos.y += PosisionOffset[1].z;
	}
	if (input.vID == 3) {
		pos.x += PosisionOffset[0].w;
		pos.y += PosisionOffset[1].w;
	}

	//pos.xy += PosisionOffset[input.vID];

	//if (input.vID == 3) {
	//	pos.xy += float2(0.5,0);
	//}

	pos = mul(pos, World);
	pos = mul(pos, Projection);

	VS_OUT outPut;
	outPut.pos = pos;
	outPut.uv = input.uv;
	return outPut;
}

float4 PS(VS_OUT input) : SV_TARGET
{
	//return float4(1,0,0,1);
	//return float4(input.uv,0,1);
	return myTex2D.Sample(smpWrap,input.uv) * Alpha;
//float4 color = myTex2D.Sample(smpWrap, input.uv) * Alpha;
//
//return float4(color.x,color.x, color.x, color.w);
}