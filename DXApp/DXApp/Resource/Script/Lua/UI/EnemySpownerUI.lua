


function addImte (ui)

	ui:addText ("EnemySpowner")

	--敵を出すことのできる最大値
	ui:addSlider ("BornMax", Vec2 (1.0, 50.0),10.0)

	--敵の出る間隔
	ui:addSlider ("MaxInterval", Vec2 (1.0, 30.0),20.0)

	ui:addSlider ("MinInterval", Vec2 (1.0, 30.0),10.0)

	--スポナーの位置
	ui:addSlider ("PositionX", Vec2 (-2000.0, 2000.0),0.0)
	ui:addSlider ("PositionY", Vec2 (-2000.0, 2000.0),0.0)
	ui:addSlider ("PositionZ", Vec2 (-2000.0, 2000.0),0.0)

	--スポーンの確率。A+B+Cの合計で100に収まるようにしてください
	ui:addSlider ("EnemyTypeA", Vec2 (0.0, 100.0),25.0)
	ui:addSlider ("EnemyTypeB", Vec2 (0.0, 100.0),25.0)
	ui:addSlider ("EnemyTypeC", Vec2 (0.0, 100.0),50.0)

	--時間経過とともに増えるエネミーの数
	ui:addSlider ("TimeBornUpEnemy", Vec2 (0.0, 10.0),5.0)
end