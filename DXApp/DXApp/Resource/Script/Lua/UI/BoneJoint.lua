

function addImte (ui)
	ui:addText ("BoneJoint")

	ui:addSlider ("OffsetX", Vec2 (-10.0, 10.0), 0.0)
	ui:addSlider ("OffsetY", Vec2 (-10.0, 10.0), 0.0)
	ui:addSlider ("OffsetZ", Vec2 (-10.0, 10.0), 0.0)

	ui:addSlider ("Weight", Vec2 (0.0, 1.0), 0.0)
end

