

function addImte (ui)

	ui:addText ("SpriteRenderer")

	--スケールと回転の中心位置 デフォルト中心
	ui:addSlider ("PivotX", Vec2 (0.0, 1.0),0.5)
	ui:addSlider ("PivotY", Vec2 (0.0, 1.0),0.5)
	
	ui:addSlider ("ScaleX", Vec2 (0.0, 1.0), 1.0)
	ui:addSlider ("ScaleY", Vec2 (0.0, 1.0), 1.0)

	--描画順番　値が低いほど早く描画される
	ui:addSlider ("CallOader", Vec2 (0.0, 100.0), 0.0)

	--テスト用
	ui:addSlider ("RightScale", Vec2 (0.0, 1.0), 1.0)
end

