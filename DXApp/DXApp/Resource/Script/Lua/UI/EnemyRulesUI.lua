
function addImte (ui)
	ui:addText ("EnemyRules")

	--コンボで加算される値
	ui:addSlider ("AddCombo",Vec2(1.0, 10.0),1.0)
	--スコアで加算される値
	ui:addSlider ("AddScore",Vec2(1.0, 50.0),10.0)
	--タイマーで加算される値
	ui:addSlider ("AddGameTimer",Vec2(0.0, 600.0),30.0)
	--この個数以上の場合
	ui:addSlider ("FeverTimer",Vec2(0.0, 10.0),5.0)
	--コンボ時にスコアとタイマーに乗算される値
	ui:addSlider ("MultiplyCount",Vec2(1.0, 10.0),1.0)
	--敵を何対まとめて倒したときにフォントが変化するかの値
	ui:addSlider ("FiverEnemyDeadCount",Vec2(1.0, 20.0),5.0)
	--タイマーの合計値を表示する時間
	ui:addSlider ("TotalTimer",Vec2(1.0, 120.0),30.0)

end