

function addImte (ui)

	ui:addText ("Player")
	ui:addSlider ("Speed", Vec2 (1, 100.0), 0.0)
	--回転摩擦
	ui:addSlider ("Friction", Vec2 (0.01, 1.0),0.0)

	--構えた時の回転速度
	ui:addSlider ("SetUpRotationSpeed",Vec2(1.0,100.0),0.0)
	--構えて動いた時の移動速度
	ui:addSlider ("SetUpMoveSpeed",Vec2(1.0,100.0),0.0)
	--前方向ジャンプ力
	ui:addSlider ("JumpFrontForce", Vec2 (2000.0, 20000.0), 0.0)
	--上方向ジャンプ力
	ui:addSlider ("JumpUpForce", Vec2 (2000.0, 20000.0), 0.0)
	--斜方投射オフセット
	ui:addSlider ("AngleOffset", Vec2 (10.0, 30.0), 10.0)
end

