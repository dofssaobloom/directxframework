

function addImte (ui)

	ui:addText ("PointLight")
	--近くへの影響
	ui:addSlider ("Near", Vec2 (0.001, 1.0), 1)
	--遠くへの影響
	ui:addSlider ("Far", Vec2 (0.001, 1.0), 1)
	--ライト強度
	ui:addSlider ("Power", Vec2 (0.001, 1.0), 1)
	--ライト強度
	ui:addSlider ("Bright", Vec2 (0.001, 1000.0), 10)

	--ライトカラー
	ui:addColorEditor ("LightColor", Vec4 ())

	ui:addSlider ("LightOffsetX", Vec2 (-1000.0, 1000.0), 0.0)
	ui:addSlider ("LightOffsetY", Vec2 (-1000.0, 1000.0), 0.0)
	ui:addSlider ("LightOffsetZ", Vec2 (-1000.0, 1000.0), 0.0)
end

