

function addImte (ui)

	ui:addText ("RigidBody")
	--物理挙動を向こうにするかどうか
	ui:addCheckBox ("IsTrigger", false)

	--物理エンジンでポジションの移動がされないようにするかどうか
	ui:addText ("FreezePosition")
	ui:addCheckBox ("PosX", false)
	ui:addCheckBox ("PosY", false)
	ui:addCheckBox ("PosZ", false)

	--物理エンジンで慣性で回転するかどうか
	ui:addText ("FreezeRotation")
	ui:addCheckBox ("RotateX", false)
	ui:addCheckBox ("RotateY", false)
	ui:addCheckBox ("RotateZ", false)

	--質量
	ui:addSlider ("Mass", Vec2 (0.0, 50.0), 0.0)
	--反発係数
	ui:addSlider ("Restitution", Vec2 (0.0, 1.0), 0.0)
	--摩擦係数
	ui:addSlider ("Rriction", Vec2 (0.0, 1.0), 0.0)

	--回転の中心
	ui:addSlider ("OriginY", Vec2 (0.0, 1.0), 0.5)

end

