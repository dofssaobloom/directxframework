

function GetVec2 ()
	local vec = Vec2 (0.0, 0.0)
	return vec
end

function GetVec3 ()
	local vec = Vec3 (1.0, 2.0, 3.0)
	return vec:normalize ()
end

function GetVec4 ()
	local vec = Vec4 (1.0, 2.0, 3.0, 4.0)
	return vec:normalize ()
end