#pragma once

#define UsingNamespace  using namespace framework; using namespace component;

#include<Source\Application\WindowFactory\WindowFactory.h>
#include<Source\Application\Screen\Screen.h>
#include<Source\Application\Scene\Scene.h>
#include<Source\Application\Application.h>
#include<Source\Util\Event\WindowEvent.h>
#include<Source\Util\IO\CSVLoader.h>
#include<Source\Util\IO\FbxLoader.h>
#include<Source\Util\IO\ModelInfoLoader.h>
#include<Source\Util\IO\TextLoader.h>
#include<Source\Util\Math\Math.h>
#include<Source\Util\Math\Transform.h>
#include<Source\Util\Math\Vector.h>
#include<Source\Util\Render\DXFunc.h>
#include<Source\Util\Render\RenderTargetStack.h>
#include<Source\Util\Template\Event.h>
#include<Source\Util\Thread\Thread.h>
#include<Source\Util\Thread\CriticalSection.h>
#include<Source\Util\Timer\Timer.h>
#include<Source\Util\Win\WinFunc.h>
#include<Source\Util\Type.h>
#include<Source\Util\WrapFunc.h>
#include<Source\Component\UpdateComponent.h>
#include<Source\Component\Component.h>
#include<Source\Component\Render3DComponent.h>
#include<Source\Component\Render2DComponent.h>
#include<Source\Device\Input\Input.h>
#include<Source\Device\Render\DirectXInstance.h>
#include<Source\Entity\Entity.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Resource\Texture\Texture2D.h>
#include<Source\Resource\Texture\RenderTarget.h>
#include<Source\Resource\Texture\MultiRenderTarget.h>
#include<Source\Resource\Texture\MSAARenderTarget.h>
#include<Source\Resource\Texture\MSAAMultiRenderTarget.h>
#include<Source\Component\SceneChangeComponent.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Util\Win\WinFunc.h>
#include<Source\Component\Physics\BulletRigidBody.h>
#include<Source\Component\ComponentInclude.h>
#include<Source\Util\WrapFunc.h>
#include<Source\Util\Type.h>
#include<Source\Util\Selector.h>
#include<Source\Util\btUtil.h>
#include<Source\Util\Timer\FPSTimer.h>
#include<Source\Util\Timer\ProcessTimer.h>
#include<Source\Util\Timer\Timer.h>
#include<Source\Util\Thread\IRunnable.h>
#include<Source\Util\Lua\LuaSetting.h>
#include<Source\Util\IO\OggLoader.h>
#include<resource.h>
#include<string>
#include<vector>
#include<list>
#include<Source\Resource\Model\Line.h>


#include<Source\Component\Effect\GradationEffect.h>
#include<Source\Util\ImGUI\ImGUI.h>
#include<Source\Util\Lua\LuaBase.h>
#include<Source\Util\Lua\LuaCpp.h>