#include"UpdateContainer.h"
#include<Source\Component\SceneChangeComponent.h>
#include<Source\Component\Physics\PhysicsWorld.h>


namespace framework {
	UpdateContainer::UpdateContainer()
	{
	}
	UpdateContainer::~UpdateContainer()
	{
	}
	void UpdateContainer::init()
	{
		//無効なウィーク削除
		TaskContainer::checkDelete();

		for (auto& task : m_Container) {
			if (task.expired())continue;
			task.lock()->setup();
		}

		m_pSceneChanger = framework::Entity::findGameObj("GlobalEvent").lock()->getComponent<component::SceneChangeComponent>();

		m_pWorld = framework::Entity::findGameObj("PhysicsWorld").lock()->getComponent<component::PhysicsWorld>();

	}
	void UpdateContainer::update()
	{
		checkDelete();

		callOrderSort();

		for (auto& task : m_Container) {
			if (task.expired())continue;
			task.lock()->update();
		}

		m_pWorld.lock()->physicsUpdate();

		m_pSceneChanger.lock()->changeScene();
	}
}
