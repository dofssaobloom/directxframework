#include"Render3DContainer.h"
#include<Source\Util\Event\WindowEvent.h>
#include<Source\Util\Render\DXFunc.h>
#include<Source\Resource\Texture\Texture2D.h>
#include<Source\Util\Math\Transform.h>
#include<Source\Resource\Texture\DepthTarget.h>
#include<Source\Resource\Texture\RenderTarget.h>
#include<Source\Device\Render\Renderer\2D\Single\SpriteRenderer.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Resource\Texture\MultiRenderTarget.h>
#include<Source\Application\Screen\Screen.h>
#include<Source\Device\Render\Shader\DeferredLighting.h>
#include<Source\Resource\Texture\CubeReanderTarget.h>
#include<Source\Resource\Texture\CubeDepthTarget.h>
#include<Source\Entity\Entity.h>
#include<Source\Resource\Texture\MSAAMultiRenderTarget.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Component\Effect\DirectionalLightComponent.h>
#include<Source\Device\Render\Renderer\3D\Instance\SkinningInstanceRenderer.h>
#include<Source\Util\Win\WinFunc.h>
#include<Source\Util\CSharp\SharpClass.h>
#include<Source\Device\Render\Renderer\Effekseer\EffectManager.h>
#include<Effekseer.h>
#include<EffekseerRendererDX11.h>
#include<Source\Util\Lua\LuaBase.h>
#include<Source\Component\Physics\PhysicsWorld.h>
#include <imgui.h>
#include<imgui_impl_dx11.h>

#include<Source\Util\ImGUI\ImGUI.h>

namespace framework {

	Render3DContainer::Render3DContainer()
		:m_ShadowSize(1920 * 2, 1080 * 2)
#ifdef _MDEBUG
		, m_IsColorBufferDraw(false)
#endif
	{
	}

	Render3DContainer::~Render3DContainer()
	{
	}

	void Render3DContainer::init()
	{
		//無効なウィーク削除
		TaskContainer::checkDelete();
		for (auto task : m_Container) {
			task.lock()->setup();
		}

		initBuffer();

		auto buffer = util::getBackBuffer();

		D3D11_TEXTURE2D_DESC desc;

		buffer->GetDesc(&desc);

		///m_pShadowMap = std::make_unique<RenderTarget>(m_ShadowSize.x, m_ShadowSize.y, DXGI_FORMAT_R8G8B8A8_UNORM);
		m_pShadowMap = std::make_unique<RenderTarget>(m_ShadowSize.x, m_ShadowSize.y, DXGI_FORMAT_R32_FLOAT);

		m_pShadowDepth = std::make_unique<DepthTarget>(m_ShadowSize.x, m_ShadowSize.y, DXGI_FORMAT_R32_TYPELESS);


		//ステンシルステートとラスタライザーは3D描画の初期化段階でセットしておく
		//TODO ラスタライザーはレンダラーごとに持たせる予定
		auto stencilState = util::createDepthStencilState();
		auto rs = util::createRasterizerSate(FALSE);
		m_pBlend = util::createBlendState();
		util::getContext()->RSSetState(rs);
		util::getContext()->OMSetDepthStencilState(stencilState.p, 0);
		util::setBlendState(m_pBlend.p);

		m_pShadowBuffer = std::make_shared<framework::Texture2D>(m_ShadowSize.x, m_ShadowSize.y, m_pShadowMap->getShaderView());

		ResourceManager::getInstance()->removeTexture("ShadowMap");
		ResourceManager::getInstance()->importTexture2D("ShadowMap", m_pShadowBuffer);

		util::WindowEvent::getInstance()->addEvent(ID_COLOR, [&](HWND hwnd) {
#ifdef _MDEBUG
			//カラーバッファを表示するだけのためにシャドウマップの描画したくないのでフラグでやる
			m_IsColorBufferDraw = true;
#endif // _MDEBUG
			//m_DrawCallBack = [&](std::weak_ptr<component::CameraComponent> currentCamera) {

				//最終的な描画結果を表示
				//currentCamera.lock()->gBufferDraw(0);
			//};
		});
		util::WindowEvent::getInstance()->addEvent(ID_NORMAL, [&](HWND hwnd) {
#ifdef _MDEBUG
			//カラーバッファを表示するだけのためにシャドウマップの描画したくないのでフラグでやる
			m_IsColorBufferDraw = false;
#endif // _MDEBUG
			m_DrawCallBack = [&](std::weak_ptr<component::CameraComponent> currentCamera) {
				//最終的な描画結果を表示
				currentCamera.lock()->gBufferDraw(1);
			};
		});

		util::WindowEvent::getInstance()->addEvent(ID_WORLD, [&](HWND hwnd) {
#ifdef _MDEBUG
			//カラーバッファを表示するだけのためにシャドウマップの描画したくないのでフラグでやる
			m_IsColorBufferDraw = false;
#endif // _MDEBUG
			m_DrawCallBack = [&](std::weak_ptr<component::CameraComponent> currentCamera) {
				//最終的な描画結果を表示
				currentCamera.lock()->gBufferDraw(2);
			};
		});

		util::WindowEvent::getInstance()->addEvent(ID_HEIGHT, [&](HWND hwnd) {
#ifdef _MDEBUG
			//カラーバッファを表示するだけのためにシャドウマップの描画したくないのでフラグでやる
			m_IsColorBufferDraw = false;
#endif // _MDEBUG
			m_DrawCallBack = [&](std::weak_ptr<component::CameraComponent> currentCamera) {
				//最終的な描画結果を表示
				currentCamera.lock()->gBufferDraw(3);
			};
		});

		util::WindowEvent::getInstance()->addEvent(ID_SHADOW, [&](HWND hwnd) {
#ifdef _MDEBUG
			//カラーバッファを表示するだけのためにシャドウマップの描画したくないのでフラグでやる
			m_IsColorBufferDraw = false;
#endif // _MDEBUG
			m_DrawCallBack = [&](std::weak_ptr<component::CameraComponent> currentCamera) {
				//最終的な描画結果を表示
				currentCamera.lock()->gBufferDraw(4);
			};
		});

		util::WindowEvent::getInstance()->addEvent(ID_ENVIRONEMT, [&](HWND hwnd) {
#ifdef _MDEBUG
			//カラーバッファを表示するだけのためにシャドウマップの描画したくないのでフラグでやる
			m_IsColorBufferDraw = false;
#endif // _MDEBUG
			m_DrawCallBack = [&](std::weak_ptr<component::CameraComponent> currentCamera) {
				//最終的な描画結果を表示
				currentCamera.lock()->gBufferDraw(5);
			};
		});

		util::WindowEvent::getInstance()->addEvent(ID_METALNESS, [&](HWND hwnd) {
#ifdef _MDEBUG
			//カラーバッファを表示するだけのためにシャドウマップの描画したくないのでフラグでやる
			m_IsColorBufferDraw = false;
#endif // _MDEBUG
			m_DrawCallBack = [&](std::weak_ptr<component::CameraComponent> currentCamera) {
				//最終的な描画結果を表示
				currentCamera.lock()->gBufferDraw(6);
			};
		});

		util::WindowEvent::getInstance()->addEvent(ID_SHADOW_MAP, [&](HWND hwnd) {
#ifdef _MDEBUG
			//カラーバッファを表示するだけのためにシャドウマップの描画したくないのでフラグでやる
			m_IsColorBufferDraw = false;
#endif // _MDEBUG
			m_DrawCallBack = [&](std::weak_ptr<component::CameraComponent> currentCamera) {
				m_pShadowMap->draw(util::Vec2(Screen::WINDOW_WIDTH_HALF, Screen::WINDOW_HEIGHT_HALF));
			};
		});

		util::WindowEvent::getInstance()->addEvent(ID_LIGHTING, [&](HWND hwnd) {
#ifdef _MDEBUG
			//カラーバッファを表示するだけのためにシャドウマップの描画したくないのでフラグでやる
			m_IsColorBufferDraw = false;
#endif // _MDEBUG
			m_DrawCallBack = [&](std::weak_ptr<component::CameraComponent> currentCamera) {
				currentCamera.lock()->finalBufferDraw();
			};
		});

		util::WindowEvent::getInstance()->addEvent(ID_VELOCITY, [&](HWND hwnd) {
#ifdef _MDEBUG
			//カラーバッファを表示するだけのためにシャドウマップの描画したくないのでフラグでやる
			m_IsColorBufferDraw = false;
#endif // _MDEBUG
			m_DrawCallBack = [&](std::weak_ptr<component::CameraComponent> currentCamera) {
				currentCamera.lock()->gBufferDraw(7);
			};
		});

		//デフォルトでライティング
		m_DrawCallBack = [&](std::weak_ptr<component::CameraComponent> currentCamera) {
			//最終的な描画結果を表示
			currentCamera.lock()->finalBufferDraw();
		};

		util::WindowEvent::getInstance()->addEvent(VK_F2, [&](HWND hwnd) {
			resizeBuffer(util::Vec2( 1920,1080 ));
		});

		m_pWorld = framework::Entity::findGameObj("PhysicsWorld").lock()->getComponent<component::PhysicsWorld>();

		//テスト
		//std::vector<LineVertexLayout> pointList;
		//pointList.emplace_back(LineVertexLayout(util::Vec3(0, 0, 0), util::Vec3(0, 1, 0), util::Vec3(1, 0, 0)));
		//pointList.emplace_back(LineVertexLayout(util::Vec3(1, 0, 0), util::Vec3(0, 1, 0), util::Vec3(0, 1, 0)));
		//pointList.emplace_back(LineVertexLayout(util::Vec3(2, 0, 0), util::Vec3(0, 1, 0), util::Vec3(0, 1, 0)));
		//pointList.emplace_back(LineVertexLayout(util::Vec3(3, 0, 0), util::Vec3(0, 1, 0), util::Vec3(0, 0, 1)));
		//pointList.emplace_back(LineVertexLayout(util::Vec3(4, 0, 0), util::Vec3(0, 1, 0), util::Vec3(0, 0, 1)));
		//pointList.emplace_back(LineVertexLayout(util::Vec3(5, 0, 0), util::Vec3(0, 1, 0), util::Vec3(1, 0, 0)));

		//pointList.emplace_back(LineVertexLayout(util::Vec3(0, 0, 0), util::Vec3(0, 1, 0), util::Vec3(1, 0, 0)));
		//pointList.emplace_back(LineVertexLayout(util::Vec3(1, 0, 0), util::Vec3(0, 1, 0), util::Vec3(0, 1, 0)));
		//pointList.emplace_back(LineVertexLayout(util::Vec3(1, 0, 0), util::Vec3(0, 1, 0), util::Vec3(0, 0, 1)));
		//pointList.emplace_back(LineVertexLayout(util::Vec3(1, 0, 0), util::Vec3(0, 1, 0), util::Vec3(0, 0, 1)));
		//pointList.emplace_back(LineVertexLayout(util::Vec3(2, 0, 0), util::Vec3(0, 1, 0), util::Vec3(1, 0, 0)));
		//pointList.emplace_back(LineVertexLayout(util::Vec3(2, 0, 0), util::Vec3(0, 1, 0), util::Vec3(1, 0, 0)));
		//pointList.emplace_back(LineVertexLayout(util::Vec3(2, 0, 0), util::Vec3(0, 1, 0), util::Vec3(1, 0, 0)));
		//pointList.emplace_back(LineVertexLayout(util::Vec3(3, 0, 0), util::Vec3(0, 1, 0), util::Vec3(1, 0, 0)));


		//m_pLineRenderer = std::make_unique<framework::LineRenderer>(pointList);
		//m_pLineRenderer->setWidth(50);


		//backBuffer.Release();
		//m_pTarget.reset();
		//m_pDepthTarget.reset();
		//m_pShadowMap.reset();
		//m_pShadowDepth.reset();


	//	framework::DirectXInstance::getInstance()->resizeBuffer(util::Vec2(1920, 1080));
	}

	void Render3DContainer::draw()
	{
		//最小化などして描画領域がない場合は描画しない
		HRESULT hr = framework::DirectXInstance::getInstance()->getSwapChain()->Present(0, DXGI_PRESENT_TEST);
		if (hr == DXGI_STATUS_OCCLUDED) return;
		util::setBlendState(m_pBlend.p);

		//タスク削除
		checkDelete();

		//現在のメインカメラを一時退避させる
		auto currentCamera = component::CameraComponent::getMainCamera();

#ifdef _MDEBUG
		if (m_IsColorBufferDraw) {
			util::setSingleViewPort(Screen::PIXEL_WIDTH, Screen::PIXEL_HEIGHT);
			util::getContext()->OMSetRenderTargets(1, m_pTarget->getView(), m_pDepthTarget->getView());
			m_pTarget->clear(util::Vec4(0.024, 0.285, 0.35, 1));
			m_pDepthTarget->clear();
			//TODO ここにポスト処理を加える
			gBufferDraw();

			m_pWorld.lock()->draw();

			EffectManager::getInstance()->setViewMat(currentCamera.lock()->toViewMatrix());
			EffectManager::getInstance()->draw();
			return;
		}
#endif 

		shadowMapDoraw();

		if (!currentCamera.expired()) {
			//カメラをもとに戻す
			currentCamera.lock()->active();
		}

		//GBufferの描画
		currentCamera.lock()->beginToBuffer(m_pDepthTarget);

		gBufferDraw();

		m_pWorld.lock()->draw();

		currentCamera.lock()->endToBuffer();

		//ライティング処理
		currentCamera.lock()->doLighting();

		currentCamera.lock()->doEffect();

		util::setSingleViewPort(Screen::PIXEL_WIDTH, Screen::PIXEL_HEIGHT);
		util::getContext()->OMSetRenderTargets(1, m_pTarget->getView(), NULL);
		m_pTarget->clear(util::Vec4(0.024, 0.285, 0.35, 1));

		//TODO ここにポスト処理を加える
		m_DrawCallBack(currentCamera);

		util::getContext()->OMSetRenderTargets(1, m_pTarget->getView(), m_pDepthTarget->getView());

		EffectManager::getInstance()->setViewMat(currentCamera.lock()->toViewMatrix());
		EffectManager::getInstance()->draw();


	}

	void Render3DContainer::removeTask(std::weak_ptr<Render3DComponent> task)
	{
		TaskContainer::removeTask(task);
	}

	void Render3DContainer::resizeBuffer(const util::Vec2 & size)
	{
		m_pTarget.reset();

		framework::DirectXInstance::getInstance()->resizeBuffer(size);

		initBuffer();
	}

	void Render3DContainer::gBufferDraw()
	{
		//ビューポートをスクリーンサイズに設定
		util::setSingleViewPort(Screen::PIXEL_WIDTH, Screen::PIXEL_HEIGHT);

		for (auto task : m_Container) {
			task.lock()->draw();
		}

		//m_pLineRenderer->draw(&util::Transform(util::Vec3(0, -300, 0), util::Vec3(0, 0, 0), util::Vec3(100, 100, 100)));
	}

	void Render3DContainer::shadowMapDoraw()
	{
		auto shadowMapCamera = component::DirectionalLightComponent::getCamera();
		if (shadowMapCamera.expired())return;
		shadowMapCamera.lock()->active();

		util::setSingleViewPort(m_ShadowSize.x, m_ShadowSize.y);
		util::getContext()->OMSetRenderTargets(1, m_pShadowMap->getView(), m_pShadowDepth->getView());
		m_pShadowMap->clear(util::Vec4(0, 0, 0, 1));
		m_pShadowDepth->clear();

		for (auto task : m_Container) {
			task.lock()->depthDraw();
		}

		shadowMapCamera.lock()->deActive();
	}

	void Render3DContainer::checkDelete()
	{
		m_Container.remove_if([&](std::weak_ptr<Render3DComponent> _task) {
			return _task.expired();
		});

		for (auto& render : m_Container)
		{
			render.lock()->cheackDelete();
		}

		//削除対象をデリート
		for (auto deleteTask : m_DeleteContainer) {
			m_Container.remove_if([&](std::weak_ptr<Render3DComponent> _task) {
				////同じポインタを削除
				//return deleteTask.lock().get() == _task.lock().get();
				if (deleteTask.lock().get() == _task.lock().get()) {
					return true;
				}
				return false;
			});
		}
		m_DeleteContainer.clear();

		//for (auto& deleteTask : m_Container) {
		//	if (deleteTask.lock()->drawNum() < 1)
		//		deleteTask.lock()->deActive();
		//}
	}

	void Render3DContainer::initBuffer()
	{
		CComPtr<ID3D11Texture2D> backBuffer = util::getBackBuffer();
		m_pTarget = std::make_unique<framework::RenderTarget>(backBuffer, DXGI_FORMAT_R8G8B8A8_UNORM);

		m_pDepthTarget = std::make_shared<DepthTarget>(Screen::PIXEL_WIDTH, Screen::PIXEL_HEIGHT, DXGI_FORMAT_R32_TYPELESS);
	}
}