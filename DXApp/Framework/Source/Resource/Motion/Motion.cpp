#include"Motion.h"
#include<atlbase.h>
#include<assert.h>
#include<Source\Util\IO\FbxLoader.h>
#include<Source\Resource\Model\Model.h>
#include<Source\Device\Render\Renderer\3D\RenderDefine.h>
#include<Source\Resource\Texture\BoneTexture.h>
#include<Source\Util\Thread\CriticalSection.h>
#include<Source\Util\IO\BinaryLoader.h>

namespace framework {
	Motion::Motion()
	{
	}
	Motion::Motion(const std::string& filePath)
		:Motion()
	{
		//拡張子取得
		auto extension = util::extensionName(filePath);

		if (extension == "fbx") {
			readFBX(filePath);
		}
		else {
			readBinary(filePath);
		}
	}

	Motion::~Motion()
	{
	}

	void Motion::clear()
	{
		m_MotionMatWithName.clear();
	}

	std::vector<util::Mat4> Motion::getFrameMotion(int frame)
	{
		std::vector<util::Mat4> result;

		result.reserve(m_MotionMatWithName[frame].size());
		for (auto boneMat : m_MotionMatWithName[frame]) {
			result.emplace_back(boneMat.mat);
		}

		return result;
	}

	std::vector<util::Mat4> Motion::getFrameMotionWithName(const std::string & name)
	{
		//フレーム数分の行列
		std::vector<util::Mat4> result;

		for (int i = 0; i < endFrame; i++)
		{
			auto& boneList = m_MotionMatWithName[i];
			auto find = std::find_if(boneList.begin(), boneList.end(), [&](BoneMatWithName& boneMat) {
				return boneMat.name == name;
			});

			if (find != boneList.end())
				result.emplace_back(find->mat);
		}

		//result.reserve(m_MotionMatWithName[frame].size());
		//for (auto boneMat : m_MotionMatWithName[frame]) {
		//	if (boneMat.name == name) {
		//		result.emplace_back(boneMat.mat);
		//	}
		//}

		return result;
	}

	util::Mat4 Motion::getBoneInitMat(const std::string & name)
	{
		auto find = std::find_if(m_InitMat.begin(), m_InitMat.end(), [&](BoneMatWithName& boneMat) {
			return boneMat.name == name;
		});
		if (find == m_InitMat.end())
			return util::Mat4();

		return find->mat;
	}

	int Motion::getMaxFrame()
	{
		return endFrame;
	}

	void Motion::readFBX(const std::string & filePath)
	{
		util::FbxLoader loader;
		loader.loadFBX(filePath);
		m_MotionMatWithName = loader.getMotionMatrix();
		endFrame = m_MotionMatWithName.size();
		assert(m_MotionMatWithName.size() <= MAX_MOTION_FRAME && "アニメーションサイズが大きすぎます");
		if (m_MotionMatWithName.size() < MAX_MOTION_FRAME) {
			m_MotionMatWithName.resize(MAX_MOTION_FRAME);
		}
		m_InitMat = loader.getBoneInitMatrix();
	}

	void Motion::readBinary(const std::string & filePath)
	{
		util::CriticalSection c;
		util::BinaryLoader loader(filePath);

		m_MotionMatWithName.resize(MAX_MOTION_FRAME);
		loader.load<int>(&endFrame);
		for (int i = 0; i < endFrame; i++)
		{
			std::vector<BoneMatWithName> motinos;
			int boneNum;
			loader.load<int>(&boneNum);
			motinos.reserve(boneNum);


			for (int j = 0; j < boneNum; j++)
			{
				int nameSize;
				loader.load<int>(&nameSize);
				std::vector<char> buf;
				buf.reserve(nameSize);
				loader.load<char>(nameSize, &buf);

				//ストリングに変換
				std::string name;
				for (auto c : buf)
				{
					name.push_back(c);
				}

				//モーション取得
				util::Mat4 motionMat;
				loader.load<util::Mat4>(&motionMat);


				motinos.emplace_back(BoneMatWithName(name, motionMat));
			}

			m_MotionMatWithName[i] = motinos;
		}

		int boneNum;
		loader.load<int>(&boneNum);

		for (int i = 0; i < boneNum; i++)
		{
			int nameSize;
			loader.load<int>(&nameSize);
			std::vector<char> buf;
			buf.reserve(nameSize);
			loader.load<char>(nameSize, &buf);

			//ストリングに変換
			std::string name;
			for (auto c : buf)
			{
				name.push_back(c);
			}

			util::Mat4 motionMat;
			loader.load<util::Mat4>(&motionMat);
			m_InitMat.emplace_back(BoneMatWithName(name, motionMat));
		}

	}
}