#pragma once

#include<D3DX11.h>
#include<Source\Util\Type.h>
#include<Source\Util\Render\DXFunc.h>
#include<Source\Util\WrapFunc.h>
#include<vector>
#include<unordered_map>
#include<string>
#include<Source\Resource\Texture\BoneTexture.h>
#include<memory>

/**
* @file	    Motion.h
* @brief	モーション構造体
* @detail   Motionは同じボーンから作られていないといけない
* @authro	高須優輝
* @date	2017/05/04
*/

namespace framework {

	struct BoneMatWithName
	{
		BoneMatWithName() {

		}

		BoneMatWithName(const std::string& name, const util::Mat4& mat) {
			this->name = name;
			this->mat = mat;
		}

		std::string name;
		util::Mat4 mat;
	};


	class BoneTexture;
	
	class Motion
	{
	public:
		Motion();
		Motion(const std::string& filePath);
		~Motion();
		/**
		* @brief		 すべてのモーションを削除する
		*/
		void clear();

		std::vector<util::Mat4> getFrameMotion(int frame);

		std::vector<util::Mat4> getFrameMotionWithName(const std::string& name);

		/// <summary>
		/// ボーンの初期姿勢を取得
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		util::Mat4 getBoneInitMat(const std::string& name);

		int getMaxFrame();

	private:
		/// <summary>
		/// FBX読み込み
		/// </summary>
		/// <param name="filePath"></param>
		void readFBX(const std::string& filePath);

		/// <summary>
		/// バイナリ読み込み
		/// </summary>
		/// <param name="filePath"></param>
		void readBinary(const std::string& filePath);
	private:
		//!姿勢行列
		//バイナリ読み込みの場合は空になる
		std::vector<std::vector<BoneMatWithName>> m_MotionMatWithName;
		std::vector<BoneMatWithName> m_InitMat;
		int endFrame;
	};

}