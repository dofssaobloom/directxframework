#pragma once
#include<dsound.h>


namespace framework {
	/*
	DirectSoundクラス
	*/
	class DirectSound
	{
	public:

		//コンストラクタ
		DirectSound();

		//デストラクタ
		~DirectSound();

		//サウンドデバイスの作成
		bool create(HWND& hWnd);

	public:
		//DirectSoundデバイス
		CComPtr<IDirectSound8>	m_directSound8;
	};
}