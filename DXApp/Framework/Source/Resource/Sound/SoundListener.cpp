#include "SoundListener.h"
#include "SoundListener.h"
#include<assert.h>


namespace framework {

	SoundListener::SoundListener(std::shared_ptr<DirectSound> device):m_pDirectSound(device)
	{
	}

	SoundListener::~SoundListener()
	{
	}

	bool SoundListener::createSoundListener()
	{
		HRESULT result;
		DSBUFFERDESC bufferDesc;

		//プライマリバッファの説明を設定します
		bufferDesc.dwSize = sizeof(DSBUFFERDESC);
		bufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER | DSBCAPS_CTRL3D; //| DSBCAPS_GLOBALFOCUS;
		bufferDesc.dwBufferBytes = 0;
		bufferDesc.dwReserved = 0;
		bufferDesc.lpwfxFormat = NULL;
		bufferDesc.guid3DAlgorithm = GUID_NULL;

		IDirectSoundBuffer *pPrimaryBuffer;
		//デフォルトサウンドデバイスのプライマリサウンドバッファのコントロールを取得します
		result = m_pDirectSound->m_directSound8->CreateSoundBuffer(&bufferDesc, &pPrimaryBuffer, NULL);
		if (FAILED(result))
		{
			return false;
		}

		IDirectSound3DListener8* plistener;
		//リスナーインターフェイスの取得
		result = pPrimaryBuffer->QueryInterface(IID_IDirectSound3DListener8, (LPVOID*)&plistener);
		if (FAILED(result))
		{
			return false;
		}

		m_primaryBuffer.Attach(pPrimaryBuffer);
		m_listener.Attach(plistener);

		return true;
	}

	void SoundListener::setDistance(float listenDistance)
	{
		m_listener->SetDistanceFactor(listenDistance * 0.01f, DS3D_IMMEDIATE);
	}

	void SoundListener::setDoppler(float doppler)
	{
		m_listener->SetDopplerFactor(doppler, DS3D_IMMEDIATE);
	}

	void SoundListener::setPosition(const util::Transform & position)
	{
		m_listener->SetPosition(position.m_Position.x, position.m_Position.y, position.m_Position.z, DS3D_IMMEDIATE);
	}

	void SoundListener::setDopplerVelocity(const util::Vec3 & speed)
	{
		m_listener->SetVelocity(speed.x, speed.y, speed.z, DS3D_IMMEDIATE);
	}

	void SoundListener::setDirection(const util::Vec3 & flont)
	{
		//リスナーの方向
		m_listener->SetOrientation(flont.x, flont.y, flont.z, 0.0f, 1.0f, 0.0f, DS3D_IMMEDIATE);
	}

	void SoundListener::setRollOff(float decay)
	{
		//ロールオフ(音の減衰度合い)0~10
		m_listener->SetRolloffFactor(decay, DS3D_IMMEDIATE);
	}
}
