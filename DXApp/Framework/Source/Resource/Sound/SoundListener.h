#pragma once
#include<dsound.h>
#include<atlbase.h>
#include<string>
#include<list>
#include<Source\Resource\Sound\DirectSound.h>

namespace framework {

	class SoundListener
	{
	public:
		SoundListener(std::shared_ptr<DirectSound> device);
		~SoundListener();

	public:
		//リスナーの作成
		bool createSoundListener();

	public:

		//距離要素の設定(メートルをどの単位にするか)
		void setDistance(float listenDistance);

		/// <summary>
		/// ドップラー効果の設定(0~10)
		/// </summary>
		/// <param name="doppler">0〜10(0,ドップラー効果なし|1,標準|1以降,実世界の倍効果)</param>
		void setDoppler(float doppler);

		//リスナーの位置
		void setPosition(const util::Transform & position);

		//速度(ドップラー効果の計算時に必要)
		void setDopplerVelocity(const util::Vec3 & speed);

		//リスナーの方向
		void setDirection(const util::Vec3& flont);

		//音の減衰 0~10 2以上実世界ロールオフ効果の倍
		void setRollOff(float decay);

	private:
		//!サウンドデバイス
		std::shared_ptr<DirectSound> m_pDirectSound;

		CComPtr<IDirectSoundBuffer> m_primaryBuffer;

		CComPtr<IDirectSound3DListener8> m_listener;
	};
}