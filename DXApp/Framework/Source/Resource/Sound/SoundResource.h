#pragma once
#include<dsound.h>
#include<atlbase.h>
#include<string>
#include<list>

#include<Source\Resource\Sound\SoundListener.h>

namespace util {
	class OggLoader;
}

namespace framework {

	class SoundResource
	{
	public:
		SoundResource(IDirectSound8* device,const std::string& filePath);
		~SoundResource();

		/**
		* @brief		  音源再生
		*/
		void play(bool isLoop = false);

		/**
		* @brief		 　音源ストップ
		*/
		void stop();

		/**
		* @brief		 　音量変更
		*/
		void setVolume(int const& volume);

		//音源位置
		void setPosition(const util::Transform & position);

		//音の速度(ドップラー効果の計算時に必要)
		void setDopplerVelocity(const util::Vec3 & speed);

		//最短距離と最長距離の設定(音の届く距離)
		void setDistance(float min, float max);

		//3Dサウンド効果のON/OFF
		void setDoppler(bool doppler);

		bool isEnd();

	private:
		/**
		* @brief		  セカンダリバッファ作成
		*/
		void createSecondaryBffer(IDirectSound8* device,const DWORD decodeSize ,WAVEFORMATEX& fomat);
	
		/**
		* @brief		  データをデコード
		*/
		void decode(util::OggLoader& loader);

		CComPtr<IDirectSoundBuffer8> getSecoundBuffer(IDirectSoundBuffer *sb);

		/**
		* @brief		  再生の終わっているものをリストから消す
		*/
		void checkDelete();

	private:
		//デバイス
		IDirectSound8* m_pDevice;

		//!セカンダリバッファ
		CComPtr<IDirectSoundBuffer8> m_SecondBuffer;

		//コピーバッファ
		std::list< CComPtr<IDirectSoundBuffer8>> m_CopyInstance;

		//3D音源
		CComPtr<IDirectSound3DBuffer8> m_secondary3DBuffer1;
	};
}