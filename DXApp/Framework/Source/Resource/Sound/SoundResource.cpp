#include"SoundResource.h"
#include<Source\Util\IO\OggLoader.h>
#include<assert.h>

namespace framework {

	SoundResource::SoundResource(IDirectSound8* device, const std::string& filePath)
		:m_pDevice(device)
	{
		util::OggLoader loader(filePath);

		auto waveFormat = loader.getFormat();

		const DWORD decodeSize = loader.getDecodeSize();

		//セカンダリバッファを作成する
		createSecondaryBffer(device, decodeSize, waveFormat);

		decode(loader);
	}

	SoundResource::~SoundResource()
	{
	}

	void SoundResource::play(bool isLoop)
	{
		checkDelete();
		//再生中か確認
		if (isEnd()) {

			auto loopFlag = isLoop ? DSBPLAY_LOOPING : 0;

			m_SecondBuffer->Play(0, 0, loopFlag);

		}
		else {
			//コピーして再生
			IDirectSoundBuffer *sSound;
			m_pDevice->DuplicateSoundBuffer(m_SecondBuffer.p, &sSound);

			auto copy = getSecoundBuffer(sSound);
			//仲介用のサウンドバッファーオブジェクトは要らないので開放
			sSound->Release();
			auto loopFlag = isLoop ? DSBPLAY_LOOPING : 0;

			LONG volume = 0;
			m_SecondBuffer->GetVolume(&volume);
			//少し値を変えないと音量がMAXになるバグがあるので少し変える
			volume -= 1;

			copy->SetVolume(volume);

			//コピーしたインスタンス再生
			copy->Play(0,0, loopFlag);
			m_CopyInstance.emplace_back(copy);
		}
	}

	void SoundResource::stop()
	{
		m_SecondBuffer->Stop();
	}

	void SoundResource::setVolume(int const& volume)
	{
		m_SecondBuffer->SetVolume(volume);
	}

	void SoundResource::setPosition(const util::Transform & position)
	{
		m_secondary3DBuffer1->SetPosition(position.m_Position.x, position.m_Position.y, position.m_Position.z, DS3D_IMMEDIATE);
	}

	void SoundResource::setDopplerVelocity(const util::Vec3 & speed)
	{
		m_secondary3DBuffer1->SetVelocity(speed.x, speed.y, speed.z, DS3D_IMMEDIATE);
	}

	void SoundResource::setDistance(float min, float max)
	{
		//センチメートル
		m_secondary3DBuffer1->SetMinDistance(min , DS3D_IMMEDIATE);
		m_secondary3DBuffer1->SetMaxDistance(max , DS3D_IMMEDIATE);
	}

	void SoundResource::setDoppler(bool doppler)
	{
		if (doppler) 
		{
			m_secondary3DBuffer1->SetMode(DS3DMODE_NORMAL, DS3D_IMMEDIATE);
		}
		else 
		{
			m_secondary3DBuffer1->SetMode(DS3DMODE_DISABLE, DS3D_IMMEDIATE);
		}
	}

	bool SoundResource::isEnd()
	{
		//checkDelete();

		DWORD status = 0;

		m_SecondBuffer->GetStatus(&status);

		//再生されていたら
		if ((status & DSBSTATUS_PLAYING) == 1) {
			return false;
		}

		if (!m_CopyInstance.empty())return false;
		//コピーの中に再生されているものが一つでも居たらfalse
		//for (auto& copy : m_CopyInstance)
		//{
		//	copy->GetStatus(&status);
		//	if ((status & DSBSTATUS_PLAYING) == 1) {
		//		return false;
		//	}
		//}

		return true;
	}

	void SoundResource::createSecondaryBffer(IDirectSound8 * device, const DWORD decodeSize, WAVEFORMATEX& format)
	{
		// プライマリバッファの作成
		DSBUFFERDESC desc;
		desc.dwSize = sizeof(DSBUFFERDESC);
		//desc.dwFlags = DSBCAPS_CTRLPAN | DSBCAPS_CTRLVOLUME | DSBCAPS_CTRLFREQUENCY | DSBCAPS_GLOBALFOCUS;
		desc.dwFlags = DSBCAPS_CTRLVOLUME | DSBCAPS_CTRL3D | DSBCAPS_GLOBALFOCUS;
		desc.dwBufferBytes = decodeSize;
		desc.dwReserved = 0;
		desc.lpwfxFormat = &format;
		desc.guid3DAlgorithm = DS3DALG_DEFAULT;
		//desc.guid3DAlgorithm = DS3DALG_NO_VIRTUALIZATION;

		IDirectSoundBuffer *pPrimaryBuffer;
		bool isError = device->CreateSoundBuffer(&desc, &pPrimaryBuffer, NULL);
		assert(!isError && "プライマリバッファ作成失敗");

		IDirectSoundBuffer8* pSecondaryBuffer;
		isError = pPrimaryBuffer->QueryInterface(IID_IDirectSoundBuffer8, reinterpret_cast<LPVOID*>(&pSecondaryBuffer));
		assert(!isError && "セカンダリバッファの作成に失敗しました");

		IDirectSound3DBuffer8* pSecondary3DBuffer;
		//セカンダリサウンドバッファに3Dインターフェイスを取得します
		pSecondaryBuffer->QueryInterface(IID_IDirectSound3DBuffer8, (void**)&pSecondary3DBuffer);

		//メンバに渡す
		m_SecondBuffer.Attach(pSecondaryBuffer);
		m_secondary3DBuffer1.Attach(pSecondary3DBuffer);
		pPrimaryBuffer->Release();
	}

	void SoundResource::decode(util::OggLoader & loader)
	{
		//!書き込み先バッファ
		LPVOID pWriteBuffer;
		DWORD lockSize = 0;
		DWORD totalSize = 0;

		bool isError = m_SecondBuffer->Lock(0, 0, &pWriteBuffer, &lockSize, NULL, NULL, DSBLOCK_ENTIREBUFFER);
		assert(!isError && "バッファのロックに失敗しました");

		while (!loader.readSound(&totalSize, pWriteBuffer));

		isError = m_SecondBuffer->Unlock(pWriteBuffer, lockSize,NULL,0);
		assert(!isError && "バッファのアンロックに失敗しました");

	}

	//バッファーからインスタンス取得
	CComPtr<IDirectSoundBuffer8> SoundResource::getSecoundBuffer(IDirectSoundBuffer * sb)
	{
		IDirectSoundBuffer8* pSecondaryBuffer;
		bool isError = sb->QueryInterface(IID_IDirectSoundBuffer8, reinterpret_cast<LPVOID*>(&pSecondaryBuffer));
		assert(!isError && "セカンダリバッファの作成に失敗しました");

		//メンバに渡す
		m_SecondBuffer.Attach(pSecondaryBuffer);
		return m_SecondBuffer;
	}

	void SoundResource::checkDelete()
	{
		m_CopyInstance.remove_if([](CComPtr<IDirectSoundBuffer8>& copy) {
			if (copy == nullptr)return true;
			DWORD status = 0;
			copy->GetStatus(&status);
			//終わっているものが消される
			return (status & DSBSTATUS_PLAYING) == 0;
		});


	}

}