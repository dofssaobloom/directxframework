#include "Framework.h"
#include "DirectSound.h"


namespace framework {
	DirectSound::DirectSound() 
	{
	}

	DirectSound::~DirectSound()
	{
	}

	bool DirectSound::create(HWND & hWnd)
	{
		IDirectSound8*	pdirectSound8;

		//サウンドデバイス作成
		DirectSoundCreate8(NULL, &pdirectSound8, NULL);

		//協調レベルのセット（ここで音を鳴らすウィンドウを指定する必要がある）
		if (FAILED(pdirectSound8->SetCooperativeLevel(hWnd, DSSCL_PRIORITY)))
			return false; //失敗

		m_directSound8.Attach(pdirectSound8);

		return true;
	}
}
