#pragma once
#include<Source\Util\Render\DXFunc.h>
#include"Model.h"

namespace framework {
	//剛体メッシュよう頂点データ
	struct LineVertexLayout
	{
		LineVertexLayout(){
		}

		LineVertexLayout(util::Vec3& pos, util::Vec3& normal, util::Vec3& color)
			:position(pos),
			normal(normal),
			color(color)
		{
		}


		//!頂点座標
		util::Vec3 position;
		//!法線
		util::Vec3 normal;
		//!色
		util::Vec3 color;
	};


	struct Line : public Model
	{
		/**
		* @brief	基底コンストラクタ
		*/
		Line() {
		}
		/**
		* @brief	コンストラクタ
		* @detail	読み込み
		*/
		Line(std::vector<LineVertexLayout>& pointList) {

		
/*
			std::vector<int> index;
			index.resize(pointList.size() * 2);

			int count = 0;
			util::foreach(pointList.size(), [&](int i) {
				index[count] = i;
				index[count + 1] = i + 1;
				count += 2;
			});
*/
			
			std::vector<int> index;
			index.resize(pointList.size() * 2);

			int count = 0;
			util::foreach(pointList.size(), [&](int i) {
				index[count] = i;
				index[count + 1] = i + 1;
				count += 2;
			});

			index.erase(index.end()-1);

			this->vertexNum = pointList.size();

			this->indexNum = index.size();

			this->mesh.vertexBuffer = util::createVertexBuffer(sizeof(LineVertexLayout) * this->vertexNum, pointList.data());
			this->mesh.indexBuffer = util::createIndexBuffer(sizeof(int) * this->indexNum, index.data());
		}
	};

}