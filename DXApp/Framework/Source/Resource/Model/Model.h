#pragma once

#include<D3DX11.h>
#include<atlbase.h>
#include<Source\Resource\Texture\Texture2D.h>
#include<memory>
#include<Source\Util\Type.h>
#include<string>
#include<Source\Util\IO\FbxLoader.h>
#include<Source\Util\Render\DXFunc.h>
#include<Source\Util\WrapFunc.h>
#include<Source\Util\Template\Template.h>
#include<Source\Util\Thread\CriticalSection.h>
#include<Source\Util\IO\BinaryLoader.h>


#define BONE_NUM 64

namespace framework {

	enum class ModelType
	{
		rigid,
		skin,
	};

	//剛体メッシュよう頂点データ
	struct RigidVertexLayout
	{
		//!頂点座標
		util::Vec3 position;
		//!法線
		util::Vec3 normal;
		//!バイノーマル
		util::Vec3 binormal;
		//!バイノーマル
		util::Vec3 tangent;
		//!テクスチャ座標
		util::Vec2 uv;

		bool operator == (const RigidVertexLayout& v) const
		{
			return std::memcmp(this, &v, sizeof(RigidVertexLayout)) == 0;
		}
	};

	//剛体メッシュ
	struct Mesh
	{
		CComPtr<ID3D11Buffer> vertexBuffer;
		CComPtr<ID3D11Buffer> indexBuffer;
	};

	struct Model {
		Mesh mesh;
		int vertexNum = 0;
		int indexNum = 0;
		ModelType type = ModelType::rigid;
	};

	struct RigidModel : public Model
	{
		/**
		* @brief	基底コンストラクタ
		*/
		RigidModel() {
		}
		/**
		* @brief	コンストラクタ
		* @detail	読み込み
		*/
		RigidModel(const std::string& filePath) {

			//拡張子取得
			auto extension = util::extensionName(filePath);

			if (extension == "fbx") {
				readFBX(filePath);
			}
			else {
				readBinary(filePath);
			}


		}


	private:
		void readFBX(const std::string& fbxFilePath) {
			util::FbxLoader loader;
			loader.loadFBX(fbxFilePath);

			auto meshCount = loader.getMeshCount();
			auto vertex = loader.getVertex();
			auto uv = loader.getUV();
			auto normal = loader.getNormal();
			auto index = loader.getIndex();
			auto binroaml = loader.getBinormal();
			auto tangent = loader.getTangent();

			std::vector<int> totalIndex;
			util::foreach(meshCount, [&](int mesh) {
				util::foreach(index[mesh].size(), [&](int i) {
					totalIndex.emplace_back(index[mesh][i]);
				});
			});

			std::vector<RigidVertexLayout> data;
			util::foreach(meshCount, [&](int mesh) {
				//インデックス順のものを頂点順に変換
				for (int i = 0, end = index[mesh].size(); i != end; ++i) {
					RigidVertexLayout layout;

					layout.position = vertex[mesh][index[mesh][i]];
					layout.normal = normal[mesh][i];
					layout.binormal = binroaml[mesh][i];
					layout.tangent = tangent[mesh][i];
					layout.uv = uv[mesh][i];

					data.emplace_back(layout);
				}
			});

			std::vector<RigidVertexLayout> finalVertex;
			finalVertex.reserve(data.size());
			std::vector<int> finalIndex;
			finalIndex.reserve(totalIndex.size());

			for (auto& v : data)
			{
				auto find = std::find(finalVertex.begin(), finalVertex.end(), v);

				if (find == finalVertex.end()) {
					//重複していない
					finalIndex.emplace_back(finalVertex.size());
					finalVertex.emplace_back(v);
				}
				else {
					//重複している
					auto index = std::distance(finalVertex.begin(), find);
					finalIndex.emplace_back(index);
				}
			}


			this->vertexNum = finalVertex.size();
			this->indexNum = finalIndex.size();

			this->mesh.vertexBuffer = util::createVertexBuffer(sizeof(RigidVertexLayout) * this->vertexNum, finalVertex.data());
			this->mesh.indexBuffer = util::createIndexBuffer(sizeof(int) * this->indexNum, finalIndex.data());
		}

		void readBinary(const std::string& binaryFilePath) {
			util::CriticalSection c;
			util::BinaryLoader loader(binaryFilePath);

			std::vector<RigidVertexLayout> vBuffer;
			loader.load<int>(&vertexNum);
			loader.load<RigidVertexLayout>(vertexNum, &vBuffer);

			std::vector<int> iBuffer;
			loader.load<int>(&indexNum);
			loader.load<int>(indexNum, &iBuffer);

			this->mesh.vertexBuffer = util::createVertexBuffer(sizeof(RigidVertexLayout) * this->vertexNum, vBuffer.data());
			this->mesh.indexBuffer = util::createIndexBuffer(sizeof(int) * this->indexNum, iBuffer.data());
		}
	};

	//スキンメッシュよう頂点データ(仮)
	struct SkinningVertexLayout
	{
		//!頂点座標
		util::Vec3 position;
		//!法線
		util::Vec3 normal;
		//!バイノーマル
		util::Vec3 binormal;
		//!バイノーマル
		util::Vec3 tangent;
		//!テクスチャ座標
		util::Vec2 uv;
		//!ボーンのid
		int boneID[4] = { 0 };
		//!ボーンの影響度
		float weights[4] = { 0 };
		//!メッシュ番号
		int meshID;

		bool operator == (const SkinningVertexLayout& v) const
		{
			return std::memcmp(this, &v, sizeof(SkinningVertexLayout)) == 0;
		}
	};


	struct SkinningModel : public Model
	{
		struct BoneAndWeight {
			int boneID[4];
			float wight[4];
		};

		/**
		* @brief	基底コンストラクタ
		*/
		SkinningModel() {
		}
		/**
		* @brief	コンストラクタ
		* @detail	読み込み
		*/
		SkinningModel(const std::string& filePath) {

			//拡張子取得
			auto extension = util::extensionName(filePath);

			if (extension == "fbx") {
				readFBX(filePath);
			}
			else {
				readBinary(filePath);
			}
		}

	private:

		void readFBX(const std::string& filePath) {
			util::FbxLoader loader;
			loader.loadFBX(filePath);

			auto meshCount = loader.getMeshCount();
			auto vertex = loader.getVertex();
			auto uv = loader.getUV();
			auto normal = loader.getNormal();
			auto index = loader.getIndex();
			auto binroaml = loader.getBinormal();
			auto tangent = loader.getTangent();
			auto weightList = loader.getBoneWeight();

			std::vector<int> totalIndex;
			util::foreach(meshCount, [&](int mesh) {
				util::foreach(index[mesh].size(), [&](int i) {
					totalIndex.emplace_back(index[mesh][i]);
				});
			});

			std::vector<SkinningVertexLayout> data;
			util::foreach(meshCount, [&](int mesh) {

				//インデックス順のものを頂点順に変換
				for (int i = 0, end = index[mesh].size(); i != end; ++i) {
					SkinningVertexLayout layout;
					layout.position = vertex[mesh][index[mesh][i]];
					layout.normal = normal[mesh][i];
					layout.binormal = binroaml[mesh][i];
					layout.tangent = tangent[mesh][i];
					layout.uv = uv[mesh][i];
					layout.meshID = mesh;

					if (weightList.size() > mesh) {
						auto weights = util::vectorCopyif<util::BoneAndWeight>(weightList[mesh].weightList, [&](util::BoneAndWeight obj) {
							return obj.index == index[mesh][i];
						});

						util::foreach(weights.size(), [&](int i) {
							layout.boneID[i] = weights[i].boneID;
							layout.weights[i] = weights[i].weight;
						});
					}

					data.emplace_back(layout);
				}
			});

			std::vector<SkinningVertexLayout> finalVertex;
			finalVertex.reserve(data.size());
			std::vector<int> finalIndex;
			finalIndex.reserve(totalIndex.size());

			for (auto& v : data)
			{
				auto find = std::find(finalVertex.begin(), finalVertex.end(), v);

				if (find == finalVertex.end()) {
					//重複していない
					finalIndex.emplace_back(finalVertex.size());
					finalVertex.emplace_back(v);
				}
				else {
					//重複している
					auto index = std::distance(finalVertex.begin(), find);
					finalIndex.emplace_back(index);
				}
			}

			this->vertexNum = finalVertex.size();

			this->indexNum = finalIndex.size();

			this->mesh.vertexBuffer = util::createVertexBuffer(sizeof(SkinningVertexLayout) * this->vertexNum, finalVertex.data());

			this->mesh.indexBuffer = util::createIndexBuffer(sizeof(int) * this->indexNum, finalIndex.data());

		}

		void readBinary(const std::string& binaryFilePath) {
			util::CriticalSection c;
			util::BinaryLoader loader(binaryFilePath);

			std::vector<SkinningVertexLayout> vBuffer;
			loader.load<int>(&vertexNum);
			loader.load<SkinningVertexLayout>(vertexNum,&vBuffer);

			std::vector<int> iBuffer;
			loader.load<int>(&indexNum);
			loader.load<int>(indexNum, &iBuffer);

			this->mesh.vertexBuffer = util::createVertexBuffer(sizeof(SkinningVertexLayout) * this->vertexNum, vBuffer.data());
			this->mesh.indexBuffer = util::createIndexBuffer(sizeof(int) * this->indexNum, iBuffer.data());
		}
	};


}