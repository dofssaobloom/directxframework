#pragma once
#include<Effekseer.h>
#include<EffekseerRendererDX11.h>
#include<Source\Util\Type.h>
#include<Source\Util\Math\Transform.h>

namespace framework {

	class Effect
	{
	public:
		Effect(Effekseer::Handle handle);
		Effect(Effekseer::Handle handle, util::Vec3 position);
		Effect(Effekseer::Handle handle, util::Transform& trans);
		~Effect();

		void update(Effekseer::Manager* pManager);

		/// <summary>
		/// 再生時間変更
		/// </summary>
		/// <param name="speed"></param>
		void setSpeed(float speed);

		bool isExists(Effekseer::Manager* pManager);

		const Effekseer::Handle getHandle();


	public:
		util::Transform m_Trans;

	private:
		Effekseer::Vector3D convVec(const util::Vec3& vec);

	private:
		Effekseer::Handle m_Handle;
		float m_Speed;
	};
}