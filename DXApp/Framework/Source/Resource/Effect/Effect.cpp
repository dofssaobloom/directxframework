#include"Effect.h"

namespace framework {

	Effect::Effect(Effekseer::Handle handle)
		:Effect(handle, util::Vec3(0, 0, 0))
	{
	}

	Effect::Effect(Effekseer::Handle handle, util::Vec3 position)
		: Effect(handle, util::Transform(position, util::Vec3(0, 0, 0), util::Vec3(1, 1, 1)))
	{
	}

	Effect::Effect(Effekseer::Handle handle, util::Transform & trans)
		: m_Handle(handle),
		m_Trans(trans)
	{
		m_Speed = 1.0f;
	}

	Effect::~Effect()
	{
	}

	void Effect::update(Effekseer::Manager * pManager)
	{
		pManager->SetLocation(m_Handle, convVec(m_Trans.m_Position));
		pManager->SetScale(m_Handle, m_Trans.m_Scale.x, m_Trans.m_Scale.y, m_Trans.m_Scale.z);
		pManager->SetRotation(m_Handle, XMConvertToRadians(m_Trans.m_Rotation.x), XMConvertToRadians(m_Trans.m_Rotation.y), XMConvertToRadians(m_Trans.m_Rotation.z));
		pManager->SetSpeed(m_Handle,m_Speed);
	}

	void Effect::setSpeed(float speed)
	{
		m_Speed = speed;
	}

	bool Effect::isExists(Effekseer::Manager * pManager)
	{
		return pManager->Exists(m_Handle);
	}

	const Effekseer::Handle Effect::getHandle()
	{
		return m_Handle;
	}

	Effekseer::Vector3D Effect::convVec(const util::Vec3 & vec)
	{
		return Effekseer::Vector3D(vec.x, vec.y, vec.z);
	}

}