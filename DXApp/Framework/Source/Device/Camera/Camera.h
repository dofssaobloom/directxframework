#pragma once
#include<Source\Util\Type.h>

namespace framework {

	class Camera
	{
	public:
		Camera();
		Camera(util::Vec3 eyePos,util::Vec3 lookPos);
		~Camera();

		void setEyePosition(util::Vec3 eyePos);

		void setLookAt(util::Vec3 lookPos);

		util::Vec3 getLookPos();

		util::Vec3 getEyePos();

		/**
		* @brief					ビュー行列に変換及び取得
		* @return					ビュー変換行列
		*/
		util::Mat4 toViewMatrix();

		/**
		* @brief					ビュー行列に変換済み行列取得
		* @detail					toViewMatrixが呼ばれていなければ古い行列になってしまうので注意
		* @return					ビュー変換行列
		*/
		util::Mat4 getViewMatrix();

		/**
		* @brief					プロジェクション取得
		* @return					プロジェクション変換行列
		*/
		util::Mat4 getProjMatrix();

		void setOffset(const util::Vec3& offset);

	private:
		//!視点座標
		util::Vec3 m_EyePosition;
		//!注視点座標
		util::Vec3 m_LookPosition;
		//!ビュー変換行列
		util::Mat4 m_ViewMatrix;
		//!プロジェクション
		util::Mat4 m_Projection;
		//!注視点オフセット
		util::Vec3 m_Offset;
	};

}