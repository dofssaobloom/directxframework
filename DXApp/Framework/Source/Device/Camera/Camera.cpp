#include"Camera.h"


namespace framework {
	Camera::Camera()
		:Camera(util::Vec3(),util::Vec3())
	{
	}

	Camera::Camera(util::Vec3 eyePos, util::Vec3 lookPos)
		: m_EyePosition(eyePos),
		m_LookPosition(lookPos)
	{
		m_Projection = XMMatrixPerspectiveFovLH(
			XMConvertToRadians(Screen::PERSPECTIVE),
			Screen::WINDOW_WIDTH / (float)Screen::WINDOW_HEIGHT,//float���Y�꒍��
			1.0f,
			Screen::FAR_);
	}

	Camera::~Camera()
	{
	}

	void Camera::setEyePosition(util::Vec3 eyePos)
	{
		m_EyePosition = eyePos;
	}

	void Camera::setLookAt(util::Vec3 lookPos)
	{
		m_LookPosition = lookPos;
	}

	util::Vec3 Camera::getLookPos()
	{
		return m_LookPosition;
	}

	util::Vec3 Camera::getEyePos()
	{
		return m_EyePosition;
	}

	util::Mat4 Camera::toViewMatrix()
	{
		util::Vec4 upDirection = { 0.0f,1.0f,0.0f,1.0f };

		auto lookPos = m_LookPosition + m_Offset;

		util::Mat4 viewMat = XMMatrixLookAtLH(
			m_EyePosition.toXMVector(),
			lookPos.toXMVector(),
			upDirection.toXMVector());

		m_ViewMatrix = viewMat.transpose();
		return m_ViewMatrix;
	}

	util::Mat4 Camera::getViewMatrix()
	{
		return m_ViewMatrix;
	}

	util::Mat4 Camera::getProjMatrix()
	{
		return m_Projection.transpose();
	}

	void Camera::setOffset(const util::Vec3 & offset)
	{
		m_Offset = offset;
	}
}