#pragma once
#include<dsound.h>
#include<atlbase.h>
#include<Windows.h>
#include<unordered_map>
#include<memory>

#include<Source\Resource\Sound\SoundResource.h>
#include<Source\Resource\Sound\DirectSound.h>
#include<Source\Resource\Sound\SoundListener.h>

namespace framework {

	class SoundPlayer
	{
	public:
		SoundPlayer(HWND hWnd);
		~SoundPlayer();


		/**
		* @brief	再生するサウンドを切り替え
		*/
		void playSound(const std::string& key,bool isLoop = false);

		/**
		* @brief	音量変更
		*/
		void setVolume(const std::string& key, int const& volume);


		void setPosition(const std::string& key, const util::Transform & position);

		void setDopplerVelocity(const std::string& key, const util::Vec3 & speed);

		void setDistance(const std::string& key, float min, float max);

		void setDoppler(const std::string & key, bool doppler);

		/**
		* @brief	サウンド読み込み
		*/
		void importResource(const std::string& key, const std::string& filePath);

		/**
		* @brief	クリア
		*/
		void clear();

		bool isPlaying(const std::string& key);

		/// <summary>
		/// リスナー作成
		/// </summary>
		/// <returns></returns>
		std::shared_ptr<SoundListener> createListener();

	private:
		//!サウンドデバイス
		//CComPtr<IDirectSound8> m_pSoundDevice;
		std::shared_ptr<DirectSound> m_pDirectSound;

		//!サウンドリソース
		std::unordered_map<std::string, std::shared_ptr<SoundResource>> m_ResourceContainer;
	};
}