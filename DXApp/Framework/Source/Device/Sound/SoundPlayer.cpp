#include"SoundPlayer.h"
#include<assert.h>

namespace framework {

	SoundPlayer::SoundPlayer(HWND hWnd)
	{
		m_pDirectSound = std::make_shared<DirectSound>();
		m_pDirectSound->create(hWnd);
	}

	SoundPlayer::~SoundPlayer()
	{
	}


	void SoundPlayer::playSound(const std::string & key, bool isLoop)
	{
		assert(m_ResourceContainer.find(key) != m_ResourceContainer.end() && "登録されていません");

		m_ResourceContainer[key]->play(isLoop);
	}

	void SoundPlayer::setVolume(const std::string & key,int const& volume)
	{
		assert(m_ResourceContainer.find(key) != m_ResourceContainer.end() && "登録されていません");
		m_ResourceContainer[key]->setVolume(volume);
	}

	void SoundPlayer::setPosition(const std::string & key, const util::Transform & position)
	{
		assert(m_ResourceContainer.find(key) != m_ResourceContainer.end() && "登録されていません");
		m_ResourceContainer[key]->setPosition(position);
	}

	void SoundPlayer::setDopplerVelocity(const std::string & key, const util::Vec3 & speed)
	{
		assert(m_ResourceContainer.find(key) != m_ResourceContainer.end() && "登録されていません");
		m_ResourceContainer[key]->setDopplerVelocity(speed);
	}

	void SoundPlayer::setDistance(const std::string & key, float min, float max)
	{
		assert(m_ResourceContainer.find(key) != m_ResourceContainer.end() && "登録されていません");
		m_ResourceContainer[key]->setDistance(min, max);
	}

	void SoundPlayer::setDoppler(const std::string & key, bool doppler)
	{
		assert(m_ResourceContainer.find(key) != m_ResourceContainer.end() && "登録されていません");
		m_ResourceContainer[key]->setDoppler(doppler);
	}

	void SoundPlayer::importResource(const std::string & key, const std::string & filePath)
	{
		assert(m_ResourceContainer.find(key) == m_ResourceContainer.end() && "すでに登録されています");
		m_ResourceContainer[key] = std::make_shared<SoundResource>(m_pDirectSound->m_directSound8, filePath);
	}

	void SoundPlayer::clear()
	{
		m_ResourceContainer.clear();
	}

	bool SoundPlayer::isPlaying(const std::string& key)
	{
		assert(m_ResourceContainer.find(key) != m_ResourceContainer.end() && "登録されていません");
		return  !m_ResourceContainer[key]->isEnd();
	}

	std::shared_ptr<SoundListener> SoundPlayer::createListener()
	{
		return std::make_shared<SoundListener>(m_pDirectSound);
	}
}