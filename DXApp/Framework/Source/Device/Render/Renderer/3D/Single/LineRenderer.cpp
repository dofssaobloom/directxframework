#include "Framework.h"
#include "LineRenderer.h"
#include<Source\Resource\Model\Line.h>


namespace framework {



	LineRenderer::ConstantBuffer::ConstantBuffer()
	{
		pAlwaysGPU = util::createConstantBuffer(sizeof(AlwaysBuffer));
		pSometimeGPU = util::createConstantBuffer(sizeof(SometimeBuffer));
		pControllGPU = util::createConstantBuffer(sizeof(ControllBuffer));
	}

	LineRenderer::LineRenderer(std::vector<LineVertexLayout>& pointList)
	{
		m_pLineModel = std::make_unique<Line>(pointList);
		m_pMaterial = ResourceManager::getInstance()->getMaterial("Line");

		D3D11_INPUT_ELEMENT_DESC layout[] = {
			{ "SV_Position",0,DXGI_FORMAT_R32G32B32_FLOAT,0,0,D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "NORMAL",		0,DXGI_FORMAT_R32G32B32_FLOAT,0,sizeof(util::Vec3),D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "COLOR",		0,DXGI_FORMAT_R32G32B32_FLOAT,0,sizeof(util::Vec3) * 2,D3D11_INPUT_PER_VERTEX_DATA,0 },
		};

		m_pInputLayout = util::createIA(layout, _countof(layout), m_pMaterial->getVertexByte()->GetBufferPointer(), m_pMaterial->getVertexByte()->GetBufferSize());

		//カリングなしステート作成
		m_pRsterizerState = util::createRasterizerSate(false, false);
	}

	LineRenderer::~LineRenderer()
	{
	}

	void LineRenderer::draw(util::Transform * drawTrans)
	{

		//パイプラインにシェーダーセット
		m_pMaterial->active();

		//現在のステートを一時退避させる
		ID3D11RasterizerState* currentRSState;

		util::getContext()->RSGetState(&currentRSState);

		util::getContext()->RSSetState(m_pRsterizerState);

		m_ConstantBuffer.alwaysBuffer.width = m_LineWidth;
		m_ConstantBuffer.alwaysBuffer.color = m_Color;

		//!更新用変数
		m_ConstantBuffer.alwaysBuffer.world = drawTrans->toMatrix().toXMMatrix();

		m_ConstantBuffer.alwaysBuffer.view = component::CameraComponent::getMainCamera().lock()->toViewMatrix().toXMMatrix();

		util::updateConstantBuffer<AlwaysBuffer>(m_ConstantBuffer.pAlwaysGPU.p, m_ConstantBuffer.alwaysBuffer);

		m_ConstantBuffer.sometimeBuffer.proj = component::CameraComponent::getMainCamera().lock()->getProjection().toXMMatrix();

		util::updateConstantBuffer<SometimeBuffer>(m_ConstantBuffer.pSometimeGPU.p, m_ConstantBuffer.sometimeBuffer);

		util::updateConstantBuffer<ControllBuffer>(m_ConstantBuffer.pControllGPU.p, m_ConstantBuffer.controllBuffer);

		//頂点バッファセット
		UINT strides[1] = { sizeof(LineVertexLayout) };
		UINT offsets[1] = { 0 };
		util::getContext()->IASetVertexBuffers(0, 1, &m_pLineModel->mesh.vertexBuffer.p, strides, offsets);
		util::getContext()->IASetIndexBuffer(m_pLineModel->mesh.indexBuffer.p, DXGI_FORMAT_R32_UINT, 0);

		//util::getContext()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
		util::getContext()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST);

		//定数バッファにセットするための変数
		ID3D11Buffer* mediationBuffer[3];

		mediationBuffer[0] = m_ConstantBuffer.pSometimeGPU.p;
		mediationBuffer[1] = m_ConstantBuffer.pAlwaysGPU.p;
		mediationBuffer[2] = m_ConstantBuffer.pControllGPU.p;

		m_pMaterial->setConstantBuffer(mediationBuffer, 3);

		util::getContext()->IASetInputLayout(m_pInputLayout.p);

		util::getContext()->DrawIndexed(m_pLineModel->indexNum, 0, 0);

		m_pMaterial->deActive();

		util::getContext()->RSSetState(currentRSState);
	}

	void LineRenderer::setWidth(float width)
	{
		m_LineWidth = width;
	}

	void LineRenderer::setControllPoint(int id, const util::Vec3 & pos)
	{
		m_ConstantBuffer.controllBuffer.positoin[id] = util::Vec4(pos.x, pos.y, pos.z, 1);
	}

	void LineRenderer::setColor(const util::Vec3& color)
	{
		m_Color = color;
	}

}