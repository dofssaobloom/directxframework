#pragma once
#include"IRenderer.h"
#include<list>
#include<Source\Util\Type.h>
#include<D3DX11.h>
#include<atlbase.h>

#define ControllPointNum 100

namespace framework {
	class Line;
	struct LineVertexLayout;

	class LineRenderer : public IRenderer
	{
	private:
		//更新頻度の高いバッファ
		struct AlwaysBuffer {
			XMMATRIX view;
			XMMATRIX world;
			float width;
			util::Vec3 color;
		};

		//更新頻度の低いバッファ
		struct SometimeBuffer
		{
			XMMATRIX proj;
		};

		struct ControllBuffer
		{
			util::Vec4 positoin[ControllPointNum];
		};

		struct ConstantBuffer {
			ConstantBuffer();
			//!更新頻度が高いバッファ
			CComPtr<ID3D11Buffer> pAlwaysGPU;
			//!更新頻度の低いバッファ
			CComPtr<ID3D11Buffer> pSometimeGPU;
			
			CComPtr<ID3D11Buffer> pControllGPU;

			AlwaysBuffer alwaysBuffer;
			SometimeBuffer sometimeBuffer;
			ControllBuffer controllBuffer = {};
		};

	public:
		LineRenderer(std::vector<LineVertexLayout>& pointList);
		~LineRenderer();

		// IRenderer を介して継承されました
		virtual void draw(util::Transform * drawTrans) override;

		/// <summary>
		/// 線の太さセット
		/// </summary>
		/// <param name="width"></param>
		void setWidth(float width);

		/// <summary>
		/// コントロールポイントセット
		/// </summary>
		void setControllPoint(int id,const util::Vec3& pos);

		void setColor(const util::Vec3& color);

	private:
		//!線の形状データ
		std::unique_ptr<Line> m_pLineModel;
		//!shader
		std::shared_ptr<IMaterial> m_pMaterial;
		//!入力レイアウト
		CComPtr<ID3D11InputLayout> m_pInputLayout;

		ConstantBuffer m_ConstantBuffer;
		
		CComPtr<ID3D11RasterizerState> m_pRsterizerState;

		//!線の太さ
		float m_LineWidth;

		util::Vec3 m_Color;
	};


}