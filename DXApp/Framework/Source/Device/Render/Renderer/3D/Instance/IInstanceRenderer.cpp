#include "Framework.h"
#include "IInstanceRenderer.h"

void framework::IInstanceRenderer::clear()
{
}

void framework::IInstanceRenderer::deleteIdListClear()
{
	m_DeleteId.clear();
}

void framework::IInstanceRenderer::addDeleteID(int id)
{
	m_DeleteId.emplace_back(id);
}

bool framework::IInstanceRenderer::isDeleteID(const int id)
{
	auto find = std::find(m_DeleteId.begin(), m_DeleteId.end(),id);

	return find != m_DeleteId.end();
}
