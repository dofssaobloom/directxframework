#pragma once
#include<list>
#include<memory>
#include<vector>
#include<Source\Device\Render\Renderer\3D\RenderDefine.h>

/**
* @file	    IInstanceRenderer.h
* @brief	インスタンス描画クラスがすべて実装している
* @authro	高須優輝
* @date	2017/04/01
*/

namespace util {
	struct Transform;
}

namespace framework {

	struct MaterialParam;
	class IMaterial;
	class Texture2D;
	class CubeTexture;
	class CubeRenderTrget;
	class CubeDepthTarget;

	class IInstanceRenderer
	{
	public:
		IInstanceRenderer() {};
		~IInstanceRenderer() {};

		/**
		* @brief					初期化
		*/
		virtual void init() {};

		/**
		* @brief				マテリアルのパラメータセット
		* @param param			マテリアルパラメータ
		*/
		virtual void setMaterialParam(MaterialParam& param) {};

		/**
		* @brief					描画
		* @param drawTransList		描画に使うトランスフォームのリスト
		*/
		void draw(std::list<util::Transform*> drawTransList, int offset = 0){
			drawLoop(drawTransList, offset);
		};

		/**
		* @brief					深度用描画
		* @param drawTransList		描画に使うトランスフォームのリスト
		*/
		void depthDraw(std::list<util::Transform*> drawTransList, int offset = 0) {
			depthDrawLoop(drawTransList, offset);
		};

		virtual void clear();

		/// <summary>
		/// 削除したIDリストをクリアーする
		/// </summary>
		void deleteIdListClear();

		/// <summary>
		/// 削除したIDを記憶させる
		/// </summary>
		/// <param name="id"></param>
		void addDeleteID(int id);

	protected:
		/**
		* @brief				すべて描画するまで再帰的に繰り返す
		* @param drawTransList	描画するトランスフォームリスト
		* @param offset			offsetは関数内でdrawの引数のものをそのまま入れる
		* @param drawAction		drawNumを使って処理しなければならない関数のポインタ
		*/
		void drawLoop(const std::list<util::Transform*>& drawTransList,int offset ) {
			//ワールド変換行列を定数バッファに詰め替える
			auto it = drawTransList.begin();
			//offset分イテレータを進めておく
			for (int i = 0; i != offset; ++i) {
				it++;
			}

			int drawNum;
			const int transListSize = drawTransList.size();
			for (drawNum = 0; drawNum != MAX_DRAW_NUM; ++drawNum) {
				//トランスリストより多くは描画できないようにする
				if (transListSize <= drawNum + offset)break;
				instanceAction(drawNum + offset,(*it));
				it++;
			}

			//パイプラインの設定
			setPipeline(drawNum);
			//draw回数が上限超えていればこう一度drawcallする
			drawComeback(drawTransList, drawNum, offset);
		}

		/**
		* @brief				drawLoopと同じ　深度バッファ用のコールバック用ループ
		* @param drawTransList	描画するトランスフォームリスト
		* @param offset			offsetは関数内でdrawの引数のものをそのまま入れる
		* @param drawAction		drawNumを使って処理しなければならない関数のポインタ
		*/
		void depthDrawLoop(const std::list<util::Transform*>& drawTransList, int offset) {
			//ワールド変換行列を定数バッファに詰め替える
			auto it = drawTransList.begin();
			//offset分イテレータを進めておく
			for (int i = 0; i != offset; ++i) {
				it++;
			}

			int drawNum;
			const int transListSize = drawTransList.size();
			for (drawNum = 0; drawNum != MAX_DRAW_NUM; ++drawNum) {
				//トランスリストより多くは描画できないようにする
				if (transListSize <= drawNum + offset)break;
				depthInstanceAction(drawNum + offset, (*it));
				it++;
			}

			//パイプラインの設定
			setDepthPipeline(drawNum);
			//draw回数が上限超えていればこう一度drawcallする
			depthDrawComeback(drawTransList, drawNum, offset);
		}

		/**
		* @brief				すべて描画するまで再帰的に繰り返す
		* @param drawTransList	drawの引数をそのまま入れる
		* @param drawNum		描画数を数えてその数を入れる
		* @param offset			drawの引数をそのまま入れる
		*/
		void drawComeback(const std::list<util::Transform*>& drawTransList,int drawNum ,int offset) {
			auto&& offset_ = drawNum + offset;
			if ((drawTransList.size()) - offset_> 0) {
				draw(drawTransList, offset_);
			}
		}

		/**
		* @brief				インスタンスごとに情報を取り出すためのコールバック
		* @param instanceNum	何番目のインスタンスか
		* @param trans			そのループ描画するトランスフォーム
		*/
		virtual void instanceAction(int instanceNum, util::Transform* trans) = 0;

		/**
		* @brief			パイプラインの設定
		* @param drawNum	描画する数
		*/
		virtual void setPipeline(int drawNum) = 0;


		void depthDrawComeback(const std::list<util::Transform*>& drawTransList, int drawNum, int offset) {
			auto&& offset_ = drawNum + offset;
			if ((drawTransList.size()) - offset_> 0) {
				depthDraw(drawTransList, offset_);
			}
		}

		/**
		* @brief				インスタンスごとに情報を取り出すためのコールバック
		* @param instanceNum	何番目のインスタンスか
		* @param trans			そのループ描画するトランスフォーム
		*/
		virtual void depthInstanceAction(int instanceNum, util::Transform* trans) = 0;

		/**
		* @brief			パイプラインの設定
		* @param drawNum	描画する数
		*/
		virtual void setDepthPipeline(int drawNum) = 0;

		bool isDeleteID(const int id);

		private:
			std::list<int> m_DeleteId;


	};
}