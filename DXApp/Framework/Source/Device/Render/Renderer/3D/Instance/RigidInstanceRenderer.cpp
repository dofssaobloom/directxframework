#include"RigidInstanceRenderer.h"
#include<Source\Device\Render\Shader\StandardMaterial.h>
#include<Source\Resource\Texture\Texture2D.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Resource\Model\Model.h>
#include<Source\Util\Math\Transform.h>
#include<Source\Util\WrapFunc.h>
#include<Source\Device\Camera\Camera.h>
#include<Source\Resource\Texture\CubeReanderTarget.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Device\Input\Input.h>
#include<Source\Util\Math\Math.h>
#include<Source\Entity\Entity.h>
#include<Source\Application\Screen\Screen.h>
#include<Source\Util\Render\DXFunc.h>
#include<Source\Component\Effect\DirectionalLightComponent.h>
#include<Source\Device\Input\Input.h>

namespace framework {


	RigidInstanceRenderer::RigidInstanceRenderer(std::shared_ptr<Model> model, std::vector <std::string> textureNames)
	{
		m_pAlwaysBuffer = util::createConstantBuffer(sizeof(AlwaysBuffer));
		m_pSometimeBuffer = util::createConstantBuffer(sizeof(SometimeBuffer));
		m_pMaterialBuffer = util::createConstantBuffer(sizeof(MaterialBuffer));
		m_pSkinBuffer = util::createConstantBuffer(sizeof(SkinningBuffer));
		m_Model = model;
		m_TextureNames = textureNames;
	}

	RigidInstanceRenderer::~RigidInstanceRenderer()
	{

	}

	void RigidInstanceRenderer::init()
	{
		//マテリアル取得はハードコーディングで固定のものを取得するようにしておく
		m_pMaterial = ResourceManager::getInstance()->getMaterial("RigidInstance");

		m_pShadowMaterial = ResourceManager::getInstance()->getMaterial("ShadowInstance");

		//入力レイアウト作成/////////////
		//TODO　レンダラインスタンスごとにIAオブジェクトが重複するので対策が必要
		D3D11_INPUT_ELEMENT_DESC layout[] = {
			{ "SV_Position",0,DXGI_FORMAT_R32G32B32_FLOAT,0,0,D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "NORMAL",		0,DXGI_FORMAT_R32G32B32_FLOAT,0,sizeof(util::Vec3),D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "BINORMAL",	0,DXGI_FORMAT_R32G32B32_FLOAT,0,sizeof(util::Vec3) * 2,D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "TANGENT",	0,DXGI_FORMAT_R32G32B32_FLOAT,0,sizeof(util::Vec3) * 3,D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "TEXTURE"	   ,0,DXGI_FORMAT_R32G32_FLOAT   ,0,sizeof(util::Vec3) * 4,D3D11_INPUT_PER_VERTEX_DATA,0 },
		};
		m_pInputLayout = util::createIA(layout, _countof(layout), m_pMaterial->getVertexByte()->GetBufferPointer(), m_pMaterial->getVertexByte()->GetBufferSize());

		auto sampler = util::createSamplerState(util::SamplerType::Wrap);
		m_pMaterial->setSamplerState(0, 1, &sampler.p);

		m_Textures.reserve(m_TextureNames.size());
		for (auto& name : m_TextureNames) {
			m_Textures.emplace_back(ResourceManager::getInstance()->getTexture2D(name));
		}
		m_TextureNum = m_Textures.size();
		m_pShaderResource = std::make_unique<ID3D11ShaderResourceView*[]>(m_TextureNum);
		util::foreach(m_Textures.size(), [&](int i) {
			m_pShaderResource[i] = *(m_Textures[i]->getReource());
		});

		m_MaterialBuffer.specular = 1.0f;
		m_MaterialBuffer.height = 1.0f;

		SometimeBuffer sb;

		sb.proj = component::CameraComponent::getMainCamera().lock()->getProjection().toXMMatrix();

		util::updateConstantBuffer<SometimeBuffer>(m_pSometimeBuffer.p, sb);

		SkinningBuffer skinB;
		util::updateConstantBuffer<SkinningBuffer>(m_pSkinBuffer.p, skinB);

	}

	void RigidInstanceRenderer::setMaterialParam(MaterialParam & param)
	{
		m_MaterialBuffer.specular = param.specular;
		m_MaterialBuffer.height = param.height;
		m_MaterialBuffer.isNotLighting = param.isNotLighting;
		m_MaterialBuffer.bias = param.bias;
		m_MaterialBuffer.metallic = param.mettalic;

		util::updateConstantBuffer<MaterialBuffer>(m_pMaterialBuffer.p, m_MaterialBuffer);
	}

	void RigidInstanceRenderer::clear()
	{
		m_TextureNames.clear();
		m_Textures.clear();
		m_pShaderResource.reset();
	}

	void RigidInstanceRenderer::instanceAction(int instanceNum, util::Transform* trans)
	{
		m_AlwaysBuffer.world[instanceNum] = trans->toMatrix().toXMMatrix();
		m_AlwaysBuffer.rotate[instanceNum] = trans->toRotateMatrix().toXMMatrix();
	}

	void RigidInstanceRenderer::setPipeline(int drawNum)
	{
		//パイプラインにシェーダーセット
		m_pMaterial->active();

		setTexture();

		m_AlwaysBuffer.preView = m_PreView.toXMMatrix();

		m_AlwaysBuffer.view = component::CameraComponent::getMainCamera().lock()->toViewMatrix().toXMMatrix();
	
		m_PreView = m_AlwaysBuffer.view;

		m_AlwaysBuffer.eyePos = component::CameraComponent::getMainCamera().lock()->getGameObj().lock()->getTransform()->m_Position;

		m_AlwaysBuffer.lightView = component::DirectionalLightComponent::getCamera().lock()->toViewMatrix().toXMMatrix();

		m_AlwaysBuffer.near_ = 1.0f;

		m_AlwaysBuffer.far_ = Screen::FAR_;

		util::updateConstantBuffer<AlwaysBuffer>(m_pAlwaysBuffer.p, m_AlwaysBuffer);



		//頂点バッファセット
		UINT strides[1] = { sizeof(RigidVertexLayout) };
		UINT offsets[1] = { 0 };
		util::getContext()->IASetVertexBuffers(0, 1, &m_Model->mesh.vertexBuffer.p, strides, offsets);
		util::getContext()->IASetIndexBuffer(m_Model->mesh.indexBuffer.p, DXGI_FORMAT_R32_UINT, 0);
		util::getContext()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		//定数バッファにセットするための変数
		ID3D11Buffer* mediationBuffer[3];

		//順番厳守
		mediationBuffer[0] = m_pSometimeBuffer.p;
		mediationBuffer[1] = m_pAlwaysBuffer.p;
		mediationBuffer[2] = m_pMaterialBuffer.p;

		m_pMaterial->setConstantBuffer(mediationBuffer, 3);

		util::getContext()->IASetInputLayout(m_pInputLayout.p);

		util::getContext()->DrawIndexedInstanced(m_Model->indexNum, drawNum,0, 0, 0);

		m_pMaterial->deActive();

	}

	void RigidInstanceRenderer::setTexture()
	{
		m_pMaterial->setTexture(0, m_TextureNum, m_pShaderResource.get());

		auto shadowMap = ResourceManager::getInstance()->getTexture2D("ShadowMap");
		//firestはテクスチャの数
		m_pMaterial->setTexture(9, 1, shadowMap->getReource());//キューブマップは10番目にセットする
	}

	void RigidInstanceRenderer::depthInstanceAction(int instanceNum, util::Transform * trans)
	{
		auto entity = (Entity*)trans->getUserPointer();
		auto client = entity->getComponent<component::RenderClientComponent>();
		m_InstanceID[instanceNum] = (int)trans;
		//PrePositionはownerで処理している
		m_AlwaysBuffer.preWorld[instanceNum] = client.lock()->m_PrePosition.toMatrix().toXMMatrix();
		m_AlwaysBuffer.world[instanceNum] = trans->toMatrix().toXMMatrix();
		m_AlwaysBuffer.rotate[instanceNum] = trans->toRotateMatrix().toXMMatrix();
	}

	void RigidInstanceRenderer::setDepthPipeline(int drawNum)
	{
		//パイプラインにシェーダーセット
		m_pShadowMaterial->active();

		m_AlwaysBuffer.view = component::DirectionalLightComponent::getCamera().lock()->toViewMatrix().toXMMatrix();

		//m_AlwaysBuffer.eyePos = component::CameraComponent::getMainCamera().lock()->getGameObj().lock()->getTransform()->m_Position;

		//m_AlwaysBuffer.lightView = component::DirectionalLightComponent::getCamera().lock()->toViewMatrix().toXMMatrix();

		m_AlwaysBuffer.near_ = 1.0f;

		m_AlwaysBuffer.far_ = Screen::FAR_;


		//static float b = 0.0f;

		//if (Input::getKey().isKeyDown(KeyBoard::KeyCords::A)) {
		//	b += 0.0001;
		//}

		//if (Input::getKey().isKeyDown(KeyBoard::KeyCords::S)) {
		//	b -= 0.0001;
		//}

		//m_MaterialBuffer.bias = b;
		static float test = 0.0f;

		//if (Input::getKey().isKeyDown(KeyBoard::KeyCords::R)) {
		//	test += 0.001f;
		//}
		//if (Input::getKey().isKeyDown(KeyBoard::KeyCords::T)) {
		//	test -= 0.001f;
		//}

		m_AlwaysBuffer.fatParam = test;

		util::updateConstantBuffer<AlwaysBuffer>(m_pAlwaysBuffer.p, m_AlwaysBuffer);

		//頂点バッファセット
		UINT strides[1] = { sizeof(RigidVertexLayout) };
		UINT offsets[1] = { 0 };
		util::getContext()->IASetVertexBuffers(0, 1, &m_Model->mesh.vertexBuffer.p, strides, offsets);
		util::getContext()->IASetIndexBuffer(m_Model->mesh.indexBuffer.p, DXGI_FORMAT_R32_UINT, 0);
		util::getContext()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		//定数バッファにセットするための変数
		ID3D11Buffer* mediationBuffer[4];

		//順番厳守
		mediationBuffer[0] = m_pSometimeBuffer.p;
		mediationBuffer[1] = m_pAlwaysBuffer.p;
		mediationBuffer[2] = m_pMaterialBuffer.p;
		mediationBuffer[3] = m_pSkinBuffer.p;

		m_pShadowMaterial->setConstantBuffer(mediationBuffer, 4);

		util::getContext()->IASetInputLayout(m_pInputLayout.p);

		util::getContext()->DrawIndexedInstanced(m_Model->indexNum, drawNum, 0, 0, 0);

		m_pShadowMaterial->deActive();
	}


}