#include "SkinningInstanceRenderer.h"
#include<Source\Device\Render\Shader\StandardMaterial.h>
#include<Source\Resource\Texture\Texture2D.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Resource\Model\Model.h>
#include<Source\Util\Math\Transform.h>
#include<Source\Util\WrapFunc.h>
#include<Source\Device\Camera\Camera.h>
#include<Source\Resource\Texture\CubeReanderTarget.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Device\Input\Input.h>
#include<Source\Util\Math\Math.h>
#include<Source\Entity\Entity.h>
#include<Source\Application\Screen\Screen.h>
#include<Source\Util\Render\DXFunc.h>
#include<Source\Resource\Motion\Motion.h>
#include<Source\Component\Effect\DirectionalLightComponent.h>
#include<Source\Entity\Entity.h>
#include<Source\Resource\Texture\BoneTexture.h>
#include<Source\Util\Thread\Thread.h>
#include<Source\Resource\ResourceManager.h>
#include<iostream>


namespace framework {
	//std::unique_ptr<BoneTexture> SkinningInstanceRenderer::m_pBoneTexure = std::make_unique<BoneTexture>(BONE_NUM , MAX_MOTION_FRAME);

	SkinningInstanceRenderer::SkinningInstanceRenderer(std::shared_ptr<Model> model, std::vector<std::string> textureNames, std::vector <std::string> motionNames)
	{
		m_ConstantData.pAlwaysGPU = util::createConstantBuffer(sizeof(AlwaysBuffer));
		m_ConstantData.pSometimeGPU = util::createConstantBuffer(sizeof(SometimeBuffer));
		m_ConstantData.pMaterialGPU = util::createConstantBuffer(sizeof(MaterialBuffer));
		m_ConstantData.pSkinningGPU = util::createConstantBuffer(sizeof(SkinningBuffer));
		m_ResourceData.pModel = model;
		m_LoadName.textureNames = textureNames;

		auto find = std::remove_if(m_LoadName.textureNames.begin(), m_LoadName.textureNames.end(), [](const std::string& str) {
			return str == "";
		});

		m_LoadName.textureNames.erase(find,m_LoadName.textureNames.end());

		m_LoadName.motionNames = motionNames;
	}

	SkinningInstanceRenderer::~SkinningInstanceRenderer()
	{
	}

	void SkinningInstanceRenderer::init()
	{
		//マテリアル取得はハードコーディングで固定のものを取得するようにしておく
		m_pMaterial = ResourceManager::getInstance()->getMaterial("SkinningInstance");
		m_pShadowMaterial = ResourceManager::getInstance()->getMaterial("ShadowInstance");

		//入力レイアウト作成/////////////
		//TODO　レンダラインスタンスごとにIAオブジェクトが重複するので対策が必要
		D3D11_INPUT_ELEMENT_DESC layout[] = {
			{ "SV_Position",0,DXGI_FORMAT_R32G32B32_FLOAT,0,0,D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "NORMAL",		0,DXGI_FORMAT_R32G32B32_FLOAT,0,sizeof(util::Vec3),D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "BINORMAL",	0,DXGI_FORMAT_R32G32B32_FLOAT,0,sizeof(util::Vec3) * 2,D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "TANGENT",	0,DXGI_FORMAT_R32G32B32_FLOAT,0,sizeof(util::Vec3) * 3,D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "TEXTURE"	   ,0,DXGI_FORMAT_R32G32_FLOAT   ,0,sizeof(util::Vec3) * 4,D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "TEXTURE"	   ,1,DXGI_FORMAT_R32G32B32A32_UINT   ,0,sizeof(util::Vec2) + sizeof(util::Vec3) * 4,D3D11_INPUT_PER_VERTEX_DATA,0 },
			{ "TEXTURE"	   ,2,DXGI_FORMAT_R32G32B32A32_FLOAT  ,0,(sizeof(util::Vec2) + sizeof(util::Vec3) * 4) + sizeof(util::Vec4),D3D11_INPUT_PER_VERTEX_DATA,0 },
		};
		m_pInputLayout = util::createIA(layout, _countof(layout), m_pMaterial->getVertexByte()->GetBufferPointer(), m_pMaterial->getVertexByte()->GetBufferSize());

		auto sampler = util::createSamplerState(util::SamplerType::Wrap);
		m_pMaterial->setSamplerState(0, 1, &sampler.p);

		m_ResourceData.pTextures.reserve(m_LoadName.textureNames.size());
		for (auto& name : m_LoadName.textureNames) {
			m_ResourceData.pTextures.emplace_back(ResourceManager::getInstance()->getTexture2D(name));
		}
		m_TextureNum = m_ResourceData.pTextures.size();
		m_pShaderResource = std::make_unique<ID3D11ShaderResourceView*[]>(m_TextureNum);
		util::foreach(m_TextureNum, [&](int i) {
			m_pShaderResource[i] = *(m_ResourceData.pTextures[i]->getReource());
		});

		m_ConstantData.materialBuffer.specular = 1.0f;
		m_ConstantData.materialBuffer.height = 1.0f;


		SometimeBuffer sb;

		sb.proj = component::CameraComponent::getMainCamera().lock()->getProjection().toXMMatrix();

		util::updateConstantBuffer<SometimeBuffer>(m_ConstantData.pSometimeGPU.p, sb);


		//ボーンテクスチャに書き込み
		//writeBoneTexture();
	}

	void SkinningInstanceRenderer::setMaterialParam(MaterialParam & param)
	{
		//定数バッファの都合上同じ構造体だと手間かもしれないのでひとつづつコピー処理を書いていく
		m_ConstantData.materialBuffer.specular = param.specular;
		m_ConstantData.materialBuffer.height = param.height;
		m_ConstantData.materialBuffer.isNotLighting = param.isNotLighting;
		m_ConstantData.materialBuffer.bias = param.bias;
		//m_ConstantData.materialBuffer.bias = 1.0;


		util::updateConstantBuffer<MaterialBuffer>(m_ConstantData.pMaterialGPU.p, m_ConstantData.materialBuffer);
	}

	void SkinningInstanceRenderer::instanceAction(int instanceNum, util::Transform * trans)
	{
		m_ConstantData.alwaysBuffer.world[instanceNum] = trans->toMatrix().toXMMatrix();
		m_ConstantData.alwaysBuffer.rotate[instanceNum] = trans->toRotateMatrix().toXMMatrix();

		frameAnimeParam(instanceNum, trans);
	}

	void SkinningInstanceRenderer::setPipeline(int drawNum)
	{
		//パイプラインにシェーダーセット
		m_pMaterial->active();

		setTexture();

		m_ConstantData.alwaysBuffer.preView = m_PreView.toXMMatrix();

		m_ConstantData.alwaysBuffer.view = component::CameraComponent::getMainCamera().lock()->toViewMatrix().toXMMatrix();

		m_PreView = m_ConstantData.alwaysBuffer.view;

		m_ConstantData.alwaysBuffer.eyePos = component::CameraComponent::getMainCamera().lock()->getGameObj().lock()->getTransform()->m_Position;

		m_ConstantData.alwaysBuffer.lightView = component::DirectionalLightComponent::getCamera().lock()->toViewMatrix().toXMMatrix();

		m_ConstantData.alwaysBuffer.near_ = 1.0f;
		m_ConstantData.alwaysBuffer.far_ = Screen::FAR_;


		util::updateConstantBuffer<AlwaysBuffer>(m_ConstantData.pAlwaysGPU.p, m_ConstantData.alwaysBuffer);


		util::updateConstantBuffer<SkinningBuffer>(m_ConstantData.pSkinningGPU.p, m_ConstantData.skinningBuffer);

		//頂点バッファセット
		UINT strides[1] = { sizeof(SkinningVertexLayout) };
		UINT offsets[1] = { 0 };
		util::getContext()->IASetVertexBuffers(0, 1, &m_ResourceData.pModel->mesh.vertexBuffer.p, strides, offsets);
		util::getContext()->IASetIndexBuffer(m_ResourceData.pModel->mesh.indexBuffer.p, DXGI_FORMAT_R32_UINT, 0);
		util::getContext()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		//定数バッファにセットするための変数
		ID3D11Buffer* mediationBuffer[4];

		//順番厳守
		mediationBuffer[0] = m_ConstantData.pSometimeGPU.p;
		mediationBuffer[1] = m_ConstantData.pAlwaysGPU.p;
		mediationBuffer[2] = m_ConstantData.pMaterialGPU.p;
		mediationBuffer[3] = m_ConstantData.pSkinningGPU.p;

		m_pMaterial->setConstantBuffer(mediationBuffer, 4);

		util::getContext()->IASetInputLayout(m_pInputLayout.p);

		util::getContext()->DrawIndexedInstanced(m_ResourceData.pModel->indexNum, drawNum,0, 0, 0);

		//util::getContext()->DrawIndexedInstanced(m_ResourceData.pModel->indexNum, drawNum, 0, 0, 0);

		m_pMaterial->deActive();
	}

	void SkinningInstanceRenderer::setTexture()
	{
		m_pMaterial->setTexture(0, m_TextureNum, m_pShaderResource.get());

		auto shadowMap = ResourceManager::getInstance()->getTexture2D("ShadowMap");
		//firestはテクスチャの数
		m_pMaterial->setTexture(9, 1, shadowMap->getReource());

		auto setView = m_ResourceData.pBoneTexure->getShaderView();
		m_pMaterial->setTexture(11, 1, &setView);
	}

	void SkinningInstanceRenderer::writeBoneTexture(std::weak_ptr<component::AnimatorComponent> animator)
	{
		//TODO ここでアニメーションをテクスチャに
		//TODOアニメーターの順番にする必要がある

		//auto& motionNames = m_LoadName.motionNames;

		//縦にアニメーションをつんでいく
		m_ResourceData.pBoneTexure = std::make_unique<BoneTexture>(BONE_NUM, MAX_MOTION_FRAME * animator.lock()->animeCount());

		std::vector<std::shared_ptr<Motion>> motionContainer;

		/*	for (auto name : motionNames) {
				motionContainer.emplace_back(ResourceManager::getInstance()->getMotion(name));
			}*/

		for (auto name : animator.lock()->getMotionNames()) {
			motionContainer.emplace_back(ResourceManager::getInstance()->getMotion(name));
		}

		m_ResourceData.pBoneTexure->begin();
		//モーションの種類分まわす
		util::foreach(motionContainer.size(), [&](int i) {

			//!マトリクスの行の数
			const int matrixY = 4;

			const int motionOffset = ((MAX_MOTION_FRAME * matrixY)) * i;

			util::foreach(MAX_MOTION_FRAME, [&](int frame) {

				const int frameOffset = (frame * matrixY) + motionOffset;

				auto motionMat = motionContainer[i]->getFrameMotion(frame);

				util::foreach(motionMat.size(), [&](int boneNum) {

					auto mat = XMMatrixTranspose(motionMat[boneNum].toXMMatrix());

					m_ResourceData.pBoneTexure->beginToBuffer(boneNum, frameOffset + 0, [&](float* r, float* g, float* b, float* a) {
						*r = mat._11;
						*g = mat._12;
						*b = mat._13;
						*a = mat._14;
					});

					m_ResourceData.pBoneTexure->beginToBuffer(boneNum, frameOffset + 1, [&](float* r, float* g, float* b, float* a) {
						*r = mat._21;
						*g = mat._22;
						*b = mat._23;
						*a = mat._24;
					});

					m_ResourceData.pBoneTexure->beginToBuffer(boneNum, frameOffset + 2, [&](float* r, float* g, float* b, float* a) {
						*r = mat._31;
						*g = mat._32;
						*b = mat._33;
						*a = mat._34;
					});

					m_ResourceData.pBoneTexure->beginToBuffer(boneNum, frameOffset + 3, [&](float* r, float* g, float* b, float* a) {
						*r = mat._41;
						*g = mat._42;
						*b = mat._43;
						*a = mat._44;
					});

				});
			});

		});
		m_ResourceData.pBoneTexure->end();
	}

	void SkinningInstanceRenderer::frameAnimeParam(const int instanceID, util::Transform * trans)
	{
		auto entity = (Entity*)(trans->getUserPointer());

		if (entity == nullptr)return;

		auto animator = entity->getComponent<component::AnimatorComponent>();

		if (animator.expired()) {
			assert(false && "アニメーターが接続されていません");
			m_ConstantData.skinningBuffer.drawData[instanceID] = util::Vec4();
			return;
		}

		m_ConstantData.skinningBuffer.drawData[instanceID].x = animator.lock()->getFrame();
		m_ConstantData.skinningBuffer.drawData[instanceID].y = animator.lock()->getCurrentMotionID();
		//アにメーターフラグを立てる
		m_ConstantData.skinningBuffer.drawData[instanceID].z = 1;

		//ブレンドするかの判定
		bool isBlend = animator.lock()->isBlend();
		if (isBlend) {
			//ブレンド率
			m_ConstantData.skinningBuffer.blendData[instanceID].x = animator.lock()->getBlendRate();

			////ブレンドするアニメーション番号
			m_ConstantData.skinningBuffer.blendData[instanceID].y = animator.lock()->getBlendMotionID();

			////ブレンドするかどうか
			m_ConstantData.skinningBuffer.blendData[instanceID].z = isBlend;

			////フレーム
			m_ConstantData.skinningBuffer.blendData[instanceID].w = animator.lock()->getBlendFrame();
		}
		else {
			//ブレンドしないときは他の設定は使わないので省く
			m_ConstantData.skinningBuffer.blendData[instanceID].z = isBlend;
		}
	}

	void SkinningInstanceRenderer::clear()
	{
		m_ResourceData.pTextures.clear();
		m_ResourceData.pBoneTexure.reset();
	}

	void SkinningInstanceRenderer::depthInstanceAction(int instanceNum, util::Transform * trans)
	{
		//前回のトランスフォームのアドレスを覚えておいてこれでインスタンス削除時の配列のズレを修正する
		auto entity = (Entity*)trans->getUserPointer();
		auto client = entity->getComponent<component::RenderClientComponent>();
		m_InstanceID[instanceNum] = (int)trans;
		
		m_ConstantData.alwaysBuffer.preWorld[instanceNum] = client.lock()->m_PrePosition.toMatrix().toXMMatrix();
		m_ConstantData.alwaysBuffer.world[instanceNum] = trans->toMatrix().toXMMatrix();
		m_ConstantData.alwaysBuffer.rotate[instanceNum] = trans->toRotateMatrix().toXMMatrix();

		m_ConstantData.skinningBuffer.texLerp[instanceNum].x = client.lock()->getTexLerp();
		m_ConstantData.skinningBuffer.fatParam[instanceNum].x = client.lock()->getFat();

		frameAnimeParam(instanceNum, trans);
	}

	void SkinningInstanceRenderer::setDepthPipeline(int drawNum)
	{
		//パイプラインにシェーダーセット
		m_pShadowMaterial->active();

		m_ConstantData.alwaysBuffer.view = component::DirectionalLightComponent::getCamera().lock()->toViewMatrix().toXMMatrix();
		m_ConstantData.alwaysBuffer.near_ = 1.0f;
		m_ConstantData.alwaysBuffer.far_ = Screen::FAR_;


		//m_ConstantData.alwaysBuffer.eyePos = component::CameraComponent::getMainCamera().lock()->getGameObj().lock()->getTransform()->m_Position;

		//m_ConstantData.alwaysBuffer.lightView = component::DirectionalLightComponent::getCamera().lock()->toViewMatrix().toXMMatrix();
		//auto lightMat = component::DirectionalLightComponent::getTransform()->toMatrix();

		auto setView = m_ResourceData.pBoneTexure->getShaderView();
		m_pMaterial->setTexture(11, 1, &setView);


		util::updateConstantBuffer<AlwaysBuffer>(m_ConstantData.pAlwaysGPU.p, m_ConstantData.alwaysBuffer);

		util::updateConstantBuffer<SkinningBuffer>(m_ConstantData.pSkinningGPU.p, m_ConstantData.skinningBuffer);

		//頂点バッファセット
		UINT strides[1] = { sizeof(SkinningVertexLayout) };
		UINT offsets[1] = { 0 };
		util::getContext()->IASetVertexBuffers(0, 1, &m_ResourceData.pModel->mesh.vertexBuffer.p, strides, offsets);
		util::getContext()->IASetIndexBuffer(m_ResourceData.pModel->mesh.indexBuffer.p, DXGI_FORMAT_R32_UINT, 0);
		util::getContext()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		//定数バッファにセットするための変数
		ID3D11Buffer* mediationBuffer[4];

		//順番厳守
		mediationBuffer[0] = m_ConstantData.pSometimeGPU.p;
		mediationBuffer[1] = m_ConstantData.pAlwaysGPU.p;
		mediationBuffer[2] = m_ConstantData.pMaterialGPU.p;
		mediationBuffer[3] = m_ConstantData.pSkinningGPU.p;

		m_pMaterial->setConstantBuffer(mediationBuffer, 4);

		util::getContext()->IASetInputLayout(m_pInputLayout.p);


		util::updateConstantBuffer<MaterialBuffer>(m_ConstantData.pMaterialGPU.p, m_ConstantData.materialBuffer);

		util::getContext()->DrawIndexedInstanced(m_ResourceData.pModel->indexNum, drawNum,0, 0, 0);

		//util::getContext()->DrawIndexedInstanced(m_ResourceData.pModel->indexNum, drawNum, 0, 0, 0);

		m_pShadowMaterial->deActive();
	}

}
