#pragma once
#include<Source\Util\Render\DXFunc.h>
#include<Source\Resource\Model\Model.h>
#include<Source\Util\Type.h>
#include<list>
#include<vector>
#include<Source\Device\Render\Renderer\3D\Instance\IInstanceRenderer.h>
#include<Source\Resource\Texture\CubeTexture.h>

/**
* @file	    RigidRenderer.h
* @brief	剛体インスタンス描画クラス
* @detail   テクスチャのレジスタ0~9までとし、10番目にはキューブテクスチャが入る
* @authro	高須優輝
* @date	2017/03/13
*/



namespace util {
	struct Transform;
}

namespace framework {

	struct MaterialParam;
	class IMaterial;
	class Texture2D;
	class CubeRenderTrget;
	class CubeDepthTarget;

	class RigidInstanceRenderer : public IInstanceRenderer
	{
	public:
		/**
		* @brief		コストラクタ
		* @param modelName     モデルのKey
		* @param textureNames  テクスチャの名前の入ったvector
		*/
		RigidInstanceRenderer(std::shared_ptr<Model> model, std::vector <std::string> textureNames);
		~RigidInstanceRenderer();

		//更新頻度の高いバッファ
		struct AlwaysBuffer
		{
			XMMATRIX view;
			//前フレーム
			XMMATRIX preView;
			//!シャドウマッピング用行列
			XMMATRIX lightView;
			//!最大描画数はシェーダーに記述されている100
			XMMATRIX world[MAX_DRAW_NUM];
			//!最大描画数はシェーダーに記述されている
			XMMATRIX preWorld[MAX_DRAW_NUM];
			//!法線回転用の行列
			XMMATRIX rotate[MAX_DRAW_NUM];
			util::Vec3 eyePos;
			float near_;
			float far_;
			float fatParam;
			util::Vec2 damy;
		};

		//更新頻度の高いバッファ
		struct SkinningBuffer
		{
			//!ボーン行列 メッシュの数　ボーンの数
			//XMMATRIX boneMatrix[BONE_NUM];
			util::Vec4 drawData[MAX_DRAW_NUM];//16の倍数にするために２足す
		};

		struct MaterialBuffer {
			float height;
			float specular;
			//!ライティングするかどうか
			float isNotLighting;
			float bias;

			float metallic;
			util::Vec3 damy;
		};

		//あまり更新しないバッファ
		struct SometimeBuffer
		{
			XMMATRIX proj;
		};

		/**
		* @brief					初期化
		*/
		void init() override;

		/**
		* @brief				マテリアルのパラメータセット
		* @param param			マテリアルパラメータ
		*/
		void setMaterialParam(MaterialParam& param)override;

		void clear();

	private:
		/**
		* @brief				インスタンスごとに情報を取り出すためのコールバック
		* @param instanceNum	何番目のインスタンスか
		* @param trans			そのループ描画するトランスフォーム
		*/
		virtual void instanceAction(int instanceNum, util::Transform*  trans);
		/**
		* @brief			パイプラインの設定
		*/
		void setPipeline(int drawNum)override;

		void setTexture();

	private:
		std::vector <std::string> m_TextureNames;
		//!シェーダーにテクスチャを配列で渡すための変数
		std::unique_ptr<ID3D11ShaderResourceView*[]> m_pShaderResource;
		std::shared_ptr<Model> m_Model;
		std::vector<std::shared_ptr<Texture2D>> m_Textures;
		CComPtr<ID3D11InputLayout> m_pInputLayout;
		//!更新頻度が高いバッファ
		CComPtr<ID3D11Buffer> m_pAlwaysBuffer;
		//!更新頻度の高いバッファ
		CComPtr<ID3D11Buffer> m_pMaterialBuffer;
		//!更新頻度の低いバッファ
		CComPtr<ID3D11Buffer> m_pSometimeBuffer;
		CComPtr<ID3D11Buffer> m_pSkinBuffer;
		std::shared_ptr<IMaterial> m_pMaterial;
		//!シャドウマップ用マテリアル
		std::shared_ptr<IMaterial> m_pShadowMaterial;
		//!更新用変数
		AlwaysBuffer m_AlwaysBuffer;
		MaterialBuffer m_MaterialBuffer;
		//前フレームのビュー行列
		util::Mat4 m_PreView;

		int m_InstanceID[MAX_DRAW_NUM];


		int m_TextureNum = 0;

		// IInstanceRenderer を介して継承されました
		virtual void depthInstanceAction(int instanceNum, util::Transform * trans) override;
		virtual void setDepthPipeline(int drawNum) override;
	};




}
