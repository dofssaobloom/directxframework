#pragma once
#include<Effekseer.h>
#include<EffekseerRendererDX11.h>
#include<Source\Util\Type.h>
#include<unordered_map>
#include<string>
#include<memory>
#include<Source\Resource\Effect\Effect.h>
#include<unordered_map>

namespace framework {

	class EffectManager
	{
	public:
		~EffectManager();

		void create(int spriteNum = 1000);

		void registPath(const std::string& filePath);

		std::weak_ptr<framework::Effect> generate(const std::string &key, const util::Vec3& pos);
		std::weak_ptr<framework::Effect> generate(const std::string &key, util::Transform& trans);

		void setViewMat(const util::Mat4& mat);

		void setProjMat(const util::Mat4& mat);

		void draw();

		static EffectManager* getInstance();

		void clear();

		void effectStop(Effekseer::Handle& handle);

		void effectPlay(Effekseer::Effect* instance);

		bool isEffectExits(std::weak_ptr<framework::Effect> pEffect);

		/// <summary>
		/// エフェクシアのかリング処理
		/// </summary>
		void culling();


		/// <summary>
		/// エフェクトのアップデートを止める
		/// </summary>
		void updateStop();

		/// <summary>
		/// エフェクトのアップデートを始める
		/// </summary>
		void updateStart();

	private:
		EffectManager();
		void createEffekseerMat(Effekseer::Matrix44* result, const util::Mat4& inMat);
		void checkDelete();
		void effectInstanceUpdate();

	private:
		static std::unique_ptr<EffectManager> m_pInstance;

		EffekseerRenderer::Renderer* m_pRenderer;
		Effekseer::Manager* m_pManager;

		//!エフェクトのテンプレートデータ
		std::unordered_map<std::string,Effekseer::Effect*> m_EffectContainer;
		std::list<std::shared_ptr<framework::Effect>> m_InstanceContainer;

		bool m_IsUpdateStop;
	};;




}