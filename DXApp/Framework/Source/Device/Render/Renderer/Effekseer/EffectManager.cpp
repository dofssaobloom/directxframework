#include"EffectManager.h"
#include<D3DX11.h>
#include<xnamath.h>
#include<Source\Application\Screen\Screen.h>

namespace framework {

	std::unique_ptr<EffectManager> EffectManager::m_pInstance = std::unique_ptr<EffectManager>(new EffectManager());


	EffectManager::EffectManager()
	{
		m_IsUpdateStop = false;
	}

	EffectManager::~EffectManager()
	{
		clear();

		for (auto& c : m_EffectContainer) {
			ES_SAFE_RELEASE(c.second);
		}

		m_EffectContainer.clear();

		if (m_pManager)
			m_pManager->Destroy();
		if (m_pRenderer)
			m_pRenderer->Destroy();

	}

	void EffectManager::create(int spriteNum)
	{
		if (m_pRenderer != nullptr) {
			m_pRenderer->Destroy();
		}
		if (m_pManager != nullptr) {
			m_pManager->Destroy();
		}

		m_pRenderer = ::EffekseerRendererDX11::Renderer::Create(util::getDevice(), util::getContext(), spriteNum);
		m_pManager = ::Effekseer::Manager::Create(spriteNum);


		util::Mat4 projMat = XMMatrixPerspectiveFovLH(
			XMConvertToRadians(Screen::PERSPECTIVE),
			Screen::WINDOW_WIDTH / (float)Screen::WINDOW_HEIGHT,//floatし忘れ注意
			1.0f,
			Screen::FAR_);

		setProjMat(projMat);

		// 描画方法の指定、独自に拡張しない限り定形文です。
		m_pManager->SetSpriteRenderer(m_pRenderer->CreateSpriteRenderer());
		m_pManager->SetRibbonRenderer(m_pRenderer->CreateRibbonRenderer());
		m_pManager->SetRingRenderer(m_pRenderer->CreateRingRenderer());
		m_pManager->SetModelRenderer(m_pRenderer->CreateModelRenderer());
		m_pManager->SetModelLoader(m_pRenderer->CreateModelLoader());
		// テクスチャ画像の読込方法の指定(パッケージ等から読み込む場合拡張する必要があります。)
		m_pManager->SetTextureLoader(m_pRenderer->CreateTextureLoader());
		// 座標系の指定(RHで右手系、LHで左手系)
		m_pManager->SetCoordinateSystem(::Effekseer::CoordinateSystem::LH);


		//m_pManager->CreateCullingWorld(30000.0f, 30000.0f, 30000.0f, 1);

	}


	void EffectManager::registPath(const std::string & filePath)
	{
		util::CSVLoader loader(filePath);

		auto param = loader.load();

		param.erase(param.begin());

		for (auto p : param) {
			auto wPath = util::StringToWString(p[1]);
			auto path = (EFK_CHAR*)(wPath.c_str());

			m_EffectContainer[p[0]] = Effekseer::Effect::Create(m_pManager, path);
		}
	}

	std::weak_ptr<framework::Effect> EffectManager::generate(const std::string & key, const util::Vec3& pos)
	{
		assert(m_EffectContainer.find(key) != m_EffectContainer.end() && "キーが登録されていません");

		auto instance = m_pManager->Play(m_EffectContainer[key], 0, 0, 0);

		auto effect = std::make_shared<framework::Effect>(instance, pos);

		m_InstanceContainer.emplace_back(effect);

		return effect;
	}

	std::weak_ptr<framework::Effect> EffectManager::generate(const std::string & key, util::Transform & trans)
	{
		assert(m_EffectContainer.find(key) != m_EffectContainer.end() && "キーが登録されていません");

		auto instance = m_pManager->Play(m_EffectContainer[key], 0, 0, 0);

		auto effect = std::make_shared<framework::Effect>(instance, trans);

		m_InstanceContainer.emplace_back(effect);

		return effect;
	}

	void EffectManager::setViewMat(const util::Mat4 & mat)
	{
		Effekseer::Matrix44 viewMat;

		createEffekseerMat(&viewMat, mat);
		m_pRenderer->SetCameraMatrix(viewMat.Transpose());
	}

	void EffectManager::setProjMat(const util::Mat4 & mat)
	{
		Effekseer::Matrix44 projMat;

		createEffekseerMat(&projMat, mat);
		m_pRenderer->SetProjectionMatrix(projMat);
	}

	void EffectManager::draw()
	{
		checkDelete();
		effectInstanceUpdate();

		if (!m_IsUpdateStop)
			m_pManager->Update();


		m_pRenderer->BeginRendering();
		//culling();
		m_pManager->Draw();
		m_pRenderer->EndRendering();
	}

	EffectManager * EffectManager::getInstance()
	{
		return m_pInstance.get();
	}

	void EffectManager::clear()
	{
		//for (auto effect : m_InstanceContainer)
		//{
		//	auto handle = effect->getHandle();
		//	m_pManager->StopEffect(handle);
		//}

		m_pManager->StopAllEffects();
		m_InstanceContainer.clear();

	}

	void EffectManager::effectStop(Effekseer::Handle & handle)
	{
		m_pManager->StopEffect(handle);
	}

	void EffectManager::effectPlay(Effekseer::Effect* instance)
	{

	}

	bool EffectManager::isEffectExits(std::weak_ptr<framework::Effect> pEffect)
	{
		auto handle = pEffect.lock()->getHandle();
		bool is = !m_pManager->Exists(handle);
		return  is;
	}

	void EffectManager::culling()
	{
		m_pManager->CalcCulling(m_pRenderer->GetCameraProjectionMatrix(), false);
	}

	void EffectManager::updateStop()
	{
		m_IsUpdateStop = true;
	}

	void EffectManager::updateStart()
	{
		m_IsUpdateStop = false;
	}

	void EffectManager::createEffekseerMat(Effekseer::Matrix44 * result, const util::Mat4 & inMat)
	{
		util::foreach(4, [&](int i) {
			util::foreach(4, [&](int j) {
				result->Values[j][i] = inMat.m[j][i];
			});
		});
	}

	void EffectManager::checkDelete()
	{
		//インスタンスが切れているオブジェクト削除
		m_InstanceContainer.remove_if([&](std::shared_ptr<framework::Effect>& effect) {
			return !effect->isExists(m_pManager);
		});
	}

	void EffectManager::effectInstanceUpdate()
	{
		for (auto effect : m_InstanceContainer) {
			effect->update(m_pManager);
		}
	}

}