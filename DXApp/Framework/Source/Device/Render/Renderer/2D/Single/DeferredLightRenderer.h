#pragma once

#include"ISpriteRenderer.h"


#define MAX_POINT_LIGHT_NUM 160

namespace framework {

	struct DeferredAlways
	{
		XMMATRIX world;
		util::Vec4 directionalLightPosition;
		util::Vec4 directionalLightColor;
		//util::Vec4 lightPos[MAX_POINT_LIGHT_NUM];
		//util::Vec4 attribu[MAX_POINT_LIGHT_NUM];
		//util::Vec4 pointLightColor[MAX_POINT_LIGHT_NUM];
		int activeLightNum;//有効なライトの数
		util::Vec3 cameraPos;
		util::Vec4 ambientColor;
	};

	struct DeferredSometime {
		XMMATRIX projection;
	};

	struct PointLightBuffer {
		util::Vec4 lightPos[MAX_POINT_LIGHT_NUM];
		util::Vec4 attribu[MAX_POINT_LIGHT_NUM];
		util::Vec4 pointLightColor[MAX_POINT_LIGHT_NUM];
	};

	class DeferredLightRenderer : public ISpriteRenderer<DeferredAlways, DeferredSometime>
	{
	public:
		DeferredLightRenderer();
		~DeferredLightRenderer();

	private:

		// ISpriteRenderer を介して継承されました
		virtual void begin() override;

		virtual void end() override;

		virtual void drawBefore()override;

		virtual void setPipeline() override;

	private:
		DeferredAlways ab;

		PointLightBuffer m_LightBuffer;
		//!更新頻度が高いバッファ
		CComPtr<ID3D11Buffer> m_pLightBufferGPU;

	};



}