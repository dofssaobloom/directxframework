#include"DeferredLightRenderer.h"
#include<Source\Component\Effect\DirectionalLightComponent.h>
#include<Source\Component\Effect\PointLightComponent.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Entity\Entity.h>

namespace framework {


	DeferredLightRenderer::DeferredLightRenderer()
		:ISpriteRenderer("DeferredLighting")
	{
		m_pLightBufferGPU = util::createConstantBuffer(sizeof(PointLightBuffer));
	}

	DeferredLightRenderer::~DeferredLightRenderer()
	{
	}

	void DeferredLightRenderer::begin()
	{
	}

	void DeferredLightRenderer::end()
	{

	}

	void DeferredLightRenderer::drawBefore()
	{

		ID3D11Buffer* buffer[1];
		buffer[0] = m_pLightBufferGPU.p;

		m_pMaterial->setConstantBuffer(buffer, 1, 2);
	}

	void DeferredLightRenderer::setPipeline()
	{
		ab.world = m_WoldMat.toXMMatrix();
		util::Vec3&& dPos = component::DirectionalLightComponent::getLightPosition();
		util::Vec3&& dColor = component::DirectionalLightComponent::getLightColor();
		util::Vec3&& dAmbient = component::DirectionalLightComponent::getLightAmbient();
		ab.directionalLightPosition = util::Vec4(dPos.x, dPos.y, dPos.z, 1);
		ab.directionalLightColor = util::Vec4(dColor.x, dColor.y, dColor.z, 1);
		ab.ambientColor = util::Vec4(dAmbient.x, dAmbient.y, dAmbient.z, 1);

		auto&& lightArray = component::PointLightComponent::getLightArray();
		ab.activeLightNum = component::PointLightComponent::getTotalLightNum();

		ab.cameraPos = component::CameraComponent::getMainCamera().lock()->getGameObj().lock()->getTransform()->m_Position;

		util::updateConstantBuffer<DeferredAlways>(m_pAlwaysBuffer.p, ab);

		DeferredSometime sb;
		sb.projection = m_Projection.toXMMatrix();

		util::updateConstantBuffer<DeferredSometime>(m_pSometimeBuffer.p, sb);

		util::foreach(ab.activeLightNum, [&](int i) {
			util::Vec3 pPos = lightArray.get()[i]->position + lightArray.get()[i]->offset;
			m_LightBuffer.lightPos[i] = util::Vec4(pPos.x, pPos.y, pPos.z,1);
			util::Vec3 pColor = lightArray.get()[i]->color;
			m_LightBuffer.pointLightColor[i] = util::Vec4(pColor.x, pColor.y, pColor.z, 1);
			m_LightBuffer.attribu[i].x = lightArray.get()[i]->power;
			m_LightBuffer.attribu[i].y = lightArray.get()[i]->near_;
			m_LightBuffer.attribu[i].z = lightArray.get()[i]->far_;
			m_LightBuffer.attribu[i].w = lightArray.get()[i]->bright;
		});

		util::updateConstantBuffer<PointLightBuffer>(m_pLightBufferGPU.p, m_LightBuffer);

	}


}