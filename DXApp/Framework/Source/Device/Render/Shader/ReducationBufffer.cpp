#include "Framework.h"
#include "ReducationBufffer.h"

namespace framework {

	ReducationBuffer::ReducationBuffer(int x, int y, std::shared_ptr<Texture2D>& tex2D)
	{
		m_Target = std::make_unique<RenderTarget>(9, 9, DXGI_FORMAT_R8G8B8A8_UNORM);
		m_Renderer.setTexture(tex2D);
		m_Renderer.setSize(util::Vec2(Screen::WINDOW_WIDTH, Screen::WINDOW_HEIGHT));
	}

	ReducationBuffer::~ReducationBuffer()
	{
	}

	void ReducationBuffer::write(int viewNum)
	{
		//現在のターゲットを一時退避
		util::RenderTargetStack stack(viewNum);

		auto size = m_Target->getSize();
		util::setSingleViewPort(Screen::WINDOW_WIDTH, Screen::WINDOW_HEIGHT);
		util::getContext()->OMSetRenderTargets(1, m_Target->getView(), NULL);
		m_Target->clear(util::Vec4(0, 0, 0, 1));

		util::Transform trans(util::Vec3(Screen::WINDOW_WIDTH_HALF, Screen::WINDOW_HEIGHT_HALF, 0), util::Vec3(0, 0, 0), util::Vec3(140, 80, 1));
		m_Renderer.draw(&trans);
	}

	std::shared_ptr<Texture2D> ReducationBuffer::getReducationTex2D()
	{
		auto&& size = m_Target->getSize();
		return std::make_shared<Texture2D>(size.x, size.y, m_Target->getShaderView());
	}

}
