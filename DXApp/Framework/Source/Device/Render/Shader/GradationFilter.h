#pragma once
#include<Source\Resource\Texture\RenderTarget.h>
#include<Source\Device\Render\Renderer\2D\Single\PostEffectRenderer.h>
#include<Source\Util\Type.h>
#include<memory>
#include<atlbase.h>

namespace framework {

	class Texture2D;

	class GradationFilter
	{
	public:
		enum class BlurType
		{
			Vertical,
			Horizontal
		};

		GradationFilter(std::shared_ptr<Texture2D> tex2D);
		GradationFilter(std::shared_ptr<Texture2D> tex2D, float scale);
		~GradationFilter();

		/**
		* @brief				内部テクスチャに書き込み
		* @param source			ぼかす元テクスチャのシェーダーview
		* @param viewNum	    現在セットされているレンダーターゲットの数
		*/
		void write(int viewNum);

		/**
		* @brief				描画
		*/
		void write();
		
		/**
		* @brief				シェーダーview取得
		*/
		CComPtr<ID3D11ShaderResourceView> getShaderView();

		/**
		* @brief				tex2D取得
		*/
		std::shared_ptr<Texture2D> getTex2D();

		/**
		* @brief				描画
		*/
		void draw();

		util::Vec2 getTargetSize()const;

		//ずらし値のセット
		void setShift(float shift);

		/// <summary>
		/// ぼかし方向設定
		/// </summary>
		/// <param name=""></param>
		void setBlurType(BlurType);

		/// <summary>
		/// オフセット設定
		/// </summary>
		/// <param name="offset"></param>
		void setOffset(float offset);

	private:
		
		/// <summary>
		/// ガウスの重み計算
		/// </summary>
		void culcGaussWeight(float dispersion);
		

	private:
		//グラデーションフィルタの定数バッファ
		struct GradationConstant {
			XMMATRIX world;
			//!ピクセルのサイズ
			util::Vec2 damy;
			float offset;
			float isVertical;
			util::Vec4 weight0;
			util::Vec4 weight1;
		};


		std::unique_ptr<RenderTarget> m_Target;
		PostEffectRenderer<GradationConstant> renderer;
		std::shared_ptr<Texture2D> m_pTexture2D;
		GradationConstant m_Param;
		float m_Shift;
		float m_Scale;
	};

}
