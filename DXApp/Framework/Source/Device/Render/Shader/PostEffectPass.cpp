#include"PostEffectPass.h"
#include<Source\Component\Effect\PostEffectComponent.h>
#include<Source\Resource\Texture\MultiRenderTarget.h>
#include<Source\Resource\Texture\MSAAMultiRenderTarget.h>
#include<Source\Resource\Texture\DepthTarget.h>

namespace framework {

	PostEffectPass::PostEffectPass(EffectContainer effects, std::shared_ptr<framework::MultiRenderTarget> gBuffer, std::shared_ptr<framework::MultiRenderTarget> lBuffer)
		:m_EffectContainer(effects),
		m_pGBuffer(gBuffer),
		m_pLBuffer(lBuffer)
	{
		m_SwapTarget.reserve(2);
		//m_SwapTarget.emplace_back(std::make_shared<RenderTarget>(Screen::PIXEL_WIDTH, Screen::PIXEL_HEIGHT, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM));
		//m_SwapTarget.emplace_back(std::make_shared<RenderTarget>(Screen::PIXEL_WIDTH, Screen::PIXEL_HEIGHT, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM));

		//TODO トーンマッピングが完成したらHDRに切り替える
		m_SwapTarget.emplace_back(std::make_shared<RenderTarget>(Screen::PIXEL_WIDTH, Screen::PIXEL_HEIGHT, DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_FLOAT));
		m_SwapTarget.emplace_back(std::make_shared<RenderTarget>(Screen::PIXEL_WIDTH, Screen::PIXEL_HEIGHT, DXGI_FORMAT::DXGI_FORMAT_R16G16B16A16_FLOAT));
	
		m_TextureContainer["Specular"] = std::make_shared<framework::Texture2D>(framework::Screen::PIXEL_WIDTH,
			framework::Screen::PIXEL_HEIGHT, lBuffer->getShaderView(1));
		m_TextureContainer["ShadowDepth"] = std::make_shared<framework::Texture2D>(framework::Screen::PIXEL_WIDTH,
			framework::Screen::PIXEL_HEIGHT, gBuffer->getShaderView(4));
		m_TextureContainer["Velocity"] = std::make_shared<framework::Texture2D>(framework::Screen::PIXEL_WIDTH,
			framework::Screen::PIXEL_HEIGHT, gBuffer->getShaderView(7));
	}

	PostEffectPass::~PostEffectPass()
	{
	}

	void PostEffectPass::setDepth(std::shared_ptr<framework::DepthTarget>& inDepth)
	{
		if (m_TextureContainer.end() != m_TextureContainer.find("Depth"))return;
		m_TextureContainer["Depth"] = std::make_shared<framework::Texture2D>(framework::Screen::PIXEL_WIDTH,
			framework::Screen::PIXEL_HEIGHT, inDepth->getShaderView());
	}

	void PostEffectPass::rendering()
	{
		m_TextureContainer["Main"] = std::make_shared<framework::Texture2D>(framework::Screen::PIXEL_WIDTH,
			framework::Screen::PIXEL_HEIGHT, m_pLBuffer->getShaderView(0));

		for (auto& effect : m_EffectContainer) {

			//テクスチャの選択
			effect.lock()->selectTexture(m_TextureContainer);

			util::getContext()->OMSetRenderTargets(1, m_SwapTarget[0]->getView(), NULL);

			m_SwapTarget[0]->clear(util::Vec4(0, 0, 0, 1));

			effect.lock()->doEffect(util::Vec2(Screen::WINDOW_WIDTH_HALF, Screen::WINDOW_HEIGHT_HALF));

			util::getContext()->OMSetRenderTargets(0, NULL, NULL);

			//メインとして使うターゲットを更新

			m_SwapTarget[1]->copy(*(m_SwapTarget[0].get()));

			m_TextureContainer["Main"] = std::make_shared<framework::Texture2D>(framework::Screen::PIXEL_WIDTH,
				framework::Screen::PIXEL_HEIGHT, m_SwapTarget[1]->getShaderView());
		}
	}

	void PostEffectPass::draw()
	{
		m_SwapTarget[0]->draw(util::Vec2(Screen::WINDOW_WIDTH_HALF, Screen::WINDOW_HEIGHT_HALF));
	}

	void PostEffectPass::addEffect(const std::weak_ptr<component::PostEffectComponent>& effect)
	{
		//同じエフェクトがあったら追加しない
		for (auto& e : m_EffectContainer) {
			if (e.lock().get() == effect.lock().get())
				return;
		}

		m_EffectContainer.emplace_back(effect);
	}

	void PostEffectPass::removeEffect(const std::weak_ptr<component::PostEffectComponent>& effect)
	{
		m_EffectContainer.remove_if([&](const std::weak_ptr<component::PostEffectComponent>& inEffect) {
			return inEffect.lock().get() == effect.lock().get();
		});
	}

}