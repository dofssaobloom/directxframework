#include"GradationFilter.h"
#include<Source\Util\Type.h>
#include<Source\Util\Render\DXFunc.h>
#include<Source\Application\Screen\Screen.h>
#include<Source\Util\Render\RenderTargetStack.h>
#include<Source\Util\Math\Gauss.h>

namespace framework {
	GradationFilter::GradationFilter(std::shared_ptr<Texture2D> tex2D)
		:GradationFilter(tex2D,1)
	{

	}
	GradationFilter::GradationFilter(std::shared_ptr<Texture2D> tex2D,float scale)
		:renderer("Gradation")
		, m_pTexture2D(tex2D),
		m_Scale(scale),
		m_Shift(1.0f)
	{
		auto size = tex2D->getSize();
		m_Target = std::make_unique<RenderTarget>(size.x/ m_Scale,size.y/ m_Scale,DXGI_FORMAT_R8G8B8A8_UNORM);
		renderer.setSize(util::Vec2(Screen::WINDOW_WIDTH,Screen::WINDOW_HEIGHT));
		m_Param.isVertical = 1;
		m_Param.offset = 16;

		renderer.setBuffer(m_Param);
		renderer.setTexture(m_pTexture2D);
	}

	GradationFilter::~GradationFilter()
	{
	}

	void GradationFilter::write(int viewNum)
	{
		//現在セットされている描画ターゲットを一時退避
		util::RenderTargetStack stack(viewNum);
		write();
	}

	void GradationFilter::write()
	{
		auto size = m_Target->getSize();
		util::setSingleViewPort(size.x, size.y);
		util::getContext()->OMSetRenderTargets(1, m_Target->getView(), NULL);
		m_Target->clear(util::Vec4(0,0,0,1));

		renderer.setBuffer(m_Param);

		util::Transform trans(util::Vec3(Screen::WINDOW_WIDTH_HALF, Screen::WINDOW_HEIGHT_HALF, 0), util::Vec3(0, 0, 0), util::Vec3(1, 1, 1));
		renderer.draw(&trans);

		util::getContext()->OMSetRenderTargets(0, NULL, NULL);
	}

	CComPtr<ID3D11ShaderResourceView> GradationFilter::getShaderView()
	{
		return m_Target->getShaderView();
	}

	std::shared_ptr<Texture2D> GradationFilter::getTex2D()
	{
		return m_pTexture2D;
	}

	void GradationFilter::draw()
	{
		util::Transform trans(util::Vec3(Screen::WINDOW_WIDTH_HALF, Screen::WINDOW_HEIGHT_HALF,0) , util::Vec3(0,0,0) ,util::Vec3(m_Scale, m_Scale, m_Scale));
		m_Target->draw(&trans);
	}

	util::Vec2 GradationFilter::getTargetSize() const
	{
		return m_Target->getSize();
	}

	void GradationFilter::setShift(float shift)
	{
		m_Shift = shift;
		culcGaussWeight(pow(m_Shift, 2));
	}

	void GradationFilter::setBlurType(BlurType type)
	{
		if (BlurType::Vertical == type) {
			m_Param.isVertical = true;
		}
		else if (BlurType::Horizontal == type)
		{
			m_Param.isVertical = false;
		}
	}

	void GradationFilter::setOffset(float offset)
	{
		m_Param.offset = offset;
	}

	void GradationFilter::culcGaussWeight(float dispersion)
	{
		float total = 0.0f;

		//ウェイトの数
		const int numWeight = 8;

		float gaussWeight[8];

		for (size_t i = 0; i < numWeight; i++)
		{
			float pos = 1.0f + 2.0f * (float)i;
			gaussWeight[i] = expf(-0.5f * pow(pos,2) / dispersion);
			total += 2.0f * gaussWeight[i];
		}
		
		float invTotal = 1.0f / total;
		for (size_t i = 0; i < numWeight; i++)
		{
			gaussWeight[i] *= invTotal;
		}

		//hlsl用に変換する
		m_Param.weight0.x = gaussWeight[0];
		m_Param.weight0.y = gaussWeight[1];
		m_Param.weight0.z = gaussWeight[2];
		m_Param.weight0.w = gaussWeight[3];

		m_Param.weight1.x = gaussWeight[4];
		m_Param.weight1.y = gaussWeight[5];
		m_Param.weight1.z = gaussWeight[6];
		m_Param.weight1.w = gaussWeight[7];

	}
}