#pragma once
#include<Framework.h>
#include<Source\Device\Render\Renderer\2D\Single\SpriteRenderer.h>

namespace framework {

	/// <summary>
	/// 縮小バッファクラス
	/// </summary>
	class ReducationBuffer
	{
	public:
		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="x">縮小サイズ</param>
		/// <param name="y">縮小サイズ</param>
		/// <param name="tex2D">元画像</param>
		ReducationBuffer(int x,int y,std::shared_ptr<Texture2D>& tex2D);
		~ReducationBuffer();
		

		/// <summary>
		/// 縮小バッファへ書き込み実行
		/// </summary>
		/// 現在セットされているレンダーターゲットの数
		void write(int viewNum);

		/// <summary>
		/// 縮小バッファ取得
		/// </summary>
		/// <returns></returns>
		std::shared_ptr<Texture2D> getReducationTex2D();

	private:
		std::unique_ptr<RenderTarget> m_Target;
		//!元データ
		std::shared_ptr<Texture2D> m_pTexture2D;
		SpriteRenderer m_Renderer;

	};

}