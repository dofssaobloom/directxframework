#define DIRECTINPUT_VERSION 0x0800
#include "Framework.h"
#include "KeyBoard.h"
#define INITGUID
#include<assert.h>

namespace framework {

	KeyBoard::KeyBoard(HINSTANCE hInst)
	{
		auto hr = FAILED(DirectInput8Create(hInst, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_pDinput, NULL));

		assert(!hr && "キーボードインプットデバイスの作成失敗");

		hr = m_pDinput->CreateDevice(
			GUID_SysKeyboard,	// キーボードを受け付ける
			&m_pKey,		// IDirectInputDevice8ポインタ
			NULL);

		assert(!hr && "キーデバイスの作成失敗");

		hr = m_pKey->SetDataFormat(&c_dfDIKeyboard);

		assert(!hr && "フォーマット作成失敗");
	}

	KeyBoard::~KeyBoard()
	{
		m_pDinput->Release();
		m_pKey->Release();
	}

	void KeyBoard::update()
	{
		m_pKey->Acquire();

		preCopy();
		m_pKey->GetDeviceState(sizeof(m_CurrentState),&m_CurrentState);
	}

	bool KeyBoard::isKeyDown(KeyCords keys)
	{
		return m_CurrentState[static_cast<unsigned short>( keys)] & 0x80;
	}

	bool KeyBoard::isKeyDownTrriger(KeyCords keys)
	{
		return isKeyDown(keys) && !(m_PreState[static_cast<unsigned short>(keys)] & 0x80);
	}

	bool KeyBoard::isKeyUpTrriger(KeyCords keys)
	{
		return !isKeyDown(keys) && (m_PreState[static_cast<unsigned short>(keys)] & 0x80);
	}

	void KeyBoard::preCopy()
	{
		util::foreach(256, [&](int i) {
			m_PreState[i] = m_CurrentState[i];
		});
	}

}