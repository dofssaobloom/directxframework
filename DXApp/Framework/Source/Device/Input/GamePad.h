#pragma once
//デフォルト敷居値
#define DEFAULT_THRESHOLD 100 
#include<Windows.h>
#include<XInput.h>
#include<memory>
#include<functional>
#include<Source\Util\Type.h>
#include<unordered_map>
#include<functional>
#include<iostream>

/**
* @file	GamePad.h
* @brief	ゲームパッドクラス
* @authro	高須優輝
* @date	2017/03/27
*/

namespace framework {

	enum class ButtonCode : unsigned short
	{
		A = XINPUT_GAMEPAD_A,
		B = XINPUT_GAMEPAD_B,
		Y = XINPUT_GAMEPAD_Y,
		X = XINPUT_GAMEPAD_X,
		//!スタートボタンを押したとき
		Start = XINPUT_GAMEPAD_START,
		//!バックボタンを押したとき
		Back = XINPUT_GAMEPAD_BACK,
		//!右スティックを押し込んだとき
		RightThumb = XINPUT_GAMEPAD_RIGHT_THUMB,
		//!左スティックを押し込んだとき
		LeftThumb = XINPUT_GAMEPAD_LEFT_THUMB,
		//!RB
		RightShouler = XINPUT_GAMEPAD_RIGHT_SHOULDER,
		//!LB
		LeftShouler = XINPUT_GAMEPAD_LEFT_SHOULDER,

	};

	//ButtonCode begin(ButtonCode) {
	//	return ButtonCode::A;
	//}

	//ButtonCode end(ButtonCode) {
	//	return ButtonCode::LeftShouler;
	//}
	//ButtonCode operator*(ButtonCode code) { return code; }
	//ButtonCode operator++(ButtonCode& code) {
	//	return code = ButtonCode(std::underlying_type<ButtonCode>::type(code) + 1);
	//}

	enum class StickDir : unsigned short
	{
		Right,
		Left,
		Up,
		Down,
	};

	struct ControllerState {
		//!コントローラの状態
		XINPUT_STATE state;
		//!接続されているかどうか
		bool isConect;
	};

	class GamePad {
	public:
		/**
		* @brief	基底コンストラクタ
		* @dital	基底ではゲームパッドの数は4個
		*/
		GamePad();
		/**
		* @brief				引数付きコンストラクタ
		* @parma  controllerNum	接続できるコントローラの最大数
		*/
		GamePad(int controllerNum);
		~GamePad();

		/**
		* @brief				ゲームパッドの状態更新
		*/
		void updateController();

		/**
		* @brief				ボタンが押されているか確認
		* @parma  code			押されているか確かめたいボタン
		* @parma  padLocation	押されているか確かめたいボタン
		*/
		bool getButton(ButtonCode code, int padLocation = 0);

		/**
		* @brief				ボタンが押されているかトリガー判定
		* @parma  code			押されているか確かめたいボタン
		* @parma  padLocation	押されているか確かめたいボタン
		*/
		bool getButtonTrriger(ButtonCode code, int padLocation = 0);

		/**
		* @brief				ボタンが上がった瞬間
		* @parma  code			押されているか確かめたいボタン
		* @parma  padLocation	押されているか確かめたいボタン
		*/
		bool getUpButtonTrriger(ButtonCode code, int padLocation = 0);


		bool isRightStick(StickDir dir);

		bool isLeftStick(StickDir dir);

		/// <summary>
		/// LTが押されたかどうか
		/// </summary>
		/// <param name="threshold">敷居値　この値よりアナログ値が高くなければ押したことにならない</param>
		/// 0 ~ 255の範囲
		/// <returns></returns>
		bool isPushLT(float threshold = DEFAULT_THRESHOLD, int padLocation = 0);

		/// <summary>
		/// RTが押されたかどうか
		/// </summary>
		/// <param name="threshold">敷居値　この値よりアナログ値が高くなければ押したことにならない</param>
		/// 0 ~ 255の範囲
		/// <returns></returns>
		bool isPushRT(float threshold = DEFAULT_THRESHOLD, int padLocation = 0);

		/// <summary>
		/// LTが押されたかどうか トリガー判定
		/// </summary>
		/// <param name="threshold">敷居値　この値よりアナログ値が高くなければ押したことにならない</param>
		/// 0 ~ 255の範囲
		/// <returns></returns>
		bool isPushLTTrriger(float threshold = DEFAULT_THRESHOLD, int padLocation = 0);

		/// <summary>
		/// LTが離された瞬間
		/// </summary>
		/// <param name="threshold"></param>
		/// <param name="padLocation"></param>
		/// <returns></returns>
		bool isUpLTTrriger(float threshold = DEFAULT_THRESHOLD, int padLocation = 0);


		/// <summary>
		/// RTが押されたかどうか トリガー判定
		/// </summary>
		/// <param name="threshold">敷居値　この値よりアナログ値が高くなければ押したことにならない</param>
		/// 0 ~ 255の範囲
		/// <returns></returns>
		bool isPushRTTrriger(float threshold = DEFAULT_THRESHOLD, int padLocation = 0);

		/// <summary>
		/// RTが離された瞬間
		/// </summary>
		/// <param name="threshold"></param>
		/// <param name="padLocation"></param>
		/// <returns></returns>
		bool isUpRTTrriger(float threshold = DEFAULT_THRESHOLD, int padLocation = 0);

		/**
		* @brief				左スティックの数値取得
		* @detail				-32768〜32767の範囲
		*/
		util::Vec2 getLeftStick(int padLocation = 0);

		/**
		* @brief				右スティックの数値取得
		* @detail				-32768〜32767の範囲
		*/
		util::Vec2 getRightStick(int padLocation = 0);

		/**
		* @brief				LTのアナログ値取得
		* @padLocation			コントローラの番号
		* @detail				0 ~ 255の範囲
		*/
		int getAnalogLT(int padLocation = 0);

		/**
		* @brief				RTのアナログ値取得
		* @detail				0 ~ 255の範囲
		*/
		int getAnalogRT(int padLocation = 0);

		/**
		* @brief				入力があるかどうか
		*/
		bool isStickNoe(const util::Vec2& vec);

		const int getConectNum();

	public:
		static const short m_MaxStickNum = 32767;
		static const short m_MinStickNum = -32768;
		static const short m_MaxHalfStickNum = m_MaxStickNum / 2;
		static const short m_MinHalfStickNum = m_MinStickNum / 2;

	private:
		bool isConnect(int location);
		void padEach(const std::function<void(DWORD i)>& action);
		util::Vec2 noizeCut(const util::Vec2& vec);

		/// <summary>
		/// -1 ~ 1の間に保管する
		/// </summary>
		/// <param name="vec"></param>
		void clampStick(util::Vec2* vec);

	private:
		const unsigned int m_MaxControllers;
		std::unique_ptr<ControllerState[]> m_pControllers;
		std::unique_ptr<ControllerState[]> m_pPreControllers;

		//	short int m_DownFrame[256];

		std::unordered_map<StickDir, std::function<bool(const util::Vec2& vec)>> m_StickAction;
	};





}