#pragma once
#include"GamePad.h"
#include<functional>
#include<list>
#include<utility>
#include<Source\Device\Input\KeyBoard.h>
#include<memory>

namespace framework {

	class Input
	{
	public:
		~Input();

		static GamePad& getGamePad();

		static IKeyBoard& getKey();

		static void update();

		static void createInstance(HINSTANCE hInst);



	private:
		Input();

	private:
		static GamePad m_GamePad;
		static std::unique_ptr<KeyBoard> m_pKey;
	};


}