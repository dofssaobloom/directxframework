#include"Input.h"


namespace framework {

	//TODO パッドの数を変えられるようにする
	GamePad Input::m_GamePad(1);
	std::unique_ptr<KeyBoard> Input::m_pKey = nullptr;

	Input::Input()
	{
	}

	Input::~Input()
	{
	}

	GamePad& Input::getGamePad()
	{
		return m_GamePad;
	}

	IKeyBoard & Input::getKey()
	{
		return *(m_pKey.get());
	}

	void Input::update()
	{
		m_GamePad.updateController();
		m_pKey->update();
	}

	void Input::createInstance(HINSTANCE hInst)
	{
		m_pKey = std::make_unique<KeyBoard>(hInst);
	}
}