#pragma once
#include <dinput.h>
#include<atlbase.h>


namespace framework {

	class IKeyBoard abstract
	{
	public:
		/// <summary>
		/// キーボード入力一覧
		/// </summary>
		enum class KeyCords :unsigned short
		{
			A = DIK_A,
			B = DIK_B,
			C = DIK_C,
			D = DIK_D,
			E = DIK_E,
			F = DIK_F,
			G = DIK_G,
			H = DIK_H,
			I = DIK_I,
			J = DIK_J,
			K = DIK_K,
			L = DIK_L,
			N = DIK_N,
			M = DIK_M,
			O = DIK_O,
			P = DIK_P,
			Q = DIK_Q,
			R = DIK_R,
			S = DIK_S,
			T = DIK_T,
			U = DIK_U,
			V = DIK_V,
			W = DIK_W,
			X = DIK_X,
			Y = DIK_Y,
			Z = DIK_Z,
			SPACE = DIK_SPACE,
			ESCAPE = DIK_ESCAPE,
			UP = DIK_UP,
			DOWN = DIK_DOWN,
			RIGHT = DIK_RIGHT,
			LEFT = DIK_LEFT,
			LSHIFT = DIK_LSHIFT,
			RSHIFT = DIK_RSHIFT,
			ENTER = DIK_RETURN,
			BACK = DIK_BACKSPACE,
		};

		virtual bool isKeyDown(KeyCords keys) = 0;

		virtual bool isKeyDownTrriger(KeyCords keys) = 0;

		virtual bool isKeyUpTrriger(KeyCords keys) = 0;

	};

	class KeyBoard : public IKeyBoard
	{
	public:

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="hInst">ウィンドウインスタンス</param>
		KeyBoard(HINSTANCE hInst);
		~KeyBoard();

		void update();

		/// <summary>
		/// ボタンが入力されたかどうか
		/// </summary>
		/// <param name="keys">確かめたいボタンのコード</param>
		/// <returns>true = 押されてる　falseおされていない</returns>
		bool isKeyDown(KeyCords keys)override;

		/// <summary>
		/// ボタンが入力されたかどうかトリガー判定
		/// </summary>
		/// <param name="keys">確かめたいボタンのコード</param>
		/// <returns>true = 押されてる　falseおされていない</returns>
		bool isKeyDownTrriger(KeyCords keys)override;


		/// <summary>
		/// キーを離した瞬間
		/// </summary>
		/// <param name="keys"></param>
		/// <returns></returns>
		bool isKeyUpTrriger(KeyCords keys)override;

		/// <summary>
		/// プレステートにコピー
		/// </summary>
		void preCopy();

	private:
		LPDIRECTINPUT8 m_pDinput;
		LPDIRECTINPUTDEVICE8 m_pKey;
		//入力を取得
		BYTE m_CurrentState[256];
		BYTE m_PreState[256];
	};
}