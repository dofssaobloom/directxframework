#include"GamePad.h"
#include<Source\Util\WrapFunc.h>
#include<assert.h>

namespace framework {

	GamePad::GamePad()
		:GamePad(1)
	{
	}

	GamePad::GamePad(int controllerNum)
		: m_MaxControllers(controllerNum)
	{
		m_pControllers = std::make_unique<ControllerState[]>(m_MaxControllers);
		m_pPreControllers = std::make_unique<ControllerState[]>(m_MaxControllers);
		updateController();


		m_StickAction[StickDir::Up] = [&](const util::Vec2& vec) {
			const float half = 0.5f;
			return vec.y > half;
		};

		m_StickAction[StickDir::Down] = [&](const util::Vec2& vec) {
			const float half = 0.5f;
			return vec.y <- half;
		};

		m_StickAction[StickDir::Right] = [&](const util::Vec2& vec) {
			const float half = 0.5f;
			return vec.x > half;
		};

		m_StickAction[StickDir::Left] = [&](const util::Vec2& vec) {
			const float half = 0.5f;
			return vec.x < -half;
		};


		//for (auto code : ButtonCode())
		//{
		//	m_DownFrame[(short int)code] = 0;
		//}
	}

	GamePad::~GamePad()
	{
	}

	void GamePad::updateController()
	{
		padEach([&](DWORD i) {
			DWORD dwResult;
			m_pPreControllers[i].state = m_pControllers[i].state;
			m_pPreControllers[i].isConect = m_pControllers[i].isConect;
			dwResult = XInputGetState(i, &m_pControllers[i].state);
			m_pControllers[i].isConect = dwResult == ERROR_SUCCESS;
		});
	}

	bool GamePad::getButton(ButtonCode code, int padLocation)
	{
		//trueでなければ問題があるので何もしない
		if (!isConnect(padLocation))return false;

		WORD buttons = m_pControllers[padLocation].state.Gamepad.wButtons;
		
		return buttons & static_cast<unsigned short>(code);
	}

	bool GamePad::getButtonTrriger(ButtonCode code, int padLocation)
	{
		//trueでなければ問題があるので何もしない
		if (!isConnect(padLocation))return false;

		WORD buttons = m_pControllers[padLocation].state.Gamepad.wButtons;
		WORD preButtons = m_pPreControllers[padLocation].state.Gamepad.wButtons;

		bool isDown = buttons & static_cast<unsigned short>(code);
		bool isPreDown = preButtons & static_cast<unsigned short>(code);
		return isDown && !isPreDown;
	}

	bool GamePad::getUpButtonTrriger(ButtonCode code, int padLocation)
	{
		//trueでなければ問題があるので何もしない
		if (!isConnect(padLocation))return false;

		WORD buttons = m_pControllers[padLocation].state.Gamepad.wButtons;
		WORD preButtons = m_pPreControllers[padLocation].state.Gamepad.wButtons;

		bool isDown = buttons & static_cast<unsigned short>(code);
		bool isPreDown = preButtons & static_cast<unsigned short>(code);
		return !isDown && isPreDown;
	}

	bool GamePad::isRightStick(StickDir dir)
	{
		auto&& right = getRightStick();

		return m_StickAction[dir](right);
	}

	bool GamePad::isLeftStick(StickDir dir)
	{
		auto&& left = getLeftStick();

		return m_StickAction[dir](left);
	}

	bool GamePad::isPushLT(float threshold, int padLocation)
	{
		return getAnalogLT(padLocation) >= threshold;
	}

	bool GamePad::isPushRT(float threshold, int padLocation)
	{
		return getAnalogRT(padLocation) >= threshold;
	}

	bool GamePad::isPushLTTrriger(float threshold, int padLocation)
	{
		if (!isConnect(padLocation))return false;

		bool isCurrentTrriger = isPushLT(threshold);

		auto& pad = m_pPreControllers[padLocation].state.Gamepad;

		bool isPreTrriger = pad.bLeftTrigger >= threshold;

		return isCurrentTrriger && !isPreTrriger;
	}

	bool GamePad::isUpLTTrriger(float threshold, int padLocation)
	{
		if (!isConnect(padLocation))return false;

		bool isCurrentTrriger = isPushLT(threshold);

		auto& pad = m_pPreControllers[padLocation].state.Gamepad;

		bool isPreTrriger = pad.bLeftTrigger >= threshold;

		return !isCurrentTrriger && isPreTrriger;
	}

	bool GamePad::isPushRTTrriger(float threshold, int padLocation)
	{
		if (!isConnect(padLocation))return false;

		bool isCurrentTrriger = isPushRT(threshold);

		auto& pad = m_pPreControllers[padLocation].state.Gamepad;

		bool isPreTrriger = pad.bRightTrigger >= threshold;

		return isCurrentTrriger && !isPreTrriger;
	}

	bool GamePad::isUpRTTrriger(float threshold, int padLocation)
	{
		if (!isConnect(padLocation))return false;

		bool isCurrentTrriger = isPushRT(threshold);

		auto& pad = m_pPreControllers[padLocation].state.Gamepad;

		bool isPreTrriger = pad.bRightTrigger >= threshold;

		return !isCurrentTrriger && isPreTrriger;
	}

	util::Vec2 GamePad::getLeftStick(int padLocation)
	{
		//trueでなければ問題があるので何もしない
		if (!isConnect(padLocation))return util::Vec2();
		auto& pad = m_pControllers[padLocation].state.Gamepad;

		util::Vec2 left(pad.sThumbLX, pad.sThumbLY);

		clampStick(&left);

		return noizeCut(left);
	}

	util::Vec2 GamePad::getRightStick(int padLocation)
	{
		//trueでなければ問題があるので何もしない
		if (!isConnect(padLocation))return util::Vec2();
		auto& pad = m_pControllers[padLocation].state.Gamepad;

		util::Vec2 right(pad.sThumbRX, pad.sThumbRY);

		clampStick(&right);

		return noizeCut(right);
	}

	int GamePad::getAnalogLT(int padLocation)
	{
		if (!isConnect(padLocation))return 0;

		auto& pad = m_pControllers[padLocation].state.Gamepad;

		return pad.bLeftTrigger;
	}

	int GamePad::getAnalogRT(int padLocation)
	{
		if (!isConnect(padLocation))return 0;

		auto& pad = m_pControllers[padLocation].state.Gamepad;

		return pad.bRightTrigger;
	}

	bool GamePad::isStickNoe(const util::Vec2 & vec)
	{
		//xy両方とも一定以上入力されていなければtrue
		if (vec.x == 0 && vec.y == 0)
			return true;
		return false;
	}

	const int GamePad::getConectNum()
	{
		int count = 0;
		padEach([&](DWORD i) {
			if (m_pControllers[i].isConect) {
				count++;
			}
		});
		return count;
	}

	bool GamePad::isConnect(int location)
	{
		assert(location < m_MaxControllers && "パッドの最大数を超えています");
		if (location > m_MaxControllers)return false;
		//接続されていなければ何もしない
		return m_pControllers[location].isConect;
	}

	void GamePad::padEach(const std::function<void(DWORD i)>& action)
	{
		util::foreach<DWORD>(m_MaxControllers, [&](int i) {
			action(i);
		});
	}

	util::Vec2 GamePad::noizeCut(const util::Vec2 & vec)
	{
		util::Vec2 result(0,0);
		
		//指定した以上の数値でなければ0を返す
		if (abs(vec.x) >= 0.2) {
			result.x = vec.x;
		}
		if (abs(vec.y) >= 0.2) {
			result.y = vec.y;
		}
		return result;
	}

	void GamePad::clampStick(util::Vec2 * vec)
	{
		//範囲が多きすぎるので-1 ~ 1に保管
		float xRate = abs(vec->x) / m_MaxStickNum;
		float x = util::lerp<float>(xRate, 0.0f, 1.0f) * util::sign(vec->x);

		float yRate = abs(vec->y) / m_MaxStickNum;
		float y = util::lerp<float>(yRate, 0.0f, 1.0f) * util::sign(vec->y);
		vec->x = x;
		vec->y = y;
	}




}