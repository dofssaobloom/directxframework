#include"Entity.h"
#include<assert.h>
#include<algorithm>
#include<functional>
#include<iostream>
#include<Source\Component\Physics\PhysicsComponent.cpp>

namespace framework {

	Entity::EntityContainer Entity::m_EntityContainer;
	std::list<WeakEntity> Entity::m_DeleteContainer;

	Entity::Entity(std::string name, std::string tag, util::Transform transform)
		:m_Name(name),
		m_Tag(tag),
		m_pTransform(transform),
		m_InitTransform(transform)
	{
		m_pTransform.setUserPointer(this);
		//std::hash<std::string> hash;
		//m_Hash = hash(name);
	}

	Entity::Entity(const Entity & other)
	{
		for (auto& comp : other.m_ComponentList)
		{
			//auto a = decltype(*comp);
			m_ComponentList.emplace_back(std::make_shared<Component>(*comp));
		}

		for (auto& comp : m_ComponentList)
		{
			comp->active();
			comp->onConect();
			comp->init();
		}
	}

	Entity::~Entity()
	{
		//逆から消していく
		m_ComponentList.sort();

		for (auto& component : m_ComponentList) {
			component->clear();
		}
		m_ComponentList.clear();
	}

	util::Transform * framework::Entity::getTransform()
	{
		return &m_pTransform;
	}

	const std::string& Entity::getName()
	{
		return m_Name;
	}

	const std::string& Entity::getTag()
	{
		return m_Tag;
	}

	void Entity::active()
	{
		m_IsActive = true;
		//自身のdeActiveで非アクティブになったコンポーネントをすべてアクティブにする
		if (m_ComponentList.empty())return;

		for (auto& component : m_ComponentList) {
			auto state = m_ComponentStateManager.popComponentState(component);
			if (state == ComponentState::notFind) continue;

			auto stateFunc = state == ComponentState::active ? std::bind(&Component::active, component) : std::bind(&Component::deActive, component);
			stateFunc();
		}
	}

	void Entity::deActive()
	{
		m_IsActive = false;
		//コンポーネントをすべて非アクティブにする
		for (auto component : m_ComponentList) {
			m_ComponentStateManager.registComponent(component);
			component->deActive();
		}
	}

	bool Entity::isActive()
	{
		return m_IsActive;
	}

	void Entity::onCollisionEnter(const HitData& other)
	{
		for (auto& component : m_ComponentList) {
			component->onCollisionEnter(other);
		}
	}

	void Entity::onCollisionStay(const HitData& other)
	{
		for (auto& component : m_ComponentList) {
			component->onCollisionStay(other);
		}
	}

	void Entity::onCollisionExit(const HitData& other)
	{
		for (auto& component : m_ComponentList) {
			//component->onCollisionExit(other);
		}
	}

	std::weak_ptr<Component> Entity::addComponent(std::shared_ptr<Component> component)
	{
		//component->onConect();
		m_ComponentList.emplace_back(component);
		return component;
	}

	void Entity::addEvent(const std::string & key, std::function<void(void)> eventFunc)
	{
		m_Event.addEvent(key, eventFunc);
	}

	void Entity::onEvent(const std::string & key)
	{
		m_Event.onEvent(key);
	}

	void Entity::clearEvent()
	{
		m_Event.clear();
	}

	void Entity::setParent(std::weak_ptr<Entity> parent)
	{
		m_pParent = parent;

		//親に自分を子として追加
		m_pParent.lock()->addChild(Entity::findGameObj(this));
	}

	std::weak_ptr<Entity> Entity::getParent()
	{
		return m_pParent;
	}

	const int Entity::childCount() const
	{
		return m_Child.size();
	}

	WeakEntity Entity::getChild(int location) const
	{
		return m_Child[location];
	}

	WeakEntity Entity::findChildIf(std::function<bool(WeakEntity child)> action)
	{
		auto find = std::find_if(m_Child.begin(), m_Child.end(), action);

		return find == m_Child.end() ? WeakEntity() : *find;
	}

	void Entity::initEntity()
	{
		for (auto& entity : m_EntityContainer) {
			entity->init();
		}
	}

	std::weak_ptr<Entity> Entity::createEntity(const std::string & name, const std::string & tag, util::Transform trans)
	{
#ifdef _MDEBUG

		util::WinFunc::outLog("同じ名前にエンティティーがあります");

#endif // _MDEBUG


		auto entity = std::make_shared<Entity>(name, tag, trans);
		m_EntityContainer.emplace_back(entity);
		return entity;
	}

	std::weak_ptr<Entity> Entity::findGameObj(const std::string & name)
	{
		//名前が一致するものを検索
		auto it = std::find_if(std::begin(m_EntityContainer), std::end(m_EntityContainer), [&](std::shared_ptr<Entity> entity) {
			return entity->getName() == name;
		});

		return it != std::end(m_EntityContainer) ? *it : nullptr;
	}

	std::weak_ptr<Entity> Entity::findGameObj(const Entity * pEntity)
	{
		//名前が一致するものを検索
		auto it = std::find_if(std::begin(m_EntityContainer), std::end(m_EntityContainer), [&](std::shared_ptr<Entity> entity) {
			return entity.get() == pEntity;
		});

		return it != std::end(m_EntityContainer) ? *it : nullptr;
	}

	std::list<std::weak_ptr<Entity>> Entity::findGameObjs(const std::string & name)
	{
		std::list<std::weak_ptr<Entity>> result;

		std::copy_if(m_EntityContainer.begin(), m_EntityContainer.end(), std::back_inserter(result),
			[&](std::weak_ptr<Entity> entity) {
			return entity.lock()->getName() == name;
		});

		return result;
	}

	std::list<std::weak_ptr<Entity>>  Entity::findGameObjWithTags(const std::string & tag)
	{
		std::list<std::weak_ptr<Entity>> result;

		std::copy_if(m_EntityContainer.begin(), m_EntityContainer.end(), std::back_inserter(result),
			[&](std::weak_ptr<Entity> entity) {
			return entity.lock()->getTag() == tag;
		});

		return result;
	}

	void Entity::removeEntity(std::weak_ptr<Entity> entity)
	{
		entity.lock()->deActive();
		m_DeleteContainer.emplace_back(entity);

		for (size_t i = 0, count = entity.lock()->childCount(); i < count; i++)
		{
			entity.lock()->getChild(i).lock()->deActive();
			m_DeleteContainer.emplace_back(entity.lock()->getChild(i));
		}
	}

	void Entity::deadEntityClear()
	{
		//参照がなくなるので自動開放
		//for (auto& d : m_DeleteContainer) {
		//	m_EntityContainer.remove_if([&](std::shared_ptr<Entity> _entity) {
		//		return _entity.get() == d._Get();
		//	});
		//}
		//m_DeleteContainer.clear();

		//一度に開放すると処理落ちするので毎フレーム一つずつ消していく
		auto begin = m_DeleteContainer.begin();
		if (begin == m_DeleteContainer.end())return;

		m_EntityContainer.remove_if([&](std::shared_ptr<Entity> _entity) {
			return _entity.get() == begin->_Get();
		});

		m_DeleteContainer.erase(begin);
	}

	WeakEntity Entity::instantiate(WeakEntity original, util::Transform* trans)
	{
		std::hash<std::string> hash;
		int num = (int)original._Get();
		num = hash(std::to_string(num));

		//auto newEntity = createEntity(original.lock()->getName() + "Copy" + std::to_string(num), original.lock()->getTag(), *trans);

		auto newEntity = std::make_shared<Entity>((*original._Get()));

		m_EntityContainer.emplace_back(newEntity);

		return newEntity;
	}

	WeakEntity Entity::instantiate(WeakEntity original)
	{
		return instantiate(original, original.lock()->getTransform());
	}

	void Entity::clear()
	{
		m_EntityContainer.clear();
	}

	void Entity::allComponentClear()
	{
		for (auto& entity : m_EntityContainer) {
			entity->componentClear();
		}
	}

	void Entity::init()
	{
		m_pTransform = m_InitTransform;
		m_pTransform.setUserPointer(this);
	}
	void Entity::componentClear()
	{
		for (auto& component : m_ComponentList) {
			component->clear();

		}
	}
	void Entity::addChild(WeakEntity childEntity)
	{
		m_Child.emplace_back(childEntity);
	}
}