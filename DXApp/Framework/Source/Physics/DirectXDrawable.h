#pragma once
#include<Include\BULLET\LinearMath\btIDebugDraw.h>
#include<Source\Device\Render\Shader\StandardMaterial.h>

/**
* @file	DirectXDrawable.h
* @brief	BulletPhysicsの描画用インターフェース
* @authro	高須優輝
* @date	2017/08/31
*/

namespace framework {

	class DirectXDrawable : public btIDebugDraw
	{
	public:
		DirectXDrawable();
		~DirectXDrawable();

		/**
		* @brief	ライン描画実行
		*/
		virtual void flushLines()override;


		/**
		* @brief	描画するラインを追加する
		*/
		virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3& color)override;

		/**
		* @brief	描画するラインを追加する
		*/
		virtual void    drawLine(const btVector3& from, const btVector3& to, const btVector3& fromColor, const btVector3& toColor)override;

		/**
		* @brief	ビュー変換行列セット
		*/
		void setViewMat(util::Mat4 view);

		/**
		* @brief	プロジェション行列セット
		*/
		void setProjMat(util::Mat4 proj);

		virtual void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color)override;

		virtual void	setDebugMode(int debugMode) override;
		virtual int		getDebugMode()  const override;


		//未対応
		virtual void	reportErrorWarning(const char* warningString) override{}
		virtual void	draw3dText(const btVector3& location, const char* textString) override{}


	private:

		//!入力レイアウト
		CComPtr<ID3D11InputLayout> m_pLayout;
		//!シェーダー管理マテリアル
		std::shared_ptr<IMaterial> m_pMaterial;
		//!定数バッファ
		CComPtr<ID3D11Buffer> m_pConstBuffer;
		//!プロジェクション行列
		util::Mat4 m_Proj;
		//!ビュー行列
		util::Mat4 m_View;

		int bitDebugMode = 0;


		struct Vertex
		{
			Vertex(const btVector3& p, const btVector3& c);
			FLOAT pos[3];
			FLOAT color[3];
		};

		struct Line
		{
			Line(const Vertex& f, const Vertex& t) : from(f), to(t) {}
			Line(const btVector3& f, const btVector3& t, const btVector3& fc, const btVector3& tc)
				: from(f, fc), to(t, tc) {}

			Vertex from, to;
		};

		static const size_t MAX_LINE = 100000;
		//!ライン格納変数
		std::vector<Line> aLine;
	};

}