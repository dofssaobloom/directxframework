#include"DirectXDrawable.h"
#include<D3DX11.h>
#include<xnamath.h>


struct ConstBuff
{
	//!プロジェクション
	XMMATRIX mtxProj;
	//!ビュー
	XMMATRIX mtxView;
	//!ワールド
	XMMATRIX mtxWorld;
};

namespace framework {
	
	DirectXDrawable::DirectXDrawable()
	{
		m_pMaterial = ResourceManager::getInstance()->getMaterial("BulletDebug");

		//入力レイアウト作成/////////////
		D3D11_INPUT_ELEMENT_DESC layout[] = {
			{ "SV_Position", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};
		m_pLayout = util::createIA(layout, _countof(layout), m_pMaterial->getVertexByte()->GetBufferPointer(), m_pMaterial->getVertexByte()->GetBufferSize());

		m_pConstBuffer = util::createConstantBuffer(sizeof(ConstBuff));
	}

	DirectXDrawable::~DirectXDrawable()
	{
	}

	void DirectXDrawable::flushLines()
	{
		if (aLine.size() == 0)return;

		 auto vertexBuffer = util::createVertexBuffer(sizeof(Line) * aLine.size(),aLine.data());
		 ConstBuff cbuff;
		 cbuff.mtxView = m_View.toXMMatrix();
		 cbuff.mtxProj = m_Proj.toXMMatrix();
		 cbuff.mtxWorld = XMMatrixIdentity();

		 //定数バファ更新
		 util::updateConstantBuffer(m_pConstBuffer.p,cbuff);

		 ID3D11Buffer* constBuffer[1];
		 constBuffer[0] = m_pConstBuffer.p;

		 m_pMaterial->setConstantBuffer(constBuffer, 1);

		 util::getContext()->IASetInputLayout(m_pLayout.p);

		 // 頂点バッファ
		 UINT stride = sizeof(Vertex);
		 UINT offset = 0;
		 ID3D11Buffer* vb[1] = { vertexBuffer.p };
		 util::getContext()->IASetVertexBuffers(0, 1, vb, &stride, &offset);

		 util::getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);

		 m_pMaterial->active();

		 util::getContext()->Draw(aLine.size()* 2,0);

		 aLine.clear();

		 m_pMaterial->deActive();
	}

	void DirectXDrawable::drawLine(const btVector3 & from, const btVector3 & to, const btVector3 & color)
	{
		drawLine(from, to, color, color);
	}

	void DirectXDrawable::drawLine(const btVector3 & from, const btVector3 & to, const btVector3 & fromColor, const btVector3 & toColor)
	{
		// バッファにまとめて、あとから一括描画
		if (aLine.size() >= MAX_LINE)return;
		aLine.push_back(Line(from, to, fromColor, toColor));
	}

	void DirectXDrawable::setViewMat(util::Mat4 view)
	{
		m_View = view;
	}

	void DirectXDrawable::setProjMat(util::Mat4 proj)
	{
		m_Proj = proj;
	}

	void DirectXDrawable::drawContactPoint(const btVector3 & PointOnB, const btVector3 & normalOnB, btScalar distance, int lifeTime, const btVector3 & color)
	{
		drawLine(PointOnB, PointOnB + normalOnB*distance, color);
		btVector3 ncolor(0, 0, 0);
		drawLine(PointOnB, PointOnB + normalOnB*0.01f, ncolor);
	}

	void DirectXDrawable::setDebugMode(int debugMode)
	{
		bitDebugMode = debugMode;
	}

	int DirectXDrawable::getDebugMode() const
	{
		return bitDebugMode;
	}

	DirectXDrawable::Vertex::Vertex(const btVector3 & p, const btVector3 & c)
	{
		pos[0] = p[0];
		pos[1] = p[1];
		pos[2] = p[2];
		color[0] = c[0];
		color[1] = c[1];
		color[2] = c[2];
	}

}