#pragma once
#include<memory>
#include<Source\Component\Render2DComponent.h>

namespace framework {
	class SpriteRenderer;
	class Texture2D;
}

namespace component {
	class GUIComponent;
	class SpriteRenderComponent : public framework::Render2DComponent
	{
	public:
	
		SpriteRenderComponent();
		~SpriteRenderComponent();

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		void init()override;

		void draw();

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		void setAlpha(float alpha);

		std::string getSpriteName();

		const util::Vec2 getSize();

		//頂点に直接変形を加える
		void transform(util::Vec2 offset[4]);

		/// <summary>
		/// 横方向のスケール変更
		/// </summary>
		void rightScaling(float scale);

		/// <summary>
		/// 横方向のスケール変更
		/// </summary>
		void downScaling(float scale);

		void setSort(float sort);

		/// <summary>
		/// アルファ値取得
		/// </summary>
		/// <returns></returns>
		virtual float getAlpha()override;

	private:
		std::unique_ptr<framework::SpriteRenderer> m_pRenderer;
		std::shared_ptr<framework::Texture2D> m_pTexture;
		std::weak_ptr<GUIComponent> m_pUI;
		std::string m_SpriteName;
		util::Vec2 m_Size;
		std::string m_LuaPath;
		float* m_Sort;
	};


}