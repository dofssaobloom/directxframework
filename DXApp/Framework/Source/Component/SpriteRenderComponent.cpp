#include"SpriteRenderComponent.h"
#include<Source\Device\Render\Renderer\2D\Single\SpriteRenderer.h>
#include<Source\Application\Screen\Screen.h>
#include<Source\Resource\Texture\Texture2D.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Entity\Entity.h>
#include<Source\Component\UI\GUIComponent.h>

namespace component {

	SpriteRenderComponent::SpriteRenderComponent()
	{
		m_CallOrder = 0;
	}

	SpriteRenderComponent::~SpriteRenderComponent()
	{
	}

	void SpriteRenderComponent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath });

		(*componentInitalizer).emplace_back(m_pUI);
	}

	void SpriteRenderComponent::init()
	{
		m_pRenderer = std::make_unique<framework::SpriteRenderer>();
		m_pTexture = framework::ResourceManager::getInstance()->getTexture2D(m_SpriteName);

		m_Size = m_pTexture->getSize();
		m_pRenderer->setSize(m_Size);
		m_pRenderer->setTexture(m_pTexture);
		
		m_Sort = m_pUI.lock()->getSliderPointer("CallOader");
	}

	void SpriteRenderComponent::draw()
	{

		m_CallOrder = *m_Sort;

		//rightScaling(m_pUI.lock()->getSlider("RightScale"));

		auto trans = m_Entity.lock()->getTransform();
		util::Transform drawTrans = *trans;
		drawTrans.m_Position = trans->m_Position + util::Vec3(m_Size.x / 2, m_Size.y / 2, 0);
		m_pRenderer->draw(&drawTrans);
	}

	void SpriteRenderComponent::setParam(const std::vector<std::string>& param)
	{
		//パラメータがあるかチェック
		//paramCheck(param);
		m_SpriteName = param[0];
		m_LuaPath = param[1];
		//m_CallOrder = 0;

		//if (param.size() < 2)return;
		//if (param[1] == "")return;

		//m_CallOrder = atof(param[1].c_str());
	}

	void SpriteRenderComponent::setAlpha(float alpha)
	{
		m_pRenderer->setAlpha(alpha);
	}


	std::string SpriteRenderComponent::getSpriteName()
	{
		return m_SpriteName;
	}

	const util::Vec2 SpriteRenderComponent::getSize()
	{
		return m_Size;
	}

	void SpriteRenderComponent::transform(util::Vec2 offset[4])
	{
		m_pRenderer->setVertexOffset(offset);
	}

	void SpriteRenderComponent::rightScaling(float scale)
	{
		float x1 = 0;
		float x2 = util::lerp<float>(scale,-2,0);
		float x3 = util::lerp<float>(scale, -2, 0);
		float x4 = 0;

		util::Vec2 param[] = {util::Vec2(x1,0),util::Vec2(x2,0) ,util::Vec2(x3,0) ,util::Vec2(x4,0) };

		m_pRenderer->setVertexOffset(param);
	}

	void SpriteRenderComponent::downScaling(float scale)
	{
	}

	void SpriteRenderComponent::setSort(float sort)
	{
		m_CallOrder = *m_Sort = sort;
	}

	float SpriteRenderComponent::getAlpha()
	{
		return m_pRenderer->getAlpha();
	}


}