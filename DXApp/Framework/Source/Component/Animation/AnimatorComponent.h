#pragma once
#include<Source\Component\UpdateComponent.h>
#include<string>
#include<unordered_map>
#include<memory>
#include<Source\Util\Template\Event.h>
#include<functional>
#include<Source\Util\Timer\Timer.h>

namespace framework {
	class Motion;

	struct AnimData
	{
		std::string name;
		//!再生速度
		int frameSpeed;
		//!アニメーションのイベント
		util::Event<std::string> animEvent;
		//!ループするかどうか
		bool isLoop;
		//!最後のフレーム
		int endFrame;
		//!アニメーションの番号
		int id;
		//!フレーム数
		int frame = 0;
		//!逆再生するかどうか
		bool isReverse;

		bool isEnd()
		{
			return frame >= endFrame;
		}

		float rate()
		{
			return (float)frame / endFrame;
		}
	};
}

namespace component {

	enum class MotionFrame
	{
		begin,//はじめ
		end,//最後
		half,//真ん中
	};

	class AnimatorComponent : public framework::UpdateComponent
	{
	public:

		struct LoadData
		{
			std::string name;
			bool isLoop;
			int speed;
		};

		class EmitEvent
		{
		public:
			EmitEvent(std::weak_ptr<framework::Entity> entity, const std::string& animationName,const std::string& eventName,int emitFrame,int liveFrame );
			~EmitEvent();

			/**
			* @brief		イベント検索
			* @param currentAnime	現在のアニメーションの名前
			* @param currentFrame	現在のフレーム数
			*/
			void sourchEvent(const std::weak_ptr<framework::AnimData>& currentAnime, int currentFrame);

		private:
			/**
			* @brief		イベントの発行条件かどうか
			*/
			bool isFrameCondition(int currentFrame);

			/**
			* @brief		イベント発行
			*/
			void emitEvent();

		private:
			//!イベントを持っているエンティティ
			std::weak_ptr<framework::Entity> m_Entity;
			//!発行フレーム
			const int m_EmitFrame;
			//!生存フレーム数
			const int m_LiveFrame;
			//!登録されている名前
			const std::string m_EventName;
			//!登録されている名前
			const std::string m_AnimationName;
		};

		AnimatorComponent();
		~AnimatorComponent();

		/**
		* @brief		初期化
		*/
		void init();

		/**
		* @brief			アニメーション登録
		* @param stateName  状態の名前
		* @param data		アニメーションの情報
		*/
		void addAnime(const std::string& stateName, std::shared_ptr< framework::AnimData > data);

		// UpdateComponent を介して継承されました
		virtual void update() override;

		/**
		* @brief		クリアー
		*/
		void clear();

		/// <summary>
		/// 前回のフレーム数を引き継いで変更する
		/// </summary>
		/// <param name="stateName"></param>
		/// <param name="startFrame"></param>
		void changeAnime(const std::string& stateName);

		/// <summary>
		/// フレーム数を指定して変更する
		/// </summary>
		/// <param name="stateName"></param>
		/// <param name="startFrame"></param>
		void changeAnime(const std::string& stateName,int startFrame);

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		/// <summary>
		/// 現在のモーションフレーム取得
		/// </summary>
		/// <returns></returns>
		const int getFrame();

		/// <summary>
		/// ブレンド中のモーションの現在フレーム
		/// セットされていなければ0を返す
		/// </summary>
		/// <returns></returns>
		const int getBlendFrame();

		/// <summary>
		/// 現在のモーションID取得
		/// </summary>
		/// <returns></returns>
		const int getCurrentMotionID();

		/// <summary>
		/// ブレンドするアニメーションのIDを取得
		/// セットされてない場合は現在のアニメーションと同じ番号を返す
		/// </summary>
		/// <returns></returns>
		const int getBlendMotionID();

		const std::string& getBlendMotionName();

		
		const int getEndFrame();

		/// <summary>
		/// 現在のアニメーションが終了したかどうか
		/// ループが有効な場合常にfalse
		/// </summary>
		/// <returns></returns>
		bool isEnd();

		/// <summary>
		/// ブレンドでセットされたアニメーションが終わったか
		/// ループが有効な場合は常にfalse
		/// </summary>
		/// <returns></returns>
		bool isBlendAnimeEnd();

		/**
		* @brief		現在のフレーム数を0~1で取得
		*/
		float rate();

		/**
		* @brief		idの順番にソートされたモーションの名前取得
		*/
		std::list<std::string> getMotionNames();

		/**
		* @brief		アニメーションストップ
		*/
		void onStop();

		//
		void onStopTimer(short time);

		/**
		* @brief		アニメーション開始
		*/
		void onStart();

		/// <summary>
		/// モーションブレンド有効化
		/// モーションブレンドはシェーダーで処理が重くなるように分岐してるので使わないときはoffにしておく
		/// </summary>
		/// <param name="stateName">ブレンドするアニメーションの名前</param>
		void beginBlend(const std::string& stateName,int startFrame,int blendTime);

		/// <summary>
		/// モーションブレンド有効化
		/// モーションブレンドはシェーダーで処理が重くなるように分岐してるので使わないときはoffにしておく
		/// </summary>
		/// <param name="stateName">ブレンドするアニメーションの名前</param>
		void beginBlend(const std::string& stateName, MotionFrame, int blendTime);

		/// <summary>
		/// モーションブレンド無効化
		/// </summary>
		void endBlend();

		/// <summary>
		/// ブレンド率セット
		/// </summary>
		/// <param name="rate"></param>
		//void setBlendRate(float rate);

		/// <summary>
		/// ブレンド率取得
		/// 0で現在のアニメーション
		/// 1でブレンドアニメーションに近づく
		/// </summary>
		/// <returns></returns>
		float getBlendRate()const;

		/// <summary>
		/// ブレンド有効かどうか
		/// </summary>
		bool isBlend();

		/// <summary>
		/// ブレンド率タイマーを止める
		/// </summary>
		void stopBlendTimer();

		/// <summary>
		/// ブレンド率タイマースタート
		/// </summary>
		void startBlendTimer();

		/// <summary>
		/// 現在のアニメーションの名前取得
		/// </summary>
		/// <returns></returns>
		std::string getCurrentName();

		/// <summary>
		/// ブレンド方向の逆転
		/// </summary>
		void switchBlendDir();

		/// <summary>
		/// ブレンドタイマー終了
		/// </summary>
		/// <returns></returns>
		bool isBlendTimeEnd();

		/// <summary>
		/// 逆再生フラグ反転
		/// </summary>
		void changeReverseCurrentAnim();

		/// <summary>
		/// 逆再生フラグ反転
		/// </summary>
		void changeReverseCurrentBlend();

		/// <summary>
		/// 逆再生フラグ反転
		/// </summary>
		void changeReverseCurrentAnim(bool flag);

		/// <summary>
		/// 逆再生フラグ反転
		/// </summary>
		void changeReverseCurrentBlend(bool flag);

		/// <summary>
		/// アニメーション数取得
		/// </summary>
		/// <returns></returns>
		short animeCount();

	private:

		/**
		* @brief		アニメーションのフレームを更新する
		*/
		void updateMotion(std::weak_ptr<framework::AnimData>& animedata);

		void registData(const LoadData& data);

		/**
		* @brief		イベント登録
		*/
		void registEvent(const std::vector<std::string> param);

		/**
		* @brief		イベント更新
		*/
		void updateEvent();

	private:
		//アニメーションの数
		int m_AnimeCount;

		//!アニメーションコンテナ
		std::unordered_map<std::string,std::shared_ptr< framework::AnimData >> m_AnimeDatas;

		//!現在のアニメーション
		std::weak_ptr<framework::AnimData> m_CurrentAnime;

		//!ブレンド用のアニメーション
		std::weak_ptr<framework::AnimData> m_BlendAnime;

		std::vector<LoadData> m_AddList;

		//!最初のモーションの名前
		std::string m_FirstName;

		//!イベント発行クラスコンテナ
		std::vector<EmitEvent> m_EmitterContainer;

		bool m_IsStop;

		bool m_IsBlendStop;

		std::unique_ptr<util::Timer> m_pTimer;

		//!ブレンド有効
		bool m_IsBlend;

		//!現在からブレンドアニメーションへ傾くか
		bool m_IsUpBlend;

		//!ブレンド時間タイマー
		std::unique_ptr<util::Timer> m_pBlendTimer;
	};




}