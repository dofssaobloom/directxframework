#include"AnimatorComponent.h"
#include<Source\Resource\Motion\Motion.h>
#include<assert.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Entity\Entity.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Util\IO\CSVLoader.h>
#include<Source\Resource\Motion\Motion.h>
#include<assert.h>

namespace component {

	using namespace framework;

	AnimatorComponent::AnimatorComponent()
		:m_IsBlend(false),
		m_IsUpBlend(true)
	{
		m_CallOrder = 0;
	}

	AnimatorComponent::~AnimatorComponent()
	{
	}

	void AnimatorComponent::init()
	{
		m_AnimeDatas.clear();
		m_AnimeCount = 0;
		m_IsStop = false;

		for (auto& data : m_AddList) {
			registData(data);
		}

		if (m_FirstName == "") {
			//最初に読み込んだものを初期モーションにする
			changeAnime(m_AddList[0].name);
		}
		else {
			changeAnime(m_FirstName);
		}

		m_IsBlendStop = false;
	}

	void AnimatorComponent::addAnime(const std::string & stateName, std::shared_ptr< framework::AnimData > data)
	{
		assert(m_AnimeDatas.find(stateName) == m_AnimeDatas.end() && "このキーはすでに登録されています");
		data->id = m_AnimeCount;
		data->name = stateName;
		m_AnimeDatas[stateName] = data;
		m_AnimeCount += 1;
	}

	void AnimatorComponent::update()
	{
		if (m_IsStop) return;
		if (m_pTimer != nullptr) {
			m_pTimer->update();
			if (m_pTimer->isEnd()) {
				m_pTimer.release();
			}
			return;
		}

		updateEvent();
		updateMotion(m_CurrentAnime);
		updateMotion(m_BlendAnime);
		if (m_pBlendTimer != nullptr) {
			if(!m_IsBlendStop)
				m_pBlendTimer->update();
		}
	}

	void AnimatorComponent::clear()
	{
		m_AnimeDatas.clear();
	}

	void AnimatorComponent::changeAnime(const std::string & stateName)
	{
		assert(m_AnimeDatas.find(stateName) != m_AnimeDatas.end() && "このキーは登録されていません");
		if (!m_CurrentAnime.expired()) {
			//同じものを指定してたら何もしない
			if (m_CurrentAnime.lock()->name == stateName)return;
		}

		m_CurrentAnime = m_AnimeDatas[stateName];
	}

	void AnimatorComponent::changeAnime(const std::string & stateName, int startFrame)
	{
		assert(m_AnimeDatas.find(stateName) != m_AnimeDatas.end() && "このキーは登録されていません");
		if (!m_CurrentAnime.expired()) {
			//同じものを指定してたら何もしない
			if (m_CurrentAnime.lock()->name == stateName)return;
		}

		m_CurrentAnime = m_AnimeDatas[stateName];

		m_CurrentAnime.lock()->frame = startFrame;
	}

	/*
	param1 モーション設定csv
	param2 モーションエベントcsv
	*/
	void AnimatorComponent::setParam(const std::vector<std::string>& param)
	{
		assert(param.size() >= 2 && "パラメータが足りません");

		util::CSVLoader settingLoader(param[0]);
		auto loadData = settingLoader.load();

		//!コメント行削除
		loadData.erase(loadData.begin());
		m_AddList.reserve(loadData.size());
		for (auto data : loadData) {
			assert(data.size() >= 3 && "情報が正しく入っていません");

			LoadData ld;
			ld.name = data[0];
			ld.isLoop = (bool)std::atoi(data[1].c_str());
			ld.speed = std::atoi(data[2].c_str());
			m_AddList.emplace_back(ld);
		}

		m_FirstName = m_AddList[0].name;

		util::CSVLoader eventLoader(param[1]);

		auto eventData = eventLoader.load();
		eventData.erase(eventData.begin());
		for (auto& param : eventData) {
			registEvent(param);
		}
	}

	const int AnimatorComponent::getFrame()
	{
		return m_CurrentAnime.lock()->frame;
	}

	const int AnimatorComponent::getBlendFrame()
	{
		if (m_BlendAnime.expired())return 0;
		return m_BlendAnime.lock()->frame;
	}

	const int AnimatorComponent::getCurrentMotionID()
	{
		return m_CurrentAnime.lock()->id;
	}

	const int AnimatorComponent::getBlendMotionID()
	{
		//現在のモーションと同じ番号を返しておけばブレンドしても見た目が変わらない
		if (m_BlendAnime.expired())return getCurrentMotionID();
		return m_BlendAnime.lock()->id;
	}

	const std::string & AnimatorComponent::getBlendMotionName()
	{
		if (m_BlendAnime.expired())return std::string();
		return m_BlendAnime.lock()->name;
	}

	const int AnimatorComponent::getEndFrame() {
		return m_CurrentAnime.lock()->endFrame;
	}

	bool AnimatorComponent::isEnd()
	{
		if (m_CurrentAnime.lock()->isLoop)return false;
		return m_CurrentAnime.lock()->isEnd();
	}

	bool AnimatorComponent::isBlendAnimeEnd()
	{
		if (m_BlendAnime.expired())return true;
		if (m_BlendAnime.lock()->isLoop)return false;
		return m_BlendAnime.lock()->isEnd();
	}

	float AnimatorComponent::rate()
	{
		return m_CurrentAnime.lock()->rate();
	}

	std::list<std::string> AnimatorComponent::getMotionNames()
	{
		std::vector<std::shared_ptr<framework::AnimData>> aniData;

		aniData.reserve(m_AnimeDatas.size());

		for (auto container : m_AnimeDatas) {
			aniData.emplace_back(container.second);
		}

		std::sort(aniData.begin(), aniData.end(), [&](const std::weak_ptr<framework::AnimData>& left, const std::weak_ptr<framework::AnimData>& right) {
			return left.lock()->id <= right.lock()->id;
		});

		std::list<std::string> nameContainer;
		for (auto data : aniData) {
			nameContainer.emplace_back(data->name);
		}

		return nameContainer;
	}

	void AnimatorComponent::onStop()
	{
		m_IsStop = true;
	}

	void AnimatorComponent::onStopTimer(short time)
	{
		m_pTimer = std::make_unique<util::Timer>(time);
	}

	void AnimatorComponent::onStart()
	{
		m_IsStop = false;
	}

	void AnimatorComponent::beginBlend(const std::string& stateName, int startFrameint, int blendTime)
	{
		assert(m_AnimeDatas.find(stateName) != m_AnimeDatas.end() && "このキーは登録されていません");
		m_BlendAnime = m_AnimeDatas[stateName];
		m_BlendAnime.lock()->frame = startFrameint;//フレームをリセット
		m_IsBlend = true;
		m_IsUpBlend = true;
		m_pBlendTimer = std::make_unique<util::Timer>(blendTime);
	}

	void AnimatorComponent::beginBlend(const std::string & stateName, MotionFrame frame, int blendTime)
	{
		switch (frame)
		{
		case component::MotionFrame::begin:
			beginBlend(stateName, 0, blendTime);
			break;
		case component::MotionFrame::end:
			//最後のフレームからセットする
			beginBlend(stateName, m_AnimeDatas[stateName]->endFrame, blendTime);
			break;
		case component::MotionFrame::half:
			//最後のフレームからセットする
			beginBlend(stateName, m_AnimeDatas[stateName]->endFrame / 2, blendTime);
			break;
		default:
			break;
		}
	}

	void AnimatorComponent::endBlend()
	{
		//ブレンドアニメーション解除
		m_BlendAnime.reset();
		m_IsBlend = false;
	}

	//void AnimatorComponent::setBlendRate(float rate)
	//{
	//	m_BlendRate = rate;
	//}

	float AnimatorComponent::getBlendRate() const
	{
		if (m_IsUpBlend) {
			return 1.0f - m_pBlendTimer->rate();
		}
		else {
			return m_pBlendTimer->rate();
		}
	}

	bool AnimatorComponent::isBlend()
	{
		return m_IsBlend;
	}

	void AnimatorComponent::stopBlendTimer()
	{
		m_IsBlendStop = true;
	}

	void AnimatorComponent::startBlendTimer()
	{
		m_IsBlendStop = false;
	}

	std::string AnimatorComponent::getCurrentName()
	{
		return m_CurrentAnime.lock()->name;
	}

	void AnimatorComponent::switchBlendDir()
	{
		if (m_pBlendTimer == nullptr)return;
		m_IsUpBlend = !m_IsUpBlend;
		m_pBlendTimer->init();

	}

	bool AnimatorComponent::isBlendTimeEnd()
	{
		if (m_pBlendTimer == nullptr)return false;
		return m_pBlendTimer->isEnd();
	}

	void AnimatorComponent::changeReverseCurrentAnim()
	{
		m_CurrentAnime.lock()->isReverse = !m_CurrentAnime.lock()->isReverse;
	}

	void AnimatorComponent::changeReverseCurrentBlend()
	{
		if (m_BlendAnime.expired())return;
		m_BlendAnime.lock()->isReverse = !m_BlendAnime.lock()->isReverse;
	}

	void AnimatorComponent::changeReverseCurrentAnim(bool flag)
	{
		m_CurrentAnime.lock()->isReverse = flag;
	}

	void AnimatorComponent::changeReverseCurrentBlend(bool flag)
	{
		if (m_BlendAnime.expired())return;
		m_BlendAnime.lock()->isReverse = flag;
	}

	short AnimatorComponent::animeCount()
	{
		return m_AnimeDatas.size();
	}


	void AnimatorComponent::updateMotion(std::weak_ptr<framework::AnimData>& animedata)
	{
		//参照が切れていたら何もしない
		if (animedata.expired())return;
		if (animedata.lock()->isReverse) {
			//エンドフレーム達していてかつループ設定になっていれば0に戻して何もしない
			if (animedata.lock()->frame == 0) {
				if (animedata.lock()->isLoop) {
					animedata.lock()->frame = animedata.lock()->endFrame;
					return;
				}
			}

			animedata.lock()->frame -= animedata.lock()->frameSpeed;

			animedata.lock()->frame = max(animedata.lock()->frame, 0);
		}
		else {
			//エンドフレーム達していてかつループ設定になっていれば0に戻して何もしない
			if (animedata.lock()->isEnd()) {
				if (animedata.lock()->isLoop) {
					animedata.lock()->frame = 0;
					return;
				}
			}

			animedata.lock()->frame += animedata.lock()->frameSpeed;
			//endフレームを超えないようにする
			animedata.lock()->frame = min(animedata.lock()->frame, animedata.lock()->endFrame);
		}
	}

	void AnimatorComponent::registData(const LoadData & data)
	{
		auto motion = ResourceManager::getInstance()->getMotion(data.name);

		//モーションの切替時ディープコピーが発生するのでスマポで生成
		auto anim = std::make_shared<AnimData >();
		anim->isLoop = data.isLoop;
		anim->frameSpeed = data.speed;
		anim->endFrame = motion->getMaxFrame() - 1;
		anim->isReverse = false;

		addAnime(data.name, anim);
	}
	void AnimatorComponent::registEvent(const std::vector<std::string> param)
	{
		m_EmitterContainer.emplace_back(EmitEvent(m_Entity, param[0], param[1], std::atoi(param[2].c_str()), std::atoi(param[3].c_str())));

	}

	void AnimatorComponent::updateEvent()
	{
		for (auto& emitter : m_EmitterContainer) {
			emitter.sourchEvent(m_CurrentAnime, m_CurrentAnime.lock()->frame);
		}

		if (isBlend()) {
			for (auto& emitter : m_EmitterContainer) {
				emitter.sourchEvent(m_BlendAnime, m_BlendAnime.lock()->frame);
			}
		}
	}

	AnimatorComponent::EmitEvent::EmitEvent(std::weak_ptr<framework::Entity> entity, const std::string& animationName,
		const std::string& eventName, int emitFrame, int liveFrame)
		:m_Entity(entity),
		m_AnimationName(animationName),
		m_EventName(eventName),
		m_EmitFrame(emitFrame),
		m_LiveFrame(liveFrame)
	{
	}
	AnimatorComponent::EmitEvent::~EmitEvent()
	{
	}
	void AnimatorComponent::EmitEvent::emitEvent()
	{
		if (m_Entity.expired())return;
		m_Entity.lock()->onEvent(m_EventName);
	}
	void AnimatorComponent::EmitEvent::sourchEvent(const std::weak_ptr<framework::AnimData>& currentAnime, int currentFrame)
	{
		//現在の名前と一致してなければ何もしない
		if (m_AnimationName != currentAnime.lock()->name)return;
		if (!isFrameCondition(currentFrame))return;

		emitEvent();
	}
	bool AnimatorComponent::EmitEvent::isFrameCondition(int currentFrame)
	{
		return m_EmitFrame <= currentFrame && m_EmitFrame + m_LiveFrame >= currentFrame;
	}
}