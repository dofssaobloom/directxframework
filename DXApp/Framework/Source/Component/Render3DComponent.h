#pragma once
#include<Source\Component\Component.h>
#include<list>

namespace util {

	class Transform;

}

namespace framework {


	class Render3DComponent : public framework::Component
	{
	public:
		Render3DComponent();
		~Render3DComponent();

		/**
		* @brief		１つのレンダーターゲットに描画
		*/
		virtual void init();

		virtual void setup() {}

		/**
		* @brief		Taskリストに追加
		*/
		virtual void active() override;

		/**
		* @brief		Taskリストから削除
		*/
		virtual void deActive() override;

		/**
		* @brief		１つのレンダーターゲットに描画
		*/
		virtual void draw() = 0;

		/**
		* @brief		深度バッファターゲットに描画
		*/
		virtual void depthDraw() = 0;

		/**
		* @brief			描画数取得
		*/
		virtual int drawNum() = 0;

		/**
		* @brief			不正エンティティー除去
		*/
		virtual void cheackDelete() = 0;
	private:

	};

}