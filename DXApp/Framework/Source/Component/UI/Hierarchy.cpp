#include "Framework.h"
#include "Hierarchy.h"
#include<Source\Util\ImGUI\ImCollapsignHeader.h>
#include"Inspector.h"
#include<Source\Util\ImGUI\ImCheckBox.h>


UsingNamespace;

namespace component {

	Hierarchy::Hierarchy()
	{
	}

	Hierarchy::~Hierarchy()
	{
	}

	void Hierarchy::init()
	{
#ifdef _MDEBUG
		std::vector<std::weak_ptr<Component>> damy;
		m_pHierarchy = m_Entity.lock()->addComponent<GUIComponent>(&damy,{ "Resource/Script/Lua/UI/Template.lua" });
		//何もしないLuaを呼ぶ
		//m_UI.lock()->setParam(std::vector<std::string>{ "Resource/Script/Lua/UI/Template.lua" });
		m_pHierarchy.lock()->init();
		m_pHierarchy.lock()->m_CallOrder = 100;
		m_pHierarchy.lock()->setUIPosition(util::Vec2(20, 20));
		m_pHierarchy.lock()->setUIScale(util::Vec2(300, 500));
		
		entityFind();

		m_pInspector = Entity::findGameObj("Inspector").lock()->getComponent<Inspector>();
#endif

	}

	void Hierarchy::update()
	{
#ifdef _MDEBUG
		if (m_pButton.lock()->onClick()) {
			entityFind();
		}

		checkDelete();

		for (auto& obj : m_ObjectList) {
			auto name = obj.lock()->getName();
			auto header = m_pHierarchy.lock()->getItem<util::ImCollapsignHeader>(name);
			auto button = header.lock()->getItem<util::ImButton>(name + "Inspector");
			auto ui = obj.lock()->getComponent<GUIComponent>();
			if (button.lock()->onClick()) {
				m_pInspector.lock()->changeInspectorUI(obj);
			}
		}

#endif
	}

	void Hierarchy::entityFind()
	{
		m_pHierarchy.lock()->clearItem();

		m_pHierarchy.lock()->addButton("ReFind");
		m_pButton = m_pHierarchy.lock()->getItem<util::ImButton>("ReFind");

		m_ObjectList = Entity::findHaveComponentEntity<GUIComponent>();

		m_ObjectList.remove_if([&](framework::WeakEntity& entity) {
			return entity._Get() == m_Entity._Get();
		});

		for (auto& obj : m_ObjectList) {

			auto name = obj.lock()->getName();
			m_pHierarchy.lock()->addCollapsingHeader(name);
			auto header = m_pHierarchy.lock()->getItem<util::ImCollapsignHeader>(name);
			header.lock()->addItem(std::make_shared<util::ImButton>(name + "Inspector"));

			//UIをすべてディアクティブする
			auto uis = obj.lock()->getComponents<GUIComponent>();
			for (auto& ui : uis)
			{
				ui.lock()->deActive();
			}
		}
	}

	void Hierarchy::checkDelete()
	{
		m_ObjectList.remove_if([](framework::WeakEntity obj) {
			return obj.expired();
		});
	}

}
