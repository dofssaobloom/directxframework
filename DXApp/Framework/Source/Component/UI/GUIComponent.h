#pragma once
#include<Framework.h>
#include<memory>
#include<Source\Util\ImGUI\ImGUI.h>
#include<Source\Util\ImGUI\IImItem.h>
#include<list>
#include<iterator>
#include<algorithm>
#include<Source\Util\ImGUI\IImItem.h>
#include<Source\Util\ImGUI\ImColorEditor.h>
#include<Source\Util\ImGUI\ImSlider.h>
#include<Source\Util\ImGUI\ImText.h>
#include<Source\Util\ImGUI\ImButton.h>
#include<Source\Util\IO\BinaryWriter.h>

namespace util {
	class ImGUI;
	class LuaCpp;
	class ImInputText;
}

namespace component {

	class GUIComponent : public framework::Render2DComponent
	{
	public:
		GUIComponent();
		~GUIComponent();

		/**
		* @brief		ゲームオブジェクトに接続された瞬間
		*/
		virtual void onConect()override;

		/**
		* @brief		初期化
		*/
		virtual void init()override;

		/**
		* @brief		描画
		*/
		virtual void draw() override;

		/**
		* @brief		アイテム飲みドロウ
		*/
		void itemDraw();

		/// <summary>
		/// FPS表示
		/// </summary>
		void showFPS();

		/**
		* @brief		アクティブにする
		*/
		virtual void active() override;

		/**
		* @brief		ディアクティブにする
		*/
		virtual void deActive() override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param)override;

		/// <summary>
		/// トランスフォーム表示
		/// </summary>
		void drawTransform();


		/// <summary>
		/// テキスト追加
		/// </summary>
		/// <param name="text"></param>
		void addText(const std::string& text);

		/// <summary>
		/// スライダー追加
		/// </summary>
		/// <param name="text"></param>
		/// <param name="range"></param>
		void addSlider(const std::string& text, const util::Vec2& range, const float initNum);

		/// <summary>
		/// カラーエディター追加
		/// </summary>
		/// <param name="text"></param>
		void addColorEditor(const std::string& text, const util::Vec4& initNum);


		/// <summary>
		/// ボタン追加
		/// </summary>
		void addButton(const std::string& text);

		/// <summary>
		/// プルダウン追加
		/// </summary>
		/// <param name="text"></param>
		void addCollapsingHeader(const std::string& text);

		/// <summary>
		/// チェックボックス追加
		/// </summary>
		/// <param name="text"></param>
		/// <param name="initNum"></param>
		void addCheckBox(const std::string& text, const bool initNum);

		/// <summary>
		/// 文字入力項目追加
		/// </summary>
		/// <param name="defaultName"></param>
		void addInputText(const std::string & textName,const std::string& defaultName);

		/// <summary>
		/// スライダーの値取得
		/// </summary>
		/// <param name="text">UI識別</param>
		/// <returns></returns>
		float getSlider(const std::string& text);

		/// <summary>
		/// 値をポインタで取得
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		float* getSliderPointer(const std::string& text);

		/// <summary>
		/// カラー取得
		/// </summary>
		/// <param name="text">取得するパラメータのUI識別</param>
		/// <returns></returns>
		util::Vec4 getColor(const std::string& text);

		/// <summary>
		/// 値をポインターで取得する
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		util::Vec4* getColorPointer(const std::string& text);

		/// <summary>
		///　テキスト書き換えようのポインタ取得
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		std::string* getTextPointer(const std::string& text);


		/// <summary>
		/// ボタンが押されたかどうか
		/// </summary>
		/// <param name="text">UI識別</param>
		/// <returns></returns>
		bool onClick(const std::string& text);

		/// <summary>
		/// チェックボックスが入っているかどうか
		/// </summary>
		/// <param name="text">チェックボックスの名前</param>
		/// <returns></returns>
		bool isCheck(const std::string& text);

		/// <summary>
		/// 値をポインターで取得する
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		bool* isCheckPointer(const std::string& text);

		/// <summary>
		/// 座標セット
		/// </summary>
		/// <param name="pos"></param>
		void setUIPosition(const util::Vec2& pos);

		/// <summary>
		/// サイズセット
		/// </summary>
		/// <param name="size"></param>
		void setUIScale(const util::Vec2& size);

		/// <summary>
		/// このインスタンスのすべてのアイテムの弱参照を渡す
		/// </summary>
		/// <returns></returns>
		std::list<std::weak_ptr<util::IImItem>> getItemAllFromThisInstance();

		/// <summary>
		/// このインスタンスが接続されているゲームオブジェクトのすべてのアイテムの弱参照を渡す
		/// </summary>
		/// <returns></returns>
		std::list<std::weak_ptr<util::IImItem>> getItemAllFromEntity();

		/// <summary>
		/// ルアの名前取得
		/// </summary>
		/// <returns></returns>
		std::string getLuaName();
		

		/// <summary>
		/// ゲームオブジェクトからUIコンポーネントを識別して取得する
		/// </summary>
		/// <param name="luaName"></param>
		/// <returns></returns>
		static std::weak_ptr<GUIComponent> getUIComponent(std::list<std::weak_ptr<GUIComponent>>& uis,const std::string& luaName);


		/// <summary>
		/// UIのアイテムすべて削除
		/// </summary>
		void clearItem();

		/// <summary>
		/// 指定した名前のアイテムを取得
		/// </summary>
		template<typename T>
		std::weak_ptr<T> getItem(const std::string& text) {

			auto find = std::find_if(m_Item.begin(), m_Item.end(), [&](std::weak_ptr<util::IImItem> item) {
				return item.lock()->name() == text;
			});

#ifdef _MDEBUG 
			std::weak_ptr<T> item = std::dynamic_pointer_cast<T>((*find));
			assert(!item.expired() && "指定したアイテムが見つかりません");
			return item;
#else
			return std::dynamic_pointer_cast<T>((*find));
#endif
		}

		/// <summary>
		/// 指定した型のアイテムをすべて取得
		/// </summary>
		/// <returns></returns>
		template<typename T>
		std::vector<std::weak_ptr<T>> getItemTypeAll() {

			auto copyList = util::listCopyif<std::shared_ptr<util::IImItem>>(m_Item, [&](std::shared_ptr<util::IImItem>& item) {
				//キャストできれば指定した型
				return std::dynamic_pointer_cast<T>(item) != nullptr;
			});

			std::vector<std::weak_ptr<T>> result;
			for (auto& copy : copyList) {
				result.emplace_back(std::dynamic_pointer_cast<T>(copy));
			}

			return result;
		}


	private:

		/// <summary>
		/// パラメータ読み込み
		/// </summary>
		void paramRead();

		/// <summary>
		/// スライダーパラメータ書き出し
		/// </summary>
		void sliderWrite(util::FileType type = util::FileType::newFile);

		/// <summary>
		/// スライダーパラメータ書き出し
		/// </summary>
		void colorEditorWrite(util::FileType type = util::FileType::addFile);

		/// <summary>
		/// チェックボックス書き出し
		/// </summary>
		void checkBoxWrite(util::FileType type = util::FileType::addFile);

		/// <summary>
		/// バイナリのバファからデータを取り出す
		/// </summary>
		/// <param name="text">見つけたらイテレータで場所を返す</param>
		std::vector<float>::iterator bainalyToData(const std::string& text);

		/// <summary>
		/// バイナリーをプレファブフォルダに出力
		/// </summary>
		void createPrefab(const std::string& faileName);

		/// <summary>
		/// ファイル出力ボタン
		/// </summary>
		void prefabCreateButton();

	protected:
		std::unique_ptr<util::ImGUI> m_pUI;
		std::unique_ptr<util::LuaCpp> lua;
		std::list<std::shared_ptr<util::IImItem>> m_Item;

		std::weak_ptr<util::ImInputText> m_pInputText;
		std::weak_ptr<util::ImButton> m_pCeateButton;

		//シーンフォルダのUIMeta + UI.luaの名前
		std::string m_SceneFolderPath;
		std::string m_PrefabPathPath;
		const std::string m_FolderPath;
		//拡張子の名前
		const std::string m_ExtensionName;
		std::string m_LuaName;

		bool m_IsPrefab;

		std::vector<float>  m_Paramas;

	};
}