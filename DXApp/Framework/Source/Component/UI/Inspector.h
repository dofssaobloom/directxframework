#pragma once
#include<Framework.h>

namespace util {
	class ImGUI;
}

namespace component {

	/// <summary>
	/// コンポーネントについているUIを表示する
	/// </summary>
	class Inspector : public framework::Render2DComponent
	{
	public:
		Inspector();
		~Inspector();

		void init()override;

		/// <summary>
		/// UI描画
		/// </summary>
		virtual void draw() override;


		void changeInspectorUI(std::weak_ptr<framework::Entity>& entity);

	private:
		void checkDelete();

	private:
		std::list<std::weak_ptr< GUIComponent>> m_DrawUIs;

		std::unique_ptr<util::ImGUI> m_pUI;
	};



}