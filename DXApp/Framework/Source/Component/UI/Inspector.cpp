#include "Framework.h"
#include "Inspector.h"

namespace component {

	Inspector::Inspector()
	{

	}

	Inspector::~Inspector()
	{
	}

	void Inspector::init()
	{
		auto pos = util::Vec2(960, 20);
		auto size = util::Vec2(300, 500);
		m_pUI = std::make_unique<util::ImGUI>(m_Entity.lock()->getName(), pos, size);
	}

	void Inspector::draw()
	{

#ifdef _MDEBUG
		checkDelete();
		if (m_DrawUIs.empty())return;
		m_pUI->begin();

		(*m_DrawUIs.begin()).lock()->drawTransform();

		for (auto& ui : m_DrawUIs)
		{
			if (ui.expired())continue;
			ui.lock()->itemDraw();
		}

		m_pUI->end();
#endif // !_MDEBUG
	}

	void Inspector::changeInspectorUI(std::weak_ptr<framework::Entity>& entity)
	{
		m_DrawUIs = entity.lock()->getComponents<GUIComponent>();

	}

	void Inspector::checkDelete()
	{
		m_DrawUIs.remove_if([](std::weak_ptr< GUIComponent>& ui) {
			return ui.expired();
		});
	}

}