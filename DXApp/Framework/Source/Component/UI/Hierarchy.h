#pragma once
#include<Framework.h>
#include<Source\Util\ImGUI\ImGUI.h>

namespace util {
	class ImButton;
	class ImCheckBox;
}

namespace component {
	class Inspector;

	/// <summary>
	/// ゲームオブジェクトを一覧して項目を選択されたらInspectorを起動するクラス
	/// </summary>
	class Hierarchy : public framework::UpdateComponent
	{
	public:
		Hierarchy();
		~Hierarchy();

		/**
		* @brief		初期化
		*/
		virtual void init()override;

		/// <summary>
		/// 更新
		/// Releaseビルドでは処理されない
		/// </summary>
		virtual void update()override;

		/// <summary>
		/// UIを持っているゲームオブジェクトを検索する
		/// </summary>
		void entityFind();

	private:

		/// <summary>
		/// 参照切れをチェックしてリストから消す
		/// </summary>
		void checkDelete();


	private:
		//!表示するオブジェクトのリスト
		std::list<framework::WeakEntity > m_ObjectList;

		std::weak_ptr<GUIComponent> m_pHierarchy;

		//std::weak_ptr<GUIComponent> m_pCurrentUI;

		std::weak_ptr<Inspector> m_pInspector;

		//!再検索ボタン
		std::weak_ptr<util::ImButton> m_pButton;

	};
}