#include "GUIComponent.h"
#include<Source\Util\Type.h>
#include<Source\Util\ImGUI\ImGUI.h>
#include<Source\Util\Lua\LuaCpp.h>
#include<Source\Util\Event\WindowEvent.h>
#include<Source\Util\IO\BinaryLoader.h>
#include<direct.h>
#include<Source\Application\Scene\Scene.h>
#include<Source\Util\ImGUI\ImCollapsignHeader.h>
#include<Source\Util\Type.h>
#include<Source\Util\ImGUI\ImCheckBox.h>
#include<algorithm>
#include<Source\Application\Application.h>
#include<Source\Util\ImGUI\ImInputText.h>
#include<Source\Util\IO\TextWriter.h>

namespace component {

	GUIComponent::GUIComponent()
		:m_FolderPath(framework::Scene::m_CurrentScenePath + "/UIMeta")
		, m_PrefabPathPath("Resource/ByteData/UIPrefab/"),
		m_ExtensionName(".pfb"),
		m_IsPrefab(false)
	{
		m_CallOrder = 0;
	}

	GUIComponent::~GUIComponent()
	{
#ifdef _MDEBUG
		if (m_IsPrefab)return;
		//順不動
		sliderWrite();
		colorEditorWrite();
		checkBoxWrite();
#endif
	}

	void GUIComponent::onConect()
	{
		auto pos = util::Vec2(0.0f, 0.0f);
		auto size = util::Vec2(256.0f, 256.0f);
		m_pUI = std::make_unique<util::ImGUI>(m_Entity.lock()->getName(), pos, size);

		try {
			//アイテムを追加する
			lua->callVoidFunc("addImte", this);
		}
		catch (luabind::error& e) {
			luabind::object errorMsg(luabind::from_stack(e.state(), -1));
			std::stringstream ss;
			ss << errorMsg << std::endl;
			auto hWnd = FindWindow("Debug", NULL);
			util::WinFunc::drawMessageBox(hWnd, "Error", ss.str().c_str());
		}
		//プレファブ用文字入力
		addInputText("PrefabText" + m_LuaName, "Empty");
		m_pInputText = getItem<util::ImInputText>("PrefabText" + m_LuaName);
		addButton(m_LuaName + "OutPut");
		m_pCeateButton = getItem<util::ImButton>(m_LuaName + "OutPut");
	}

	void GUIComponent::draw()
	{
#ifdef _MDEBUG
		m_pUI->begin();

		itemDraw();

		m_pUI->end();
#endif
	}

	void GUIComponent::itemDraw()
	{
		for (auto& item : m_Item)
		{
			item->draw(m_pUI.get());
		}
		prefabCreateButton();
	}

	void GUIComponent::showFPS()
	{
		m_pUI->text("FPS : " + std::to_string(framework::Application::getFPS()));
	}

	void GUIComponent::init()
	{

	}

	void GUIComponent::active()
	{
		framework::Render2DComponent::active();
	}

	void GUIComponent::deActive()
	{
		framework::Render2DComponent::deActive();
	}

	void GUIComponent::setParam(const std::vector<std::string>& param)
	{
		auto filePath = param[0];

		//auto removePeriod = m_ExtensionName;
		////ピリオド削除
		//removePeriod.erase(removePeriod.begin());

		//UIの名前部分だけ抽出
		std::vector<std::string> splitBuffer1;

		//ファイルパスを'/'ごとに分ける
		util::split(filePath, '/', &splitBuffer1);

		std::vector<std::string> splitBuffer2;

		util::split(*(splitBuffer1.end() - 1), '.', &splitBuffer2);

		m_LuaName = splitBuffer2[0];

		//バイナリファイルの名前をゲームオブジェクトの名前+Luaの名前にする
		m_SceneFolderPath = m_FolderPath + "/" + m_Entity.lock()->getName() + m_LuaName + ".byte";

		lua = std::make_unique<util::LuaCpp>(filePath);

		//読み込むファイルパスを差し替える
		if (param.size() >= 2) {
			if (param[1] != "") {
				m_IsPrefab = true;
				//プレファブバイナルだった場合はパスを作らずに直接読み込む
				m_SceneFolderPath = param[1];
			}
		}
		paramRead();

	}

	void GUIComponent::drawTransform()
	{
		auto trans = m_Entity.lock()->getTransform();
		m_pUI->text("World");
		m_pUI->text("X" + std::to_string(trans->m_Position.x)
			+ "Y" + std::to_string(trans->m_Position.x)
			+ "Z " + std::to_string(trans->m_Position.x));
		m_pUI->text("Rotation");
		m_pUI->text("X" + std::to_string(trans->m_Rotation.x)
			+ "Y" + std::to_string(trans->m_Rotation.y)
			+ "Z " + std::to_string(trans->m_Rotation.z));
	}

	void GUIComponent::createPrefab(const std::string& faileName)
	{
		auto createPath = m_PrefabPathPath;
		createPath += faileName;
		createPath += m_ExtensionName;

		auto temp = m_SceneFolderPath;

		m_SceneFolderPath = createPath;

		sliderWrite();
		colorEditorWrite();
		checkBoxWrite();

		m_SceneFolderPath = temp;
	}

	void GUIComponent::prefabCreateButton()
	{
		if (m_pInputText.expired())return;
		std::string fileName = m_pInputText.lock()->getText();
		if (!m_pCeateButton.lock()->onClick())return;
		createPrefab(fileName);
	}

	void GUIComponent::addText(const std::string & text)
	{
		m_Item.emplace_back(std::make_shared<util::ImText>(text));
	}

	void GUIComponent::addSlider(const std::string & text, const util::Vec2 & range, const float initNum)
	{
		auto find = bainalyToData(text);
		float _initNum;
		if (find == m_Paramas.end()) {
			//見つからなければ
			_initNum = initNum;
		}
		else {
			//見つけたidの次にデータが格納されている
			find++;
			_initNum = (*find);
		}

		m_Item.emplace_back(std::make_shared<util::ImSlider>(text, range, _initNum));
	}

	void GUIComponent::addColorEditor(const std::string & text, const util::Vec4 & initNum)
	{
		auto find = bainalyToData(text);
		util::Vec4 _initNum;
		if (find == m_Paramas.end()) {
			//見つからなければ
			_initNum = initNum;
		}
		else {
			//見つけたidの次にデータが格納されている
			find++;
			_initNum.x = (*find);
			find++;
			_initNum.y = (*find);
			find++;
			_initNum.z = (*find);
			find++;
			_initNum.w = (*find);
		}

		m_Item.emplace_back(std::make_shared<util::ImColorEditor>(text, _initNum));
	}

	void GUIComponent::addButton(const std::string & text)
	{
		m_Item.emplace_back(std::make_shared<util::ImButton>(text));
	}

	void GUIComponent::addCollapsingHeader(const std::string & text)
	{
		m_Item.emplace_back(std::make_shared<util::ImCollapsignHeader>(text));
	}

	void GUIComponent::addCheckBox(const std::string & text, const bool initNum)
	{
		auto find = bainalyToData(text);
		float _initNum;
		if (find == m_Paramas.end()) {
			//見つからなければ
			_initNum = initNum;
		}
		else {
			//見つけたidの次にデータが格納されている
			find++;
			_initNum = (*find);
		}

		m_Item.emplace_back(std::make_shared<util::ImCheckBox>(text, (bool)_initNum));
	}

	void GUIComponent::addInputText(const std::string & textName, const std::string & defaultName)
	{
		m_Item.emplace_back(std::make_shared<util::ImInputText>(textName, defaultName));
	}

	float GUIComponent::getSlider(const std::string & text)
	{
		auto slider = getItem<util::ImSlider>(text);
		return slider.lock()->constValue();
	}

	float* GUIComponent::getSliderPointer(const std::string & text)
	{
		auto slider = getItem<util::ImSlider>(text);
		return slider.lock()->valuePointer();
	}

	util::Vec4 GUIComponent::getColor(const std::string & text)
	{
		auto colorEditor = getItem<util::ImColorEditor>(text);
		return colorEditor.lock()->constValue();
	}

	util::Vec4* GUIComponent::getColorPointer(const std::string & text)
	{
		auto colorEditor = getItem<util::ImColorEditor>(text);
		return colorEditor.lock()->valuePointer();
	}

	std::string * GUIComponent::getTextPointer(const std::string & text)
	{
		auto imText = getItem<util::ImText>(text);
		return imText.lock()->getTextPointer();
	}

	bool GUIComponent::onClick(const std::string & text)
	{
		auto button = getItem<util::ImButton>(text);
		return button.lock()->onClick();
	}

	bool GUIComponent::isCheck(const std::string & text)
	{
		auto checkBox = getItem<util::ImCheckBox>(text);
		return checkBox.lock()->isCheckConstValue();
	}

	bool* GUIComponent::isCheckPointer(const std::string & text)
	{
		auto checkBox = getItem<util::ImCheckBox>(text);
		return checkBox.lock()->isCheckPointer();
	}

	void GUIComponent::setUIPosition(const util::Vec2 & pos)
	{
		m_pUI->setPositoin(pos);
	}

	void GUIComponent::setUIScale(const util::Vec2 & size)
	{
		m_pUI->setScale(size);
	}

	std::list<std::weak_ptr<util::IImItem>> GUIComponent::getItemAllFromThisInstance()
	{
		std::list<std::weak_ptr<util::IImItem>> result;

		for (auto& item : m_Item)
		{
			result.emplace_back(item);
		}

		return result;
	}

	std::list<std::weak_ptr<util::IImItem>> GUIComponent::getItemAllFromEntity()
	{
		std::list<std::weak_ptr<util::IImItem>> allItem;

		//すべてのUIを取得
		auto uis = m_Entity.lock()->getComponents<component::GUIComponent>();

		for (auto& ui : uis)
		{
			auto getItem = ui.lock()->getItemAllFromThisInstance();
			//すべてのアイテムを集める
			allItem.insert(allItem.begin(), getItem.begin(), getItem.end());
		}
		return allItem;
	}

	std::string GUIComponent::getLuaName()
	{
		return m_LuaName;
	}

	std::weak_ptr<GUIComponent> GUIComponent::getUIComponent(std::list<std::weak_ptr<GUIComponent>>& uis, const std::string & luaName)
	{
		auto find = std::find_if(uis.begin(), uis.end(), [&](std::weak_ptr<GUIComponent>& ui) {
			return ui.lock()->getLuaName() == luaName;
		});

		if (find == uis.end())return std::weak_ptr<GUIComponent>();

		return *find;
	}

	void GUIComponent::clearItem()
	{
		m_Item.clear();
	}

	void GUIComponent::paramRead()
	{
		//プレファブのときはフォルダを作らない
		if (!m_IsPrefab) {
			//読み込む前にフォルダがあるか確かめる
			struct stat statBuf;

			//もしシーンフォルダにメタフォルダがなければ作成する
			if (stat(m_FolderPath.c_str(), &statBuf) != 0) {
				//UIメタフォルダ作成
				_mkdir(m_FolderPath.c_str());
			}
		}

		util::BinaryLoader loader(m_SceneFolderPath);

		loader.load<float>(&(m_Paramas));
	}


	void GUIComponent::sliderWrite(util::FileType type)
	{
		//データをファイルを保存する
		util::BinaryWriter writer(m_SceneFolderPath, type);

		auto sliders = getItemTypeAll < util::ImSlider >();
		std::vector<float> writeData;
		for (auto& slider : sliders) {
			slider.lock()->getWriteData(&writeData);
		}

		writer.write<float>(writeData);
	}

	void GUIComponent::colorEditorWrite(util::FileType type)
	{
		//データをファイルを保存する
		//sliderの後に格納されるようにする
		util::BinaryWriter writer(m_SceneFolderPath, type);

		auto colors = getItemTypeAll < util::ImColorEditor >();
		std::vector<float> writeData;
		for (auto& color : colors) {
			color.lock()->getWriteData(&writeData);
		}

		writer.write<float>(writeData);
	}

	void GUIComponent::checkBoxWrite(util::FileType type)
	{
		//データをファイルを保存する
		//colorの後に格納されるようにす
		util::BinaryWriter writer(m_SceneFolderPath, type);

		auto checkBox = getItemTypeAll < util::ImCheckBox >();
		std::vector<float> writeData;
		for (auto& check : checkBox) {
			check.lock()->getWriteData(&writeData);
		}

		writer.write<float>(writeData);
	}

	std::vector<float>::iterator GUIComponent::bainalyToData(const std::string & text)
	{
		float id = std::hash<std::string>{}(text);

		auto find = std::find_if(m_Paramas.begin(), m_Paramas.end(), [&](const float param) {
			return param == id;
		});

		return find;
	}
}
