#pragma once
#include<Framework.h>

namespace component {

	class GUIComponent;

	class SoundPlayerComponent : public framework::UpdateComponent
	{
	public:
		SoundPlayerComponent();
		~SoundPlayerComponent();


		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		void init()override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param)override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

	private:
		std::string m_SoundName;
		//!最初に設定されたボリューム
		float m_MaxVolume;
		float* m_Volume;
		float* m_MaxDistance;
		float* m_MinDistance;
		bool* m_isDoppler;

		//音のフェードタイマー
		util::Timer m_SoundFadeTimer;

		std::weak_ptr<GUIComponent> m_pUI;
		std::string m_LuaPath;
		std::string m_PrefabPath;
	};
}