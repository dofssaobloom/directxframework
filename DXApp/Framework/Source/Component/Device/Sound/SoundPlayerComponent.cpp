#include "Framework.h"
#include "SoundPlayerComponent.h"
#include<Source\Component\UI\GUIComponent.h>

namespace component {

	SoundPlayerComponent::SoundPlayerComponent()
		:m_SoundFadeTimer(30 * 3)
	{
	}

	SoundPlayerComponent::~SoundPlayerComponent()
	{
	}

	void SoundPlayerComponent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath,m_PrefabPath });

		(*componentInitalizer).emplace_back(m_pUI);
	}

	void SoundPlayerComponent::init()
	{
		framework::ResourceManager::getInstance()->playSound(m_SoundName, true);

		m_Volume = m_pUI.lock()->getSliderPointer("Volume");
		m_MaxVolume = *m_Volume;
		*m_Volume = DSBVOLUME_MIN;
		m_MaxDistance = m_pUI.lock()->getSliderPointer("MaxDistance");
		m_MinDistance = m_pUI.lock()->getSliderPointer("MinDistance");
		m_isDoppler = m_pUI.lock()->isCheckPointer("Doppler");


		framework::ResourceManager::getInstance()->setVolume(m_SoundName, *m_Volume);
		framework::ResourceManager::getInstance()->setDoppler(m_SoundName, *m_isDoppler);
		framework::ResourceManager::getInstance()->setDistance(m_SoundName, *m_MinDistance, *m_MaxDistance);

		//ドップラーの計算に必要な速度
		auto speed = util::Vec3(1.0f, 0.0f, 0.0f);
		framework::ResourceManager::getInstance()->setDopplerVelocity(m_SoundName, speed);

		//位置を最初にセットする
		auto trans = m_Entity.lock()->getTransform();
		framework::ResourceManager::getInstance()->setPosition(m_SoundName, *trans);

		m_SoundFadeTimer.init();
	}

	void SoundPlayerComponent::setParam(const std::vector<std::string>& param)
	{
		m_SoundName = param[0];

		m_LuaPath = param[1];
		m_PrefabPath = param[2];
	}

	void SoundPlayerComponent::update()
	{
		if (!m_SoundFadeTimer.isEnd() && !(*m_isDoppler)) {
			*m_Volume = util::lerp<float>(1.0f - m_SoundFadeTimer.rate(), -2500, m_MaxVolume);
			framework::ResourceManager::getInstance()->setVolume(m_SoundName, *m_Volume);
		}

#ifdef _MDEBUG
		framework::ResourceManager::getInstance()->setVolume(m_SoundName, *m_Volume);
		framework::ResourceManager::getInstance()->setDoppler(m_SoundName, *m_isDoppler);
		framework::ResourceManager::getInstance()->setDistance(m_SoundName, *m_MinDistance, *m_MaxDistance);
#endif
		m_SoundFadeTimer.update();


		if (m_isDoppler) {
			//位置の更新
			auto trans = m_Entity.lock()->getTransform();
			framework::ResourceManager::getInstance()->setPosition(m_SoundName, *trans);
		}
	}
}