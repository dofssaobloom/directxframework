#include "Framework.h"
#include "ListenerComponent.h"
#include<Windows.h>


namespace component {

	ListenerComponent::ListenerComponent()
	{
	}

	ListenerComponent::~ListenerComponent()
	{
	}

	void ListenerComponent::init()
	{
		m_pListener = framework::ResourceManager::getInstance()->createListener();
		m_pListener->createSoundListener();

		m_pListener->setDistance(1.0f);
		m_pListener->setRollOff(1.0f);
		m_pListener->setDoppler(1.0f);

		//ドップラー効果に必要な速度
		auto velocity = util::Vec3(1.0f, 0.0f, 0.0f);
		m_pListener->setDopplerVelocity(velocity);

		//リスナーの位置
		auto trans = m_Entity.lock()->getTransform();
		m_pListener->setPosition(*trans);
	}

	void ListenerComponent::onConect()
	{
	}

	void ListenerComponent::setParam(const std::vector<std::string>& param)
	{
	}

	void ListenerComponent::update()
	{
		//位置の更新
		auto trans = m_Entity.lock()->getTransform();
		m_pListener->setPosition(*trans);

		//リスナーの方向
		auto front = -trans->front();
		//front.x -= front.x;
		m_pListener->setDirection((front));
	}
}