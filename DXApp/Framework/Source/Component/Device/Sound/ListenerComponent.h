#pragma once
#include<Framework.h>
#include<Source\Resource\Sound\SoundListener.h>

namespace component {

	class ListenerComponent : public framework::UpdateComponent
	{
	public:
		ListenerComponent();
		~ListenerComponent();

		void init()override;

		void onConect()override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param)override;

		// UpdateComponent を介して継承されました
		virtual void update() override;

	private:
		std::shared_ptr<framework::SoundListener> m_pListener;
	};
}