#include "Framework.h"
#include "LineRenderComponent.h"


namespace component {

	LineRenderComponent::LineRenderComponent()
		:m_PosOffset()
	{
		
	}

	LineRenderComponent::~LineRenderComponent()
	{
	}

	void LineRenderComponent::init()
	{
		m_Transform = *(m_Entity.lock()->getTransform());
	}

	void LineRenderComponent::draw()
	{
		//リストが空またはレンダラーがヌルポだったら何もしない
		if (m_PointList.empty() || m_pLineRenderer == nullptr)return;

		//ポジションの更新
		m_Transform.m_Position = m_Entity.lock()->getTransform()->m_Position + m_PosOffset;
		m_Transform.m_Rotation = m_Entity.lock()->getTransform()->m_Rotation;
		m_pLineRenderer->draw(&m_Transform);
	}

	void LineRenderComponent::setParam(const std::vector<std::string>& param)
	{
		m_LineWidth = std::atof(param[0].c_str());
	}

	void LineRenderComponent::addPoint(framework::LineVertexLayout & pointData)
	{
		m_PointList.emplace_back(pointData);
		m_pLineRenderer = std::make_unique<framework::LineRenderer>(m_PointList);
		m_pLineRenderer->setWidth(m_LineWidth);
	}

	void LineRenderComponent::addPoint(std::vector<framework::LineVertexLayout>& pointData)
	{
		//連結
		m_PointList.insert(m_PointList.end(), pointData.begin(), pointData.end());
		m_pLineRenderer = std::make_unique<framework::LineRenderer>(m_PointList);
		m_pLineRenderer->setWidth(m_LineWidth);
	}

	void LineRenderComponent::pointListClear()
	{
		m_PointList.clear();
	}

	void LineRenderComponent::setLineWidth(float width)
	{
		m_LineWidth = width;
		m_pLineRenderer->setWidth(m_LineWidth);
	}

	void LineRenderComponent::depthDraw()
	{
	}

	int LineRenderComponent::drawNum()
	{
		return 1;
	}

	void LineRenderComponent::cheackDelete()
	{
	}

	void LineRenderComponent::setOffset(const util::Vec3& offset) {
		m_PosOffset = offset;
	}

	void LineRenderComponent::setScale(const util::Vec3& scale) {
		m_Transform.m_Scale = scale;
	}

	void LineRenderComponent::setControllPoint(int id,const util::Vec3 point)
	{
		m_pLineRenderer->setControllPoint(id,point);
	}
	void LineRenderComponent::setColor(const util::Vec3 & color)
	{
		m_pLineRenderer->setColor(color);
	}
}