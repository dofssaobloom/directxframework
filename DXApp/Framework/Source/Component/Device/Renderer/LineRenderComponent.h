#pragma once
#include<Framework.h>
#include<Source\Device\Render\Renderer\3D\Single\LineRenderer.h>

namespace framework {
	class LineRenderer;
	struct LineVertexLayout;
}

namespace component {

	class LineRenderComponent : public framework::Render3DComponent
	{
	public:
		LineRenderComponent();
		~LineRenderComponent();

		/**
		* @brief		初期化
		*/
		virtual void init()override;

		/**
		* @brief		描画
		*/
		virtual void draw() override;

		/**
		* @brief			パラメータセット
		* @param param		文字列パラメータ
		*/
		void setParam(const std::vector<std::string>& param)override;

		/// <summary>
		/// 一点の追加
		/// </summary>
		/// <param name="pointData"></param>
		void addPoint(framework::LineVertexLayout& pointData);

		/// <summary>
		/// 一度にリストでポイントを追加
		/// </summary>
		/// <param name="pointData"></param>
		void addPoint(std::vector<framework::LineVertexLayout>& pointData);

		/// <summary>
		/// ポイントリストクリア
		/// </summary>
		void pointListClear();

		/// <summary>
		/// ラインの太さセット
		/// </summary>
		/// <param name="width"></param>
		void setLineWidth(float width);

		// Render3DComponent を介して継承されました
		virtual void depthDraw() override;

		virtual int drawNum() override;

		virtual void cheackDelete() override;

		void setOffset(const util::Vec3& offset);

		void setScale(const util::Vec3& scale);


		void setControllPoint(int id,const util::Vec3 point);

		void setColor(const util::Vec3& color);


	private:
		std::unique_ptr<framework::LineRenderer> m_pLineRenderer;
		std::vector<framework::LineVertexLayout> m_PointList;

		//!ポジションのオフセット
		util::Vec3 m_PosOffset;

		//!描画用トランスフォーム
		util::Transform m_Transform;
		float m_LineWidth;

	};
}