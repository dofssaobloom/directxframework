#include"CameraComponent.h"
#include<Source\Entity\Entity.h>
#include<assert.h>
#include<Source\Resource\Texture\MSAAMultiRenderTarget.h>
#include<Source\Application\Screen\Screen.h>
#include<D3DX11.h>
#include<Source\Resource\Texture\DepthTarget.h>
#include<Source\Util\Render\DXFunc.h>
#include<Source\Component\Effect\BloomEffect.h>
#include<Source\Resource\Texture\Texture2D.h>
#include<Source\Resource\Texture\MSAADepthTarget.h>
#include<Source\Device\Render\Shader\DeferredLighting.h>
#include<Source\Resource\Texture\MultiRenderTarget.h>
#include<Source\Device\Render\Shader\PostEffectPass.h>
#include<Source\Device\Render\Shader\GradationFilter.h>

using namespace framework;

namespace component {

	WeakCamera CameraComponent::m_MainCamera;

	CameraComponent::CameraComponent()
	{
		m_CallOrder = 0;
	}

	CameraComponent::~CameraComponent()
	{
	}

	void CameraComponent::init()
	{
		if (m_LookEntity.expired()) {
			m_Camera.setLookAt(util::Vec3(0, 0, 1));
			m_Radius = 100;
		}
		else {
			//初期の距離からカメラ距離を固定
			auto length = m_Entity.lock()->getTransform()->m_Position - m_LookEntity.lock()->getTransform()->m_Position;

			m_Radius = length.length();
		}

		m_pLighting = std::make_unique<framework::DeferredLighting>();

		std::vector<std::shared_ptr<Texture2D>> textureContainer;

		auto size = m_pGBuffer->getSize();

		for (size_t i = 0,end = m_pGBuffer->getBufferNum(); i < end; ++i)
		{
			textureContainer.emplace_back(std::make_shared<Texture2D>(size.x, size.y, m_pGBuffer->getShaderView(i)));
		}

		auto env = ResourceManager::getInstance()->getTextureCube("Environment");

		size = env->getSize();
		//最後に環境マップを追加する
		textureContainer.emplace_back(std::make_shared<Texture2D>(size.x, size.y, env->getShaderResourceView()));

		m_pLighting->setTexture( textureContainer);

		//自身についているポストエフェクトコンポーネント取得
		findPostEffect();

		////影ぼかし
		/*m_pShadowGradationV = std::make_unique<framework::GradationFilter>(std::make_shared<framework::Texture2D>(framework::Screen::WINDOW_WIDTH,
			framework::Screen::WINDOW_HEIGHT,
			m_pGBuffer->getShaderView(4)));
		m_pShadowGradationV->setShift(2);
		m_pShadowGradationV->setBlurType(framework::GradationFilter::BlurType::Vertical);
		
		m_pShadowGradationH = std::make_unique<framework::GradationFilter>(m_pShadowGradationV->getTex2D());
		m_pShadowGradationH->setShift(20);
		m_pShadowGradationH->setBlurType(framework::GradationFilter::BlurType::Horizontal);
*/
		m_pPass = std::make_unique<framework::PostEffectPass>(m_PostEfect, m_pGBuffer, m_pLBuffer);
	}

	void CameraComponent::active()
	{
		//すべてのカメラコンポーネントを非アクティブにして自分をアクティブにする
		cameraLoop([](WeakCamera camera) {
			camera.lock()->deActive();
		});

		UpdateComponent::active();

		m_MainCamera = m_Entity.lock()->getComponent<CameraComponent>(this);
		update();//ビュー行列に変換する際にデータがすべて0だとエラーが出るので一度更新しておく
	}

	void CameraComponent::deActive()
	{
		UpdateComponent::deActive();
		//メインカメラを消す
		m_MainCamera.reset();
	}

	void CameraComponent::update()
	{
		//ポインタが不正になっていたら処理しない
		if (m_Entity.expired())return;
		auto trans = m_Entity.lock()->getTransform();
		m_Camera.setEyePosition(trans->m_Position + m_EyeOffset);
		m_Camera.setOffset(m_LookOffset);
		if (!m_LookEntity.expired()) {
			//注視点がセットされている場合はそれを見る
			posUpdaet();
		}
		else {
			//注視点が無い場合はトランスフォームのZ方向を見る
			m_Camera.setLookAt(trans->m_Position + trans->front() * 1.0f);
		}

		//90度になってしまうと描画がばるってしまうのでクランプ
		trans->m_Rotation.x = util::clamp<float>(trans->m_Rotation.x, -89, 89);
	}

	util::Mat4 CameraComponent::toViewMatrix()
	{
		return m_Camera.toViewMatrix().toXMMatrix();
	}

	util::Mat4 CameraComponent::getProjection()
	{
		return m_Camera.getProjMatrix();
	}

	void CameraComponent::setParam(const std::vector<std::string>& param)
	{
		if (param.empty())return;

		lookSet(param);

		renderTargetNumSet(param);

		m_Friction = std::atof(param[1].c_str());
	}

	void CameraComponent::setLookOffset(const util::Vec3 offset)
	{
		m_LookOffset = offset;
	}

	const util::Vec3 & CameraComponent::getOffet()
	{
		return m_LookOffset;
	}

	void CameraComponent::setEyeOffset(const util::Vec3 offset)
	{
		m_EyeOffset = offset;
	}

	void CameraComponent::doLighting()
	{
		//影ぼかし更新
		//m_pShadowGradationV->write();
		//m_pShadowGradationH->write();


		util::setSingleViewPort(framework::Screen::PIXEL_WIDTH, framework::Screen::PIXEL_HEIGHT);
		m_pLBuffer->clear(util::Vec4(0, 0, 0, 1));
		util::getContext()->OMSetRenderTargets(m_pLBuffer->getBufferNum(), m_pLBuffer->getView(), NULL);
		m_pLighting->draw();
		util::getContext()->OMSetRenderTargets(0, NULL, NULL);
	}

	void CameraComponent::doEffect()
	{
		//内部テクスチャの更新
		m_pPass->rendering();
	}

	void CameraComponent::beginToBuffer(std::shared_ptr<framework::DepthTarget>& inDepth)
	{
		util::getContext()->OMSetRenderTargets(m_pGBuffer->getBufferNum(), m_pGBuffer->getView(), inDepth->getView());
		m_pGBuffer->clear(util::Vec4(0,0,0,1));
		inDepth->clear();

		m_pPass->setDepth(inDepth);
	}

	void CameraComponent::endToBuffer()
	{
		util::getContext()->OMSetRenderTargets(0, NULL, NULL);
		//m_pGBuffer->resolveTexutre();
	}

	void CameraComponent::gBufferDraw(int drawLocation)
	{
		m_pGBuffer->draw(drawLocation);
	}

	void CameraComponent::lightingDraw(int drawLocation)
	{
		m_pLBuffer->draw(drawLocation);
	}

	void CameraComponent::finalBufferDraw()
	{
		//アクティブなエフェクトを追加する
		for (auto& e : m_PostEfect) {
			if (e.lock()->isActive()) {
				addImageEffect(e);
			}
			else {
				removeImageEffect(e);
			}
		}

		m_pPass->draw();
	}

	void CameraComponent::addImageEffect(const std::weak_ptr<PostEffectComponent>& effect)
	{
		m_pPass->addEffect(effect);
	}

	void CameraComponent::removeImageEffect(const std::weak_ptr<PostEffectComponent>& effect)
	{
		m_pPass->removeEffect(effect);
	}

	framework::WeakEntity CameraComponent::getLookEntity()
	{
		return m_LookEntity;
	}

	util::Vec3 CameraComponent::convertViewDir(const util::Vec3 & vec)
	{
		auto cameraTrans = component::CameraComponent::getMainCamera().lock()->getGameObj().lock()->getTransform();
		auto cameraTransCopy(*cameraTrans);

		auto front = cameraTransCopy.front();
		front.y = 0;

		util::Mat4 rotete;
		util::createMatrixFromFront(front.normalize(), &rotete);

		//カメラの向いてる方向に回転させる
		return XMVector3Transform(vec.toXMVector(), rotete.toXMMatrix());
	}

	std::vector<WeakCamera> CameraComponent::allCameras()
	{
		std::vector<WeakCamera> cameraContainer;
		cameraLoop([&](WeakCamera camera) {
			cameraContainer.emplace_back(camera);
		});
		return cameraContainer;
	}

	WeakCamera CameraComponent::findCamera(const std::string& cameraEntityName)
	{
		WeakCamera findCamera;
		haveCameraEntityLoop([&](WeakEntity entity) {
			if (entity.lock()->getName() != cameraEntityName)return;
			findCamera = entity.lock()->getComponent<CameraComponent>();
		});
		return findCamera;
	}

	std::vector<WeakCamera> CameraComponent::findCameras(const std::string & cameraEntityName)
	{
		std::vector<WeakCamera> findCamera;
		haveCameraEntityLoop([&](WeakEntity entity) {
			if (entity.lock()->getName() != cameraEntityName)return;
			auto camerasList = entity.lock()->getComponents<CameraComponent>();
			for (auto camera : camerasList) {
				findCamera.emplace_back(camera);
			}
		});
		return findCamera;
	}

	WeakCamera CameraComponent::getMainCamera()
	{
		return m_MainCamera;
	}

	void CameraComponent::lookSet(const std::vector<std::string>& param)
	{
		if (param[0] == "")return;
		if (param[0] == "none")return;

		auto lookEntity = framework::Entity::findGameObj(param[0]);
		assert(!lookEntity.expired() && "注視点に指定したEntityが存在しないか不正です");

		m_LookEntity = lookEntity;
	}

	void CameraComponent::renderTargetNumSet(const std::vector<std::string>& param)
	{
		DXGI_FORMAT format[8] = { DXGI_FORMAT_R8G8B8A8_UNORM ,DXGI_FORMAT_R16G16B16A16_FLOAT ,DXGI_FORMAT_R32G32B32A32_FLOAT,DXGI_FORMAT_R16G16B16A16_FLOAT ,
			DXGI_FORMAT_R16G16B16A16_FLOAT,DXGI_FORMAT_R8G8B8A8_UNORM,DXGI_FORMAT_R8G8B8A8_UNORM,DXGI_FORMAT_R16G16B16A16_FLOAT };


		m_pGBuffer = std::make_shared<MultiRenderTarget>(8, Screen::PIXEL_WIDTH, Screen::PIXEL_HEIGHT, format);
		format[0] = DXGI_FORMAT_R16G16B16A16_FLOAT;
		format[1] = DXGI_FORMAT_R16G16B16A16_FLOAT;
		m_pLBuffer = std::make_shared<framework::MultiRenderTarget>(2, Screen::PIXEL_WIDTH, Screen::PIXEL_HEIGHT, format);
	}

	void CameraComponent::findPostEffect()
	{
		m_PostEfect = m_Entity.lock()->getComponents<PostEffectComponent>();
	}

	void CameraComponent::posUpdaet()
	{
		auto lookTrans = m_LookEntity.lock()->getTransform();
		auto preLook = m_Camera.getLookPos();
		auto velocity = lookTrans->m_Position - preLook;
		auto newPos = preLook + (velocity * m_Friction);
		m_Camera.setLookAt(newPos);
		auto trans = m_Entity.lock()->getTransform();
		auto temp = trans->m_Position - newPos;
		trans->lookAt(temp);
	}

	void CameraComponent::cameraLoop(std::function<void(WeakCamera)> cameraAction)
	{
		haveCameraEntityLoop([&](WeakEntity entity) {
			auto components = entity.lock()->getComponents<CameraComponent>();
			for (auto camera : components) {
				cameraAction(camera);
			}
		});

	}
	void CameraComponent::haveCameraEntityLoop(std::function<void(WeakEntity)> entityAction)
	{
		//カメラコンポーネントを持っているエンティティーをすべて探す
		haveComponentEntityLoop<CameraComponent>(entityAction);
	}
}