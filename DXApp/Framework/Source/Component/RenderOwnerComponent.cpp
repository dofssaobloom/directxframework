#include"RenderOwnerComponent.h"
#include<Source\Device\Render\Renderer\3D\Instance\RigidInstanceRenderer.h>
#include<Source\Device\Render\Renderer\3D\Instance\SkinningInstanceRenderer.h>
#include<Source\Entity\Entity.h>
#include<Source\Component\RenderClientComponent.h>
#include<algorithm>
#include<Source\Util\WrapFunc.h>
#include<Source\Device\Render\Renderer\3D\Instance\CubeMapInstanceRenderer.h>
#include<Source\Util\Render\RenderTargetStack.h>
#include<Source\Resource\Texture\CubeReanderTarget.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Device\Render\Renderer\3D\Instance\SkinningInstanceRenderer.h>
#include<Source\Device\Input\Input.h>
#include<Source\Util\Lua\LuaBase.h>
#include<Source\Component\UI\GUIComponent.h>

namespace component {

	RenderOwnerComponent::RenderOwnerComponent()
	{
		m_CallOrder = 100;
	}

	RenderOwnerComponent::~RenderOwnerComponent()
	{
	}

	void RenderOwnerComponent::init()
	{
		m_Renderer->init();
		m_Renderer->setMaterialParam(m_MaterialParam);

		//アクティブにされているクライアントはクライアント自信がリストに追加するので集める必要がない
		auto skinningRenderer = std::dynamic_pointer_cast<framework::SkinningInstanceRenderer>(m_Renderer);
		if (skinningRenderer.get() != nullptr) {
			//適当なクライアントのアニメータを取得してモーションテクスチャ作成
			auto animator = m_DrawObject.begin()->second.lock()->getAnimator();
			skinningRenderer->writeBoneTexture(animator);
		}

		auto uiObj = framework::Entity::findGameObj("DefferdLightUI");
		if (!uiObj.expired()) {
			m_UI = uiObj.lock()->getComponent<GUIComponent>();
		}
	}

	void RenderOwnerComponent::draw()
	{

		std::list<util::Transform*> drawList;
		//描画オブジェクトのトランスフォーム取り出し
		for (auto& obj : m_DrawObject) {
			drawList.emplace_back(obj.first.lock()->getTransform());
		}

#ifdef _MDEBUG
		if (!m_UI.expired()) {
			m_MaterialParam.specular = m_UI.lock()->getSlider("Specular");
			m_MaterialParam.mettalic = m_UI.lock()->getSlider("Metallic");
			m_Renderer->setMaterialParam(m_MaterialParam);
		}
#endif

		m_Renderer->draw(drawList);
	}

	void RenderOwnerComponent::depthDraw()
	{

		//削除対象削除
		cheackDelete();

		std::list<util::Transform*> drawList;
		//描画オブジェクトのトランスフォーム取り出し
		for (auto& obj : m_DrawObject) {
			drawList.emplace_back(obj.first.lock()->getTransform());
		}
	

		m_Renderer->depthDraw(drawList);

		for (auto obj : m_DrawObject)
		{
			obj.second.lock()->m_PrePosition = *(obj.first.lock()->getTransform());
		}
	}

	void RenderOwnerComponent::setParam(const std::vector<std::string>& param)
	{
		paramCheck(param);

		const int textureNum = std::atoi(param[1].c_str());
		util::foreach(textureNum, [&](int i) {
			//モデルの名前は飛ばして読み込む
			m_TextureNames.emplace_back(param[i + 2]);
		});

		//モデルのタイプを見てレンダラを切り替える
		auto model = framework::ResourceManager::getInstance()->getModel(param[0]);
		if (model->type == framework::ModelType::rigid)
			m_Renderer = std::make_shared<framework::RigidInstanceRenderer>(model, m_TextureNames);
		else if (model->type == framework::ModelType::skin) {
			skinningSetting(param, model);
		}
		m_ModelName = param[0];

		//!テクスチャーまでのオフセット（パラメータの最初の２つはモデルの名前とテクスチャの枚数）
		const int textureOffset = 2;

		util::LuaBase lua(param[textureNum + textureOffset].c_str());

		m_MaterialParam.height = lua.getParam<float>("Height");
		m_MaterialParam.specular = lua.getParam<float>("Specular");
		m_MaterialParam.isNotLighting = lua.getParam<bool>("IsNotLighting");
		m_MaterialParam.bias = lua.getParam<float>("Bias");

		//m_MaterialParam.height = std::atof(param[textureNum + textureOffset].c_str());
		////!次のマテリアルパラメータ読み込み
		//m_MaterialParam.specular = std::atof(param[textureNum + textureOffset + 1].c_str());
		////!次のマテリアルパラメータ読み込み
		//m_MaterialParam.isNotLighting = std::atof(param[textureNum + textureOffset + 2].c_str());
	}

	std::string RenderOwnerComponent::getModelName()
	{
		return m_ModelName;
	}

	std::vector<std::string> RenderOwnerComponent::getTexNames()
	{
		return m_TextureNames;
	}

	std::vector<std::weak_ptr<RenderClientComponent>> RenderOwnerComponent::getClients()
	{
		//詰め替える
		std::vector<std::weak_ptr<RenderClientComponent>> result;
		for (auto& obj : m_DrawObject) {
			result.emplace_back(obj.second);
		}

		return result;
	}

	int RenderOwnerComponent::drawNum()
	{
		auto element = getActiveElement();
		return element.size();
	}

	void RenderOwnerComponent::clear()
	{
		m_TextureNames.clear();
		m_Renderer->clear();
	}

	void RenderOwnerComponent::addDrawClient(std::weak_ptr<RenderClientComponent>& client)
	{
		m_DrawObject.emplace_back(std::make_pair(client.lock()->getGameObj(), client));
	}

	void RenderOwnerComponent::removeDrawClient(std::weak_ptr<RenderClientComponent>& client)
	{
		m_RemoveList.emplace_back(client);
	}

	void  RenderOwnerComponent::cheackEntity(std::function<void(DrawElement)> action)
	{
		std::list<util::Transform*> drawTrans;

		//!条件に合ったものをコピーするための変数
		DrawContainer copy;

		//描画条件を満たしたものを抽出
		std::copy_if(std::begin(m_DrawObject), std::end(m_DrawObject), std::back_inserter(copy),
			[&](DrawElement element) {
			return isOK(element);
		});

		for (auto& element : copy) {
			action(element);
		}
	}

	std::list<util::Transform*> RenderOwnerComponent::getActiveTransform()
	{
		std::list<util::Transform*> drawTrans;
		cheackEntity([&](DrawElement element) {
			drawTrans.emplace_back(element.first.lock()->getTransform());

		});

		return drawTrans;
	}

	std::list<DrawElement> RenderOwnerComponent::getActiveElement()
	{
		std::list<DrawElement> activeElement;
		cheackEntity([&](DrawElement element) {
			activeElement.emplace_back(element);
		});
		return activeElement;
	}

	bool RenderOwnerComponent::isOK(const DrawElement & element)
	{
		//描画コンポーネントがアクティブである
		if (element.second.lock()->isActive() && !element.first.expired()) {
			return true;
		}

		return false;
	}

	void RenderOwnerComponent::cheackDelete()
	{
		//エンティティーまたはコンポーネントのポインタがなくなっていればリストから削除
		auto deleteList = std::remove_if(m_DrawObject.begin(), m_DrawObject.end(), [&](DrawElement element) {
			return element.first.expired() || element.second.expired();
		});

		//要素削除
		m_DrawObject.erase(deleteList, m_DrawObject.end());

		for (auto& remove : m_RemoveList) {
			m_DrawObject.remove_if([&](DrawElement& drawElement) {
				return drawElement.second._Get() == remove._Get();
			});
		}

		m_RemoveList.clear();
	}

	void RenderOwnerComponent::eachEntity(std::function<void(std::weak_ptr<framework::Entity>)> action)
	{
		auto entityList = framework::Entity::findHaveComponentEntity<RenderClientComponent>();
		for (auto& entity : entityList) {
			action(entity);
		}
	}

	void RenderOwnerComponent::skinningSetting(const std::vector<std::string>& param, std::shared_ptr<framework::Model> model)
	{
		std::vector<std::string> motionNames;

		if (param.size() < 11)return;

		//モーションの名前取得
		util::foreach(param.size() - 10, [&](int i) {
			//モーションと同じ名前でモーションを登録する
			motionNames.emplace_back(param[i + 10]);
		});

		m_Renderer = std::make_shared<framework::SkinningInstanceRenderer>(model, m_TextureNames, motionNames);
	}

	void RenderOwnerComponent::culcViewCullingVertex()
	{
	}



}