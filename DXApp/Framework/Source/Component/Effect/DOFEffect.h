#pragma once

#include "PostEffectComponent.h"
#include<Source\Device\Render\Renderer\2D\Single\PostEffectRenderer.h>


namespace component {
	class GUIComponent;

	class DOFEffect :public PostEffectComponent
	{
	public:
		DOFEffect();
		~DOFEffect();

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;


		void init()override;


		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		/**
		* @brief		ポストエフェクトで必要なテクスチャを選択できる
		* @param texContainer	テクスチャコンテナ
		*/
		virtual void selectTexture(framework::TextureContainer& texContainer)override;

		// PostEffectComponent を介して継承されました
		virtual void effectDraw(util::Transform * trans) override;


	private:

		struct DOFParam {
			XMMATRIX world;
			float center;
			float near_;
			float far_;
			float damy;
		};

		std::string m_LuaPath;

		float m_Center;
		float m_Horizontal;
		float m_Vertical;

		std::unique_ptr<framework::PostEffectRenderer<DOFParam>> m_pRenderer;

		std::weak_ptr<GUIComponent> m_pUI;

	};

}