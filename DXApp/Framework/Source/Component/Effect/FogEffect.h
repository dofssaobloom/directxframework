#pragma once

#include<Framework.h>
#include"PostEffectComponent.h"
#include<Source\Device\Render\Renderer\2D\Single\PostEffectRenderer.h>

namespace component {

	class GUIComponent;


	class FogEffect : public PostEffectComponent
	{
	public:
		FogEffect();
		~FogEffect();

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;


		void init()override;

		/**
		* @brief		ポストエフェクトで必要なテクスチャを選択できる
		* @param texContainer	テクスチャコンテナ
		*/
		virtual void selectTexture(framework::TextureContainer& texContainer)override;

		// PostEffectComponent を介して継承されました
		virtual void effectDraw(util::Transform * trans) override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

	private:

		struct  FogParam
		{
			XMMATRIX world;
			util::Vec3 fogClor;
			float start;
			float end;
			float near_;
			float far_;
			float damy;
		};

		FogParam m_Param;

		std::unique_ptr<framework::PostEffectRenderer<FogParam>> m_pRenderer;
		std::string m_LuaPath;
		std::weak_ptr<GUIComponent> m_pUI;

	};

}
