#include "Framework.h"
#include "GradationEffect.h"
#include<Source\Application\Screen\Screen.h>
#include<Source\Util\Lua\LuaBase.h>
#include<Source\Component\UI\GUIComponent.h>


namespace component {

	GradationEffect::GradationEffect()
	{
	}

	GradationEffect::~GradationEffect()
	{
	}


	void GradationEffect::init()
	{
		m_pRenderer = std::make_unique<framework::PostEffectRenderer<GradationParam>>("SpriteMaterial");

		GradationParam param;
		param.alpha = 1.0f;
		param.posisionOffset[0] = util::Vec4();
		param.posisionOffset[1] = util::Vec4();

		m_pRenderer->setBuffer(param);
		m_pRenderer->setSize(util::Vec2(framework::Screen::WINDOW_WIDTH, framework::Screen::WINDOW_HEIGHT));

		m_Vertical = 2;
		m_Horizontal = 2;
	}
	void GradationEffect::selectTexture(framework::TextureContainer & texContainer)
	{

		auto tex0 = Gdatation(2, texContainer["Main"], framework::GradationFilter::BlurType::Horizontal, m_Horizontal);

		auto tex1 = Gdatation(1, tex0, framework::GradationFilter::BlurType::Vertical, m_Vertical);


		util::setSingleViewPort(framework::Screen::PIXEL_WIDTH, framework::Screen::PIXEL_HEIGHT);
		std::vector<std::shared_ptr<framework::Texture2D>> textures;
		textures.emplace_back(tex1);


		m_pRenderer->setTexture(textures);
	}

	void GradationEffect::effectDraw(util::Transform * trans)
	{

		m_pRenderer->draw(trans);
	}

	void GradationEffect::setShift(const util::Vec2& shift)
	{
		m_Vertical = shift.x;
		m_Horizontal = shift.y;
	}


	std::shared_ptr<framework::Texture2D> GradationEffect::Gdatation(int size, std::shared_ptr<framework::Texture2D>& inTex,
		framework::GradationFilter::BlurType type, float shift)
	{
		float offset = 16;

		framework::GradationFilter gFilter1(inTex, size);
		gFilter1.setBlurType(type);
		gFilter1.setOffset(offset);


		gFilter1.setShift(shift);

		gFilter1.write();

		auto texSize = inTex->getSize();
		return std::make_shared<framework::Texture2D>(texSize.x, texSize.y, gFilter1.getShaderView());
	}

}