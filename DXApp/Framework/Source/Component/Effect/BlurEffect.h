#pragma once
#include"PostEffectComponent.h"
#include<Source\Device\Render\Renderer\2D\Single\PostEffectRenderer.h>

namespace component {
	class GUIComponent;

	class BlurEffect : public PostEffectComponent
	{
	public:
		BlurEffect();
		~BlurEffect();

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;


		void init()override;

		/**
		* @brief		ポストエフェクトで必要なテクスチャを選択できる
		* @param texContainer	テクスチャコンテナ
		*/
		virtual void selectTexture(framework::TextureContainer& texContainer)override;

		// PostEffectComponent を介して継承されました
		virtual void effectDraw(util::Transform * trans) override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

	private:
		struct Blur
		{
			XMMATRIX world;
			util::Vec2 pixSize;
			float speed;
			float damy;
		};

		std::string m_LuaPath;

		float m_Speed;

		std::weak_ptr<GUIComponent> m_pUI;

		std::unique_ptr<framework::PostEffectRenderer<Blur>> m_pRenderer;



	};



}