#include "DOFEffect.h"
#include<Source\Application\Screen\Screen.h>
#include<Source\Util\Lua\LuaBase.h>
#include<Source\Component\UI\GUIComponent.h>
#include<Source\Device\Render\Shader\GradationFilter.h>

namespace component {

	DOFEffect::DOFEffect()
	{
	}

	DOFEffect::~DOFEffect()
	{
	}

	void DOFEffect::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer,{ m_LuaPath });

		(*componentInitalizer).emplace_back(m_pUI);
	}

	void DOFEffect::init()
	{
		m_pRenderer = std::make_unique<framework::PostEffectRenderer<DOFParam>>("DOF");

		m_Center = m_pUI.lock()->getSlider("CenterDOF");
		m_Horizontal = m_pUI.lock()->getSlider("HorizontalDOF");
		m_Vertical = m_pUI.lock()->getSlider("VerticalDOF");

		DOFParam param;
		param.near_ = 1;
		param.far_ = framework::Screen::FAR_;
		param.center = m_Center;
		m_pRenderer->setBuffer(param);


		m_pRenderer->setSize(util::Vec2(framework::Screen::WINDOW_WIDTH, framework::Screen::WINDOW_HEIGHT));
	}

	void DOFEffect::setParam(const std::vector<std::string>& param)
	{
		m_LuaPath = param[0];
	}

	void DOFEffect::selectTexture(framework::TextureContainer & texContainer)
	{

#ifdef _MDEBUG
		m_Center = m_pUI.lock()->getSlider("CenterDOF");

		DOFParam param;
		param.near_ = 1;
		param.far_ = framework::Screen::FAR_;
		param.center = m_Center;
		m_pRenderer->setBuffer(param);

#endif

		//被射界深度用にメインテクスチャをボカス
		framework::GradationFilter gFilter1(texContainer["Main"], 2);
#ifdef _MDEBUG
		m_Vertical = m_pUI.lock()->getSlider("VerticalDOF");
#endif
		gFilter1.setShift(m_Vertical);

		gFilter1.setBlurType(framework::GradationFilter::BlurType::Vertical);
		gFilter1.write();

		framework::GradationFilter gFilter2(std::make_shared<framework::Texture2D>(framework::Screen::PIXEL_WIDTH,
			framework::Screen::PIXEL_HEIGHT, gFilter1.getShaderView()), 2);
#ifdef _MDEBUG
		m_Horizontal = m_pUI.lock()->getSlider("HorizontalDOF");
#endif
		gFilter2.setShift(m_Horizontal);

		gFilter2.setBlurType(framework::GradationFilter::BlurType::Horizontal);
		gFilter2.write();

		util::setSingleViewPort(framework::Screen::PIXEL_WIDTH, framework::Screen::PIXEL_HEIGHT);
		std::vector<std::shared_ptr<framework::Texture2D>> textures;
		textures.emplace_back(texContainer["Main"]);
		textures.emplace_back(std::make_shared<framework::Texture2D>(framework::Screen::PIXEL_WIDTH,
			framework::Screen::PIXEL_HEIGHT, gFilter2.getShaderView()));
		textures.emplace_back(texContainer["ShadowDepth"]);


		m_pRenderer->setTexture(textures);
	}

	void DOFEffect::effectDraw(util::Transform * trans)
	{
		m_pRenderer->draw(trans);
	}

}