#pragma once
#include"PostEffectComponent.h"
#include<Source\Device\Render\Renderer\2D\Single\PostEffectRenderer.h>
#include<Source\Device\Render\Shader\GradationFilter.h>

namespace component {

	class GUIComponent;

	class BloomEffect : public PostEffectComponent
	{
	public:
		BloomEffect();
		~BloomEffect();

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;


		void init()override;

		/**
		* @brief		ポストエフェクトで必要なテクスチャを選択できる
		* @param texContainer	テクスチャコンテナ
		*/
		virtual void selectTexture(framework::TextureContainer& texContainer)override;

		// PostEffectComponent を介して継承されました
		virtual void effectDraw(util::Transform * trans) override;


		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param)override;

	private:

		std::shared_ptr<framework::Texture2D> Gdatation(int size,std::shared_ptr<framework::Texture2D>& inTex, framework::GradationFilter::BlurType type, float shift);

	private:
		struct BloomParam
		{
			XMMATRIX world;
			util::Vec2 pixSize;
			float bright;
			float damy;
		};

		
		std::unique_ptr<framework::PostEffectRenderer<BloomParam>> m_pRenderer;

		std::string m_LuaPath;

		//!横ぼかし
		float m_Horizontal;

		//!縦ぼかし
		float m_Vertical;

		std::weak_ptr<GUIComponent> m_pUI;

		//std::shared_ptr<framework::Texture2D> m_pTex;
	};

}