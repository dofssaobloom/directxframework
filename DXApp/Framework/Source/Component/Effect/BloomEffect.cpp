#include"BloomEffect.h"
#include<Source\Application\Screen\Screen.h>
#include<Source\Util\Lua\LuaBase.h>
#include<Source\Component\UI\GUIComponent.h>


namespace component {

	BloomEffect::BloomEffect()
	{
	}

	BloomEffect::~BloomEffect()
	{
	}

	void BloomEffect::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer,{ m_LuaPath });

		(*componentInitalizer).emplace_back(m_pUI);
	}

	void BloomEffect::init()
	{
		m_pRenderer = std::make_unique<framework::PostEffectRenderer<BloomParam>>("Bloom");

		BloomParam d;
		d.pixSize.x = 1.0f / framework::Screen::PIXEL_WIDTH;
		d.pixSize.y = 1.0f / framework::Screen::PIXEL_HEIGHT;
		d.bright = m_pUI.lock()->getSlider("Bright");
		m_pRenderer->setBuffer(d);
		m_pRenderer->setSize(util::Vec2(framework::Screen::WINDOW_WIDTH, framework::Screen::WINDOW_HEIGHT));


		m_Horizontal = m_pUI.lock()->getSlider("HorizontalBloom");

		m_Vertical = m_pUI.lock()->getSlider("VerticalBloom");
		
		//m_pTex = std::make_shared<framework::Texture2D>(framework::Screen::WINDOW_WIDTH, framework::Screen::WINDOW_HEIGHT);
	}

	void BloomEffect::selectTexture(framework::TextureContainer & texContainer)
	{

#ifdef _MDEBUG
		m_Vertical = m_pUI.lock()->getSlider("VerticalBloom");
#endif
		auto tex0 = Gdatation(2, texContainer["Specular"], framework::GradationFilter::BlurType::Horizontal, m_Horizontal);

#ifdef _MDEBUG
		m_Horizontal = m_pUI.lock()->getSlider("HorizontalBloom");
#endif
		auto tex1 = Gdatation(1, tex0, framework::GradationFilter::BlurType::Vertical, m_Vertical);


		util::setSingleViewPort(framework::Screen::PIXEL_WIDTH, framework::Screen::PIXEL_HEIGHT);
		std::vector<std::shared_ptr<framework::Texture2D>> textures;
		textures.emplace_back(texContainer["Main"]);
		textures.emplace_back(tex1);


		m_pRenderer->setTexture(textures);
	}

	void BloomEffect::effectDraw(util::Transform * trans)
	{
#ifdef _MDEBUG
		BloomParam d;
		d.bright = m_pUI.lock()->getSlider("Bright");
		d.pixSize.x = 1.0f / framework::Screen::PIXEL_WIDTH;
		d.pixSize.y = 1.0f / framework::Screen::PIXEL_HEIGHT;
		m_pRenderer->setBuffer(d);
#endif

		m_pRenderer->draw(trans);
	}

	void BloomEffect::setParam(const std::vector<std::string>& param)
	{
		m_LuaPath = param[0];
	}

	std::shared_ptr<framework::Texture2D> BloomEffect::Gdatation(int size, std::shared_ptr<framework::Texture2D>& inTex,
		framework::GradationFilter::BlurType type,float shift)
	{
		float offset = 16;

		framework::GradationFilter gFilter1(inTex, size);
		gFilter1.setBlurType(type);
		gFilter1.setOffset(offset);


		gFilter1.setShift(shift);

		gFilter1.write();

		auto texSize = inTex->getSize();
		return std::make_shared<framework::Texture2D>(texSize.x, texSize.y, gFilter1.getShaderView());
	}

}