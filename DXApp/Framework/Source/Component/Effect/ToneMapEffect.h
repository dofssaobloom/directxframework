#pragma once
#include<Source\Component\Effect\PostEffectComponent.h>

namespace component {
	class GUIComponent;



	class ToneMapEffect : public PostEffectComponent
	{
	public:
		ToneMapEffect();
		~ToneMapEffect();

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;


		void init()override;

		/**
		* @brief		ポストエフェクトで必要なテクスチャを選択できる
		* @param texContainer	テクスチャコンテナ
		*/
		virtual void selectTexture(framework::TextureContainer& texContainer)override;

		// PostEffectComponent を介して継承されました
		virtual void effectDraw(util::Transform * trans) override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param)override;

	private:

		struct HDRParam
		{
			XMMATRIX world;
			float exposure;
			util::Vec3 damy;
		};

		std::unique_ptr<framework::PostEffectRenderer<HDRParam>> m_pRenderer;

		std::string m_LuaPath;
		std::weak_ptr<GUIComponent> m_pUI;

	};


}