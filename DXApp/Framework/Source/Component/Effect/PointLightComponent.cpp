#include"PointLightComponent.h"
#include<Source\Entity\Entity.h>
#include<Source\Util\WrapFunc.h>
#include<Source\Component\UI\GUIComponent.h>
#include<Source\Util\Win\WinFunc.h>
#include<typeinfo>

namespace component {

	LightList PointLightComponent::m_PointLightList;

	std::shared_ptr<PointLightElement*> PointLightComponent::m_Result = nullptr;

	PointLightComponent::PointLightComponent()
	{
	}

	PointLightComponent::~PointLightComponent()
	{
	}

	void PointLightComponent::init()
	{
		m_LightAttribute.position = m_Entity.lock()->getTransform()->m_Position;

		auto guis = m_Entity.lock()->getComponents<GUIComponent>();

		auto find = std::find_if(guis.begin(), guis.end(), [](std::weak_ptr<GUIComponent>& gui) {
			return gui.lock()->getLuaName() == "PointLightUI";
		});

		m_pUI = *find;
		if (m_pUI.expired()) {
#ifdef _MDEBUG
			util::WinFunc::drawMessageBox("GetComponent失敗", typeid(GUIComponent).name());
#endif
			return;
		}
		m_Bright = m_pUI.lock()->getSliderPointer("Bright");
		m_Power = m_pUI.lock()->getSliderPointer("Power");
		m_Near_ = m_pUI.lock()->getSliderPointer("Near");
		m_Far_ = m_pUI.lock()->getSliderPointer("Far");
		m_LightAttribute.bright = *m_Bright;
		m_LightAttribute.power = *m_Power;
		m_LightAttribute.near_ = *m_Near_;
		m_LightAttribute.far_ = *m_Far_;

		//m_LightAttribute.power = m_pUI.lock()->getSlider("Power");
		//m_LightAttribute.near_ = m_pUI.lock()->getSlider("Near");
		//m_LightAttribute.far_ = m_pUI.lock()->getSlider("Far");

		m_LightAttribute.offset.x = m_pUI.lock()->getSlider("LightOffsetX");
		m_LightAttribute.offset.y = m_pUI.lock()->getSlider("LightOffsetY");
		m_LightAttribute.offset.z = m_pUI.lock()->getSlider("LightOffsetZ");

		m_Color = m_pUI.lock()->getColorPointer("LightColor");
		m_LightAttribute.color = util::Vec3(m_Color->x, m_Color->y, m_Color->z);
	}

	void PointLightComponent::active()
	{
		UpdateComponent::active();
		auto key = (int)(m_Entity._Get());
		//キーがすでに登録されていれば処理しない
		if (m_PointLightList.find(key) != m_PointLightList.end())return;
		m_PointLightList[key] = &m_LightAttribute;

		//TODO 初期化時に負担が大きいので場合によっては方法を変える必要がある
		//アクティブにより配列のサイズが変わる
		m_Result = std::shared_ptr<PointLightElement*>(new PointLightElement*[m_PointLightList.size()], std::default_delete<PointLightElement*[]>());
	}

	void PointLightComponent::deActive()
	{
		UpdateComponent::deActive();
		auto itr = m_PointLightList.find((int)(m_Entity._Get()));
		if (itr == m_PointLightList.end())return;
		m_PointLightList.erase(itr);


		//ディアクティブにより配列のサイズが変わる
		m_Result = std::shared_ptr<PointLightElement*>(new PointLightElement*[m_PointLightList.size()], std::default_delete<PointLightElement*[]>());
	}

	const int PointLightComponent::getTotalLightNum()
	{
		return m_PointLightList.size();
	}

	std::shared_ptr<PointLightElement*> PointLightComponent::getLightArray()
	{
		int i = 0;
		for (auto& element : m_PointLightList) {
			m_Result.get()[i] = element.second;
			i++;
		}
		return m_Result;
	}

	void PointLightComponent::update()
	{
		//ポジションは毎フレーム更新する
		m_LightAttribute.position = m_Entity.lock()->getTransform()->m_Position;

#ifdef _MDEBUG
		/*	m_LightAttribute.power = m_pUI.lock()->getSlider("Power");
			m_LightAttribute.near_ = m_pUI.lock()->getSlider("Near");
			m_LightAttribute.far_ = m_pUI.lock()->getSlider("Far");*/
		m_LightAttribute.bright = *m_Bright;
		m_LightAttribute.power = *m_Power;
		m_LightAttribute.near_ = *m_Near_;
		m_LightAttribute.far_ = *m_Far_;

		m_LightAttribute.offset.x = m_pUI.lock()->getSlider("LightOffsetX");
		m_LightAttribute.offset.y = m_pUI.lock()->getSlider("LightOffsetY");
		m_LightAttribute.offset.z = m_pUI.lock()->getSlider("LightOffsetZ");


		m_LightAttribute.color = util::Vec3(m_Color->x, m_Color->y, m_Color->z);
#endif
	}

	void PointLightComponent::clear()
	{
		//if (m_PointLightList.empty())return;
		//m_PointLightList.clear();
	}

	void PointLightComponent::setColor(const util::Vec4 & color)
	{
		*m_Color = color;
		m_LightAttribute.color = util::Vec3(m_Color->x, m_Color->y, m_Color->z);
	}

	void PointLightComponent::setAtten(const util::Vec3 & atten)
	{
		m_LightAttribute.power = *m_Power = atten.x;
		m_LightAttribute.near_ = *m_Near_ = atten.y;
		m_LightAttribute.far_ = *m_Far_ = atten.z;
	}

	//0 power
	//1 near
	//2 far
	//3 ~ 5 color
	void PointLightComponent::setParam(const std::vector<std::string>& param)
	{
		//m_LightAttribute.position = m_Entity.lock()->getTransform()->m_Position;
		//m_LightAttribute.power = std::atof(param[0].c_str());
		//m_LightAttribute.near_ = std::atof(param[1].c_str());
		//m_LightAttribute.far_ = std::atof(param[2].c_str());
		//m_LightAttribute.color = util::atof<util::Vec3>(param,3).reault;
	}

	//void PointLightComponent::setPower(const util::Vec3& power)
	//{
	//	m_LightAttribute.power = power.x;
	//	m_LightAttribute.near_ = power.y;
	//	m_LightAttribute.far_  = power.z;
	//}

	PointLightElement*  PointLightComponent::getAttrib()
	{
		return &m_LightAttribute;
	}


}