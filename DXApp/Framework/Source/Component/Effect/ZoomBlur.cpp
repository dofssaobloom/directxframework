#include"ZoomBlur.h"
#include<Source\Util\Lua\LuaBase.h>
#include<Source\Util\Timer\Timer.h>
#include<Source\Util\Math\Math.h>
UsingNamespace;

namespace component {

	ZoomBlur::ZoomBlur()
	{
	}

	ZoomBlur::~ZoomBlur()
	{
	}

	void ZoomBlur::init()
	{
		m_pRenderer = std::make_unique<framework::PostEffectRenderer<ZoomBlurParam>>("ZoomBlur");
		m_pRenderer->setSize(util::Vec2(framework::Screen::WINDOW_WIDTH, framework::Screen::WINDOW_HEIGHT));

		ZoomBlurParam param;

		param.shift = 0;
		m_pRenderer->setBuffer(param);

		m_IsStart = false;

		m_Projectin = CameraComponent::getMainCamera().lock()->getProjection();
		m_Length = 0;
	}

	void ZoomBlur::selectTexture(framework::TextureContainer & texContainer)
	{

		std::vector<std::shared_ptr<framework::Texture2D>> textures;
		textures.emplace_back(texContainer["Main"]);

		m_pRenderer->setTexture(textures);
	}

	void ZoomBlur::effectDraw(util::Transform * trans)
	{
		blurUpdate();

		m_pRenderer->draw(trans);
	}

	void ZoomBlur::onBlurStart(util::Transform* centerPos)
	{
		util::Vec4 pos(0, 0, 0, 1);
		auto viewMat = CameraComponent::getMainCamera().lock()->toViewMatrix().transpose().toXMMatrix();


		auto world = XMVector4Transform(pos.toXMVector(), centerPos->toMatrix().transpose().toXMMatrix());
		auto viewPos = XMVector4Transform(world, viewMat);
		util::Vec4 projePos = XMVector4Transform(viewPos, m_Projectin.toXMMatrix());

		projePos.x /= projePos.w;
		projePos.y /= projePos.w;
		projePos.z /= projePos.w;


		projePos.x = (projePos.x * 0.5) + 0.5;
		projePos.y = (projePos.y * 0.5) + 0.5;

		//util::Mat4 wvp = m_Projectin.toXMMatrix() * view * world.toXMMatrix();

		//world = XMVector3Transform(world.toXMVector(), wvp.toXMMatrix());

		m_Param.cetner = util::Vec2(1.0f - projePos.x, projePos.y);

		m_Length = calucLenth(centerPos);

		m_pTimer->init();
		m_IsStart = true;
		active();
	}

	void ZoomBlur::setParam(const std::vector<std::string>& param)
	{
		util::LuaBase lua(param[0]);
		m_MaxShift = lua.getParam<float>("MaxShift");

		auto time = lua.getParam<float>("BlurTime");

		m_pTimer = std::make_unique<util::Timer>(time);
	}

	void ZoomBlur::blurUpdate()
	{
		if (!m_IsStart)return;

		m_pTimer->update();

		//遠いほどずらしを弱くする
		m_Param.shift = util::bezierCurve(m_pTimer->rate(), 0.0f, m_MaxShift / 3, m_MaxShift, 0.0f) * m_Length;


		if (m_pTimer->isEnd()) {
			m_pTimer->init();
			m_Param.cetner = util::Vec2();
			m_Param.shift = 0;
			m_IsStart = false;
			m_Length = 0;
			deActive();
		}
		m_pRenderer->setBuffer(m_Param);

	}

	float ZoomBlur::calucLenth(util::Transform * otherTrans)
	{
		auto&& vec = m_Entity.lock()->getTransform()->m_Position - otherTrans->m_Position;
		auto length = vec.length();

		const int maxLength = 2000;
		length = min(length, maxLength);
		auto result = length / maxLength;
		result = 1.0f - result;

		return result;
	}

}