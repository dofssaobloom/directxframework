#pragma once

#include<Framework.h>
#include"PostEffectComponent.h"
#include<Source\Device\Render\Renderer\2D\Single\PostEffectRenderer.h>

namespace component {

	class FXAA : public PostEffectComponent
	{
	public:
		FXAA();
		~FXAA();

		void init()override;

		/**
		* @brief		ポストエフェクトで必要なテクスチャを選択できる
		* @param texContainer	テクスチャコンテナ
		*/
		virtual void selectTexture(framework::TextureContainer& texContainer)override;

		// PostEffectComponent を介して継承されました
		virtual void effectDraw(util::Transform * trans) override;

	private:
		struct FXAAParam
		{
			XMMATRIX world;
			float lumaThreshold;
			util::Vec3 damy;
		};

		std::unique_ptr<framework::PostEffectRenderer<FXAAParam>> m_pRenderer;
	};


}
