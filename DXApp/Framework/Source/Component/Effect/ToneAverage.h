#pragma once
#include"PostEffectComponent.h"
#include<Source\Device\Render\Renderer\2D\Single\PostEffectRenderer.h>

namespace component {

	class ToneAverage : public PostEffectComponent
	{
	public:
		ToneAverage();
		~ToneAverage();


		void init()override;

		/**
		* @brief		ポストエフェクトで必要なテクスチャを選択できる
		* @param texContainer	テクスチャコンテナ
		*/
		virtual void selectTexture(framework::TextureContainer& texContainer)override;

		// PostEffectComponent を介して継承されました
		virtual void effectDraw(util::Transform * trans) override;

	private:

		struct ToneAverageParam
		{
			XMMATRIX world;
		};

		std::unique_ptr<framework::PostEffectRenderer<ToneAverageParam>> m_pRenderer;

	};

}