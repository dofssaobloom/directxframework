#include "Framework.h"
#include "ToneMapEffect.h"
#include<Source\Device\Render\Shader\GradationFilter.h>
#include<Source\Device\Render\Shader\ReducationBufffer.h>
#include<Source\Component\UI\GUIComponent.h>


namespace component {

	ToneMapEffect::ToneMapEffect()
	{
	}

	ToneMapEffect::~ToneMapEffect()
	{
	}

	void ToneMapEffect::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath });

		(*componentInitalizer).emplace_back(m_pUI);
	}

	void ToneMapEffect::init()
	{
		m_pRenderer = std::make_unique<framework::PostEffectRenderer<HDRParam>>("ToneMap");

		HDRParam param;
		param.exposure = m_pUI.lock()->getSlider("Tone");
		m_pRenderer->setBuffer(param);
		m_pRenderer->setSize(util::Vec2(framework::Screen::WINDOW_WIDTH, framework::Screen::WINDOW_HEIGHT));

	}

	void ToneMapEffect::selectTexture(framework::TextureContainer & texContainer)
	{
		std::vector<std::shared_ptr<framework::Texture2D>> textures;

		//framework::ReducationBuffer reducation(9,9, texContainer["Main"]);
		//reducation.write(1);

		textures.emplace_back(texContainer["Main"]);

		//framework::GradationFilter gFilter1(texContainer["Main"],1);
		//gFilter1.setOffset(0);

		//textures.emplace_back(std::make_shared<framework::Texture2D>(9, 9, gFilter1.getShaderView()));

		//m_pRenderer->setTexture(textures);
	}


	void ToneMapEffect::setParam(const std::vector<std::string>& param)
	{
		m_LuaPath = param[0];
	}


	void ToneMapEffect::effectDraw(util::Transform * trans)
	{


#ifdef _MDEBUG

		HDRParam param;
		param.exposure = m_pUI.lock()->getSlider("Tone");
		m_pRenderer->setBuffer(param);

#endif // _MDEBUG


		m_pRenderer->draw(trans);
	}
}
