#include"DirectionalLightComponent.h"
#include<list>
#include<algorithm>
#include<Source\Entity\Entity.h>
#include<Source\Util\WrapFunc.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Component\UI\GUIComponent.h>

namespace component {

	std::weak_ptr<DirectionalLightComponent> DirectionalLightComponent::m_DerectionalLight;

	DirectionalLightComponent::DirectionalLightComponent()
	{
		m_Color = util::Vec3(1,1,1);
	}

	DirectionalLightComponent::~DirectionalLightComponent()
	{
	}

	void DirectionalLightComponent::init()
	{	
		m_pUI = m_Entity.lock()->getComponent<GUIComponent>();
		if (m_pUI.expired()) {
#ifdef _MDEBUG
			util::WinFunc::drawMessageBox("GetComponent失敗", typeid(GUIComponent).name());
#endif
			return;
		}

		auto color = m_pUI.lock()->getColor("LightColor");
		m_Color = util::Vec3(color.x, color.y, color.z);
		auto ambient = m_pUI.lock()->getColor("AmbientColor");
		m_Ambient = util::Vec3(ambient.x, ambient.y, ambient.z);

	}

	void DirectionalLightComponent::active()
	{
		haveDirectionalLightEntityLoop([&](framework::WeakEntity entity) {
			auto componentList = entity.lock()->getComponents<DirectionalLightComponent>();
			for (auto& light : componentList) {
				light.lock()->deActive();
			}
		});
		framework::UpdateComponent::active();

		m_DerectionalLight = m_Entity.lock()->getComponent<DirectionalLightComponent>(this);
	}

	void DirectionalLightComponent::deActive()
	{
		m_DerectionalLight.reset();
		framework::UpdateComponent::deActive();
	}

	void DirectionalLightComponent::update()
	{
#ifdef _MDEBUG
		auto color = m_pUI.lock()->getColor("LightColor");
		m_Color = util::Vec3(color.x,color.y, color.z);
		auto ambient = m_pUI.lock()->getColor("AmbientColor");
		m_Ambient = util::Vec3(ambient.x, ambient.y, ambient.z);
#endif

		m_pCamera.lock()->update();//シャドウマップ用もカメラは常にアップデートする
	}

	void DirectionalLightComponent::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		framework::Entity::createEntity("ShadowCameraLook", "Look", util::Transform());
		
		auto camera = CameraComponent::getMainCamera();

		m_pCamera = m_Entity.lock()->addComponent<component::CameraComponent>(componentInitalizer, { "ShadowCameraLook" ,"1"});
		m_pCamera.lock()->setGameObj(m_Entity);		

		//シャドウ用のカメラが追加されてメインカメラが切り替わってしまうのでココでもとに戻す
		if (!camera.expired()) {
			camera.lock()->active();
		}

		componentInitalizer->emplace_back(m_pCamera);
	}

	util::Vec3 DirectionalLightComponent::getLightPosition()
	{
		//アクティブなライトコンポーネントがなければ真上に設定
		if (m_DerectionalLight.expired()) return util::Vec3(0,1,0);
		return m_DerectionalLight.lock()->getGameObj().lock()->getTransform()->m_Position;
	}

	util::Vec3 DirectionalLightComponent::getLightColor()
	{
		if (m_DerectionalLight.expired()) return util::Vec3(0, 0, 0);
		return m_DerectionalLight.lock()->getColor();
	}

	util::Vec3 DirectionalLightComponent::getLightAmbient()
	{
		if (m_DerectionalLight.expired()) return util::Vec3(0, 0, 0);
		return m_DerectionalLight.lock()->getAmbient();
	}

	std::weak_ptr<CameraComponent> DirectionalLightComponent::getCamera()
	{
		//アクティブなライトが無ければ空のカメラを渡す
		if (m_DerectionalLight.expired())return std::weak_ptr<CameraComponent>();
		return m_DerectionalLight.lock()->m_pCamera;
	}

	//util::Transform * DirectionalLightComponent::getTransform()
	//{
	//	return m_DerectionalLight.lock()->getGameObj().lock()->getTransform();
	//}

	void DirectionalLightComponent::setParam(const std::vector<std::string>& param)
	{
		//rgb成分がパラメータとしてなければ何もしない
		if (param.size() < 3) return;

		m_Color = util::atof<util::Vec3>(param).reault;
	}


	void DirectionalLightComponent::haveDirectionalLightEntityLoop(std::function<void(framework::WeakEntity)> entityAction)
	{
		haveComponentEntityLoop<DirectionalLightComponent>(entityAction);
	}

	util::Vec3 DirectionalLightComponent::getColor()
	{
		return m_Color;
	}

	util::Vec3 DirectionalLightComponent::getAmbient()
	{
		return m_Ambient;
	}

}