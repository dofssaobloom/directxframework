#pragma once
#include"PostEffectComponent.h"
#include<Source\Device\Render\Renderer\2D\Single\PostEffectRenderer.h>

namespace component {

	class GradationEffect : public PostEffectComponent
	{
	public:
		GradationEffect();
		~GradationEffect();

		void init()override;

		/**
		* @brief		ポストエフェクトで必要なテクスチャを選択できる
		* @param texContainer	テクスチャコンテナ
		*/
		virtual void selectTexture(framework::TextureContainer& texContainer)override;

		// PostEffectComponent を介して継承されました
		virtual void effectDraw(util::Transform * trans) override;


		void setShift(const util::Vec2& shift);


	private: 
		std::shared_ptr<framework::Texture2D> Gdatation(int size, std::shared_ptr<framework::Texture2D>& inTex, framework::GradationFilter::BlurType type, float shift);

	private:
		struct GradationParam
		{
			XMMATRIX world;
			float alpha;
			util::Vec3 damy;
			
			util::Vec4 posisionOffset[2];
		};

		//!横ぼかし
		float m_Horizontal;

		//!縦ぼかし
		float m_Vertical;

		std::unique_ptr<framework::PostEffectRenderer<GradationParam>> m_pRenderer;
	};
}