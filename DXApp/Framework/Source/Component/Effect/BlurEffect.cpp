#include"BlurEffect.h"
#include<Source\Device\Render\Shader\GradationFilter.h>
#include<Source\Util\Lua\LuaBase.h>
#include<Framework.h>
#include<Source\Component\UI\GUIComponent.h>

namespace component {

	BlurEffect::BlurEffect()
	{
	}

	BlurEffect::~BlurEffect()
	{
	}

	void BlurEffect::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{	
			m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath });

			(*componentInitalizer).emplace_back(m_pUI);
	}

	void BlurEffect::init()
	{
		m_pRenderer = std::make_unique<framework::PostEffectRenderer<Blur>>("Blur");

		m_Speed = m_pUI.lock()->getSlider("MotionBluerSpeed");

		Blur d;
		d.pixSize.x = 1.0f / framework::Screen::PIXEL_WIDTH;
		d.pixSize.y = 1.0f / framework::Screen::PIXEL_HEIGHT;
		d.speed = m_Speed;
		m_pRenderer->setBuffer(d);
		m_pRenderer->setSize(util::Vec2(framework::Screen::WINDOW_WIDTH, framework::Screen::WINDOW_HEIGHT));
	}

	void BlurEffect::selectTexture(framework::TextureContainer & texContainer)
	{
		util::setSingleViewPort(framework::Screen::PIXEL_WIDTH, framework::Screen::PIXEL_HEIGHT);
		std::vector<std::shared_ptr<framework::Texture2D>> textures;
		textures.emplace_back(texContainer["Main"]);
		textures.emplace_back(texContainer["Velocity"]);

		m_pRenderer->setTexture(textures);
	}

	void BlurEffect::effectDraw(util::Transform * trans)
	{
#ifdef _MDEBUG
		m_Speed = m_pUI.lock()->getSlider("MotionBluerSpeed");
		Blur d;
		d.pixSize.x = 1.0f / framework::Screen::PIXEL_WIDTH;
		d.pixSize.y = 1.0f / framework::Screen::PIXEL_HEIGHT;
		d.speed = m_Speed;
		m_pRenderer->setBuffer(d);
#endif

		m_pRenderer->draw(trans);
	}

	void BlurEffect::setParam(const std::vector<std::string>& param)
	{

		m_LuaPath = param[0];

	}

}