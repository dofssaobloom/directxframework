#pragma once
#include<Source\Component\Component.h>
#include<string>
#include<unordered_map>
#include<Source\Resource\Texture\RenderTarget.h>
#include<D3DX11.h>
#include<Source\Util\Type.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Entity\Entity.h>

namespace framework {
	//PostEffectPass.hでも定義している
	using TextureContainer = std::unordered_map<std::string, std::shared_ptr<Texture2D>>;
}

namespace component {
 
	class PostEffectComponent : public framework::Component
	{
	public:
		PostEffectComponent() {};
		virtual ~PostEffectComponent() {};

		void init()override {
		
			m_pCamera = m_Entity.lock()->getComponent<CameraComponent>();
		}

		void active()override {
			if (m_isActive)return;
			Component::active();
			
			////自分を取得
			//auto& my = m_Entity.lock()->getComponent<PostEffectComponent>(this);
			//m_pCamera.lock()->addImageEffect(my);
		}

		void deActive()override {
			if (!m_isActive)return;
			Component::deActive();

			////自分を取得
			//auto& my = m_Entity.lock()->getComponent<PostEffectComponent>(this);
			//m_pCamera.lock()->removeImageEffect(my);
		}

		/**
		* @brief		ポストエフェクトで必要なテクスチャを選択できる
		* @param texContainer	テクスチャコンテナ
		*/
		virtual void selectTexture(framework::TextureContainer& texContainer) {};

		/**
		* @brief		ポストエフェクトをつける
		* @param mainTarget	作り上げてる描画結果
		*/
		void doEffect(const util::Vec2& pos) {
			util::Transform trans(util::Vec3(pos.x,pos.y,0),util::Vec3(0,0,0),util::Vec3(1,1,1));
		
			effectDraw(&trans);
		};

		int getCallOrder() {
			return m_CallOrder;
		}

	private:
		virtual void effectDraw(util::Transform* trans) = 0;

	private:
		std::weak_ptr<CameraComponent> m_pCamera;

	protected:
		//!低いほど先に描画される
		unsigned int m_CallOrder;
		//!シェーダービュー
		std::vector<ID3D11ShaderResourceView*> m_pShaderView;

	};

}