#include "Framework.h"
#include "FXAA.h"

namespace component {

	FXAA::FXAA()
	{
	}

	FXAA::~FXAA()
	{
	}

	void FXAA::init()
	{
		m_pRenderer = std::make_unique<framework::PostEffectRenderer<FXAAParam>>("FXAA");

		FXAAParam param;

		param.lumaThreshold = 0.329999983;

		m_pRenderer->setBuffer(param);

		m_pRenderer->setSize(util::Vec2(framework::Screen::WINDOW_WIDTH, framework::Screen::WINDOW_HEIGHT));
	}

	void FXAA::selectTexture(framework::TextureContainer & texContainer)
	{
		std::vector<std::shared_ptr<framework::Texture2D>> textures;
		textures.emplace_back(texContainer["Main"]);
		m_pRenderer->setTexture(textures);
	}

	void FXAA::effectDraw(util::Transform * trans)
	{
		//FXAAParam param;


		//static float a = 0.0f;

		//if (framework::Input::getKey().isKeyDown(framework::KeyBoard::KeyCords::Y)) {
		//	a+= 0.01f;
		//}

		//if (framework::Input::getKey().isKeyDown(framework::KeyBoard::KeyCords::H)) {
		//	a -= 0.01f;
		//}

		//param.lumaThreshold = a;

		//m_pRenderer->setBuffer(param);

		m_pRenderer->draw(trans);
	}

}
