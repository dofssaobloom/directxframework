#include "Framework.h"
#include "FogEffect.h"
#include<Source\Util\Lua\LuaBase.h>

namespace component {


	FogEffect::FogEffect()
	{
	}

	FogEffect::~FogEffect()
	{
	}


	void FogEffect::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath });

		(*componentInitalizer).emplace_back(m_pUI);
	}

	void FogEffect::init()
	{
		m_pRenderer = std::make_unique<framework::PostEffectRenderer<FogParam>>("Fog");

		m_Param.far_ = framework::Screen::FAR_;
		m_Param.near_ = 1;
		m_Param.start = m_pUI.lock()->getSlider("Start");
		m_Param.end = m_pUI.lock()->getSlider("End");
		util::Vec4 color;
		color = m_pUI.lock()->getColor("FogColor");
		m_Param.fogClor = util::Vec3(color.x, color.y, color.z);
		m_pRenderer->setBuffer(m_Param);
		m_pRenderer->setSize(util::Vec2(framework::Screen::WINDOW_WIDTH, framework::Screen::WINDOW_HEIGHT));

	}

	void FogEffect::selectTexture(framework::TextureContainer & texContainer)
	{
		util::setSingleViewPort(framework::Screen::PIXEL_WIDTH, framework::Screen::PIXEL_HEIGHT);
		std::vector<std::shared_ptr<framework::Texture2D>> textures;
		textures.emplace_back(texContainer["Main"]);
		textures.emplace_back(texContainer["ShadowDepth"]);

		m_pRenderer->setTexture(textures);

	}

	void FogEffect::effectDraw(util::Transform * trans)
	{

		/*FogParam p;

		static float s = 0.0f, e = 0.0f;

		if (framework::Input::getKey().isKeyDown(framework::KeyBoard::KeyCords::Y) && !framework::Input::getKey().isKeyDown(framework::KeyBoard::KeyCords::LSHIFT)) {
			s += 0.01f;
		}
		if (framework::Input::getKey().isKeyDown(framework::KeyBoard::KeyCords::H) && !framework::Input::getKey().isKeyDown(framework::KeyBoard::KeyCords::LSHIFT)) {
			s -= 0.01f;
		}

		if (framework::Input::getKey().isKeyDown(framework::KeyBoard::KeyCords::Y) && framework::Input::getKey().isKeyDown(framework::KeyBoard::KeyCords::LSHIFT)) {
			e += 0.01f;
		}
		if (framework::Input::getKey().isKeyDown(framework::KeyBoard::KeyCords::H) && framework::Input::getKey().isKeyDown(framework::KeyBoard::KeyCords::LSHIFT)) {
			e -= 0.01f;
		}

		p.fogClor = m_Param.fogClor;

		p.start = s;
		p.end = e;

		m_pRenderer->setBuffer(p);*/

#ifdef _MDEBUG
		m_Param.start = m_pUI.lock()->getSlider("Start");
		m_Param.end = m_pUI.lock()->getSlider("End");
		util::Vec4 color;
		color = m_pUI.lock()->getColor("FogColor");
		m_Param.fogClor = util::Vec3(color.x, color.y, color.z);
		m_pRenderer->setBuffer(m_Param);
#endif

		m_pRenderer->draw(trans);
	}

	void FogEffect::setParam(const std::vector<std::string>& param)
	{
		//util::LuaBase lua(param[0]);
		//m_Param.start = lua.getParam<float>("FogStart");
		//m_Param.end = lua.getParam<float>("FogEnd");
		//util::Vec3 color;
		//color.x = lua.getParam<float>("ColorR");
		//color.y = lua.getParam<float>("ColorG");
		//color.z = lua.getParam<float>("ColorB");
		//

		m_LuaPath = param[0];
	}

}
