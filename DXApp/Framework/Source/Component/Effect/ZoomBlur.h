#pragma once
#include"PostEffectComponent.h"
#include<Source\Device\Render\Renderer\2D\Single\PostEffectRenderer.h>
#include<Source\Device\Render\Shader\GradationFilter.h>
#include<memory>

namespace util {
	class Timer;
}

namespace component {

	class ZoomBlur : public PostEffectComponent
	{
	public:
		ZoomBlur();
		~ZoomBlur();

		void init()override;

		/**
		* @brief		ポストエフェクトで必要なテクスチャを選択できる
		* @param texContainer	テクスチャコンテナ
		*/
		virtual void selectTexture(framework::TextureContainer& texContainer)override;

		// PostEffectComponent を介して継承されました
		virtual void effectDraw(util::Transform * trans) override;

		/// <summary>
		/// ブラー開始
		/// </summary>
		void onBlurStart(util::Transform*  centerPos);

	private:

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);


		/// <summary>
		/// ブラー処理更新
		/// </summary>
		void blurUpdate();


		/// <summary>
		/// 距離計算
		/// </summary>
		/// <param name="otherTrans"></param>
		/// <returns></returns>
		float calucLenth(util::Transform* otherTrans);

	private:
		struct ZoomBlurParam
		{
			XMMATRIX world;
			util::Vec2 cetner;
			float shift;//ずらし値
			float damy;
		};

		//!最大のずらし値
		float m_MaxShift;

		ZoomBlurParam m_Param;

		std::unique_ptr<util::Timer> m_pTimer;

		bool m_IsStart;

		float m_Length;

		std::unique_ptr<framework::PostEffectRenderer<ZoomBlurParam>> m_pRenderer;

		util::Mat4 m_Projectin;

	};



}
