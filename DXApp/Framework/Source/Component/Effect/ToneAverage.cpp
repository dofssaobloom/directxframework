#include "Framework.h"
#include "ToneAverage.h"

namespace component {
	ToneAverage::ToneAverage()
	{
	}

	ToneAverage::~ToneAverage()
	{
	}

	void ToneAverage::init()
	{
		m_pRenderer = std::make_unique<framework::PostEffectRenderer<ToneAverageParam>>("ToneAverage");

		m_pRenderer->setSize(util::Vec2(framework::Screen::WINDOW_WIDTH, framework::Screen::WINDOW_HEIGHT));

	}

	void ToneAverage::selectTexture(framework::TextureContainer & texContainer)
	{

	}

	void ToneAverage::effectDraw(util::Transform * trans)
	{
		m_pRenderer->draw(trans);
	}

}
