#include "Framework.h"
#include "FPSCounter.h"
#include<Source\Component\UI\GUIComponent.h>

namespace component {

	FPSCounter::FPSCounter()
	{
	}

	FPSCounter::~FPSCounter()
	{
	}

	void FPSCounter::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath });

		(*componentInitalizer).emplace_back(m_pUI);
	}

	void FPSCounter::init()
	{
		m_Count = m_pUI.lock()->getTextPointer("Counter");
	}

	void FPSCounter::update()
	{
		*(m_Count) = std::to_string(framework::Application::getFPS());
	}

	void FPSCounter::setParam(const std::vector<std::string>& param)
	{
		m_LuaPath = param[0];
	}

}