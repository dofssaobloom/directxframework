#pragma once
#include<Framework.h>
#include<vector>

namespace component {

	class SpriteRenderComponent;

	class Counter : public framework::Component
	{
	public:
		Counter();
		~Counter();
	
		/**
		* @brief		アクティブにする
		*/
		virtual void active() override;

		/**
		* @brief		ディアクティブにする
		*/
		virtual void deActive() override;

		virtual void setParam(const std::vector<std::string>& param);
		void init()override;
		void setAlpha(float alpha);
		void changeNum(int num);
		void setSort(short sort);
	private:
		std::weak_ptr<SpriteRenderComponent> addSprite(const std::string& num);

		//!キル数
		int m_Count;

		std::weak_ptr<SpriteRenderComponent> m_pCurrentRenderer;
		std::vector < std::weak_ptr<SpriteRenderComponent>> m_Renderers;
	};

}