#include "Framework.h"
#include "Counter.h"

namespace component {


	Counter::Counter()
	{
	}

	Counter::~Counter()
	{
	}

	void Counter::active()
	{
	}

	void Counter::deActive()
	{
	}

	void Counter::setParam(const std::vector<std::string>& param)
	{
		m_Renderers.emplace_back(addSprite(param[0].c_str()));
		m_Renderers.emplace_back(addSprite(param[1].c_str()));
		m_Renderers.emplace_back(addSprite(param[2].c_str()));
		m_Renderers.emplace_back(addSprite(param[3].c_str()));
		m_Renderers.emplace_back(addSprite(param[4].c_str()));
		m_Renderers.emplace_back(addSprite(param[5].c_str()));
		m_Renderers.emplace_back(addSprite(param[6].c_str()));
		m_Renderers.emplace_back(addSprite(param[7].c_str()));
		m_Renderers.emplace_back(addSprite(param[8].c_str()));
		m_Renderers.emplace_back(addSprite(param[9].c_str()));
	}

	void Counter::init()
	{
		//fontの9まで入れる
		/*
		for (size_t i = 0; i < 10; i++)
		{
			m_Renderers.emplace_back(addSprite(std::to_string(i)));
		}
		*/

		//0のみ登録
		m_pCurrentRenderer = m_Renderers[0];

		//全部アクティブにすると全部描画されるため0のみアクティブ
		m_pCurrentRenderer.lock()->active();
	}

	void Counter::setAlpha(float alpha)
	{
		m_pCurrentRenderer.lock()->setAlpha(alpha);
	}

	void Counter::changeNum(int num)
	{
		m_pCurrentRenderer.lock()->deActive();

		num = util::clamp(num, 0, 9);

		if (num >= 0 || num <= 9) {
			//一個づつアクティブにする
			m_pCurrentRenderer = m_Renderers[num];
		}

		m_pCurrentRenderer.lock()->active();
	}

	void Counter::setSort(short sort) {
		for (auto& r : m_Renderers) {
			r.lock()->setSort(sort);
		}
	}

	std::weak_ptr<SpriteRenderComponent> Counter::addSprite(const std::string & num)
	{
		std::vector<std::weak_ptr<Component>> damy;
		auto sprite = m_Entity.lock()->addComponent<SpriteRenderComponent>(&damy,{num,"Resource/Script/Lua/UI/SpriteRenderer.lua"});
		sprite.lock()->init();
		sprite.lock()->deActive();
		return sprite;
	}

}