#pragma once
#include<Framework.h>

namespace component {
	class GUIComponent;

	class FPSCounter : public framework::UpdateComponent
	{
	public:
		FPSCounter();
		~FPSCounter();

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		/**
		* @brief		初期化
		*/
		virtual void init()override;

		/**
		* @brief		更新
		*/
		virtual void update()override;

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

	private:
		std::weak_ptr<GUIComponent> m_pUI;
		std::string* m_Count;
		std::string m_LuaPath;
	};
}