#include"BoxRenderClientComponent.h"
#include<Source\Util\WrapFunc.h>

namespace component {

	BoxRenderClientComponent::BoxRenderClientComponent()
	{
	}

	BoxRenderClientComponent::~BoxRenderClientComponent()
	{
	}

	void BoxRenderClientComponent::init()
	{
		m_Transform.m_Rotation = m_Entity.lock()->getTransform()->m_Rotation;
		m_Transform.m_Scale = m_Scale;
	}

	void BoxRenderClientComponent::setParam(const std::vector<std::string>& param)
	{
		if (param.size() < 3)return;
		//パラメータをcharaからfloatに変えて保存
		m_Color = util::atof<util::Vec3>(param).reault;
		if (param.size() < 6)return;
		m_Offset = util::atof<util::Vec3>(param,3).reault;
		if (param.size() < 9)return;
		m_Scale = util::atof<util::Vec3>(param, 6).reault;
	}

	const util::Vec3 BoxRenderClientComponent::getColor()
	{
		return m_Color;
	}

	void BoxRenderClientComponent::setColor(util::Vec3 & color)
	{
		m_Color = color;
	}

	util::Vec3 BoxRenderClientComponent::getOffset()
	{
		return m_Offset;
	}

	void BoxRenderClientComponent::update()
	{
		//ポジションとアングルのみ自分のエンティティーから受け継ぐ
		auto trans = m_Entity.lock()->getTransform();
		m_Transform.m_Position = trans->m_Position + m_Offset;
		m_Transform.m_Rotation = trans->m_Rotation;
	}

	util::Transform * BoxRenderClientComponent::getTransform()
	{
		return &m_Transform;
	}

}