#pragma once
#include<Source\Component\Component.h>
#include<string>
#include<memory>
#include<Source\Component\Animation\AnimatorComponent.h>

/**
* @file	RigidInstanceRenderComponent.h
* @brief	インスタンス描画オーナーコンポーネントクラス
* @dital	このコンポーネントを実装しているオブジェクトはインスタンス描画される
* @authro	高須優輝
* @date	2017/03/13
*/

namespace framework {
	class CubeRenderTrget;
	class CubeDepthTarget;
}

namespace component {

	class RenderOwnerComponent;
	class BulletBoxCollider;
	class BulletSphereCollider;
	class RenderOwnerComponent;

	class RenderClientComponent:public framework::UpdateComponent
	{
	public:
		RenderClientComponent();
		~RenderClientComponent();

		const std::string& getModelName();

		/**
		* @brief		ゲームオブジェクトに接続された瞬間
		*/
		virtual void onConect()override;

		/**
		* @brief		このコンポーントをアクティブにする
		*/
		virtual void active()override;

		/**
		* @brief		このコンポーントをディアクティブにする
		カリング処理するためディアクティブしてもアップデートタスクからは削除されない
		*/
		virtual void deActive()override;

		/**
		* @brief		初期化
		* @Detail		すべてのコンポーネントが一斉に呼ばれる
		*/
		virtual void init()override;

		/** 
		* @brief			パラメータセット
		* @param param		文字列パラメータ
		*/
		void setParam(const std::vector<std::string>& param)override;

		/**
		* @brief			このクライアントのキューブマップの書き込みを開始する
		*/
		//void cubMapWriteBegin();

		/**
		* @brief			このクライアントのキューブマップの書き込みを終了
		*/
		//void cubMapWriteEnd();

		//std::shared_ptr<framework::CubeRenderTrget> getCubeMap();

		/**
		* @brief			このクライアントのキューブマップの書き込みを終了
		*/
		std::weak_ptr<AnimatorComponent> getAnimator();

		/// <summary>
		/// カリングをするかどうか
		/// </summary>
		/// <returns></returns>
		bool isCullingActive();

		bool isOutScreen(const util::Vec4 & point);

		util::Vec3* getCullingPoint();

		// UpdateComponent を介して継承されました
		virtual void update() override;

		void setTexLerp(float x);

		float getTexLerp();

		/// <summary>
		/// 太らせ値セット
		/// </summary>
		/// <param name="x"></param>
		void setFat(float x);

		/// <summary>
		/// 太らせ値取得
		/// </summary>
		/// <returns></returns>
		float getFat();
	public:
		util::Transform m_PrePosition;

	private:
		std::weak_ptr<component::RenderOwnerComponent> findModelOwner();

		/// <summary>
		/// カリング判定用頂点作成
		/// </summary>
		void createCullingCollistion();

	private:
		//std::shared_ptr<framework::CubeRenderTrget> m_pCubeTarget;
		//std::shared_ptr<framework::CubeDepthTarget> m_pCubeDepthTarget;
		std::weak_ptr<AnimatorComponent> m_pAnimator;
		std::weak_ptr<RenderOwnerComponent> m_pOwner;
		util::Mat4 m_Projection;
		std::weak_ptr<CameraComponent> m_pMainCamera;

		//!描画するモデルの名前
		std::string m_ModelName;

		//カリングするか
		bool m_IsCullingActive;

		//立方体で判定する
		util::Vec3 m_CullingPoints[8];

		//!前フレームのアクティブ状態s
		bool m_IsPreActive;

		//!テクスチャのラープ値
		float m_TexLerp;

		//太らせ値
		float m_FatNum;
	};



}