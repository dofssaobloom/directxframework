#pragma once
#include<Source\Component\Component.h>
#include<Source\Component\Render2DComponent.h>
#include<Source\Component\Render3DComponent.h>
#include<Source\Component\RenderClientComponent.h>
#include<Source\Component\RenderOwnerComponent.h>
#include<Source\Component\SpriteRenderComponent.h>
#include<Source\Component\UpdateComponent.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Component\Effect\DirectionalLightComponent.h>
#include<Source\Component\Effect\PointLightComponent.h>
#include<Source\Component\BoxRenderClientComponent.h>
#include<Source\Component\Animation\AnimatorComponent.h>
#include<Source\Component\Physics\BoxCollider.h>
#include<Source\Component\Physics\CircleCollider.h>
#include<Source\Component\Effect\BloomEffect.h>
#include<Source\Component\Stage\StageMotion.h>
#include<Source\Component\Physics\RigidBody.h>
#include<Source\Component\Effect\BloomEffect.h>
#include<Source\Component\Effect\BlurEffect.h>
#include<Source\Component\FadeComponent.h>
#include<Source\Component\Physics\BulletRigidBody.h>
#include<Source\Component\Physics\BulletBoxCollider.h>
#include<Source\Component\Physics\BulletSphereCollider.h>
#include<Source\Component\Effect\DOFEffect.h>
#include<Source\Component\Effect\FogEffect.h>
#include<Source\Component\Effect\FXAA.h>
#include<Source\Component\Util\Counter.h>
#include<Source\Component\Device\Sound\SoundPlayerComponent.h>
#include<Source\Component\UI\GUIComponent.h>
#include<Source\Component\Effect\ToneMapEffect.h>
#include<Source\Component\Util\FPSCounter.h>
#include<Source\Component\Device\Renderer\LineRenderComponent.h>
#include<Source\Component\Effect\GradationEffect.h>

#include<Source\Component\Device\Sound\ListenerComponent.h>
#include<Source\Component\Util\Counter.h>