#include"RenderClientComponent.h"
#include"RenderOwnerComponent.h"
#include<Source\Entity\Entity.h>
#include<Source\Component\CubeMapCreateComponent.h>
#include<Source\Resource\Texture\CubeReanderTarget.h>
#include<Source\Resource\Texture\CubeDepthTarget.h>
#include<Source\Util\Render\DXFunc.h>
#include<Source\Application\Screen\Screen.h>
#include<algorithm>
#include<Source\Component\Physics\BulletBoxCollider.h>
#include<Source\Component\Physics\BulletSphereCollider.h>


namespace component {


	RenderClientComponent::RenderClientComponent()
	{
		//m_pCubeTarget = std::make_shared<framework::CubeRenderTrget>(256, 256, DXGI_FORMAT_R16G16B16A16_FLOAT);
		//m_pCubeDepthTarget = std::make_shared<framework::CubeDepthTarget>(256, 256, DXGI_FORMAT_R32_TYPELESS);
		m_CallOrder = 99;
	}

	RenderClientComponent::~RenderClientComponent()
	{
	}

	const std::string & RenderClientComponent::getModelName()
	{
		return m_ModelName;
	}

	void RenderClientComponent::onConect()
	{
		auto renderer = framework::Entity::findGameObj("Renderer");
		auto owners = renderer.lock()->getComponents<RenderOwnerComponent>();

		auto find = std::find_if(owners.begin(), owners.end(), [&](std::weak_ptr<RenderOwnerComponent> owner) {
			return owner.lock()->getModelName() == m_ModelName;
		});
		assert(find != owners.end() && "モデルがcsvに登録されていません");

		m_pOwner = *find;
	}

	void RenderClientComponent::active()
	{
		m_pOwner.lock()->addDrawClient(m_Entity.lock()->getComponent<RenderClientComponent>(this));
	}

	void RenderClientComponent::deActive()
	{
		m_pOwner.lock()->removeDrawClient(m_Entity.lock()->getComponent<RenderClientComponent>(this));
	}


	void RenderClientComponent::init()
	{
		bool isSkin = false;

		auto model = framework::ResourceManager::getInstance()->getModel(m_ModelName);
		isSkin = model->type == framework::ModelType::skin;

		m_pAnimator = m_Entity.lock()->getComponent<AnimatorComponent>();
		if (isSkin)
			assert(!m_pAnimator.expired() && "アニメーターが必要です");

		//初期座標で初期化
		m_PrePosition = *(m_Entity.lock()->getTransform());

		createCullingCollistion();

		m_pMainCamera = framework::Entity::findGameObj("MainCamera").lock()->getComponent<component::CameraComponent>();
		m_Projection = m_pMainCamera.lock()->getProjection();
		m_Projection = m_Projection.transpose();

		m_TexLerp = 0;
		m_FatNum = 0;

		UpdateComponent::active();
		update();
	}

	void RenderClientComponent::setParam(const std::vector<std::string>& param)
	{
		//描画するモデルの名前
		m_ModelName = param[0];

		//デフォルトカリングOn
		m_IsCullingActive = true;

		if (param.size() < 2)return;
		if (param[1] == "")return;

		m_IsCullingActive = (bool)std::atoi(param[1].c_str());
	}

	//void RenderClientComponent::cubMapWriteBegin()
	//{
	//	util::setSingleViewPort(m_pCubeTarget->getSize().x, m_pCubeTarget->getSize().y);
	//	util::getContext()->OMSetRenderTargets(1, m_pCubeTarget->getView(), m_pCubeDepthTarget->getView());
	//	m_pCubeTarget->clear(util::Vec4(0.0f, 0.0f, 0.0f, 1.0f));
	//	m_pCubeDepthTarget->clear();
	//}

	//void RenderClientComponent::cubMapWriteEnd()
	//{
	//	util::setSingleViewPort(framework::Screen::PIXEL_WIDTH, framework::Screen::PIXEL_WIDTH);
	//	util::getContext()->OMSetRenderTargets(0, NULL, NULL);
	//}

	//std::shared_ptr<framework::CubeRenderTrget> RenderClientComponent::getCubeMap()
	//{
	//	return m_pCubeTarget;
	//}

	std::weak_ptr<AnimatorComponent> RenderClientComponent::getAnimator()
	{
		return m_pAnimator;
	}

	bool RenderClientComponent::isCullingActive()
	{
		return m_IsCullingActive;
	}

	util::Vec3 * RenderClientComponent::getCullingPoint()
	{
		return m_CullingPoints;
	}

	float RenderClientComponent::getFat()
	{
		return m_FatNum;
	}

	std::weak_ptr<component::RenderOwnerComponent> RenderClientComponent::findModelOwner()
	{
		auto renderer = framework::Entity::findGameObj("Renderer");
		auto renderComponentList = renderer.lock()->getComponents<component::RenderOwnerComponent>();
		//自分のオーナーを探す
		auto findComponent = std::find_if(renderComponentList.begin(), renderComponentList.end(), [&](std::weak_ptr<component::RenderOwnerComponent> component) {
			return component.lock()->getModelName() == m_ModelName;
		});

		return *findComponent;
	}

	void RenderClientComponent::createCullingCollistion()
	{
		//ついている形状コンポーネントから頂点作成
		auto shape = m_Entity.lock()->getComponent < component::PhysicsShape >();
		if (shape.expired())return;

		ViewCullingVertex vertex;
		shape.lock()->createViewCullingCollision(vertex);

		util::foreach(8, [&](int i) {
			m_CullingPoints[i] = vertex.pos[i];
		});

	}

	bool RenderClientComponent::isOutScreen(const util::Vec4 & point)
	{
		return !(point.x <= 1 && point.x >= -1 &&
			point.y <= 1 && point.y >= -1 &&
			point.z <= 1 && point.z >= 0);
	}


	void RenderClientComponent::update()
	{
		//カリング処理
		util::Mat4 view = m_pMainCamera.lock()->toViewMatrix();
		view = view.transpose();
		//カリングがoffになっていたら処理しない
		if (!isCullingActive())return;
		if (!m_Entity.lock()->isActive())return;
		util::Vec4 viewProjPoint[8];
		auto centerPos = m_Entity.lock()->getTransform()->m_Position;
		for (size_t i = 0; i < 8; ++i)
		{
			auto temp = getCullingPoint();
			util::Vec4 pos(centerPos.x + temp[i].x, centerPos.y + temp[i].y, centerPos.z + temp[i].z, 1);
			pos = XMVector4Transform(pos.toXMVector(), view.toXMMatrix());
			pos = XMVector4Transform(pos.toXMVector(), m_Projection.toXMMatrix());
			viewProjPoint[i] = pos / pos.w;
		}

		m_IsPreActive = m_isActive;
		if (isOutScreen(viewProjPoint[0]) && isOutScreen(viewProjPoint[1]) && isOutScreen(viewProjPoint[2]) && isOutScreen(viewProjPoint[3]) &&
			isOutScreen(viewProjPoint[4]) && isOutScreen(viewProjPoint[5]) && isOutScreen(viewProjPoint[6]) && isOutScreen(viewProjPoint[7])) {
			//すべての頂点がスクリーンの外だったらディアクティブ
			//deActive();
			m_isActive = false;
		}
		else {
			//どこか１頂点でも入っていたらアクティブ				
			//active();
			m_isActive = true;
		}

		//アクティブ状態の切り替わったときだけ描画を切り替える
		if (m_isActive && !m_IsPreActive) {
			active();
		}
		if (!m_isActive && m_IsPreActive) {
			deActive();
		}
	}

	void RenderClientComponent::setTexLerp(float x)
	{
		m_TexLerp = x;
	}

	float RenderClientComponent::getTexLerp()
	{
		return m_TexLerp;
	}

	void RenderClientComponent::setFat(float x)
	{
		m_FatNum = x;
	}

}