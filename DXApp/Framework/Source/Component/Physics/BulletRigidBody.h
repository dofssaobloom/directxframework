#pragma once

/**
* @file	BulletRigidBody.h
* @brief	剛体を表すクラス
* @deteil   必ず剛体の形状を表すクラスを同じEntityにAddコンポーネントしなければならない
* @authro	高須優輝
* @date	2017/09/12
*/


#include<Include\BULLET\btBulletDynamicsCommon.h>
#include<Source\Component\UpdateComponent.h>
#include<Source\Util\btUtil.h>
#include<Source\Component\Physics\PhysicsComponent.h>
#include<Include\BULLET\BulletCollision\CollisionDispatch\btGhostObject.h>

namespace component {

	class PhysicsShape;
	class PhysicsWorld;
	class GUIComponent;

	enum class ForceMode
	{
		Force, //継続した推力を与える
		Impulse,//順次の推力を与える

	};

	class BulletRigidBody : public framework::PhysicsComponent
	{
	public:
		struct  FreezeElementPointer;

		struct  FreezeElement
		{
			void operator=(FreezeElementPointer other) {
				x = *other.x;
				y = *other.y;
				z = *other.z;
			}

			bool x, y, z;
		};

		BulletRigidBody();
		~BulletRigidBody();
		/**
		* @brief		このコンポーントをアクティブにする
		*/
		virtual void active()override;

		/**
		* @brief		このコンポーントをディアクティブにする
		*/
		virtual void deActive()override;

		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override;

		/**
		* @brief		ゲームオブジェクトに接続された瞬間
		*/
		virtual void onConect();
		
		/**
		* @brief		初期化
		* @Detail		すべてのコンポーネントが一斉に呼ばれる
		*/
		virtual void init();

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		/// <summary>
		/// 指定ベクトルに力を与える
		/// </summary>
		void addForce(const util::Vec3& thrust, ForceMode mode = ForceMode::Impulse);

		void updateRigidBodyTrans();

		void updateEntityTrans();

		const bool isTrigger()const;

		/// <summary>
		/// ポジションフラグを見て物理エンジンの移動量を打ち消す
		/// </summary>
		void moveVelocityOption();

		/// <summary>
		/// 回転フラグを見て物理エンジンの回転量を打ち消す
		/// </summary>
		void angleVelocityOption();

		/// <summary>
		/// ポジションフリーズフラグセット
		/// </summary>
		/// <param name="flags"></param>
		void setPositionFreezeFlag(FreezeElement flags);

		/// <summary>
		/// 回転フリーズフラグセット
		/// </summary>
		/// <param name="flags"></param>
		void setRotateFreezeFlag(FreezeElement flags);

		/// <summary>
		/// 座標のフリーズ状態取得
		/// </summary>
		/// <returns></returns>
		FreezeElement getPosFreezeFlag();
		
		/// <summary>
		/// 回転におフリーズ状態取得
		/// </summary>
		/// <returns></returns>
		FreezeElement getRotateFreezeFlag();

		/// <summary>
		/// デフォルトのオリジンを取得
		/// </summary>
		/// <returns></returns>
		float getDefOriginY();

		
		void getLinearVelocity(util::Vec3& result);

		/// <summary>
		/// 形状クラス取得
		/// </summary>
		/// <returns></returns>
		std::weak_ptr<PhysicsShape>& getShape();

		/// <summary>
		/// リジットボディ取得
		/// </summary>
		/// <returns></returns>
		btRigidBody* getBtRigidBody();

		//トリガー判定の切り替え
		void setTrriger(bool isTrriger);

	private:

		/// <summary>
		/// オフセットをモデルの正面方向に変換する
		/// </summary>
		/// <returns></returns>
		util::Vec3 offsetConvertFront();

	private:

		struct  FreezeElementPointer
		{
			FreezeElement  operator*()const {
				return {*x,*y,*z};
			}

			void operator=(FreezeElement other){
				*x = other.x;
				*y = other.y;
				*z = other.z;
			}

			bool* x, *y,* z;
		};

		struct RIGIDBODY_DESC
		{
			//!質量
			float* wight;
			//!反射係数
			float* restitution;
			//!摩擦係数
			float* friction;
			//!トリガーかどうか
			bool* isTrigger;

			FreezeElementPointer position;

			FreezeElementPointer rotation;
		};

		RIGIDBODY_DESC m_Desc;

#ifdef _MDEBUG
		//!前フレーム回転フリーズフラグ
		//デバッグ処理がある場合は切り替えを検知しなければいけないので変数を作る
		FreezeElement m_PreRotateFlag;
		FreezeElement m_CurrentRotateFlag;
#endif

		std::unique_ptr<btRigidBody> m_pRigidBody;

		std::weak_ptr<PhysicsShape> m_pShape;

		std::weak_ptr<PhysicsWorld> m_pWorld;

		//!慣性モーメント
		btVector3 m_Inertia;

		//!保存用
		btVector3 m_OriginalIertia;

		std::string m_LuaPath;

		std::string m_PrefabPath;

		//!addForceの関数
		std::unordered_map<ForceMode, std::function<void(const util::Vec3 &)>> m_pForceAction;

		std::weak_ptr<GUIComponent> m_pUI;

		util::Vec3 m_Offset;

		float m_DefOriginY;

	};

}