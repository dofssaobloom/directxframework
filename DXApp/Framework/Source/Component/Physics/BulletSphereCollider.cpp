#include "BulletSphereCollider.h"

namespace component {


	BulletSphereCollider::BulletSphereCollider()
	{
		m_CallOrder = 0;
	}

	BulletSphereCollider::~BulletSphereCollider()
	{
	}

	void BulletSphereCollider::setParam(const std::vector<std::string>& param)
	{
		PhysicsShape::setParam(param);
	}

	void BulletSphereCollider::init()
	{
		PhysicsShape::init();
		m_pScale = m_pUI.lock()->getSliderPointer("Scale");
		createShape();//リジッドボディより先に初期化されなくてはならない
	}

	void BulletSphereCollider::getScale(float * result)
	{
		*result = *m_pScale;
	}

	void BulletSphereCollider::update()
	{
#ifdef _MDEBUG
		setScale(*m_pScale);
#endif
	}

	util::Vec3 BulletSphereCollider::getOrigin()
	{
		return util::Vec3(0, (*m_pScale) / 2, 0);
	}

	float BulletSphereCollider::originY()
	{
		return *m_pScale;
	}

	void BulletSphereCollider::createViewCullingCollision(ViewCullingVertex& result)
	{
		float size = *m_pScale;
		auto pos = m_Entity.lock()->getTransform()->m_Position + size;
		//前4つ
		result.pos[0] = util::Vec3(size, size, size);
		result.pos[1] = util::Vec3(size, -size, size);
		result.pos[2] = util::Vec3(-size, -size, size);
		result.pos[3] = util::Vec3(-size, size, size);

		//後ろ4つ
		result.pos[4] = util::Vec3(size, size, -size);
		result.pos[5] = util::Vec3(size, -size, -size);
		result.pos[6] = util::Vec3(-size, -size, -size);
		result.pos[7] = util::Vec3(-size, size, -size);
	}

	void BulletSphereCollider::setScale(float scale)
	{
		*m_pScale = scale;
		auto collision = dynamic_cast<btSphereShape*>(m_pCollision.get());
		collision->setUnscaledRadius(*m_pScale);
	}

	void BulletSphereCollider::setOffset(const util::Vec3 & offset)
	{
		*m_pOffsetX = offset.x;
		*m_pOffsetY = offset.y;
		*m_pOffsetZ = offset.z;
	}

	void BulletSphereCollider::createShape()
	{
		m_pCollision = std::make_unique<btSphereShape> (*m_pScale);
		//btTransform centerOffset;
		//centerOffset.setIdentity();
		//centerOffset.setOrigin(util::convertBtVector3(m_Offset));
		//m_pMotionState = std::make_unique<btDefaultMotionState>(btTransform(btQuaternion(), util::convertBtVector3(m_Offset)));
		m_pMotionState = std::make_unique<btDefaultMotionState>(btTransform(btQuaternion(0,0,0,1),btVector3(0,0,0)));
		//m_pcompoundShape = std::make_unique<btCompoundShape>();
	//	m_pcompoundShape->addChildShape(centerOffset, m_pCollision.get());
	}

}