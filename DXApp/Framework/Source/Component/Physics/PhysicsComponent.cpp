#include"PhysicsComponent.h"
#include<Source\Entity\Entity.h>
#include<Source\Task\TaskManager.h>
#include<Source\Util\Template\Template.h>

namespace framework {

	PhysicsComponent::PhysicsComponent()
	{
	}


	PhysicsComponent::~PhysicsComponent()
	{
	}

	void PhysicsComponent::active()
	{
		auto wcom = m_Entity.lock()->getComponent<PhysicsComponent>(this);

		TaskManager::getInstance()->addTask(wcom);
		m_isActive = true;
	}

	void PhysicsComponent::deActive()
	{
		auto wcom = m_Entity.lock()->getComponent<PhysicsComponent>(this);

		TaskManager::getInstance()->removeTask(wcom);
		m_isActive = false;
	}

	void PhysicsComponent::init()
	{
		m_HitContainer.clear();
	}

	void PhysicsComponent::onCollisionEnters()
	{
		//空になるまで
		while (!onCollitsionContainer.empty()) {
			onCollitsionContainer.front().first.lock()->onCollisionEnter(onCollitsionContainer.front().second);
			onCollitsionContainer.pop_front();
		}
	}


	bool PhysicsComponent::isHit()
	{
		//for (auto& hitObj : m_HitContainer) {
		//	//１つでもヒットしているものがあればtrue
		//	if (hitObj.second) {
		//		return true;
		//	}
		//}
		//return false;
		return true;
	}

	bool PhysicsComponent::isHit(std::weak_ptr<Entity> entity)
	{
		//auto find = std::find_if(m_HitContainer.begin(), m_HitContainer.end(), [&](const HitPair& pair) {
		//	return pair.first.lock()->getGameObj()._Get() == entity._Get();
		//});

		////コンテナになければfalse
		//if (find == m_HitContainer.end())return false;

		//return find->second;
		return true;
	}

	void PhysicsComponent::addHitEnter(std::weak_ptr<PhysicsComponent> other, const util::Vec3& pointA, const util::Vec3& pointB)
	{

		auto find = std::find_if(m_HitContainer.begin(), m_HitContainer.end(), [&](HitPair& paier) {
			return paier.first._Get() == other._Get();
		});

		if (find != m_HitContainer.end()) {
			find->second.isHit = true;
			return;
		}

		auto&& otherEntity = other.lock()->getGameObj();
		auto hitData = HitData(otherEntity, true, pointA, pointB);
		//ヒットした履歴がなければ追加する
		onCollitsionContainer.emplace_front(std::make_pair(m_Entity, hitData));
		m_HitContainer.emplace_back(std::make_pair(other,hitData));
	}

	void PhysicsComponent::hitObjAction(bool hitFlag, std::function<void(std::weak_ptr<Entity>)> action)
	{
		//for (auto& hitObj : m_HitContainer) {
		//	//１つでもヒットしているものがあればtrue
		//	if (hitObj.second != hitFlag)return;
		//	action(hitObj.first.lock()->getGameObj());
		//}

	}

	//呼ばれる順番 1
	void PhysicsComponent::checkDelete()
	{
		m_HitContainer.remove_if([](HitPair hitObj) {
			if (hitObj.second.other.expired())return true;
			return  !hitObj.second.other.lock()->isActive();
		});
	}
	//呼ばれる順番 2
	void PhysicsComponent::collisionUpdate()
	{
		if (m_HitContainer.empty())return;

		//ヒットフラグがtrueのものだけ取得
		auto finds = util::listCopyif<HitPair>(m_HitContainer, [&](HitPair& paier) {
			return paier.second.isHit;
		});

		//ヒットフラグが立っているものはStayを呼ぶ
		for (auto& other : finds) {
			onStayContainer.emplace_back(std::make_pair(m_Entity, other.second));
		}

		//ヒットフラグが立っていないものだけを取得
		finds = util::listCopyif<HitPair>(m_HitContainer, [&](HitPair paier) {
			return !paier.second.isHit;
		});

		for (auto& other : finds) {
			onExitContainer.emplace_back(std::make_pair(m_Entity, other.second));
		}

		//ヒットフラグがfalseのものを削除
		m_HitContainer.remove_if([&](HitPair& paier) {
			return !paier.second.isHit;
		});

		//一度ヒットフラグをすべてfalseにする
		//次のループでもヒットしたものはtrueになるのでコールバックが呼ばれる
		for (auto& pair : m_HitContainer) {
			pair.second.isHit = false;
		}

	}

	//呼ばれる順番 3
	void PhysicsComponent::callCollisionAction()
	{
		checkDelete(onCollitsionContainer);
		checkDelete(onStayContainer);
		checkDelete(onExitContainer);

		//空になるまで
		while (!onCollitsionContainer.empty()) {
			onCollitsionContainer.front().first.lock()->onCollisionEnter(onCollitsionContainer.front().second);
			onCollitsionContainer.pop_front();
		}

		//空になるまで
		while (!onStayContainer.empty()) {
			onStayContainer.front().first.lock()->onCollisionStay(onStayContainer.front().second);
			onStayContainer.pop_front();
		}

		//空になるまで
		while (!onExitContainer.empty()) {
			onExitContainer.front().first.lock()->onCollisionExit(onExitContainer.front().second);
			onExitContainer.pop_front();
		}
	}

	void PhysicsComponent::checkDelete(CallBackContainer & container)
	{
		//ペアのどちらかが参照切れしてたらリストから消す
		auto deleteElement = std::remove_if(container.begin(), container.end(), [](std::pair<std::weak_ptr<Entity>, HitData>& paier) {
			return paier.first.expired() || paier.second.other.expired();
		});

		container.erase(deleteElement, container.end());
	}



}