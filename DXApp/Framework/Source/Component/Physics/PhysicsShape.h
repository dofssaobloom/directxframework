#pragma once
#include<Framework.h>
#include<Include\BULLET\btBulletDynamicsCommon.h>
#include<Source\Component\UI\GUIComponent.h>
#include<string>

namespace component {

	struct ViewCullingVertex {
		util::Vec3 pos[8];
	};

	class PhysicsShape abstract : public framework::UpdateComponent
	{
	public:
		PhysicsShape() {
			m_CallOrder = 0;
		}
		virtual ~PhysicsShape() {}

		/**
		* @brief						自分が実装されているEntityにコンポーネントを追加する
		* @param 	componentInitalizer	このvectorに格納されたコンポーネントは一斉に初期化される
		*/
		virtual void componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)override {
			m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath,m_PrefabPath });
			(*componentInitalizer).emplace_back(m_pUI);
		}

		virtual void init()override {
			m_pOffsetX = m_pUI.lock()->getSliderPointer("OffsetX");
			m_pOffsetY = m_pUI.lock()->getSliderPointer("OffsetY");
			m_pOffsetZ = m_pUI.lock()->getSliderPointer("OffsetZ");

			m_pRotaionOffsetX = m_pUI.lock()->getSliderPointer("RotaionOffsetX");
			m_pRotaionOffsetY = m_pUI.lock()->getSliderPointer("RotaionOffsetY");
			m_pRotaionOffsetZ = m_pUI.lock()->getSliderPointer("RotaionOffsetZ");

		}


		void onConect() override {
			//接続されたタイミングで形状作成
			//このあとrigidBodyが取得する

			m_CallOrder = 0;
		}


		/// <summary>
		/// 形状作成
		/// </summary>
		virtual void createShape() = 0;

		/// <summary>
		/// 回転中心
		/// </summary>
		/// <returns></returns>
		virtual util::Vec3 getOrigin() = 0;

		btCollisionShape* getShape() {
			return m_pCollision.get();
		}

		btDefaultMotionState* getMotion() {
			return m_pMotionState.get();
		}

		util::Vec3 getOffset() const {
			return  util::Vec3((*m_pOffsetX), (*m_pOffsetY), (*m_pOffsetZ));
		}

		util::Vec3 getRotationOffset() {
			return  util::Vec3((*m_pRotaionOffsetX), (*m_pRotaionOffsetY), (*m_pRotaionOffsetZ));
		}

		void getLocalInertia(float mass, util::Vec3* localInertia) {
			btVector3 resutl;
			m_pCollision->calculateLocalInertia(mass, resutl);
			*localInertia = util::convertUtVector3(resutl);
		}

		void getLocalInertia(float mass, btVector3* localInertia) {
			m_pCollision->calculateLocalInertia(mass, *localInertia);
		}

		/// <summary>
		/// 回転中心
		/// </summary>
		/// <returns></returns>
		virtual	float originY() = 0;


		/// <summary>
		/// ビューカリング用のコリジョン作成
		/// </summary>
		virtual void createViewCullingCollision(ViewCullingVertex& result) = 0;


		virtual void setParam(const std::vector<std::string>& param)
		{
			m_LuaPath = param[0];
			m_PrefabPath = param[1];
		}

	protected:
		std::unique_ptr<btCollisionShape> m_pCollision;
		std::unique_ptr<btDefaultMotionState> m_pMotionState;
		float *m_pOffsetX;
		float *m_pOffsetY;
		float *m_pOffsetZ;
		float *m_pRotaionOffsetX;
		float *m_pRotaionOffsetY;
		float *m_pRotaionOffsetZ;
		std::weak_ptr<GUIComponent> m_pUI;
		std::string m_LuaPath;
		std::string m_PrefabPath;
	};

}