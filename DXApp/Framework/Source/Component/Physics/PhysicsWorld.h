#pragma once
#include<Include\BULLET\btBulletDynamicsCommon.h>
#include<Source\Component\Component.h>
#include<functional>
#include<Source\Component\Physics\PhysicsRaycast.h>


namespace framework {
	class DirectXDrawable;
	struct PhysicsRaycast;
}

namespace component {
	class BulletRigidBody;

	class PhysicsWorld : public framework::Component
	{
	public:
		PhysicsWorld();
		~PhysicsWorld();

		/**
		* @brief		初期化
		* @Detail		すべてのコンポーネントが一斉に呼ばれる
		*/
		virtual void init();

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);

		void addRigidBody(btRigidBody* rigidBody);

		void addCollisionObj(btCollisionObject* obj);

		void removeRigidBody(btRigidBody* rigidBody);

		void removeCollisionObj(btCollisionObject* obj);

		// Render3DComponent を介して継承されました
		virtual void draw();

		void physicsUpdate();

		framework::PhysicsRaycast rayCast(const util::Vec3& begin, const util::Vec3& end);


		/**
		* @brief		クリアー
		*/
		virtual void clear();

		/// <summary>
		/// すでに理事度ボディが含まれているか
		/// </summary>
		/// <param name=""></param>
		bool isContainsRigidBody(btRigidBody*);

		/// <summary>
		/// raycastLine描画
		/// </summary>
		/// <param name="begin"></param>
		/// <param name="end"></param>
		void drawLine(const util::Vec3& begin, const util::Vec3& end);


		/// <summary>
		/// 物理開始
		/// </summary>
		void activePhysics();

		/// <summary>
		/// 物理ストップ
		/// </summary>
		void deActivePhysics();

	private:

		void eachRigidBody(std::function<void(std::weak_ptr<BulletRigidBody>)> action);

		static bool collisionCallBack(btManifoldPoint& cp, void* body0, void* body1);

		//static bool collisionAddCallBack(btManifoldPoint& cp, const btCollisionObjectWrapper* colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper* colObj1Wrap, int partId1, int index1);

		/// <summary>
		/// 参照切れ削除
		/// </summary>
		void checkDelete();

	private:
		struct Bullet
		{
			Bullet();
			~Bullet();

			std::unique_ptr<framework::DirectXDrawable> pDrawable;
			std::unique_ptr<btDefaultCollisionConfiguration> pCollisionConfig;
			std::unique_ptr<btCollisionDispatcher> pDispatcher;
			std::unique_ptr<btBroadphaseInterface> pBroadphase;
			std::unique_ptr<btSequentialImpulseConstraintSolver> pSolver;
			std::unique_ptr<btDiscreteDynamicsWorld> btWorld;
		};

		Bullet m_Bullet;

		//描画命令
		std::function<void()> m_DrawFunc;

		bool m_IsPhysicsStop;

	};



}