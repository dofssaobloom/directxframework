#include "Framework.h"
#include "PhysicsRaycast.h"
#include<Source\Util\btUtil.h>

namespace framework {

	PhysicsRaycast::PhysicsRaycast(const util::Vec3& rayBegin, const util::Vec3& rayEnd)
		:btCollisionWorld::ClosestRayResultCallback(util::convertBtVector3(rayBegin), util::convertBtVector3(rayEnd))
	{
	}

	PhysicsRaycast::~PhysicsRaycast()
	{
		
	}

	WeakEntity PhysicsRaycast::getHitEntityWeak()
	{
		auto entity = (framework::Entity*)((btRigidBody*)m_collisionObject)->getUserPointer();
		return Entity::findGameObj(entity);
	}
	Entity * PhysicsRaycast::getHitEntity()
	{
		return (framework::Entity*)((btRigidBody*)m_collisionObject)->getUserPointer();
	}
}