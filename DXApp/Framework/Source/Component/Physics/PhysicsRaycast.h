#pragma once
#include<Include\BULLET\btBulletDynamicsCommon.h>
#include<Framework.h>

namespace framework {

	struct PhysicsRaycast : public btCollisionWorld::ClosestRayResultCallback
	{
	public:
		PhysicsRaycast(const util::Vec3& rayBegin, const util::Vec3& rayEnd);
		~PhysicsRaycast();

		/// <summary>
		/// レイがヒットしたゲームオブジェクトを取得
		/// ウィークポインターに変換するためにゲームオブジェクトを検索しているので、
		/// ゲームオブジェクトを保持する必要がないときはgetHitEntityを使う
		/// </summary>
		/// <returns></returns>
		WeakEntity getHitEntityWeak();

		/// <summary>
		/// レイがヒットしたゲームオブジェクトを取得
		/// getHitEntityWeak よりも高速
		/// 生ポインタなので保持したい場合はgetHitEntityWeakを使う
		/// </summary>
		/// <returns></returns>
		Entity* getHitEntity();

	private:

	};

}