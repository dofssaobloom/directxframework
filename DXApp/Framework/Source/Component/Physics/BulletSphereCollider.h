#pragma once
#include "Framework.h"
#include<Source\Component\Physics\BulletRigidBody.h>
#include<Source\Component\Physics\PhysicsShape.h>

namespace component {
	class GUIComponent;

	class BulletSphereCollider : public PhysicsShape
	{
	public:
		BulletSphereCollider();
		~BulletSphereCollider();

		/**
		* @brief		パラメータをセット
		* @param param  文字列パラメータ
		*/
		virtual void setParam(const std::vector<std::string>& param);


		/**
		* @brief		初期化
		*/
		virtual void init();


		/// <summary>
		///　球体のスケール取得
		/// </summary>
		void getScale(float* result);

		// UpdateComponent を介して継承されました
		virtual void update()override;


		/// <summary>
		/// 回転中心
		/// </summary>
		/// <returns></returns>
		virtual util::Vec3 getOrigin()override;


		/// <summary>
		/// 回転中心
		/// </summary>
		/// <returns></returns>
		virtual	float originY() override;

		/// <summary>
		/// ビューカリング用のコリジョン作成
		/// </summary>
		virtual void createViewCullingCollision(ViewCullingVertex& result) override;

		/// <summary>
		/// 球体コリジョンのスケールをセットする
		/// </summary>
		/// <param name="scale"></param>
		void setScale(float scale);

		/// <summary>
		/// オフセットセット
		/// </summary>
		/// <param name="offset"></param>
		void setOffset(const util::Vec3& offset);
	private:
		// BulletRigidBody を介して継承されました
		void createShape() override;
	private:
		float* m_pScale;

	};

}