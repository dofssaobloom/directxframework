#pragma once

/**
* @file	BulletBoxCollider.h
* @brief	BulletBoxCollider.h
* @deteil   rigidoBodyがなければ動作しない
* @authro	高須優輝
* @date	2017/09/12
*/

#include<Framework.h>
#include<Include\BULLET\btBulletCollisionCommon.h>
#include<memory>
#include<Source\Component\Physics\PhysicsShape.h>

namespace component {
	class GUIComponent;

	class BulletBoxCollider : public PhysicsShape
	{
	public:
		BulletBoxCollider();
		~BulletBoxCollider();

		/// <summary>
		/// パラメータのセット
		/// </summary>
		/// <param name="param">
		/// sizeX
		/// sizeY
		/// sizeZ
		/// </param>
		void setParam(const std::vector<std::string>& param)override;

		/**
		* @brief		初期化
		*/
		virtual void init();


		/// <summary>
		/// コライダーサイズ取得
		/// </summary>
		void getBoxSize(util::Vec3* result);

		// PhysicsShape を介して継承されました
		virtual void update() override;

		/// <summary>
		/// 回転中心
		/// </summary>
		/// <returns></returns>
		virtual util::Vec3 getOrigin()override;

		/// <summary>
		/// 回転中心
		/// </summary>
		/// <returns></returns>
		virtual	float originY() override;

		/// <summary>
		/// ビューカリング用のコリジョン作成
		/// </summary>
		virtual void createViewCullingCollision(ViewCullingVertex& result) override;

	private:
		// BulletRigidBody を介して継承されました
		void createShape() override;

	private:
		//!ボックスのサイズ
		util::Vec3 m_Size;
		std::string m_LuaPath;
		std::string m_PrefabPath;		
		//!スケールのサイズをコライダーのサイズにするか
		bool m_IsScaleToColiderSize;

	};
}