#include"BulletRigidBody.h"
#include<Source\Component\Physics\PhysicsWorld.h>
#include<Source\Util\btUtil.h>
#include<Source\Component\Physics\BulletBoxCollider.h>
#include<xnamath.h>
#include<Source\Component\Physics\PhysicsShape.h>
#include<float.h>

using namespace util;

namespace component {

	BulletRigidBody::BulletRigidBody()
	{
		m_pForceAction[ForceMode::Force] = [&](const util::Vec3 & thrust) {
			m_pRigidBody->applyCentralForce(util::convertBtVector3(thrust));
		};

		m_pForceAction[ForceMode::Impulse] = [&](const util::Vec3 & thrust) {
			m_pRigidBody->applyCentralImpulse(util::convertBtVector3(thrust));
		};
		m_CallOrder = 1;
	}

	BulletRigidBody::~BulletRigidBody()
	{
		if (m_pWorld.expired())return;

		//ディアクティブ状態の場合はすでになくなっているので処理しない
		if (m_pWorld.lock()->isContainsRigidBody(m_pRigidBody.get()))
			m_pWorld.lock()->removeRigidBody(m_pRigidBody.get());


	}

	void BulletRigidBody::active()
	{
		PhysicsComponent::active();

		if (m_pWorld.expired())return;
		if (!m_pWorld.lock()->isContainsRigidBody(m_pRigidBody.get()))
			m_pWorld.lock()->addRigidBody(m_pRigidBody.get());

	}

	void BulletRigidBody::deActive()
	{
		PhysicsComponent::deActive();

		if (m_pWorld.expired())return;
		if (m_pWorld.lock()->isContainsRigidBody(m_pRigidBody.get()))
			m_pWorld.lock()->removeRigidBody(m_pRigidBody.get());

	}

	void BulletRigidBody::componentSet(std::vector<std::weak_ptr<Component>>* componentInitalizer)
	{
		m_pUI = m_Entity.lock()->addComponent<GUIComponent>(componentInitalizer, { m_LuaPath ,m_PrefabPath });
		(*componentInitalizer).emplace_back(m_pUI);
	}

	void BulletRigidBody::onConect()
	{
	}

	void BulletRigidBody::init()
	{
#ifdef _MDEBUG

		if (m_pUI.expired()) {
			util::WinFunc::drawMessageBox("エラー", m_Entity.lock()->getName() + "RigidBodyのLuaのパスが間違ってます");
		}
#endif

		m_Desc.isTrigger = m_pUI.lock()->isCheckPointer("IsTrigger");

		m_Desc.position.x = m_pUI.lock()->isCheckPointer("PosX");
		m_Desc.position.y = m_pUI.lock()->isCheckPointer("PosY");
		m_Desc.position.z = m_pUI.lock()->isCheckPointer("PosZ");

		m_Desc.rotation.x = m_pUI.lock()->isCheckPointer("RotateX");
		m_Desc.rotation.y = m_pUI.lock()->isCheckPointer("RotateY");
		m_Desc.rotation.z = m_pUI.lock()->isCheckPointer("RotateZ");

#ifdef _MDEBUG
		m_PreRotateFlag = m_Desc.rotation;
#endif

		m_Desc.wight = m_pUI.lock()->getSliderPointer("Mass");
		m_Desc.restitution = m_pUI.lock()->getSliderPointer("Restitution");
		m_Desc.friction = m_pUI.lock()->getSliderPointer("Rriction");
		m_pShape = m_Entity.lock()->getComponent<PhysicsShape>();

		auto trans = m_Entity.lock()->getTransform();

		btQuaternion qrot;
		qrot.setEuler(XMConvertToRadians(trans->m_Rotation.y),
			XMConvertToRadians(trans->m_Rotation.x),
			XMConvertToRadians(trans->m_Rotation.z));


		//	質量と慣性モーメントの計算
		m_pShape.lock()->getLocalInertia(*m_Desc.wight, &m_Inertia);
		m_OriginalIertia = m_Inertia;

		btRigidBody::btRigidBodyConstructionInfo info(*m_Desc.wight, m_pShape.lock()->getMotion(), m_pShape.lock()->getShape(), m_Inertia);

		m_pRigidBody = std::make_unique<btRigidBody>(info);

		setRotateFreezeFlag(*(m_Desc.rotation));

		m_pRigidBody->setUserPointer(m_Entity._Get());
		m_pShape.lock()->getShape()->setUserPointer(m_Entity._Get());

		m_pRigidBody->setWorldTransform(btTransform(qrot, convertBtVector3(trans->m_Position)));

		m_pRigidBody->setRestitution(*m_Desc.restitution);
		m_pRigidBody->setFriction(*m_Desc.friction);

		if (*m_Desc.isTrigger) {
			m_pRigidBody->setCollisionFlags(m_pRigidBody->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
		}
		else {
			m_pRigidBody->setCollisionFlags(0);
		}

		bool isDynamic = *m_Desc.wight != 0;
		if (isDynamic) {
			m_pRigidBody->setActivationState(DISABLE_DEACTIVATION);
		}
		else {
			m_pRigidBody->setActivationState(btCollisionObject::CF_STATIC_OBJECT);
		}

		m_pWorld = framework::Entity::findGameObj("PhysicsWorld").lock()->getComponent<PhysicsWorld>();

		if (m_isActive && m_Entity.lock()->isActive())
			m_pWorld.lock()->addRigidBody(m_pRigidBody.get());


		m_DefOriginY = m_pUI.lock()->getSlider("OriginY");

		m_Entity.lock()->getTransform()->m_Origin = util::Vec3(0, util::lerp<float>(m_DefOriginY, -m_pShape.lock()->originY(), m_pShape.lock()->originY()), 0);

		m_Offset = m_pShape.lock()->getOffset();
	}

	void BulletRigidBody::setParam(const std::vector<std::string>& param)
	{
		m_LuaPath = param[0];
		m_PrefabPath = param[1];
	}

	void BulletRigidBody::addForce(const util::Vec3 & thrust, ForceMode mode)
	{
		m_pForceAction[mode](thrust);
	}

	void BulletRigidBody::updateRigidBodyTrans()
	{

#ifdef _MDEBUG
		//トリガー判定の切り替えはリリーズ時は初期化でしか出来ないようにする
		if (*m_Desc.isTrigger) {
			m_pRigidBody->setCollisionFlags(m_pRigidBody->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
		}
		else {
			m_pRigidBody->setCollisionFlags(0);
		}

		//切り替えの検知が必要なのでデバッグのときはリアルタイムに取得する
		m_CurrentRotateFlag.x = m_pUI.lock()->isCheck("RotateX");
		m_CurrentRotateFlag.y = m_pUI.lock()->isCheck("RotateY");
		m_CurrentRotateFlag.z = m_pUI.lock()->isCheck("RotateZ");


		//切り替わりを検知する
		if (m_CurrentRotateFlag.x != m_PreRotateFlag.x) {
			setRotateFreezeFlag(*(m_Desc.rotation));
		}
		if (m_CurrentRotateFlag.y != m_PreRotateFlag.y) {
			setRotateFreezeFlag(*(m_Desc.rotation));
		}
		if (m_CurrentRotateFlag.z != m_PreRotateFlag.z) {
			setRotateFreezeFlag(*(m_Desc.rotation));
		}

		//前フレームとして保存
		m_PreRotateFlag = m_CurrentRotateFlag;

		m_pRigidBody->setMassProps(*m_Desc.wight, m_Inertia);
		m_pRigidBody->setRestitution(*m_Desc.restitution);
		m_pRigidBody->setFriction(*m_Desc.friction);

		bool isDynamic = *m_Desc.wight != 0;
		if (isDynamic) {
			m_pRigidBody->setActivationState(DISABLE_DEACTIVATION);
		}
		else {
			m_pRigidBody->setActivationState(btCollisionObject::CF_STATIC_OBJECT);
		}

		//auto originY = m_pUI.lock()->getSlider("OriginY");

		//m_Entity.lock()->getTransform()->m_Origin = util::Vec3(0, util::lerp<float>(originY, -m_pShape.lock()->originY(), m_pShape.lock()->originY()), 0);

#endif

		m_Offset = m_pShape.lock()->getOffset();

		moveVelocityOption();

		angleVelocityOption();

		btTransform trans = m_pRigidBody->getWorldTransform();
		auto myTrans = m_Entity.lock()->getTransform();

		//値が不正だったら反映させない
		if (myTrans->isPositionNan()) {
			return;
		}

		trans.setOrigin(convertBtVector3(myTrans->m_Position) - util::convertBtVector3(m_Offset));

		btQuaternion qrot;
		auto&& rotateOffset = m_pShape.lock()->getRotationOffset();
		qrot.setEuler(XMConvertToRadians(myTrans->m_Rotation.y - rotateOffset.y), XMConvertToRadians(myTrans->m_Rotation.x - rotateOffset.x), XMConvertToRadians(myTrans->m_Rotation.z - rotateOffset.z));
		trans.setRotation(qrot);

		//物理エンジンのトランスフォーム更新
		m_pRigidBody->setWorldTransform(trans);
	}

	void BulletRigidBody::updateEntityTrans()
	{
		btTransform trans = m_pRigidBody->getWorldTransform();

		auto pos = convertUtVector3(trans.getOrigin());
		//値が不正だったら反映させない
		if (_isnan(pos.x) | _isnan(pos.y) | _isnan(pos.z))return;

		auto myTrans = m_Entity.lock()->getTransform();
		btQuaternion qrot = trans.getRotation();

		util::Quaternion q(qrot.getX(), qrot.getY(), qrot.getZ(), qrot.getW());

		auto euler = util::quaternionToEuler(q);

		auto&& rotateOffset = m_pShape.lock()->getRotationOffset();

		//自分のエンジンのトランスフォーム更新
		myTrans->m_Rotation = euler + rotateOffset;

		myTrans->m_Position = pos + m_Offset;

	}

	void BulletRigidBody::setTrriger(bool isTrriger) {
		*m_Desc.isTrigger = isTrriger;
	}

	const bool BulletRigidBody::isTrigger()const
	{
		return *m_Desc.isTrigger;
	}

	void BulletRigidBody::moveVelocityOption()
	{
		auto moveVelocity = m_pRigidBody->getLinearVelocity();
		if (*(m_Desc.position.x)) {
			moveVelocity.setX(0);
		}
		if (*(m_Desc.position.y)) {
			moveVelocity.setY(0);
		}
		if (*(m_Desc.position.z)) {
			moveVelocity.setZ(0);
		}
		m_pRigidBody->setLinearVelocity(moveVelocity);
	}

	void BulletRigidBody::angleVelocityOption()
	{
		auto angleVelocity = m_pRigidBody->getAngularVelocity();

		if (*(m_Desc.rotation.x)) {
			angleVelocity.setX(0);
		}
		if (*(m_Desc.rotation.y)) {
			angleVelocity.setY(0);
		}
		if (*(m_Desc.rotation.z)) {
			angleVelocity.setZ(0);
		}
		m_pRigidBody->setAngularVelocity(angleVelocity);
	}

	void BulletRigidBody::setPositionFreezeFlag(FreezeElement flags)
	{
		m_Desc.position = flags;
	}

	void BulletRigidBody::setRotateFreezeFlag(FreezeElement flags)
	{
 		m_Desc.rotation = flags;

		//フリーズされてる軸の慣性を消す
		m_Inertia = m_OriginalIertia;
		if (*(m_Desc.rotation.x)) {
			m_Inertia.setX(0);
		}
		if (*(m_Desc.rotation.y)) {
			m_Inertia.setY(0);
		}
		if (*(m_Desc.rotation.z)) {
			m_Inertia.setZ(0);
		}

		//インテラル更新
		m_pRigidBody->setMassProps(*m_Desc.wight, m_Inertia);
	}

	BulletRigidBody::FreezeElement  BulletRigidBody::getPosFreezeFlag()
	{
		return *(m_Desc.position);
	}
	BulletRigidBody::FreezeElement  BulletRigidBody::getRotateFreezeFlag()
	{
		return *(m_Desc.rotation);
	}
	float BulletRigidBody::getDefOriginY()
	{
		return m_DefOriginY;
	}
	util::Vec3 BulletRigidBody::offsetConvertFront()
	{
		return XMVector3Transform(m_pShape.lock()->getOffset().toXMVector(), m_Entity.lock()->getTransform()->toRotateMatrix().toXMMatrix());
	}

	void BulletRigidBody::getLinearVelocity(util::Vec3& result) {
		result = util::convertUtVector3(m_pRigidBody->getLinearVelocity());
	}
	std::weak_ptr<PhysicsShape>& BulletRigidBody::getShape()
	{
		return m_pShape;
	}
	btRigidBody * BulletRigidBody::getBtRigidBody()
	{
		return m_pRigidBody.get();
	}
}