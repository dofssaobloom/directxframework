#include "Framework.h"
#include "BulletBoxCollider.h"
#include<Source\Util\btUtil.h>
#include<minmax.h>

namespace component {

	BulletBoxCollider::BulletBoxCollider()
	{
		m_CallOrder = 0;
	}

	BulletBoxCollider::~BulletBoxCollider()
	{
	}

	void BulletBoxCollider::setParam(const std::vector<std::string>& param)
	{
		m_IsScaleToColiderSize = false;
		PhysicsShape::setParam(param);
		if (param.size() < 3)return;
		if (param[2] == "")return;
		m_IsScaleToColiderSize = (bool)std::atoi(param[2].c_str());
	}

	void BulletBoxCollider::init()
	{
		PhysicsShape::init();

		//このフラグがあったらトランスフォームのスケールをコライダーのスケールとして初期化する
		if (m_IsScaleToColiderSize) {
			auto&& scale = m_Entity.lock()->getTransform()->m_Scale;

			m_Size.x = m_pUI.lock()->getSlider("SizeX") + scale.x;
			m_Size.y = m_pUI.lock()->getSlider("SizeY") + scale.y;
			m_Size.z = m_pUI.lock()->getSlider("SizeZ") + scale.z;
		}
		else {
			m_Size.x = m_pUI.lock()->getSlider("SizeX");
			m_Size.y = m_pUI.lock()->getSlider("SizeY");
			m_Size.z = m_pUI.lock()->getSlider("SizeZ");
		}
		createShape();
	}

	void BulletBoxCollider::getBoxSize(util::Vec3 * result)
	{
		result->x = m_Size.x;
		result->y = m_Size.y;
		result->z = m_Size.z;
	}

	util::Vec3 BulletBoxCollider::getOrigin()
	{
		return util::Vec3(0,m_Size.y/2,0);
	}

	float BulletBoxCollider::originY()
	{
		return m_Size.y;
	}

	void BulletBoxCollider::createViewCullingCollision(ViewCullingVertex& result)
	{
		util::Vec3 size = m_Size;

		////前4つ
		//result.pos[0] = util::Vec3(size.x, size.y, size.z);
		//result.pos[1] = util::Vec3(size.x, -size.y, size.z);
		//result.pos[2] = util::Vec3(-size.x, -size.y, size.z);
		//result.pos[3] = util::Vec3(-size.x, size.y, size.z);

		////後ろ4つ
		//result.pos[4] = util::Vec3(size.x, size.y, -size.z);
		//result.pos[5] = util::Vec3(size.x, -size.y, -size.z);
		//result.pos[6] = util::Vec3(-size.x, -size.y, -size.z);
		//result.pos[7] = util::Vec3(-size.x, size.y, -size.z);


		float half = max(size.x,max(size.y,size.z));

		result.pos[0] = util::Vec3(half, half * 2, half);
		result.pos[1] = util::Vec3(half, 0, half);
		result.pos[2] = util::Vec3(-half, 0, half);
		result.pos[3] = util::Vec3(-half, half * 2, half);

		//後ろ4つ
		result.pos[4] = util::Vec3(half, half * 2, -half);
		result.pos[5] = util::Vec3(half, 0, -half);
		result.pos[6] = util::Vec3(-half, 0, -half);
		result.pos[7] = util::Vec3(-half, half * 2, -half);

		//result.pos[0] = util::Vec3(0, 0, 0);
		//result.pos[1] = util::Vec3(0, 0, 0);
		//result.pos[2] = util::Vec3(0, 0, 0);
		//result.pos[3] = util::Vec3(0, 0, 0);

		////後ろ4つ
		//result.pos[4] = util::Vec3(0, 0, 0);
		//result.pos[5] = util::Vec3(0, 0, 0);
		//result.pos[6] = util::Vec3(0, 0, 0);
		//result.pos[7] = util::Vec3(0, 0, 0);
	}

	void BulletBoxCollider::createShape()
	{
		m_pCollision = std::make_unique<btBoxShape>(util::convertBtVector3(m_Size));
		m_pMotionState = std::make_unique<btDefaultMotionState>(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
	}

	void BulletBoxCollider::update()
	{
#ifdef _MDEBUG
		if (m_IsScaleToColiderSize) {
			auto&& scale = m_Entity.lock()->getTransform()->m_Scale;

			m_Size.x = m_pUI.lock()->getSlider("SizeX") + scale.x;
			m_Size.y = m_pUI.lock()->getSlider("SizeY") + scale.y;
			m_Size.z = m_pUI.lock()->getSlider("SizeZ") + scale.z;
		}
		else {
			m_Size.x = m_pUI.lock()->getSlider("SizeX");
			m_Size.y = m_pUI.lock()->getSlider("SizeY");
			m_Size.z = m_pUI.lock()->getSlider("SizeZ");
		}
		auto collision = dynamic_cast<btBoxShape*>(m_pCollision.get());
		collision->setImplicitShapeDimensions(util::convertBtVector3(m_Size));

#endif
	}



}