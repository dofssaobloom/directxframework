#include"PhysicsWorld.h"
#include<Source\Component\Render3DComponent.h>
#include<Source\Physics\DirectXDrawable.h>
#include<Source\Component\CameraComponent.h>
#include<Source\Component\Physics\BulletRigidBody.h>
#include<Source\Component\Physics\PhysicsComponent.h>
#include<Source\Entity\Entity.h>
#include<Source\Util\Event\WindowEvent.h>
#include<Include\BULLET\BulletCollision\Gimpact\btGImpactShape.h>

namespace component {


	PhysicsWorld::PhysicsWorld()
	{
	}

	PhysicsWorld::~PhysicsWorld()
	{
	}

	void PhysicsWorld::init()
	{
		//リセット時のバグ防止で同じイベントの重複を防ぐために一度削除を行う
		util::WindowEvent::getInstance()->removeEvent(ID_BULLET_DRAOW);
		util::WindowEvent::getInstance()->addEvent(ID_BULLET_DRAOW, [&](HWND hwnd) {

			if (util::WinFunc::isMenuCheck(hwnd, ID_BULLET_DRAOW)) {
				m_DrawFunc = [&]() {
					m_Bullet.pDrawable->setViewMat(component::CameraComponent::getMainCamera().lock()->toViewMatrix().toXMMatrix());

					m_Bullet.btWorld->debugDrawWorld();
				};
			}
			else {
				m_DrawFunc = []() {

				};
			}
		});

		//デフォルトで描画しない
		m_DrawFunc = [&]() {
		};

		m_IsPhysicsStop = false;
	}

	void PhysicsWorld::setParam(const std::vector<std::string>& param)
	{
		auto gravity = util::atof<util::Vec3>(param).reault;

		m_Bullet.btWorld->setGravity(btVector3(gravity.x, gravity.y, gravity.z));

		m_Bullet.pDrawable->setProjMat(XMMatrixTranspose(XMMatrixPerspectiveFovLH(
			XMConvertToRadians(framework::Screen::PERSPECTIVE),
			framework::Screen::WINDOW_WIDTH / (float)framework::Screen::WINDOW_HEIGHT,//floatし忘れ注意
			1.0f,
			framework::Screen::FAR_)));
	}

	void PhysicsWorld::addRigidBody(btRigidBody* rigidBody)
	{
		m_Bullet.btWorld->addRigidBody(rigidBody);
	}

	void PhysicsWorld::addCollisionObj(btCollisionObject * obj)
	{
		m_Bullet.btWorld->addCollisionObject(obj);
	}

	void PhysicsWorld::removeRigidBody(btRigidBody * rigidBody)
	{
		m_Bullet.btWorld->removeRigidBody(rigidBody);
	}

	void PhysicsWorld::removeCollisionObj(btCollisionObject * obj)
	{
		m_Bullet.btWorld->removeCollisionObject(obj);
	}

	void PhysicsWorld::draw()
	{
		m_DrawFunc();
		//m_Bullet.pDrawable->setViewMat(component::CameraComponent::getMainCamera().lock()->toViewMatrix().toXMMatrix());

		//m_Bullet.btWorld->debugDrawWorld();

	}

	void PhysicsWorld::physicsUpdate()
	{
		if (m_IsPhysicsStop)return;

		eachRigidBody([&](std::weak_ptr<BulletRigidBody> rigidBody) {
			rigidBody.lock()->updateRigidBodyTrans();
		});

		m_Bullet.btWorld->stepSimulation(1.f / 30.0f, 1);

		eachRigidBody([&](std::weak_ptr<BulletRigidBody> rigidBody) {
			rigidBody.lock()->updateEntityTrans();
		});
	}

	framework::PhysicsRaycast PhysicsWorld::rayCast(const util::Vec3 & begin, const util::Vec3 & end)
	{
#ifdef _MDEBUG
		drawLine(begin, end);
#endif

		framework::PhysicsRaycast res(begin, end);

		auto&& btBegin = util::convertBtVector3(begin);
		auto&& btEnd = util::convertBtVector3(end);

		m_Bullet.btWorld->rayTest(btBegin, btEnd, res);

		return res;
	}

	void PhysicsWorld::clear()
	{
		const int count = m_Bullet.btWorld->getNumCollisionObjects();

		util::reForeach(count, [&](int i) {

			auto objectArray = m_Bullet.btWorld->getCollisionObjectArray();

			auto size = objectArray.size();

			m_Bullet.btWorld->removeCollisionObject(objectArray[i]);
		});
	}

	bool PhysicsWorld::isContainsRigidBody(btRigidBody *inRigid)
	{
		auto collisionObjArray = m_Bullet.btWorld->getCollisionObjectArray();

		const int arrayNum = m_Bullet.btWorld->getNumCollisionObjects();

		bool isContains = false;
		util::foreach(arrayNum, [&](int i) {
			auto btRigid = btRigidBody::upcast(collisionObjArray[i]);
			if (inRigid == btRigid) {
				isContains = true;
				return;
			}
		});

		return isContains;
	}

	void PhysicsWorld::drawLine(const util::Vec3 & begin, const util::Vec3 & end)
	{
		m_Bullet.btWorld->getDebugDrawer()->drawLine(util::convertBtVector3(begin), util::convertBtVector3(end), btVector3(1, 0, 0));
	}

	void PhysicsWorld::activePhysics()
	{
		m_IsPhysicsStop = false;
	}

	void PhysicsWorld::deActivePhysics()
	{
		m_IsPhysicsStop = true;
	}

	void PhysicsWorld::eachRigidBody(std::function<void(std::weak_ptr<BulletRigidBody>)> action)
	{
		auto collisionObjArray = m_Bullet.btWorld->getCollisionObjectArray();

		const int arrayNum = m_Bullet.btWorld->getNumCollisionObjects();

		util::foreach(arrayNum, [&](int i) {
			auto btRigid = btRigidBody::upcast(collisionObjArray[i]);
			auto entity = (framework::Entity*)btRigid->getUserPointer();
			auto rigidBody = entity->getComponent<component::BulletRigidBody>();

			if (rigidBody.expired())
			{
				return;
			}
			action(rigidBody);
		});
	}

	bool PhysicsWorld::collisionCallBack(btManifoldPoint& cp, void* body0, void* body1)
	{
		btRigidBody* btBody0 = (btRigidBody*)body0;
		btRigidBody* btBody1 = (btRigidBody*)body1;

		//userPointerにはEntityがセットされているので取り出す
		auto entity0 = (framework::Entity*)btBody0->getUserPointer();
		auto entity1 = (framework::Entity*)btBody1->getUserPointer();

		auto component0 = entity0->getComponent<BulletRigidBody>();
		auto component1 = entity1->getComponent<BulletRigidBody>();

		//どちらかがphysicsコンポーネントがついていなければ処理しない
		if (component0.expired() || component1.expired())return false;


		util::Vec3 p1 = util::convertUtVector3(cp.getPositionWorldOnA());
		util::Vec3 p2 = util::convertUtVector3(cp.getPositionWorldOnB());


		component0.lock()->addHitEnter(component1, p1, util::convertUtVector3(cp.m_normalWorldOnB));
		component1.lock()->addHitEnter(component0, p2, util::convertUtVector3(cp.m_normalWorldOnB));

		return true;
	}

	//bool PhysicsWorld::collisionAddCallBack(btManifoldPoint & cp, const btCollisionObjectWrapper * colObj0Wrap, int partId0, int index0, const btCollisionObjectWrapper * colObj1Wrap, int partId1, int index1)
	//{
	//	////userPointerにはEntityがセットされているので取り出す
	//	//auto entity0 = (framework::Entity*)(colObj0Wrap->m_shape->getUserPointer());
	//	//auto entity1 = (framework::Entity*)(colObj1Wrap->m_shape->getUserPointer());

	//	//auto component0 = entity0->getComponent<framework::PhysicsComponent>();
	//	//auto component1 = entity1->getComponent<framework::PhysicsComponent>();

	//	////どちらかがphysicsコンポーネントがついていなければ処理しない
	//	//if (component0.expired() || component1.expired())return false;

	//	//component0.lock()->addHitEnter(component1);
	//	//component1.lock()->addHitEnter(component0);

	//	//auto d = cp.getDistance();

	//	//auto a = cp.getPositionWorldOnA() - cp.getPositionWorldOnB();
	//	//util::Vec3 vec = util::convertUtVector3(a.normalize());

	//	//auto dot = vec.dot(util::Vec3(0,1,0));


	//	//entity0->getComponent<PhysicsShape>().lock()->getShape()->

	//	//
	//	//btGImpactMeshShape *shape = static_cast<btGImpactMeshShape*>(entity0->getComponent<PhysicsShape>().lock()->getShape());

	//	//btTriangleShapeEx triangle;
	//	//shape->getBulletTriangle(index0, triangle);
	//	//btVector3 vert;
	//	//for (int i = 0; i<3; ++i)
	//	//{
	//	//	triangle.getVertex(i, vert);
	//	//	std::cout << vert.getX() << " " << vert.getY() << " " << vert.getZ() << std::endl;
	//	//}

	//	return true;
	//}

	void PhysicsWorld::checkDelete()
	{
	}

	PhysicsWorld::Bullet::Bullet()
	{
		pDrawable = std::make_unique<framework::DirectXDrawable>();
		pDrawable->setDebugMode(btIDebugDraw::DBG_DrawWireframe);

		pCollisionConfig = std::make_unique<btDefaultCollisionConfiguration>();
		pDispatcher = std::make_unique<btCollisionDispatcher>(pCollisionConfig.get());
		// Broadphase
		// 衝突する可能性のあるオブジェクト同士を検出する処理
		// アルゴリズムを選ぶ
		// 通常はbtDbvtBroadphase(Dynamic AABB Tree)でOK
		// うまくいかない場合はbtAxis3Sweep(Sweep and Prune)を試してみる
		pBroadphase = std::make_unique<btDbvtBroadphase>();

		//// constraint solver（拘束条件の解法）
		//// オブジェクト同士が重ならないなどの拘束条件を解決する方法
		pSolver = std::make_unique<btSequentialImpulseConstraintSolver>();

		btWorld = std::make_unique<btDiscreteDynamicsWorld>(pDispatcher.get(), pBroadphase.get(),
			pSolver.get(), pCollisionConfig.get());

		btWorld->setDebugDrawer(pDrawable.get());

		gContactProcessedCallback = collisionCallBack;

		//gContactAddedCallback = collisionAddCallBack;
	}

	PhysicsWorld::Bullet::~Bullet()
	{
		btWorld->setDebugDrawer(nullptr);
	}

}