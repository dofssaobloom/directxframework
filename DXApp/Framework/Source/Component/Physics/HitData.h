#pragma once
#include<Source\Util\Type.h>

namespace framework {

	class PhysicsComponent;
	class Entity;

	struct HitData
	{
		HitData(std::weak_ptr<Entity> other, bool isHit, util::Vec3 hitPosA, util::Vec3 hitPosB) {
			this->other = other;
			this->isHit = isHit;
			this->hitPosA = hitPosA;
			this->hitPosB = hitPosB;
		}

		HitData(const HitData& other) {
			this->other = other.other;
			this->isHit = other.isHit;
			this->hitPosA = other.hitPosA;
			this->hitPosB = other.hitPosB;
		}

		std::weak_ptr<Entity> other;
		bool isHit;
		util::Vec3 hitPosA;
		util::Vec3 hitPosB;
	};

}