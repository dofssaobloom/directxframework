#pragma once
#include"Selector.h"
#include<functional>

namespace util {

	class MenuVector
	{
	public:
		MenuVector();
		~MenuVector();

		void init();
		void init(int namber);

		/// <summary>
		/// メニュー追加
		/// </summary>
		void addMenu(std::function<void()> selectAction);

		/// <summary>
		/// 選択項目の関数実行
		/// </summary>
		void applay();

		void enter();

		void back();

		/// <summary>
		/// 選択されているインデックス取得
		/// </summary>
		/// <returns></returns>
		const int getCount()const;

	private:

		//!メニュー項目
		std::unique_ptr<Selector> m_pMenuElement;
		std::vector<std::function<void()>> m_MenuFunction;
	};
}