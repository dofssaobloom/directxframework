#pragma once
#include<windows.h>
#include<string>
#include<Source\Application\Application.h>

/// <summary>
/// windowsAPIラッパークラス
/// </summary>
namespace util {
	class WinFunc
	{
	public:

		~WinFunc() {};

		/// <summary>
		/// ウィンドウハンドルを指定してメッセージウィンドウを出す
		/// </summary>
		/// <param name="hWnd"></param>
		/// <param name="title"></param>
		/// <param name="message"></param>
		static void drawMessageBox(HWND hWnd, const std::string& title, const std::string& message) {
			MessageBox(hWnd, (LPCSTR)(message.c_str()), (LPCSTR)(title.c_str()), MB_ICONINFORMATION);
		}

		/// <summary>
		/// アプリケーションのウィンドウハンドルを取得してメッセージウィンドウを出す
		/// </summary>
		/// <param name="title"></param>
		/// <param name="message"></param>
		static void drawMessageBox(const std::string& title, const std::string& message) {
			HWND hWnd = FindWindow(framework::Application::m_WindowName.c_str(), NULL);
			if (hWnd == NULL)return;//ウィンドウハンドルが見つからなければ何もしない
			MessageBox(hWnd, (LPCSTR)(message.c_str()), (LPCSTR)(title.c_str()), MB_ICONINFORMATION);
		}

		static bool drawQuestion(HWND hWnd, LPCSTR title, LPCSTR message) {
			if (MessageBox(hWnd, message, title, MB_YESNO | MB_ICONQUESTION) == IDYES) {
				//IDYESだったときのみtrueが返る
				return true;
			}
			return false;
		}
		
		/**
		* @brief			イミディエイトウィンドウに文字列を表示する　
		* @param msg		出直したい文字列
		* @detail			デバッグなしの場合は出力されない
		*/
		static void outLog(const std::string& msg) {
			std::string temp = msg + "\n";
			OutputDebugString(temp.c_str());
		}

		
		/// <summary>
		/// メニューのチェックボックスにチェックが入っているかどうか
		/// </summary>
		/// <param name="hWnd">ウィンドウハンドル</param>
		/// <param name="id">ItemのID</param>
		/// <returns></returns>
		static bool isMenuCheck(const HWND& hWnd,const int& id) {
			auto hMenu = GetMenu(hWnd);
			auto state = GetMenuState(hMenu,id,MF_BYCOMMAND);
			
			return state & MFS_CHECKED;
		}

		//メニューのチェックを反転する
		static void switchCheckItem(const HWND& hWnd, const int& id) {
			auto hMenu = GetMenu(hWnd);
			if (isMenuCheck(hWnd,id)) {
				CheckMenuItem(hMenu, id, MF_BYCOMMAND | MFS_UNCHECKED);
			}
			else {
				CheckMenuItem(hMenu, id, MF_BYCOMMAND | MFS_CHECKED);
			}
		}
	


	private:
		WinFunc() {};
	};
}