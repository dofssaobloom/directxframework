#pragma once


namespace util {

	class Timer
	{
	public:
		Timer();

		//何フレームを一秒にするか
		Timer(int time);

		~Timer();

		//コンストラクタの値に戻す
		void init();

		void init(int time);

		void update();

		//1秒終わったかどうか
		bool isEnd()const;

		//経過時間の取得
		float rate()const;

		int getTime();

		//初期設定時間取得
		const int maxTime()const;

	private:
		//const short m_MaxTime;
		short m_MaxTime;
		short m_CurrentTime;
	};


}