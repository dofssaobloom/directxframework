#pragma once
#include<Source\Util\Lua\LuaBase.h>
#include<string>

namespace util {

	/// <summary>
	/// C++メソッドをサポートするLuaインスタンス
	/// </summary>
	class LuaCpp : public LuaBase
	{

	public:
		LuaCpp(const std::string& filePath);
		~LuaCpp();

		template<typename ...Param>
		void callVoidFunc(const std::string& methodName, Param ...param) {
			luabind::call_function<void>(m_pLuaState, methodName.c_str(), param...);
		}

		template<typename T,typename ...Param>
		T callFunc(const std::string& methodName, Param ...param) {
			return luabind::call_function<T>(m_pLuaState, methodName.c_str(),param...);
		}

	private:
	};
}