#include "Framework.h"
#include "LuaCpp.h"
#include"luaSetting.h"

namespace util {

	LuaCpp::LuaCpp(const std::string& filePath)
	{
		//!luaインスタンス生成
		m_pLuaState = luaL_newstate();

		//!Lua標準ライブラリ追加
		luaL_openlibs(m_pLuaState);

		luabind::open(m_pLuaState);

		util::includeAll(m_pLuaState);

		if (luaL_dofile(m_pLuaState, filePath.c_str()))
		{
#ifdef _DEBUG
			//	std::cout << lua_tostring(m_pLuaState, -1) << std::endl;
			util::WinFunc::outLog(lua_tostring(m_pLuaState, -1));
			assert(false && "ルアー読み込み失敗しました");
#endif
		}
	}

	LuaCpp::~LuaCpp()
	{
	}

}
