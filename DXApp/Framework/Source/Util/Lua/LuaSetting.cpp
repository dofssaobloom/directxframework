#include "Framework.h"
#include "LuaSetting.h"
#include<Source\VeryUse.h>
#include<Source\Util\ImGUI\ImGUI.h>
#include<Source\Component\UI\GUIComponent.h>

namespace util {

	void includeAll(lua_State * l)
	{
		includeVector(l);
		includeImGUI(l);
	}

	void includeImGUI(lua_State * l) {
		//luabind::module(l)[
		//	luabind::class_<ImGUI>("ImGUI")
		//		.def("text", &ImGUI::text)
		//		.def("slider", &ImGUI::slider)
		//		.def("colorEditor", &ImGUI::colorEditor)
		//		.def("button", &ImGUI::button)
		//];

		luabind::module(l)[
			luabind::class_ < component::GUIComponent >("GUIComponent")
				.def("addText", &component::GUIComponent::addText)
				//.def("addSlider", &component::GUIComponent::addSlider)
				//.def("addSlider", static_cast<void(component::GUIComponent::*)(const std::string& text, const util::Vec2& range)>(&component::GUIComponent::addSlider))
				.def("addSlider", static_cast<void(component::GUIComponent::*)(const std::string& text, const util::Vec2& range,const float initNum)>(&component::GUIComponent::addSlider))
				//.def("addColorEditor", static_cast<void(component::GUIComponent::*)(const std::string & text)>(&component::GUIComponent::addColorEditor))
				.def("addColorEditor", static_cast<void(component::GUIComponent::*)(const std::string & text, const util::Vec4 & initNum)>(&component::GUIComponent::addColorEditor))
				.def("addButton", &component::GUIComponent::addButton)
				.def("addCheckBox", &component::GUIComponent::addCheckBox)
		];

	}

	void includeVector(lua_State * l)
	{
		luabind::module(l)[
			luabind::class_<Vec2>("Vec2")
				.def(luabind::constructor<>())
				.def(luabind::constructor<float, float>())
				.def(luabind::constructor<Vec2&>())
				.def("normalize", &Vec2::normalize)
				.def("dot", &Vec2::dot)
				.def("length", &Vec2::length)
				.def_readwrite("x",&Vec2::x)
				.def_readwrite("y", &Vec2::y)

				
				.def(luabind::self + float())
				.def(luabind::self - float())
				.def(luabind::self * float())
				.def(luabind::self / float())
		];

		luabind::module(l)[
			luabind::class_<Vec3>("Vec3")
				.def(luabind::constructor<>())
				.def(luabind::constructor<float, float, float>())
				.def(luabind::constructor<Vec3&>())
				.def("normalize", &Vec3::normalize)
				.def("dot", &Vec3::dot)
				.def("length", &Vec3::length)
				.def_readwrite("x", &Vec3::x)
				.def_readwrite("y", &Vec3::y)
				.def_readwrite("z", &Vec3::z)
		];

		luabind::module(l)[
			luabind::class_<Vec4>("Vec4")
				.def(luabind::constructor<>())
				.def(luabind::constructor<float, float, float, float>())
				.def(luabind::constructor<Vec4&>())
				.def("normalize", &Vec4::normalize)
				.def_readwrite("x", &Vec4::x)
				.def_readwrite("y", &Vec4::y)
				.def_readwrite("z", &Vec4::z)
				.def_readwrite("w", &Vec4::w)
		];
	}

}