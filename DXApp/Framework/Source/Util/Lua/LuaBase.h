#pragma once

#include<string>
#include<assert.h>
#include<functional>
#include<Source\Util\Win\WinFunc.h>
#include<Source\VeryUse.h>


namespace util {

	/// <summary>
	/// 最低限のLuaの機能をサポートする
	/// </summary>
	class LuaBase
	{
	protected:

		template<typename T>
		struct Param;

		template<>
		struct Param<std::string> {
			Param(lua_State* L) {
				result = lua_tostring(L, -1);
			}

			std::string result;
		};

		template<>
		struct Param<int> {
			Param(lua_State* L) {
				result = lua_tointeger(L, -1);
			}

			int result;
		};

		template<>
		struct Param<float> {
			Param(lua_State* L) {
				result = (float)lua_tonumber(L, -1);
			}

			float result;
		};

		template<>
		struct Param<double> {
			Param(lua_State* L) {
				result = (double)lua_tonumber(L, -1);
			}

			double result;
		};


		template<>
		struct Param<bool> {
			Param(lua_State* L) {
				result = lua_toboolean(L, -1);
			}

			bool result;
		};



	public:
		LuaBase(const std::string& filePath) {
			//!luaインスタンス生成
			m_pLuaState = luaL_newstate();

			//!Lua標準ライブラリ追加
			luaL_openlibs(m_pLuaState);

			luabind::open(m_pLuaState);

			if (luaL_dofile(m_pLuaState, filePath.c_str()))
			{
#ifdef _MDEBUG
			//	std::cout << lua_tostring(m_pLuaState, -1) << std::endl;
				util::WinFunc::outLog(lua_tostring(m_pLuaState, -1));
				assert(false && "ルアー読み込み失敗しました");
#endif
			}
			
		}

		virtual ~LuaBase() {
			//luaインスタンス破棄
			lua_close(m_pLuaState);
		}


		template<typename T>
		T getParam(const std::string& name) {
			lua_getglobal(m_pLuaState, name.c_str());

			return Param<T>(m_pLuaState).result;
		}

	protected:
		LuaBase() {
		}

	protected:
		lua_State* m_pLuaState;

	};

}