#include"WrapFunc.h"
#include<memory>
#include<sstream>
#include<iostream>

namespace util {


	std::wstring StringToWString(const std::string & src)
	{
		std::wstring dest;

		wchar_t* a;
		std::unique_ptr<wchar_t[]> pWcs = std::make_unique<wchar_t[]>(src.length() + 1);
		size_t ret;
		mbstowcs_s(&ret,pWcs.get(), src.length() + 1,src.c_str(), _TRUNCATE);
		dest = pWcs.get();
		return dest;
	}

	void split(const std::string str, const char delimiter, std::vector<std::string>* buffer)
	{
		std::stringstream ss(str);
		std::string item;

		while (std::getline(ss,item, delimiter))
		{
			(*buffer).emplace_back(item);
		}
	}

	std::string extensionName(const std::string & filePath)
	{
		std::vector<std::string> splitBuffer1;

		//ファイルパスを'/'ごとに分ける
		util::split(filePath, '/', &splitBuffer1);

		std::vector<std::string> splitBuffer2;

		util::split(*(splitBuffer1.end() - 1), '.', &splitBuffer2);

		return splitBuffer2[1];
	}
	

}