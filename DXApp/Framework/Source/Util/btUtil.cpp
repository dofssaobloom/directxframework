#include "Framework.h"
#include "btUtil.h"

btVector3 util::convertBtVector3(const util::Vec3 & vec3)
{
	return btVector3(vec3.x, vec3.y, vec3.z);
}

util::Vec3 util::convertUtVector3(const btVector3 & vec3)
{
	return util::Vec3(vec3.getX(), vec3.getY(), vec3.getZ());
}

btVector3 util::convertBtRotate(const btQuaternion & inQuaternion)
{
	btMatrix3x3 mat(inQuaternion);

	util::Mat4 m(
		mat[0][0], mat[0][1], mat[0][2], 0,
		mat[1][0], mat[1][1], mat[1][2], 0,
		mat[2][0], mat[2][1], mat[2][2], 0,
		0, 0, 0, 1
	);

	util::Vec3 result;
	util::convertMatToRotateVec(m, &result);

	return btVector3(result.x, result.y, result.z);



	//util::Vec4 qrot(inQuaternion.getX(), inQuaternion.getY(), inQuaternion.getZ(),0);
	//
	//if (qrot.x == 0 && qrot.y == 0 && qrot.z == 0)return btVector3();

	//float ang = inQuaternion.getW();
	//auto a = XMLoadFloat4(&qrot);
	//auto rotate = XMQuaternionRotationAxis(a, ang);
	//auto mat = XMMatrixRotationQuaternion(rotate);
	//
	//util::Vec3 result;
	//util::convertMatToRotateVec(mat,&result);

	//return btVector3(result.x, result.y, result.z);
}

btQuaternion util::convertBtQuaternion(const XMVECTOR & xmQuaternion)
{
	return btQuaternion(xmQuaternion.x, xmQuaternion.y, xmQuaternion.z, xmQuaternion.w);
}

XMVECTOR util::convertUtQuaternion(const btQuaternion & btQuaternion)
{
	return util::Vec4(btQuaternion.getX(), btQuaternion.getY(), btQuaternion.getZ(), btQuaternion.getW()).toXMVector();
}
