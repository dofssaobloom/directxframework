#include<Source\VeryUse.h>
#include "ImSlider.h"
#include<Source\Util\ImGUI\ImGUI.h>
#include<minmax.h>

namespace util {

	ImSlider::ImSlider(const std::string text, const util::Vec2& range, float initNum)
		:IImItem(text),
		m_Range(range)
	{
		m_SliderNum = max(range.x, initNum);
	}

	ImSlider::~ImSlider()
	{
	}

	void ImSlider::draw(const ImGUI* ui)
	{
		ui->slider(m_Name, &m_SliderNum, m_Range);
	}

	float * ImSlider::valuePointer()
	{
		return &m_SliderNum;
	}

	float ImSlider::constValue()
	{
		return m_SliderNum;
	}

	void ImSlider::getWriteData(std::vector<float>* sliderBuffer) const
	{
		float id = std::hash<std::string>{}(name());
		(*sliderBuffer).emplace_back(id);
		(*sliderBuffer).emplace_back(m_SliderNum);
	}
}