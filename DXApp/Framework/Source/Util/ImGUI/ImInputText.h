#pragma once
#include<Source\Util\ImGUI\IImItem.h>

namespace util {

	/// <summary>
	/// ImGUI文字	入力クラス
	/// </summary>
	class ImInputText : public IImItem
	{
	public:
		ImInputText(const std::string& text, const std::string& defaultText);
		~ImInputText();

		/// <summary>
		/// テキスト取得
		/// </summary>
		/// <returns></returns>
		std::string getText();

		// IImItem を介して継承されました
		virtual void draw(const ImGUI * ui) override;

	private:

		std::unique_ptr<char[]> m_pBuffer;

	};
}
