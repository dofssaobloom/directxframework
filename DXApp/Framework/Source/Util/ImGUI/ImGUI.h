#pragma once

#include<Windows.h>
#include<string>
#include<Source\Util\Type.h>


struct ImVec4;


/// <summary>
/// ImGUIのラッパークラス
/// </summary>
/// どの関数も呼ばれたフレームしかUI項目を表示しないので注意
namespace util {

	class ImGUI
	{
	public:

		/// <summary>
		/// API初期化
		/// </summary>
		static void initAPI(HWND hWind);

		/// <summary>
		/// API終了
		/// </summary>
		static void endAPI();

		/// <summary>
		/// UIをすべて描画
		/// </summary>
		static void allDraw();

		/// <summary>
		/// UIの一斉更新
		/// </summary>
		/// 必ず毎フレーム一回呼ばなくてはならない
		static void newFrame();

		/// <summary>
		/// マウスを使えるようにする
		/// </summary>
		/// <param name="hWnd"></param>
		/// <param name="msg"></param>
		/// <param name="wParam"></param>
		/// <param name="lParam"></param>
		static bool updateMouseHandle(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

		/// <summary>
		/// コンストラクタ
		/// </summary>
		/// <param name="name">識別するための名前</param>
		/// <param name="pos">初期座標</param>
		/// <param name="size">初期サイズ</param>
		ImGUI(const std::string& name, const util::Vector2& pos = util::Vector2(0, 0), const util::Vec2& size = util::Vec2(500, 500));
		~ImGUI();

		/// <summary>
		/// UIの設定開始
		/// </summary>
		void begin();

		/// <summary>
		/// UIの設定終了
		/// </summary>
		void end();

		/// <summary>
		/// テキストを追加する
		/// </summary>
		/// <param name="message"></param>
		void text(const std::string & message) const;


		/// <summary>
		/// スライダー追加
		/// </summary>
		/// <param name="name">表示する文字</param>
		/// <param name="result">スライダーの値を格納する変数のアドレス</param>
		/// <param name="range">範囲</param>
		void slider(const std::string& name, float* result, const util::Vector2& range = util::Vector2(0, 1))const;

		/// <summary>
		/// カラーパレット追加
		/// </summary>
		/// <param name="name">表示名前</param>
		/// <param name="result">パレットの色を格納する変数のアドレス</param>
		void colorEditor(const std::string& name, ImVec4* result)const;

		/// <summary>
		/// ボタン追加
		/// </summary>
		/// <param name="name">表示する名前</param>
		/// 毎フレーム呼ばれないと表示され続けない
		bool button(const std::string& name)const;

		/// <summary>
		/// プルダウンメニュー
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		bool callapsingHeader(const std::string& name)const;

		/// <summary>
		/// チェックボックス
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		bool checkBox(const std::string& name, bool* result)const;

		/// <summary>
		/// ポジションセット
		/// </summary>
		/// <param name="pos"></param>
		void setPositoin(const util::Vec2& pos);

		/// <summary>
		/// スケールセット
		/// </summary>
		/// <param name="pos"></param>
		void setScale(const util::Vec2& scale);

		/// <summary>
		/// 横に次のアイテムを改行せずに表示する
		/// </summary>
		void sameLine();

		/// <summary>
		/// 文字入力
		/// </summary>
		/// <param name="buffer"></param>
		void inputText(const std::string& name,char* buffer, size_t size)const;

	protected:
		//!UIを識別するための名前
		std::string m_Name;
		//ウィンドウ座標
		util::Vector2 m_Position;
		//ウィンドウサイズ
		util::Vector2 m_Size;

		//描画するかどうかのフラグ
		static bool m_IsDraw;
	};


}