#include "Framework.h"
#include "ImCheckBox.h"

namespace util {

	ImCheckBox::ImCheckBox(const std::string& text,bool initNum)
		:IImItem(text),m_IsCheck(initNum)
	{
	}

	ImCheckBox::~ImCheckBox()
	{
	}
	void ImCheckBox::draw(const ImGUI * ui)
	{
		ui->checkBox(m_Name,&m_IsCheck);
	}
	void ImCheckBox::getWriteData(std::vector<float>* checkBuffer) const
	{
		float id = std::hash<std::string>{}(name());
		(*checkBuffer).emplace_back(id);
		(*checkBuffer).emplace_back((int)m_IsCheck);
	}
	bool* ImCheckBox::isCheckPointer()
	{
		return &m_IsCheck;
	}
	bool ImCheckBox::isCheckConstValue()
	{
		return m_IsCheck;
	}
}