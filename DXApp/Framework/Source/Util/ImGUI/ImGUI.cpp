#include "Framework.h"
#include "ImGUI.h"
#include <imgui.h>
#include<imgui_impl_dx11.h>
#include<Source\Util\Render\DXFunc.h>


using namespace ImGui;

#ifdef _MDEBUG
bool util::ImGUI::m_IsDraw = true;
#else
bool util::ImGUI::m_IsDraw = false;
#endif

void util::ImGUI::initAPI(HWND hWind)
{	
	CreateContext();
	//IMGUI初期化
	ImGui_ImplDX11_Init(hWind, util::getDevice(), util::getContext());
	StyleColorsClassic();

	//Relase時は登録しない
#ifdef _MDEBUG
	util::WindowEvent::getInstance()->addEvent(ID_GUI, [&](HWND hwnd) {
		m_IsDraw = util::WinFunc::isMenuCheck(hwnd, ID_GUI);
	});
	m_IsDraw = util::WinFunc::isMenuCheck(hWind, ID_GUI);
#else
	m_IsDraw = false;
#endif
}

void util::ImGUI::endAPI()
{
	//IMGUI終了
	ImGui_ImplDX11_Shutdown();
}

util::ImGUI::ImGUI(const std::string & name, const util::Vector2& pos, const util::Vec2& size)
	:m_Name(name),
	m_Position(pos),
	m_Size(size)
{
}

util::ImGUI::~ImGUI()
{
}

void util::ImGUI::begin()
{
	if (!m_IsDraw)return;

	auto& style = GetStyle();
	style.GrabRounding = style.FrameRounding = 8;
	SetNextWindowSize(ImVec2(m_Size.x, m_Size.y), ImGuiCond_Always);
	SetNextWindowPos(ImVec2(m_Position.x, m_Position.y), ImGuiCond_Always);
	Begin(m_Name.c_str());
}

void util::ImGUI::end()
{
	if (!m_IsDraw)return;
	End();
}


void util::ImGUI::text(const std::string & message)const
{
	if (!m_IsDraw)return;
	Text(message.c_str());
}

void util::ImGUI::slider(const std::string & name, float * result, const util::Vector2 & range)const
{
	if (!m_IsDraw)return;
	SliderFloat(name.c_str(), result, range.x, range.y);
}


void util::ImGUI::colorEditor(const std::string& name, ImVec4* result)const
{
	if (!m_IsDraw)return;
	ColorEdit3(name.c_str(), (float*)result);
}

bool util::ImGUI::button(const std::string & name)const
{
	if (!m_IsDraw)return false;
	return Button(name.c_str());
}

bool util::ImGUI::callapsingHeader(const std::string & name) const
{
	if (!m_IsDraw)return false;
	return CollapsingHeader(name.c_str());
}

bool util::ImGUI::checkBox(const std::string & name,bool* result)const
{
	if (!m_IsDraw)return false;
	return Checkbox(name.c_str(), result);
}

void util::ImGUI::setPositoin(const util::Vec2 & pos)
{
	m_Position = pos;
}

void util::ImGUI::setScale(const util::Vec2 & scale)
{
	m_Size = scale;
}

void util::ImGUI::sameLine()
{
	if (!m_IsDraw)return;
	SameLine();
}

void util::ImGUI::inputText(const std::string& name,char* buffer,size_t size)const
{
	if (!m_IsDraw)return;
	ImGui::InputText(name.c_str(), buffer, size);
}


void util::ImGUI::allDraw()
{
	if (!m_IsDraw)return;
	Render();

	ImGui_ImplDX11_RenderDrawData(GetDrawData());
}

void util::ImGUI::newFrame()
{
	if (!m_IsDraw)return;
	ImGui_ImplDX11_NewFrame();
}

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

bool util::ImGUI::updateMouseHandle(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	return ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam);
}
