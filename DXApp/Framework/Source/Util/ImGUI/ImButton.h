#pragma once
#include<Source\Util\ImGUI\IImItem.h>
#include<functional>

namespace util {

	/// <summary>
	/// ボタンアクション
	/// luaを意識してるのでなるべく機能を持たせない
	/// </summary>
	class ImButton : public IImItem
	{
	public:
		ImButton(const std::string& text);
		ImButton(const std::string& text, std::function<void()>& callFunc);
		~ImButton();


		void draw(const ImGUI* ui)override;

		bool onClick()const;

	private:
		bool m_OnClick;
		std::function<void()> m_Function;
	};
}