#include "Framework.h"
#include "ImButton.h"

util::ImButton::ImButton(const std::string& text)
	:IImItem(text)
	, m_OnClick(false)
{
	m_Function = []() {};
}

util::ImButton::ImButton(const std::string & text, std::function<void()>& callFunc)
	:IImItem(text)
	, m_OnClick(false)
{
	m_Function = callFunc;
}

util::ImButton::~ImButton()
{
}

void util::ImButton::draw(const ImGUI * ui)
{
	m_OnClick = ui->button(m_Name);
	if (m_OnClick) {
		m_Function();
	}
}

bool util::ImButton::onClick() const
{
	return m_OnClick;
}
