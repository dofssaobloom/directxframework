#pragma once
#include<Source\Util\ImGUI\IImItem.h>

namespace util {

	class ImText : public IImItem
	{
	public:
		ImText(const std::string& text);
		~ImText();

		/// <summary>
		/// テキストを書き換える場合これを使う
		/// </summary>
		/// <returns></returns>
		std::string* getTextPointer();

		void draw(const ImGUI* ui) override;

	private:

	};
}