#include "Framework.h"
#include "ImInputText.h"
#include<memory>

namespace util {

	ImInputText::ImInputText(const std::string& text, const std::string& defaultText = "empty" )
		:IImItem(text)
	{
		m_pBuffer = std::make_unique<char[]>(32);

		for (size_t i = 0; i < defaultText.size(); i++)
		{
			m_pBuffer[i] =  defaultText[i];
		}	
	}

	ImInputText::~ImInputText()
	{
	}

	std::string ImInputText::getText()
	{
		return std::string(m_pBuffer.get());
	}

	void ImInputText::draw(const ImGUI * ui)
	{
		char* buf = m_pBuffer.get();
		ui->inputText(m_Name,buf,32);

	}

}