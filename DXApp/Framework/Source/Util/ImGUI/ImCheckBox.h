#pragma once
#include"IImItem.h"

namespace util {

	class ImCheckBox : public IImItem
	{
	public:
		ImCheckBox(const std::string& text,bool initNum);
		~ImCheckBox();

		// IImItem を介して継承されました
		virtual void draw(const ImGUI * ui) override;

		/// <summary>
		/// 書き出し用データの用意
		/// </summary>
		void getWriteData(std::vector<float>* check)const;

		/// <summary>
		/// 参照を取得
		/// </summary>
		/// <returns></returns>
		bool* isCheckPointer();

		/// <summary>
		/// ディープコピーされた値を取得
		/// </summary>
		/// <returns></returns>
		bool isCheckConstValue();

	private:

		bool m_IsCheck;

	};

}