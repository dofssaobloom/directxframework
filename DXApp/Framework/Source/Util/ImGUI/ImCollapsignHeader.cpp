#include "Framework.h"
#include "ImCollapsignHeader.h"
#include<Source\Util\ImGUI\ImGUI.h>


namespace util {

	ImCollapsignHeader::ImCollapsignHeader(const std::string& text)
		:IImItem(text)
	{
	
	}

	ImCollapsignHeader::~ImCollapsignHeader()
	{
	}

	void ImCollapsignHeader::draw(const ImGUI * ui)
	{
		if (ui->callapsingHeader(m_Name)) {
			for (auto& item : m_Items)
			{
				item->draw(ui);
			}
		}
	}

	void ImCollapsignHeader::addItem(std::shared_ptr<util::IImItem> item)
	{
		m_Items.emplace_back(item);
	}


}
