#pragma once
#include<Source\Util\ImGUI\IImItem.h>
#include<functional>

namespace util {

	/// <summary>
	/// プルダウン項目
	/// </summary>
	class ImCollapsignHeader : public IImItem
	{
	public:
		ImCollapsignHeader(const std::string& text);
		~ImCollapsignHeader();

		virtual void draw(const ImGUI* ui)override;

		/// <summary>
		/// アイテム追加
		/// </summary>
		/// <param name="item"></param>
		void addItem(std::shared_ptr<util::IImItem> item);

		/// <summary>
		/// textの一致するアイテムを取得
		/// </summary>
		template<typename T>
		std::weak_ptr<T> getItem(const std::string& text) {

			auto find = std::find_if(m_Items.begin(), m_Items.end(), [&](std::shared_ptr<util::IImItem>& item) {
				return item->name() == text;
			});

			return std::static_pointer_cast<T>(*find);
		}

	private:
		//アクティブになったとき表示されるUI
		std::vector<std::shared_ptr<util::IImItem>> m_Items;

	};


}