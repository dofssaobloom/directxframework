#pragma once
#include<string>

namespace util {

	class ImGUI;

	/// <summary>
	/// ImGUI用アイテムインターフェース
	/// </summary>
	class IImItem
	{
	public:
		IImItem(const std::string& name)
			:m_Name(name)
		{}
		~IImItem() {}

		virtual void draw(const ImGUI* ui) = 0;

		const std::string& name()const;
	protected:
		std::string m_Name;
	};
}