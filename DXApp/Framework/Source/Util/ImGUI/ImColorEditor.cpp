#include "Framework.h"
#include "ImColorEditor.h"

util::ImColorEditor::ImColorEditor(const std::string & text,
	const util::Vec4& initNum)
	:IImItem(text),m_Color(initNum)
{
}

util::ImColorEditor::~ImColorEditor()
{
}

void util::ImColorEditor::draw(const ImGUI * ui)
{
	ui->colorEditor(m_Name, (ImVec4*)&m_Color);
}

util::Vec4* util::ImColorEditor::valuePointer()
{
	return &m_Color;
}

const util::Vec4& util::ImColorEditor::constValue()
{
	return m_Color;
}

void util::ImColorEditor::getWriteData(std::vector<float>* colorBuffer) const
{
	float id = std::hash<std::string>{}(name());
	(*colorBuffer).emplace_back(id);
	(*colorBuffer).emplace_back(m_Color.x);
	(*colorBuffer).emplace_back(m_Color.y);
	(*colorBuffer).emplace_back(m_Color.z);
	(*colorBuffer).emplace_back(m_Color.w);
}
