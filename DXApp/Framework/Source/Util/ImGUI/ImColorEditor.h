#pragma once
#include<Source\Util\ImGUI\IImItem.h>
#include <imgui.h>
#include<imgui_impl_dx11.h>
#include<Source\Util\Type.h>

namespace util {

	/// <summary>
	/// UIカラーエディター
	/// </summary>
	class ImColorEditor : public IImItem
	{
	public:
		ImColorEditor(const std::string& text,const util::Vec4& initNum = util::Vec4());
		~ImColorEditor();

		void draw(const ImGUI* ui) override;

		/// <summary>
		/// カラーのアドレス取得
		/// </summary>
		/// <returns></returns>
		util::Vec4* valuePointer();

		/// <summary>
		/// ディープコピーされた値取得
		/// </summary>
		/// <returns></returns>
		const util::Vec4& constValue();

		/// <summary>
		/// 書き出し用データの用意
		/// </summary>
		void getWriteData(std::vector<float>* colorBuffer)const;
	private:
		util::Vec4 m_Color;

	};
}