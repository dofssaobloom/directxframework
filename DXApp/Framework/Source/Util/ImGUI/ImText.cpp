#include "Framework.h"
#include "ImText.h"

util::ImText::ImText(const std::string& text)
	:IImItem(text)
{
}

util::ImText::~ImText()
{
}

std::string*  util::ImText::getTextPointer()
{
	return &m_Name;
}

void util::ImText::draw(const ImGUI * ui)
{
	ui->text(m_Name);
}
