#pragma once
#include<Source\Util\ImGUI\IImItem.h>
#include<string>
#include<Source\Util\Type.h>

namespace util {

	class ImGUI;

	class ImSlider : public IImItem
	{
	public:
		ImSlider(const std::string text,const util::Vec2& range,float initNum = 0);
		~ImSlider();

		void draw(const ImGUI* ui)override;

		float* valuePointer();

		float constValue();

		/// <summary>
		/// 書き出し用データの用意
		/// </summary>
		void getWriteData(std::vector<float>* sliderBuffer)const;

	private:
		const util::Vec2 m_Range;
		float m_SliderNum;
		bool isInputKey;
	};
}