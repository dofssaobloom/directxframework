#pragma once
#include<iostream>
#include<fstream>
#include<functional>

namespace util {

	/// <summary>
	/// バイナリ読み込みクラス
	/// </summary>
	class BinaryLoader
	{
	public:
		BinaryLoader(const std::string& filePath);
		~BinaryLoader();

		/// <summary>
		/// バイナリデータをベクターで取得する
		/// </summary>
		template<typename T>
		void load(std::vector<T>* buffer) {

			loadLoop<T>([&](T& data) {
				(*buffer).emplace_back(data);
			});
		}

		template<typename T>
		void load(int loopNum,std::vector<T>* buffer) {

			loadLoop<T>(loopNum,[&](T& data) {
				(*buffer).emplace_back(data);
			});
		}


		template<typename T>
		void load(T* buffer) {
			m_Stream.read(reinterpret_cast<char*>(buffer), sizeof(T));
		}

	private:

		/// <summary>
		/// 読み込みループ関数
		/// </summary>
		template<typename T>
		void loadLoop(std::function<void(T&)> bufferAction) {
			while (true) {
				T result;
				m_Stream.read(reinterpret_cast<char*>(&result), sizeof(T));
				if (m_Stream.eof()) {
					break;
				}

				bufferAction(result);
			}
		}

		/// <summary>
		/// 回数つきループ
		/// </summary>
		template<typename T>
		void loadLoop(int loopNum,std::function<void(T&)> bufferAction) {

			for (int i = 0; i < loopNum; i++)
			{
				T result;
				m_Stream.read(reinterpret_cast<char*>(&result), sizeof(T));
				if (m_Stream.eof()) {
					break;
				}

				bufferAction(result);
			}
		}

	private:
		std::ifstream m_Stream;
	};


}