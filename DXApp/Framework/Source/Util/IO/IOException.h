#pragma once

namespace exception{

	class IOException : public std::exception
	{
	public:
		IOException(const std::string& message)
			:std::exception(message.c_str())
		{

		}

	};

}