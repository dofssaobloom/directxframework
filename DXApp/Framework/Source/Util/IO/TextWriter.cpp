#include "Framework.h"
#include "TextWriter.h"

namespace util {

	TextWriter::TextWriter(const std::string& filePath, FileType type)
	{
		std::_Iosb<int>::_Openmode _fileType;
		switch (type)
		{
			//新規
		case util::FileType::newFile:
			_fileType = std::ios::trunc;
			break;

			//追記
		case util::FileType::addFile:
			_fileType = std::ios::app;
			break;
		default:
			//デフォルトは新規
			_fileType = std::ios::trunc;
			break;
		}

		m_Stream.open(filePath,std::ios::out | _fileType);
	}

	TextWriter::~TextWriter()
	{
		m_Stream.close();
	}

	void TextWriter::write(const std::string & data)
	{
		m_Stream << data;
	}
}