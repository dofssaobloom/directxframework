#include "Framework.h"
#include "BinaryWriter.h"
#include<assert.h>

namespace util {

	BinaryWriter::BinaryWriter(const std::string& filePath, FileType fileType)
	{
		std::_Iosb<int>::_Openmode _fileType;
		switch (fileType)
		{
			//新規
		case util::FileType::newFile:
			_fileType =  std::ios::trunc;
			break;

			//追記
		case util::FileType::addFile:
			_fileType = std::ios::app;
			break;
		default:
			//デフォルトは新規
			_fileType = std::ios::trunc;
			break;
		}
		
		m_Stream.open(filePath, std::ios::out | std::ios::binary | _fileType);

		assert(m_Stream && "ファイルが開けませんでした");

	}

	BinaryWriter::~BinaryWriter()
	{
		m_Stream.close();
	}

}