#pragma once
#include<iostream>
#include<fstream>

namespace util {

	enum class FileType
	{
		newFile,
		addFile,
	};

	/// <summary>
	/// バイナリ書き出しクラス
	/// </summary>
	class BinaryWriter
	{
	public:
		BinaryWriter(const std::string& filePath, FileType fileType);
		~BinaryWriter();


		/// <summary>
		/// vectorに格納されたデータをバイナリで書き出す
		/// </summary>
		template<typename T>
		void write(std::vector<T>& data) {
			m_Stream.write(reinterpret_cast<char*>(data.data()), sizeof(T) * data.size());
		}

		/// <summary>
		/// 単体書き込み
		/// data データ
		/// size データサイズ
		/// </summary>
		template<typename T>
		void write(T& data, size_t size) {
			m_Stream.write(reinterpret_cast<char*>(&data), size);
		}

		/// <summary>
		/// char*で直接書き込み
		/// </summary>
		/// <param name="data"></param>
		/// <param name="size"></param>
		void write(const char* data, size_t size) {
			m_Stream.write(data, size);
		}

		/// <summary>
		/// String書き込み
		/// </summary>
		/// <param name="str"></param>
		void write(const std::string& str) {
			m_Stream.write(str.c_str(), str.size());
		}


	private:
		std::ofstream m_Stream;
	};


}