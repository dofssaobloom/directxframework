#include "BinaryLoader.h"
#include<assert.h>
#include<Source\Util\IO\IOException.h>
#include<Source\Util\IO\BinaryWriter.h>

namespace util {

	BinaryLoader::BinaryLoader(const std::string& filePath)
	{
		try {
			m_Stream.open(filePath, std::ios::binary);
			if (!m_Stream) {
				throw exception::IOException("ファイルオープンに失敗しました");
			}
		}
		catch (exception::IOException& e){
			//ファイルを開くのに失敗した場合新しいものを作成する
			std::unique_ptr<BinaryWriter> writer = std::make_unique<BinaryWriter>(filePath,FileType::newFile);
			writer.reset();//ファイルを閉じて生成

			m_Stream.open(filePath, std::ios::binary);
		}

	}

	BinaryLoader::~BinaryLoader()
	{
		m_Stream.close();
	}


}