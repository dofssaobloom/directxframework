#pragma once
#include<iostream>
#include<fstream>
#include"BinaryWriter.h"

namespace util {

	/// <summary>
	/// テキスト書き出しクラス
	/// </summary>
	class TextWriter
	{
	public:
		TextWriter(const std::string& filePath,FileType type);
		~TextWriter();

		/// <summary>
		/// 書き出し
		/// </summary>
		/// <param name="data"></param>
		void write(const std::string& data);

	private:
		std::ofstream m_Stream;

	};
}