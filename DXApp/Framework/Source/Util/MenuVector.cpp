#include "Framework.h"
#include "MenuVector.h"

namespace util {

	MenuVector::MenuVector()
	{
	}

	MenuVector::~MenuVector()
	{
	}

	void MenuVector::init() {
		m_pMenuElement->init();
	}

	void MenuVector::init(int namber) {
		m_pMenuElement->init(namber);
	}

	void MenuVector::addMenu(std::function<void()> selectAction)
	{
		if (m_pMenuElement == nullptr)
			m_pMenuElement = std::make_unique<Selector>(1);
		else
			//!２個めから一個ずつ増やしていく
			m_pMenuElement = std::make_unique<Selector>(m_pMenuElement->getSelectCount() + 1);

		m_MenuFunction.emplace_back(selectAction);
	}

	void MenuVector::applay()
	{
		assert(!m_MenuFunction.empty() && "項目がゼロです");
		m_MenuFunction[m_pMenuElement->getSelectNum()]();
	}

	void MenuVector::enter()
	{
		m_pMenuElement->enter();
	}

	void MenuVector::back()
	{
		m_pMenuElement->back();
	}

	const int MenuVector::getCount() const
	{
		return m_pMenuElement->getSelectNum();
	}


}