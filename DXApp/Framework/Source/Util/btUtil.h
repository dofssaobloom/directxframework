#pragma once
#include<Source\Util\Type.h>
#include<Include\BULLET\btBulletDynamicsCommon.h>

namespace util {


	/// <summary>
	/// Util::Vec3をbtVector3に変換
	/// </summary>
	btVector3 convertBtVector3(const util::Vec3& vec3);

	/// <summary>
	/// btVector3をUtil::Vec3に変換
	/// </summary>
	util::Vec3 convertUtVector3(const btVector3& vec3);

	/// <summary>
	/// btQuaternionをbtVector3に変換
	/// </summary>
	btVector3 convertBtRotate(const btQuaternion& q);

	/// <summary>
	/// xnmaのクオータニオンからバレット用クオータニオン型に変換する
	/// </summary>
	btQuaternion convertBtQuaternion(const XMVECTOR& xmQuaternion);

	XMVECTOR convertUtQuaternion(const btQuaternion& btQuaternion);
}