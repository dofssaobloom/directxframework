#pragma once

#include<unordered_map>
#include<functional>

namespace util {
	
	template<typename T, typename ...Param>
	class Event
	{
	public:
		Event() {

		}
		virtual ~Event() {

		}

		void addEvent(const T& key, std::function<void(Param...)> func) {
			m_Funcs[key].emplace_back(func);
		}

		void onEvent(const T& key, Param... param) {
			for (auto& func : m_Funcs[key]) {
				func(param...);
			}
		}

		/// <summary>
		/// 指定したkeyのイベントを削除する
		/// </summary>
		/// <param name="key"></param>
		void removeEvent(const T& key) {
			auto find = m_Funcs.find(key);
			if (find == m_Funcs.end())return;
			m_Funcs.erase(find);
		}

		void clear() {
			m_Funcs.clear();
		}

	private:
		using FuncContainer = std::unordered_map<T, std::vector<std::function<void(Param...)>>>;
		FuncContainer m_Funcs;
	};
}