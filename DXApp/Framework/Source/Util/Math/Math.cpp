#include"Math.h"
#include"Transform.h"


namespace util {



	XMVECTOR axisRotate(const util::Vec3 & axis, Degree angle)
	{
		angle = XMConvertToRadians(angle);

		return XMQuaternionRotationAxis(XMLoadFloat3(&axis), angle);
	}

	void createMatrixFromFront(util::Vec3& front, util::Mat4 * result)
	{
		util::Vec3 temp = front + util::Vec3(0, -1, 0);

		util::Vec3 left = XMVector3Cross(front.toXMVector(), temp.toXMVector());

		util::Vec3 up = XMVector3Cross(front.toXMVector(), left.toXMVector());

		*result = util::Mat4(
			left.x, left.y, left.z, 0,
			up.x, up.y, up.z, 0,
			front.x, front.y, front.z, 0,
			0, 0, 0, 1);
	}

	void createMatrixFromLeft(util::Vec3& left, util::Mat4 * result)
	{
		util::Vec3 temp = left + util::Vec3(0, -1, 0);

		util::Vec3 front = XMVector3Cross(temp.toXMVector(), left.toXMVector());

		util::Vec3 up = XMVector3Cross(front.toXMVector(), left.toXMVector());

		*result = util::Mat4(
			left.x, left.y, left.z, 0,
			up.x, up.y, up.z, 0,
			front.x, front.y, front.z, 0,
			0, 0, 0, 1);
	}

	int sign(float x)
	{
		int sign = !signbit(x);
		return sign ? 1 : -1;
	}

	float bezierCurve(float x, float p1, float p2, float p3)
	{
		float q1 = lerp(x, p1, p2);
		float q2 = lerp(x, p2, p3);

		return lerp(x, q1, q2);
	}

	float bezierCurve(float x, float p1, float p2, float p3, float p4)
	{
		float q1 = lerp(x, p1, p2);
		float q2 = lerp(x, p2, p3);
		float q3 = lerp(x, p3, p4);

		float q4 = lerp(x, q1, q2);
		float q5 = lerp(x, q2, q3);

		return lerp(x, q4, q5);
	}

	void convertMatToRotateVec(const util::Mat4 & m, util::Vec3 * result)
	{
		//const double threshold = 0.001;

		////1のとき
		//if (abs(m.at(2,1) - 1.0) < threshold) {
		//	result->x = PI / 2;
		//	result->y = 0;
		//	result->z = atan2(m.at(1,0),m.at(0,0));
		//}
		//else if(abs(m.at(2, 1) + 1.0) < threshold){//-1のとき
		//	result->x = -PI / 2;
		//	result->y = 0;
		//	result->z = atan2(m.at(1, 0), m.at(0, 0));
		//}
		//else {
		//	result->x = asin(m.at(2,1));
		//	result->y = atan2(-m.at(2,0),m.at(2,2));
		//	result->z = atan2(-m.at(0, 1), m.at(1, 1));
		//}
	}

	void convertQuatToRotateVec(const util::Quaternion & q, util::Vec3 * result)
	{
		float a;
		XMVECTOR b;
		XMQuaternionToAxisAngle(&b, &a, q.toXMVector());
		//XMQuaternionToAxisAngle(&util::Vec3(1, 0, 0).toXMVector(), &result->x, q.toXMVector());
		//XMQuaternionToAxisAngle(&util::Vec3(0, 1, 0).toXMVector(), &result->y, q.toXMVector());
		//XMQuaternionToAxisAngle(&util::Vec3(0, 0, 1).toXMVector(), &result->z, q.toXMVector());
	}

	util::Mat4 lerpMatrix(float num, const util::Mat4 & min, const util::Mat4 & max)
	{
		return util::Mat4(
			lerp<float>(num, min._11, max._11), lerp<float>(num, min._12, max._12), lerp<float>(num, min._13, max._13), lerp<float>(num, min._14, max._14),
			lerp<float>(num, min._21, max._21), lerp<float>(num, min._22, max._22), lerp<float>(num, min._23, max._23), lerp<float>(num, min._24, max._24),
			lerp<float>(num, min._31, max._31), lerp<float>(num, min._32, max._32), lerp<float>(num, min._33, max._33), lerp<float>(num, min._34, max._34),
			lerp<float>(num, min._41, max._41), lerp<float>(num, min._42, max._42), lerp<float>(num, min._43, max._43), lerp<float>(num, min._44, max._44)
		);
	}

	XMVECTOR createAxisRotate(const util::Vec3 & angle)
	{
		auto rotateQuaternion = XMQuaternionRotationRollPitchYaw(XMConvertToRadians(angle.x), XMConvertToRadians(angle.y), XMConvertToRadians(angle.z));
		return rotateQuaternion;
	}

	int getDigid(int num, int digid)
	{
		return (num / digid) % 10;
	}

	float quaternionToPitch(Quaternion & q)
	{
		//小数点以下切り上げ
		return (XMConvertToDegrees(asin(2.0 * (q.w*q.x - q.z*q.y))));
	}

	float quaternionToYaw(Quaternion & q)
	{
		return (XMConvertToDegrees(atan2(2.0 * (q.w*q.y + q.x*q.z),
			1 - 2 * (q.y*q.y + q.x*q.x))));

	}

	float quaternionToRoll(Quaternion & q)
	{
		return (XMConvertToDegrees(atan2(2.0 * (q.w*q.z + q.y*q.x),
			1 - 2 * (q.x*q.x + q.z*q.z))));

	}

	Vec3 quaternionToEuler(Quaternion & quaternion)
	{
		float x = quaternionToPitch(quaternion);
		float y = quaternionToYaw(quaternion);
		float z = quaternionToRoll(quaternion);

		return util::Vec3(x, y, z);
	}

	void toEulerAngle(const Quaternion& q, double& roll, double& pitch, double& yaw)
	{
		// roll (x-axis rotation)
		double sinr = +2.0 * (q.w * q.x + q.y * q.z);
		double cosr = +1.0 - 2.0 * (q.x * q.x + q.y * q.y);
		roll = XMConvertToDegrees(atan2(sinr, cosr));

		// pitch (y-axis rotation)
		double sinp = +2.0 * (q.w * q.y - q.z * q.x);
		if (fabs(sinp) >= 1)
			pitch = XMConvertToDegrees(copysign(PI / 2, sinp)); // use 90 degrees if out of range
		else
			pitch = asin(sinp);

		// yaw (z-axis rotation)
		double siny = +2.0 * (q.w * q.z + q.x * q.y);
		double cosy = +1.0 - 2.0 * (q.y * q.y + q.z * q.z);
		yaw = XMConvertToDegrees(atan2(siny, cosy));
	}

	float round(float src, int n)
	{
		float dst;

		dst = src * pow(10,n);

		dst = ceil(dst);

		return dst / pow(10, n);
	}

	Vector3 rotation(const util::Quaternion & q, const util::Vec3 & p)
	{
		Quaternion qCon = XMQuaternionConjugate(q.toXMVector());//共役Quatrernion
		Quaternion qVec = util::Quaternion(p, 1);//Vector->Quaternionに変換
		Quaternion Q = q;
		Q = XMQuaternionMultiply(Q.toXMVector(), qVec.toXMVector()); //回転Quaternion*位置Vecter*共役Quaternionで掛け算
		Q = XMQuaternionMultiply(Q.toXMVector(), qCon.toXMVector());
		return Vector3(Q.x, Q.y, Q.z);
	}

	Transform matrixToTransform(const util::Mat4 & mat)
	{
		Transform result;

		result.m_Position = util::Vec3(mat._14, mat._24,mat._34);

		result.m_Scale = util::Vec3(mat._11, mat._22, mat._33);

		auto tempMat = mat;

		tempMat._11 = 1;
		tempMat._22 = 1;
		tempMat._33 = 1;
		tempMat._44 = 1;

		tempMat._14 = 0;
		tempMat._24 = 0;
		tempMat._34 = 0;


		util::Quaternion q = XMQuaternionRotationMatrix(tempMat.toXMMatrix());

		result.m_Rotation = util::quaternionToEuler(q);

		return result;
	}



}