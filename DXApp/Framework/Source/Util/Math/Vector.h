#pragma once
#include<D3DX11.h>
#include<xnamath.h>

namespace util {

	struct Vector4;

	template<typename T>
	class IVector
	{
		virtual T normalize()const = 0;

		virtual XMVECTOR toXMVector()const = 0;

		/// <summary>
		/// すべての要素が0かどうか
		/// </summary>
		/// <returns></returns>
		virtual bool isZero()const = 0;

		virtual const T operator+ ()const = 0;

		virtual const T operator- ()const = 0;

		virtual const T operator+ (const T& other)const = 0;

		virtual const T operator+ (const float value)const = 0;

		virtual const T operator- (const T& other)const = 0;

		virtual const T operator- (const float value)const = 0;

		virtual const T operator* (const T& other)const = 0;

		virtual const T operator* (const float value)const = 0;

		virtual const T operator/ (const T& other)const = 0;

		virtual const T operator/ (const float value)const = 0;

		virtual const T operator= (const T& other)const = 0;

		virtual const T operator+= (const T& other)const = 0;

		virtual const T operator-= (const T& other)const = 0;

		virtual const T operator*= (const T& other)const = 0;

		virtual const T operator/= (const T& other)const = 0;

	};


	struct Vector2 : public XMFLOAT2
	{
		Vector2();
		Vector2(FLOAT x_, FLOAT y_);
		Vector2(Vector2& vec);
		Vector2(XMFLOAT2 vec);
		Vector2(const XMVECTOR& vec);

		Vector2 normalize()const;

		XMVECTOR toXMVector() const;

		/// <summary>
		/// すべての要素が0かどうか
		/// </summary>
		/// <returns></returns>
		bool isZero()const;

		float dot(Vector2& other)const;

		static Vector2 lerp(float t, Vector2& begin, Vector2& end);

		static Vector2 bezier(float t, Vector2& begin, Vector2& center, Vector2& end);

		float length()const;

		const Vector2 operator+ ()const;

		const Vector2 operator- ()const;

		const Vector2 operator+ (const Vector2& other)const;

		const Vector2 operator+ (const float value)const;

		const Vector2 operator- (const Vector2& other)const;

		const Vector2 operator- (const float value)const;

		const Vector2 operator* (const Vector2& other)const;

		const Vector2 operator* (const float value)const;

		const Vector2 operator/ (const Vector2& other)const;

		const Vector2 operator/ (const float value)const;

		const Vector2 operator= (const Vector2& other);

		const Vector2 operator+= (const Vector2& other);

		const Vector2 operator-= (const Vector2& other);

		const Vector2 operator*= (const Vector2& other);

		const Vector2 operator/= (const Vector2& other);

	};

	struct Vector3 : public XMFLOAT3//, public IVector<Vector3>
	{
		Vector3();
		Vector3(FLOAT x_, FLOAT y_, FLOAT z_);
		Vector3(Vector3& other);
		Vector3(XMFLOAT3 vec);
		Vector3(const XMVECTOR& vec);

		Vector3 normalize()const;

		void clamp(const Vector3& min, const Vector3& max);

		float  length()const;

		float distance(Vector3& other)const;

		XMVECTOR toXMVector()const;

		float dot(Vector3& other)const;

		Vector3 cross(Vector3& other)const;

		static Vector3 lerp(float t, Vector3& begin,Vector3& end);

		/// <summary>
		/// すべての要素が0かどうか
		/// </summary>
		/// <returns></returns>
		bool isZero()const;

		/// <summary>
		/// 各軸の回転値でクオータニオンを作成する
		/// </summary>
		/// <param name="vec"></param>
		/// <returns></returns>
		static Vector4 toQuaternion(const util::Vector3& vec);

		const Vector3 operator+ ()const;

		const Vector3 operator- ()const;

		const Vector3 operator+ (const Vector3& other)const;

		const Vector3 operator+ (const float value)const;

		const Vector3 operator- (const Vector3& other)const;

		const Vector3 operator- (const float value)const;

		const Vector3 operator* (const Vector3& other)const;

		const Vector3 operator* (const float value)const;

		const Vector3 operator/ (const Vector3& other)const;

		const Vector3 operator/ (const float value)const;

		const Vector3 operator=	(const Vector3& other);

		const Vector3 operator=	(const XMVECTOR& other);

		const Vector3 operator+= (const Vector3& other);

		const Vector3 operator-= (const Vector3& other);

		const Vector3 operator*= (const Vector3& other);

		const Vector3 operator/= (const Vector3& other);
	};

	struct Vector4 : public XMFLOAT4//, public IVector<Vector4>
	{
		Vector4();
		Vector4(FLOAT x_, FLOAT y_, FLOAT z_, FLOAT w_);
		Vector4(const Vector3& other, FLOAT w_);
		Vector4(Vector4& other);
		Vector4(XMFLOAT4 other);
		Vector4(const XMVECTOR& vec);

		Vector4 normalize()const;


		/// <summary>
		/// すべての要素が0かどうか
		/// </summary>
		/// <returns></returns>
		bool isZero()const;

		XMVECTOR toXMVector()const;

		/// <summary>
		/// 指定した桁を切り上げる
		/// </summary>
		/// <param name="n"></param>
		void ceilToMember(int n);

		const Vector4 operator+ ()const;

		const Vector4 operator- ()const;

		const Vector4 operator+ (const Vector4& other)const;

		const Vector4 operator+ (const float value)const;

		const Vector4 operator- (const Vector4& other)const;

		const Vector4 operator- (const float value)const;

		const Vector4 operator* (const Vector4& other)const;

		const Vector4 operator* (const float value)const;

		const Vector4 operator/ (const Vector4& other)const;

		const Vector4 operator/ (const float value)const;

		const Vector4 operator=	(const Vector4& other);

		const Vector4 operator=	(const XMVECTOR& other);

		const Vector4 operator+= (const Vector4& other);

		const Vector4 operator-= (const Vector4& other);

		const Vector4 operator*= (const Vector4& other);

		const Vector4 operator/= (const Vector4& other);
	};

	class Matrix : public XMFLOAT4X4
	{
	public:
		Matrix();

		Matrix(float m00, float m01, float m02, float m03,
			float m10, float m11, float m12, float m13,
			float m20, float m21, float m22, float m23,
			float m30, float m31, float m32, float m33);

		Matrix(XMMATRIX& mat);

		/// <summary>
		/// 指定した位置の値をatanで計算する
		/// </summary>
		/// <param name="index1">1番目のインデックス</param>
		/// <param name="index2">2番目のインデックス</param>
		/// <returns></returns>
		double at(int index1,int index2)const;

		Matrix transpose()const;

		~Matrix();

		XMMATRIX toXMMatrix()const;

		const Matrix operator* (const float value)const;

		const Matrix operator+ (const Matrix& value)const;
	};

}