#include "Vector.h"

namespace util {
	Vector2::Vector2()
		:Vector2(0, 0)
	{
	}
	Vector2::Vector2(FLOAT x_, FLOAT y_)
		: XMFLOAT2(x_, y_)
	{
	}

	Vector2::Vector2(Vector2& vec)
	{
		this->x = vec.x;
		this->y = vec.y;
	}

	Vector2::Vector2(XMFLOAT2 vec)
		: XMFLOAT2(vec)
	{
	}

	Vector2::Vector2(const XMVECTOR & vec)
	{
		XMStoreFloat2(this, vec);
	}

	Vector2 Vector2::normalize()const
	{
		//0���Z���
		if (x == 0.0 && y == 0, 0)return Vector2();

		auto&& temp = XMVector2Normalize(XMLoadFloat2(this));
		return  Vector2(temp);
	}

	XMVECTOR Vector2::toXMVector()const
	{
		return XMLoadFloat2(this);
	}

	bool Vector2::isZero()const
	{
		return x == 0.0f && y == 0.0f;
	}

	float Vector2::dot(Vector2 & other) const
	{
		auto dot = XMVector2Dot((*this).toXMVector(), other.toXMVector());
		return XMVectorGetX(dot);
	}

	Vector2 Vector2::lerp(float t, Vector2& begin, Vector2& end) {
		util::Vec2 result;
		result.x = util::lerp<float>(t, begin.x, end.x);
		result.y = util::lerp<float>(t, begin.y, end.y);
		return result;
	}

	Vector2 Vector2::bezier(float t, Vector2& begin, Vector2& center, Vector2& end) {
		util::Vec2 result;
		result.x = util::bezierCurve(t, begin.x, center.x, end.x);
		result.y = util::bezierCurve(t, begin.y, center.y, end.y);
		return result;
	}

	float Vector2::length() const
	{
		auto len = XMVector2Length(this->toXMVector());

		if (len.x == 0 && len.y == 0)
			return 0.0f;

		return XMVectorGetX(len);
	}

	const Vector2 Vector2::operator+()const
	{
		return *this;
	}

	const Vector2 Vector2::operator-()const
	{
		return util::Vector2(-x, -y);
	}

	const Vector2 Vector2::operator+(const Vector2& other)const
	{
		return Vector2(x + other.x, y + other.y);
	}

	const Vector2 Vector2::operator+(const float value)const
	{
		return Vector2(x + value, y + value);
	}

	const Vector2 Vector2::operator-(const Vector2& other)const
	{
		return Vector2(x - other.x, y - other.y);
	}

	const Vector2 Vector2::operator-(const float value)const
	{
		return Vector2(x - value, y - value);
	}

	const Vector2 Vector2::operator*(const Vector2& other)const
	{
		return Vector2(x * other.x, y * other.y);
	}

	const Vector2 Vector2::operator*(const float value)const
	{
		return Vector2(x * value, y * value);
	}

	const Vector2 Vector2::operator/(const Vector2& other)const
	{
		return Vector2(x / other.x, y / other.y);
	}

	const Vector2 Vector2::operator/(const float value)const
	{
		return Vector2(x / value, y / value);
	}

	const Vector2 Vector2::operator=(const Vector2& other)
	{
		this->x = other.x;
		this->y = other.y;
		return *this;
	}

	const Vector2 Vector2::operator+=(const Vector2& other)
	{
		this->x += other.x;
		this->y += other.y;
		return *this;
	}

	const Vector2 Vector2::operator-=(const Vector2& other)
	{
		this->x -= other.x;
		this->y -= other.y;
		return *this;
	}

	const Vector2 Vector2::operator*=(const Vector2& other)
	{
		this->x *= other.x;
		this->y *= other.y;
		return *this;
	}

	const Vector2 Vector2::operator/=(const Vector2& other)
	{
		this->x /= other.x;
		this->y /= other.y;
		return *this;
	}

	///////////////// Vector3 //////////////////////////////////////////////////////////////

	Vector3::Vector3()
		:Vector3(0, 0, 0)
	{
	}

	Vector3::Vector3(FLOAT x_, FLOAT y_, FLOAT z_)
		: XMFLOAT3(x_, y_, z_)
	{
	}

	Vector3::Vector3(Vector3 & other)
	{
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;
	}

	Vector3::Vector3(XMFLOAT3 vec)
		: XMFLOAT3(vec)
	{
	}

	Vector3::Vector3(const XMVECTOR& vec)
	{
		XMStoreFloat3(this, vec);
	}

	Vector3 Vector3::normalize()const
	{
		auto&& temp = XMVector3Normalize(XMLoadFloat3(this));
		return  Vector3(temp);
	}

	void Vector3::clamp(const Vector3& min, const Vector3& max) {
		x = util::clamp<float>(x, min.x, max.x);
		y = util::clamp<float>(y, min.y, max.y);
		z = util::clamp<float>(z, min.z, max.z);
	}

	float Vector3::length()const
	{
		auto len = XMVector3Length(this->toXMVector());

		if (len.x == 0 && len.y == 0 && len.z == 0)
			return 0.0f;

		return XMVectorGetX(len);
	}

	float Vector3::distance(Vector3 & other)const
	{
		float temp = pow(other.x - this->x, 2) + pow(other.y - this->y, 2) + pow(other.z - this->z, 2);
		temp = sqrt(temp);
		return temp;
	}

	XMVECTOR Vector3::toXMVector()const
	{
		return XMLoadFloat3(this);
	}

	float Vector3::dot(Vector3 & other)const
	{
		auto dot = XMVector3Dot((*this).toXMVector(), other.toXMVector());
		return XMVectorGetX(dot);
	}

	Vector3 Vector3::cross(Vector3 & other)const
	{
		return  XMVector3Cross((*this).toXMVector(), other.toXMVector());
	}

	Vector3 Vector3::lerp(float t, Vector3& begin, Vector3& end)
	{
		util::Vec3 result;
		result.x = util::lerp<float>(t, begin.x, end.x);
		result.y = util::lerp<float>(t, begin.y, end.y);
		result.z = util::lerp<float>(t, begin.z, end.z);
		return result;
	}

	bool Vector3::isZero() const
	{
		return x == 0.0f && y == 0.0f && z == 0.0f;
	}

	Vector4 Vector3::toQuaternion(const util::Vector3 & vec)
	{
		return XMQuaternionRotationRollPitchYawFromVector(vec.toXMVector());
	}

	//Quaternion Vector3::toQuaternion(const util::Vector3 & vec)
	//{
	//	//return XMQuaternionRotationRollPitchYawFromVector(vec.toXMVector());
	//}

	const Vector3 Vector3::operator+()const
	{
		return *this;
	}

	const Vector3 Vector3::operator-()const
	{
		return util::Vector3(-x, -y, -z);
	}

	const Vector3 Vector3::operator+(const Vector3& other)const
	{
		return Vector3(x + other.x, y + other.y, z + other.z);
	}

	const Vector3 Vector3::operator+(const float value)const
	{
		return Vector3(x + value, y + value, z + value);
	}

	const Vector3 Vector3::operator-(const Vector3& other)const
	{
		return Vector3(x - other.x, y - other.y, z - other.z);
	}

	const Vector3 Vector3::operator-(const float value)const
	{
		return Vector3(x - value, y - value, z - value);
	}

	const Vector3 Vector3::operator*(const Vector3& other)const
	{
		return Vector3(x * other.x, y * other.y, z * other.z);
	}

	const Vector3 Vector3::operator*(const float value)const
	{
		return Vector3(x * value, y * value, z * value);
	}

	const Vector3 Vector3::operator/(const Vector3& other)const
	{
		return Vector3(x / other.x, y / other.y, z / other.z);
	}

	const Vector3 Vector3::operator/(const float value)const
	{
		return Vector3(x / value, y / value, z / value);
	}

	const Vector3 Vector3::operator=(const Vector3 & other)
	{
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;
		return *this;
	}

	const Vector3 Vector3::operator=(const XMVECTOR&  other)
	{
		Vector3 temp;
		XMStoreFloat3(&temp, other);

		this->x = temp.x;
		this->y = temp.y;
		this->z = temp.z;
		return *this;
	}

	const Vector3 Vector3::operator+=(const Vector3& other)
	{
		this->x += other.x;
		this->y += other.y;
		this->z += other.z;
		return *this;
	}

	const Vector3 Vector3::operator-=(const Vector3& other)
	{
		this->x -= other.x;
		this->y -= other.y;
		this->z -= other.z;
		return *this;
	}

	const Vector3 Vector3::operator*=(const Vector3& other)
	{
		this->x *= other.x;
		this->y *= other.y;
		this->z *= other.z;
		return *this;
	}

	const Vector3 Vector3::operator/=(const Vector3& other)
	{
		this->x /= other.x;
		this->y /= other.y;
		this->z /= other.z;
		return *this;
	}

	///////////////// Vector4 //////////////////////////////////////////////////////////////

	Vector4::Vector4()
		:Vector4(0, 0, 0, 0)
	{
	}

	Vector4::Vector4(FLOAT x_, FLOAT y_, FLOAT z_, FLOAT w_)
		: XMFLOAT4(x_, y_, z_, w_)
	{
	}

	Vector4::Vector4(const Vector3 & other, FLOAT w_)
	{
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;
		this->w = w_;
	}

	Vector4::Vector4(Vector4 & other)
	{
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;
		this->w = other.w;
	}

	Vector4::Vector4(XMFLOAT4 other)
	{
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;
		this->w = other.w;
	}

	Vector4::Vector4(const XMVECTOR& vec)
	{
		Vector4 temp;
		XMStoreFloat4(&temp, vec);

		this->x = temp.x;
		this->y = temp.y;
		this->z = temp.z;
		this->w = temp.w;
	}

	Vector4 Vector4::normalize() const
	{
		auto&& tmep = util::Vec3(x, y, z).normalize();
		return Vector4(tmep.x, tmep.y, tmep.z, 0);
	}

	bool Vector4::isZero() const
	{
		return x == 0.0f && y == 0.0f && z == 0.0f && w == 0.0f;
	}

	XMVECTOR Vector4::toXMVector()const
	{
		return XMLoadFloat4(this);
	}

	void Vector4::ceilToMember(int n)
	{
		x = util::round(x, n);
		y = util::round(y, n);
		z = util::round(z, n);
		w = util::round(w, n);
	}

	const Vector4 Vector4::operator+()const
	{
		return *this;
	}

	const Vector4 Vector4::operator-()const
	{
		return util::Vector4(-x, -y, -z, -w);
	}

	const Vector4 Vector4::operator+(const Vector4 & other)const
	{
		return Vector4(x + other.x, y + other.y, z + other.z, w + other.w);
	}

	const Vector4 Vector4::operator+(const float value)const
	{
		return Vector4(x + value, y + value, z + value, w + value);
	}

	const Vector4 Vector4::operator-(const Vector4 & other)const
	{
		return Vector4(x - other.x, y - other.y, z - other.z, w - other.w);
	}

	const Vector4 Vector4::operator-(const float value)const
	{
		return Vector4(x - value, y - value, z - value, w - value);
	}

	const Vector4 Vector4::operator*(const Vector4 & other)const
	{
		return Vector4(x * other.x, y * other.y, z * other.z, w * other.w);
	}

	const Vector4 Vector4::operator*(const float value)const
	{
		return Vector4(x * value, y * value, z * value, w * value);
	}

	const Vector4 Vector4::operator/(const Vector4 & other)const
	{
		return Vector4(x / other.x, y / other.y, z / other.z, w / other.w);
	}

	const Vector4 Vector4::operator/(const float value)const
	{
		return Vector4(x / value, y / value, z / value, w / value);
	}

	const Vector4 Vector4::operator=(const Vector4 & other)
	{
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;
		this->w = other.w;
		return *this;
	}

	const Vector4 Vector4::operator=(const XMVECTOR & other)
	{
		Vector4 temp;
		XMStoreFloat4(&temp, other);

		this->x = temp.x;
		this->y = temp.y;
		this->z = temp.z;
		this->w = temp.w;
		return *this;
	}

	const Vector4 Vector4::operator*=(const Vector4 & other)
	{
		this->x *= other.x;
		this->y *= other.y;
		this->z *= other.z;
		this->w *= other.w;
		return *this;
	}

	const Vector4 Vector4::operator-=(const Vector4 & other)
	{
		this->x -= other.x;
		this->y -= other.y;
		this->z -= other.z;
		this->w -= other.w;
		return *this;
	}

	const Vector4 Vector4::operator+=(const Vector4 & other)
	{
		this->x += other.x;
		this->y += other.y;
		this->z += other.z;
		this->w += other.w;
		return *this;
	}

	const Vector4 Vector4::operator/=(const Vector4 & other)
	{
		this->x /= other.x;
		this->y /= other.y;
		this->z /= other.z;
		this->w /= other.w;
		return *this;
	}

	Matrix::Matrix()
		:XMFLOAT4X4()
	{
	}

	Matrix::Matrix(float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23,
		float m30, float m31, float m32, float m33)
		: XMFLOAT4X4(m00, m01, m02, m03,
			m10, m11, m12, m13,
			m20, m21, m22, m23,
			m30, m31, m32, m33)
	{
	}

	Matrix::Matrix(XMMATRIX& mat) {
		XMStoreFloat4x4(this, mat);
	}

	double Matrix::at(int index1, int index2)const
	{
		return atan(m[index1][index2]);
	}

	Matrix Matrix::transpose()const
	{
		return XMMatrixTranspose(this->toXMMatrix());
	}

	Matrix::~Matrix()
	{
	}

	XMMATRIX Matrix::toXMMatrix()const
	{
		return XMLoadFloat4x4(this);
	}

	const Matrix Matrix::operator* (const float value) const {
		return util::Mat4(this->_11 * value, this->_12 * value, this->_13 * value, this->_14 * value,
			this->_21 * value, this->_22 * value, this->_23* value, this->_24 * value,
			this->_31 * value, this->_32 * value, this->_33 * value, this->_34 * value,
			this->_41 * value, this->_42 * value, this->_43 * value, this->_44 * value);
	}

	const Matrix Matrix::operator+ (const Matrix& value)const {
		return util::Mat4(
			this->_11 + value._11, this->_12 + value._12, this->_13 + value._12, this->_14 * value._14,
			this->_21 + value._21, this->_22 + value._22, this->_23 + value._22, this->_24 * value._24,
			this->_31 + value._31, this->_32 + value._32, this->_33 + value._32, this->_34 * value._34,
			this->_41 + value._41, this->_42 + value._42, this->_43 + value._42, this->_44 * value._44);
	}
}


