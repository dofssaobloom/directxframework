#pragma once
#include<random>

namespace util {

	class Randam
	{
	public:
		Randam();
		~Randam();

		/// <summary>
		/// 0~100ÌÍÍÌð¶¬
		/// </summary>
		/// <returns></returns>
		template<typename T = int>
		T next() {
			return next<T>(100);
		}

		/// <summary>
		/// 0 ~ maxÌÍÍÅ¶¬
		/// </summary>
		/// <param name="max"></param>
		/// <returns></returns>
		template<typename T = int>
		T next(const T max) {
			return next<T>(0, max);
		}

		template<typename T>
		struct Rand;

		template<>
		struct Rand<int>{

			Rand(std::mt19937& mt,int min,int max) {
				std::uniform_int_distribution<> rand(min, max);
				result = rand(mt);
			}

			int result;
		};

		template<>
		struct Rand<float>{
			Rand(std::mt19937& mt,float min, float max) {
				std::uniform_real_distribution<> rand(min, max);
				result = rand(mt);
			}

			float result;
		};

		/// <summary>
		/// min ~ maxÌÍÍÅ¶¬
		/// </summary>
		/// <param name="min"></param>
		/// <param name="max"></param>
		/// <returns></returns>
		template<typename T>
		T next(const T min, const T max) {
			Rand<T> rand(m_MT, min, max);

			return rand.result;
		}

		//template<>
		//template<int>
		//int next(const int min, const int max)
		//{
		//	std::uniform_int_distribution<> rand(min, max);
		//	return rand(m_MT);
		//}

		//template<>
		//template<float>
		//float next(const float min, const float max)
		//{
		//	std::uniform_real_distribution<> rand(min, max);
		//	return rand(m_MT);
		//}


	private:
		std::random_device m_Rand;
		std::mt19937 m_MT;
	};

}