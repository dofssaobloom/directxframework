#pragma once
#include<Source\Util\Type.h>
#include<minmax.h>
#include<math.h>
#include<vector>
#include<random>
#include <map>

#define PI 3.14159265359

namespace util {
	struct Transform;
	using Radian = float;
	using Degree = float;
	using Quaternion = util::Vec4;

	XMVECTOR axisRotate(const util::Vec3& axis, Degree angle);

	template<typename T>
	T clamp(T num, T min, T max) {
		return min(max(num, min), max);
	}

	/**
	* @brief		1次元線形補完(0 ~ 1)
	*/
	template<typename T = float>
	T lerp(T num, T min, T max) {
		return (1.0f -num ) * min + max *  num;
	}

	util::Mat4 lerpMatrix(float num, const util::Mat4& min, const util::Mat4& max);

	void createMatrixFromFront(util::Vec3& front,util::Mat4* result);

	void createMatrixFromLeft(util::Vec3& left, util::Mat4 * result);

	/**
	* @brief		値の符号を返す
	* @return		+なら１　-なら-1
	*/
	int sign(float x);

	float bezierCurve(float x,float p1,float p2,float p3);

	float bezierCurve(float x, float p1, float p2, float p3, float p4);

	/// <summary>
	/// 回転行列を各軸オイラー各に変換
	/// </summary>
	void convertMatToRotateVec(const util::Mat4& m,util::Vec3* result);

	/// <summary>
	/// クオータニオンを各軸オイラー各に変換
	/// </summary>
	void convertQuatToRotateVec(const util::Quaternion& m, util::Vec3* result);

	XMVECTOR createAxisRotate(const util::Vec3& angle);

	/// <summary>
	/// 桁取得
	/// </summary>
	int getDigid(int num,int digid);

	/// <summary>
	/// クオータニオンからピッチ取得
	/// </summary>
	float quaternionToPitch(Quaternion& quaternion);

	/// <summary>
	/// クオータニオンからヨー取得
	/// </summary>
	float quaternionToYaw(Quaternion& quaternion);

	/// <summary>
	/// クオータニオンからロール取得
	/// </summary>
	float quaternionToRoll(Quaternion& quaternion);

	/// <summary>
	/// クオータニオンをオイラー角に変換
	/// </summary>
	Vec3 quaternionToEuler(Quaternion& quaternion);

	/// <summary>
	/// クオータニオンをオイラー角に変換
	/// </summary>
	void toEulerAngle(const Quaternion& q, double& roll, double& pitch, double& yaw);

	/// <summary>
	/// 小数点以下の指定桁を切り上げする
	/// </summary>
	float round(float src,int n);

	Vector3 rotation(const util::Quaternion & q, const util::Vec3 & p);

	/// <summary>
	/// 行列をトランスフォームに変換
	/// </summary>
	Transform matrixToTransform(const util::Mat4& mat);

//	float multidimensionalBezier(std::vector<float> param);
}