#pragma once
#include<vector>
#include<math.h>
#include<Source\Util\Math\Math.h>
#include<string>
#include<Source\Util\Template\Template.h>

namespace util {

	class Gauss
	{
	public:

		Gauss::Gauss()
		{
		}

		Gauss::~Gauss()
		{
		}

		//template<typename T = double>
		//std::vector<T> getList(const T& sigma, const int count) {

		//	std::vector<T> result;
		//	const int half = count / 2;
		//	result.resize(half);

		//	util::foreach(half, [&](int i) {
		//		result[i] = gauss<T>(sigma, i);
		//	});

		//	std::vector<T> copy = result;

		//	std::sort(copy.begin(), copy.end(), [](T left, T right) {
		//		return left < right;
		//	});

		//	std::copy(copy.begin(), copy.end(), std::back_inserter(result));

		//	return result;
		//}

		template<typename T = double>
		std::vector<T> getList(const T& sigma, const int count) {

			std::vector<T> result;
			const int half = count / 2;
			result.resize(half);
			T total = 0.0;

			util::foreach(half, [&](int i) {
				result[i] = gauss<T>(sigma, i);
				total += result[i];
			});

			std::vector<T> copy = result;

			std::sort(copy.begin(), copy.end(), [](T left, T right) {
				return left < right;
			});

			std::copy(copy.begin(), copy.end(), std::back_inserter(result));

			for (auto& r : result) {
				r /= total;
			}

			return result;
		}

		//template<typename T = double>
		//T gauss(double sigma, double x) {
		//	T d = pow(sigma, 2) / 100;
		//	T r = 1.0 + pow(x, 2);
		//	T w = exp(-0.5 * pow(r, 2) / d);
		//	w = pow(w, 2);
		//}

		template<typename T = double>
		T gauss(double sigma, double x) {
			double expVal = -1 * (pow(x, 2) / pow(2 * sigma, 2));
			double divider = sqrt(2 * PI * pow(sigma, 2));
			return (1 / divider) * exp(expVal);
		}


	private:


	};

}