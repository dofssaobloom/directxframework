#include"Scene.h"
#include<Source\Util\IO\CSVLoader.h>
#include<Source\Entity\Entity.h>
#include<Source\Util\WrapFunc.h>
#include<Source\Resource\ResourceManager.h>
#include<Source\Task\TaskManager.h>
#include<Source\Util\WrapFunc.h>
#include<Source\Component\ComponentInclude.h>
#include<Source\Component\BoxRenderOwnerComponent.h>
#include<memory>
#include<Source\Application\Application.h>
#include<Source\Device\Render\Renderer\3D\Instance\SkinningInstanceRenderer.h>
#include<Source\Component\Player\AttackComponent.h>
#include<Source\Component\Scene\Title\Menu.h>
#include<Source\Component\SceneChangeComponent.h>
#include<Source\Util\Math\Math.h>
#include<Source\Component\Physics\PhysicsWorld.h>
#include<Source\Component\Effect\ZoomBlur.h>
#include<Source\Component\UI\Hierarchy.h>
#include<Source\Component\UI\Inspector.h>
#include<Source\Device\Render\Renderer\2D\Single\SpriteRenderer.h>
#include<Source\Util\Lua\LuaCpp.h>


namespace framework {

	const std::string Scene::m_LootPath = "Resource/Scene/";
	std::string	Scene::m_NextSceneName = "";
	std::string Scene::m_CurrentSceneName = "";
	std::string Scene::m_CurrentScenePath = "";

	Scene::Scene()
	{
		m_Builder.registComponent("RenderClientComponent", ComponentBuilder::componentRegister<component::RenderClientComponent>());
		m_Builder.registComponent("RenderOwnerComponent", ComponentBuilder::componentRegister<component::RenderOwnerComponent>());
		m_Builder.registComponent("SpriteRenderComponent", ComponentBuilder::componentRegister<component::SpriteRenderComponent>());
		m_Builder.registComponent("DirectionalLightComponent", ComponentBuilder::componentRegister<component::DirectionalLightComponent>());
		m_Builder.registComponent("PointLightComponent", ComponentBuilder::componentRegister<component::PointLightComponent>());
		m_Builder.registComponent("BoxRenderClientComponent", ComponentBuilder::componentRegister<component::BoxRenderClientComponent>());
		m_Builder.registComponent("CameraComponent", ComponentBuilder::componentRegister<component::CameraComponent>());
		m_Builder.registComponent("AnimatorComponent", ComponentBuilder::componentRegister<component::AnimatorComponent>());
		m_Builder.registComponent("BloomEffect", ComponentBuilder::componentRegister<component::BloomEffect>());
		m_Builder.registComponent("StageMotion", ComponentBuilder::componentRegister<component::StageMotion>());
		m_Builder.registComponent("BlurEffect", ComponentBuilder::componentRegister<component::BlurEffect>());
		m_Builder.registComponent("FadeComponent", ComponentBuilder::componentRegister<component::FadeComponent>());
		m_Builder.registComponent("BulletBoxCollider", ComponentBuilder::componentRegister<component::BulletBoxCollider>());
		m_Builder.registComponent("BulletSphereCollider", ComponentBuilder::componentRegister<component::BulletSphereCollider>());
		m_Builder.registComponent("DOFEffect", ComponentBuilder::componentRegister<component::DOFEffect>());
		m_Builder.registComponent("FogEffect", ComponentBuilder::componentRegister<component::FogEffect>());
		m_Builder.registComponent("FXAA", ComponentBuilder::componentRegister<component::FXAA>());
		m_Builder.registComponent("BulletRigidBody", ComponentBuilder::componentRegister<component::BulletRigidBody>());
		m_Builder.registComponent("ZoomBlur", ComponentBuilder::componentRegister<component::ZoomBlur>());
		m_Builder.registComponent("SoundPlayerComponent", ComponentBuilder::componentRegister<component::SoundPlayerComponent>());
		m_Builder.registComponent("GUIComponent", ComponentBuilder::componentRegister<component::GUIComponent>());
		m_Builder.registComponent("ToneMapEffect", ComponentBuilder::componentRegister<component::ToneMapEffect>());
		m_Builder.registComponent("FPSCounter", ComponentBuilder::componentRegister<component::FPSCounter>());
		m_Builder.registComponent("LineRenderComponent", ComponentBuilder::componentRegister<component::LineRenderComponent>());
		m_Builder.registComponent("ListenerComponent", ComponentBuilder::componentRegister<component::ListenerComponent>());
		m_Builder.registComponent("Counter", ComponentBuilder::componentRegister<component::Counter>());
		m_Builder.registComponent("GradationEffect", ComponentBuilder::componentRegister<component::GradationEffect>());


	}

	Scene::~Scene()
	{

	}

	void Scene::construction()
	{
		//システムオブジェクト作成
		createSystemObject();
		createEntity(m_CurrentScenePath);
		createComponent(m_CurrentScenePath);
		loadNextSceneName(m_CurrentScenePath);

		//コンポーネントをハードコーディングでセット
		util::foreach(m_CreatedComponent.size(), [&](int i) {
			m_CreatedComponent[i].lock()->componentSet(&m_CreatedComponent);
		});
	}

	void Scene::changeScene()
	{
		std::string next = m_NextSceneName;
		setScenePath(next);
		clear();
		loadMaterial();
		resourceLoad();
		construction();
		init();
		framework::TaskManager::getInstance()->init();

		//アップデートが一回も呼ばれずに描画に行ってしまうので一度呼んでおく
		framework::TaskManager::getInstance()->update();
	}


	void Scene::init()
	{
		Entity::initEntity();


		std::sort(m_CreatedComponent.begin(), m_CreatedComponent.end(), [](std::weak_ptr<Component>& left, std::weak_ptr<Component>& right) {
			return left.lock()->getCallOrder() < right.lock()->getCallOrder();
		});

		for (auto& component : m_CreatedComponent) {
			component.lock()->init();
		}
	}

	void Scene::reset()
	{
		Entity::allComponentClear();
		init();
	}

	void Scene::createSystemObject()
	{
		//システムで必須のオブジェクトを作成しておく
		auto entity = framework::Entity::createEntity("Renderer", "System", util::Transform());
		auto component = entity.lock()->addComponent(std::make_shared<component::BoxRenderOwnerComponent>());
		component.lock()->componentCreated(entity);
		component.lock()->active();
		m_CreatedComponent.emplace_back(component);

		entity = framework::Entity::createEntity("GlobalEvent", "System", util::Transform());
		entity.lock()->addEvent("ChangeScene", [&]() {
			changeScene();
		});

		std::vector<std::weak_ptr<Component>> damy;
		m_CreatedComponent.emplace_back(entity.lock()->addComponent<component::SceneChangeComponent>(&damy));

		entity = framework::Entity::createEntity("PhysicsWorld", "BulletPhysics", util::Transform());
		auto world = entity.lock()->addComponent<component::PhysicsWorld>(&damy);
		std::vector<std::string> param = { "0","-5280","0" };
		world.lock()->setParam(param);
		m_CreatedComponent.emplace_back(world);

		entity = framework::Entity::createEntity("Hierarchy", "System", util::Transform(util::Vec3(), util::Vec3(), util::Vec3(1, 1, 1)));
		auto hierarchy = entity.lock()->addComponent<component::Hierarchy>(&damy);
		m_CreatedComponent.emplace_back(hierarchy);

		entity = framework::Entity::createEntity("Inspector", "System", util::Transform(util::Vec3(), util::Vec3(), util::Vec3(1, 1, 1)));
		auto inspector = entity.lock()->addComponent<component::Inspector>(&damy);
		m_CreatedComponent.emplace_back(inspector);

	}

	void Scene::resourceLoad()
	{
		//リソース読み込み
		SceneThread sceneThread(m_CurrentScenePath);
		sceneThread.start();

		util::SetMultiThreadFlag(TRUE);

		util::LuaCpp lua("Resource/Script/Lua/Load.lua");

		auto stencilState = util::createDepthStencilState();
		auto rs = util::createRasterizerSate(FALSE);
		/*D3D11_BLEND_DESC blendDesc;

		ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
		blendDesc.AlphaToCoverageEnable = FALSE;
		blendDesc.IndependentBlendEnable = TRUE;
		blendDesc.RenderTarget[0].BlendEnable = true;
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_SRC_ALPHA;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_DEST_ALPHA;
		blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;*/

		D3D11_BLEND_DESC blendDesc;
		ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
		blendDesc.AlphaToCoverageEnable = FALSE;
		blendDesc.IndependentBlendEnable = TRUE;
		blendDesc.RenderTarget[0].BlendEnable = true;
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_SRC_ALPHA;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

		auto blend = util::createBlendState(false, &blendDesc);
		util::getContext()->RSSetState(rs.p);
		util::getContext()->OMSetDepthStencilState(stencilState.p, 0);
		util::setBlendState(blend.p);

		util::Timer timer(30);
		timer.init();

		auto tex = std::make_shared<framework::Texture2D>("Resource/Texture/Loading.png");
		std::unique_ptr<framework::SpriteRenderer> renderer = std::make_unique<framework::SpriteRenderer>();
		renderer->setSize(tex->getSize());
		renderer->setTexture(tex);

		auto redMashroomTex = std::make_shared<framework::Texture2D>("Resource/Texture/Title/UI_redmashroom.png");
		std::unique_ptr<framework::SpriteRenderer> redMashroomRenderer = std::make_unique<framework::SpriteRenderer>();
		redMashroomRenderer->setSize(redMashroomTex->getSize());
		redMashroomRenderer->setTexture(redMashroomTex);
		auto redSecondMashroomTex = std::make_shared<framework::Texture2D>("Resource/Texture/Title/UI_redmashroom.png");
		std::unique_ptr<framework::SpriteRenderer> redSecondMashroomRenderer = std::make_unique<framework::SpriteRenderer>();
		redSecondMashroomRenderer->setSize(redSecondMashroomTex->getSize());
		redSecondMashroomRenderer->setTexture(redSecondMashroomTex);

		auto blackMashroomTex = std::make_shared<framework::Texture2D>("Resource/Texture/Title/UI_blackmashroom.png");
		std::unique_ptr<framework::SpriteRenderer> blackMashroomRenderer = std::make_unique<framework::SpriteRenderer>();
		blackMashroomRenderer->setSize(blackMashroomTex->getSize());
		blackMashroomRenderer->setTexture(blackMashroomTex);
		auto blackSecondMashroomTex = std::make_shared<framework::Texture2D>("Resource/Texture/Title/UI_blackmashroom.png");
		std::unique_ptr<framework::SpriteRenderer> blackSecondMashroomRenderer = std::make_unique<framework::SpriteRenderer>();
		blackSecondMashroomRenderer->setSize(blackSecondMashroomTex->getSize());
		blackSecondMashroomRenderer->setTexture(blackSecondMashroomTex);

		auto greenMashroomTex = std::make_shared<framework::Texture2D>("Resource/Texture/Title/UI_greenmashroom.png");
		std::unique_ptr<framework::SpriteRenderer> greenMashroomRenderer = std::make_unique<framework::SpriteRenderer>();
		greenMashroomRenderer->setSize(greenMashroomTex->getSize());
		greenMashroomRenderer->setTexture(greenMashroomTex);

		auto bgTex = std::make_shared<framework::Texture2D>("Resource/Texture/LoadGB.png");
		std::unique_ptr<framework::SpriteRenderer> bgRenderer = std::make_unique<framework::SpriteRenderer>();
		bgRenderer->setSize(bgTex->getSize());
		bgRenderer->setTexture(bgTex);

		auto fadeTex = std::make_shared<framework::Texture2D>("Resource/Texture/Fade.png");
		std::unique_ptr<framework::SpriteRenderer> fadeRenderer = std::make_unique<framework::SpriteRenderer>();
		fadeRenderer->setSize(fadeTex->getSize());
		fadeRenderer->setTexture(fadeTex);


		auto uiTarget = std::make_shared<framework::RenderTarget>(framework::Screen::UI_WIDTH, framework::Screen::UI_HEIGHT, DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM);

		std::unique_ptr<framework::RenderTarget> backBuufer = std::make_unique<framework::RenderTarget>(util::getBackBuffer(), DXGI_FORMAT_R8G8B8A8_UNORM);

		auto luaMoveTimer = lua.getParam<int>("MoveTimer");
		auto luaInterbalTimer = lua.getParam<int>("blackInterbalTimer");
		auto luaGreenInterbal = lua.getParam<int>("greenInterbalTimer");
		auto luaRedSecondInterbal = lua.getParam<int>("redSecondInterbalTimer");
		auto luaBrackSecondInterbal = lua.getParam<int>("blackSecondInterbalTimer");
		auto loadPosition = lua.callFunc<util::Vec2>("LoadPos");
		util::Vec4 EnemyPosition = lua.callFunc<util::Vec4>("EnemyPos");

		util::Timer moveTimer(luaMoveTimer);
		util::Timer blackMoveTimer(luaMoveTimer);
		util::Timer greenMoveTimer(luaMoveTimer);
		util::Timer redsScondMoveTimer(luaMoveTimer);
		util::Timer blackSecondMoveTimer(luaMoveTimer);

		util::Timer blackStartTimer(luaInterbalTimer);
		util::Timer greenStartTimer(luaGreenInterbal);
		util::Timer redsScondStartTimer(luaRedSecondInterbal);
		util::Timer blackSecondStartTimer(luaBrackSecondInterbal);

		m_brack = false;
		m_green = false;
		m_redsecond = false;
		m_blacksecond = false;
		bool isLoadEnd = false;
		bool isThredEnd = false;

		util::Timer fadeTimer(30);

		while (!isLoadEnd)
		{
			//2D用ターゲットに切り替え
			util::getContext()->OMSetRenderTargets(1, uiTarget->getView(), NULL);
			uiTarget->clear(util::Vec4(0, 0, 0, 0));

			fadeTimer.update();
			if (sceneThread.isEnd() && !isThredEnd) {
				fadeTimer.init();
				isThredEnd = true;
			}

			if (isThredEnd) {
				if (fadeTimer.isEnd()) {
					isLoadEnd = true;
				}
			}



			m_FpsTimer.update();
			timer.update();
			if (timer.isEnd())
				timer.init();

			float scale = 0.2f;
			float mashroomScale = 0.3f;

			//2D用ターゲットと同じサイズのビューポート
			util::setSingleViewPort(framework::Screen::WINDOW_WIDTH, framework::Screen::WINDOW_HEIGHT);
			draw(bgRenderer.get(), util::Vec2(1280 * 0.5, 720 * 0.5), util::Vec2(0.7, 0.7));

			//大きさの変更
			draw(renderer.get(), loadPosition, util::Vec2(scale, scale));

			moveTimer.update();
			blackStartTimer.update();
			greenStartTimer.update();
			redsScondStartTimer.update();
			blackSecondStartTimer.update();

			if (moveTimer.isEnd()) {
				moveTimer.init();
			}
			if (blackStartTimer.isEnd()) {
				blackMoveTimer.update();
			}
			if (blackMoveTimer.isEnd()) {
				blackMoveTimer.init();
			}
			if (greenStartTimer.isEnd()) {
				greenMoveTimer.update();
			}
			if (greenMoveTimer.isEnd()) {
				greenMoveTimer.init();
			}
			if (redsScondStartTimer.isEnd()) {
				redsScondMoveTimer.update();
			}
			if (redsScondMoveTimer.isEnd()) {
				redsScondMoveTimer.init();
			}
			if (blackSecondStartTimer.isEnd()) {
				blackSecondMoveTimer.update();
			}
			if (blackSecondMoveTimer.isEnd()) {
				blackSecondMoveTimer.init();
			}

			auto redMoveSpd = util::bezierCurve(moveTimer.rate(), EnemyPosition.x, EnemyPosition.y, EnemyPosition.z, EnemyPosition.w);
			auto blackMoveSpd = util::bezierCurve(blackMoveTimer.rate(), EnemyPosition.x, EnemyPosition.y, EnemyPosition.z, EnemyPosition.w);
			auto greenMoveSpd = util::bezierCurve(greenMoveTimer.rate(), EnemyPosition.x, EnemyPosition.y, EnemyPosition.z, EnemyPosition.w);
			auto redsecondMoveSpd = util::bezierCurve(redsScondMoveTimer.rate(), EnemyPosition.x, EnemyPosition.y, EnemyPosition.z, EnemyPosition.w);
			auto blacksecondMoveSpd = util::bezierCurve(blackSecondMoveTimer.rate(), EnemyPosition.x, EnemyPosition.y, EnemyPosition.z, EnemyPosition.w);

			draw(redMashroomRenderer.get(), util::Vec2(1200 - 480, redMoveSpd), util::Vec2(mashroomScale, mashroomScale));
			draw(redSecondMashroomRenderer.get(), util::Vec2(1200 - 120, redsecondMoveSpd), util::Vec2(mashroomScale, mashroomScale));
			draw(blackMashroomRenderer.get(), util::Vec2(1200 - 360, blackMoveSpd), util::Vec2(mashroomScale, mashroomScale));
			draw(blackSecondMashroomRenderer.get(), util::Vec2(1200, blacksecondMoveSpd), util::Vec2(mashroomScale, mashroomScale));
			draw(greenMashroomRenderer.get(), util::Vec2(1200 - 240, greenMoveSpd), util::Vec2(mashroomScale, mashroomScale));

			if (isThredEnd) {
				fadeRenderer->setAlpha(1.0f - fadeTimer.rate());
			}
			else {
				fadeRenderer->setAlpha(fadeTimer.rate());
			}
			
			draw(fadeRenderer.get(), util::Vec2(1280 / 2, 720 / 2), util::Vec2(1280, 720));



			//最終出力なのでバックバッファと同じサイズのビューポート
			util::setSingleViewPort(framework::Screen::PIXEL_WIDTH, framework::Screen::PIXEL_HEIGHT);
			util::getContext()->OMSetRenderTargets(1, backBuufer->getView(), NULL);
			backBuufer->clear(util::Vec4(0, 0, 0, 1));

			//ターゲットの描画位置はウィンドウの中心位置
			auto trans = util::Transform(util::Vec3(framework::Screen::WINDOW_WIDTH_HALF, framework::Screen::WINDOW_HEIGHT_HALF, 0), util::Vec3(0, 0, 0), util::Vec3(1, 1, 1));

			uiTarget->draw(&trans);

			framework::DirectXInstance::getInstance()->getSwapChain()->Present(0, 0);
			auto waiteTime = m_FpsTimer.waiteTime();
			if (waiteTime > 0) {
				Sleep(waiteTime);
			}
		}


		util::getContext()->OMSetRenderTargets(0, NULL, NULL);
		sceneThread.join();
		util::SetMultiThreadFlag(FALSE);
		m_CreateList = sceneThread.getCreateList();
	}

	void Scene::createEntity(const std::string & fileName)
	{
		util::CSVLoader entityCSVLoader(fileName + "/Entity.csv");
		auto entityList = entityCSVLoader.load();

		//CSVのコメント行削除
		entityList.erase(entityList.begin());


		//!一時的にエンティティを保持するためのコンテナ
		std::list<std::pair<std::string, std::weak_ptr<Entity>>> entityTempContainer;

		for (auto& list : entityList) {
			auto transform = util::Transform(util::atof<util::Vec3>(list, 3).reault,
				util::atof<util::Vec3>(list, 6).reault,
				util::atof<util::Vec3>(list, 9).reault);
			auto createdEntity = Entity::createEntity(list[1], list[2], transform);
			std::function<void()> func = (bool)std::atoi(list[0].c_str()) ? std::bind(&Entity::active, createdEntity.lock().get()) : std::bind(&Entity::deActive, createdEntity.lock().get());
			m_EntityActiveFunc.emplace_back(func);
			if (list.size() >= 13) {
				if (list[12] != "")
					entityTempContainer.emplace_back(std::make_pair(list[12], createdEntity));
			}
		}

		for (auto entity : entityTempContainer) {
			entity.second.lock()->setParent(Entity::findGameObj(entity.first));
		}
	}

	void Scene::createComponent(const std::string & fileName)
	{
		util::CSVLoader componentCSVLoader(fileName + "/Component.csv");
		auto componentList = componentCSVLoader.load();

		//CSVのコメント行削除
		componentList.erase(componentList.begin());
		componentList.reserve(componentList.size());

		//コンポーネントからaddComponentされたものを接続
		for (auto crete : m_CreateList) {
			auto&& entity = Entity::findGameObj(crete.entityName);
			auto component = m_Builder.create(crete.componentName);
			entity.lock()->addComponent(component);
			component->componentCreated(entity);
			component->active();
			m_CreatedComponent.emplace_back(component);
			component->setParam(crete.param);
		}

		for (auto& list : componentList) {

			//コンポーネントの名前をもとに生成
			auto component = m_Builder.create(list[1]);
			//接続したいオブジェクトを名前でさがす(名前かぶりはない)
			auto entity = Entity::findGameObj(list[2]);
			assert(!entity.expired() && "接続しようとしたゲームオブジェクトが存在しません");
			entity.lock()->addComponent(component);//ゲームオブジェクトにコンポーネント付与
			component->setGameObj(entity);
			bool isActive = (bool)std::atoi(list[0].c_str());
			//パラメータまでの情報を削除
			list.erase(list.begin(), std::next(list.begin(), 3));
			//パラメータがあればセットする
			if (!list.empty())
				component->setParam(list);

			component->onConect();

			//0以外だったらアクティブ
			if (isActive)component->active();
			else component->deActive();
			m_CreatedComponent.emplace_back(component);

		}

		//コンポーネントが作られたあとにエンティティのアクティブフラグを切り替えて
		//エンティティー優先にする
		for (auto func : m_EntityActiveFunc) {
			func();
		}

	}

	void Scene::loadNextSceneName(const std::string & fileName)
	{
		util::CSVLoader loader(fileName + "/NextScene.csv");
		auto data = loader.load();
		data.erase(data.begin());
		m_NextSceneName = data[0][0];
	}

	void Scene::clear()
	{
		framework::TaskManager::getInstance()->clear();
		ResourceManager::getInstance()->clear();
		Entity::clear();
		m_EntityActiveFunc.clear();
		m_CreatedComponent.clear();
	}

	void Scene::setScenePath(const std::string & path)
	{
		m_CurrentSceneName = path;
		m_CurrentScenePath = m_LootPath + path;
	}

}